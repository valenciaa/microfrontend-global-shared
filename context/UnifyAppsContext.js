import React, { createContext, useContext, useEffect, useState } from 'react'

export const UnifyAppsContext = createContext({})
export const UnifyAppsUpdateContext = createContext({})

function useUnifyAppsContext() {
  return useContext(UnifyAppsContext)
}

function useUnifyAppsUpdateContext() {
  return useContext(UnifyAppsUpdateContext)
}

export const UnifyAppsProvider = ({ children, unifyBaseStyles, ...props }) => {
  const { pageFrom, baseHeaderTitle } = props
  const [disabledBaseHeader, setDisabledBaseHeader] = useState(false)
  const [disabledBaseFooter, setDisabledBaseFooter] = useState(true)
  const [isChatScrollBypass, setIsChatScrollBypass] = useState(false)
  const [headerTitle, setHeaderTitle] = useState(baseHeaderTitle)

  useEffect(() => {
    if (baseHeaderTitle) {
      setHeaderTitle(baseHeaderTitle)
    }
  }, [baseHeaderTitle])

  return (
    <UnifyAppsContext.Provider
      value={{
        pageFrom,
        unifyBaseStyles,
        disabledBaseHeader,
        disabledBaseFooter,
        headerTitle,
        isChatScrollBypass,
      }}
    >
      <UnifyAppsUpdateContext.Provider
        value={{
          setDisabledBaseHeader,
          setDisabledBaseFooter,
          setHeaderTitle,
          setIsChatScrollBypass,
        }}
      >
        {children}
      </UnifyAppsUpdateContext.Provider>
    </UnifyAppsContext.Provider>
  )
}

export { useUnifyAppsContext, useUnifyAppsUpdateContext }
