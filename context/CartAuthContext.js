import localforage from 'localforage'
import React, { createContext, useContext, useEffect, useState } from 'react'
import config from '../../../config'
import Observation from '../container/observation'
import useMemoState from '../utils/hooks/useMemoState'

export const CartAuthContext = createContext({})

export const CartAuthProvider = ({
  children,
  currentPage = '',
  correlationID = '',
}) => {
  const [state, setState] = useState({
    cart: null,
    auth: null,
    access_token: '',
    minicart_id: null,
    // isAuthRefetched only true after auth data is fetched or get from localforage.
    isAuthRefetched: false,
    // isCartRefetched only true after done fetch minicart data (and done checking payment cart if any)
    isCartRefetched: false,
    // ? to store b2bc data, path, url key and name
    b2bcData: null,
    // ? to store klk activation status
    isKlk: false,

    // ? State and refetch for get minicart v3
    isLoadingGetCart: false,
    isFetchingGetCart: false,
    isSuccessGetCart: false,
    isFetchedGetCart: false,
    refetchMinicartItems: null,

    // ? state to set current page
    currentPage: currentPage,

    // ? cart total qty
    // ? seperate state from cart so info can be updated independently, used in desktop only
    totalCartQty: 0,

    // ? state to set correlationID
    correlationID: correlationID,
  })

  useEffect(() => {
    async function getStorageAuthData() {
      //! Need to remind, comment this back if you want to merge to master
      // await localforage.setItem('access_token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcl9pZCI6IjQ3OTE0NjgiLCJpYXQiOjE2OTI4NDM4MzksImlzcyI6IndhcGkucnVwYXJ1cGEifQ.ZKLv4pqXPAw7V8kLHL7Hxa7h0LItSHX-6Dxb0GvI9QQ') // if you want to login in mobile web just put minicart_id from stg that already login and uncomment this
      let accessToken = '' // for validation state isLogin

      let minicartId = '' // if there is no minicart_id, we get in from cart_id

      let b2bc = await localforage.getItem('b2bc')

      if (config.isB2b) {
        const authDataB2b = await localforage.getItem(
          `persist:ruparupa_b2b_${config.companyNameCSS}`,
        )
        const parsedAuthDataB2b = JSON.parse(authDataB2b)

        accessToken = parsedAuthDataB2b?.access_token
        minicartId = parsedAuthDataB2b?.minicart_id || ''
      } else {
        accessToken = await localforage.getItem('access_token') // for validation state isLogin

        minicartId = (await localforage.getItem('minicart_id')) ?? '' // if there is no minicart_id, we get in from cart_id
      }

      // check activation Status
      let isKlkActive = false
      const authData = await localforage.getItem('persist:ruparupa')
      let parsedAuthData = JSON.parse(authData)
      parsedAuthData = parsedAuthData?.auth
        ? JSON.parse(parsedAuthData.auth)
        : null
      if (parsedAuthData?.isKlk) {
        isKlkActive = true
      } // isKlk selalu diluar user => ada parsedAuthData.user dan parsedAuthData.isKlk

      setState((prev) => ({
        ...prev,
        access_token: accessToken,
        minicart_id: minicartId,
        b2bcData: JSON.parse(b2bc),
        isKlk: isKlkActive,
      }))
    }
    getStorageAuthData()
  }, [])

  const data = useMemoState(state, setState)
  const onChangeState = data.handleChangeState

  return (
    <CartAuthContext.Provider value={data}>
      <Observation
        handleChangeState={onChangeState}
        state={data.state}
        setState={data.setState}
      >
        {children}
      </Observation>
    </CartAuthContext.Provider>
  )
}

export function useCartAuthContext() {
  return useContext(CartAuthContext)
}
