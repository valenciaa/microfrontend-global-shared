import React, { createContext, useContext, useEffect, useState } from 'react'
import {
  getCorrelationId,
  recoBodyFeed,
  vuePageView,
  vueRecommendationView,
} from '../utils/VueAIUtil'
import isEmpty from 'lodash/isEmpty'
import isNumber from 'lodash/isNumber'

import {
  useGetVueRecommend,
  useSendTrackEvent,
} from '../services/api/mutations/global'
import { useLayoutContext } from '../../../src/context/LayoutContext'
import config from '../../../config'
// import { useRouter } from 'next/router'

export const VueRecoContext = createContext({})

export const VueContext = ({ children, ...props }) => {
  const {
    from = 'pcp-category',
    categoryDetail = {},
    keyword = '',
    ssrTotalProduct = 0,
    brand = '',
    additionalProps = {},
    ruleBase = '',
  } = props

  // const router = useRouter()

  const [vueModuleData, setVueModuleData] = useState(null)

  const [isRefetch, setIsRefetch] = useState(true)
  const [activeKeyword, setActiveKeyword] = useState('')

  const { state: pcpContext } = useLayoutContext()

  const {
    nbHits = undefined,
    inViewVueModule,
    pageTypeDefault = '',
  } = pcpContext || {}

  const determinePageFrom = () => {
    if (!isEmpty(keyword)) {
      if (recoContext?.productsCsr?.total === 0) {
        return 'empty-search'
      }
      setActiveKeyword(keyword)
      return 'search'
    }

    return from
  }

  const [recoContext, setRecoContext] = useState({
    vueRecommedation: vueModuleData,
    correlationID: '',
    inViewModule: null,
    productsCsr: null,
    // algolia
    productsAlgolia: undefined,
    // tahu
    vueInView: false,
    additionalValue: null,
    tahuModule: null,
  })

  useEffect(() => {
    if (isEmpty(inViewVueModule?.data)) {
      if (
        recoContext?.inViewModule &&
        !isEmpty(recoContext?.inViewModule?.data)
      ) {
        vueRecommendationView({
          pageFrom: from,
          module: recoContext?.inViewModule,
          correlationID: recoContext?.correlationID,
          sendTracking,
        })
      }

      // if reco is shown but dont have data, dont send tracker
      return
    }
    let pageFrom = from === 'keyword' ? determinePageFrom() : from

    if (!isEmpty(pageTypeDefault) && pageFrom !== pageTypeDefault) {
      pageFrom = pageTypeDefault
    }
    if (from === 'keyword' && nbHits === 0 && !isEmpty(keyword)) {
      pageFrom = 'empty-search'
    }

    if (['pcp-category', 'pcp-brand']?.includes(from)) {
      if (pageFrom === 'pcp-category') {
        pageFrom = 'cat'
      }

      if (!isEmpty(keyword)) {
        pageFrom = 'pcp-search'
        if (ssrTotalProduct === 0 || nbHits === 0) {
          pageFrom = 'empty-search'
        }
      }
    }

    vueRecommendationView({
      pageFrom: pageFrom,
      module: inViewVueModule,
      correlationID: recoContext?.correlationID,
      sendTracking,
    })
  }, [inViewVueModule, recoContext?.inViewModule]) // handle recomendation product view in view

  const { mutate } = useGetVueRecommend({
    onSuccess: async (res) => {
      if (res?.data?.data) {
        const { setFetchVueArticle = {} } = additionalProps || {}
        if (from === 'microsite' && setFetchVueArticle) {
          setFetchVueArticle(true)
        }

        const data = res?.data?.data

        data?.data_module?.forEach((element, idx) => {
          if (
            isEmpty(element?.data) ||
            (from === 'pcp-category' && idx === 1)
          ) {
            // or stop hit when reco is on second place
            // if reco is shown but dont have data, dont send tracker
            return
          }

          let altPageFrom = from
          // for feed will send pcp-category and, for tracker send cat
          if (['pcp-category', 'pcp-brand']?.includes(from)) {
            altPageFrom = 'cat'
            if (from === 'pcp-brand') {
              altPageFrom = from
            }
            if (!isEmpty(keyword)) {
              altPageFrom = 'pcp-search'
              if (
                !isNumber(recoContext?.productsCsr?.total) &&
                (ssrTotalProduct === 0 || nbHits === 0)
              ) {
                altPageFrom = 'empty-search'
              }
            }
          }

          const useInViewPage = [
            'page-not-found',
            'rewards',
            'pcp-brand',
            'keyword',
            'microsite',
          ]
          const pageName = determinePageFrom()
          let pageUseInView = true
          if (
            ['pcp-category', 'keyword']?.includes(from) &&
            pageName === 'empty-search'
          ) {
            pageUseInView = false
          }

          if (!useInViewPage.includes(from) && pageUseInView) {
            // another than page useInViewPage will Hit recoView at top tier, because when page is showing, the reco will also shown
            vueRecommendationView({
              pageFrom: from === 'keyword' ? 'search' : altPageFrom,
              module: element,
              correlationID: recoContext?.correlationID,
              sendTracking,
            })
          }
        })
        setVueModuleData(data)
      }
    },
  })

  const { mutate: sendTracking } = useSendTrackEvent()

  useEffect(() => {
    if (keyword !== activeKeyword) {
      setIsRefetch(true)
    }
  }, [keyword]) // Explicitly define dependencies

  const fetchVueReco = async (pageFrom, { additionalValue = null } = {}) => {
    try {
      const newCorrelationID = getCorrelationId()

      setRecoContext((prevContext) => ({
        ...prevContext,
        correlationID: newCorrelationID,
      }))

      if (config?.node_env === 'development') {
        console.log(
          `Fetching Vue Reco for ${pageFrom}\nwith correlation id ${newCorrelationID}...`,
        )
      }

      const payload = await recoBodyFeed({
        from: pageFrom,
        categoryDetail,
        keyword,
        correlationID: newCorrelationID,
        brand,
        ...(additionalValue && { additionalValue }),
        ...(ruleBase && { ruleBase }),
      })

      // for feed will send pcp-category and, for tracker send cat
      if (pageFrom === 'pcp-category') {
        pageFrom = 'cat'
      }

      if (
        ['pcp-category', 'pcp-brand']?.includes(from) &&
        payload?.data?.page_name === 'search'
      ) {
        pageFrom = 'pcp-search'
      }
      if (from === 'page-not-found') {
        pageFrom = from
      }
      vuePageView({
        pageFrom,
        correlationID: newCorrelationID,
        sendTracking,
      })

      // Call mutate function with the payload
      if (isRefetch) {
        if (pageFrom === 'microsite') {
          if (!additionalProps?.fetchVueArticle) {
            mutate(payload)
            return
          }
          return
        }

        mutate(payload)
        setIsRefetch(false)
      } else if (
        ['search'].includes(pageFrom) ||
        (pageFrom === 'empty-search' && from === 'keyword')
      ) {
        mutate(payload)
      }
    } catch (error) {
      if (config?.node_env === 'development') {
        console.error('Failed to fetch Vue Reco:', error)
      }
    }
  }

  useEffect(() => {
    // recoContext?.productsCsr is used exclusively on category pages, rrr pages, and 404 pages.
    // It is managed within ProductContainer.js. Whenever a product changes,
    // recoContext?.productsCsr is updated by modifying the context states.

    if (
      (recoContext?.productsCsr?.total === undefined &&
        !['page-not-found', 'rewards']?.includes(from)) ||
      from === 'microsite'
    ) {
      // page not found will always have nbhits undefined so we bypass it
      return
    }

    fetchVueReco(determinePageFrom())
  }, [recoContext?.productsCsr])

  useEffect(() => {
    // nbHits only for keyword page, where it will changes every keyword different

    if (from === 'keyword') {
      if (recoContext?.productsAlgolia !== undefined) {
        // prevent hit feed multiples when changes filter on /jual
        if (keyword === activeKeyword) {
          return
        }

        fetchVueReco(
          recoContext?.productsAlgolia === 0 ? 'empty-search' : 'search',
        )
      }

      return
    }
  }, [recoContext?.productsAlgolia])

  useEffect(() => {
    // vueInView value is boolean, only flag is the ueer already see the component or not
    const { vueInView, additionalValue = null } = recoContext || {}

    if (
      from === 'microsite' &&
      vueInView &&
      additionalValue?.vue_attributes !== undefined
    ) {
      fetchVueReco('microsite', {
        additionalValue,
      })
      return
    }
  }, [recoContext?.vueInView])

  useEffect(() => {
    setRecoContext((prevContext) => ({
      ...prevContext,
      vueRecommedation: vueModuleData,
    }))
  }, [vueModuleData])

  // State to store the list of slot_ids that have been processed (slot id will be uniq)
  const [processedSlotIds, setProcessedSlotIds] = useState([])

  useEffect(() => {
    const { pageFrom = '', module = {} } = recoContext?.tahuModule || {}
    const { slot_id = '' } = recoContext?.tahuModule?.module || {}

    if (recoContext?.tahuModule === null && !slot_id) {
      return
    }

    if (!processedSlotIds?.includes(slot_id)) {
      setProcessedSlotIds((prevSlotIds) => [...prevSlotIds, slot_id]) // Store the slot_id

      // Send the data (replace with your actual sending logic)
      vueRecommendationView({
        pageFrom: pageFrom,
        module: module,
        sendTracking,
        correlationID: recoContext?.correlationID,
        slot_id,
      })
    }
    // else is 'Slot ID already processed'
  }, [recoContext?.tahuModule])

  return (
    <VueRecoContext.Provider value={{ ...recoContext, setRecoContext }}>
      {children}
    </VueRecoContext.Provider>
  )
}

export function useVueContext() {
  return useContext(VueRecoContext)
}
