import { createContext, useContext, useState } from 'react'
import useMemoState from '../utils/hooks/useMemoState'

export const AddressContext = createContext({})

const initialState = {
  address: [],
  addressLength: 0,
  fullAddressErrorMessage: '',
  selectedAddressId: '',
  selectedEdit: {},
  showAdd: false,
  showEdit: false,
  showQuitConfirmation: false,
  showPrimaryConfirmation: false,
  showDeleteConfirmation: false,
  showMap: false,
  showConfirmationFullAddress: false,
  showAddressCardList: false,
  isChecked: false,
  isShowInfoBox: false,
  infoBoxType: '',
  infoBoxMessage: '',
  isLoading: false,
  isRefetching: false,
  mode: 'add',
  pageFrom: '',
}

export const AddressProvider = ({ children }) => {
  const [state, setState] = useState(initialState)

  const data = useMemoState(state, setState)

  return (
    <AddressContext.Provider value={data}>{children}</AddressContext.Provider>
  )
}

export function useAddressContext() {
  return useContext(AddressContext)
}
