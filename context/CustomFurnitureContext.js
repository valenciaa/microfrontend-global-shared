import React, { createContext, useContext, useEffect, useState } from 'react'

export const CustomFurnitureContext = createContext()

export const CustomFurnitureProvider = ({ children, results }) => {
  const [customFurnitureResults, setCustomFurnitureResults] = useState(results)
  const [infobarType, setinfobarType] = useState('')
  const [errMsg, setErrMsg] = useState('')

  useEffect(() => {
    setCustomFurnitureResults(results)
  }, [results])

  const setSnackbar = (type = '', message = '') => {
    // condition if type is error-general, but message is empty
    if (type === 'error-general' && !message) {
      setinfobarType('error-default')
    } else {
      setinfobarType(type)
      setErrMsg(message)
    }
  }

  const value = {
    customFurnitureResults,

    // function to display snackbar
    setSnackbar,
    infobarType,
    errMsg,
  }

  return (
    <CustomFurnitureContext.Provider value={value}>
      {children}
    </CustomFurnitureContext.Provider>
  )
}

export const useCustomFurnitureContext = () => {
  return useContext(CustomFurnitureContext)
}
