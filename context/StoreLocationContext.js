import { useRouter } from 'next/router'
import React, {
  createContext,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'
import config from '../../../config'
import {
  CATALOGUES_COMPANIES,
  CATALOGUES_ROW_FILTERS,
  CATALOGUES_SIZE,
  DUMMY_CATALOGUES_DATA,
} from '../utils/constants'

export const StoreLocationContext = createContext()

export const StoreLocationProvider = ({ children, results, styles }) => {
  const router = useRouter()

  const [storeLocationResults, setStoreLocationResults] = useState(results)
  const [storeLocationStyles, setStoreLocationStyles] = useState(styles)
  const [page, setPage] = useState(1)
  const [infobarType, setinfobarType] = useState('')
  const [errMsg, setErrMsg] = useState('')

  useEffect(() => {
    setStoreLocationResults(results)
    setStoreLocationStyles(styles)
  }, [results, styles])

  const filterRow = CATALOGUES_ROW_FILTERS[config.companyCode]

  const setCurrentPage = (page) => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
    router.push(
      {
        pathname: router.pathname,
        query: {
          ...router.query,
          offset: (page - 1) * CATALOGUES_SIZE,
        },
      },
      undefined,
      {
        shallow: false,
      },
    )
    setStoreLocationResults((prevStoreLocationResults) => ({
      ...prevStoreLocationResults,
      ecatalogues: {
        ...prevStoreLocationResults.ecatalogues,
        catalogDetail: DUMMY_CATALOGUES_DATA,
      },
    }))
    setPage(page)
  }

  useEffect(() => {
    if (results?.query.offset) {
      setPage(Math.ceil(results.query.offset / CATALOGUES_SIZE + 1))
    }
  }, [results?.query.offset])

  const storeLocationData = useMemo(
    () => ({
      storeLocationResults,
      storeLocationStyles,
      companies: CATALOGUES_COMPANIES,
      filterRow,
      size: CATALOGUES_SIZE,
      page,
      setCurrentPage,
    }),
    [storeLocationResults, storeLocationStyles],
  )

  const setSnackbar = (type = '', message = '') => {
    // condition if type is error-general, but message is empty
    if (type === 'error-general' && !message) {
      setinfobarType('error-default')
    } else {
      setinfobarType(type)
      setErrMsg(message)
    }
  }

  const value = {
    storeLocationData,

    // function to display snackbar
    setSnackbar,
    infobarType,
    errMsg,
  }

  return (
    <StoreLocationContext.Provider value={value}>
      {children}
    </StoreLocationContext.Provider>
  )
}

export const useStoreLocationContext = () => {
  return useContext(StoreLocationContext)
}
