import React, { createContext, useContext, useState, useEffect } from 'react'
import { useGetPinStatus } from '../services/api/queries/pin'

export const PinContext = createContext({})
export const PinUpdateContext = createContext({})

function usePinContext() {
  return useContext(PinContext)
}

function usePinUpdateContext() {
  return useContext(PinUpdateContext)
}

export const PinProvider = ({ children }) => {
  const [pinContext, setPinContext] = useState({
    pinStatus: {},
    failedPin: 0,
    toggleModalSetNewPin: { toggle: false, from: '', success: false },
    toggleModalUserHasPin: { toggle: false, from: '', success: false },
    toggleModalBlockedPin: { toggle: false, from: '', success: false }, // for now only for mobile web
    toggleModalRecreatePin: { toggle: false, from: '', sucess: false }, // for now only for mobile web
  })
  const {
    data: _pinStatusResp,
    isLoading: pinStatusLoading,
    isError: pinStatusIsError,
    refetch: refetchPinStatus,
  } = useGetPinStatus()

  useEffect(() => {
    if (_pinStatusResp && !pinStatusLoading && !pinStatusIsError) {
      setPinContext((current) => ({ ...current, pinStatus: _pinStatusResp }))
    }
  }, [_pinStatusResp])

  function failedPinCounter(failed = true) {
    setPinContext((current) => ({
      ...current,
      failedPin: failed ? current.failedPin + 1 : 0,
    }))
  }

  function toggleModalForPin(toggleModalType, toggle, from, success) {
    // for now only for mobile web
    setPinContext((current) => ({
      ...current,
      [toggleModalType]: { toggle: toggle, from: from, success: success },
    }))
  }

  return (
    <PinContext.Provider value={{ ...pinContext, pinStatusLoading }}>
      <PinUpdateContext.Provider
        value={{ refetchPinStatus, failedPinCounter, toggleModalForPin }}
      >
        {children}
      </PinUpdateContext.Provider>
    </PinContext.Provider>
  )
}

export { usePinContext, usePinUpdateContext }
