import { useState, useContext, createContext, useEffect } from 'react'

import useMemoState from '../utils/hooks/useMemoState'
// import { useGetMinicartItems } from '../services/api/queries/minicart'
import { useGetAllWishlistV2 } from '../services/api/queries/global'
import {
  useAddMinicartItem,
  useAddOrDeleteWishlistItems,
  useDeleteItems,
  usePaymentPreprocessing,
  useUpdateItemCheck,
  useUpdateItemQty,
} from '../services/api/mutations/minicart'
import { useCartAuthContext } from './CartAuthContext'
import config from '../../../config'

export const MinicartContext = createContext({})
export const debounceTime = 500

export const MinicartProvider = ({
  children,
  from = 'minicart',
  cartProps,
}) => {
  const { state: cartAuthContext, handleChangeState } = useCartAuthContext()
  const [allItems, setAllItems] = useState([])
  const [itemsPerGroup, setItemsPerGroup] = useState({})
  const [isAllChecked, setIsAllChecked] = useState(false)
  const [isAnyChecked, setIsAnyChecked] = useState(false)
  const [summary, setSummary] = useState({
    total: 0,
    subTotal: 0,
    totalItemChecked: 0,
    totalItem: 0,
    discount: 0,
  })
  const [infobarType, setinfobarType] = useState('')
  const [errMsg, setErrMsg] = useState('')
  const [vueProductIds, setVueProductIds] = useState([])
  const [wishlistItemsSku, setWishlistItemsSku] = useState([])

  // Revamp Wishlist State
  const [isOpenWishlistInfobox, setIsOpenWishlistInfobox] = useState(false)

  const refetchMinicartItems = cartAuthContext?.refetchMinicartItems
  const wishListParam = {
    limit: 30,
    offset: 0,
  }

  const [cartContext, setCartContex] = useState({ cartProps })

  useEffect(() => {
    setCartContex({ cartProps })
  }, [cartProps])

  const { state: itemsPerGroupMemo, setState: setItemsPerGroupMemo } =
    useMemoState(itemsPerGroup, setItemsPerGroup)

  // const { isLoading: isLoadingGetCart, isFetching: isFetchingGetCart, isSuccess: isSuccessGetCart, refetch: refetchMinicartItems } = useGetMinicartItems({
  //   onSuccess: (res) => {
  //     let productIds = []
  //     let groups = {}
  //     res.items.forEach((group) => {
  //       productIds = [...productIds, ...group.group_items.map((el) => el.sku)]
  //       groups = {
  //         ...groups,
  //         [group.store_code]: group
  //       }
  //     })
  //     setItemsPerGroup(groups)
  //     setAllItems(res.items)
  //     updateViewedSummary(res.grand_total, res.subtotal, res.total_checked_item_qty, res.total_item_qty, res.discount_amount)
  //     setVueProductIds(productIds)
  //     refetchWishlist()
  //   }
  // })

  useEffect(() => {
    if (cartAuthContext.cart) {
      const cart = cartAuthContext?.cart
      let productIds = []
      let groups = {}
      cart?.items?.forEach((group) => {
        productIds = [...productIds, ...group.group_items.map((el) => el.sku)]
        groups = {
          ...groups,
          [group.store_code]: group,
        }
      })
      setItemsPerGroup(groups)
      setAllItems(cart.items)
      updateViewedSummary(
        cart.grand_total,
        cart.subtotal,
        cart.total_checked_item_qty,
        cart.total_item_qty,
        cart.discount_amount,
      )
      setVueProductIds(productIds)
      if (from === 'minicart' && !config.isB2b) {
        refetchWishlist()
      }
    }
  }, [cartAuthContext.cart, from])

  const { data: wishlists, refetch: refetchWishlist } = useGetAllWishlistV2(
    wishListParam,
    {
      enabled: false,
      onSuccess: (res) => {
        if (res.items && Array.isArray(res.items)) {
          setWishlistItemsSku(
            res.items.map((el) => {
              if (el.sku.length && el.sku) {
                return el.sku
              } else {
                return ''
              }
            }),
          )
        }
      },
    },
  )

  const { mutate: addMinicartItem, isLoading: isLoadingAddMinicartItem } =
    useAddMinicartItem({
      onSuccess: async (res) => {
        if (res?.data?.error) {
          if (from === 'wishlist') {
            setIsOpenWishlistInfobox(true)
            setSnackbar('error-minicart', res.data.error.message)
          } else {
            setSnackbar(
              'error-general',
              res.data.error.message ? res.data.error.message : '',
            )
          }
        } else {
          if (from === 'wishlist') {
            if (config.environment === 'desktop') {
              refetchMinicartItems()
            }

            setIsOpenWishlistInfobox(true)
            setinfobarType('success-text-white')
          } else {
            setinfobarType('success-add')
          }
          {
            from === 'minicart' &&
              window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth',
              })
          }
        }
      },
    })

  const { mutate: updateQty, isLoading: isLoadingUpdateQty } = useUpdateItemQty(
    {
      onSuccess: (res) => {
        if (res.data.error) {
          setSnackbar(
            'error-general',
            res.data.error.message ? res.data.error.message : '',
          )
        } else {
          const {
            grand_total: total,
            subtotal,
            total_checked_item_qty: totalItemChecked,
            total_item_qty: totalItem,
            discount_amount: discount,
          } = res.data.data
          updateViewedSummary(
            total,
            subtotal,
            totalItemChecked,
            totalItem,
            discount,
          )
        }
      },
    },
  )

  const { mutate: mutateUpdateCheck, isLoading: isLoadingUpdateCheck } =
    useUpdateItemCheck({
      onSuccess: (res) => {
        if (res.data.error) {
          setSnackbar(
            'error-general',
            res.data.error.message ? res.data.error.message : '',
          )
        } else {
          const {
            grand_total: total,
            subtotal,
            total_checked_item_qty: totalItemChecked,
            total_item_qty: totalItem,
            discount_amount: discount,
          } = res.data.data
          updateViewedSummary(
            total,
            subtotal,
            totalItemChecked,
            totalItem,
            discount,
          )
        }
      },
    })

  const { mutate: mutateDeleteItems, isLoading: isLoadingDelete } =
    useDeleteItems({
      onSuccess: (res) => {
        if (res.data.error) {
          setSnackbar(
            'error-general',
            res.data.error.message ? res.data.error.message : '',
          )
        } else {
          const {
            grand_total: total,
            subtotal,
            total_checked_item_qty: totalItemChecked,
            total_item_qty: totalItem,
            discount_amount: discount,
          } = res.data.data
          updateViewedSummary(
            total,
            subtotal,
            totalItemChecked,
            totalItem,
            discount,
          )
        }
      },
    })

  const { mutate: addWishlistItems, isLoading: isLoadingWishlists } =
    useAddOrDeleteWishlistItems({
      onSuccess: (res) => {
        if (res.data.error) {
          if (res.data.status === 403) {
            window.location.href = config.baseURL + 'auth/login'
          } else {
            if (res?.data?.error?.message?.includes('Wishlist penuh')) {
              setSnackbar('error-wishlist-full', res?.data?.error?.message)
            } else {
              setSnackbar(
                'error-general',
                res?.data?.error?.message ? res?.data?.error?.message : '',
              )
            }
          }
        } else {
          mutateDeleteItems(
            { items: res?.data?.data?.success },
            {
              onSuccess: () => {
                refetchWishlist()
                deleteItemsByIds(
                  res?.data?.data?.success.map((el) => {
                    return {
                      id: el.id,
                      storeCode: el.storeCode,
                    }
                  }),
                )
                setSnackbar('success-add-wishlist')
              },
            },
          )
        }
      },
    })

  const updateItemBySku = (sku, storeCode, property, value, itemsPerGroup) => {
    const items = [...itemsPerGroup[storeCode]?.group_items]

    for (let i = 0; i < items.length; i++) {
      if (items[i].sku === sku) {
        // ? new value or the opposite value
        items[i][property] = value
        break
      }
    }
    setItemsPerGroupMemo({
      ...itemsPerGroup,
      [storeCode]: {
        ...itemsPerGroup[storeCode],
        group_items: items,
      },
    })
  }

  const updateItemByPackageId = (
    packageId,
    storeCode,
    property,
    value,
    itemsPerGroup,
  ) => {
    const items = [...itemsPerGroup[storeCode]?.group_items]

    for (let i = 0; i < items.length; i++) {
      if (items[i].package && items[i].package.package_id === packageId) {
        // ? new value or the opposite value
        items[i][property] = value

        if (property === 'qty_ordered') {
          items[i].package.qty_ordered = value
        }
        items[i].package_items.forEach((item) => {
          // ? new value or the opposite value
          item[property] = value
          if (property === 'qty_ordered') {
            item.package.qty_ordered = value * item.minimum_order
          }
        })
        break
      }
    }
    setItemsPerGroupMemo({
      ...itemsPerGroup,
      [storeCode]: {
        ...itemsPerGroup[storeCode],
        group_items: items,
      },
    })
  }

  const updateItemsByStoreCode = (
    storeCode,
    property,
    value,
    itemsPerGroup,
  ) => {
    const items = [...itemsPerGroup[storeCode]?.group_items]

    items.forEach((item) => {
      item[property] = value || !item[property]
      if (item.package && item.package?.package_id) {
        item.package_items.forEach((item) => {
          // ? new value or the opposite value
          item[property] = value
        })
      }
    })

    setItemsPerGroupMemo({
      ...itemsPerGroup,
      [storeCode]: {
        ...itemsPerGroup[storeCode],
        group_items: items,
      },
    })
  }

  // ? DON'T FORGET ON SUCCfESS IN MUTATE
  const { mutate: paymentPreprocessing, isLoading: isLoadingPreprocessing } =
    usePaymentPreprocessing()

  // ? deletedSkus = [{storeCode: "", id: 0}]
  const deleteItemsByIds = (deletedSkus = []) => {
    let itemsPerGroup = {
      ...itemsPerGroupMemo,
    }

    deletedSkus.forEach((el) => {
      if (!itemsPerGroup[el.storeCode]) {
        return
      }

      const items = [...itemsPerGroup[el.storeCode].group_items]

      const filteredItems = items.filter((item) => item.id !== el.id)
      if (filteredItems.length) {
        itemsPerGroup = {
          ...itemsPerGroup,
          [el.storeCode]: {
            ...itemsPerGroup[el.storeCode],
            group_items: filteredItems,
          },
        }
      } else {
        delete itemsPerGroup[el.storeCode]
      }
    })
    setItemsPerGroupMemo(itemsPerGroup)
  }

  const updateViewedSummary = (
    total = 0,
    subTotal = 0,
    totalItemChecked = 0,
    totalItem = 0,
    discount = 0,
  ) => {
    setSummary({
      total: total,
      subTotal: subTotal,
      totalItemChecked: totalItemChecked,
      totalItem: totalItem,
      discount: discount,
    })

    if (config.environment === 'desktop') {
      // ? Handle change qty in desktop for now
      handleChangeState('totalCartQty', totalItem)
    }
  }

  const setSnackbar = (type = '', message = '') => {
    // ? condition if type is error-general, but message is empty
    if (type === 'error-general' && !message) {
      setinfobarType('error-default')
    } else {
      setinfobarType(type)
      setErrMsg(message)
    }
  }

  const value = {
    // ? list of all minicart items, TYPE array of object
    allItems,

    // ? list of all minicart items, TYPE object with storename as property
    itemsPerGroup: itemsPerGroupMemo,
    setItemsPerGroup: setItemsPerGroupMemo,

    isLoadingGetCart:
      cartAuthContext?.isFetchingGetCart ||
      cartAuthContext?.isLoadingGetCart ||
      isLoadingAddMinicartItem,
    isLoadingDelete,
    // ? is loading state for summary total price
    isLoadingSummary:
      cartAuthContext?.isFetchingGetCart ||
      cartAuthContext?.isLoadingGetCart ||
      isLoadingAddMinicartItem ||
      isLoadingDelete ||
      isLoadingUpdateQty ||
      isLoadingUpdateCheck ||
      isLoadingPreprocessing,
    isSuccessGetCart: cartAuthContext?.isSuccessGetCart,
    isFetchedGetCart: cartAuthContext?.isFetchedGetCart,

    // ? price summary, total dan discount
    summary,
    updateViewedSummary,
    refetchMinicartItems,

    // ? flag is true if ANY/ONE items is checked
    isAllChecked,
    setIsAllChecked,

    // ? flag is true if ALL items is checked
    isAnyChecked,
    setIsAnyChecked,

    addMinicartItem,
    updateItemBySku,
    updateItemByPackageId,
    updateItemsByStoreCode,
    updateQty,
    deleteItemsByIds,

    mutateUpdateCheck,
    mutateDeleteItems,

    wishlists,
    wishlistItemsSku,
    refetchWishlist,

    // ? function to display snackbar
    setSnackbar,
    infobarType,
    errMsg,

    // ? list of all sku in minicart for vue ai
    vueProductIds,

    paymentPreprocessing,
    isLoadingPaymentPrecessing: isLoadingPreprocessing,

    // ? mutate and loading multiple wishlist
    addWishlistItems,
    isLoadingWishlists,
    ...cartContext,

    // revamp wishlsit props
    isOpenWishlistInfobox,
    setIsOpenWishlistInfobox,
  }

  return (
    <MinicartContext.Provider value={value}>
      {children}
    </MinicartContext.Provider>
  )
}

export function useMinicartContext() {
  return useContext(MinicartContext)
}
