import { createContext, useContext, useState } from 'react'

export const SavedPaymentMethodContext = createContext({})

const useSavedPaymentMethodContext = () => useContext(SavedPaymentMethodContext)

export const SavedPaymentMethodProvider = (props) => {
  const { children } = props

  const [section, setSection] = useState('')
  const [gopay, setGopay] = useState({
    isFetching: false,
    data: null,
  })

  const value = { section, setSection, gopay, setGopay }

  return (
    <SavedPaymentMethodContext.Provider value={value}>
      {children}
    </SavedPaymentMethodContext.Provider>
  )
}

export { useSavedPaymentMethodContext }
