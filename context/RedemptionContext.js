import React, { createContext, useContext, useState, useEffect } from 'react'
import isArray from 'lodash/isArray'
import { voucherCatalogMutate } from '../container/redemption/redemptionUtils'
import { useGetAvailableVouchers } from '../services/api/queries/global'
import {
  useGetProductsRedeemByCoin,
  useGetRedemptionApproval,
  useGetRedemptionApprovalInbox,
} from '../services/api/queries/coin'
import config from '../../../config'

export const RedemptionContext = createContext({})
export const RedemptionUpdateContext = createContext({})

const itemPerPage = config.environment === 'desktop' ? 15 : 10

function useRedemptionContext() {
  return useContext(RedemptionContext)
}

function useRedemptionUpdateContext() {
  return useContext(RedemptionUpdateContext)
}

export const RedemptionProvider = ({ children, ...props }) => {
  const [redemptionContext, setRedemptionContext] = useState({
    availableVouchers: [],
    memberTier: '',
    currentVoucherCatalogLength: 0,
    pendingRedemptionApproval: { data: {}, toggleModal: false },
    hasApprovedRedemptionApproval: { data: {}, toggleModal: false },
    hasRejectedRedemptionApproval: { data: {}, toggleModal: false },
    hasExpiredRedemptionApproval: { data: {}, toggleModal: false },
    toggleModalSuccessRedeem: { toggle: false, from: '' }, // for now for mobile web
    modalSuccessRedeemData: {},
    pendingRedemptionApprovalInboxId: '',
    // Helpers
    hasMoreVoucherCatalog: false,
    nextVoucherCatalogOffset: '',
    queryFilterVoucherCatalog: false,
    allowRedeemVoucher: true,
    err: '',
    inboxRedeemStatus: null,
  })

  const [productsRedeembyCoin, setProductsRedeemByCoin] = useState([])
  const [offset, setOffset] = useState(0)
  const [totalItem, setTotalItem] = useState(0)
  const [enabledProductsRedeembyCoin, setEnabledProductsRedeembyCoin] =
    useState(false)

  const { headerType, setHeaderType } = props
  const {
    isLoading: availableVouchersLoading,
    refetch: refetchAvailableVouchers,
  } = useGetAvailableVouchers(
    {
      nextGet: redemptionContext.nextVoucherCatalogOffset,
    },
    {
      enabled:
        redemptionContext.queryFilterVoucherCatalog &&
        headerType === 'redemption',
      onSuccess: (data) => {
        const mutationData = voucherCatalogMutate(
          redemptionContext.availableVouchers,
          data?.data,
        )
        setRedemptionContext((current) => ({
          ...current,
          availableVouchers: mutationData.data,
          memberTier: data?.data?.customer?.membership_status || '',
          currentVoucherCatalogLength: mutationData.voucherCatalogLength,
          nextVoucherCatalogOffset: data?.data?.next_rewards_id || '',
          hasMoreVoucherCatalog: data?.data?.has_next || false,
          queryFilterVoucherCatalog: false,
          allowRedeemVoucher:
            data?.data?.customer?.allow_coin_redemption ?? true,
        }))
      },
      onError: (error) => {
        setRedemptionContext((current) => ({
          ...current,
          queryFilterVoucherCatalog: false,
          err: error.message,
        }))
      },
    },
  )
  const {
    data: _redemptionApprovalResp,
    isLoading: redemptionApprovalLoading,
    isError: redemptionApprovalIsError,
    refetch: refetchPendingApproval,
  } = useGetRedemptionApproval(null, {
    enabled: false,
    onError: (error) =>
      setRedemptionContext((current) => ({ ...current, err: error.message })),
  })
  const {
    data: _redemptionApprovalInboxResp,
    isLoading: redemptionApprovalInboxLoading,
    isError: redemptionApprovalInboxIsError,
    refetch: refetchRedemptionInbox,
  } = useGetRedemptionApprovalInbox(
    {
      requestId: redemptionContext.pendingRedemptionApprovalInboxId,
    },
    {
      enabled: false,
      // still don't know why this react query needs to use cacheTime although already use refetch
      cacheTime: 1 * 1000,
    },
  )

  const {
    isFetching: isLoadingProductsRedeembyCoin,
    refetch: refetchProductsRedeembyCoin,
  } = useGetProductsRedeemByCoin(
    {
      limit: itemPerPage,
      offset,
    },
    {
      enabled: enabledProductsRedeembyCoin,
      onSuccess: (response) => {
        if (isArray(response.products) && response.products.length) {
          if (productsRedeembyCoin.length) {
            setProductsRedeemByCoin([
              ...productsRedeembyCoin,
              ...response.products,
            ])
          } else {
            setTotalItem(+response.total)
            setProductsRedeemByCoin(response.products)
            setEnabledProductsRedeembyCoin(true)
          }
        }
      },
      onError: (err) => {
        console.log('ERR: Redemption Failed ',err)
      },
    },
  )

  useEffect(() => {
    // fetch voucher catalog for the first time
    if (headerType === 'redemption') {
      triggerFilterVoucherCatalog(true)
    }
  }, [])

  useEffect(() => {
    if (redemptionContext.queryFilterVoucherCatalog) {
      refetchAvailableVouchers()
    }
  }, [redemptionContext.queryFilterVoucherCatalog])

  useEffect(() => {
    if (
      _redemptionApprovalResp &&
      !redemptionApprovalLoading &&
      !redemptionApprovalIsError
    ) {
      setRedemptionContext((current) => ({
        ...current,
        err: '',
        pendingRedemptionApproval: {
          data: _redemptionApprovalResp,
          toggleModal: current.pendingRedemptionApproval.toggleModal,
        },
      }))
    }
  }, [_redemptionApprovalResp])

  useEffect(() => {
    if (
      _redemptionApprovalInboxResp &&
      !redemptionApprovalInboxLoading &&
      !redemptionApprovalInboxIsError
    ) {
      const inboxRedeemStatus = _redemptionApprovalInboxResp?.status
      if (inboxRedeemStatus === 0) {
        setRedemptionContext((current) => ({
          ...current,
          pendingRedemptionApproval: {
            data: _redemptionApprovalInboxResp,
            toggleModal: current.pendingRedemptionApproval.toggleModal,
          },
          pendingRedemptionApprovalInboxId: '',
          inboxRedeemStatus: inboxRedeemStatus,
        }))
      } else if (inboxRedeemStatus === 1) {
        setRedemptionContext((current) => ({
          ...current,
          hasApprovedRedemptionApproval: {
            data: _redemptionApprovalInboxResp,
            toggleModal: current.hasApprovedRedemptionApproval.toggleModal,
          },
          pendingRedemptionApprovalInboxId: '',
          inboxRedeemStatus: inboxRedeemStatus,
        }))
      } else if (inboxRedeemStatus === 2) {
        setRedemptionContext((current) => ({
          ...current,
          hasRejectedRedemptionApproval: {
            data: _redemptionApprovalInboxResp,
            toggleModal: current.hasRejectedRedemptionApproval.toggleModal,
          },
          pendingRedemptionApprovalInboxId: '',
          inboxRedeemStatus: inboxRedeemStatus,
        }))
      } else if (inboxRedeemStatus === 5) {
        setRedemptionContext((current) => ({
          ...current,
          hasExpiredRedemptionApproval: {
            data: _redemptionApprovalInboxResp,
            toggleModal: current.hasExpiredRedemptionApproval.toggleModal,
          },
          pendingRedemptionApprovalInboxId: '',
          inboxRedeemStatus: inboxRedeemStatus,
        }))
      }
    }
  }, [_redemptionApprovalInboxResp])

  useEffect(() => {
    if (redemptionContext.pendingRedemptionApprovalInboxId !== '') {
      refetchRedemptionInbox()
    }
  }, [redemptionContext.pendingRedemptionApprovalInboxId])

  function triggerFilterVoucherCatalog(flag) {
    setRedemptionContext((current) => ({
      ...current,
      availableVouchers: [],
      currentVoucherCatalogLength: 0,
      nextVoucherCatalogOffset: '',
      hasMoreVoucherCatalog: false,
      queryFilterVoucherCatalog: flag,
    }))
  }

  function fetchMoreVoucherCatalogData() {
    if (redemptionContext.hasMoreVoucherCatalog) {
      setRedemptionContext((current) => ({
        ...current,
        queryFilterVoucherCatalog: true,
      }))
    }
  }

  function getLatestApproval() {
    setRedemptionContext((current) => ({ ...current, err: '' }))
    refetchPendingApproval()
  }

  function toggleModalApprovalRedemption(type, toggle) {
    if (type === 'pending') {
      setRedemptionContext((current) => ({
        ...current,
        pendingRedemptionApproval: {
          data: current.pendingRedemptionApproval.data,
          toggleModal: toggle,
        },
      }))
    }
  }

  function setSuccessRedeemData(data) {
    setRedemptionContext((current) => ({
      ...current,
      modalSuccessRedeemData: data,
    }))
  }

  function toggleModalSuccessRedeem(toggle, from) {
    // for now for mobile web
    setRedemptionContext((current) => ({
      ...current,
      toggleModalSuccessRedeem: { toggle: toggle, from: from },
    }))
  }

  function destroyApprovalRedemption(type) {
    // make the object pendingRedemptionApproval inside redemptionContext state empty after customer has given their response or they closed the modal approval
    if (type === 'pending') {
      setRedemptionContext((current) => ({
        ...current,
        pendingRedemptionApprovalInboxId: '',
        inboxRedeemStatus: null,
        pendingRedemptionApproval: {
          data: {},
          toggleModal: current.pendingRedemptionApproval.toggleModal,
        },
      }))
    } else {
      setRedemptionContext((current) => ({
        ...current,
        inboxRedeemStatus: null,
        err: '',
      }))
    }
  }

  function getPendingRedemptionDataInbox(id) {
    setRedemptionContext((current) => ({
      ...current,
      pendingRedemptionApprovalInboxId: id,
    }))
  }

  const handleNextPage = () => {
    setOffset(offset + itemPerPage)
  }

  const firstFetchProductRedeemByCoin = () => {
    setOffset(0)
    setProductsRedeemByCoin([])
    refetchProductsRedeembyCoin()
  }

  const resetProductRedeemByCoin = () => {
    setOffset(0)
    setProductsRedeemByCoin([])
  }

  return (
    <RedemptionContext.Provider
      value={{
        ...redemptionContext,
        availableVouchersLoading,
        productsRedeembyCoin,
        isLoadingProductsRedeembyCoin,
        totalItem,
      }}
    >
      <RedemptionUpdateContext.Provider
        value={{
          setHeaderType,
          refetchAvailableVouchers,
          triggerFilterVoucherCatalog,
          fetchMoreVoucherCatalogData,
          getLatestApproval,
          refetchRedemptionInbox,
          toggleModalApprovalRedemption,
          setSuccessRedeemData,
          toggleModalSuccessRedeem,
          destroyApprovalRedemption,
          getPendingRedemptionDataInbox,
          firstFetchProductRedeemByCoin,
          handleNextPage,
          resetProductRedeemByCoin,
        }}
      >
        {children}
      </RedemptionUpdateContext.Provider>
    </RedemptionContext.Provider>
  )
}

export { useRedemptionContext, useRedemptionUpdateContext }
