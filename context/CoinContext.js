import React, { createContext, useContext, useState, useEffect } from 'react'
import { coinHistoryMutate } from '../container/coin/coinUtils'
// import { useGetAllMerchant } from '../services/api/queries/global' // COMMENT FOR NOW BECAUSE FILTER IS NOT PROVIDED FROM STAMPS
import {
  useGetCoinsCredit,
  useGetCoinsExpired,
  useGetCoinsHistory,
} from '../services/api/queries/coin'

export const CoinContext = createContext({})
export const CoinUpdateContext = createContext({})

function useCoinContext() {
  return useContext(CoinContext)
}

function useCoinUpdateContext() {
  return useContext(CoinUpdateContext)
}

export const CoinProvider = ({ children, ...props }) => {
  const [coinContext, setCoinContext] = useState({
    balance: {},
    expired: {},
    filters: {
      transactionPeriod: { filter: '', value: '' },
      // Comment for now because filter by specific date not provided by vendor: stamps
      // startDate: '',
      // endDate: '',
      coinActivity: {
        filter: 'Semua Aktivitas Koin',
        value: 'coin_all_activity',
      },
      shoppingMethod: { filter: 'Semua Metode', value: 'all_metode' },
    },

    coinsHistoryResp: [],
    currentHistoryLength: 0,
    totalAllHistory: 0,

    brandsListMap: [],
    brandsFilterSelected: [],
    // Helpers
    hasMoreHistory: false,
    nextHistoryOffset: '',
    queryFilterCoinsHistory: false,
    brandsSearch: '',
    err: '',
  })
  const { headerType, setHeaderType } = props
  const {
    data: _coinsCreditResp,
    isLoading: coinsCreditLoading,
    isError: coinsCreditIsError,
    refetch: refetchCoinsCredit,
  } = useGetCoinsCredit()
  const {
    data: _coinsExpiredResp,
    isLoading: coinsExpiredLoading,
    isError: coinsExpiredIsError,
  } = useGetCoinsExpired()
  const { isLoading: coinsHistoryLoading, refetch: refetchCoinHistory } =
    useGetCoinsHistory(
      {
        monthGrouping: true,
        nextGet: coinContext.nextHistoryOffset,
      },
      {
        enabled: coinContext.queryFilterCoinsHistory && headerType === 'coin',
        onSuccess: (data) => {
          const mutationData = coinHistoryMutate(
            coinContext.coinsHistoryResp,
            data.data,
          )
          setCoinContext((current) => ({
            ...current,
            coinsHistoryResp: mutationData.data,
            currentHistoryLength: mutationData.historyLength,
            nextHistoryOffset: data.nextGet,
            hasMoreHistory: data.hasMore,
            queryFilterCoinsHistory: false,
          }))
        },
        onError: (error) => {
          setCoinContext((current) => ({
            ...current,
            queryFilterCoinsHistory: false,
            err: error.message,
          }))
        },
      },
    )

  useEffect(() => {
    // fetch coin history for the first time
    if (headerType === 'coin') {
      triggerFilterCoinHistory(true)
    }
  }, [])

  useEffect(() => {
    if (_coinsCreditResp && !coinsCreditLoading && !coinsCreditIsError) {
      setCoinContext((current) => ({ ...current, balance: _coinsCreditResp }))
    }
    if (_coinsExpiredResp && !coinsExpiredLoading && !coinsExpiredIsError) {
      setCoinContext((current) => ({ ...current, expired: _coinsExpiredResp }))
    }
  }, [_coinsCreditResp, _coinsExpiredResp])

  useEffect(() => {
    if (coinContext.queryFilterCoinsHistory) {
      refetchCoinHistory()
    }
  }, [coinContext.queryFilterCoinsHistory])

  function fetchMoreCoinHistoryData() {
    if (coinContext.hasMoreHistory) {
      setCoinContext((current) => ({
        ...current,
        queryFilterCoinsHistory: true,
      }))
    }
  }

  function handleForm(event, formType) {
    if (event) {
      const { name, value } = event.target
      switch (formType) {
        case 'brandsSearch':
          setCoinContext((current) => ({ ...current, [name]: value }))
          break
      }
    }
  }

  function updateFilters(
    filterCategory,
    filterTitle,
    filterValue,
    reset = false,
  ) {
    if (!reset) {
      setCoinContext((current) => ({
        ...current,
        filters: {
          ...current.filters,
          [filterCategory]: { filter: filterTitle, value: filterValue },
        },
      }))
    } else {
      setCoinContext((current) => ({
        ...current,
        filters: {
          transactionPeriod: { filter: '', value: '' },
          coinActivity: {
            filter: 'Semua Aktivitas Koin',
            value: 'coin_all_activity',
          },
          shoppingMethod: { filter: 'Semua Metode', value: 'all_metode' },
        },
      }))
    }
  }

  function updateBrandsFilters(data, newDataToBeMap, isFromStockpiles = false) {
    // newDataToBeMap means parameter to update the current render of original brands selections
    // example: ['1', '2', '3'] to ['2', '1', '3']
    // in summary, we need to put the option that we just selected in the top of the current render of original brands selections

    // isFromStockPiles means whether the user delete brands filter directly from stockpiles filter
    // if (!isFromStockpiles)
    setCoinContext((current) => ({
      ...current,
      brandsFilterSelected: data,
      brandsListMap: newDataToBeMap,
      queryFilterCoinsHistory: isFromStockpiles,
    }))
  }

  function resetBrandsFilters() {
    const brandsListMapTemp = [...coinContext.brandsListMap]
    brandsListMapTemp.sort(function (a, b) {
      return a.merchant_name.localeCompare(b.merchant_name)
    })
    setCoinContext((current) => ({
      ...current,
      brandsFilterSelected: [],
      brandsListMap: brandsListMapTemp,
    }))
  }

  function searchingBrands(search, reset = false) {
    // for searching brands
    // reset = false means we empty the string value of brandsSearch property
    // reset = true means we replace the value of brandsListMap property original map data
    if (!reset) {
      const brandsListTemp = [...coinContext.brandsListMap]
      const brandsFilterSearch = brandsListTemp.filter((brand) =>
        brand.merchant_name.toLowerCase().includes(search.toLowerCase()),
      )
      brandsFilterSearch.sort(function (a, b) {
        return a.merchant_name.localeCompare(b.merchant_name)
      })
      setCoinContext((current) => ({
        ...current,
        brandsListMap: brandsFilterSearch,
      }))
    } else {
      setCoinContext((current) => ({ ...current, brandsSearch: '' }))
    }
  }

  function triggerFilterCoinHistory(flag) {
    setCoinContext((current) => ({
      ...current,
      coinsHistoryResp: [],
      currentHistoryLength: 0,
      nextHistoryOffset: '',
      hasMoreHistory: false,
      queryFilterCoinsHistory: flag,
    }))
  }

  return (
    <CoinContext.Provider
      value={{
        ...coinContext,
        coinHistoryLoading: coinsHistoryLoading,
      }}
    >
      <CoinUpdateContext.Provider
        value={{
          setHeaderType,
          fetchMoreCoinHistoryData,
          handleForm,
          refetchCoinsCredit,
          updateFilters,
          updateBrandsFilters,
          resetBrandsFilters,
          searchingBrands,
          triggerFilterCoinHistory,
        }}
      >
        {children}
      </CoinUpdateContext.Provider>
    </CoinContext.Provider>
  )
}

export { useCoinContext, useCoinUpdateContext }
