import React, { createContext, useContext, useState } from 'react'

export const OtpContext = createContext({})
export const OtpUpdateContext = createContext({})

function useOtpContext() {
  return useContext(OtpContext)
}

function useOtpUpdateContext() {
  return useContext(OtpUpdateContext)
}

export const OtpProvider = ({ children, ...props }) => {
  const [otpContext] = useState({
    OtpStyles: props.OtpStyles,
  })

  return (
    <OtpContext.Provider value={{ ...otpContext }}>
      <OtpUpdateContext.Provider value={null}>
        {children}
      </OtpUpdateContext.Provider>
    </OtpContext.Provider>
  )
}
export { useOtpContext, useOtpUpdateContext }
