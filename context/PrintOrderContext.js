import React, { createContext, useContext, useEffect, useState } from 'react'

export const PrintOrderContext = createContext({})

export const PrintOrderProvider = ({ children, results, styles }) => {
  const [printOrderContext, setPrintOrderContext] = useState({
    results,
    styles,
  })

  useEffect(() => {
    setPrintOrderContext({ results, styles })
  }, [results, styles])

  return (
    <PrintOrderContext.Provider value={printOrderContext}>
      {children}
    </PrintOrderContext.Provider>
  )
}

export function usePrintOrderContext() {
  return useContext(PrintOrderContext)
}
