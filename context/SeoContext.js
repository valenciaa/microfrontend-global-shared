import { createContext, useContext, useState } from 'react'

export const SeoContext = createContext({})

export const SeoProvider = ({
  seoData = {
    products: [],
    categories: [],
    algoliaProducts: [],
    metaData: {},
    sosmedData: {},
    seoAutoLinkData: [],
    urlPathSeoData: '',
    tahuFooterData: {},
  },
  children,
}) => {
  const {
    products,
    categories,
    algoliaProducts,
    metaData,
    sosmedData,
    seoAutoLink,
    urlPath,
    tahuFooter,
    is404 = false,
  } = seoData
  const [productsSeo, setProductsSeo] = useState(
    products?.map((el) => {
      return {
        name: el.name,
        url_key: el.url_key,
        review: el.review,
        variants: el.variants,
      }
    }),
  )
  const [categoriesSeo] = useState(categories)
  const [autoLinkSeo] = useState(seoAutoLink)
  const [urlSeo] = useState(urlPath)
  const [tahuFooterSeo] = useState(tahuFooter)
  const [seoFooter, setSeoFooter] = useState('')
  const [algoliaProductsSeo] = useState(
    algoliaProducts?.initialResults?.[
      Object.keys(algoliaProducts.initialResults).find((key) =>
        /productsvariant/.test(key),
      )
    ]?.results?.map((elResults) => {
      const results = []
      if (elResults?.hits) {
        for (let i = 0; i < elResults?.hits?.length; i++) {
          results.push({
            name: elResults?.hits[i].name,
            url_key: elResults?.hits[i].url_key,
            review: elResults?.hits[i].review,
            default_price: elResults?.hits[i].default_price || 0,
            special_price: elResults?.hits[i].special_price || 0,
            images: elResults?.hits[i].images || 0,
          })
        }
      }
      return results
    }),
  )
  const [metaDataSeo, setMetaDataSeo] = useState(metaData)
  const [sosmedDataSeo] = useState(sosmedData)

  return (
    <SeoContext.Provider
      value={{
        productsSeo,
        setSeoFooter,
        seoFooter,
        setProductsSeo,
        categoriesSeo,
        algoliaProductsSeo,
        metaDataSeo,
        sosmedDataSeo,
        autoLinkSeo,
        is404,
        setMetaDataSeo,
        urlSeo,
        tahuFooterSeo,
      }}
    >
      {children}
    </SeoContext.Provider>
  )
}

export function useSeoContext() {
  return useContext(SeoContext)
}
