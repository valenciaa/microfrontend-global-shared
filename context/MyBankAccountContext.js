import { createContext, useContext, useState } from 'react'
import useMemoState from '../utils/hooks/useMemoState'

export const MyBankAccountContext = createContext({})

export const MyBankAccountProvider = ({ children }) => {
  const [state, setState] = useState({
    bankAccountOwnerName: '',
    bankAccountNumber: '',
    partnerReferenceNo: '',
    selectedBank: {},
  })

  const data = useMemoState(state, setState)

  return (
    <MyBankAccountContext.Provider value={data}>
      {children}
    </MyBankAccountContext.Provider>
  )
}

export function useMyBankAccountContext() {
  return useContext(MyBankAccountContext)
}
