import React, { createContext, useContext, useEffect, useState } from 'react'
import {
  useGetCategories,
  useGetCustomerMembership,
  useGetHowToUseVoucher,
  useGetOfficialPartner,
  useGetPartner,
  useGetUserBankAccount,
  useGetVoucher,
} from '../services/api/queries/global'
import { globalAPI } from '../services/IndexApi'
import config from '../../../config'
import { GetAuthData } from '../../../src/utils/misc/GetAuthData'
import { useRouter } from 'next/router'
import { saveCurrentPath } from '../utils/PrevPathSession'

export const MyVoucherContext = createContext({})
export const MyVoucherDetailContext = createContext({})
export const MyVoucherFilterContext = createContext({})
export const MyVoucherPaginationContext = createContext({})
export const MyVoucherRefundContext = createContext({})
export const MyVoucherSortContext = createContext({})
export const MyVoucherTabContext = createContext({})

const useMyVoucherContext = () => useContext(MyVoucherContext)
const useMyVoucherDetailContext = () => useContext(MyVoucherDetailContext)
const useMyVoucherFilterContext = () => useContext(MyVoucherFilterContext)
const useMyVoucherPaginationContext = () =>
  useContext(MyVoucherPaginationContext)
const useMyVoucherRefundContext = () => useContext(MyVoucherRefundContext)
const useMyVoucherSortContext = () => useContext(MyVoucherSortContext)
const useMyVoucherTabContext = () => useContext(MyVoucherTabContext)

export const MyVoucherProvider = (props) => {
  const { children, voucherWalletStyles } = props
  const [categories, setCategories] = useState({})
  const [officialPartners, setOfficialPartners] = useState({})
  const [partners, setPartners] = useState({})
  const [vouchers, setVouchers] = useState({})
  const [showRefund, setShowRefund] = useState(false)
  const [showAccount, setShowAccount] = useState(false)
  const [showRequestRefund, setShowRequestRefund] = useState(false)
  const [showManageAccount, setShowManageAccount] = useState(false)
  const [showDeleteAccount, setShowDeleteAccount] = useState(false)
  const [showSetMainAccout, setShowMainAccount] = useState(false)
  const [showFailedDeleteAccount, setShowFailedDeleteAccount] = useState(false)
  const [userAccounts, setUserAccounts] = useState({})
  const [dataVoucherRefund, setDataVoucherRefund] = useState({})
  const [voucherDetail, setVoucherDetail] = useState({})
  const [voucherDetailCode, setVoucherDetailCode] = useState('')
  const [pagePosition, setPagePosition] = useState(1)
  const [tabActive, setActiveTab] = useState('all')
  const [accountMain, setAccountMain] = useState({})
  const [manageAccountData, setManageAccountData] = useState({})
  const [howToUseVoucher, setHowToUseVoucher] = useState('')
  const [refundStatus, setRefundStatus] = useState({
    error: false,
    message: '',
  })
  const [sortVoucherBy, setSortVoucherBy] = useState({
    label: 'Segera Berakhir',
    value: 'expire_soon',
  })
  const [filterVoucherBy, setFilterVoucherBy] = useState([
    { key: 'online_status', checked: [] },
    { key: 'official_partner', checked: [] },
    { key: 'promo_type', checked: [] },
    { key: 'payment_method', checked: [] },
    { key: 'partner', checked: [] },
  ])
  const [isOpenAddBankAccountModal, setIsOpenAddBankAccountModal] =
    useState(false)
  const [isOpenOtpModal, setIsOpenOtpModal] = useState(false)
  const [isOpenBankAccountInfobox, setIsOpenBankAccountInfobox] =
    useState(false)
  const [bankAccountActionMessage, setBankAccountActionMessage] = useState('')
  const onlineStatus = voucherDetail?.data?.voucher_detail?.online_status
  const router = useRouter()

  const { data: summarizeMembership, isLoading: summarizeMembershipLoading } =
    useGetCustomerMembership()

  // Handle parameter for get vouchers
  // Handle limir per tab voucher
  const handleLimit = () => {
    if (tabActive === 'promo') {
      return 20
    }
    if (tabActive === 'partner') {
      return 15
    }
    if (tabActive === 'spesial') {
      return 5
    }
    return 0
  }

  const paramVouchers = {
    category: tabActive,
    online_status: filterVoucherBy
      .filter((i) => i.key === 'online_status')[0]
      .checked.map((i) => i.value)
      .join(','),
    official_partner: filterVoucherBy
      .filter((i) => i.key === 'official_partner')[0]
      .checked.map((i) => i.value)
      .join(','),
    promo_type: filterVoucherBy
      .filter((i) => i.key === 'promo_type')[0]
      .checked.map((i) => i.value)
      .join(','),
    payment_method: filterVoucherBy
      .filter((i) => i.key === 'payment_method')[0]
      .checked.map((i) => i.value)
      .join(','),
    partner: filterVoucherBy
      .filter((i) => i.key === 'partner')[0]
      .checked.map((i) => i.value)
      .join(','),
    sort_by: sortVoucherBy.value,
    page: pagePosition,
    limit: handleLimit(),
  }

  const handleTitle = () => {
    if (onlineStatus === '1') {
      return 'cara-pakai-voucher-wallet-offline'
    }
    if (onlineStatus === '2') {
      return 'cara-pakai-voucher-wallet-online'
    }
    if (onlineStatus === '3') {
      return 'cara-pakai-voucher-wallet'
    }
    return ''
  }

  const wordingParams = {
    companyCode: 'All',
    title: handleTitle(),
  }

  // Handle open modal refund
  const modalRefundOpen = (data) => {
    if (data) {
      setDataVoucherRefund(data)
    }
    setShowRefund(true)
  }

  // Handle close modal refund
  const modalRefundClose = () => {
    setRefundStatus({
      error: false,
      message: '',
    })
    setShowRefund(false)
  }

  // Handle click continue at modal refund
  // And modal request refund open, modal refund close
  const modalRefundContinue = async (password, email) => {
    const auth = await GetAuthData()

    const payload = {
      accountId: accountMain?.customer_rekening_id,
      accountName: accountMain?.account_name,
      accountNumber: accountMain?.account_number,
      password,
      phone: accountMain?.phone,
      termsFlag: true,
      voucherCode: dataVoucherRefund?.voucher_code,
    }

    const dcPayload = {
      action: 'cairkan_voucher',
      voucher_code: dataVoucherRefund?.voucher_code,
      email: email || auth?.email,
      password: password,
      account_name: accountMain?.account_name,
      account_number: accountMain?.account_number,
      customer_rekening_id: accountMain?.customer_rekening_id,
    }

    const response = dataVoucherRefund?.voucher_code?.includes('VRCDC')
      ? await globalAPI.postVoucherRefundDC(dcPayload)
      : await globalAPI.postVoucherRefund(payload)

    if (response.status === 200 && !response.data.error) {
      setRefundStatus({
        error: false,
        message: '',
      })
      setShowRequestRefund(true)
      setShowRefund(false)
    }

    if (
      (response.status === 200 && response.data.error) ||
      (response.status === 500 && response.data.error)
    ) {
      const errorMsg = () => {
        const msg = response?.data?.message
        if (
          msg ===
            'Error: Gagal validasi password/passkey, mohon cek ulang kembali' ||
          msg === 'wrong password'
        ) {
          return 'Password yang anda masukkan salah'
        }

        if (msg === 'Error: [1.0.1] Voucher Code tidak Valid') {
          return 'Kode voucher tidak valid'
        }

        return msg
      }

      setRefundStatus({
        error: true,
        message: errorMsg(),
      })
    }
  }

  // Handle update user account
  const handleClickUpdateMainAccount = async () => {
    const { customer_rekening_id } = manageAccountData
    const payload = {
      customer_rekening_id,
      is_active: true,
      is_default: true,
    }

    const response = await globalAPI.setUserBankAccount(payload)

    if (response.status === 200 && !response.data.error) {
      setShowMainAccount(false)
      const responseUpdate = await globalAPI.getUserBankAccont()

      if (responseUpdate.status === 200 && !responseUpdate.data.error) {
        const mainAccount = responseUpdate.data.data.filter(
          (i) => i.is_default === 10,
        )[0]
        setAccountMain({ ...mainAccount })
        setUserAccounts(responseUpdate.data)
        setShowManageAccount(false)
        setShowRefund(true)
        getUserBankAccount.refetch()
      }
    }
  }

  // Handle delete user account
  const handleClickDeleteUserAccount = async () => {
    const { customer_rekening_id } = manageAccountData
    const payload = {
      customer_rekening_id,
      is_active: false,
      is_default: false,
    }

    const response = await globalAPI.setUserBankAccount(payload)
    if (response.status === 200 && !response.data.error) {
      const responseUpdate = await globalAPI.getUserBankAccont()

      if (responseUpdate?.data?.message === 'success') {
        const mainAccount = responseUpdate.data.data.filter(
          (i) => i.is_default === 10,
        )[0]
        setAccountMain({ ...mainAccount })
        setUserAccounts(responseUpdate.data)
        setShowDeleteAccount(false)
        setShowRefund(true)
        getUserBankAccount.refetch()
      }
    } else {
      setShowDeleteAccount(false)
      setShowFailedDeleteAccount(true)
    }
  }

  // Handle close modal show failed delete account
  const modalFailedDeleteAccountClose = () => {
    setShowFailedDeleteAccount(false)
    setShowManageAccount(true)
  }

  // Handle close modal request refund
  const modalRequestRefundClose = async () => {
    const response = await globalAPI.getVouchers(paramVouchers)
    if (response.status === 200 && !response.data.error) {
      setVouchers(response?.data)
      setShowRequestRefund(false)
      window?.location?.reload()
    }
  }

  // Handle open modal select account
  // And modal refund close
  const modalSelectAccountOpen = () => {
    setShowRefund(false)
    setShowAccount(true)
  }

  // Handle close modal select account
  // And modal refund show
  const modalSelectAccountClose = () => {
    setShowAccount(false)
    setShowRefund(true)
  }

  // Handle click add account
  const modalAddAccountShow = () => {
    // const url = 'my-account?tab=my-bank-accounts&limit=10&offset=0'
    // window.location.href = config.legacyBaseURL + url
    setIsOpenAddBankAccountModal(true)
    setShowAccount(false)
  }

  const modalAddAccountShowMobile = (voucher) => {
    const url = '/my-bank-account/add'
    // window.location.href = config.legacyBaseURL + url
    window.sessionStorage.setItem('voucher_mine', voucher)
    saveCurrentPath()
    router.push(url)
  }

  // Handle click manage account
  const handleManageAccount = (data) => {
    setManageAccountData(data)
    setShowAccount(false)
    setShowManageAccount(true)
  }

  const handleShowConfirmationSetAccount = () => {
    setShowManageAccount(false)
    setShowMainAccount(true)
  }

  const handleCloseConfirmationSetAccount = () => {
    setShowMainAccount(false)
    setShowManageAccount(true)
  }

  const handleCloseManageAccount = () => {
    setShowManageAccount(false)
  }

  const handleCloseManageAccountDesktop = () => {
    setShowManageAccount(false)
    setShowAccount(true)
  }

  // Handle click delete account
  const handleClickDeleteAccount = () => {
    setShowManageAccount(false)
    setShowDeleteAccount(true)
  }

  const handleCloseDeleteAccount = () => {
    setShowManageAccount(true)
    setShowDeleteAccount(false)
  }

  // Handle click change main account
  const handleChangeMainAccount = (account) => setAccountMain(account)

  // Handle get value pagination
  const getValuePage = (data) => setPagePosition(data)

  // Handle get value tab
  const getValueTab = (data) => {
    setActiveTab(data.value)
    setPagePosition(1)
  }

  // Handle add bank account after submitting otp in my voucher

  const handleOpenVoucherRefundModal = (voucher) => {
    if (config.environment === 'mobile') {
      setDataVoucherRefund(voucher)
    }
    if (getUserBankAccount?.data?.length > 0) {
      setShowAccount(true)
    } else {
      setShowRefund(true)
    }
  }

  // Handle get value sort
  const getValueSort = (data) => setSortVoucherBy(data)

  // Handle clear value sort
  const clearValueSort = () => setSortVoucherBy({ label: '', value: '' })

  // Handle get voucher detail code
  const getVoucherDetailCode = (data) => setVoucherDetailCode(data)

  // Handle get value filter
  const getValueFilter = (values) => {
    const currentFilter = filterVoucherBy
    currentFilter.forEach((cur, curIdx) => {
      values.forEach((val) => {
        if (val.key === cur.key) {
          currentFilter[curIdx].checked = val.checked
        }
      })
    })
    setFilterVoucherBy([...currentFilter])
  }

  // Handle remove value filter
  const removeValueFilter = (opt) => {
    const setNewChecked = (checked) => {
      return checked.filter((i) => i.value !== opt.value)
    }

    const newArr = filterVoucherBy.map((i) => ({
      ...i,
      checked:
        i.key === opt.key && i.checked.length
          ? setNewChecked(i.checked)
          : i.checked,
    }))

    setFilterVoucherBy([...newArr])
  }

  // Handle remove all value filter
  const removeAllValueFilter = () => {
    setFilterVoucherBy([
      { key: 'online_status', checked: [] },
      { key: 'official_partner', checked: [] },
      { key: 'promo_type', checked: [] },
      { key: 'payment_method', checked: [] },
      { key: 'partner', checked: [] },
    ])
  }

  // List Filter
  const listFilter = [
    {
      name: 'online_status',
      options: [
        { label: 'Pakai Online', value: 'online' },
        { label: 'Pakai di Toko', value: 'offline' },
      ],
      title: 'Tipe Voucher',
      withSearch: false,
    },
    {
      name: 'official_partner',
      options: officialPartners.data
        ?.map((i) => ({
          label: i.official_partner_name,
          value: i.official_partner,
        }))
        .sort((a, b) => {
          if (a.label < b.label) {
            return -1
          }
          if (a.label > b.label) {
            return 1
          }
          return 0
        }),
      placeholder: 'Cari official partner',
      title: 'Official Partner',
      withSearch: true,
    },
    {
      name: 'promo_type',
      options: [
        { label: 'Cashback', value: 'cashback' },
        { label: 'Koin', value: 'koin' },
        { label: 'Ongkos Kirim', value: 'ongkos_kirim' },
        { label: 'Potongan Harga', value: 'potongan' },
      ],
      title: 'Promo',
      withSearch: false,
    },
    {
      name: 'payment_method',
      options: [
        { label: 'Akulaku', value: 'akulaku' },
        { label: 'BCA', value: 'bca' },
        { label: 'BNI', value: 'bni' },
        { label: 'BRI', value: 'bri' },
        { label: 'BSI', value: 'bsi' },
        { label: 'BTN', value: 'btn' },
        { label: 'CIMB Niaga', value: 'cimb' },
        { label: 'Citibank', value: 'citibank' },
        { label: 'DANA', value: 'dana' },
        { label: 'DanaKini', value: 'danakini' },
        { label: 'Danamon', value: 'danamon' },
        { label: 'DBS', value: 'dbs' },
        { label: 'Gopay', value: 'gopay' },
        { label: 'HSBC', value: 'hsbc' },
        { label: 'Kredivo', value: 'kredivo' },
        { label: 'Mandiri', value: 'mandiri' },
        { label: 'Mega', value: 'mega' },
        { label: 'OCBC', value: 'ocbc' },
        { label: 'OVO', value: 'ovo' },
        { label: 'Panin Bank', value: 'panin-bank' },
        { label: 'Permata', value: 'permata' },
        { label: 'QRIS', value: 'qris' },
        { label: 'ShopeePay', value: 'shoopepay' },
        { label: 'UOB', value: 'uob' },
        { label: 'VA Transfer', value: 'va-transfer' },
      ],
      placeholder: 'Cari metode',
      title: 'Metode Pembayaran',
      withSearch: true,
    },
    {
      name: 'partner',
      options: partners.data
        ?.map((i) => ({
          label: i.partner_name,
          value: i.partner,
        }))
        .sort((a, b) => {
          if (a.label < b.label) {
            return -1
          }
          if (a.label > b.label) {
            return 1
          }
          return 0
        }),
      placeholder: 'Cari partner',
      title: 'Partner',
      withSearch: true,
    },
  ]

  // List Sort
  const listSort = [
    { label: 'Segera Berakhir', value: 'expire_soon' },
    { label: 'Terbaru', value: 'newest' },
    { label: 'Terpopuler', value: 'popular' },
  ]

  // List Tab
  const listTab = [
    { label: 'Semua Voucher', value: 'all' },
    { label: 'Voucher Saya', value: 'mine' },
    { label: 'Voucher Spesial', value: 'spesial' },
    { label: 'Voucher Partner', value: 'partner' },
    { label: 'Voucher Promo', value: 'promo' },
  ]

  if (config.environment === 'mobile') {
    listTab.push({ label: 'Voucher Merchant', value: 'external' })
  }

  // Set MyVoucherContext.Provider value
  const myVoucherContextValue = {
    categories,
    officialPartners,
    tabActive,
    partners,
    vouchers,
    voucherWalletStyles,
    userDetail: {
      membership: summarizeMembership?.membership || null,
      isLoading: summarizeMembershipLoading,
    },
  }

  // Set MyVoucherDetailContext.Provider value
  const myVoucherDetailContextValue = {
    getVoucherDetailCode,
    howToUseVoucher,
    voucherDetail,
  }

  // Set MyVoucherFilterContext.Provider value
  const myVoucherFilterContextValue = {
    filterVoucherBy,
    getValueFilter,
    listFilter,
    removeAllValueFilter,
    removeValueFilter,
  }

  // Set MyVoucherPaginationContext.Provider value
  const myVoucherPaginationContextValue = { getValuePage, pagePosition }

  // Set MyVoucherSortContext.Provider value
  const myVoucherSortContextValue = {
    clearValueSort,
    getValueSort,
    listSort,
    sortVoucherBy,
  }

  // Set MyVoucherTabContext.Provider value
  const myVoucherTabContextValue = { getValueTab, listTab }

  // Set MyVoucherRefundContext.Provider value
  const myVoucherRefundContextValue = {
    accountMain,
    dataVoucherRefund,
    handleChangeMainAccount,
    handleClickDeleteAccount,
    handleClickUpdateMainAccount,
    handleClickDeleteUserAccount,
    handleCloseDeleteAccount,
    handleCloseManageAccount,
    handleManageAccount,
    handleShowConfirmationSetAccount,
    handleCloseConfirmationSetAccount,
    handleCloseManageAccountDesktop,
    manageAccountData,
    modalAddAccountShow,
    modalAddAccountShowMobile,
    modalFailedDeleteAccountClose,
    modalRefundClose,
    modalRefundContinue,
    modalRefundOpen,
    modalRequestRefundClose,
    modalSelectAccountClose,
    modalSelectAccountOpen,
    refetchUserBankAccounts: () => getUserBankAccount.refetch(),
    refundStatus,
    showAccount,
    showDeleteAccount,
    showFailedDeleteAccount,
    showManageAccount,
    showRefund,
    showRequestRefund,
    showSetMainAccout,
    userAccounts,
    bankAccountActionMessage,
    isOpenAddBankAccountModal,
    isOpenBankAccountInfobox,
    isOpenOtpModal,
    handleOpenVoucherRefundModal,
    setBankAccountActionMessage,
    setIsOpenAddBankAccountModal,
    setIsOpenBankAccountInfobox,
    setIsOpenOtpModal,
    setShowAccount,
  }

  // Call api to get list categories
  const getCategories = useGetCategories()

  // Call api to get list official partners
  const getOfficialPartner = useGetOfficialPartner(null)

  // Call api to get list partners
  const getPartner = useGetPartner(null)

  // Call api to get list vouchers
  const getVoucher = useGetVoucher(paramVouchers)

  // Call api to get how to use voucher
  const getHowToUseVoucher = useGetHowToUseVoucher(wordingParams)

  // Call api to get user bank account
  const getUserBankAccount = useGetUserBankAccount()

  useEffect(() => {
    setVouchers({
      data: getVoucher.data,
      error: getVoucher.error,
      isLoading: getVoucher.isLoading,
    })
  }, [getVoucher.data])

  useEffect(() => {
    const getVoucherDetail = async () => {
      setVoucherDetail({
        ...voucherDetail,
        isLoading: true,
      })
      if (voucherDetailCode.length) {
        const response = await globalAPI.getVoucherDetail({
          voucher_code: voucherDetailCode,
        })
        if (response?.status === 200 && !response?.data?.error) {
          setVoucherDetail({
            data: response?.data?.data,
            error: response?.data?.error,
            isLoading: false,
          })
        }
      }
    }
    getVoucherDetail()
  }, [voucherDetailCode])

  useEffect(() => {
    setCategories({
      data: getCategories.data,
      error: getCategories.error,
      isLoading: getCategories.isLoading,
    })

    setOfficialPartners({
      data: getOfficialPartner.data,
      error: getOfficialPartner.error,
      isLoading: getOfficialPartner.isLoading,
    })
    setPartners({
      data: getPartner.data,
      error: getPartner.error,
      isLoading: getPartner.isLoading,
    })
  }, [getCategories.data, getOfficialPartner.data, getPartner.data])

  useEffect(() => {
    if (getUserBankAccount.data && getUserBankAccount.data.length) {
      const mainAccount = getUserBankAccount.data.filter(
        (i) => i.is_default === 10,
      )[0]
      setAccountMain({ ...mainAccount })
    }
    setUserAccounts({
      data: getUserBankAccount.data,
      error: getUserBankAccount.error,
      isLoading: getUserBankAccount.isLoading,
    })
  }, [getUserBankAccount.data])

  useEffect(() => {
    if (getHowToUseVoucher.data && getHowToUseVoucher.data.globalBody) {
      setHowToUseVoucher(getHowToUseVoucher.data.globalBody)
    }
  }, [getHowToUseVoucher.data])

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }, [pagePosition, filterVoucherBy])

  return (
    <MyVoucherContext.Provider value={myVoucherContextValue}>
      <MyVoucherDetailContext.Provider value={myVoucherDetailContextValue}>
        <MyVoucherFilterContext.Provider value={myVoucherFilterContextValue}>
          <MyVoucherPaginationContext.Provider
            value={myVoucherPaginationContextValue}
          >
            <MyVoucherRefundContext.Provider
              value={myVoucherRefundContextValue}
            >
              <MyVoucherSortContext.Provider value={myVoucherSortContextValue}>
                <MyVoucherTabContext.Provider value={myVoucherTabContextValue}>
                  {children}
                </MyVoucherTabContext.Provider>
              </MyVoucherSortContext.Provider>
            </MyVoucherRefundContext.Provider>
          </MyVoucherPaginationContext.Provider>
        </MyVoucherFilterContext.Provider>
      </MyVoucherDetailContext.Provider>
    </MyVoucherContext.Provider>
  )
}

export {
  useMyVoucherContext,
  useMyVoucherDetailContext,
  useMyVoucherFilterContext,
  useMyVoucherPaginationContext,
  useMyVoucherRefundContext,
  useMyVoucherSortContext,
  useMyVoucherTabContext,
}
