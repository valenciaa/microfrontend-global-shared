import React, {
  createContext,
  useContext,
  useEffect,
  useState,
  useRef,
} from 'react'
import {
  useGetVueRecommend,
  useSendTrackEvent,
} from '../services/api/mutations/global'
import {
  bodyFeeds,
  bodyTrack,
  setDefaultData,
  setMetaData,
} from '../utils/VueAIUtil'
import isEmpty from 'lodash/isEmpty'
import { useRouter } from 'next/router'
import { GetUniqueId } from '../utils'
import isNumber from 'lodash/isNumber'
import { useLayoutContext } from '../../../src/context/LayoutContext'

export const VueRecommendationContext = createContext({})

/** the full url https://www.ruparupa.com/c/produk/kategori-selma/ */
const exceptionPage = ['produk/kategori-selma']

export const VueRecommendationProvider = ({ children, ...props }) => {
  let {
    useVueV2 = false,
    pageName,
    moduleName = '',
    productId = '',
    productPrice = '',
    categoryDetail = {},
    brand = '',
    vueRecommedation,
    from,
    products,
    isFetchingProduct,
    productData,
    keyword,
  } = props

  const { state: pcpContext } = useLayoutContext()
  const {
    nbHits = undefined,
    correlationID = undefined,
    vueInView = false,
    typeHit = false,
    vuePageName = false,
    searchKeyword = '',
  } = pcpContext || {}

  useEffect(() => {
    if (!isEmpty(typeHit) && !isEmpty(vuePageName)) {
      // this step for direct hit from productContainer csr, searchKeyword in didMount so it rerender each keyword changes
      if (typeHit === 'direct') {
        fetchVueReco(vuePageName)
      }
    }
  }, [typeHit, vuePageName, searchKeyword])

  const [keywordFlaging, setKeywordFlaging] = useState()

  useEffect(() => {
    if (keyword && pageName && nbHits !== undefined) {
      pageName = isNumber(nbHits) && nbHits === 0 ? 'empty-search' : pageName
      isHitUseEffectRef.current = true

      hitRecoKeyword(keyword, pageName)
    }
  }, [nbHits])

  const hitRecoKeyword = (keyword, pageName) => {
    setKeywordFlaging(keyword)
    if (keyword !== keywordFlaging) {
      fetchVueReco(pageName)
    }
  }

  useEffect(() => {
    if (keyword && products !== undefined) {
      isHitUseEffectRef.current = true

      let pageName = 'pcp-search'
      if (products?.total < 1) {
        pageName = 'empty-search'
      }

      hitRecoKeyword(keyword, pageName)
    }
  }, [keyword, products])

  /** CategoryData(Object) contain level and url path **/
  const [vueModuleData, setVueModuleData] = useState(false)

  const router = useRouter()

  const { mutate } = useGetVueRecommend({
    onSuccess: async (res) => {
      if (res?.data?.data) {
        const data = res?.data?.data
        setVueModuleData(data)
      }
    },
  })
  const { mutate: sendTracking } = useSendTrackEvent()

  useEffect(() => {
    if (from === 'page-not-found') {
      fetchVueTrack(from)
      fetchVueReco(from)
    }
    if (from === 'home' || from === 'pdp') {
      fetchVueTrack(from, 'direct')

      if (from === 'home') {
        fetchVueReco(from)
      }
    }
  }, [])

  const useInViewPage = ['pdp']
  const useVueVer2 = ['pdp', 'home']
  useEffect(() => {
    if (vueInView && useInViewPage.includes(from)) {
      fetchVueReco(from)
    }
  }, [vueInView])

  const routerQuery = router?.query?.categories || ''
  const combinedString = routerQuery?.[0] + '/' + routerQuery?.[1] || ''

  const fetchVueReco = async (pageName) => {
    if (routerQuery) {
      if (exceptionPage?.includes(combinedString)) {
        const emptDataVue = {
          data_module: [
            {
              module_name: 'Best Seller',
              data: [],
            },
            {
              module_name: 'Complimentary PCP 60-140',
              data: [],
            },
          ],
        }
        setVueModuleData(emptDataVue)
        return
      }
    }
    const payload = await bodyFeeds(
      pageName,
      moduleName,
      productId,
      categoryDetail,
      keyword,
      brand,
    )

    if (
      (vueInView && useInViewPage.includes(from)) ||
      useVueVer2.includes(from)
    ) {
      if (correlationID) {
        payload.data.correlation_id = correlationID
      }
    }

    mutate(payload)
  }

  const [typeFlaging, setTypeFlaging] = useState()

  const fetchVueTrack = async (pageName, type, inititalProduct) => {
    const useKeyword = router?.query?.keyword || false
    if (useKeyword) {
      pageName = 'search'
    }
    if (useKeyword && inititalProduct?.total < 1) {
      pageName = 'empty-search'
    }
    const ugid = await GetUniqueId()
    const url = window?.location?.href
    const defaultData = setDefaultData(
      url,
      document?.referrer,
      'pageView',
      'pageView',
      'true',
      ugid,
    )
    let product_list = null

    if (from === 'pdp' && type === 'direct') {
      product_list = [
        {
          product_id: productId,
          price: productPrice,
        },
      ]
    }
    const metaData = setMetaData(null, null, null, null, pageName, product_list)

    if (type !== typeFlaging) {
      defaultData.correlation_id = correlationID
      sendTracking(bodyTrack(defaultData, {}, metaData))
    }
  }

  const getRecommendationData = (type, inititalProduct = products, typeLog) => {
    if (useVueV2) {
      setTypeFlaging(type)

      let trackType = pageName

      if (['cat', 'brand'].includes(from)) {
        if (from === 'cat') {
          pageName = 'pcp-category'
        }

        if (from === 'brand') {
          pageName = trackType = 'pcp-brand'
          if (keyword) {
            trackType = 'search'
          }
          fetchVueTrack(trackType, type, inititalProduct)
        }

        if (router?.query?.keyword) {
          pageName = 'pcp-search'
        }
      }

      if (from === 'cat') {
        if (routerQuery) {
          /** if not include skip fetchTrack */
          if (!exceptionPage?.includes(combinedString)) {
            if (type !== typeFlaging) {
              trackType = 'cat'
              if (keyword) {
                trackType = 'search'
              }
              fetchVueTrack(trackType, type, inititalProduct)
            }
          }
        }
      }

      if (from === 'keyword') {
        trackType = 'search'
        fetchVueTrack(trackType, type)
      }
      if (from === 'keyword') {
        pageName = 'pcp-search'
      }
      if (inititalProduct?.total < 1) {
        pageName = 'empty-search'
      }
      if (from === '404') {
        pageName = 'page-not-found'
      }

      if (categoryDetail) {
        const { level, url_path } = categoryDetail //eslint-disable-line
        categoryDetail.l_level = level
        categoryDetail.l_category = url_path //eslint-disable-line
      }
      if (type !== typeFlaging) {
        const vueDebug = false /**  set true for Debugging */
        if (vueDebug) {
          vueLog(typeLog, pageName, trackType, from)
        }

        if (pageName) {
          fetchVueReco(pageName)
        }
      }
    }
  }

  const [queryLength, setQueryLength] = useState(undefined)
  const isHitUseEffectRef = useRef(false)

  useEffect(() => {
    setQueryLength(Object?.keys(router?.query)?.length)

    if (
      products === undefined &&
      (isEmpty(keyword) || (queryLength === undefined && !keyword))
    ) {
      getRecommendationData('DM', products, 'DM')
    }
  }, [])

  const [previousUrl, setPreviousUrl] = useState(null)

  // Use useEffect to track the previous URL when the route changes
  useEffect(() => {
    if (router.asPath !== router.route) {
      setPreviousUrl(router.asPath)
    }
  }, [router.asPath, router.route])

  useEffect(() => {
    let hitValue = 'DU'

    // prevent not hit vue when user come from not found page
    if (keyword && products == null && previousUrl === null) {
      // when came from empty page -> product is csr (undefined) -> so we get value from ssr productData
      if (isNumber(productData?.total) && productData?.total === 0) {
        hitValue = 'DU-ES'
      }
      if (isHitUseEffectRef.current) {
        return
      }
      if (
        from === 'cat' ||
        (from === 'cat' && isFetchingProduct === undefined)
      ) {
        return
      }
      getRecommendationData(hitValue, productData, 'DU-ES - DM')
      return
    }
    // prevent not hit vue  user come from not found page

    if (
      keyword &&
      !isFetchingProduct &&
      products != null &&
      isNumber(products?.total)
    ) {
      if (isNumber(products?.total) && products?.total === 0) {
        hitValue = 'DU-ES'
      }
      if (isHitUseEffectRef.current) {
        return
      }

      getRecommendationData(hitValue, products, 'DU-ES')
    }
  }, [isFetchingProduct])

  const vueLog = (type, pageName, trackType, from) => {
    const logData = {
      'Mount On': type,
      'Page Name': pageName,
      'Page Type': trackType,
      'Page From': from,
    }

    console.table(logData)
  }

  let vueData = vueModuleData
  if (!isEmpty(vueRecommedation)) {
    vueData = vueRecommedation
  }

  props.vueRecommedation = vueData
  props.correlationID = correlationID
  return (
    <VueRecommendationContext.Provider value={props}>
      {children}
    </VueRecommendationContext.Provider>
  )
}

export function useVueRecommendationContext() {
  return useContext(VueRecommendationContext)
}
