import React, { createContext, useContext, useState, useEffect } from 'react'
import localforage from 'localforage'
import Cookies from 'js-cookie'
import config from '../../../config'
import {
  useGetAllProvinces,
  useGetAllCities,
  useGetAllDistricts,
  useGetSearchedAddress,
  useGetListPhoneCountryCodes,
} from '../services/api/queries/global'
import {
  useGetCustomerMembership,
  useGetProfileDetail,
  useGetHobbiesList,
  useGetPetTypesList,
} from '../services/api/queries/profile'
import { parsePhoneLimit } from '../utils/PhoneNumber'

export const MembershipAccountContext = createContext({})
export const MembershipAccountDetailContext = createContext({})
export const MembershipAccountUpdateContext = createContext({})

function useMembershipAccountContext() {
  return useContext(MembershipAccountContext)
}

function useMembershipAccountDetailContext() {
  return useContext(MembershipAccountDetailContext)
}

function useMembershipAccountUpdateContext() {
  return useContext(MembershipAccountUpdateContext)
}

export const MembershipAccountProvider = ({ children, ...props }) => {
  const [membershipAccount, setMembershipAccountContext] = useState({})
  const [membershipAccountDetail, setMembershipAccountDetailContext] = useState(
    {
      // Form to render any profile changes
      mainFormData: {
        name: '', // this value cannot be edited
        email: '', // to edit this value must render its own form
        phone: '', // to edit this value must render its own form
        maritalStatus: 0, // to edit this value no need to render its own form
        gender: '', // to edit this value no need to render its own form
        birthday: '', // to edit this value no need to render its own form
        address: '', // to edit this value no need to render its own form
        locations: {}, // to edit this value must render its own form
      },
      // mediaSocialFormData: { twitter: '', instagram: '', facebook: '' }, // to edit this object must render its own form
      childrenFormData: [], // to edit this value must render its own form
      petsFormData: [], // to edit this value must render its own form
      hobbies: [], // to edit this value must render its own form
      // These form editor has its own form
      emailFormEditor: '',

      phoneCountryCode: 'ID',
      minimumPhoneNumberLength: 8,
      maximumPhoneNumberLength: 12,

      phoneFormEditor: '',
      locationsFormEditor: {
        district: {
          id: 0,
          name: '',
        },
        city: {
          id: 0,
          name: '',
        },
        province: {
          id: 0,
          name: '',
        },
      },
      // mediaSocialFormEditor: { twitter: '', instagram: '', facebook: '' },
      childrenFormEditor: { name: '', birthday: '', gender: '' },
      petsFormEditor: {
        name: '',
        birthday: '',
        gender: '',
        type: { code: '', name: '' },
        breed: { code: '', name: '' },
      },
      petTypeFormEditor: {
        type: { code: '', name: '' },
        breed: { code: '', name: '' },
      },
      hobbiesFormEditor: [],
      // List Options to be map
      phoneCountryCodeListMap: [],
      provincesListMap: [],
      citiesListMap: [],
      addressSearchResultsMap: [],
      petTypesListMap: [],
      districtsListMap: [],
      hobbiesListMap: [],
      // Helpers
      originalBirthday: '',
      emailIsVerified: false,
      phoneIsVerified: false,
      // phoneCountryCodeSearch: '',
      petSearch: '',
      toggleModalUpdateProfileConfirmation: false,
      toggleModalAddChildAndPetConfirmation: { toggle: false, addType: '' },
      toggleModalEditCourseAction: { toggle: false, editFrom: '', index: null },
      toggleModalEditConfirmation: {
        toggle: false,
        editAction: '',
        editFrom: '',
        index: null,
      },
      queryCityAfterProvince: { flag: false, provinceId: 0 },
      queryDistrictAfterCity: { flag: false, cityId: 0 },
      addressSearch: '',
      addressSearchQuery: '',
      errYupMainForm: {},
      success: '',
      err: '',
    },
  )
  // props setMemberPayload only for mobile web
  const {
    headerType,
    setHeaderType,
    profileChangesFlag,
    setProfileChangesFlag,
    toggleModalSaveChanges,
    setToggleModalSaveChanges,
    setMemberPayload,
    setPreviousPath,
  } = props

  const {
    data: _membershipAccountResp,
    isLoading: membershipAccountLoading,
    isError: membershipAccountIsError,
  } = useGetCustomerMembership(null, {
    enabled: headerType === 'profile',
    onError: (error) => {
      if (error?.message?.toLowerCase() === 'token not valid') {
        localforage.clear()
        Cookies.remove('access_token')
        Cookies.remove('algolia_ssr')
        Cookies.remove('ugid')
        Cookies.remove('sessionrupaUID')
        Cookies.remove('rr-sid')
        Cookies.remove('geolocation')
        localStorage.removeItem('SHIPPING_LOCATION')
        window.location.href = config.baseURL
        return
      }
    },
  })
  const { data: _profileDetailResp, refetch: refetchProfileDetail } =
    useGetProfileDetail(null, {
      enabled: headerType === 'change-profile',
      onSuccess: (data) => {
        const userData = data?.user
        // const socialMediaData = userData?.social_media_profile

        setMembershipAccountDetailContext((current) => ({
          ...current,
          mainFormData: {
            name: userData.name,
            email: userData.email,
            phone: userData.international?.phone_raw_number,
            maritalStatus: userData.marital_status.toString(),
            gender: userData.gender,
            birthday: userData.birthday,
            address: userData.address,
            locations: userData.location,
          },
          // mediaSocialFormData: {
          //   twitter: socialMediaData.twitter,
          //   instagram: socialMediaData.instagram,
          //   facebook: socialMediaData.facebook
          // },
          childrenFormData: userData.children,
          petsFormData: userData?.pets !== null ? userData.pets : [], // need to validate not null because BE returns null if there is not data
          hobbies: userData?.hobbies !== null ? userData.hobbies : [], // need to validate not null because BE returns null if there is not data
          // mediaSocialFormEditor: {
          //   twitter: socialMediaData.twitter,
          //   instagram: socialMediaData.instagram,
          //   facebook: socialMediaData.facebook
          // },
          hobbiesFormEditor: userData?.hobbies !== null ? userData.hobbies : [],
          locationsFormEditor: userData.location,
          petTypesListMap: data.petType,
          hobbiesListMap: data.hobbiesSelections,
          originalBirthday: userData.birthday,
          emailIsVerified: userData.is_email_verified,
          phoneIsVerified: userData.is_phone_verified,
          queryCityAfterProvince: {
            flag: userData?.location?.province?.id !== 0,
            provinceId: userData?.location?.province?.id,
          },
          queryDistrictAfterCity: {
            flag: userData?.location?.city?.id !== 0,
            cityId: userData?.location?.city?.id,
          },
          phoneCountryCode:
            userData?.international?.international_country_code || 'ID',
        }))
      },
    })
  const { refetch: refetchGetHobbiesList } = useGetHobbiesList(null, {
    enabled: false,
    onSuccess: (data) => {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        hobbiesListMap: data,
      }))
    },
    onError: (error) => {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        err: error.message,
      }))
    },
  })
  const { refetch: refetchGetPetTypesList } = useGetPetTypesList(null, {
    enabled: false,
    onSuccess: (data) => {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        petTypesListMap: data,
      }))
    },
    onError: (error) => {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        err: error.message,
      }))
    },
  })
  const { refetch: refetchListPhoneCountryCodes } = useGetListPhoneCountryCodes(
    null,
    {
      onSuccess: (data) => {
        setMembershipAccountDetailContext((current) => ({
          ...current,
          phoneCountryCodeListMap: data?.list_country_codes,
        }))
      },
    },
  )

  const {
    data: _allProvincesResp,
    isLoading: allProvincesLoading,
    isError: allProvincesIsError,
    refetch: refetchAllProvinces,
  } = useGetAllProvinces(null, {
    enabled:
      headerType === 'change-profile' || headerType === 'complete-profile',
  })
  const {
    data: _allCitiesResp,
    isLoading: allCitiesLoading,
    isError: allCitiesIsError,
  } = useGetAllCities(
    { provinceId: membershipAccountDetail.queryCityAfterProvince.provinceId },
    { enabled: membershipAccountDetail.queryCityAfterProvince.flag },
  )
  const {
    data: _allDistrictsResp,
    isLoading: allDistrictsLoading,
    isError: allDistrictsIsError,
  } = useGetAllDistricts(
    { cityId: membershipAccountDetail.queryDistrictAfterCity.cityId },
    { enabled: membershipAccountDetail.queryDistrictAfterCity.flag },
  )
  const {
    data: _searchedAddressResp,
    isLoading: searchedAddressLoading,
    isError: searchedAddressIsError,
  } = useGetSearchedAddress(
    {
      addressSearchType: membershipAccountDetail.addressSearchQuery,
      search: membershipAccountDetail.addressSearch,
      provinceId: membershipAccountDetail.queryCityAfterProvince.provinceId,
      cityId: membershipAccountDetail.queryDistrictAfterCity.cityId,
    },
    {
      enabled:
        membershipAccountDetail.addressSearchQuery === 'all-by-district' ||
        membershipAccountDetail.addressSearchQuery === 'by-province' ||
        membershipAccountDetail.addressSearchQuery === 'by-city' ||
        membershipAccountDetail.addressSearchQuery === 'by-district',
      onError: (error) => {
        setMembershipAccountDetailContext((current) => ({
          ...current,
          addressSearchQuery: '',
          err: error.message,
        }))
      },
    },
  )

  useEffect(() => {
    if (
      _membershipAccountResp &&
      !membershipAccountLoading &&
      !membershipAccountIsError
    ) {
      if (setMemberPayload) {
        setMemberPayload(_membershipAccountResp.membership)
      }
      setMembershipAccountContext(_membershipAccountResp)
      localforage.setItem(
        'persist:ruparupa',
        JSON.stringify({
          auth: JSON.stringify({
            user: { ..._membershipAccountResp.user },
            // ? isKLK harus di luar user
            isKlk: true,
          }),
        }),
      )
    }
  }, [_membershipAccountResp])

  useEffect(() => {
    if (_allProvincesResp && !allProvincesLoading && !allProvincesIsError) {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        provincesListMap: _allProvincesResp,
      }))
    }
    if (_allCitiesResp && !allCitiesLoading && !allCitiesIsError) {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        citiesListMap: _allCitiesResp,
        queryCityAfterProvince: {
          ...current.queryCityAfterProvince,
          flag: false,
        },
      }))
    }
    if (_allDistrictsResp && !allDistrictsLoading && !allDistrictsIsError) {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        districtsListMap: _allDistrictsResp,
        queryDistrictAfterCity: {
          ...current.queryDistrictAfterCity,
          flag: false,
        },
      }))
    }
  }, [_allProvincesResp, _allCitiesResp, _allDistrictsResp])

  useEffect(() => {
    if (membershipAccountDetail.petSearch.length > 2) {
      searchingPet(membershipAccountDetail.petSearch, false)
    } else if (
      _profileDetailResp &&
      membershipAccountDetail.petSearch.length <= 1
    ) {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        petTypesListMap: _profileDetailResp.petType,
      }))
    }
  }, [membershipAccountDetail.petSearch])

  useEffect(() => {
    if (
      _searchedAddressResp?.length > 0 &&
      !searchedAddressLoading &&
      !searchedAddressIsError
    ) {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        addressSearchResultsMap: _searchedAddressResp,
        addressSearchQuery: '',
        err: '',
      }))
    }
  }, [_searchedAddressResp])

  useEffect(() => {
    if (
      membershipAccountDetail.phoneCountryCode &&
      membershipAccountDetail.phoneCountryCodeListMap
    ) {
      const limit = parsePhoneLimit(
        membershipAccountDetail.phoneCountryCodeListMap,
        membershipAccountDetail.phoneCountryCode,
      )
      setMembershipAccountDetailContext((current) => ({
        ...current,
        minimumPhoneNumberLength: limit.min,
        maximumPhoneNumberLength: limit.max,
      }))
    }
  }, [
    membershipAccountDetail.phoneCountryCode,
    membershipAccountDetail.phoneCountryCodeListMap,
  ])

  function handleForm(event, formType, data) {
    if (event) {
      const { name, value } = event.target
      switch (formType) {
        case 'mainFormData': // mainFormData contains value that doesn't need to render its own form
        case 'childrenFormEditor':
        case 'petsFormEditor':
          if (name === 'name') {
            const newRgx = new RegExp(/^[a-zA-Z ]*$/) // regex validate alphabets with spaces
            const prohibitedInputRegexDetector = value.match(newRgx)
            if (prohibitedInputRegexDetector !== null) {
              setMembershipAccountDetailContext((current) => ({
                ...current,
                [formType]: { ...current[formType], [name]: value },
              }))
            }
          } else {
            setMembershipAccountDetailContext((current) => ({
              ...current,
              [formType]: { ...current[formType], [name]: value },
            }))
          }
          // only set setProfileChangesFlag inside function below to true here because the mainFormData doesn't have their own form to be rendered
          if (
            formType === 'mainFormData' &&
            headerType !== 'complete-profile'
          ) {
            handleHasEditProfile()
          }
          break
        case 'emailFormEditor':
        case 'phoneFormEditor':
        case 'petSearch':
        case 'addressSearch':
        case 'phoneCountryCodeSearch':
          setMembershipAccountDetailContext((current) => ({
            ...current,
            [formType]: value.trim(),
          }))
          break
        case 'mediaSocialFormEditor': // handle media social form
          setMembershipAccountDetailContext((current) => ({
            ...current,
            mediaSocialFormEditor: {
              ...current.mediaSocialFormEditor,
              [name]: value.trim(),
            },
          }))
          break
      }
    } else {
      switch (formType) {
        case 'hobbies': // this 'hobbies' case only used in desktop since the behaviour is different from mobile site
        case 'hobbiesFormEditor':
        case 'petTypeFormEditor':
          setMembershipAccountDetailContext((current) => ({
            ...current,
            [formType]: data,
          }))
          break
        case 'petType': // this case is updating from petsFormEditor to petsFormEditor
          setMembershipAccountDetailContext((current) => ({
            ...current,
            petsFormEditor: {
              ...current.petsFormEditor,
              type: data.type,
              breed: data.breed,
            },
          }))
          break
        case 'globalAddress': // to handle auto complete locationsFormEditor if customer search from default view locations form and then just click one of the search result
          setMembershipAccountDetailContext((current) => ({
            ...current,
            locationsFormEditor: data,
          }))
          break
        case 'province': // to handle if customer choose province in province form
          setMembershipAccountDetailContext((current) => ({
            ...current,
            locationsFormEditor: {
              province: data,
              city: { id: 0, name: '' },
              district: { id: 0, name: '' },
            },
          }))
          break
        case 'city': // to handle if customer choose city in city form
          setMembershipAccountDetailContext((current) => ({
            ...current,
            locationsFormEditor: {
              ...current.locationsFormEditor,
              city: data,
              district: { id: 0, name: '' },
            },
          }))
          break
        case 'district': // to handle if customer choose district in district form
          setMembershipAccountDetailContext((current) => ({
            ...current,
            locationsFormEditor: {
              ...current.locationsFormEditor,
              district: data,
            },
          }))
          break
        case 'locationsFormEditorReset': // reset the locationsFormEditor value with mainFormData.locations if customer click back button
          setMembershipAccountDetailContext((current) => ({
            ...current,
            locationsFormEditor: data,
          }))
          break
        case 'phoneCountryCode':
          setMembershipAccountDetailContext((current) => ({
            ...current,
            phoneCountryCode: data,
          }))
      }
    }
  }

  function updateRealFormByEditor(type, data) {
    // this function is to updating the final render data with the form that just edited
    switch (type) {
      case 'medsos':
        setMembershipAccountDetailContext((current) => ({
          ...current,
          mediaSocialFormData: current.mediaSocialFormEditor,
        }))
        break
      case 'children': {
        const childrenTemp = [...membershipAccountDetail.childrenFormData]
        data.name = data.name.trim()
        childrenTemp.push(data)
        setMembershipAccountDetailContext((current) => ({
          ...current,
          childrenFormData: childrenTemp,
        }))
        break
      }
      case 'pets': {
        const petsTemp = [...membershipAccountDetail.petsFormData]
        data.name = data.name.trim()
        petsTemp.push(data)
        setMembershipAccountDetailContext((current) => ({
          ...current,
          petsFormData: petsTemp,
          petSearch: '',
        }))
        break
      }
      case 'hobby':
        setMembershipAccountDetailContext((current) => ({
          ...current,
          hobbies: current.hobbiesFormEditor,
        }))
        break
      case 'locations':
        setMembershipAccountDetailContext((current) => ({
          ...current,
          mainFormData: { ...current.mainFormData, locations: data },
        }))
        break
    }
    if (headerType !== 'complete-profile') {
      handleHasEditProfile()
    }
  }

  function clearForm(type) {
    // this function is to clear the form whenever we close the form
    if (type === 'children') {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        childrenFormEditor: { name: '', birthday: '', gender: '' },
      }))
    } else if (type === 'pets') {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        petsFormEditor: {
          name: '',
          birthday: '',
          gender: '',
          type: { code: '', name: '' },
          breed: { code: '', name: '' },
        },
        petTypeFormEditor: {
          type: { code: '', name: '' },
          breed: { code: '', name: '' },
        },
      }))
    } else if (type === 'email') {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        emailFormEditor: '',
      }))
    } else if (type === 'phone') {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        phoneFormEditor: '',
      }))
    }
  }

  function searchingPet(search, reset = false) {
    // for searching pet types
    // reset = false means we empty the string value of petSearch property
    // reset = true means we replace the value of petTypesListMap property with search lists result
    if (!reset) {
      const petFilterSearch = membershipAccountDetail.petTypesListMap.filter(
        (pet) =>
          (pet.stamps_pet_type_label + pet.pet_breed_label)
            .toLowerCase()
            .includes(search.toLowerCase()),
      )
      setMembershipAccountDetailContext((current) => ({
        ...current,
        petTypesListMap: petFilterSearch,
      }))
    } else {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        petSearch: '',
      }))
    }
  }

  function deleteAdditionalInformation(type) {
    // only for pets for now
    if (type === 'pets') {
      const petsTemp = [...membershipAccountDetail.petsFormData]
      petsTemp.splice(
        membershipAccountDetail.toggleModalEditConfirmation.index,
        1,
      )
      setMembershipAccountDetailContext((current) => ({
        ...current,
        petsFormData: petsTemp,
      }))
    }
    handleHasEditProfile()
  }

  function handleChangePhoneCountryCode(countryCode) {
    setMembershipAccountDetailContext((current) => ({
      ...current,
      phoneCountryCode: countryCode,
    }))
  }

  function queryLocationsTrigger(locationQuery, flag, value) {
    // flag means to start request from useGetAllCities if the value is true
    if (locationQuery === 'queryCityAfterProvince') {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        queryCityAfterProvince: { flag: flag, provinceId: value },
      }))
    } else if (locationQuery === 'queryDistrictAfterCity') {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        queryDistrictAfterCity: { flag: flag, cityId: value },
      }))
    } else if (locationQuery === 'search') {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        addressSearchQuery: value,
      }))
    }
  }

  function searchingAddress(search, reset = false) {
    // for searching address
    if (!reset) {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        addressSearch: search,
      }))
    } else {
      setMembershipAccountDetailContext((current) => ({
        ...current,
        addressSearchResultsMap: [],
        addressSearch: '',
        err: '',
      }))
    }
  }

  function toggleModalAddMoreData(toggle, addType) {
    // for now only for mobile web
    // addType can be filled with 'addMore' or 'done'
    // 'addMore' means after you click button 'Profil Sudah Benar', the render is still inside the form
    // 'done' means after you click button 'Profil Sudah Benar', you will be returned to 'Edit Profil' page
    setMembershipAccountDetailContext((current) => ({
      ...current,
      toggleModalAddChildAndPetConfirmation: {
        toggle: toggle,
        addType: addType,
      },
    }))
  }

  function toggleModalEditCourseAction(toggle, editFrom, index) {
    // for toggleModalEditCourseAction
    // editFrom can only be filled with 'pets' for now
    // index is the index data that you click
    setMembershipAccountDetailContext((current) => ({
      ...current,
      toggleModalEditCourseAction: {
        toggle: toggle,
        editFrom: editFrom,
        index: index,
      },
    }))
  }

  function toggleModalEditConfirmation(toggle, editAction, editFrom, index) {
    // for toggleModalEditCourseAction
    // editAction can only be filled with 'delete' for now and it's possible for this property to has value 'edit' in the future
    // editFrom here is get from parameters editFrom in function toggleModalEditCourseAction
    // index here is get from parameters index in function toggleModalEditCourseAction
    setMembershipAccountDetailContext((current) => ({
      ...current,
      toggleModalEditConfirmation: {
        toggle: toggle,
        editAction: editAction,
        editFrom: editFrom,
        index: index,
      },
    }))
  }

  function toggleModalForChangeProfile(toggleModalType, toggle) {
    // for toggleModalUpdateProfileConfirmation
    setMembershipAccountDetailContext((current) => ({
      ...current,
      [toggleModalType]: toggle,
    }))
  }

  function collectErrYup(errYupCollection) {
    // to save err from yup validation for mandatory main form data if there is empty
    setMembershipAccountDetailContext((current) => ({
      ...current,
      errYupMainForm: errYupCollection,
    }))
  }

  function handleHasEditProfile() {
    if (!profileChangesFlag && config.environment === 'desktop') {
      window.history.pushState(null, '', window.location.href) // to make work eventListener popstate in SaveChangesModal.js
    }
    setProfileChangesFlag(true)
  }

  return (
    <MembershipAccountContext.Provider value={membershipAccount}>
      <MembershipAccountDetailContext.Provider
        value={{ ...membershipAccountDetail, toggleModalSaveChanges }}
      >
        <MembershipAccountUpdateContext.Provider
          value={{
            setHeaderType,
            setProfileChangesFlag,
            setToggleModalSaveChanges,
            setPreviousPath,
            refetchProfileDetail,
            refetchListPhoneCountryCodes,
            refetchAllProvinces,
            refetchGetHobbiesList,
            refetchGetPetTypesList,
            handleForm,
            updateRealFormByEditor,
            clearForm,
            searchingPet,
            deleteAdditionalInformation,
            toggleModalAddMoreData,
            toggleModalEditCourseAction,
            toggleModalEditConfirmation,
            toggleModalForChangeProfile,
            handleChangePhoneCountryCode,
            queryLocationsTrigger,
            searchingAddress,
            collectErrYup,
            handleHasEditProfile,
          }}
        >
          {children}
        </MembershipAccountUpdateContext.Provider>
      </MembershipAccountDetailContext.Provider>
    </MembershipAccountContext.Provider>
  )
}

export {
  useMembershipAccountContext,
  useMembershipAccountDetailContext,
  useMembershipAccountUpdateContext,
}
