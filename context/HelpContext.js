import React, { createContext, useContext, useEffect, useState } from 'react'

export const HelpContext = createContext()

// Create a provider component for managing help context
export const HelpProvider = ({ children, helpProps }) => {
  const [helpContext, setHelpContext] = useState({ helpProps })

  useEffect(() => {
    setHelpContext({ helpProps })
  }, [helpProps])

  return (
    <HelpContext.Provider value={helpContext}>{children}</HelpContext.Provider>
  )
}

// Custom hook for consuming help context
export function useHelpContext() {
  return useContext(HelpContext)
}
