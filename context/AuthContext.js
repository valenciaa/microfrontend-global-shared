import React, { createContext, useContext, useState, useEffect } from 'react'
import Cookies from 'js-cookie'
import { useGetListPhoneCountryCodes } from '../services/api/queries/global'
import { parsePhoneLimit } from '../utils/PhoneNumber'
import { useGetSocialLoginUrl } from '../services/api/queries/auth'

export const AuthContext = createContext({})

export const AuthProvicer = ({ children, styles }) => {
  const [authStyles] = useState(styles)
  const [form1Body, setForm1Body] = useState({})
  const [form2Body, setForm2Body] = useState({})
  const [isEmailDisabled, setIsEmailDisabled] = useState(false)
  const [isPhoneDisabled, setIsPhonelDisabled] = useState(false)
  const [mergeAccountInfo, setMergeAccountInfo] = useState(null)
  const [formError, setFormError] = useState('')
  const [isMemberIdOnly, setIsMemberIdOnly] = useState(false)
  const [phoneCountryCode, setPhoneCountryCode] = useState('ID') // ? Default always indonesia
  const [phoneCountryCodeListMap, setPhoneCountryCodeListMap] = useState([])
  const [phoneNumberLimit, setPhoneNumberLimit] = useState({ min: 8, max: 12 }) // ? Default always indonesia min and max
  const [googleClientId, setGoogleClientId] = useState(null) // ? for google sign in

  const { refetch: refetchGetPhoneCountryCodes } = useGetListPhoneCountryCodes(
    null,
    {
      onSuccess: (data) => {
        setPhoneCountryCodeListMap(data?.list_country_codes)
      },
    },
  )

  useGetSocialLoginUrl(
    {
      type_oauth: 'google',
      device: 'web',
    },
    {
      onSuccess: (data) => {
        if (data?.google?.client_id) {
          setGoogleClientId(data?.google?.client_id)
        } else {
          console.log('ERROR GET SOCIAL LOGIN URL')
        }
      },
    },
  )

  useEffect(() => {
    if (phoneCountryCode && phoneCountryCodeListMap) {
      setPhoneNumberLimit(
        parsePhoneLimit(phoneCountryCodeListMap, phoneCountryCode),
      )
    }
  }, [phoneCountryCode, phoneCountryCodeListMap])

  function checkActivateTrackCookie() {
    // function get cookie for track mixpanel
    const trackActivate = Cookies.get('track_activate')
    return trackActivate
  }

  return (
    <AuthContext.Provider
      value={{
        authStyles,
        form1Body,
        setForm1Body,
        form2Body,
        setForm2Body,
        isEmailDisabled,
        setIsEmailDisabled,
        isPhoneDisabled,
        setIsPhonelDisabled,
        mergeAccountInfo,
        setMergeAccountInfo,
        formError,
        setFormError,
        setIsMemberIdOnly,
        isMemberIdOnly,
        checkActivateTrackCookie,
        phoneCountryCodeListMap,
        phoneCountryCode,
        setPhoneCountryCode,
        phoneNumberLimit,
        setPhoneNumberLimit,
        refetchGetPhoneCountryCodes,
        googleClientId,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export function useAuthContext() {
  return useContext(AuthContext)
}
