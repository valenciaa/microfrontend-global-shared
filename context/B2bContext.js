import React, { createContext, useContext, useEffect, useState } from 'react'

export const B2bContext = createContext({})

export const B2bProvider = ({ children, results, styles, b2bStyles }) => {
  const [b2bContext, setB2bContext] = useState({ results, styles, b2bStyles })

  useEffect(() => {
    setB2bContext({ results, styles, b2bStyles })
  }, [results, styles, b2bStyles])

  return (
    <B2bContext.Provider value={b2bContext}>{children}</B2bContext.Provider>
  )
}

export function useB2bContext() {
  return useContext(B2bContext)
}
