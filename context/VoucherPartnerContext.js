import React, { createContext, useContext, useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import localforage from 'localforage'
import isEmpty from 'lodash/isEmpty'
import GetParam from '../utils/GetParam'
import {
  useGetVouchers,
  useGetVoucherPartnerDetail,
} from '../services/api/queries/voucher'
import { useRedeemVoucherPartner } from '../services/api/mutations/voucher'
import { useGetDedicatedMemberTier } from '../services/api/queries/membership'
import { mixpanelKlkTrack } from '../utils/MixpanelWrapper'

export const VoucherPartnerContext = createContext({})
export const VoucherPartnerUpdateContext = createContext({})

function useVoucherPartnerContext() {
  return useContext(VoucherPartnerContext)
}

function useVoucherPartnerUpdateContext() {
  return useContext(VoucherPartnerUpdateContext)
}

export const VoucherPartnerProvider = ({ children, ...props }) => {
  const [voucherPartnerContext, setVoucherPartnerContext] = useState({
    voucherPartnerList: [],
    category: '',
    sortBy: 'expire_soon',
    page: 1,
    totalPage: null,
    voucherPartnerDetail: null,
    voucherPartnerCode: '',
    currentTab: '',
    // Helpers
    queryFilterVoucherPartner: false,
    err: '',
    errRedeem: '',
  })
  const router = useRouter()
  const [tier, setTier] = useState('')
  const [memberID, setMemberID] = useState('')
  const [isButtonSubmit, setIsButtonSubmit] = useState(false)
  const { pageFrom, voucherRuleParam } = props

  const {
    isLoading: voucherPartnerLoading,
    isFetching: voucherPartnerFeching,
    refetch: refetchVoucherPartner,
  } = useGetVouchers(
    {
      category: voucherPartnerContext.category,
      online_status: '',
      official_partner: '',
      promo_type: '',
      payment_method: '',
      partner: '',
      sort_by: voucherPartnerContext.sortBy,
      page: voucherPartnerContext.page,
      limit: 0,
    },
    {
      enabled: false,
      onSuccess: (data) => {
        const tab = GetParam('tab')
        setVoucherPartnerContext((current) => ({
          ...current,
          voucherPartnerList: data?.external_voucher_list || [],
          totalPage: data?.total_page,
          currentTab: !tab || tab === 'voucher' ? 'Voucher' : 'Promo Merchant',
          queryFilterVoucherPartner: false,
          err: '',
        }))
      },
      onError: (error) => {
        const tab = GetParam('tab')
        setVoucherPartnerContext((current) => ({
          ...current,
          voucherPartnerList: [],
          currentTab: !tab || tab === 'voucher' ? 'Voucher' : 'Promo Merchant',
          queryFilterVoucherPartner: false,
          err: error?.message.includes('422')
            ? 'Server Error, please try again later'
            : error.message || 'Server Error',
        }))
      },
    },
  )
  const {
    data: voucherPartnerDetailData,
    isLoading: voucherPartnerDetailLoading,
    iFetching: voucherPartnerDetailFetching,
    refetch: refetchVoucherPartnerDetail,
  } = useGetVoucherPartnerDetail(
    {
      rule_id: pageFrom === 'detail-voucher-merchant' && voucherRuleParam,
    },
    {
      enabled: false,
      onSuccess: (data) => {
        setVoucherPartnerContext((current) => ({
          ...current,
          voucherPartnerDetail: data?.voucher_detail,
          voucherBenefit: data?.voucher_benefit || {},
          err: '',
        }))
      },
      onError: (error) => {
        setVoucherPartnerContext((current) => ({
          ...current,
          err: error.message || 'Server Error',
        }))
      },
    },
  )
  const {
    mutate: redeemVoucherPartner,
    isLoading: redeemVoucherPartnerLoading,
  } = useRedeemVoucherPartner({
    onSuccess: (res) => {
      setIsButtonSubmit(false)
      if (!res?.data?.error) {
        setVoucherPartnerContext((current) => ({
          ...current,
          voucherPartnerCode:
            res?.data?.data?.voucher_detail?.voucher_code || '',
        }))

        const notes = localStorage.getItem('voucher_merchant_notes')
        localStorage.removeItem('voucher_merchant_notes')

        mixpanelKlkTrack('Sukses Pakai Voucher Merchant', '', {
          voucherName: `${res?.data?.data?.voucher_detail?.voucher_code_title} - ${res?.data?.data?.voucher_detail?.benefit}`,
          voucherCode: res?.data?.data?.voucher_detail?.voucher_code,
          voucherNotes: notes
        })
      } else {
        setVoucherPartnerContext((current) => ({
          ...current,
          errRedeem: res?.data?.data?.messages?.[0] || 'Server Error',
        }))
      }
    },
    onError: (error) => {
      setVoucherPartnerContext((current) => ({
        ...current,
        errRedeem: error.message || 'Server Error',
      }))
      setIsButtonSubmit(false)
    },
  })
  const {
    isLoading: dedicatedMemberTierLoading,
    isFetching: dedicatedMemberTierFetching,
    refetch: refetchDedicatedMemberTier,
  } = useGetDedicatedMemberTier(null, {
    enabled: false,
    onSuccess: async (data) => {
      setTier(data?.membership_tier)
      const authData = await localforage.getItem('persist:ruparupa')
      let parsedAuthData = JSON.parse(authData)
      parsedAuthData = parsedAuthData?.auth
        ? JSON.parse(parsedAuthData.auth)
        : null
      if (
        !isEmpty(parsedAuthData) &&
        pageFrom === 'voucher-merchant'
      ) {
        const tab = GetParam('tab')
        let finalEventName = 'Voucher'

        if (tab === 'promo-merchant') {
          finalEventName = 'Promo'
        }
        mixpanelKlkTrack(`View Page ${finalEventName} Merchant`, '', {
          membershipStatus: data?.membership_tier,
          memberId: parsedAuthData?.user?.member_id,
        })
        setMemberID(parsedAuthData?.user?.member_id)
      }
    },
  })

  useEffect(() => {
    refetchDedicatedMemberTier()
  }, [])

  useEffect(() => {
    if (!isEmpty(tier) && pageFrom === 'voucher-merchant') {
      if (
        !dedicatedMemberTierLoading &&
        !dedicatedMemberTierFetching
      ) {
        if (GetParam('tab') === 'voucher' || GetParam('tab') === null) {
          if (GetParam('tab') === null) {
            // apend query param tab=voucher if query param not exist
            router.push(
              {
                pathname: '/klk/voucher-merchant',
                query: { tab: 'voucher' },
              },
              undefined,
              { shallow: true },
            )
          }
          setTab('Voucher')
        } else if (GetParam('tab') === 'promo-merchant') {
          setTab('Promo Merchant')
        }
      }
    }
  }, [tier])

  useEffect(() => {
    if (pageFrom === 'detail-voucher-merchant' && !isEmpty(voucherRuleParam)) {
      refetchVoucherPartnerDetail()
    }
  }, [voucherRuleParam])

  useEffect(() => {
    if (voucherPartnerContext.queryFilterVoucherPartner) {
      refetchVoucherPartner()
    }
  }, [voucherPartnerContext.queryFilterVoucherPartner])

  function handleRefetchVouchers(type = '', value) {
    if (!isEmpty(type)) {
      switch (type) {
        case 'sortBy':
          setVoucherPartnerContext((current) => ({
            ...current,
            sortBy: value,
            page: 1,
            queryFilterVoucherPartner: true,
          }))
          break
        default:
          setVoucherPartnerContext((current) => ({
            ...current,
            [type]: value,
            queryFilterVoucherPartner: true,
          }))
          break
      }
      window.scrollTo(0, 0)
    }
  }

  function setTab(tab, sortBy = 'expire_soon') {
    let finalCategory
    switch (tab) {
      case 'Voucher':
        finalCategory = 'external'
        break
      case 'Promo Merchant':
        finalCategory = 'merchant'
        break
    }

    setVoucherPartnerContext((current) => ({
      ...current,
      category: finalCategory,
      sortBy,
      page: 1,
      currentTab: tab,
      queryFilterVoucherPartner: true,
    }))
  }

  if (isEmpty(tier)) {
    return null
  } else {
    return (
      <VoucherPartnerContext.Provider
        value={{
          ...voucherPartnerContext,
          voucherPartnerLoading: voucherPartnerLoading || voucherPartnerFeching,
          voucherPartnerDetailData,
          voucherPartnerDetailLoading:
            voucherPartnerDetailLoading || voucherPartnerDetailFetching,
          redeemVoucherPartnerLoading,
          pageFrom,
          tier,
          memberID,
          isButtonSubmit,
        }}
      >
        <VoucherPartnerUpdateContext.Provider
          value={{
            handleRefetchVouchers,
            redeemVoucherPartner,
            setIsButtonSubmit,
            setTab,
          }}
        >
          {children}
        </VoucherPartnerUpdateContext.Provider>
      </VoucherPartnerContext.Provider>
    )
  }
}

export { useVoucherPartnerContext, useVoucherPartnerUpdateContext }
