import React, { createContext, useContext, useEffect } from 'react'
import isEmpty from 'lodash/isEmpty'
import { useRouter } from 'next/router'
import localforage from 'localforage'

import { useValidateSocialMediaAuth } from '../services/api/mutations/auth'
import { HandleLoginResponse } from '../utils/auth/HandleLoginResponse'
import { SetInputType } from '../utils/auth/SetInputType'
import config from '../../../config'

export const SocialMediaAuthContext = createContext({})

export const SocialMediaAuthProvider = ({ children }) => {
  const router = useRouter()
  const { mutate } = useValidateSocialMediaAuth({
    onSuccess: async (res) => {
      if (res?.data?.error === false && res?.data?.data) {
        const {
          next_action: nextAction,
          access_token: accessToken,
          user: userData,
          is_b2bc: b2bcData,
          identifier,
          input_type: inputType,
          data,
          legacy_member: legacyMember,
          bypass_activate_data: bypassData,
        } = res.data.data

        // ? Check disable email dan phone
        await localforage.removeItem('KLK_isDisabled')
        await localforage.removeItem('userInfo')
        await localforage.removeItem('chosenMemberToken')

        if (nextAction === 'delete') {
          window.location.href = config.baseURL + 'auth/login?is_delete=10'
          return
        } else if (nextAction === 'login') {
          HandleLoginResponse(
            accessToken,
            userData,
            b2bcData,
            ({ isNeedForceMerging }) => {
              if (isNeedForceMerging) {
                router.push('/auth/merge-account')
              } else {
                router.push('/')
              }
            },
            router,
            undefined,
            undefined,
            'Google',
          )
        } else {
          localforage.setItem('identifier', identifier)

          await SetInputType(inputType)

          if (
            (nextAction === 'register' || nextAction === 'login_legacy') &&
            !isEmpty(data)
          ) {
            const {
              email,
              member_id: memberId,
              phone,
              companyCode,
              imageUrl,
              merchant,
              memberToken,
              validationMethod,
            } = data
            localforage.setItem('userInfo', {
              email,
              phone,
              member_id: memberId,
              companyCode,
              imageUrl,
              merchant,
              memberToken,
              validationMethod,
            })
          } else if (nextAction === 'activate') {
            await localforage.setItem('legacy_member', legacyMember)
          }

          // ? IF LEGACY MEMBERS ONLY ONE
          if (!isEmpty(bypassData)) {
            await localforage.setItem(
              'activateMemberToken',
              bypassData.activate_member_token,
            )
            sessionStorage.setItem('bp_email', bypassData.bp_email)
            sessionStorage.setItem(
              'non_bp_identifier',
              bypassData.bp_phone_raw_number,
            )
          }

          const toRegisterNextAction = ['register', 'activate', 'login_legacy']
          if (toRegisterNextAction.includes(nextAction)) {
            const isPartialRegister =
              nextAction === 'register' && !isEmpty(data)

            delete router.query.code
            router.push(
              {
                pathname: '/auth/register',
                query: {
                  ...router.query,
                  action: isPartialRegister ? 'partial-register' : nextAction,
                  // ? IF LEGACY MEMBERS ONLY ONE BYPASS TO PROFILE FORM 1
                  component: 'tnc',
                  social: 'google-cohesive',
                },
              },
              undefined,
              {
                shallow: false,
              },
            )
          }
        }
      }
    },
  })

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search)
    const codeParams = urlParams.get('code')
    mutate({
      code: codeParams,
    })
  }, [])
  return (
    <SocialMediaAuthContext.Provider value={{}}>
      {children}
    </SocialMediaAuthContext.Provider>
  )
}

export function useSocialMediaAuthContext() {
  return useContext(SocialMediaAuthContext)
}
