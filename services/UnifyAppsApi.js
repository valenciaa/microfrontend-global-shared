import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

// const isV2 = config.useAssignationPhase2

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getWarrantyList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/service-center/warranty-availability/' +
        data.invoice +
        data.transactionType,
      {},
      headerWithToken(token),
    )
  }

  const getWarrantyDetail = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/service-center/warranty-details/' + data.aceWarrantyId + data.transType,
      {},
      headerWithToken(token),
    )
  }

  const activateWarranty = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/service-center/warranty-activation/' + data.transactionType,
      { data: data.data },
      headerWithToken(token),
    )
  }

  const getTransactionDetail = async (data) => {
    const token = await tokenForage()
    if (data.transactionType === 'online') {
      return apiWrapper.get(
        '/unify/user/transaction/detail?no=' +
          data.transactionNo +
          '&type=' +
          data.transactionType +
          '&token=' +
          token,
        {},
        headerWithToken(token),
      )
    } else {
      return apiWrapper.get(
        '/unify/user/transaction/detail?no=' +
          data.transactionNo +
          '&company_code=' +
          data.companyCode +
          '&type=' +
          data.transactionType +
          '&method=' +
          data.method +
          '&token=' +
          token +
          '&receipt_id=' +
          data.receiptId,
        {},
        headerWithToken(token),
      )
    }
  }

  const transactionStatusUpdate = (data) => {
    return apiWrapper.post('/shipment/shipment-status', { data })
  }

  const recreateOrder = (data) => {
    return apiWrapper.post('/cart/recreate', { data })
  }

  const getTransactionList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/unify/user/transaction/list',
      { ...data, token },
      headerWithToken(token),
    )
  }

  const getTransactionListWithCache = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/unify/user/transaction/list-with-cache',
      { ...data, token },
      headerWithToken(token),
    )
  }

  const getTransactionFilter = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/unify/user/transaction/filter',
      null,
      headerWithToken(token),
    )
  }

  const getWaitingPaymentList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/user/order-summary', data, headerWithToken(token))
  }

  const getHowToPay = async (data) => {
    return apiWrapper.get('/unify/constant/howtopay', data)
  }

  const getWaitingPaymentDetail = async (data) => {
    return apiWrapper.get(
      '/order/' +
        data.orderId +
        '?customer_email=' +
        data.email +
        '&type=' +
        data.pageFrom,
    )
  }

  const cancelOrder = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/order/cancel', data, headerWithToken(token))
  }
  // Faktur Pajak
  const getFakturStatus = (data) => {
    return apiWrapper.get(
      `/misc/get-status/${data.orderNo}?invoice_no=${data.invoiceNo}`,
    )
  }

  const getFakturData = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/user/customer-company',
      null,
      headerWithToken(token),
    )
  }

  const deleteFakturData = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/user/delete-faktur',
      { data },
      headerWithToken(token),
    )
  }

  const addCompanyData = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/user/add-customer-company',
      data,
      headerWithToken(token),
    )
  }

  const updateCompanyData = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      '/user/customer-company',
      data,
      headerWithToken(token),
    )
  }

  const addFakturInvoice = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/user/add-faktur-invoice',
      { data },
      headerWithToken(token),
    )
  }

  const downloadFaktur = async ({ orderId, invoiceId }) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/user/downloadFaktur/${orderId}/${invoiceId}`,
      {},
      headerWithToken(token),
    )
  }

  const postFakturRevision = (data) => {
    return apiWrapper.post('/misc/revisiFaktur', { data })
  }

  const getReviewCount = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      'unify/user/unreviewed/count',
      data,
      headerWithToken(token),
    )
  }

  const getReviewList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      'unify/user/review/list',
      data,
      headerWithToken(token),
    )
  }

  // Improvement Review Rating
  const getReviewDetailProduct = async (data) => {
    const token = await tokenForage()

    return apiWrapper.get(
      'unify/user/review/detail/product',
      data,
      headerWithToken(token),
    )
  }

  const getReviewDetailService = async (data) => {
    const token = await tokenForage()

    return apiWrapper.get(
      'unify/user/review/detail/service',
      data,
      headerWithToken(token),
    )
  }

  // Service Centre
  const getServiceListOnline = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/service-center/warranty-services',
      data,
      headerWithToken(token),
    )
  }
  const getServiceListOffline = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/service-center/warranty-services/offline',
      data,
      headerWithToken(token),
    )
  }

  const getServiceDetail = async ({ serviceNo }) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/service-center/warranty-services/${serviceNo}/details`,
      {},
      headerWithToken(token),
    )
  }

  const postServiceChargeConfirmation = async ({ data }) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/service-center/warranty-services/service-charge-confirmation',
      { data },
      headerWithToken(token),
    )
  }

  const putVoucherRefund = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put('/refund/voucher', data, headerWithToken(token))
  }

  const postReview = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      'unify/user/review/',
      data.data,
      headerWithToken(token),
    )
  }

  const postReviewAverage = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      'unify/user/review/service/calculate-average',
      data,
      headerWithToken(token),
    )
  }

  // Instalasi 3.0 Start
  const getInstallationItemList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/installation/list/profile',
      data,
      headerWithToken(token),
    )
  }

  const getInstallationItemDetail = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `installation/v2/detail/${data.salesInstallationId}`,
      data,
      headerWithToken(token),
    )
  }

  const getOfflineInstallationItemDetail = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `installation/offline-detail/${data.receipt_id}`,
      data,
      headerWithToken(token),
    )
  }

  const getOfflineInstallationReceipt = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `installation/offline-receipt/${data.transactionNo}`,
      data,
      headerWithToken(token),
    )
  }

  const getItemNotInstalled = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/installation/item-not-installed',
      data,
      headerWithToken(token),
    )
  }

  const getPaymentMethodHistory = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/installation/payment-method',
      data,
      headerWithToken(token),
    )
  }

  const getRefundHistory = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/installation/refund-history',
      data,
      headerWithToken(token),
    )
  }

  const cancelInstallation = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      '/installation/update/status-canceled',
      data,
      headerWithToken(token),
    )
  }

  const rescheduleInstallation = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      '/installation/update/status-reschedule',
      data,
      headerWithToken(token),
    )
  }

  const deleteAddress = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/user/address-delete', data, headerWithToken(token))
  }

  const makePrimaryAddress = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      `/user/address-primary/${data}`,
      {},
      headerWithToken(token),
    )
  }

  const createInstallationAfterOrder = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      'installation/create/after-order',
      data,
      headerWithToken(token),
    )
  }
  // Instalasi 3.0 End

  const getBankList = async () => {
    return apiWrapper.get('misc/bank')
  }

  const postBankAccOtp = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      'user/bank-account/otp',
      data,
      headerWithToken(token),
    )
  }
  const addBankAccount = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      'user/bank-account/add',
      data,
      headerWithToken(token),
    )
  }

  // Improvement Review Rating
  const postSingleProductReview = async (data) => {
    const token = await tokenForage()

    return apiWrapper.post(
      'unify/user/review/product/single-product',
      data,
      headerWithToken(token),
    )
  }

  const postBulkProductReview = async (data) => {
    const token = await tokenForage()

    return apiWrapper.post(
      'unify/user/review/product/bulk-product',
      { data },
      headerWithToken(token),
    )
  }

  const postServiceReview = async (data) => {
    const token = await tokenForage()

    return apiWrapper.post(
      'unify/user/review/service',
      data,
      headerWithToken(token),
    )
  }

  return {
    getWarrantyList,
    getWarrantyDetail,
    activateWarranty,
    getTransactionDetail,
    transactionStatusUpdate,
    recreateOrder,
    getTransactionList,
    getTransactionListWithCache,
    getTransactionFilter,
    getWaitingPaymentList,
    getHowToPay,
    getWaitingPaymentDetail,
    cancelOrder,
    getFakturStatus,
    getFakturData,
    deleteFakturData,
    addCompanyData,
    updateCompanyData,
    addFakturInvoice,
    downloadFaktur,
    postFakturRevision,
    getReviewCount,
    getReviewList,
    getReviewDetailProduct,
    getReviewDetailService,
    getServiceListOnline,
    getServiceListOffline,
    getServiceDetail,
    postServiceChargeConfirmation,
    putVoucherRefund,
    postReview,
    postReviewAverage,
    getInstallationItemList,
    getInstallationItemDetail,
    getOfflineInstallationItemDetail,
    getOfflineInstallationReceipt,
    getItemNotInstalled,
    getPaymentMethodHistory,
    getRefundHistory,
    cancelInstallation,
    rescheduleInstallation,
    deleteAddress,
    makePrimaryAddress,
    createInstallationAfterOrder,
    getBankList,
    postBankAccOtp,
    addBankAccount,
    postSingleProductReview,
    postBulkProductReview,
    postServiceReview,
  }
}

export default { create }
