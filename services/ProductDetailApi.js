import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'
import GetB2bType from '../utils/GetB2bType'

const isV2 = config.useAssignationPhase2

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  let businessUnit = ''
  if (config.isB2b) {
    businessUnit = GetB2bType()
  }

  const getProductDetail = ({ urlKey, b2bcUrlKey }) => {
    // Do we need storeCode?
    // `/product-detail/${urlKey}?storeCode=${CheckUndefined(storeCode)
    return apiWrapper.get(
      `/product-detail/${urlKey}?newpdp=${true}${
        b2bcUrlKey ? `&b2bc_url_key=${b2bcUrlKey}` : ''
      }`,
    )
  }

  const getTncPdp = async (payload) => {
    return apiWrapper.get(`/marketing/rule/detail/${payload}`)
  }

  // Required parameters sku : product sku
  const getProductStock = (data) => {
    const isB2b = config.isB2b
    const isLongLat = data?.loc?.latitude && data?.loc?.longitude
    const longLatQuery = `?latitude=${data?.loc?.latitude}&longitude=${data?.loc.longitude}`
    let queryParams = ''
    if (isLongLat && isB2b) {
      queryParams = `${longLatQuery}&${businessUnit}`
    } else if (isLongLat && !isB2b) {
      queryParams = longLatQuery
    } else if (!isLongLat && isB2b) {
      queryParams = '?' + businessUnit
    }
    return apiWrapper.get(`/stock/${data?.sku}${queryParams}`)
  }

  const getProductMaxStock = (data) => {
    return apiWrapper.get(
      `/stock/max-stock/${data?.sku}/${data?.district}${
        businessUnit !== '' ? '?business_unit=' + businessUnit : ''
      }`,
    )
  }

  // Required parameters sku : product sku, kecamatan_id, qty_ordered
  const getProductCanDelivery = async (data) => {
    const token = await tokenForage()
    let params = {
      is_shipper: true,
    }

    if (config.isB2b) {
      params = {
        cart_type: GetB2bType(),
        is_shipper: false,
      }
    }

    if (!token) {
      return apiWrapper.get(
        `/stock${isV2 ? '/v2' : ''}/can-delivery/${data?.sku}/${
          data?.district
        }/${data?.qty}`,
        params,
      )
    } else {
      return apiWrapper.get(
        `/stock${isV2 ? '/v2' : ''}/can-delivery/${data?.sku}/${
          data?.district
        }/${data?.qty}`,
        params,
        headerWithToken(token),
      )
    }
  }

  const getBrandUrlKey = (data) => {
    return apiWrapper.get(`/misc/brand-url-key?brand_id=${data}`)
  }

  const getBankInstallment = () => {
    return apiWrapper.get('/misc/bank-installment')
  }

  const kredivoCalculator = (data) => {
    return apiWrapper.get(
      `/kredivo/calculator?amount=${data?.amount}&id=${data?.id}&name=${data?.name}&price=${data?.price}&quantity=${data?.quantity}`,
    )
  }

  const danakiniSimulation = (data) => {
    return apiWrapper.get(`/danakini/simulation?amount=${data?.amount}`)
  }

  const getVoucherPDP = (sku) => {
    return apiWrapper.get(
      `/tahu/voucher-pdp/${sku}?device=${config.environment}`,
    )
  }

  const getProvince = () => {
    return apiWrapper.get('/misc/province')
  }

  const getCity = (provinceId) => {
    return apiWrapper.get(`/misc/city/${provinceId}`)
  }

  const getKecamatan = (cityId) => {
    return apiWrapper.get(`/misc/kecamatan/${cityId}`)
  }

  const getExpressDelivery = (kecamatanId) => {
    return apiWrapper.get(`/misc/canExpressDelivery/${kecamatanId}`)
  }

  const getWordingContent = (data) => {
    return apiWrapper.get('/tahu/getWordingContent', data)
  }

  const getAddress = async () => {
    const token = await tokenForage()
    return apiWrapper.get('/user/address-list', {}, headerWithToken(token))
  }

  const getReviewRatingPDPNoToken = async (data) => {
    const token = await tokenForage()
    if (!token) {
      return apiWrapper.get('/review/product/pdp/v2', data)
    } else {
      return apiWrapper.get(
        '/review/product/pdp/v2',
        data,
        headerWithToken(token),
      )
    }
  }

  const getReviewRatingImages = async (data) => {
    return apiWrapper.get('review/product/images', data)
  }

  const toggleWishlist = async (data) => {
    const token = await tokenForage()
    const { sku, method } = data
    return apiWrapper.post(
      '/user/wishlist',
      { sku, method },
      headerWithToken(token),
    )
  }

  const likeReviewRating = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      '/review/product/update/like',
      data,
      headerWithToken(token),
    )
  }

  const getProductPriceZone = ({ data }) => {
    return apiWrapper.get(`/product/price-zone/${data.sku}`, {
      zone_id: data.zone_id,
    })
  }

  const getProductGroup = (data) => {
    return apiWrapper.get('/product/group/' + data.productVariantGroupId, {
      store_code: data.storeCode,
    })
  }

  const getVouchers = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/voucher/vouchers',
      data,
      headerWithToken(token || ''),
    )
  }

  const getRegistryList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/gift/registry', data, headerWithToken(token))
  }

  const modifyRegistryItem = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put('/gift/registry/item', data, headerWithToken(token))
  }

  const getProductBundling = (data) => {
    return apiWrapper.get(
      `/product-bundling/bundling-list?sku=${data.sku}&zoneId=${data.zone_id}`,
    )
  }

  const getProductBundlingDetail = (data) => {
    return apiWrapper.get(
      `/product-bundling/bundling-detail?packageId=${data.package_id}&zoneId=${data.zone_id}&fromSku=${data.from_sku}`,
    )
  }

  const getTotalClickAffiliate = async (data) => {
    return apiWrapper.get('/affiliate/total-click', data)
  }

  const getVirtualShowRoomPDP = async (data) => {
    // Providing default values for color, sku, and l3Category if data is undefined
    const { color = '', sku = '', l3Category = '' } = data || {}

    let additionalQuery = `?platform=${config?.environment}&category_third_lvl_pdp=${l3Category}&sku_pdp=${sku}&color_pdp=${color}`
    return apiWrapper.get('/tahu/virtual-showroom/pdp' + additionalQuery)
  }

  const getProductCheckAvailability = (data) => {
    return apiWrapper.get(`product/check-availability/${data?.urlKey?.[0]}`)
  }

  return {
    getBrandUrlKey,
    getProductDetail,
    getProductStock,
    getProductMaxStock,
    getProductCanDelivery,
    getBankInstallment,
    kredivoCalculator,
    danakiniSimulation,
    getVoucherPDP,
    getReviewRatingImages,
    getProvince,
    getCity,
    getKecamatan,
    getExpressDelivery,
    getWordingContent,
    getAddress,
    getReviewRatingPDPNoToken,
    toggleWishlist,
    likeReviewRating,
    getProductPriceZone,
    getProductGroup,
    getVouchers,
    getRegistryList,
    modifyRegistryItem,
    getProductBundling,
    getProductBundlingDetail,
    getTotalClickAffiliate,
    getVirtualShowRoomPDP,
    getProductCheckAvailability,
    getTncPdp,
  }
}

export default { create }
