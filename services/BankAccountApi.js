import apisauce from 'apisauce'
import localforage from 'localforage'

import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getRefundHistory = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/refund/history', data, headerWithToken(token))
  }

  const getUserBankAccountByAccountNumber = async (params) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/user/bank-account/validate',
      params,
      headerWithToken(token),
    )
  }

  return { getRefundHistory, getUserBankAccountByAccountNumber }
}

export default { create }
