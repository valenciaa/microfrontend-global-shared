import apisauce from 'apisauce'
import config from '../../../config'
import localforage from 'localforage'
import { DEFAULT_HEADER } from '../utils/constants'

const create = (baseURL = config.apiURL) => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: baseURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const createInstantCheckout = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/user/v2/cart/update',
      { data },
      headerWithToken(token),
    )
  }

  return {
    createInstantCheckout,
  }
}

export default { create }
