import apisauce from 'apisauce'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'
import localforage from 'localforage'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers: headers,
  })

  // Slider Banner
  const hyperPersonalized = (type, cmsTmp, token, menuId) => {
    return apiWrapper.get(
      `/hyper-personalized?type=${type}&cmsTmp=${cmsTmp}&company_code=${
        config.companyCode
      }&customer_token=${token || ''}&is_b2b=${config.isB2b || false}&device=${
        config.environment
      }&menuId=${menuId || ''}`,
      {},
      token ? headerWithToken(token) : {},
    )
  }

  // Quick Filter Button
  const PersonalizedQuickFilter = async (accessToken = '') => {
    const token = accessToken !== '' ? accessToken : await tokenForage()
    if (token) {
      // Need Authentication
      return apiWrapper.get(
        `/user/personalize-filter`,
        {},
        headerWithToken(token),
      )
    } else {
      // Guest
      return apiWrapper.get(`/misc/personalize-filter-guest`)
    }
  }

  return {
    hyperPersonalized,
    PersonalizedQuickFilter,
  }
}

export default { create }
