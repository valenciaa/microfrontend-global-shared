import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getCustomerMembershipProfile = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/summarize-profile',
      { token },
      headerWithToken(token),
    )
  }

  const getProfileDetail = async (params) => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/profile-detail', params, headerWithToken(token))
  }

  const updateProfileDetail = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put('/klk/update-profile', data, headerWithToken(token))
  }

  const updateAndVerifyEmailOrPhone = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      '/klk/update-email-phone',
      data,
      headerWithToken(token),
    )
  }

  const getDeleteAccountReasons = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/delete-reason', data, headerWithToken(token))
  }

  const deleteAccount = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/klk/close-account', data, headerWithToken(token))
  }

  const getHobbiesList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/hobbies-list', data, headerWithToken(token))
  }

  const getPetTypesList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/pet-types-list', data, headerWithToken(token))
  }

  const getMembershipInforma = async () => {
    const token = await tokenForage()
    return apiWrapper.get('/user/informa-membership',{}, headerWithToken(token))
  }

  const getPointInforma = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/user/cek-point',data , headerWithToken(token))
  }

  const tukarVoucherInforma = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('user/redeem-point',data, headerWithToken(token))
  }

  return {
    getCustomerMembershipProfile,
    getProfileDetail,
    updateProfileDetail,
    updateAndVerifyEmailOrPhone,
    getDeleteAccountReasons,
    deleteAccount,
    getHobbiesList,
    getPetTypesList,
    getMembershipInforma,
    getPointInforma,
    tukarVoucherInforma
  }
}

export default {
  create,
}
