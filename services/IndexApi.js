import AuthApi from './AuthApi'
import CartApi from './CartApi'
import CoinApi from './CoinApi'
import GlobalAPI from './GlobalApi'
import HomepageAPI from './HomepageApi'
import HyperApi from './HyperApi'
import MembershipApi from './MembershipApi'
import OtpApi from './OtpApi'
import PasswordApi from './PasswordApi'
import PcpAPI from './PcpApi'
import PinApi from './PinApi'
import ProductDetailAPI from './ProductDetailApi'
import ProfileApi from './ProfileApi'
import PromoAPI from './PromoApi'
import StaticApi from './StaticApi'
import TahuApi from './TahuApi'
import UnifyAppsApi from './UnifyAppsApi'
// import accountAPI from '../../account/services/api'
import AuthB2b from './AuthB2b'
import MinicartApi from './MinicartApi'
import SavedPaymentMethodApi from './SavedPaymentMethodApi'
import VoucherApi from './VoucherApi'
import WishlistApi from './WishlistApi'
import BankAccountApi from './BankAccountApi'
import QuotationAPI from './QuototationApi'

/* Kumpulan link API */
const globalAPI = GlobalAPI.create()
const homepageAPI = HomepageAPI.create()
const hyperAPI = HyperApi.create()
const productDetailAPI = ProductDetailAPI.create()
const staticAPI = StaticApi.create()
const pcpAPI = PcpAPI.create()
const unifyAppsApi = UnifyAppsApi.create()
const profileAPI = ProfileApi.create()
const membershipAPI = MembershipApi.create()
const otpAPI = OtpApi.create()
const cartApi = CartApi.create()
const minicartApi = MinicartApi.create()
const promoAPI = PromoAPI.create()
const authAPI = AuthApi.create()
const passwordAPI = PasswordApi.create()
const pinAPI = PinApi.create()
const coinAPI = CoinApi.create()
const tahuApi = TahuApi.create()
const voucherAPI = VoucherApi.create()
const wishlistAPI = WishlistApi.create()
const authB2b = AuthB2b.create()
const savedPaymentMethodAPI = SavedPaymentMethodApi.create()
const bankAccountAPI = BankAccountApi.create()
const quotationAPI = QuotationAPI.create()

export {
  authAPI,
  authB2b,
  cartApi,
  coinAPI,
  globalAPI,
  homepageAPI,
  hyperAPI,
  membershipAPI,
  minicartApi,
  otpAPI,
  passwordAPI,
  pcpAPI,
  pinAPI,
  productDetailAPI,
  profileAPI,
  promoAPI,
  savedPaymentMethodAPI,
  staticAPI,
  tahuApi,
  unifyAppsApi,
  voucherAPI,
  wishlistAPI,
  bankAccountAPI,
  quotationAPI,
}
