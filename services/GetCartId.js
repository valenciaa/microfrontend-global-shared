import localforage from 'localforage'

const getCartId = () => {
  // return '58eb0b7914bb9f64fd346b04'
  // return false
  return localforage
    .getItem('cart_id')
    .then((value) => {
      return value
    })
    .catch(() => {
      return false
    })
}
export default { getCartId }
