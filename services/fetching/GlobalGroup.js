import { fetchingGet, fetchingPostPut } from './Base'

import { globalAPI } from '../IndexApi'
import config from '../../../../config'

const message = {
  name: '',
  success: '',
  error: 'Terjadi kesalahan, ulangi beberapa saat lagi',
}

const fetchSubscribeNewsletter = async (
  body,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchSubscribeNewsletter'
  message.success = 'Terima kasih. Email Anda sudah didaftarkan'
  fetchingPostPut(
    body,
    setFetching,
    setErr,
    setSuccess,
    globalAPI.newsletterSubscribe(body),
    message,
  )
}

const fetchGetBannerSearch = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchGetBannerSearch'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getBannerSearchBar(params),
    message,
  )
}

const fetchGetPopularSearchAlgolia = async (
  params,
  setRes,
  setFetching,
  setErr,
) => {
  message.name = 'fetchGetPopularSearchAlgolia'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.popularSearchAlgolia(params),
    message,
  )
}

const fetchFloatingVoucher = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchFloatingVoucher'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getFloatingVoucher(params),
    message,
  )
}

const fetchGetCart = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchGetCart'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getCart(params),
    message,
    setSuccess,
  )
}

const fetchUpdateCart = async (body, setFetching, setErr, setSuccess) => {
  message.name = 'fetchUpdateCart'

  const bodyModified = {
    device: config.environment,
    affiliate_id: '',
    device_token: 'pooled',
    store_code: '',
    ...body,
  }
  fetchingPostPut(
    body,
    setFetching,
    setErr,
    setSuccess,
    globalAPI.updateCart(bodyModified),
    message,
  )
}

const fetchDeleteCartItem = async (body, setFetching, setErr, setSuccess) => {
  message.name = 'fetchDeleteCartItem'

  const bodyModified = {
    device: config.environment,
    affiliate_id: '',
    device_token: 'pooled',
    store_code: '',
    ...body,
  }
  fetchingPostPut(
    body,
    setFetching,
    setErr,
    setSuccess,
    globalAPI.deleteCartItem(bodyModified),
    message,
  )
}

const fetchGetRandomizePlaceholder = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchGetRandomizePlaceholder'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.dataPlaceholderRandom(params),
    message,
    setSuccess,
  )
}

const fetchOrderSummary = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchOrderSummary'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getOrderList(params),
    message,
    setSuccess,
  )
}

const fetchGetOrderNonAuth = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchOrderSummary'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getOrderNonAuth(params),
    message,
    setSuccess,
  )
}

const fetchCheckAuthData = async (body, setFetching, setErr, setSuccess) => {
  message.name = 'fetchCheckAuthData'
  fetchingPostPut(
    body,
    setFetching,
    setErr,
    setSuccess,
    globalAPI.checkAuthData(body),
    message,
  )
}
const fetchLogin = async (body, setFetching, setErr, setSuccess) => {
  message.name = 'fetchLogin'
  fetchingPostPut(
    body,
    setFetching,
    setErr,
    setSuccess,
    globalAPI.login(body),
    message,
  )
}

const fetchLoginB2b = async (body, setFetching, setErr, setSuccess) => {
  message.name = 'fetchLoginB2b'
  fetchingPostPut(
    body,
    setFetching,
    setErr,
    setSuccess,
    globalAPI.loginB2b(body),
    message,
  )
}

const fetchGetInbox = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchGetInbox'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getInbox(params),
    message,
    setSuccess,
  )
}

const fetchGetInboxUnread = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchGetInboxUnread'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getInboxUnread(params),
    message,
    setSuccess,
  )
}

const fetchMarkInboxAsRead = async (body, setFetching, setErr, setSuccess) => {
  message.name = 'markInboxAsRead'
  fetchingPostPut(
    body,
    setFetching,
    setErr,
    setSuccess,
    globalAPI.markInboxAsRead(body),
    message,
  )
}

const fetchGetSocialLogin = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchGetSocialLogin'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getSocialLogin(params),
    message,
    setSuccess,
  )
}

const fetchOngoingEvent = async (params, setRes, setFetching, setErr) => {
  await fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getOngoingEvent(params),
    message,
  )
}

const fetchingCartAuth = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchingCartAuth'
  await fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.cartAuth(params),
    message,
    setSuccess,
  )
}

const fetchingMinicartAuth = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchingMinicartAuth'
  await fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.minicartAuth(params),
    message,
    setSuccess,
  )
}

const fetchGetUserProfile = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
) => {
  message.name = 'fetchGetUserProfile'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.userProfile(params),
    message,
    setSuccess,
  )
}

const fetchVirtualShowroom = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchVirtualShowroom'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getShowroom(params),
    message,
  )
}

const fetchProvince = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchProvince'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getProvince(params),
    message,
  )
}

const fetchCity = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchGetBannerSearch'
  fetchingGet(setRes, setFetching, setErr, globalAPI.getCity(params), message)
}

const fetchKecamatan = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchGetBannerSearch'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getKecamatan(params),
    message,
  )
}

const fetchTopSpenderCampaign = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchTopSpenderCampaign'
  await fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.topSpenderCampaign(),
    message,
  )
}

const fetchGetAllWishlist = async (
  params,
  setRes,
  setFetching,
  setErr,
  setSuccess,
  wishListLimit,
) => {
  message.name = 'fetchGetAllWishlist'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    globalAPI.getAllWishlist(params, wishListLimit),
    message,
  )
}

export {
  fetchSubscribeNewsletter,
  fetchGetBannerSearch,
  fetchGetPopularSearchAlgolia,
  fetchFloatingVoucher,
  fetchGetCart,
  fetchUpdateCart,
  fetchDeleteCartItem,
  fetchGetRandomizePlaceholder,
  fetchOrderSummary,
  fetchGetOrderNonAuth,
  fetchCheckAuthData,
  fetchLogin,
  fetchLoginB2b,
  fetchGetInbox,
  fetchGetInboxUnread,
  fetchMarkInboxAsRead,
  fetchGetSocialLogin,
  fetchOngoingEvent,
  fetchingCartAuth,
  fetchingMinicartAuth,
  fetchGetUserProfile,
  fetchVirtualShowroom,
  fetchProvince,
  fetchCity,
  fetchKecamatan,
  fetchTopSpenderCampaign,
  fetchGetAllWishlist,
}
