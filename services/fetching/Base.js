import isEmpty from 'lodash/isEmpty'
// import API from './Api'

// const api = API.create()

const handleErrorMessage = (message, setErr) => {
  if (setErr !== null) {
    setErr({ err: true, message: message })
  }
  // notifyError(message)
}

const fetchingGet = async (
  setRes,
  setFetching,
  setErr,
  api,
  message,
  setSuccess,
) => {
  if (setFetching !== null) {
    setFetching(true)
  }
  try {
    const response = await api
    if (response.status === 200) {
      if (!isEmpty(response.data?.errors)) {
        let errMsg = response.data.errors.message
        if (setErr !== null) {
          if (response.data.errors.title === 'Unauthorized') {
            errMsg =
              'Anda tidak dapat mengakses data ini, pastikan Anda memeriksa kembali parameter yang dikirimkan sesuai dengan otoritas role Anda.'
          }
          handleErrorMessage(errMsg, setErr)
        }
      } else {
        const respData = response.data.data || response.data
        const token = (response.data && response.data.token) || null
        let data = respData
        if (token) {
          data = { ...respData, token }
        }
        if (setRes !== null) {
          setRes(data)
        }
        if (setSuccess !== null) {
          setSuccess({
            success: true,
            data: data,
            message: message.success || response.data.message || '',
          })
        }
        if (setErr !== null) {
          setErr({ err: false, message: '' })
        }
      }
    } else {
      const errMsg = `Error response status: ${response.status}, while trying to ${message.name}`
      if (setErr !== null) {
        setErr({ err: true, message: errMsg })
      }
    }
    // }
  } catch (error) {
    const errMsg = 'Terjadi kesalahan, ulangi beberapa saat lagi '
    if (setErr !== null) {
      setErr({ err: true, message: errMsg })
    }
  }
  if (setFetching !== null) {
    setFetching(false)
  }
}

const fetchingPostPut = async (
  body,
  setFetching,
  setErr,
  setSuccess,
  api,
  message,
) => {
  if (setFetching !== null) {
    setFetching(true)
  }
  try {
    const response = await api
    // if (!response.ok) {
    //   handleErrorMessage(message.error, setErr)
    // } else {
    if (response.status === 200) {
      if (!isEmpty(response.data?.errors)) {
        handleErrorMessage(response.data.errors.message, setErr)
      } else {
        if (response?.data?.error) {
          handleErrorMessage(response.data.message, setErr)
        } else {
          if (setErr !== null) {
            setErr({ err: false, message: '' })
          }
          if (setSuccess !== null) {
            let data = response.data.data
            if (message.name === 'fetchLogin') {
              data = response.data
            }
            setSuccess({
              success: true,
              data: data,
              message: message.success || response.data.message || '',
            })
          }
          if (message.success !== '') {
            // notifySuccess(startCase(message.success))
          }
        }
      }
    } else {
      handleErrorMessage(response?.data?.message, setErr)
      message.error = `Error response status: ${response.status}, while trying to ${message.name}`
      //   handleErrorMessage(message.error, setErr)
    }
    // }
  } catch (error) {
    // handleErrorMessage(message.error, setErr)
  }

  if (setFetching !== null) {
    setFetching(false)
  }
}

export { fetchingGet, fetchingPostPut }
