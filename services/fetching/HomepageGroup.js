import { fetchingGet } from '../../services/fetching/Base'

import { homepageAPI } from '../IndexApi'

const message = {
  name: '',
  success: '',
  error: 'Terjadi kesalahan, ulangi beberapa saat lagi',
}

const fetchIdeaInspiration = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchIdeaInspiration'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    homepageAPI.getIdeaInspirations(params),
    message,
  )
}

const fetchGetEvent = async (params, setRes, setFetching, setErr) => {
  message.name = 'fetchGetEvent'
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    homepageAPI.getEventDetail(params),
    message,
  )
}

const fetchWelcomeModalData = async (params, setRes, setFetching, setErr) => {
  fetchingGet(
    setRes,
    setFetching,
    setErr,
    homepageAPI.getWelcomeModalData(),
    message,
  )
}

export { fetchIdeaInspiration, fetchGetEvent, fetchWelcomeModalData }
