import Cookies from 'js-cookie'
import apisauce from 'apisauce'
import config from '../../../config'
import localforage from 'localforage'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getTahuCampaign = (data) => {
    const offset = [undefined, null].includes(data.offset)
      ? ''
      : `&offset=${data.offset}`

    const filterRow = [undefined, null].includes(data.filter_row)
      ? ''
      : `&filter_row=${data.filter_row}`
    return apiWrapper.get(
      `/tahu/campaign/active/?url_key=${data.url_key}&company_code=${
        config.companyCode
      }&type=${config.environment}${offset}&only_custom_category=${
        data.only_custom_category || 0
      }${filterRow}&get_all=${data.get_all}`,
    )
  }

  const getCategoriesContent = async (params) => {
    return apiWrapper.get('/tahu/faq-categories?isNewVersion=true', params)
  }

  const getArticlesContent = async (params) => {
    return apiWrapper.get('/tahu/faq-articles?isNewVersion=true', params)
  }

  const getArticleCategoriesContent = async (params) => {
    return apiWrapper.get('/tahu/faq-articleCategories', params)
  }

  const getReviewArticlesContent = async (params) => {
    return apiWrapper.get('/tahu/faq-reviewArticles', params)
  }

  const insertReviewArticlesContent = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/tahu/faq-insert-reviewArticles',
      data,
      headerWithToken(token || ''),
    )
  }

  const getSubCategory = async (params) => {
    return apiWrapper.get('/tahu/faq/getSubCategory/' + params)
  }

  const getPopularTopic = async (params) => {
    const { offset, limit } = params
    let queryApi = '/tahu/faq/getPopularTopic'
    queryApi += `?offset=${offset}&limit=${limit}`

    return apiWrapper.get(queryApi)
  }

  const getDetailArticle = async (data) => {
    return apiWrapper.post('/tahu/faq/get-detail-article-new', data)
  }

  const reviewAccessibility = async (params) => {
    const token = await tokenForage()
    const rrSid = (await Cookies.get('rr-sid')) || ''
    if (!token) {
      params += `?rrSid=${rrSid || ''}`
    }
    return apiWrapper.get(
      '/tahu/faq/review-accessibility/' + params,
      {},
      headerWithToken(token || ''),
    )
  }

  const getTahuCustomLink = async (params) => {
    let queryApi = 'tahu/redirect'
    queryApi += `?url_key=${params.url_key}&company_code=${config.companyCode}`
    return apiWrapper.get(queryApi)
  }

  return {
    getTahuCampaign,
    getArticleCategoriesContent,
    getReviewArticlesContent,
    insertReviewArticlesContent,
    getCategoriesContent,
    getArticlesContent,
    getSubCategory,
    getPopularTopic,
    getDetailArticle,
    reviewAccessibility,
    getTahuCustomLink,
  }
}

export default {
  create,
}
