import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getCoinsCredit = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/coins-credit', data, headerWithToken(token))
  }

  const getCoinsExpired = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/coins-expired', data, headerWithToken(token))
  }

  // const getCoinsHistory = async (data) => {
  //   const token = await tokenForage()
  //   return apiWrapper.get('/klk/coins-usage', data, headerWithToken(token))
  // }
  // new api coin history
  const getCoinsHistory = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/coins-usage-stamps',
      data,
      headerWithToken(token),
    )
  }
  // new api coin history

  const redeemCoinIntoVoucher = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/klk/voucher-redeem-by-coin',
      data,
      headerWithToken(token),
    )
  }

  const getRedemptionApproval = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/pending-redeem-approval',
      data,
      headerWithToken(token),
    )
  }

  const getRedemptionApprovalInbox = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/pending-redeem-inbox',
      data,
      headerWithToken(token),
    )
  }

  const redemptionApprovalResponse = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/klk/redeem-approval-response',
      data,
      headerWithToken(token),
    )
  }

  const getProductsRedeemByCoin = async (params = {}) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/redeem-products',
      params,
      headerWithToken(token),
    )
  }

  const getProductRedeemByCoinByUrlKey = async (urlKey) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/klk/redeem-products/${urlKey}`,
      {},
      headerWithToken(token),
    )
  }

  return {
    getCoinsCredit,
    getCoinsExpired,
    getCoinsHistory,
    redeemCoinIntoVoucher,
    getRedemptionApproval,
    getRedemptionApprovalInbox,
    redemptionApprovalResponse,
    getProductsRedeemByCoin,
    getProductRedeemByCoinByUrlKey,
  }
}

export default {
  create,
}
