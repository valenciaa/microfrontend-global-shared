import Cookies from 'js-cookie'
import localforage from 'localforage'
import { fetchingMinicartAuth } from './fetching/GlobalGroup'

const loginWithCart = async (response, paymentCartId, setSuccess) => {
  if (response) {
    // check if customer is guest or not
    const isCustomerGuest = response?.customer?.customer_is_guest > 0
    if (!isCustomerGuest) {
      // login with cart using minicart_id
      console.log('INFO: Initialize login with mini cart')
      fetchingMinicartAuth(
        { minicart_id: response?.minicart_id },
        null,
        null,
        null,
        setSuccess,
      )
    } else {
      // login with payment_cart_id
      if (paymentCartId) {
        console.log('INFO: Initialize Login with payment cart')
        fetchingMinicartAuth(
          { minicart_id: response?.minicart_id },
          null,
          null,
          null,
          setSuccess,
        )
      } else {
        console.log("ERR: There's no payment cart id or mini cart id")
      }
    }
  }
}

const storeDataAfterLoginCart = async (response, handleChangeStateData) => {
  const initMinicartId = await localforage.getItem('minicart_id')
  localforage.setItem('access_token', response.token)
  Cookies.set('access_token', response.token, { expires: 365 })
  handleChangeStateData('access_token', response.token)
  const newResponse = { ...response }
  delete newResponse.token
  const { sha256 } = require('js-sha256')
  const hashUser = newResponse?.customer_id
    ? sha256(newResponse?.customer_id.toString()).toString()
    : ''
  if (Cookies.get('algolia_ssr') !== hashUser && newResponse?.customer_id) {
    if (hashUser) {
      Cookies.set('algolia_ssr', hashUser)
    }
  }
  localforage.setItem(
    'persist:ruparupa',
    JSON.stringify({ auth: JSON.stringify({ user: newResponse }) }),
  )
  handleChangeStateData('auth', { user: newResponse })
  if (newResponse.minicart_id && newResponse.minicart_id !== initMinicartId) {
    await localforage.setItem('minicart_id', newResponse.minicart_id)
    await Cookies.set('minicart_id', newResponse.minicart_id)
    // need to pass page type here later
    console.log('INFO: Finalized login with cart')
  }

  return { user: newResponse, access_token: response.token }
}

const checkAuthUser = (data) => {
  if (data) {
    try {
      const { sha256 } = require('js-sha256')
      const authData = JSON.parse(data)?.auth
      const user = JSON.parse(authData)?.user || null
      if (user) {
        const hashUser = user?.customer_id
          ? sha256(user.customer_id.toString()).toString()
          : ''
        if (Cookies.get('algolia_ssr') !== hashUser) {
          if (hashUser) {
            Cookies.set('algolia_ssr', hashUser)
          }
        }
        return user
      } else {
        return null
      }
    } catch (e) {
      return null
    }
  }
}

export { loginWithCart, storeDataAfterLoginCart, checkAuthUser }
