import { pinAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useManagePin = mutationWrapper(pinAPI.managePin)
