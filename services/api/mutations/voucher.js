import { voucherAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

// Voucher External
export const useRedeemVoucherPartner = mutationWrapper(
  voucherAPI.redeemVoucherPartner,
)
