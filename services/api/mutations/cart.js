import { cartApi } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useCreateInstantCheckout = mutationWrapper(
  cartApi.createInstantCheckout,
)
