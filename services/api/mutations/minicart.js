import { useMutation } from 'react-query'
import localforage from 'localforage'
import Cookies from 'js-cookie'

import { useCartAuthContext } from '../../../context/CartAuthContext'
import { minicartApi } from '../../IndexApi'
import { mergeDefaultAndCustomMutationConfig, mutationWrapper } from '../base'
// import queryClient from '../client'
// import { getMinicartItemsKey } from '../queries/minicart'

export const useUpdateItemCheck = mutationWrapper(minicartApi.updateItemCheck)
export const useUpdateItemQty = mutationWrapper(minicartApi.updateItemQty)
export const useDeleteItems = mutationWrapper(minicartApi.deleteItems)
export const useUpdateItemNote = mutationWrapper(minicartApi.updateItemNote)
// export const useAddMinicartItem = mutationWrapper(minicartApi.postMinicartItem)
export function useAddMinicartItem(config, overrideDefaultConfig) {
  const { handleChangeState } = useCartAuthContext()

  const defaultConfig = {
    onSuccess: (res) => {
      const data = res?.data?.data
      const minicartId = data?.minicart_id
      if (minicartId) {
        if (handleChangeState) {
          handleChangeState('minicart_id', minicartId)
          handleChangeState('cart', data)
          handleChangeState('totalCartQty', data.total_item_qty)
        }
        localforage.setItem('minicart_id', minicartId)
        Cookies.set('minicart_id', minicartId)
      }
      // queryClient.invalidateQueries(getMinicartItemsKey)
    },
  }
  const combinedConfig = mergeDefaultAndCustomMutationConfig(
    defaultConfig,
    config,
    overrideDefaultConfig,
  )
  return useMutation(minicartApi.postMinicartItem, combinedConfig)
}
export const useUpdateItemStore = mutationWrapper(minicartApi.updateItemStore)
export const useAddOrDeleteWishlistItems = mutationWrapper(
  minicartApi.postWishlistItems,
)
export const usePaymentPreprocessing = mutationWrapper(
  minicartApi.paymentPreprocessing,
)
