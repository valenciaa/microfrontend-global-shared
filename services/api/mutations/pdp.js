import { productDetailAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useToggleWishlist = mutationWrapper(
  productDetailAPI.toggleWishlist,
)

export const useLikeReviewRating = mutationWrapper(
  productDetailAPI.likeReviewRating,
)

export const useModifyRegistryItem = mutationWrapper(
  productDetailAPI.modifyRegistryItem,
)
