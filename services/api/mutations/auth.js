import { authAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useKlkLogin = mutationWrapper(authAPI.postLogin)
export const useKlkRegister = mutationWrapper(authAPI.postRegister)
export const useValidateActivateAccount = mutationWrapper(
  authAPI.postValidateActivate,
)
export const useValidateMsStamps = mutationWrapper(authAPI.validateMsStamps)
export const useKlkChangeProfile = mutationWrapper(authAPI.putActivateProfile)
export const useKlkResetPasswordLegacy = mutationWrapper(authAPI.postRequestPin)
export const useKlkMergeAccount = mutationWrapper(authAPI.postMergeAccount)
export const useValidatePartialRegister = mutationWrapper(
  authAPI.postValidatePartialRegister,
)
export const useDecryptAes = mutationWrapper(authAPI.postDecryptAES256)
export const useValidateSocialMediaAuth = mutationWrapper(
  authAPI.validateSocialMediaAuth,
)
export const useKlkMergeAccountV2 = mutationWrapper(authAPI.putMergeAccountV2)
