import { savedPaymentMethodAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useGetCcRedirectUrl = mutationWrapper(
  savedPaymentMethodAPI.getCcRedirectUrl,
)

export const useDeleteSavedCc = mutationWrapper(
  savedPaymentMethodAPI.deleteSavedCc,
)

export const useGopayBind = mutationWrapper(savedPaymentMethodAPI.gopayBind)

export const useGopayErrorLog = mutationWrapper(
  savedPaymentMethodAPI.gopayErrorLog,
)

export const useGopayUnlink = mutationWrapper(savedPaymentMethodAPI.gopayUnlink)
