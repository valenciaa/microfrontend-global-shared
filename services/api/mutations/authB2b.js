import { authB2b } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useQuickRegistrationCheck = mutationWrapper(
  authB2b.quickRegistrationCheck,
)
export const useMergeAccountB2b = mutationWrapper(authB2b.mergeAccount)
export const useGetAdditionalLoginData = mutationWrapper(
  authB2b.getAdditionalLoginData,
)
export const useCheckAuthDataB2b = mutationWrapper(authB2b.checkAuthDataB2b)
export const useLoginB2b = mutationWrapper(authB2b.loginB2b)
export const useRegisterFormB2b = mutationWrapper(authB2b.registerInformaB2b)
export const useGetAccount = mutationWrapper(authB2b.getAccount)
