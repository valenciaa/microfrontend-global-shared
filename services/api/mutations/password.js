import { passwordAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useValidatePassword = mutationWrapper(passwordAPI.validatePassword)
export const useManagePassword = mutationWrapper(
  passwordAPI.manageUpdatePassword,
)
