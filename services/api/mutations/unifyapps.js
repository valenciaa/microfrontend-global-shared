import { unifyAppsApi } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useWarrantyActivation = mutationWrapper(
  unifyAppsApi.activateWarranty,
)
export const useTransactionStatusUpdate = mutationWrapper(
  unifyAppsApi.transactionStatusUpdate,
)
export const useRecreateOrder = mutationWrapper(unifyAppsApi.recreateOrder)
export const useCancelOrder = mutationWrapper(unifyAppsApi.cancelOrder)
export const useDeleteFakturData = mutationWrapper(
  unifyAppsApi.deleteFakturData,
)
export const useAddCompanyData = mutationWrapper(unifyAppsApi.addCompanyData)
export const useAddFakturInvoice = mutationWrapper(
  unifyAppsApi.addFakturInvoice,
)
export const useAddFakturRevision = mutationWrapper(
  unifyAppsApi.postFakturRevision,
)
export const useServiceChargeConfirmation = mutationWrapper(
  unifyAppsApi.postServiceChargeConfirmation,
)
export const usePutVoucherRefund = mutationWrapper(
  unifyAppsApi.putVoucherRefund,
)
export const usePostReviewRating = mutationWrapper(unifyAppsApi.postReview)
export const usePostReviewRatingAverage = mutationWrapper(
  unifyAppsApi.postReviewAverage,
)
export const usePostBankAccOtp = mutationWrapper(unifyAppsApi.postBankAccOtp)
export const useAddBankAccount = mutationWrapper(unifyAppsApi.addBankAccount)
// Instalasi 3.0
export const useCancelInstallation = mutationWrapper(
  unifyAppsApi.cancelInstallation,
)
export const useRescheduleInstallation = mutationWrapper(
  unifyAppsApi.rescheduleInstallation,
)
export const useDeleteAccountAddress = mutationWrapper(
  unifyAppsApi.deleteAddress,
)
export const useMakePrimaryAccountAddress = mutationWrapper(
  unifyAppsApi.makePrimaryAddress,
)
export const useCreateInstallationAfterOrder = mutationWrapper(
  unifyAppsApi.createInstallationAfterOrder,
)

// Improvement Review Rating
export const usePostSingleProductReview = mutationWrapper(
  unifyAppsApi.postSingleProductReview,
)
export const usePostBulkProductReview = mutationWrapper(
  unifyAppsApi.postBulkProductReview,
)
export const usePostServiceReview = mutationWrapper(
  unifyAppsApi.postServiceReview,
)

export const useUpdateCompanyData = mutationWrapper(
  unifyAppsApi.updateCompanyData,
)
