import { wishlistAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useRefreshWishlistItem = mutationWrapper(
  wishlistAPI.refreshWishlistItem,
)

export const useDeleteWishlistItem = mutationWrapper(
  wishlistAPI.deleteWishlistItem,
)
