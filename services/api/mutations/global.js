import Cookies from 'js-cookie'
import localforage from 'localforage'
import { useMutation } from 'react-query'
import { useCartAuthContext } from '../../../context/CartAuthContext'
import { globalAPI } from '../../IndexApi'
import { mergeDefaultAndCustomMutationConfig, mutationWrapper } from '../base'
import queryClient from '../client'
import { getMinicartKey } from '../queries/global'

export const useSubscribeNewsLetter = mutationWrapper(
  globalAPI.newsletterSubscribe,
)
// use this format only if you need to access context/hooks inside
export function useUpdateMinicart(config, overrideDefaultConfig) {
  const { handleChangeState } = useCartAuthContext()

  const defaultConfig = {
    onSuccess: (res) => {
      const data = res?.data?.data
      const minicartId = data?.minicart_id
      if (minicartId) {
        handleChangeState('minicart_id', minicartId)
        localforage.setItem('minicart_id', minicartId)
        Cookies.set('minicart_id', minicartId)
      }
      queryClient.invalidateQueries(getMinicartKey)
    },
  }
  const combinedConfig = mergeDefaultAndCustomMutationConfig(
    defaultConfig,
    config,
    overrideDefaultConfig,
  )
  return useMutation(globalAPI.updateCart, combinedConfig)
}

export const useDeleteMinicart = mutationWrapper(globalAPI.deleteCartItem, {
  onSuccess: () => {
    queryClient.invalidateQueries(getMinicartKey)
  },
})
export const useCheckAuthData = mutationWrapper(globalAPI.checkAuthData)
export const useLogin = mutationWrapper(globalAPI.login)
export const useMarkInboxAsRead = mutationWrapper(globalAPI.markInboxAsRead)
export const useUploadReviewImage = mutationWrapper(globalAPI.uploadReviewImage)
export const useuploadMultipleReviewImages = mutationWrapper(
  globalAPI.uploadMultipleReviewImages,
)
export const useSubmitReview = mutationWrapper(globalAPI.submitReview)
export const useSubscribeDownloadMobileApps = mutationWrapper(
  globalAPI.downloadMobileAppsSubscribe,
)
export const useCreateLog = mutationWrapper(globalAPI.createLog)
export const useUploadFileB2b = mutationWrapper(globalAPI.uploadFileB2b)
export const useVerifyEmailOrPhone = mutationWrapper(
  globalAPI.verifyEmailOrPhone,
)
export const useSendTrackEvent = mutationWrapper(globalAPI.sendTrackEvent)
export const useGetVueRecommend = mutationWrapper(globalAPI.getVueRecommend)
export const usePostQuotation = mutationWrapper(globalAPI.postQuotation)

export const useUpdateBankAccount = mutationWrapper(
  globalAPI.setUserBankAccount,
)

export const useDeleteAccount = mutationWrapper(globalAPI.deleteAccount)
export const useMembershipMergeMembership = mutationWrapper(
  globalAPI.membershipMergeMembership,
)
export const useMembershipRequestPin = mutationWrapper(
  globalAPI.membershipRequestPin,
)
export const useGetFreshchatRestoreId = mutationWrapper(
  globalAPI.getFreshchatRestoreId,
)
export const usePostFreshchatRestoreId = mutationWrapper(
  globalAPI.postFreshchatRestoreId,
)
export const usePutVoucherRefundDc = mutationWrapper(globalAPI.makeVoucherDc)

export const useReviewArticle = mutationWrapper(globalAPI.insertReviewArticle)

export const usePostEmailCustomFurniture = mutationWrapper(
  globalAPI.postEmailCustomFurniture,
)

// Misc
export const useSearchSugestionKecamatan = mutationWrapper(
  globalAPI.postSearchSuggestionKecamatan,
)
export const useCreateAddress = mutationWrapper(globalAPI.createAddress)
export const useUpdateAddress = mutationWrapper(globalAPI.updateAddress)

export const useValidateAddress = mutationWrapper(globalAPI.validateAddress)

export const useGoogleMapsPlacesAutocomplete = mutationWrapper(
  globalAPI.googleMapsPlacesAutocomplete,
)

export const useGoogleMapsPlacesDetail = mutationWrapper(
  globalAPI.googleMapsPlacesDetail,
)

export const useSearchByKelurahan = (config) => {
  return useMutation(globalAPI.getSearchByKelurahan, config)
}
