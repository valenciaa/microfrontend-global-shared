import { coinAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useRedeemCoinIntoVoucher = mutationWrapper(
  coinAPI.redeemCoinIntoVoucher,
)
export const useRedemptionApprovalResponse = mutationWrapper(
  coinAPI.redemptionApprovalResponse,
)
