import { profileAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useUpdateProfileDetail = mutationWrapper(
  profileAPI.updateProfileDetail,
)
export const useUpdateAndVerifyEmailOrPhone = mutationWrapper(
  profileAPI.updateAndVerifyEmailOrPhone,
)

export const useGetPointInforma = mutationWrapper(
  profileAPI.getPointInforma
)

export const useTukarVoucherInforma = mutationWrapper(
  profileAPI.tukarVoucherInforma
)

export const useDeleteAccount = mutationWrapper(profileAPI.deleteAccount)
