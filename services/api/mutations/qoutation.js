import { quotationAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useCreateQuotationUserV2 = mutationWrapper(
  quotationAPI.postQuotationUserV2,
)
export const useCreateQuotationProductsV2 = mutationWrapper(
  quotationAPI.postQuotationProductsV2,
)
export const useRequestProformaInvoice = mutationWrapper(
  quotationAPI.requestProformaInvoice,
)
export const useShareQuotationEmailV2 = mutationWrapper(
  quotationAPI.shareQuotationeEmailV2,
)
