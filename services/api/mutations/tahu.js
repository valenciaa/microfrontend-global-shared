import { tahuApi } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useInsertReviewFaq = mutationWrapper(
  tahuApi.insertReviewArticlesContent,
)
