import { otpAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useManageOtp = mutationWrapper(otpAPI.manageOtp)
export const useGenerateOTP = mutationWrapper(otpAPI.generateOTP)
export const useValidateOTP = mutationWrapper(otpAPI.validateOTP)
export const useStatusMembership = mutationWrapper(otpAPI.statusMembership)
