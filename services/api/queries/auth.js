import { useQuery } from 'react-query'
import { authAPI } from '../../IndexApi'
import queryWrapper from '../base'

// ? Check status user -> login, register atau activate
export const getMembership = 'getMembership'
export function useGetMembership(params, options) {
  return useQuery(
    [getMembership, params],
    queryWrapper(authAPI.getIdentityInfo),
    options,
  )
}

// ? Get all duplicated accounts when merging accounts
export const getDuplicateAccounts = 'getDuplicateAccounts'
export function useGetDuplicateAccounts(params, options) {
  return useQuery(
    [getDuplicateAccounts, params],
    queryWrapper(authAPI.getDuplicateAccounts),
    options,
  )
}

// ? Check apakah email dan phone keduplicate
export const getCheckDuplicate = 'getCheckDuplicate'
export function useGetCheckDuplicate(params, options) {
  return useQuery(
    [getCheckDuplicate, params],
    queryWrapper(authAPI.getCheckDuplicate),
    options,
  )
}

// ? Get all available passkey choices with token member
export const getPasskeyChoices = 'getPasskeyChoices'
export function useGetPasskeyChoices(params, options) {
  return useQuery(
    [getPasskeyChoices, params],
    queryWrapper(authAPI.getPasskeyChoices),
    options,
  )
}

// ? Get all available passkey choices with token auth
export const getPasskeyChoicesMembership = 'getPasskeyChoicesMembership'
export function useGetPasskeyChoicesMembership(params, options) {
  return useQuery(
    [getPasskeyChoicesMembership, params],
    queryWrapper(authAPI.getPasskeyChoicesMembership),
    options,
  )
}

// ? Get all available passkey choices with token auth
export const getSocialLoginUrl = 'getSocialLoginUrl'
export function useGetSocialLoginUrl(params, options) {
  return useQuery(
    [getPasskeyChoicesMembership, params],
    queryWrapper(authAPI.getSocialLoginUrl),
    options,
  )
}

// ? Get all legacy member by identifier
export const getLegacyMembersByIdentifierV2 = 'getLegacyMembersByIdentifierV2'
export function useGetLegacyMembersByIdentifierV2(params, options) {
  return useQuery(
    [getLegacyMembersByIdentifierV2, params],
    queryWrapper(authAPI.getLegacyMembersByIdentifierV2),
    options,
  )
}
