import { useQuery } from 'react-query'
import { membershipAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getDedicatedMemberTier = 'getDedicatedMemberTier'
export function useGetDedicatedMemberTier(params, options) {
  return useQuery(
    [getDedicatedMemberTier, params],
    queryWrapper(membershipAPI.getDedicatedMemberTier),
    options,
  )
}
