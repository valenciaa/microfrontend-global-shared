import { useQuery } from 'react-query'
import queryWrapper from '../base'
import { quotationAPI } from '../../IndexApi'

export const getIndustrySectors = 'useGetIndustrySectors'
export function useGetIndustrySectors(options) {
  return useQuery(
    [getIndustrySectors],
    queryWrapper(quotationAPI.getIndustrySectors),
    options,
  )
}
