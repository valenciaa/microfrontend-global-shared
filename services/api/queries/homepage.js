import { useQuery } from 'react-query'
import { homepageAPI, hyperAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getIdeaInspirationsKey = 'getIdeaInspirations'
export function useGetIdeaInspirations(params, options) {
  return useQuery(
    [getIdeaInspirationsKey, params],
    queryWrapper(homepageAPI.getIdeaInspirations),
    options,
  )
}

export const getEventKey = 'getEvent'
export function useGetEvent(params, options) {
  return useQuery(
    [getEventKey, params],
    queryWrapper(homepageAPI.getEventDetail),
    options,
  )
}

export const getCmsBlockDetailKey = 'getCmsBlockDetail'
export function useGetCmsBlockDetail(params, options) {
  return useQuery(
    [getCmsBlockDetailKey, params],
    queryWrapper(homepageAPI.getCmsBlockDetail),
    options,
  )
}

export const getCmsBlockDetailKeyV2 = 'getCmsBlockDetailV2'
export function useGetCmsBlockDetailV2(params, options) {
  return useQuery(
    [getCmsBlockDetailKeyV2, params],
    queryWrapper(homepageAPI.getCmsBlockDetailV2),
    options,
  )
}

export const getTahuWordingContent = 'getTahuWordingContent'
export function useGetTahuWordingContent(params, options) {
  return useQuery(
    [getTahuWordingContent, params],
    queryWrapper(homepageAPI.wordingContent),
    options,
  )
}

export const getCmsReq = 'getCmsReq'
export function useGetCmsReq(params, options) {
  return useQuery(
    [getCmsReq, params],
    queryWrapper(homepageAPI.b2bCmsRequest),
    options,
  )
}

export const getHyperPersonalizeKey = 'getHyperPersonalize'
export function useGetHyperPersonalize(type, cmsTmp, token, menu_id, options) {
  return useQuery(
    [getHyperPersonalizeKey, type, cmsTmp, token, menu_id],
    queryWrapper(hyperAPI.hyperPersonalized),
    options,
  )
}

export const getPersonalisedInvoiceReviewKey = 'getPersonalisedInvoiceReview'
export function useGetPersonalisedInvoiceReview(options) {
  return useQuery(
    getPersonalisedInvoiceReviewKey,
    queryWrapper(homepageAPI.getPersonalizedInvoiceReview),
    options,
  )
}
