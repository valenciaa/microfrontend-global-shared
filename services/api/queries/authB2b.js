import { useQuery } from 'react-query'
import { authB2b } from '../../IndexApi'
import queryWrapper from '../base'

//* B2B
// ? Get all available account b2b for switch account
export const getListAccountsB2b = 'getListAccountsB2b'
export function useGetAccountSwitchBu(params, options) {
  return useQuery(
    [getListAccountsB2b, params],
    queryWrapper(authB2b.getListAccountsB2b),
    options,
  )
}

export const getUserDataB2b = 'getUserDataB2b'
export function useGetUserDataB2b(params, options) {
  return useQuery(
    [getUserDataB2b, params],
    queryWrapper(authB2b.getAccount),
    options,
  )
}
