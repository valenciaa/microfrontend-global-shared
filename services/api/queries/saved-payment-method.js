import { useQuery } from 'react-query'
import queryWrapper from '../base'
import { savedPaymentMethodAPI } from '../../IndexApi'

export const getSavedCcListKey = 'getSavedCcListKey'

export function useGetSavedCcList(params, options) {
  return useQuery(
    [getSavedCcListKey, params],
    queryWrapper(savedPaymentMethodAPI.getSavedCcList),
    options,
  )
}

export const gopayInitKey = 'gopayInitKey'

export function useGopayInit(params, options) {
  return useQuery(
    [gopayInitKey, params],
    queryWrapper(savedPaymentMethodAPI.gopayInit, (response) => response),
    options,
  )
}

export const getGopayAccountKey = 'gopayAccountKey'

export function useGetGopayAccount(params, options) {
  return useQuery(
    [getGopayAccountKey, params],
    queryWrapper(savedPaymentMethodAPI.getGopayAccount, (response) => response),
    options,
  )
}
