import { useQuery } from 'react-query'
import queryWrapper from '../base'
import { bankAccountAPI } from '../../IndexApi'

export const getRefundHistoryKey = 'getRefundHistory'
export function useGetRefundHistory(params, options) {
  return useQuery(
    [getRefundHistoryKey, params],
    queryWrapper(bankAccountAPI.getRefundHistory),
    options,
  )
}

export const getUserBankAccountByAccountNumberKey =
  'getUserBankAccountByAccountNumber'
export function useGetUserBankAccountByAccountNumber(params, options) {
  return useQuery(
    [getUserBankAccountByAccountNumberKey, params],
    queryWrapper(bankAccountAPI.getUserBankAccountByAccountNumber),
    options,
  )
}
