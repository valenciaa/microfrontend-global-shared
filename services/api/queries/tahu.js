import { useQuery } from 'react-query'
import { tahuApi } from '../../IndexApi'
import queryWrapper from '../base'

export const getTahuComponent = 'getTahuComponent'
export function useGetTahuComponent(params, options) {
  return useQuery(
    [getTahuComponent, params],
    queryWrapper(tahuApi.getTahuCampaign),
    options,
  )
}

export const getCategoriesContent = 'getCategoriesContent'
export function useGetCategoriesContent(params, options) {
  return useQuery(
    [getCategoriesContent, params],
    queryWrapper(tahuApi.getCategoriesContent),
    options,
  )
}

export const getArticlesContent = 'getArticlesContent'
export function useGetArticlesContent(params, options) {
  return useQuery(
    [getArticlesContent, params],
    queryWrapper(tahuApi.getArticlesContent),
    options,
  )
}

export const getArticleCategoriesContent = 'getArticleCategoriesContent'
export function useGetArticleCategoriesContent(params, options) {
  return useQuery(
    [getArticleCategoriesContent, params],
    queryWrapper(tahuApi.getArticleCategoriesContent),
    options,
  )
}

export const getReviewArticlesContent = 'getReviewArticlesContent'
export function useGetReviewArticlesContent(params, options) {
  return useQuery(
    [getReviewArticlesContent, params],
    queryWrapper(tahuApi.getReviewArticlesContent),
    options,
  )
}

export const getSubCategory = 'getSubCategory'
export function useGetSubCategory(params, options) {
  return useQuery(
    [getSubCategory, params],
    queryWrapper(tahuApi.getSubCategory),
    options,
  )
}

export const getPopularTopic = 'getPopularTopic'
export function useGetPopularTopic(params, options) {
  return useQuery(
    [getPopularTopic, params],
    queryWrapper(tahuApi.getPopularTopic),
    options,
  )
}

export const getReviewAccessibility = 'getReviewAccessibility'
export function useGetReviewAccessibility(params, options) {
  return useQuery(
    [getReviewAccessibility, params],
    queryWrapper(tahuApi.reviewAccessibility),
    options,
  )
}

export const getDetailArticle = 'getDetailArticle'

export function useGetDetailArticle(data, options) {
  return useQuery(
    [getDetailArticle, data],
    queryWrapper(tahuApi.getDetailArticle),
    options,
  )
}