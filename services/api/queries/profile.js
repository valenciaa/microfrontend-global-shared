import { useQuery } from 'react-query'
import { profileAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getCustomerMembership = 'getCustomerMembership'
export function useGetCustomerMembership(params, options) {
  return useQuery(
    [getCustomerMembership, params],
    queryWrapper(profileAPI.getCustomerMembershipProfile),
    options,
  )
}

export const getProfileDetail = 'getProfileDetail'
export function useGetProfileDetail(params, options) {
  return useQuery(
    [getProfileDetail, params],
    queryWrapper(profileAPI.getProfileDetail),
    options,
  )
}

export const getDeleteAccountReasons = 'getDeleteAccountReasons'
export function useGetDeleteAccountReasons(params, options) {
  return useQuery(
    [getDeleteAccountReasons, params],
    queryWrapper(profileAPI.getDeleteAccountReasons),
    options,
  )
}

export const getHobbiesList = 'getHobbiesList'
export function useGetHobbiesList(params, options) {
  return useQuery(
    [getHobbiesList, params],
    queryWrapper(profileAPI.getHobbiesList),
    options,
  )
}

export const getPetTypesList = 'getPetTypesList'
export function useGetPetTypesList(params, options) {
  return useQuery(
    [getPetTypesList, params],
    queryWrapper(profileAPI.getPetTypesList),
    options,
  )
}
export const getMembershipInforma = 'getMembershipInforma'
export function useGetMembershipInforma(params, options) {
  return useQuery(
    [getMembershipInforma, params],
    queryWrapper(profileAPI.getMembershipInforma),
    options,
  )
}

