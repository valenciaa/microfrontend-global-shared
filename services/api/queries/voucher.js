import { useQuery } from 'react-query'
import { globalAPI, voucherAPI } from '../../IndexApi'
import queryWrapper from '../base'

// Voucher Wallet
export const getVouchers = 'getVouchers'
export function useGetVouchers(params, options) {
  return useQuery(
    [getVouchers, params],
    queryWrapper(globalAPI.getVouchers),
    options,
  )
}

// Voucher External
export const getVoucherPartnerDetail = 'getVoucherPartnerDetail'
export function useGetVoucherPartnerDetail(params, options) {
  return useQuery(
    [getVoucherPartnerDetail, params],
    queryWrapper(voucherAPI.getVoucherPartnerDetail),
    options,
  )
}
