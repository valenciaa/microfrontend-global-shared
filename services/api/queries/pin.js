import { useQuery } from 'react-query'
import { pinAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getPinStatus = 'getPinStatus'
export function useGetPinStatus(params, options) {
  return useQuery(
    [getPinStatus, params],
    queryWrapper(pinAPI.getPinStatus),
    options,
  )
}
