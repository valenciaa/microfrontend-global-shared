import { useQuery } from 'react-query'
import { otpAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getOtpChoices = 'getOtpChoices'
export function useGetOtpChoices(params, options) {
  return useQuery(
    [getOtpChoices, params],
    queryWrapper(otpAPI.getOtpChoices),
    options,
  )
}
