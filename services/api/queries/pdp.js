import { useQuery } from 'react-query'
import { productDetailAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getBankInstallment = 'useGetBankInstallment'
export function useGetBankInstallment(params, options) {
  return useQuery(
    [getBankInstallment, params],
    queryWrapper(productDetailAPI.getBankInstallment),
    options,
  )
}

export const getKredivoResults = 'useGetKredivoResults'
export function useGetKredivoResults(params, options) {
  return useQuery(
    [getKredivoResults, params],
    queryWrapper(productDetailAPI.kredivoCalculator),
    options,
  )
}

export const getDanakiniResults = 'useGetDanakiniResults'
export function useGetDanakiniResults(params, options) {
  return useQuery(
    [getDanakiniResults, params],
    queryWrapper(productDetailAPI.danakiniSimulation),
    options,
  )
}

export const getVoucherPDPResults = 'useGetVoucherPDPResults'
export function useGetVoucherPDPResults(params, options) {
  return useQuery(
    [getVoucherPDPResults, params],
    queryWrapper(productDetailAPI.getVoucherPDP),
    options,
  )
}

export const getTncPdp = 'useGetTncPdp'
export function useGetTncPdp(params, options) {
  return useQuery(
    [useGetTncPdp, params],
    queryWrapper(productDetailAPI.getTncPdp),
    options,
  )
}

export const getProvinceKey = 'useGetProvince'
export function useGetProvince(options) {
  return useQuery(
    getProvinceKey,
    queryWrapper(productDetailAPI.getProvince),
    options,
  )
}

export const getCityKey = 'useGetCity'
export function useGetCity(params, options) {
  return useQuery(
    [getCityKey, params],
    queryWrapper(productDetailAPI.getCity),
    options,
  )
}

export const getKecamatanKey = 'useGetKecamatan'
export function useGetKecamatan(params, options) {
  return useQuery(
    [getKecamatanKey, params],
    queryWrapper(productDetailAPI.getKecamatan),
    options,
  )
}

export const getExpressDeliveryKey = 'useGetExpressDelivery'
export function useGetExpressDelivery(params, options) {
  return useQuery(
    [getExpressDeliveryKey, params],
    queryWrapper(productDetailAPI.getExpressDelivery),
    options,
  )
}

export const getProductCanDeliveryKey = 'useGetProductCanDelivery'
export function useGetProductCanDelivery(params, options) {
  return useQuery(
    [getProductCanDeliveryKey, params],
    queryWrapper(productDetailAPI.getProductCanDelivery),
    options,
  )
}

export const getProductStockKey = 'useGetProductStock'
export function useGetProductStock(params, options) {
  return useQuery(
    [getProductStockKey, params],
    queryWrapper(productDetailAPI.getProductStock),
    options,
  )
}

export const getProductMaxStockKey = 'useGetProductMaxStock'
export function useGetProductMaxStock(params, options) {
  return useQuery(
    [getProductMaxStockKey, params],
    queryWrapper(productDetailAPI.getProductMaxStock),
    options,
  )
}

export const getWordingContentKey = 'getWordingContent'
export function useGetWordingContentKey(params, options) {
  return useQuery(
    [getWordingContentKey, params],
    queryWrapper(productDetailAPI.getWordingContent),
    options,
  )
}

export const getAddressListKey = 'getAddressList'
export function useGetAddressList(options) {
  return useQuery(
    [getAddressListKey],
    queryWrapper(productDetailAPI.getAddress, (res) => {
      return res.data.data
    }),
    options,
  )
}

export const getReviewRatingResults = 'useGetReviewRatingResults'
export function useGetReviewRatingResults(params, options) {
  return useQuery(
    [getReviewRatingResults, params],
    queryWrapper(productDetailAPI.getReviewRatingPDPNoToken),
    options,
  )
}

export const getReviewRatingImagesResults = 'useGetReviewRatingImagesResults'
export function useGetReviewRatingImagesResults(params, options) {
  return useQuery(
    [getReviewRatingImagesResults, params],
    queryWrapper(productDetailAPI.getReviewRatingImages),
    options,
  )
}

export const getProductPriceZoneKey = 'useGetProductPriceZone'
export function useGetProductPriceZone(params, options) {
  return useQuery(
    [getProductPriceZoneKey, params],
    queryWrapper(productDetailAPI.getProductPriceZone),
    options,
  )
}

export const getProductGroupKey = 'useGetProductGroup'
export function useGetProductGroup(params, options) {
  return useQuery(
    [getProductGroupKey, params],
    queryWrapper(productDetailAPI.getProductGroup),
    options,
  )
}

export const getProductDetailKey = 'useGetProductDetail'
export function useGetProductDetail(params, options) {
  return useQuery(
    [getProductDetailKey, params],
    queryWrapper(productDetailAPI.getProductDetail),
    options,
  )
}

export const getVouchersKey = 'getVouchers'
export function useGetVoucher(params, options) {
  return useQuery(
    [getVouchersKey, params],
    queryWrapper(productDetailAPI.getVouchers, options),
  )
}

export const getRegistryListKey = 'useGetRegistryList'
export function useGetRegistryList(params) {
  return useQuery(
    [getRegistryListKey, params],
    queryWrapper(productDetailAPI.getRegistryList),
  )
}

export const getProductBundlingKey = 'useGetProductBundling'
export function useGetProductBundling(params, options) {
  return useQuery(
    [getProductBundlingKey, params],
    queryWrapper(productDetailAPI.getProductBundling),
    options,
  )
}

export const getProductBundlingDetailKey = 'useGetProductBundlingDetail'
export function useGetProductBundlingDetail(params, options) {
  return useQuery(
    [getProductBundlingDetailKey, params],
    queryWrapper(productDetailAPI.getProductBundlingDetail),
    options,
  )
}

export const getTotalClickAffiliateKey = 'getTotalClickAffiliateKey'
export function useGetTotalClickAffiliate(params, options) {
  return useQuery(
    [getTotalClickAffiliateKey, params],
    queryWrapper(productDetailAPI.getTotalClickAffiliate),
    options,
  )
}

export const getVirtualShowRoomPDP = 'getVirtualShowRoomPDP'
export function useGetVirtualShowRoomPDP(params, options) {
  return useQuery(
    [getVirtualShowRoomPDP, params],
    queryWrapper(productDetailAPI?.getVirtualShowRoomPDP),
    options,
  )
}

export const getBrandUrlKey = 'getBrandUrlKey'
export function useGetBrandUrlKey(params, options) {
  return useQuery(
    [getBrandUrlKey, params],
    queryWrapper(productDetailAPI.getBrandUrlKey),
    options,
  )
}
