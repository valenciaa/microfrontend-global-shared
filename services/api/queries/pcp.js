import { useQuery } from 'react-query'
import { hyperAPI, pcpAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getProducts = 'getProducts'
export function useGetProducts(params, options) {
  return useQuery(
    [getProducts, params],
    queryWrapper(pcpAPI.getProducts),
    options,
  )
}

export const getMostSalesRecommend = 'getMostSalesRecommend'
export function useGetMostSalesRecommend(params, options) {
  return useQuery(
    [getMostSalesRecommend, params],
    queryWrapper(pcpAPI.mostSalesRecommendation),
    options,
  )
}

export const getStoreKey = 'getStore'
export function useGetStore(params, options) {
  return useQuery([getStoreKey, params], queryWrapper(pcpAPI.getStore), options)
}

export const getStoreByCityKey = 'getStoreByCity'
export function useGetStoreByCity(params, options) {
  return useQuery(
    [getStoreByCityKey, params],
    queryWrapper(pcpAPI.getStoreByCity),
    options,
  )
}

export const getPersonalizedQuickFilter = 'getPersonalizedQuickFilter'
export function useGetPersonalizedQuickFilter(options) {
  return useQuery(
    [getPersonalizedQuickFilter],
    queryWrapper(hyperAPI.PersonalizedQuickFilter),
    options,
  )
}
