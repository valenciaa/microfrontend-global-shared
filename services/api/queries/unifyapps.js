import { useQuery } from 'react-query'
import { unifyAppsApi } from '../../IndexApi'
import queryWrapper from '../base'

export const getWarrantyList = 'useGetWarrantyList'
export function useGetWarrantyList(params, options) {
  return useQuery(
    [getWarrantyList, params],
    queryWrapper(unifyAppsApi.getWarrantyList),
    options,
  )
}

export const getWarrantyDetail = 'useGetWarrantyDetail'
export function useGetWarrantyDetail(params, options) {
  return useQuery(
    [getWarrantyDetail, params],
    queryWrapper(unifyAppsApi.getWarrantyDetail),
    options,
  )
}

export const getTransactionDetail = 'useGetTransactionDetail'
export function useGetTransactionDetail(params, options) {
  return useQuery(
    [getTransactionDetail, params],
    queryWrapper(unifyAppsApi.getTransactionDetail),
    options,
  )
}

export const getTransactionList = 'useGetTransactionList'
export function useGetTransactionList(params, options) {
  return useQuery(
    [getTransactionList, params],
    queryWrapper(unifyAppsApi.getTransactionList),
    options,
  )
}

export const getTransactionListWithCache = 'useGetTransactionListWithCache'
export function useGetTransactionListWithCache(params, options) {
  return useQuery(
    [getTransactionListWithCache, params],
    queryWrapper(unifyAppsApi.getTransactionListWithCache),
    options,
  )
}

export const getTransactionFilter = 'useGetTransactionFilter'
export function useGetTransactionFilter(options) {
  return useQuery(
    [getTransactionFilter],
    queryWrapper(unifyAppsApi.getTransactionFilter),
    options,
  )
}

export const getWaitingPaymentList = 'useGetWaitingPaymentList'
export function useGetWaitingPaymentList(params, options) {
  return useQuery(
    [getWaitingPaymentList, params],
    queryWrapper(unifyAppsApi.getWaitingPaymentList),
    options,
  )
}

export const getHowToPay = 'useHowToPay'
export function useGetHowToPay(params, options) {
  return useQuery(
    [getHowToPay, params],
    queryWrapper(unifyAppsApi.getHowToPay),
    options,
  )
}

export const getWaitingPaymentDetail = 'useGetWaitingPaymentDetail'
export function useGetWaitingPaymentDetail(params, options) {
  return useQuery(
    [getWaitingPaymentDetail, params],
    queryWrapper(unifyAppsApi.getWaitingPaymentDetail),
    options,
  )
}
export const getFakturStatus = 'useGetFakturStatus'
export function useGetFakturStatus(params, options) {
  return useQuery(
    [getFakturStatus, params],
    queryWrapper(unifyAppsApi.getFakturStatus),
    options,
  )
}

export const getFakturData = 'useGetFakturData'
export function useGetFakturData(params, options) {
  return useQuery(
    [getFakturData, params],
    queryWrapper(unifyAppsApi.getFakturData),
    options,
  )
}

export const getDownloadFaktur = 'useGetDownloadFaktur'
export function useGetDownloadFaktur(params, options) {
  return useQuery(
    [getDownloadFaktur, params],
    queryWrapper(unifyAppsApi.downloadFaktur),
    options,
  )
}

export const getReviewCount = 'useGetReviewCount'
export function useGetReviewCount(params, options) {
  return useQuery(
    [useGetReviewCount, params],
    queryWrapper(unifyAppsApi.getReviewCount),
    options,
  )
}

export const getReviewList = 'useGetReviewList'
export function useGetReviewList(params, options) {
  return useQuery(
    [useGetReviewList, params],
    queryWrapper(unifyAppsApi.getReviewList),
    options,
  )
}

// Improvement Review Rating
export const getReviewDetailProduct = 'useGetReviewDetailProduct'
export function useGetReviewDetailProduct(params, options) {
  return useQuery(
    [useGetReviewDetailProduct, params],
    queryWrapper(unifyAppsApi.getReviewDetailProduct),
    options,
  )
}

export const getReviewDetailService = 'useGetReviewDetailService'
export function useGetReviewDetailService(params, options) {
  return useQuery(
    [useGetReviewDetailService, params],
    queryWrapper(unifyAppsApi.getReviewDetailService),
    options,
  )
}

// Improvement Review Rating

export const getServiceDetail = 'useGetServiceDetail'
export function useGetServiceDetail(params, options) {
  return useQuery(
    [getServiceDetail, params],
    queryWrapper(unifyAppsApi.getServiceDetail),
    options,
  )
}

export const getServiceDetailListOnline = 'useGetServiceDetailListOnline'
export function useGetServiceDetailListOnline(params, options) {
  return useQuery(
    [getServiceDetailListOnline, params],
    queryWrapper(unifyAppsApi.getServiceListOnline),
    options,
  )
}

export const getServiceDetailListOffline = 'useGetServiceDetailListOffline'
export function useGetServiceDetailListOffline(params, options) {
  return useQuery(
    [getServiceDetailListOffline, params],
    queryWrapper(unifyAppsApi.getServiceListOffline),
    options,
  )
}

// Instalasi 3.0 Start
export const getInstallationItemList = 'getInstallationItemList'
export function useGetInstallationItemList(params, options) {
  return useQuery(
    [getInstallationItemList, params],
    queryWrapper(unifyAppsApi.getInstallationItemList),
    options,
  )
}

export const getInstallationItemDetail = 'getInstallationItemDetail'
export function useGetInstallationItemDetail(params, options) {
  return useQuery(
    [getInstallationItemDetail, params],
    queryWrapper(unifyAppsApi.getInstallationItemDetail),
    options,
  )
}

export const getOfflineInstallationItemDetail =
  'getOfflineInstallationItemDetail'
export function useGetOfflineInstallationItemDetail(params, options) {
  return useQuery(
    [getOfflineInstallationItemDetail, params],
    queryWrapper(unifyAppsApi.getOfflineInstallationItemDetail),
    options,
  )
}

export const getOfflineInstallationReceipt = 'getOfflineInstallationReceipt'
export function useGetOfflineInstallationReceipt(params, options) {
  return useQuery(
    [getOfflineInstallationReceipt, params],
    queryWrapper(unifyAppsApi.getOfflineInstallationReceipt),
    options,
  )
}

export const getItemNotInstalled = 'getItemNotInstalled'
export function useGetItemNotInstalled(params, options) {
  return useQuery(
    [getItemNotInstalled, params],
    queryWrapper(unifyAppsApi.getItemNotInstalled),
    options,
  )
}

export const getPaymentMethodHistory = 'getPaymentMethodHistory'
export function useGetPaymentMethodHistory(params, options) {
  return useQuery(
    [getPaymentMethodHistory, params],
    queryWrapper(unifyAppsApi.getPaymentMethodHistory),
    options,
  )
}

export const getRefundHistory = 'getRefundHistory'
export function useGetRefundHistory(params, options) {
  return useQuery(
    [getRefundHistory, params],
    queryWrapper(unifyAppsApi.getRefundHistory),
    options,
  )
}
// Instalasi 3.0 End
export const getBankList = 'useGetBankList'
export function useGetBankList(params, options) {
  return useQuery(
    [getBankList, params],
    queryWrapper(unifyAppsApi.getBankList),
    options,
  )
}
