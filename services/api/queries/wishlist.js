import { useQuery } from 'react-query'
import queryWrapper from '../base'
import { wishlistAPI } from '../../IndexApi'

export const getWishlistSortValueKey = 'useGetWishlistSortValue'
export function useGetWishlistSortValue(options) {
  return useQuery(
    [getWishlistSortValueKey],
    queryWrapper(wishlistAPI.getWishlistSortValue),
    options,
  )
}
