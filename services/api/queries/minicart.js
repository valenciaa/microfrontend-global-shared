import Cookies from 'js-cookie'
import localforage from 'localforage'
import { useQuery } from 'react-query'
import { useCartAuthContext } from '../../../context/CartAuthContext'
import { minicartApi } from '../../IndexApi'
import queryWrapper, { mergeDefaultAndCustomMutationConfig } from '../base'

export const getMinicartItemsKey = 'useGetMinicartItems'
export function useGetMinicartItems(params, config, overrideDefaultConfig) {
  const { handleChangeState } = useCartAuthContext()

  const defaultConfig = {
    onSuccess: (res) => {
      const minicartId = res?.minicart_id

      const minicartToken = res?.minicart_token
      if (minicartId) {
        if (handleChangeState) {
          handleChangeState('minicart_id', minicartId)

          window?.sessionStorage?.setItem('minicart_token', minicartToken)
        }
        localforage.setItem('minicart_id', minicartId)
        Cookies.set('minicart_id', minicartId)
      }
    },
  }
  const combinedConfig = mergeDefaultAndCustomMutationConfig(
    defaultConfig,
    config,
    overrideDefaultConfig,
  )
  return useQuery(
    [getMinicartItemsKey, params],
    queryWrapper(minicartApi.getMinicartItems),
    combinedConfig,
  )
}
