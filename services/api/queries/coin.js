import { useQuery } from 'react-query'
import { coinAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getCoinsCredit = 'getCoinsCredit'
export function useGetCoinsCredit(params, options) {
  return useQuery(
    [getCoinsCredit, params],
    queryWrapper(coinAPI.getCoinsCredit),
    options,
  )
}

export const getCoinsExpired = 'getCoinsExpired'
export function useGetCoinsExpired(params, options) {
  return useQuery(
    [getCoinsExpired, params],
    queryWrapper(coinAPI.getCoinsExpired),
    options,
  )
}

export const getCoinsHistory = 'getCoinsHistory'
export function useGetCoinsHistory(params, options) {
  return useQuery(
    [getCoinsHistory, params],
    queryWrapper(coinAPI.getCoinsHistory),
    options,
  )
}

export const getRedemptionApproval = 'getRedemptionApproval'
export function useGetRedemptionApproval(params, options) {
  return useQuery(
    [getRedemptionApproval, params],
    queryWrapper(coinAPI.getRedemptionApproval),
    options,
  )
}

export const getRedemptionApprovalInbox = 'getRedemptionApprovalInbox'
export function useGetRedemptionApprovalInbox(params, options) {
  return useQuery(
    [getRedemptionApprovalInbox, params],
    queryWrapper(coinAPI.getRedemptionApprovalInbox),
    options,
  )
}

export const getProductsRedeemByCoin = 'getProductsRedeemByCoin'
export function useGetProductsRedeemByCoin(params, options) {
  return useQuery(
    [getProductsRedeemByCoin, params],
    queryWrapper(coinAPI.getProductsRedeemByCoin),
    options,
  )
}

export const getProductRedeemByCoinByUrlKey = 'getProductRedeemByCoinByUrlKey'
export function useGetProductRedeemByCoinByUrlKey(urlKey, options) {
  return useQuery(
    [getProductRedeemByCoinByUrlKey, urlKey],
    queryWrapper(coinAPI.getProductRedeemByCoinByUrlKey),
    options,
  )
}
