import { useQuery } from 'react-query'
import { globalAPI, homepageAPI, hyperAPI } from '../../IndexApi'
import queryWrapper from '../base'
import localforage from 'localforage'
import config from '../../../../../config'

export const getCategoriesKey = 'getCategories'
export function useGetCategories(params, options) {
  return useQuery(
    [getCategoriesKey, params],
    queryWrapper(globalAPI.getCategory),
    options,
  )
}

export const getBannerSearchKey = 'getBannerSearch'
export function useGetBannerSearch(options) {
  return useQuery(
    getBannerSearchKey,
    queryWrapper(globalAPI.getBannerSearchBar),
    options,
  )
}

export const getPopularSearchAlgoliaKey = 'getPopularSearchAlgolia'
export function useGetPopularSearchAlgolia(options) {
  return useQuery(
    getPopularSearchAlgoliaKey,
    queryWrapper(globalAPI.popularSearchAlgolia, (res) => res.data),
    options,
  )
}

export const getFloatingVoucher = 'getFloatingVoucher'
export function useGetFloatingVoucher(data, options) {
  return useQuery(
    [getFloatingVoucher, data],
    queryWrapper(globalAPI.getFloatingVoucher),
    options,
  )
}

export const getSocialProof = 'getSocialProof'
export function useGetSocialProof(data, options) {
  return useQuery(
    [getSocialProof, data],
    queryWrapper(globalAPI.getSocialProof),
    options,
  )
}

export const getOrderNonAuthKey = 'getOrderNonAuth'
export function useGetOrderNonAuth(params, options) {
  return useQuery(
    [getOrderNonAuthKey, params],
    queryWrapper(globalAPI.getOrderNonAuth),
    options,
  )
}

// consider using cart data from cartAuthContext if you want to get current user's cart data.
// the data in context is in sync with this hook (for same minicart_id)
export const getMinicartKey = 'getMinicart'
export function useGetMinicart(params, options) {
  return useQuery(
    [getMinicartKey, params],
    queryWrapper(globalAPI.getCart),
    options,
  )
}

export const getRandomizePlaceholderKey = 'getRandomizePlaceholder'
export function useGetRandomizePlaceholder(params, options) {
  return useQuery(
    [getRandomizePlaceholderKey, params],
    queryWrapper(globalAPI.dataPlaceholderRandom),
    options,
  )
}

export const getOrderSummaryKey = 'getOrderSummary'
export function useGetOrderSummary(params, options) {
  return useQuery(
    [getOrderSummaryKey, params],
    queryWrapper(globalAPI.getOrderList),
    options,
  )
}

export const getUserProfileKey = 'getUserProfile'
export function useGetUserProfile(options) {
  return useQuery(
    [getUserProfileKey],
    queryWrapper(globalAPI.userProfile),
    options,
  )
}

export const getMinicartAuthKey = 'getMinicartAuth'
export function useGetMinicartAuth(params, options) {
  return useQuery(
    [getMinicartAuthKey, params],
    queryWrapper(globalAPI.minicartAuth, (res) => {
      // use the token from res.data
      return {
        ...res.data.data,
        token: res.data.token,
      }
    }),
    options,
  )
}

export const getCartAuthKey = 'getCartAuth'
export function useGetCartAuth(params, options) {
  return useQuery(
    [getCartAuthKey, params],
    queryWrapper(globalAPI.cartAuth, (res) => {
      // use the token from res.data
      return {
        ...res.data.data,
        token: res.data.token,
      }
    }),
    options,
  )
}

export const getReviewProducts = 'getReviewProducts'
export function useGetReviewProducts(params, options) {
  return useQuery(
    [getReviewProducts, params],
    queryWrapper(globalAPI.getReviewProducts),
    options,
  )
}

export const getReasonReviewProducts = 'getReasonReviewProducts'
export function useGetReasonReviewProducts(params, options) {
  return useQuery(
    [getReasonReviewProducts, params],
    queryWrapper(globalAPI.getReasonsReviewProducts),
    options,
  )
}

export const getVouchersKey = 'getVouchers'
export function useGetVoucher(params, options) {
  return useQuery(
    [getVouchersKey, params],
    queryWrapper(globalAPI.getVouchers, options),
  )
}

export const getVirtualShowroomKey = 'getVirtualShowroom'
export function useGetVirtualShowroom(params, options) {
  return useQuery(
    [getVirtualShowroomKey, params],
    queryWrapper(globalAPI.getShowroom),
    options,
  )
}

export const getProvince = 'getProvince'
export function useGetProvince(options) {
  return useQuery([getProvince], queryWrapper(globalAPI.getProvince), options)
}

export const getProvinceCityKey = 'getProvinceCity'
export function useGetProvinceCity(params, options) {
  return useQuery(
    [getProvinceCityKey, params],
    queryWrapper(globalAPI.getProvinceCity),
    options,
  )
}

export const getCity = 'getCity'
export function useGetCity(provId, options) {
  return useQuery([getCity, provId], queryWrapper(globalAPI.getCity), options)
}

export const getKecamatan = 'getKecamatan'
export function useGetKecamatan(cityId, options) {
  return useQuery(
    [getKecamatan, cityId],
    queryWrapper(globalAPI.getKecamatan),
    options,
  )
}

export const getKelurahan = 'getKelurahan'
export function useGetKelurahan(cityId, options) {
  return useQuery(
    [getKelurahan, cityId],
    queryWrapper(globalAPI.getKelurahan),
    options,
  )
}

export const getGeocodeKey = 'getGeocode'
export function useGetGeocode(params, options) {
  return useQuery(
    [getGeocodeKey, params],
    queryWrapper(globalAPI.getGeocode),
    options,
  )
}

export const getReverseGeocodeKey = 'getReverseGeocode'
export function useGetReverseGeocode(params, options) {
  return useQuery(
    [getReverseGeocodeKey, params],
    queryWrapper(globalAPI.getReverseGeocode),
    options,
  )
}

export const getExpressDelivery = 'getExpressDelivery'
export function useGetExpressDelivery(kecId, options) {
  return useQuery(
    [getExpressDelivery, kecId],
    queryWrapper(globalAPI.getExpressDelivery),
    options,
  )
}

export const getSocialLoginKey = 'getSocialLogin'
export function useGetSocialLogin(params, options) {
  return useQuery(
    [getSocialLoginKey, params],
    queryWrapper(globalAPI.getSocialLogin),
    options,
  )
}

export const getOngoingEventKey = 'getOngoingEvent'
export function useGetOngoingEvent(params, options) {
  return useQuery(
    [getOngoingEventKey, params],
    queryWrapper(globalAPI.getOngoingEvent),
    options,
  )
}

export const getInboxKey = 'getInbox'
export function useGetInbox(params, options) {
  return useQuery(
    [getInboxKey, params],
    queryWrapper(globalAPI.getInbox),
    options,
  )
}

export const getInboxUnreadKey = 'getInboxUnread'
export function useGetInboxUnread(params, options) {
  return useQuery(
    [getInboxUnreadKey, params],
    queryWrapper(globalAPI.getInboxUnread),
    options,
  )
}

export const getBankData = 'useGetBankData'
export function useGetBankData(params, options) {
  return useQuery(
    [getBankData, params],
    queryWrapper(globalAPI.getBankData),
    options,
  )
}

export const getQuizInspirationQuestions = 'getQuizInspiration'
export function useGetQuizQuestions(params, options) {
  return useQuery(
    [getQuizInspirationQuestions, params],
    queryWrapper(globalAPI.getQuizQuestions),
    options,
  )
}

export const getQuizInspirationResults = 'useGetQuizResults'
export function useGetQuizResults(params, options) {
  return useQuery(
    [getQuizInspirationResults, params],
    queryWrapper(globalAPI.getQuizResults),
    options,
  )
}

export const getPersonalisedInformation = 'getPersonalisedInformation'
export function useGetPersonalisedInformation(params, options) {
  return useQuery(
    [getPersonalisedInformation, params],
    queryWrapper(globalAPI.getPersonalisedInformation),
    options,
  )
}

export const getWishlistAndCart = 'getWishlistAndCart'
export function useGetWishlistAndCart(params, options) {
  return useQuery(
    [getWishlistAndCart, params],
    queryWrapper(globalAPI.getWishlistAndCart),
    options,
  )
}

export const getPersonalisedVoucher = 'getPersonalisedVoucher'
export function useGetPersonalisedVoucher(params, options) {
  return useQuery(
    [getPersonalisedVoucher, params],
    queryWrapper(globalAPI.getPersonalisedVoucher),
    options,
  )
}

export const getPersonalisedOrderStatus = 'getPersonalisedOrderStatus'
export function useGetPersonalisedOrderStatus(params, options) {
  return useQuery(
    [getPersonalisedOrderStatus, params],
    queryWrapper(globalAPI.getPersonalisedOrderStatus),
    options,
  )
}

export const getLastSeenRecommendation = 'getLastSeenRecommendation'
export function useGetLastSeenRecommendation(params, options) {
  return useQuery(
    [getLastSeenRecommendation, params],
    queryWrapper(globalAPI.getLastSeenRecommendation),
    options,
  )
}

export const getAddressList = 'useGetAddressList'
export function useGetAddressList(token, options) {
  return useQuery(
    [getAddressList, token],
    queryWrapper(globalAPI.getAddressList),
    options,
  )
}

export const getProvinceData = 'getProvinceData'
export function useGetProvinceData(params, options) {
  return useQuery(
    [getProvinceData, params],
    queryWrapper(globalAPI.getProvince),
    options,
  )
}

export const getCityData = 'getCityData'
export function useGetCityData(params, options) {
  return useQuery(
    [getCityData, params],
    queryWrapper(globalAPI.getCity),
    options,
  )
}

export const getKecamatanData = 'getKecamatanData'
export function useGetKecamatanData(params, options) {
  return useQuery(
    [getKecamatanData, params],
    queryWrapper(globalAPI.getKecamatan),
    options,
  )
}

export const getPopularBrandSearch = 'getPopularBrandSearch'
export function useGetPopularBrandSearch(params, options) {
  return useQuery(
    [getPopularBrandSearch, params],
    queryWrapper(globalAPI.getPopularBrandSearchList),
    options,
  )
}

export const getStoreImage = 'getStoreImage'
export function useStoreImage(params, options) {
  return useQuery(
    [getStoreImage, params],
    queryWrapper(globalAPI.getStoreImage),
    options,
  )
}

export const getStoreLocation = 'getStoreLocation'
export function useStoreLocation(params, options) {
  return useQuery(
    [getStoreLocation, params],
    queryWrapper(globalAPI.getStoreLocation),
    options,
  )
}

export const getBestProducts = 'getBestProducts'
export function useGetBestProducts(params, options) {
  return useQuery(
    [getBestProducts, params],
    queryWrapper(globalAPI.getBestProducts),
    options,
  )
}

export const getBestBrands = 'getBestBrands'
export function useGetBestBrands(params, options) {
  return useQuery(
    [getBestBrands, params],
    queryWrapper(globalAPI.getBestBrands),
    options,
  )
}

export const getLogoUrlKey = 'getLogoUrlKey'
export function useGetLogoUrlKey(params, options) {
  return useQuery(
    [getLogoUrlKey, params],
    queryWrapper(globalAPI.getLogoUrl),
    options,
  )
}

export const getAllWishlist = 'getAllWishlist'
export function useGetAllWishlist(params, options) {
  return useQuery(
    [getAllWishlist, params],
    queryWrapper(globalAPI.getAllWishlist),
    options,
  )
}

export const getAllWishlistV2Key = 'getAllWishlistV2'
export function useGetAllWishlistV2(params, options) {
  return useQuery(
    [getAllWishlistV2Key, params],
    queryWrapper(globalAPI.getAllWishlistV2),
    options,
  )
}

export const getAllProvinces = 'getAllProvinces'
export function useGetAllProvinces(params, options) {
  return useQuery(
    [getAllProvinces, params],
    queryWrapper(globalAPI.getAllProvinces),
    options,
  )
}

export const getAllCities = 'getAllCities'
export function useGetAllCities(params, options) {
  return useQuery(
    [getAllCities, params],
    queryWrapper(globalAPI.getAllCities),
    options,
  )
}

export const getAllDistricts = 'getAllDistricts'
export function useGetAllDistricts(params, options) {
  return useQuery(
    [getAllDistricts, params],
    queryWrapper(globalAPI.getAllDistricts),
    options,
  )
}

export const getSearchedAddress = 'getSearchedAddress'
export function useGetSearchedAddress(params, options) {
  return useQuery(
    [getSearchedAddress, params],
    queryWrapper(globalAPI.getSearchedAddress),
    options,
  )
}

export const getCoinsCredit = 'getCoinsCredit'
export function useGetCoinsCredit(params, options) {
  return useQuery(
    [getCoinsCredit, params],
    queryWrapper(globalAPI.getCoinsCredit),
    options,
  )
}

export const getCoinsExpired = 'getCoinsExpired'
export function useGetCoinsExpired(params, options) {
  return useQuery(
    [getCoinsExpired, params],
    queryWrapper(globalAPI.getCoinsExpired),
    options,
  )
}

export const getCoinsHistory = 'getCoinsHistory'
export function useGetCoinsHistory(params, options) {
  return useQuery(
    [getCoinsHistory, params],
    queryWrapper(globalAPI.getCoinsHistory),
    options,
  )
}

export const getRedemptionApproval = 'getRedemptionApproval'
export function useGetRedemptionApproval(params, options) {
  return useQuery(
    [getRedemptionApproval, params],
    queryWrapper(globalAPI.getRedemptionApproval),
    options,
  )
}

export const getRedemptionApprovalInbox = 'getRedemptionApprovalInbox'
export function useGetRedemptionApprovalInbox(params, options) {
  return useQuery(
    [getRedemptionApprovalInbox, params],
    queryWrapper(globalAPI.getRedemptionApprovalInbox),
    options,
  )
}

export const getPinStatus = 'getPinStatus'
export function useGetPinStatus(params, options) {
  return useQuery(
    [getPinStatus, params],
    queryWrapper(globalAPI.getPinStatus),
    options,
  )
}

export const getDeleteAccountReasons = 'getDeleteAccountReasons'
export function useGetDeleteAccountReasons(params, options) {
  return useQuery(
    [getDeleteAccountReasons, params],
    queryWrapper(globalAPI.getDeleteAccountReasons),
    options,
  )
}

export const getInactiveTransaction = 'getInactiveTransaction'
export function useGetInactiveTransaction(params, options) {
  return useQuery(
    [getInactiveTransaction, params],
    queryWrapper(globalAPI.getInactiveTransaction),
    options,
  )
}

export const getKlkQrCode = 'getKlkQrCode'
export function useGetKlkQrCode(params, options) {
  return useQuery(
    [getKlkQrCode, params],
    queryWrapper(globalAPI.getKlkQrCode),
    options,
  )
}

export const getListPhoneCountryCodes = 'getListPhoneCountryCodes'
export function useGetListPhoneCountryCodes(params, options) {
  return useQuery(
    [getListPhoneCountryCodes, params],
    queryWrapper(globalAPI.getListPhoneCountryCodes),
    options,
  )
}

export const checkDuplicateEmailOrPhone = 'checkDuplicateEmailOrPhone'
export function useCheckDuplicateEmailOrPhone(params, options) {
  return useQuery(
    [checkDuplicateEmailOrPhone, params],
    queryWrapper(globalAPI.checkDuplicateEmailOrPhone),
    options,
  )
}

export const getAllMemberVoucher = 'getAllMemberVoucher'
export function useGetMemberVoucher(params, options) {
  return useQuery(
    [getAllMemberVoucher, params],
    queryWrapper(globalAPI.getAllMemberVoucher),
    options,
  )
}

export const getSummarizeVoucher = 'getSummarizeVoucher'
export function useGetSummarizeVoucher(params, options) {
  return useQuery(
    [getSummarizeVoucher, params],
    queryWrapper(globalAPI.getSummarizeVoucher),
    options,
  )
}

// ! TEMP, getCustomerMembership di atas tidak bisa di disabled
export const getCustomerMembershipAlt = 'getCustomerMembershipAlt'
export function useGetCustomerMembershipAlt(options) {
  return useQuery(
    getCustomerMembership,
    queryWrapper(globalAPI.getCustomerMembershipProfile),
    options,
  )
}

// useGetMembershipUpgradeRequirement
export const getMembershipUpgrade = 'getMembershipUpgrade'
export function useGetMembershipUpgradeRequirement(params, options) {
  return useQuery(
    [getMembershipUpgrade, params],
    queryWrapper(globalAPI.getMembershipUpgradeRequirement),
    options,
  )
}

export const getTierUpgradeRequirements = 'getTierUpgradeRequirements'
export function useGetTierUpgradeRequirements(params, options) {
  return useQuery(
    [getTierUpgradeRequirements, params],
    queryWrapper(globalAPI.getTierUpgradeRequirements),
    {
      ...options,
      onSettled: (res) => {
        if (!config.isB2b) {
          localforage.setItem(
            'klk_membership_level_text',
            res?.[0]?.level || '',
          )
        }
      },
    },
  )
}

export const membershipRequestPin = 'membershipRequestPin'
export function useMembershipRequestPin(params, options) {
  return useQuery(
    [membershipRequestPin, params],
    queryWrapper(globalAPI.membershipRequestPin),
    options,
  )
}

export const getMembershipLevel = 'getMembershipLevel'
export function useGetMembershipLevel(params, options) {
  return useQuery(
    [getMembershipLevel, params],
    queryWrapper(globalAPI.getMembershipLevel),
    options,
  )
}

export const getMembershipDetail = 'getMembershipDetail'
export function useGetMembershipDetail(params, options) {
  return useQuery(
    [getMembershipDetail, params],
    queryWrapper(globalAPI.getMembershipDetail),
    options,
  )
}

export const validateLegacy = 'validateLegacy'
export function useValidateLegacyMembership(params, options) {
  return useQuery(
    [validateLegacy, params],
    queryWrapper(globalAPI.validateLegacyMembership),
    options,
  )
}

export const getAvailableVouchers = 'getAvailableVouchers'
export function useGetAvailableVouchers(params, options) {
  return useQuery(
    [getAvailableVouchers, params],
    queryWrapper(globalAPI.getAvailableVouchersToBeRedeemed),
    options,
  )
}

export const getAllMerchant = 'getAllMerchant'
export function useGetAllMerchant(params, options) {
  return useQuery(
    [getAllMerchant, params],
    queryWrapper(globalAPI.getAllMerchant),
    options,
  )
}

export const getAllSurvey = 'getAllSurvey'
export function useGetAllSurvey(params, options) {
  return useQuery(
    [getAllSurvey, params],
    queryWrapper(globalAPI.getAllSurvey),
    options,
  )
}

export const getCategoriesContent = 'getCategoriesContent'
export function useGetCategoriesContent(params, options) {
  return useQuery(
    [getCategoriesContent, params],
    queryWrapper(globalAPI.getCategoriesContent),
    options,
  )
}

export const getMemberId = 'getMemberId'
export function useGetMemberId(params, options) {
  return useQuery(
    [getMemberId, params],
    queryWrapper(globalAPI.getMemberId),
    options,
  )
}

export const membershipMergeMembership = 'membershipMergeMembership'
export function useMembershipMergeMembership(params, options) {
  return useQuery(
    [membershipMergeMembership, params],
    queryWrapper(globalAPI.membershipMergeMembership),
    options,
  )
}

export const getReferralLink = 'getReferralLink'
export function useGetReferralLink(params, options) {
  return useQuery(
    [getReferralLink, params],
    queryWrapper(globalAPI.getReferralLink),
    options,
  )
}

export const getActivateTactical = 'getActivateTactical'
export function useGetActivateTactical(params, options) {
  return useQuery(
    [getActivateTactical, params],
    queryWrapper(globalAPI.getActivateTactical),
    options,
  )
}

export const getVueRecommend = 'getVueRecommend'
export function useGetVueRecommend(params, options) {
  return useQuery(
    [getVueRecommend, params],
    queryWrapper(globalAPI.getVueRecommend, options),
  )
}

export const getAwsRecommended = 'getAwsRecommended'
export function useGetAwsRecommended(params, options) {
  return useQuery(
    [getAwsRecommended, params],
    queryWrapper(globalAPI.getProductAwsRecommendation),
    options,
  )
}

export const getSeoAutoLink = 'getSeoAutoLink'
export function useGetSeoAutoLink(params, options) {
  return useQuery(
    [getSeoAutoLink, params],
    queryWrapper(globalAPI.getSeoAutoLink),
    options,
  )
}

// Voucher Wallet
export const getOfficialPartnersKey = 'getOfficialPartners'
export function useGetOfficialPartner(params, options) {
  return useQuery(
    [getOfficialPartnersKey, params],
    queryWrapper(globalAPI.getOfficialPartners, options),
  )
}

export const getPartnersKey = 'getPartners'
export function useGetPartner(params, options) {
  return useQuery(
    [getPartnersKey, params],
    queryWrapper(globalAPI.getPartners, options),
  )
}

export const getVoucherDetailKey = 'getVoucherDetail'
export function useGetVoucherDetail(params, options) {
  return useQuery(
    [getVoucherDetailKey, params],
    queryWrapper(globalAPI.getVoucherDetail),
    options,
  )
}

export const getHowToUseVoucherKey = 'getHowToUseVoucher'
export function useGetHowToUseVoucher(params, options) {
  return useQuery(
    [getHowToUseVoucherKey, params],
    queryWrapper(globalAPI.getHowToUseVoucher),
    options,
  )
}

export const getPostVoucherRefund = 'getPostVoucherRefund'
export function usePostVoucherRefund(params, options) {
  return useQuery(
    [getPostVoucherRefund, params],
    queryWrapper(globalAPI.postVoucherRefund, options),
  )
}

export const getPostVoucherRefundDC = 'getPostVoucherRefundDC'
export function usePostVoucherRefundDC(params, options) {
  return useQuery(
    [getPostVoucherRefundDC, params],
    queryWrapper(globalAPI.postVoucherRefundDC, options),
  )
}

export const getUserBankAccount = 'getUserBankAccount'
export function useGetUserBankAccount(params, options) {
  return useQuery(
    [getUserBankAccount, params],
    queryWrapper(globalAPI.getUserBankAccont),
    options,
  )
}

export const setUserBankAccount = 'setUserBankAccount'
export function useSetUserBankAccount(params, options) {
  return useQuery(
    [setUserBankAccount, params],
    queryWrapper(globalAPI.setUserBankAccount),
    options,
  )
}

// Membership
export const getCustomerMembership = 'getCustomerMembership'
export function useGetCustomerMembership(params, options) {
  return useQuery(
    [getCustomerMembership, params],
    queryWrapper(globalAPI.getCustomerMembershipProfile),
    options,
  )
}

/** B2b ACES */
export const getB2bService = 'getB2bService'
export function useGetB2bService(params, options) {
  return useQuery(
    [getB2bService, params],
    queryWrapper(globalAPI.getB2bService),
    options,
  )
}

export const getB2bClient = 'getB2bClient'
export function useGetB2bClient(params, options) {
  return useQuery(
    [getB2bClient, params],
    queryWrapper(globalAPI.getB2bClient),
    options,
  )
}

export const getB2bProductOrder = 'getB2bProductOrder'
export function useGetB2bProductOrder(params, options) {
  return useQuery(
    [getB2bProductOrder, params],
    queryWrapper(homepageAPI.b2bProductOrder),
    options,
  )
}

export const getB2bCmsRequest = 'getB2bCmsRequest'
export function useGetB2bCmsRequest(params, options) {
  return useQuery(
    [getB2bCmsRequest, params],
    queryWrapper(homepageAPI.b2bCmsRequest),
    options,
  )
}

export const getB2bProductReco = 'getB2bProductReco'
export function useGetB2bProductReco(params, options) {
  return useQuery(
    [getB2bProductReco, params],
    queryWrapper(homepageAPI.b2bProductReco),
    options,
  )
}

export const getB2bInformaClients = 'getB2bInformaClients'
export function useGetB2bInformaClients(params, options) {
  return useQuery(
    [getB2bInformaClients, params],
    queryWrapper(homepageAPI.b2bInformaClient),
    options,
  )
}

export const getArticleInspiration = 'getArticleInspiration'
export function useGetArticleInspiration(params, options) {
  return useQuery(
    [getArticleInspiration, params],
    queryWrapper(globalAPI.getArticleInspiration),
    options,
  )
}

export const getHashtag = 'getHashtag'
export function useGetHashtag(options) {
  return useQuery([getHashtag], queryWrapper(globalAPI.getHashtag), options)
}

export const getMultiplySku = 'getMultiplySku'
export function useGetMultiplySku(params, options) {
  return useQuery(
    [getMultiplySku, params],
    queryWrapper(globalAPI.getMultiplySku),
    options,
  )
}

export const newGetInspirations = 'newGetInspirations'
export function useNewGetInspirations(params, options) {
  return useQuery(
    [newGetInspirations, params],
    queryWrapper(globalAPI.newGetInspirations),
    options,
  )
}

export const getDesktopBannerRewards = 'getDesktopBannerRewards'
export function useGetDesktopBannerRewards(params, options) {
  return useQuery(
    [getDesktopBannerRewards, params],
    queryWrapper(globalAPI.getDesktopBannerRewards),
    options,
  )
}

export const getMobileBannerRewards = 'getMobileBannerRewards'
export function useGetMobileBannerRewards(params, options) {
  return useQuery(
    [getMobileBannerRewards, params],
    queryWrapper(globalAPI.getMobileBannerRewards),
    options,
  )
}

export const getHyperPersonalize = 'useHyperPersonalize'
export function useHyperPersonalize(
  type,
  cmsTmp,
  token,
  menu_id,
  options,
  dataLoader,
) {
  return useQuery(
    [getHyperPersonalize, type, cmsTmp, token, menu_id],
    queryWrapper(hyperAPI.hyperPersonalized, dataLoader),
    options,
  )
}

export const getKeywordSuggestion = 'useGetKeywordSuggestion'
export function useGetKeywordSuggestion(params, options) {
  return useQuery(
    [getKeywordSuggestion, params],
    queryWrapper(globalAPI.suggestionBar),
    options,
  )
}

export const searchbarAlgolia = 'useSearchbarAlgolia'
export function useSearchbarAlgolia(keyword, customerId, brand, options) {
  return useQuery(
    [searchbarAlgolia, keyword, customerId, brand],
    queryWrapper(globalAPI.searchbarAlgolia),
    options,
  )
}
export const searchPageRedirect = 'useSearchPageRedirect'
export function useSearchPageRedirect(keyword, options) {
  return useQuery(
    [searchbarAlgolia, keyword],
    queryWrapper(globalAPI.searchPageRedirect),
    options,
  )
}

export const getCorrelationID = 'useGetCorrelationId'
export function useGetCorrelationId(keyword, options) {
  return useQuery(
    [getCorrelationID, keyword],
    queryWrapper(globalAPI.useGetCorrelationID),
    options,
  )
}
export const getAllCategory = 'getAllCategory'
export function useGetAllCategory(params, options) {
  return useQuery(
    [getAllCategory, params],
    queryWrapper(homepageAPI.allCategory),
    options,
  )
}

export const getRevenue = 'getRevenue'
export function useGetRevenue(params, options) {
  return useQuery(
    [getRevenue, params],
    queryWrapper(globalAPI.getRevenue),
    options,
  )
}

export const getBuildingOwnerShip = 'getBuildingOwnerShip'
export function useGetBuildingOwnerShip(params, options) {
  return useQuery(
    [getBuildingOwnerShip, params],
    queryWrapper(globalAPI.getBuildingOwnership),
    options,
  )
}

export const getAnnualPurchase = 'getAnnualPurchase'
export function useGetAnnualPurchase(params, options) {
  return useQuery(
    [getAnnualPurchase, params],
    queryWrapper(globalAPI.getAnnualPurchase),
    options,
  )
}

export const getTaxClass = 'getTaxClass'
export function useGetTaxClass(params, options) {
  return useQuery(
    [getTaxClass, params],
    queryWrapper(globalAPI.getTaxClass),
    options,
  )
}

export const getCityK2 = 'getCityK2'
export function useGetCityK2(params, options) {
  return useQuery(
    [getCityK2, params],
    queryWrapper(globalAPI.getCityK2),
    options,
  )
}

export const getLob = 'getLob'
export function useGetLob(params, options) {
  return useQuery(
    [getLob, params],
    queryWrapper(globalAPI.getLineOfBusiness),
    options,
  )
}

export const getSubLineOfBusiness = 'getSubLineOfBusiness'
export function useGetSubLineOfBusiness(params, options) {
  return useQuery(
    [getSubLineOfBusiness, params],
    queryWrapper(globalAPI.getSubLineOfBusiness),
    options,
  )
}

export const getFeedbackList = 'useGetFeedbackList'
export function useGetFeedbackList(options) {
  return useQuery(
    [getFeedbackList],
    queryWrapper(globalAPI.getFeedbackList),
    options,
  )
}
