import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'
import { x } from './api/base'

const create = () => {
  const headers = DEFAULT_HEADER

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      if (config.isB2b) {
        const auth = await localforage.getItem(
          `persist:ruparupa_b2b_${config.companyNameCSS}`,
        )
        if (auth) {
          const parsedAuthDataB2b = JSON.parse(auth)
          return parsedAuthDataB2b?.access_token
        }
      }
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  // ? get membership info, member identity can be email, phone number, or member id
  const getIdentityInfo = (user) => {
    return apiWrapper.get('/klk/check-membership', {
      user,
    })
  }

  const postLogin = (body) => {
    return x(apiWrapper, () => apiWrapper.post('/klk/validate-password', body))
  }

  // ? for register new user, set pin and update profile after register
  const postRegister = (body) => {
    return x(apiWrapper, () => apiWrapper.post('/klk/register', body))
  }

  const postValidateActivate = (body) => {
    return apiWrapper.post('/klk/activate/validate', body)
  }

  const validateMsStamps = (body) => {
    return apiWrapper.put('/klk/activate/validate-ms-stamps', body)
  }

  const putActivateProfile = (body) => {
    return x(apiWrapper, () => apiWrapper.put('/klk/activate/profile', body))
  }

  const postRequestPin = (data) => {
    return apiWrapper.post('/klk/legacy-membership-pin-request', data)
  }

  const getDuplicateAccounts = async ({ is_split }) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/activate/search-all',
      { is_split },
      headerWithToken(token),
    )
  }

  const postMergeAccount = async (body) => {
    const token = await tokenForage()
    return apiWrapper.post('/klk/activate/merge', body, headerWithToken(token))
  }

  const putMergeAccountV2 = async (body) => {
    const token = await tokenForage()
    return apiWrapper.put(
      '/klk/v2/activate/merge',
      body,
      headerWithToken(token),
    )
  }

  const postValidatePartialRegister = async (body) => {
    return apiWrapper.post('/klk/register/validate', body)
  }

  const getCheckDuplicate = async (params) => {
    return apiWrapper.get('/klk/check-duplicate', params)
  }

  const postDecryptAES256 = async (body) => {
    return apiWrapper.post('/klk/decyrpt-aes', body)
  }

  const getPasskeyChoices = async (params) => {
    return apiWrapper.get('/klk/passkey-choices', params)
  }

  const getPasskeyChoicesMembership = async (params) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/passkey-choices-membership',
      params,
      headerWithToken(token),
    )
  }

  const getSocialLoginUrl = async (params) => {
    return apiWrapper.get('/klk/oauth/credentials', params)
  }

  const validateSocialMediaAuth = async (body) => {
    return apiWrapper.post('/klk/oauth/validate/google?device=web', body)
  }

  const getLegacyMembersByIdentifierV2 = async ({
    identifier,
    merchant_id,
  }) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/v2/legacy-membership',
      { identifier, merchant_id },
      headerWithToken(token),
    )
  }

  return {
    getIdentityInfo,
    postLogin,
    postRegister,
    postValidateActivate,
    validateMsStamps,
    putActivateProfile,
    postRequestPin,
    getDuplicateAccounts,
    postMergeAccount,
    postValidatePartialRegister,
    getCheckDuplicate,
    postDecryptAES256,
    getPasskeyChoices,
    getPasskeyChoicesMembership,
    getSocialLoginUrl,
    validateSocialMediaAuth,
    putMergeAccountV2,
    getLegacyMembersByIdentifierV2,
  }
}

export default {
  create,
}
