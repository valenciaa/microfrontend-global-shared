import apisauce from 'apisauce'
import Cookies from 'js-cookie'
import localforage from 'localforage'
import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import CheckUndefined from '../utils/CheckUndefined'
import { DEFAULT_HEADER } from '../utils/constants'
import { GetAuthDataB2b } from '../utils/GetAuthDataB2b'
import { getRrSID } from '../utils/GetRupaUID'

const isV2 = config.useAssignationPhase2

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization: Authorization },
    }
  }

  const headerInternalTracker = () => {
    return {
      headers: {
        ...headers,
        'Content-Type': 'application/json',
        'x-api-key': config.apiKeyInternalTracker,
      },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    // withCredentials: true,
    headers,
  })

  // this wrapper is for upload, that can be longer than 10k ticks (10s)
  const uploadWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const goRefundWrapper = apisauce.create({
    baseURL: config.goRefundURL,
    timeout: 60000,
  })

  // this is team data endpoint, not passed through our wapi apiURL
  const internalTrackerWrapper = apisauce.create({
    baseURL: config.internalTrackerApiUrl,
    timeout: 10000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      if (config.isB2b) {
        token = await GetAuthDataB2b('access_token')
      } else {
        token = await localforage.getItem('access_token')
      }
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const overrideApiWrapper = apisauce.create({
    baseURL: config.overrideShopInShop ? config.overrideApiURL : config.apiURL,
    timeout: 10000,
    headers,
  })

  //   const tokenForage = async () => {
  //     let token = ''
  //     try {
  //       token = await localforage.getItem('access_token')
  //     } catch (error) {
  //       console.log(error)
  //     }
  //     return token
  //   }

  /**
   * mendapatkan data identifikasi route
   *
   * @param {string} identifier
   * @param {string} companyCode
   * @return {[Promise]}
   */
  const getRouteIndetifier = (identifier, companyCode = config.companyCode) => {
    return apiWrapper.get(`/routes/${identifier}`, {
      company_code: companyCode,
    })
  }

  const getInspirations = (storeCode, inspirationType) => {
    let endpoint = `/inspirations?storeCode=${CheckUndefined(storeCode)}`
    if (typeof type !== 'undefined' || !isEmpty(inspirationType)) {
      endpoint = `/inspirations/promotions?storeCode=${CheckUndefined(
        storeCode,
      )}&type=${inspirationType}&is_b2b=${config.isB2b}`
    }
    return apiWrapper.get(endpoint, {})
  }

  const newGetInspirations = (data) => {
    let endpoint = `/inspirations?storeCode=${CheckUndefined(data.storeCode)}`
    if (typeof type !== 'undefined' || !isEmpty(data.inspirationType)) {
      endpoint = `/inspirations/promotions?storeCode=${CheckUndefined(
        data.storeCode,
      )}&type=${data.inspirationType}&is_b2b=${config.isB2b}`
    }
    return apiWrapper.get(endpoint, {})
  }
  const getNewInspirations = () => {
    return apiWrapper.get('/inspirations/new', {})
  }

  const getCategory = () => {
    const extendedNewFeatureParam = '&isMission=true'
    return apiWrapper.get(
      `/category/tree?env=${config.environment}&is_b2b=${config.isB2b}${extendedNewFeatureParam}`,
    )
  }

  // SEARCHING!!!
  // Required parameters cartId : cart unique id
  const getCart = async (params) => {
    const token = await tokenForage()
    if (token) {
      return apiWrapper.get(
        `/user/cart/minicart/${params?.minicart_id}`,
        {},
        headerWithToken(token),
      )
    } else {
      return apiWrapper.get(`/cart/minicart/${params?.minicart_id}`)
    }
  }

  const cartAuth = (params) => {
    return apiWrapper.get('/cart/cart-auth/' + params?.cart_id)
  }

  const minicartAuth = (params) => {
    return apiWrapper.get('/cart/minicart/cart-auth/' + params?.minicart_id)
  }

  // Required parameters data : remote_ip, customer, device, items dll (read documentation), cartId : cart unique id
  const updateCart = async (body) => {
    const data = {
      device: config.environment,
      affiliate_id: '',
      device_token: 'pooled',
      store_code: '',
      ...body,
    }

    if (config.isB2b && config.companyCode === 'HCI') {
      data.cart_type = 'informa_b2b'
    }
    if (config.isB2b && config.companyCode === 'AHI') {
      data.cart_type = 'ace_b2b' // ACE_HARDWARE
    }

    const token = await tokenForage()
    if (token) {
      return apiWrapper.post(
        '/user/cart/minicart/update',
        { data },
        headerWithToken(token),
      )
    } else {
      return apiWrapper.post('/cart/minicart/update', { data })
    }
  }

  // Required parameters cartId : cart unique id
  const deleteCartItem = async (body) => {
    const data = {
      device: config.environment,
      affiliate_id: '',
      device_token: 'pooled',
      store_code: '',
      ...body,
    }
    const token = await tokenForage()
    if (token) {
      return apiWrapper.post(
        '/user/cart/minicart/delete',
        { data },
        headerWithToken(token),
      )
    } else {
      return apiWrapper.post('/cart/minicart/delete', { data })
    }
  }

  // log
  const createLog = (data) => {
    return apiWrapper.post('/log/keyword', { data })
  }

  const popularSearch = () => {
    return apiWrapper.get('/log/popular')
  }

  const popularSearchAlgolia = () => {
    return apiWrapper.get(
      '/instant/popular',
      {},
      {
        headers: { ...headers, 'X-Algolia-UserToken': Cookies.get('_ALGOLIA') },
      },
    )
  }

  const getCustom = (columnName) => {
    return apiWrapper.get(`/misc/custom?columnName=${columnName}`)
  }

  const getInspirationsSearch = (keyword) => {
    return apiWrapper.get(`/product/inspirations?keyword=${keyword}`)
  }

  // Required parameters keyword
  const searchProductByKeyword = (keyword, storeCode) => {
    return overrideApiWrapper.get(
      `/product/search/${keyword}?storeCode=${storeCode}`,
    )
  }

  const searchbarAlgolia = (keyword, customerId, brand) => {
    let algoliaToken = null
    if (customerId && customerId !== '') {
      algoliaToken = customerId
    }
    let headerAlgolia = {
      ...headers,
    }
    if (algoliaToken) {
      headerAlgolia = {
        ...headerAlgolia,
        'X-Algolia-UserToken': algoliaToken,
      }
    }

    if (!isEmpty(brand)) {
      return apiWrapper.get(
        `/instant/${brand}?keyword=${keyword}`,
        {},
        {
          headers: { ...headerAlgolia },
        },
      )
    } else {
      return apiWrapper.get(
        `/instant/search/${keyword}`,
        {},
        {
          headers: { ...headerAlgolia },
        },
      )
    }
  }

  // Required parameters keyword
  const searchPageRedirect = (keyword) => {
    return apiWrapper.get(`/product/pageredirect/${keyword}`)
  }

  const suggestionBar = (data) => {
    return apiWrapper.get(`/instant/suggestion/${data}`)
  }

  const newsletterSubscribe = (data) => {
    return apiWrapper.post('/newsletter/subscribe', { data })
  }

  const downloadMobileAppsSubscribe = ({ body, token }) => {
    return apiWrapper.post(
      '/misc/send-message',
      { data: body },
      headerWithToken(token),
    )
  }

  const getBannerSearchBar = () => {
    return apiWrapper.get('/misc/search-banner')
  }

  // BASO
  const getFloatingVoucher = (data) => {
    return apiWrapper.get(
      `/baso/warm/floating-voucher/?companyCode=${data.companyCode}&platform=${data.platform}`,
    )
  }

  const getSocialProof = (data) => {
    return apiWrapper.get(`/baso/cold/social-proof/?url_key=${data}`)
  }

  // Facebook Conversion Api
  const facebookConversion = (data) => {
    if (config.isLiveSite) {
      return apiWrapper.post('/facebook/fbcapi', data)
    }
  }

  // Tiktok CAPI
  const tiktokConversion = (data) => {
    if (config.isLiveSite) {
      return apiWrapper.post('/tiktok/capi', data)
    }
  }

  // TAHU
  const dataPlaceholderRandom = (data) => {
    return apiWrapper.get(
      `/tahu/search-suggestion?isNew=true&is_b2b=${config.isB2b}`,
      data,
    )
  }

  const getOrderNonAuth = (data) => {
    const orderNo = data?.order_no
    const email = `?customer_email=${data?.email}`
    const token = data?.print_token ? `&print_token=${data?.print_token}` : ''
    const type = data?.page_from ? `&type=${data?.page_from}` : ''

    return apiWrapper.get('/order/' + orderNo + email + type + token)
  }

  const getOrderList = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/user/order-summary', data, headerWithToken(token))
  }

  const checkAuthData = (data) => {
    return apiWrapper.post(
      '/auth/check-otp-auth',
      data,
      headerWithToken(config.otpToken),
    )
  }

  const login = (data) => {
    return apiWrapper.post('/auth/login', data)
  }

  const uploadFileB2b = (data) => {
    return apiWrapper.post(
      '/misc/upload-file-b2b',
      data,
      headerWithToken(config.otpToken),
    )
  }

  const userProfile = async () => {
    const token = await tokenForage()
    return apiWrapper.get('/user/profile', {}, headerWithToken(token))
  }

  const getInbox = async (data) => {
    const token = await tokenForage()
    if (token) {
      return apiWrapper.get(
        '/inbox/get?token=' + token,
        data,
        headerWithToken(token),
      )
    }
    return apiWrapper.get('/inbox/get', data)
  }

  const getInboxUnread = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/inbox/count' + (token ? '?token=' + token : ''),
      {},
      headerWithToken(token),
    )
  }

  const markInboxAsRead = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/inbox/read', data, headerWithToken(token))
  }

  const getSocialLogin = () => {
    return apiWrapper.get('/misc/url-social-login')
  }

  const getOngoingEvent = (params) => {
    const timeout = params.timeout
    delete params.timeout
    return apiWrapper.get(
      `/tahu/ongoing-event?is_b2b=${config.isB2b}`,
      params,
      { timeout },
    )
  }

  const getShowroom = (data) => {
    let endpoint = `/tahu/virtual-showroom/active/new?url_key=${data?.urlKey}&b2b_type=${config?.isB2b}`
    if (data?.tab) {
      endpoint = endpoint + `&tab=${encodeURIComponent(data?.tab)}`
    }
    return apiWrapper.get(endpoint)
  }

  const getReviewProducts = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/review/summary?token=${token}`,
      {},
      headerWithToken(token),
    )
  }

  const getReasonsReviewProducts = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/review/master?type=${data.type}`,
      {},
      headerWithToken(token),
    )
  }

  const uploadReviewImage = async (data) => {
    const token = await tokenForage()
    return uploadWrapper.post('/review/upload', data, headerWithToken(token))
  }

  const uploadMultipleReviewImages = async (data) => {
    const token = await tokenForage()
    return uploadWrapper.post(
      '/review/upload/bulk',
      data,
      headerWithToken(token),
    )
  }

  const submitReview = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/review/product/upsert',
      { data },
      headerWithToken(token),
    )
  }

  /**
   * mendapatkan data provinsi
   *
   * @return  {[Promise]}
   */
  const getProvince = () => {
    return apiWrapper.get('/misc/province')
  }

  /**
   * mendapatkan data kabupaten atau kota beserta dengan provinsi
   *
   *
   * @return  {[Promise]}
   */
  const getCityAlll = () => {
    return apiWrapper.get('/misc/city')
  }
  /**
   * mendapatkan data kabupaten atau kota berdasar provinsi
   *
   * @param   {[integer]}  provId
   *
   * @return  {[Promise]}
   */
  const getCity = (provId) => {
    return apiWrapper.get(`/misc/city/${provId}`)
  }

  /**
   * mendapatkan data kecamatan berdasar kabupaten/kota
   *
   * @param   {[integer]}  cityId
   *
   * @return  {[Promise]}
   */
  const getKecamatan = (cityId) => {
    return apiWrapper.get(`/misc/kecamatan/${cityId}`)
  }

  /**
   * Get the kelurahan based on the kecamatan ID.
   *
   * @param {[integer]} kecamatanId
   * @return {[Promise]}
   */

  const getKelurahan = (kecamatanId) => {
    return apiWrapper.get(`/misc/kelurahan/${kecamatanId}`)
  }

  const postSearchSuggestionKecamatan = (search) => {
    return apiWrapper.post('/misc/full-by-kecamatan', search)
  }

  const getSearchByKelurahan = (search) => {
    return apiWrapper.get(`/misc/search-by-kelurahan?keyword=${search}`)
  }

  const getGeocode = (params) => {
    return apiWrapper.get(`/map/v2/geocode`, params)
  }

  const getReverseGeocode = (params) => {
    return apiWrapper.get(`/map/v2/reverse-geocode`, params)
  }

  /**
   * mendapatkan data province dan city berdasarkan negara
   *
   * @param   {[integer]}  countryId
   *
   * @return  {[Promise]}
   */
  const getProvinceCity = (countryId) => {
    return apiWrapper.get(`/misc/city?country_id=${countryId}`)
  }

  /**
   * mendapatkan data dukungan pengiriman
   *
   * @param   {[integer]}  kecId  [kecId id kecamatan]
   *
   * @return  {[Promise]}
   */
  const getExpressDelivery = (data) => {
    return apiWrapper.get(`/misc/get-nearest-store/`, data)
  }

  /**
   * mendapatkan data detail dari cms block
   * @param {string} identifier [url key]
   * @returns
   */
  const getCmsBlockDetail = (identifier) => {
    return apiWrapper.get(
      `/misc/cms-block-detail?identifier=${identifier}&env=${config.environment}`,
    )
  }

  const getBankData = () => {
    return apiWrapper.get('/misc/bank-installment?to=footer-3.0')
  }

  const getKeywordSuggestion = (keyword) => {
    return apiWrapper.get(`/instant/search/${keyword}`)
  }

  const getQuizQuestions = async (data) => {
    return apiWrapper.get(
      `/tahu/quiz-inspiration/active?url_key=${data}&device=${config?.environment}&is_b2b=${config.isB2b}`,
    )
  }

  const getQuizResults = async (data) => {
    if (data) {
      return apiWrapper.get(
        `/quiz/searching?id=${data?.question}&value=${data?.answer}&from=${data?.from}`,
      )
    } else {
      return null
    }
  }

  const getPersonalisedInformation = async () => {
    return apiWrapper.get('/tahu/personalised-information/active')
  }

  const getWishlistAndCart = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/personalised/wishlist-cart?token=${token}`,
      data,
      headerWithToken(token),
    )
  }

  const getPersonalisedVoucher = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/personalised/voucher-list?token=${token}`,
      data,
      headerWithToken(token),
    )
  }

  const getPersonalisedOrderStatus = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/personalised/status-order?token=${token}`,
      data,
      headerWithToken(token),
    )
  }

  const getLastSeenRecommendation = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      `/personalised/last-seen?token=${token}`,
      data,
      headerWithToken(token),
    )
  }

  // Required parameters category : category name
  const getProductByKeyword = (
    keyword,
    from,
    size,
    sort,
    brands,
    labels,
    colors,
    expressCourier,
    minimumPrice,
    maximumPrice,
    storeCode,
    deliveryMethod,
  ) => {
    let variantAttributes = ''

    if (colors && colors !== '') {
      variantAttributes = `color:${colors}`
    }
    return overrideApiWrapper.get(
      `/product${isV2 ? '/v2' : ''}/keyword/${keyword || ''}?from=${
        from || 0
      }&size=${size || 48}&sort=${sort || ''}&brands=${brands || ''}&labels=${
        labels || ''
      }&variant_attributes=${variantAttributes}&minprice=${
        minimumPrice || ''
      }&maxprice=${
        maximumPrice || ''
      }&express_courier=${expressCourier}&storeCode=${
        CheckUndefined(storeCode) || ''
      }&deliveryMethod=${deliveryMethod || ''}`,
    )
  }

  const getFeedbackList = () => {
    return apiWrapper.get('/review/feedback/list')
  }

  const submitFeedback = (data) => {
    return apiWrapper.post('/review/feedback/submit', { data })
  }

  const getAddressList = async (token) => {
    return apiWrapper.get('/user/address-list', {}, headerWithToken(token))
  }

  const createAddress = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/user/address', { data }, headerWithToken(token))
  }

  const updateAddress = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      `/user/address/${data?.address_id}`,
      { data },
      headerWithToken(token),
    )
  }

  const validateAddress = async (data) => {
    const token = await tokenForage()

    return apiWrapper.put(
      `/user/address/validate`,
      { data },
      headerWithToken(token),
    )
  }

  const getPopularBrandSearchList = (data) => {
    return apiWrapper.get('/tahu/popular-brand', data)
  }

  const topSpenderCampaign = () => {
    return apiWrapper.get('/tahu/customer/leaderboard')
  }

  // e-catalogue
  const getEcatalogue = (data) => {
    const limitQuery = data?.limit ? `&limit=${data.limit}` : ''
    return apiWrapper.get(
      `/tahu/catalog?companyCode=${data?.companyCode}&offset=${data.offset}${limitQuery}`,
    )
  }

  const getSingleEcatalogue = (data) => {
    return apiWrapper.get(`/tahu/catalog/${data.id}`)
  }

  // const getAllWishlist = (data) => {
  //   const { wishlistFrom, wishlistSize } = data.size
  //   return apiWrapper.get(`/user/v2/wishlist?sku=${data.sku}`,
  //     { wishlistFrom, wishlistSize },
  //     headerWithToken(data.auth)
  //   )
  // }

  const getAllWishlistV2 = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/user/v2/wishlist', data, headerWithToken(token))
  }

  // TODO TEMP
  const getAllWishlist = (data) => {
    const { wishlistFrom, wishlistSize } = data.size
    const { sku } = data
    return apiWrapper.get(
      '/user/wishlist',
      { wishlistFrom, wishlistSize, sku },
      headerWithToken(data?.auth || ''),
    )
  }

  const getLogoUrl = (brand) => {
    return apiWrapper.get(`/misc/logo-brand/${brand}`)
  }

  // SEO RELATED, mostly for head /jual
  const getSeoAutoLink = (keyword) => {
    return apiWrapper.get(`/seo/autoLink/${keyword}`)
  }

  // Store Image
  const getStoreImage = (keyword) => {
    return apiWrapper.get(`/misc/store-logo/${keyword}`)
  }

  // Store Location
  const getStoreLocation = (params) => {
    return apiWrapper.get('/store', params)
  }

  const getBestProducts = () => {
    return apiWrapper.get('/constants/best-products', {
      company_code: config.companyCode,
    })
  }
  const getBestBrands = () => {
    return apiWrapper.get('/constants/best-brands', {
      company_code: config.companyCode,
    })
  }

  const getStoreRegions = () => {
    return apiWrapper.get('/store/store-region')
  }

  // Referral Tactical
  const getReferralLink = (keyword) => {
    return apiWrapper.get(
      `/referral/referral-link?channel=${keyword.channel}&id=${keyword.id}&referral_tactical_id=${keyword.referral_tactical_id}`,
    )
  }

  const getActivateTactical = async () => {
    return apiWrapper.get('/referral/active-tactical')
  }
  // Vue.ai

  const sendTrackEvent = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/vue/track', data, headerWithToken(token || ''))
  }

  const getVueRecommend = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/vue/feeds', data, headerWithToken(token || ''))
  }

  const getAllProvinces = async (params) => {
    return apiWrapper.get('/klk/summarize-province', params)
  }

  const getAllCities = async (params) => {
    return apiWrapper.get('/klk/summarize-city', params)
  }

  const getAllDistricts = async (params) => {
    return apiWrapper.get('/klk/summarize-district', params)
  }

  const getSearchedAddress = async (data) => {
    return apiWrapper.post('/klk/search-address', data)
  }

  const getAvailableVouchersToBeRedeemed = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/get-available-vouchers',
      data,
      headerWithToken(token),
    )
  }

  const getInactiveTransaction = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/customer/inactive/transaction',
      data,
      headerWithToken(token),
    )
  }

  const getKlkQrCode = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/generate-qr?token=' + token,
      data,
      headerWithToken(token),
    )
  }

  const getListPhoneCountryCodes = () => {
    return apiWrapper.get('/klk/country-list', {})
  }

  const checkDuplicateEmailOrPhone = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/check-duplicate-identifier',
      data,
      headerWithToken(token),
    )
  }

  const getAllMerchant = async () => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/merchants-list', {}, headerWithToken(token))
  }

  const validateLegacyMembership = async (param) => {
    const { merchId, input } = param
    const token = await tokenForage()

    if (token) {
      return apiWrapper.get(
        `/klk/legacy-membership-validate?input=${input}&merchant=${merchId}`,
        {},
        headerWithToken(token),
      )
    }
  }

  const getSummarizeVoucher = async () => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/summarize-voucher', {}, headerWithToken(token))
  }

  const getMemberId = ({ userToken }) => {
    if (userToken) {
      return apiWrapper.get(
        '/klk/membership-id/',
        {},
        {
          headers: {
            ...headers,
            Authorization: userToken,
          },
        },
      )
    }
  }

  const getMembershipUpgradeRequirement = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/tier-upgrade-conditions',
      {},
      headerWithToken(token),
    )
  }

  const membershipRequestPin = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/klk/legacy-request-passkey-membership',
      { data },
      headerWithToken(token),
    )
  }

  const membershipMergeMembership = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/klk/legacy-request-merge-membership',
      { data },
      headerWithToken(token),
    )
  }

  const getMembershipLevel = async () => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/all-tiers', {}, headerWithToken(token))
  }

  const getAllMemberVoucher = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/user/vouchers?limit=4&page=1&filter=all',
      {},
      headerWithToken(token),
    )
  }

  const getAllSurvey = async () => {
    const token = await tokenForage()
    if (token) {
      return apiWrapper.get('/klk/all-surveys', {}, headerWithToken(token))
    }
  }

  const getTierUpgradeRequirements = async () => {
    const token = await tokenForage()
    if (token) {
      return apiWrapper.get(
        '/klk/tier-upgrade-conditions',
        {},
        headerWithToken(token),
      )
    }
  }
  const getMembershipDetail = async (data) => {
    const token = await tokenForage()
    if (token) {
      return apiWrapper.get(
        '/klk/membership-tier',
        data,
        headerWithToken(token),
      )
    }
  }

  const checkMembership = async (data) => {
    let authData = data
    const token = await tokenForage()

    if (data.email) {
      authData = data.email
    } else {
      authData = data.phone
    }

    if (token) {
      return apiWrapper.get(
        `/klk/check-membership?user=${authData}`,
        {},
        headerWithToken(token),
      )
    }
  }

  const getProductAwsRecommendation = (data) => {
    return apiWrapper.get('/product/aws/recommendation', data)
  }

  // Voucher Wallets
  const getVouchers = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/user/vouchers', data, headerWithToken(token))
  }
  const getVoucherDetail = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/user/voucher-detail', data, headerWithToken(token))
  }
  const getPartners = async () => {
    const token = await tokenForage()
    return apiWrapper.get('/user/partner-list', {}, headerWithToken(token))
  }
  const getOfficialPartners = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/user/official-partner-list',
      {},
      headerWithToken(token),
    )
  }
  const getHowToUseVoucher = (params) => {
    return apiWrapper.get('/tahu/getWordingContent', params)
  }

  const postVoucherRefund = async (data) => {
    const token = await tokenForage()
    return goRefundWrapper.post(
      '/refund/create',
      { data: { data } },
      headerWithToken(token),
    )
  }

  const postVoucherRefundDC = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      `/voucher/retref/dc-voucher`,
      { data },
      headerWithToken(token),
    )
  }

  // User Account
  const getUserBankAccont = async () => {
    const token = await tokenForage()
    return apiWrapper.get('/user/bank-account/list', {}, headerWithToken(token))
  }

  const setUserBankAccount = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      '/user/bank-account/update',
      data,
      headerWithToken(token),
    )
  }

  /** B2b ACES */
  const getB2bService = async () => {
    return apiWrapper.get('/b2b/service')
  }

  const getB2bClient = async () => {
    return apiWrapper.get('/b2b/clients')
  }

  const postQuotation = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/email/b2b?isCustom=true',
      { data },
      headerWithToken(token),
    )
  }

  // B2B INFORMA
  const getCityK2 = () => {
    return apiWrapper.get('/misc/city-kawanlama')
  }

  const getLineOfBusiness = () => {
    return apiWrapper.get('/misc/lob')
  }

  const getRevenue = () => {
    return apiWrapper.get('/misc/revenue')
  }

  const getAnnualPurchase = () => {
    return apiWrapper.get('/misc/annual-purchase')
  }

  const getBuildingOwnership = () => {
    return apiWrapper.get('/misc/building-ownership')
  }

  const getTaxClass = () => {
    return apiWrapper.get('/misc/tax-class')
  }

  const getSubLineOfBusiness = (id) => {
    return apiWrapper.get(`/misc/sub-lob/${id}`)
  }

  const getCategoryPredict = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/category/predict', data, headerWithToken(token))
  }

  // Verification
  const verifyEmailOrPhone = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      '/klk/email-phone-verification',
      data,
      headerWithToken(token),
    )
  }
  // Membership
  const getCustomerMembershipProfile = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/klk/summarize-profile',
      { token },
      headerWithToken(token),
    )
  }

  const getArticleInspiration = (data) => {
    return apiWrapper.get(
      `/tahu/article?offset=${data?.offset}&limit=${data?.limit}&sku=${
        data?.sku
      }&tag=${encodeURIComponent(data?.tag)}`,
    )
  }

  const getHashtag = () => {
    return apiWrapper.get('/tahu/article-hashtag')
  }

  // Freshchat
  const getFreshchatRestoreId = async (data) => {
    const { externalId } = data
    const token = await tokenForage()

    return apiWrapper.get(
      `/live-chat/freshchat/restore-id?external_id=${externalId}`,
      {},
      headerWithToken(token),
    )
  }

  const postFreshchatRestoreId = async (data) => {
    const token = await tokenForage()

    return apiWrapper.post(
      '/live-chat/freshchat/restore-id',
      data,
      headerWithToken(token),
    )
  }

  // DC Dashboard

  const makeVoucherDc = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put(
      `/voucher/retref/dc-voucher?action=${data.action}&email=${data.email}&retref_no=${data.retref_no}`,
      {},
      headerWithToken(token),
    )
  }

  const getDesktopBannerRewards = (params) => {
    return apiWrapper.get(
      `/misc/desktop-banner${config.isB2b ? '?from=b2b' : ''}`,
      params,
    )
  }

  const getMobileBannerRewards = (params) => {
    return apiWrapper.get('/misc/mobile-banner', params)
  }

  // Multiply SKU
  const getMultiplySku = async (sku) => {
    return apiWrapper.get(`/product/multiplysku/${sku}`)
  }

  const postInternalTracker = async (payload) => {
    return internalTrackerWrapper.post(
      '/event-tracker/add-rr-user-event',
      payload,
      headerInternalTracker(),
    )
  }

  const insertReviewArticle = async (data) => {
    const token = await tokenForage()
    if (!token) {
      const rrSid = await getRrSID()
      data['rrSid'] = rrSid || ''
    }

    return apiWrapper.post(
      '/tahu/faq-insert-reviewArticles',
      data,
      headerWithToken(token || ''),
    )
  }

  const postSetAttributeMoengage = async (payload) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/moengage/save-user-attributes',
      payload,
      headerWithToken(token),
    )
  }

  const useGetCorrelationID = () => {
    return apiWrapper.get('/vue/correlation-id')
  }

  const postEmailCustomFurniture = async (data) => {
    return apiWrapper.post('/email/custom', data)
  }

  const googleMapsPlacesAutocomplete = async (params) => {
    const token = await tokenForage()

    return apiWrapper.get('map/v2/autocomplete', params, headerWithToken(token))
  }

  const googleMapsPlacesDetail = async (params) => {
    const token = await tokenForage()

    return apiWrapper.get('map/v2/placesid', params, headerWithToken(token))
  }

  return {
    checkMembership,
    getTierUpgradeRequirements,
    getSummarizeVoucher,
    membershipMergeMembership,
    membershipRequestPin,
    getMemberId,
    getMembershipDetail,
    getAllSurvey,
    validateLegacyMembership,
    getAllMemberVoucher,
    getMembershipLevel,
    getMembershipUpgradeRequirement,
    postQuotation,
    getB2bClient,
    getB2bService,
    getCustomerMembershipProfile,
    getOrderNonAuth,
    getOrderList,
    getInspirations,
    getNewInspirations,
    getCategory,
    getCart,
    cartAuth,
    minicartAuth,
    updateCart,
    deleteCartItem,
    getInspirationsSearch,
    searchProductByKeyword,
    searchbarAlgolia,
    searchPageRedirect,
    createLog,
    popularSearch,
    popularSearchAlgolia,
    getCustom,
    suggestionBar,
    newsletterSubscribe,
    downloadMobileAppsSubscribe,
    getBannerSearchBar,
    getFloatingVoucher,
    getSocialProof,
    facebookConversion,
    tiktokConversion,
    dataPlaceholderRandom,
    checkAuthData,
    uploadFileB2b,
    login,
    userProfile,
    getInbox,
    getInboxUnread,
    markInboxAsRead,
    getSocialLogin,
    getOngoingEvent,
    getShowroom,
    getAllWishlist,
    getAllWishlistV2,
    getReviewProducts,
    getReasonsReviewProducts,
    uploadReviewImage,
    uploadMultipleReviewImages,
    submitReview,
    getProvince,
    getCityAlll,
    getCity,
    getKecamatan,
    getKelurahan,
    postSearchSuggestionKecamatan,
    getSearchByKelurahan,
    getGeocode,
    getReverseGeocode,
    getProvinceCity,
    getExpressDelivery,
    getCmsBlockDetail,
    getBankData,
    getKeywordSuggestion,
    getQuizQuestions,
    getQuizResults,
    getPersonalisedInformation,
    getWishlistAndCart,
    getVouchers,
    getVoucherDetail,
    getPartners,
    getOfficialPartners,
    getPersonalisedVoucher,
    getPersonalisedOrderStatus,
    getLastSeenRecommendation,
    getProductByKeyword,
    getFeedbackList,
    submitFeedback,
    getAddressList,
    createAddress,
    updateAddress,
    validateAddress,
    getPopularBrandSearchList,
    getSeoAutoLink,
    getStoreImage,
    getStoreLocation,
    getBestProducts,
    getBestBrands,
    getStoreRegions,
    topSpenderCampaign,
    getLogoUrl,
    getReferralLink,
    getActivateTactical,
    sendTrackEvent,
    getVueRecommend,
    getAllProvinces,
    getAllCities,
    getAllDistricts,
    getSearchedAddress,
    getAvailableVouchersToBeRedeemed,
    getInactiveTransaction,
    getKlkQrCode,
    getListPhoneCountryCodes,
    checkDuplicateEmailOrPhone,
    getAllMerchant,
    verifyEmailOrPhone,
    getProductAwsRecommendation,
    getHowToUseVoucher,
    postVoucherRefund,
    postVoucherRefundDC,
    getUserBankAccont,
    setUserBankAccount,
    getCategoryPredict,
    getCityK2,
    getLineOfBusiness,
    getSubLineOfBusiness,
    getRevenue,
    getAnnualPurchase,
    getBuildingOwnership,
    getTaxClass,
    getRouteIndetifier,
    getArticleInspiration,
    getHashtag,
    getFreshchatRestoreId,
    postFreshchatRestoreId,
    makeVoucherDc,
    newGetInspirations,
    getDesktopBannerRewards,
    getMobileBannerRewards,
    postInternalTracker,
    insertReviewArticle,
    postSetAttributeMoengage,
    getMultiplySku,
    useGetCorrelationID,
    getEcatalogue,
    getSingleEcatalogue,
    postEmailCustomFurniture,
    googleMapsPlacesAutocomplete,
    googleMapsPlacesDetail,
  }
}

export default {
  create,
}
