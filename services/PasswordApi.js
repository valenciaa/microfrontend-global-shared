import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const validatePassword = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/klk/password-validation',
      data,
      headerWithToken(token),
    )
  }

  const manageUpdatePassword = async (data) => {
    const token = await tokenForage()
    const rsToken = await localforage.getItem('rsToken')
    if (token || rsToken) {
      return apiWrapper.put(
        '/klk/manage-update-password',
        data,
        headerWithToken(rsToken || token),
      )
    } else {
      return apiWrapper.put('/klk/manage-update-password', data)
    }
  }

  return {
    validatePassword,
    manageUpdatePassword,
  }
}

export default {
  create,
}
