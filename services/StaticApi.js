// import Cookies from 'js-cookie'
import apisauce from 'apisauce'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'
// import isEmpty from 'lodash/isEmpty'
// import CheckUndefined from '../utils/CheckUndefined'
// import localforage from 'localforage'

const create = () => {
  const headers = DEFAULT_HEADER

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const getFilterLooks = () => {
    return apiWrapper.get('/static-page/hyde-living/filter')
  }

  const getProductDetails = (projectKey) => {
    return apiWrapper.get(
      `/static-page/hyde-living/detail?project_key=${
        projectKey.projectKey ? projectKey.projectKey : ''
      }`,
    )
  }

  const getOurLooks = (params) => {
    return apiWrapper.get(
      `/static-page/hyde-living?design_type=${
        params?.designType ? params?.designType?.replace(' ', '%20') : ''
      }&offset=${params?.offSet ? params?.offSet : '0'}&project_type=${
        params?.projectType ? params?.projectType : ''
      }&room_name=${params?.roomName ? params?.roomName : ''}`,
    )
  }

  const getOtherLooks = (projectKey) => {
    return apiWrapper.get(
      `/static-page/hyde-living/other-looks?project_key=${projectKey.projectKey}`,
    )
  }

  const getTestimonialVideos = () => {
    return apiWrapper.get('/static-page/hyde-living/testimonials')
  }

  const getCustomFurniture = (serviceParam) => {
    if (serviceParam) {
      return apiWrapper.get(`/static-page/custom-furniture/${serviceParam}`)
    } else {
      return apiWrapper.get('/static-page/custom-furniture')
    }
  }

  const getSeoTag = () => {
    return apiWrapper.get(
      '/seo/autoLink/customList?company_code=ODI&status=10&limit=21&typo=0',
    )
  }

  const getSeoAlphabet = (data) => {
    return apiWrapper.get(
      `/seo/autoLink/customAlphabet?letter=${data.keyword}&company_code=${config.companyCode}&status=10&offset=${data.from}&limit=${data.size}&typo=0`,
    )
  }

  return {
    getFilterLooks,
    getProductDetails,
    getOtherLooks,
    getOurLooks,
    getTestimonialVideos,
    getCustomFurniture,
    getSeoTag,
    getSeoAlphabet,
  }
}

export default {
  create,
}
