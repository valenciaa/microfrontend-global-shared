import apisauce from 'apisauce'
import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import CheckUndefined from '../utils/CheckUndefined'
import { DEFAULT_HEADER } from '../utils/constants'
import localforage from 'localforage'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  // Banner & Brand

  const desktopBanner = (params) => {
    return apiWrapper.get(
      `/misc/desktop-banner${config.isB2b ? '?is_b2b=true' : ''}`,
      params,
    )
  }

  const mobileBanner = (params) => {
    return apiWrapper.get('/misc/mobile-banner', params)
  }

  // const desktopBrand = () => {
  //   const totalToBeSplitted = config.desktopBrandSplitted
  //   return apiWrapper.get('/misc/desktop-brand', { totalToBeSplitted })
  // }

  // const mobileBrand = () => {
  //   const totalToBeSplitted = config.mobileBrandSplitted
  //   return apiWrapper.get('/misc/mobile-brand', { totalToBeSplitted })
  // }

  const getEventDetail = (params) => {
    const addressApi = !isEmpty(params.url) ? 'tahu' : 'homepage'
    return apiWrapper.get(
      `/events?event=${params.event}&storeCode=${CheckUndefined(
        params.storeCode,
      )}&platform=${config.environment}&from=${addressApi}`,
      {},
    )
  }

  const getCmsBlockDetail = (identifier) => {
    return apiWrapper.get(
      `/misc/cms-block-detail?identifier=${identifier}${
        config.isB2b ? '&is_b2b=true' : ''
      }`,
    )
  }

  const getCmsBlockDetailV2 = (params) => {
    const { identifier, company_code } = params

    const companyCode = !isEmpty(company_code)
      ? company_code
      : config.companyCode

    return apiWrapper.get(
      `/misc/cms-block-detail?identifier=${identifier}&company_code=${companyCode}${
        config.b2bType === 'informa_b2b' ? '&from=informa_b2b' : ''
      }`,
    )
  }

  const getIdeaInspirations = (params) => {
    return apiWrapper.get('/inspirations/ideas', params)
  }

  const getCart = (params) => {
    return apiWrapper.get(`/cart/${params.cartId}`)
  }

  const getWelcomeModalData = () => {
    return apiWrapper.get(
      `/tahu/popup-image${config.isB2b ? '?is_b2b=true' : ''}`,
    )
  }

  const wordingContent = (params) => {
    return apiWrapper.get('/tahu/getWordingContent', params)
  }

  const b2bProductOrder = () => {
    return apiWrapper.get('/b2b/product-order')
  }

  const b2bListOfContents = () => {
    return apiWrapper.get('/b2b/list-of-contents')
  }

  const b2bProductReco = (identifier) => {
    const { id, from, limit } = identifier

    return apiWrapper.get(
      `/b2b/b2bProductReco?type=${id}&from=${from}&limit=${limit}`,
    )
  }

  const b2bCmsRequest = (identifier) => {
    return apiWrapper.get(`/b2b/cms-request?identifier=${identifier}`)
  }

  const b2bDesktopBanner = (params) => {
    return apiWrapper.get('/b2b/b2bMainBanner', params)
  }

  const b2bInformaClient = () => {
    return apiWrapper.get('/b2b/informa-clients')
  }

  const getPersonalizedInvoiceReview = async () => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/review/personalized',
      undefined,
      headerWithToken(token),
    )
  }

  const allCategory = () => {
    return apiWrapper.get('/tahu/all-category')
  }

  return {
    b2bDesktopBanner,
    b2bProductReco,
    b2bCmsRequest,
    b2bListOfContents,
    b2bProductOrder,
    desktopBanner,
    mobileBanner,
    getEventDetail,
    getCmsBlockDetail,
    getCmsBlockDetailV2,
    getIdeaInspirations,
    getCart,
    getWelcomeModalData,
    b2bInformaClient,
    wordingContent,
    getPersonalizedInvoiceReview,
    allCategory,
  }
}

export default {
  create,
}
