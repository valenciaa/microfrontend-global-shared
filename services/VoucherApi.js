import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  // Voucher External
  const getVoucherPartnerDetail = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get(
      '/voucher-external/detail',
      data,
      headerWithToken(token),
    )
  }

  const redeemVoucherPartner = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/voucher-external/redeem',
      data,
      headerWithToken(token),
    )
  }

  return {
    getVoucherPartnerDetail,
    redeemVoucherPartner,
  }
}

export default {
  create,
}
