import apisauce from 'apisauce'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'
import localforage from 'localforage'

const create = () => {
  const headers = DEFAULT_HEADER

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const postQuotationUserV2 = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post('/quotation/v2/users', data, {
      headers: {
        Authorization: token ?? undefined,
      },
    })
  }

  const postQuotationProductsV2 = async ({ formData, quotationToken }) => {
    return apiWrapper.post('/quotation/v2/products', formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
      params: {
        cwt: quotationToken,
      },
    })
  }

  const shareQuotationeEmailV2 = (data) => {
    return apiWrapper.post('/quotation/v2/share-quotation', data)
  }

  const requestProformaInvoice = async (data) => {
    return apiWrapper.post('quotation/v2/request-proforma-invoice', data, {
      headers: { ...headers, Authorization: config.otpToken },
    })
  }

  const getIndustrySectors = async () => {
    return apiWrapper.get(
      '/quotation/v2/industry-sectors',
      {},
      {
        headers: { ...headers },
      },
    )
  }

  return {
    postQuotationUserV2,
    postQuotationProductsV2,
    requestProformaInvoice,
    getIndustrySectors,
    shareQuotationeEmailV2,
  }
}

export default { create }
