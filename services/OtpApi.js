import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getOtpChoices = async (data) => {
    return apiWrapper.get('/klk/active-otp-choices', data)
  }

  const manageOtp = async (data) => {
    const token = await tokenForage()
    const rsToken = await localforage.getItem('rsToken')
    if (token || rsToken) {
      return apiWrapper.post(
        '/klk/manage-otp-request',
        data,
        headerWithToken(
          data.otpRequestType === 'otp-reset-password' ||
            data.otpRequestType === 'otp-reset-password-odi'
            ? rsToken
            : token,
        ),
      )
    } else {
      return apiWrapper.post('/klk/manage-otp-request', data)
    }
  }

  const generateOTP = async (data) => {
    let token = await tokenForage()
// token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcl9pZCI6NTM5MDM4MywiaWF0IjoxNzMxNjY1MjcyLCJpc3MiOiJ3YXBpLnJ1cGFydXBhIn0.wBnQzR0DG1bs8Mjvd5pYwLRS4pBk_4WLuOUXFlqGdn8'
    if (token) {
      return apiWrapper.post('/membership/generate-otp', data,headerWithToken(token))
    }
  }

  const validateOTP = async (data) => {
    let token = await tokenForage()
// token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcl9pZCI6NTM5MDM4MywiaWF0IjoxNzMxNjY1MjcyLCJpc3MiOiJ3YXBpLnJ1cGFydXBhIn0.wBnQzR0DG1bs8Mjvd5pYwLRS4pBk_4WLuOUXFlqGdn8'
    if (token) {
      return apiWrapper.post('/membership/validate-otp', data,headerWithToken(token))
    }
  }

  const statusMembership = async (data) => {
    let token = await tokenForage()
    if (token) {
      return apiWrapper.post('/membership/status',data,headerWithToken(token))
    }
  }
  return {
    getOtpChoices,
    manageOtp,
    generateOTP,
    validateOTP,
    statusMembership
  }
}

export default {
  create,
}
