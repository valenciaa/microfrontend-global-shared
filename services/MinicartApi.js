import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { GetAuthDataB2b } from '../utils/GetAuthDataB2b'
import GetB2bType from '../utils/GetB2bType'

const create = () => {
  const headers = {
    'X-Company-Name': config.companyName,
    'X-Frontend-Type': config.environment,
    'user-platform': config.environment,
  }

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const headersMinicart = (Authorization, minicartToken) => {
    if (Authorization) {
      headers.Authorization = Authorization
    }

    if (minicartToken) {
      headers['X-Minicart'] = minicartToken
    }

    return {
      headers,
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 10000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      if (config.isB2b) {
        token = await GetAuthDataB2b('access_token')
      } else {
        token = await localforage.getItem('access_token')
      }
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getMinicartToken = () => {
    let minicartToken = window?.sessionStorage?.getItem('minicart_token')

    return minicartToken
  }

  const minicartForage = async () => {
    let minicartId = ''
    try {
      if (config.isB2b) {
        minicartId = await GetAuthDataB2b('minicart_id')
      } else {
        minicartId = await localforage.getItem('minicart_id')
      }
    } catch (error) {
      console.log(error)
    }
    return minicartId
  }

  const getMinicartItems = async (params = {}) => {
    const minicartId = await minicartForage()
    const token = await tokenForage()

    if (config.isB2b) {
      params.cart_type = GetB2bType()
    }

    return apiWrapper.get(
      `/cart/v4/${minicartId || ''}`,
      params,
      token ? headerWithToken(token) : { headers },
    )
  }

  const updateItemCheck = async ({ items = [], isChecked }) => {
    const minicartId = await minicartForage()
    const token = await tokenForage()
    const minicartToken = getMinicartToken()

    const payload = {
      minicart_id: minicartId,
      items,
      is_checked: isChecked,
    }

    return apiWrapper.patch(
      '/cart/v4/check',
      payload,
      headersMinicart(token, minicartToken),
    )
  }

  const updateItemQty = async ({ sku, packageId, qty, id }) => {
    const minicartId = await minicartForage()
    const token = await tokenForage()
    const minicartToken = getMinicartToken()

    const payload = {
      minicart_id: minicartId,
      sku,
      package_id: packageId,
      qty,
      id,
    }

    return apiWrapper.patch(
      '/cart/v4/qty',
      payload,
      headersMinicart(token, minicartToken),
    )
  }

  const deleteItems = async ({ items = [] }) => {
    const minicartId = await minicartForage()
    const token = await tokenForage()
    const minicartToken = getMinicartToken()

    const payload = {
      minicart_id: minicartId,
      items,
    }
    return apiWrapper.delete(
      '/cart/v4',
      {},
      {
        ...headersMinicart(token, minicartToken),
        data: payload,
      },
    )
  }

  const updateItemNote = async ({ sku, packageId, notes, id }) => {
    const minicartId = await minicartForage()
    const token = await tokenForage()
    const minicartToken = getMinicartToken()

    const payload = {
      minicart_id: minicartId,
      sku,
      package_id: packageId,
      customer_note: notes,
      id,
    }

    return apiWrapper.patch(
      '/cart/v4/notes',
      payload,
      headersMinicart(token, minicartToken),
    )
  }

  const postMinicartItem = async (body) => {
    const minicartId = await minicartForage()
    const token = await tokenForage()

    if (config.isB2b) {
      body.cart_type = GetB2bType()
    }

    // ? set minicart_id if body doesn't have minicart_id
    if (!body.minicart_id) {
      body.minicart_id = minicartId
    }

    return apiWrapper.post(
      '/cart/v3',
      body,
      token ? headerWithToken(token) : { headers },
    )
  }

  const updateItemStore = async ({
    sku,
    packageId,
    id,
    storeCode = '',
    deliveryMethod = '',
  }) => {
    const minicartId = await minicartForage()
    const token = await tokenForage()
    const minicartToken = getMinicartToken()

    const payload = {
      minicart_id: minicartId,
      sku,
      package_id: packageId,
      id,
      store_code: storeCode,
      is_change_store: true,
      delivery_method: deliveryMethod,
    }

    return apiWrapper.patch(
      '/cart/v4/shipping',
      payload,
      headersMinicart(token, minicartToken),
    )
  }

  const postWishlistItems = async (data) => {
    const token = await tokenForage()

    const { items, method } = data
    return apiWrapper.post(
      '/cart/v3/wishlist',
      { items, method },
      headerWithToken(token),
    )
  }

  const paymentPreprocessing = async (payload) => {
    const minicartId = await minicartForage()
    const token = await tokenForage()
    const minicartToken = getMinicartToken()

    const body = {
      minicart_id: minicartId,
      destination_cart_id: '',
      ...payload,
    }

    return apiWrapper.post(
      '/cart/v4/preprocess-payment-cart',
      body,
      headersMinicart(token, minicartToken),
    )
  }

  return {
    getMinicartItems,
    updateItemCheck,
    updateItemQty,
    deleteItems,
    updateItemNote,
    postMinicartItem,
    updateItemStore,
    postWishlistItems,
    paymentPreprocessing,
  }
}

export default { create }
