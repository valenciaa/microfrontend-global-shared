import apisauce from 'apisauce'
import config from '../../../config'
import isEmpty from 'lodash/isEmpty'
import { addHtml } from '../utils/UrlFormat'
import { DEFAULT_HEADER } from '../utils/constants'
const overrideShopInShop = config.overrideShopInShop
const apiURL = config.apiURL
const overrideApiURL = config.overrideApiURL

const create = (baseURL = config.apiURL) => {
  // let headers = {
  //   'X-Frontend-Type': config.environment,
  //   'X-Company-Name': !_.isEmpty(config.companyName) ? config.companyName : '',
  //   'user-platform': config.environment
  // }

  const headers = DEFAULT_HEADER

  const apiWrapper = apisauce.create({
    baseURL: baseURL,
    timeout: 60000,
    headers,
  })

  const overrideApiWrapper = apisauce.create({
    baseURL:
      overrideShopInShop || config.companyCode === 'AHI'
        ? overrideApiURL
        : apiURL,
    timeout: 10000,
    headers,
  })

  /**
   * Mendapatakan data inspirasi berdasar inentifier
   *
   * @param {string} identifier  {ex: rumah-tangga.html}
   * @returns
   */
  const getInspiration = (identifier) => {
    return apiWrapper.get(`inspirations/category/${identifier}`)
  }

  /**
   * get pcp products
   * ex: /product/category/rumah-tangga.html?keyword=&from=0&size=48&sort=lowestPrice&brands=&labels=&variant_attributes=&minprice=0&maxprice=99999999&isRuleBased=false&categoryId=2748&express_courier=&storeCode=&boost=
   *
   * @param   {[string]}  identifier
   * @param   {[object]}  params
   *
   * @return  {[response]}
   */
  const getProducts = (params) => {
    const tmpParams = { ...params }
    const identifier = tmpParams.identifier
    delete tmpParams.identifier

    if (!isEmpty(tmpParams.color)) {
      tmpParams.variant_attributes = 'color:' + tmpParams.color
      delete tmpParams.color
    }

    return apiWrapper.get(`product/v2/category/${identifier}`, tmpParams)
  }

  /**
   * mendapatkan data identifikasi route
   *
   * @param {string} identifier
   * @param {string} companyCode
   * @return {[Promise]}
   */
  const getRouteIndetifier = (identifier, companyCode) => {
    return apiWrapper.get(`/routes/${identifier}`, {
      company_code: companyCode,
    })
  }

  /**
   * mendapatkan data detail kategori
   * @param {string} urlKey   {ex: rumah-tangga/dekorasi-rumah}
   * @returns
   */
  const getDetailCategory = (urlKey) => {
    return apiWrapper.get('/category/detail', { urlKey })
  }

  /**
   * mendapatkan data navigasi untuk filter
   *
   * @param {Object} params  [{category, keyword, isRuleBased, categoryId, storeCode}]
   * @return
   */
  const getNavigationFilter = ({
    category = '',
    keyword = '',
    isRuleBased = '',
    categoryId = '',
    storeCode = '',
  }) => {
    let selectedApiWrapper = overrideApiWrapper

    if (!isEmpty(category)) {
      selectedApiWrapper = apiWrapper
      category = addHtml(category)
    }

    return selectedApiWrapper.get('/product/navigationfilters', {
      category,
      keyword,
      isRuleBased,
      categoryId,
      storeCode,
    })
  }

  /**
   * mendapatkan data list store
   *
   * @return  {[Promise]}
   */
  const getStore = (data = {}) => {
    const longitude = data.longitude ? `&longitude=${data.longitude}` : ''
    const latitude = data.latitude ? `&latitude=${data.latitude}` : ''
    return apiWrapper.get(
      `/store?ver=compact&store=${
        config.companyCode !== 'ODI' ? config.companyCode : ''
      }${longitude}${latitude}`,
    )
  }

  const getStoreByCity = (data) => {
    const cityId = data.cityId || ''
    const provinceCode = data.provinceCode || ''
    const longitude = data.longitude ? `&longitude=${data.longitude}` : ''
    const latitude = data.latitude ? `&latitude=${data.latitude}` : ''
    return apiWrapper.get(
      `/store?city_id=${cityId}&province_code=${provinceCode}&ver=compact&from=pcp&store=${
        config.companyCode !== 'ODI' ? config.companyCode : ''
      }${longitude}${latitude}`,
    )
  }

  /**
   * mendapatkan data suggestion dari keyword pencarian
   *
   * @param {string} keyword [keyword pencarian]
   * @returns
   */
  const suggestionBar = (keyword) => {
    return apiWrapper.get(`/instant/suggestion/${keyword}`)
  }

  // Aws Reco
  const mostSalesRecommendation = (data) => {
    return apiWrapper.get('/product/recomendation/sales', data)
  }

  return {
    getInspiration,
    getProducts,
    getRouteIndetifier,
    getDetailCategory,
    getNavigationFilter,
    getStore,
    getStoreByCity,
    suggestionBar,
    mostSalesRecommendation,
  }
}

export default { create }
