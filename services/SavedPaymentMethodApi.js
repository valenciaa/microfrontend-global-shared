import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'
import GetParam from '../utils/GetParam'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''

    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }

    return token
  }

  const getSavedCcList = async () => {
    const token = await tokenForage()

    return apiWrapper.get('/saved-cc/cards', {}, headerWithToken(token))
  }

  const getCcRedirectUrl = async (data) => {
    const token = await tokenForage()

    return apiWrapper.post(
      '/saved-cc/v2/redirect-url',
      data,
      headerWithToken(token),
    )
  }

  const deleteSavedCc = async (data) => {
    const token = await tokenForage()

    return apiWrapper.put('/saved-cc/delete-card', data, headerWithToken(token))
  }

  const gopayInit = async (param) => {
    const token = await tokenForage()

    return apiWrapper.get(
      '/saved-payment-method/gopay/init',
      param,
      headerWithToken(token),
    )
  }

  const gopayBind = (data) => {
    return apiWrapper.post(
      '/saved-payment-method/gopay/bind',
      data,
      headerWithToken(GetParam('access')?.split('__')?.[1]),
    )
  }

  const getGopayAccount = async () => {
    const token = await tokenForage()

    return apiWrapper.get(
      '/saved-payment-method/gopay/account',
      {},
      headerWithToken(token),
    )
  }

  const gopayUnlink = async () => {
    const token = await tokenForage()

    return apiWrapper.post(
      '/saved-payment-method/gopay/unlink',
      {},
      headerWithToken(token),
    )
  }

  const gopayErrorLog = async (data) => {
    const token = await tokenForage()

    return apiWrapper.post(
      '/saved-payment-method/gopay/error-log',
      data,
      headerWithToken(token),
    )
  }

  return {
    getSavedCcList,
    getCcRedirectUrl,
    deleteSavedCc,
    gopayInit,
    gopayBind,
    getGopayAccount,
    gopayUnlink,
    gopayErrorLog,
  }
}

export default {
  create,
}
