import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getPinStatus = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/klk/pin-status', data, headerWithToken(token))
  }

  const managePin = async (data) => {
    const token = await tokenForage()
    return apiWrapper.put('/klk/pin', data, headerWithToken(token))
  }

  return {
    getPinStatus,
    managePin,
  }
}

export default {
  create,
}
