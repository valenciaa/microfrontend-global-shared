import apisauce from 'apisauce'
import localforage from 'localforage'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL,
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await localforage.getItem('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const getDedicatedMemberTier = async (data) => {
    // this API only for return level text membership
    const token = await tokenForage()
    return apiWrapper.get('/klk/tier-level', data, headerWithToken(token))
  }

  return {
    getDedicatedMemberTier,
  }
}

export default {
  create,
}
