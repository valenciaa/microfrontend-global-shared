import apisauce from 'apisauce'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'

const create = () => {
  const headers = DEFAULT_HEADER

  const apiWrapper = apisauce.create({
    // BaseURL sementara, sambil nunggu wrapper jadi
    baseURL: `${config.apiURL}promo`,
    // baseURL: `http://localhost:8000/promo`,
    timeout: 60000,
    headers,
  })

  const getBannerUrl = () => {
    // TODO Ganti ke wrapper url
    return apiWrapper.get('/banner-url')
  }

  const getFilterData = (params) => {
    // TODO Ganti ke wrapper url
    return apiWrapper.get('/filter-data', params)
  }

  const getVouchers = (params) => {
    // TODO Ganti ke wrapper url
    return apiWrapper.get('/vouchers', params)
  }

  const getFilterDataExist = (params) => {
    return apiWrapper.get('/filter-data-exist', params)
  }

  return {
    getBannerUrl,
    getFilterData,
    getVouchers,
    getFilterDataExist,
  }
}

export default {
  create,
}
