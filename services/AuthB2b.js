import apisauce from 'apisauce'
import config from '../../../config'
import { DEFAULT_HEADER } from '../utils/constants'
import { GetAuthDataB2b } from '../utils/GetAuthDataB2b'

const create = () => {
  const headers = DEFAULT_HEADER

  const apiWrapper = apisauce.create({
    baseURL: config.apiURL + (config.isB2b ? '' : 'klk'),
    timeout: 60000,
    headers,
  })

  const tokenForage = async () => {
    let token = ''
    try {
      token = await GetAuthDataB2b('access_token')
    } catch (error) {
      console.log(error)
    }
    return token
  }

  const headerWithToken = (Authorization) => {
    return {
      headers: { ...headers, Authorization },
    }
  }

  // B2B Endpoint
  const quickRegistrationCheck = (data) => {
    return apiWrapper.post('auth/b2b/quick-registration-check', data)
  }

  const mergeAccount = async (data) => {
    const { token, ...payload } = data
    return apiWrapper.post(
      'auth/b2b/merge-account',
      payload,
      headerWithToken(token),
    )
  }

  const getListAccountsB2b = async (params) => {
    const token = await tokenForage()
    return apiWrapper.get('auth/b2b/accounts', params, headerWithToken(token))
  }

  const getAdditionalLoginData = async (params) => {
    const token = await tokenForage()
    return apiWrapper.post(
      'auth/b2b/additional-login-data',
      params,
      headerWithToken(token),
    )
  }

  const checkAuthDataB2b = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/auth/check-otp-auth-b2b',
      data,
      headerWithToken(token),
    )
  }

  const loginB2b = (data) => {
    return apiWrapper.post('/auth/b2b/login', data)
  }

  const registerInformaB2b = async (data) => {
    const token = await tokenForage()
    return apiWrapper.post(
      '/auth/b2b/register-b2b',
      data,
      headerWithToken(token),
    )
  }

  const getAccount = async (data) => {
    const token = await tokenForage()
    return apiWrapper.get('/auth/b2b/user', data, headerWithToken(token))
  }

  return {
    quickRegistrationCheck,
    mergeAccount,
    getListAccountsB2b,
    getAdditionalLoginData,
    checkAuthDataB2b,
    loginB2b,
    registerInformaB2b,
    getAccount,
  }
}

export default {
  create,
}
