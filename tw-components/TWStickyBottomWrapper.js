import clsx from 'clsx'

export default function TWStickyBottomWrapper({
  additionalClass = 'tw-sticky tw-p-4',
  children,
}) {
  return (
    <div
      className={clsx(
        additionalClass,
        'tw-shadow-bank-account-card tw-bg-white tw-left-0 tw-bottom-0 tw-mt-auto z-[1] tw-w-full',
      )}
    >
      {children}
    </div>
  )
}
