import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import TWBaseModal from './TWBaseModal'

export default function TWLoadingModal({ message = 'mohon menunggu...' }) {
  return (
    <TWBaseModal isShow animationType='fade' modalType='middle-small'>
      <div className='tw-flex tw-flex-col tw-gap-1 tw-items-center tw-px-16'>
        <CustomLazyLoadImage
          src={config.assetsURL + 'images/auth/bouncing-dot.gif'}
          height={40}
          width={80}
        />
        <div className='tw-text-grey-100 tw-text-sm tw-whitespace-nowrap'>
          {message}
        </div>
      </div>
    </TWBaseModal>
  )
}
