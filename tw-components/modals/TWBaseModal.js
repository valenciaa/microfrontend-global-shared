import clsx from 'clsx'
import { useEffect, useState } from 'react'
import { createPortal } from 'react-dom'
import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'

const setAnimation = (animationType, isAnimationRun) => {
  switch (animationType) {
    // ? flyin animation for non full screen modal
    case 'flyin':
      return clsx(
        isAnimationRun
          ? 'tw-transition-transform -tw-translate-y-1/2'
          : 'tw-transition-transform tw-translate-y-80',
      )
    case 'fade':
      return clsx(
        isAnimationRun
          ? 'tw-transition-opacity tw-opacity-100'
          : 'tw-transition-opacity tw-opacity-0',
      )
    // ? flyin animation for full screen modal
    case 'flyin-full-screen':
      return clsx(
        isAnimationRun
          ? 'tw-transition-transform -tw-translate-y-0'
          : 'tw-transition-transform tw-translate-y-full',
      )
    default:
      return undefined
  }
}

const setModalType = (modalType, customTop) => {
  const nonFullScreenModalStyle = clsx(
    'tw-absolute tw-transform tw-left-1/2 -tw-translate-x-1/2',
  )
  switch (modalType) {
    case 'bottom':
      return clsx('tw-bottom-0', nonFullScreenModalStyle)
    case 'center':
      return clsx('tw-top-[5vh]', nonFullScreenModalStyle)
    case 'deals-center':
      return clsx('tw-top-[15%]', nonFullScreenModalStyle)
    case 'middle-small':
      return clsx(
        'tw-top-1/2 -tw-translate-y-1/2 tw-px-2',
        nonFullScreenModalStyle,
      )
    case 'custom':
      return clsx(customTop, nonFullScreenModalStyle)
    case 'full':
      return clsx('tw-absolute tw-bottom-0 tw-left-0 tw-h-full')
    default:
      return clsx('tw-top-0', nonFullScreenModalStyle)
  }
}

const setPadding = (modalType, customPadding) => {
  if (customPadding) {
    return clsx(customPadding)
  }

  switch (modalType) {
    case 'full':
      return clsx('tw-p-0')
    default:
      return clsx('tw-p-3')
  }
}

const setRounded = (modalType) => {
  switch (modalType) {
    case 'full':
      return clsx('tw-rounded-none')
    case 'bottom':
      return clsx('tw-rounded-t-lg')
    default:
      return clsx('tw-rounded-lg')
  }
}

const setWidth = (isFullWidth) => {
  return clsx(isFullWidth ? 'tw-w-full' : '')
}

const setHeight = (modalType) => {
  return clsx(modalType === 'full' ? 'tw-h-full' : '')
}

const setFullOverflowCenter = (isFullOverflowCenter) => {
  return clsx(isFullOverflowCenter ? 'tw-w-full tw-flex tw-justify-center' : '')
}

export default function TWBaseModal({
  isShow = false,
  setIsShow = () => {},
  animationType = 'fade',
  children,
  childrenWrapperClass,
  modalType,
  customTop,
  customPadding,
  isFullWidth = false,
  needFloatingCloseButtonElement = false,
  backdropOnClose = false,
  customModalBottomContentHeight = null, // this prop if for adjust modal type bottom height (use case in ModalReadMore.js)
  additionalModalFullClass = '', // for modal type full adjusting button sticky bottom and search bar sticky (use case for AddressAddContent.js in mobile)
  customZIndex = clsx('tw-z-[2000]'),
  isFullOverflowCenter = false, // for modal that need to be full modalType because of scroll and need to be centered
}) {
  const [isAnimationRun, setIsAnimationRun] = useState(false)

  useEffect(() => {
    if (isShow) {
      document.body.style.overflow = 'hidden'
      setTimeout(() => {
        setIsAnimationRun(true)
      }, 100)
    } else {
      setIsAnimationRun(false)
    }

    // ? Change this because we have modal over modal, so we must do this because we don't want primary modal have overflow when close secondary modal
    return () => {
      // Search element named .tw-modal
      const el = Array?.from?.(document.querySelectorAll('.tw-modal'))

      // When not find any element .tw-modal remove the overflow hidden style
      if (!el || el?.length < 1) {
        document.body.style.removeProperty('overflow')
      }
    }
  }, [isShow])

  return createPortal(
    <>
      <div
        className={clsx(
          'tw-modal tw-transition',
          isShow
            ? `tw-fixed tw-left-0 tw-right-0 tw-bottom-0 tw-h-full tw-w-full ${customZIndex}`
            : 'tw-hidden',
        )}
      >
        <div
          className={clsx(
            'tw-h-full tw-w-full tw-bg-black tw-transition-opacity tw-duration-100 tw-z-40 tw-opacity-50',
          )}
          onClick={() => {
            if (backdropOnClose && setIsShow) {
              setIsShow(false)
            }
          }}
        />
        <div
          className={clsx(
            `tw-z-50 duration-1000 tw-max-w-container`,
            setAnimation(animationType, isAnimationRun),
            setModalType(modalType, customTop),
            setWidth(isFullWidth),
            setFullOverflowCenter(isFullOverflowCenter),
          )}
        >
          {needFloatingCloseButtonElement ? (
            <div
              className='tw-absolute -tw-top-14 tw-right-4 tw-bg-white tw-p-2 tw-rounded-full'
              onClick={() => setIsShow(false)}
            >
              <CustomLazyLoadImage
                alt='close-btn'
                height={24}
                id='close-btn'
                src={config.assetsURL + 'icon/close-dark.svg'}
                width={24}
              />
            </div>
          ) : null}
          <div
            className={clsx(
              'tw-bg-white',
              setPadding(modalType, customPadding),
              setRounded(modalType),
              setHeight(modalType),
            )}
          >
            <div
              className={clsx(
                modalType === 'full'
                  ? `tw-h-full tw-overflow-auto ${additionalModalFullClass}`
                  : '',
                modalType === 'bottom' && customModalBottomContentHeight
                  ? customModalBottomContentHeight
                  : '',
                childrenWrapperClass || '',
              )}
            >
              {children}
            </div>
          </div>
        </div>
      </div>
    </>,
    document.body,
  )
}
