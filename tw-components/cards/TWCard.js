import { useEffect, useState } from 'react'
import clsx from 'clsx'
import Skeleton from 'react-loading-skeleton'

import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import TWButton from '../buttons/TWButton'

export default function TWCard({
  key,
  images,
  name,
  sku,
  // urlKey,
  specialPrice,
  normalPrice,
  discount,
  coinPrice,
  label,
  reviewRating,
  reviewTotal,
  isButton = false,
  buttonText = '',
  buttonAction = () => {},
  urlRedirect,
}) {
  const [logCustom, setLogCustom] = useState(false)

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search)
    const logCustom = urlParams.get('custom')
    setLogCustom(logCustom === 'true')
  }, [])

  return (
    <div
      key={key}
      className={clsx(
        'tw-flex tw-flex-col tw-justify-between tw-shadow tw-rounded-b-lg',
        'tw-h-full',
        'tw-cursor-pointer',
      )}
    >
      {logCustom && config.node_env === 'development' ? (
        <div className='tw-w-full tw-text-center tw-p-2 tw-bg-red-20'>
          SKU : <b>{sku}</b>
        </div>
      ) : null}

      <a href={urlRedirect}>
        {/* Image Url */}
        <CustomLazyLoadImage
          src={config.imageURL + images[0].image_url}
          alt='product'
          width='100%'
          className='tw-object-contain tw-rounded tw-w-full tw-aspect-square'
          placeholder={<Skeleton />}
        />
        <div className='tw-p-2 tw-flex tw-flex-col tw-gap-1'>
          {/* Product Name */}
          <div className='tw-text-sm tw-text-grey-100 tw-line-clamp-2'>
            {name}
          </div>

          {/* Prices */}
          {specialPrice && discount ? (
            <div className='tw-flex tw-items-center tw-gap-1'>
              <div className='tw-text-xxs tw-text-grey-30 tw-line-through'>
                Rp{normalPrice.toLocaleString('id')}
              </div>
              <div className='tw-text-xxs tw-text-white tw-text-center tw-bg-red-50 tw-px-1 tw-py-0.5 tw-rounded-full'>
                {discount}%
              </div>
            </div>
          ) : null}
          <div className='tw-text-sm tw-text-grey-100 tw-font-bold'>
            Rp
            {specialPrice
              ? specialPrice.toLocaleString('id')
              : normalPrice.toLocaleString('id')}
          </div>

          {/* Coins */}
          {coinPrice ? (
            <div className='tw-bg-yellow-10 tw-flex tw-gap-1 tw-w-fit tw-p-1 tw-rounded-lg'>
              <CustomLazyLoadImage
                src='https://cdn.ruparupa.io/media/promotion/ruparupa/asset-cohesive/rev/Koin-Navbar-Active.png'
                alt='coin logo'
                className='tw-h-4 tw-w-4'
                placeholder={<Skeleton height={16} />}
              />
              <div className='tw-text-xs tw-font-bold tw-text-yellow-50'>
                {coinPrice.toLocaleString('id')} Koin
              </div>
            </div>
          ) : null}

          {/* Label */}
          {label ? (
            <div className='tw-flex tw-items-center tw-gap-0.5'>
              <img
                src={config.assetsURL + 'icon/info-red.svg'}
                alt='label logo'
                className='tw-h-4 tw-w-4'
              />
              <div className='tw-text-xs tw-font-medium tw-text-red-50 tw-mt-0.5'>
                {label}
              </div>
            </div>
          ) : null}

          {/* Review Rating */}
          {reviewRating && reviewTotal ? (
            <div className='tw-flex tw-items-center tw-gap-1 tw-text-grey-50 tw-text-xxs'>
              <img
                src={`${config.assetsURL}icon/star-rating.svg`}
                alt='star-rate'
                id='star-rate'
                className='tw-h-4 tw-w-4'
              />
              <div>{reviewRating}</div>
              <div className='tw-h-3 tw-border-l tw-border-solid tw-border-grey-30' />
              <span>{reviewTotal} (ulasan)</span>
            </div>
          ) : null}
        </div>
      </a>

      {isButton && buttonText ? (
        <TWButton
          type='primary-border'
          size='medium'
          width='tw-w-full'
          handleOnClick={() => buttonAction()}
        >
          {buttonText}
        </TWButton>
      ) : null}
    </div>
  )
}
