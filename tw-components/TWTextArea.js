import clsx from 'clsx'
import React from 'react'
import config from '../../../config'

const textAreaStyle = {
  desktop: 'tw-text-sm',
  mobile: 'tw-text-xs',
}

export default function TWTextArea({
  errorMessage,
  maxLength = 250,
  placeholder,
  value,
  setValue,
}) {
  const textCount = (value?.length ?? 0) + '/' + maxLength

  return (
    <div className={clsx('tw-flex tw-flex-col tw-gap-1')}>
      <div className='tw-flex'>
        <textarea
          className={clsx(
            'focus:tw-border-blue-50 tw-border tw-border-solid tw-border-grey-20 tw-px-3 tw-py-2.5 tw-outline-none tw-rounded-lg tw-w-full tw-resize-none',
            textAreaStyle[config.environment],
            'tw-h-[130px] tw-max-h-[130px]',
            value?.length > 0 ? 'tw-text-grey-100' : 'tw-text-grey-30',
          )}
          maxLength={maxLength}
          placeholder={placeholder}
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
      </div>

      <div className='tw-flex tw-justify-between'>
        {errorMessage && (
          <p className={clsx('tw-text-red-50 tw-text-xs')}>{errorMessage}</p>
        )}

        <p className='tw-text-grey-40 ui-text-3 tw-ml-auto'>{textCount}</p>
      </div>
    </div>
  )
}
