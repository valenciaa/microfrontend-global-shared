import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'

export default function TWHeaderWithoutSearch({
  title,
  isBackButton,
  backAction,
}) {
  return (
    <div className='tw-px-4 tw-py-3 tw-flex tw-gap-3 tw-shadow'>
      {isBackButton ? (
        <CustomLazyLoadImage
          src={config.assetsURL + 'icon/arrow-left.svg'}
          width={24}
          height={24}
          alt='Back Button'
          onClick={() => {
            if (backAction) {
              backAction()
            }
          }}
        />
      ) : null}
      <div className='tw-font-bold tw-text-grey-100 tw-li'>{title}</div>
    </div>
  )
}
