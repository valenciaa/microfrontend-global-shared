import React from 'react';
import PriceConverter from '../../../utils/PriceConverter';
import config from '../../../../../config'

export default function CardSpecialMember({ promo, product }) {
    const currentDate = new Date();
    let promoPcpJual = {};
    const iconCoin = 'https://cdn.ruparupa.io/media/promotion/ruparupa/upcdn/property-coin.png';
    const iconMember = 'https://cdn.ruparupa.io/media/promotion/ruparupa/upcdn/property-member.png';

    function isDateInRange(start, end) {
        const startDate = new Date(start);
        const endDate = new Date(end);
        return startDate <= currentDate && currentDate <= endDate;
    }

    // To find the lowest price within the valid date range
    function getLowestValidPrice(prices) {
        const validPrices = prices.filter(price => price.amount > 0 && isDateInRange(price.startDate, price.endDate));
        if (validPrices.length === 0) {return null}
        return Math.min(...validPrices.map(price => price.amount));
    }

    // Define prices for koin and member tiers
    const koinPrices = [
        { amount: product?.price_koin_silver?.price, startDate: product?.price_koin_silver?.start_date, endDate: product?.price_koin_silver?.end_date },
        { amount: product?.price_koin_gold?.price, startDate: product?.price_koin_gold?.start_date, endDate: product?.price_koin_gold?.end_date },
        { amount: product?.price_koin_platinum?.price, startDate: product?.price_koin_platinum?.start_date, endDate: product?.price_koin_platinum?.end_date }
    ];

    const memberPrices = [
        { amount: product?.price_member_silver?.price, startDate: product?.price_member_silver?.start_date, endDate: product?.price_member_silver?.end_date },
        { amount: product?.price_member_gold?.price, startDate: product?.price_member_gold?.start_date, endDate: product?.price_member_gold?.end_date },
        { amount: product?.price_member_platinum?.price, startDate: product?.price_member_platinum?.start_date, endDate: product?.price_member_platinum?.end_date }
    ];

    // Get the lowest valid prices
    const lowestKoinPrice = getLowestValidPrice(koinPrices);
    const lowestMemberPrice = getLowestValidPrice(memberPrices);

    // Determine which promo to display
    if (lowestKoinPrice !== null && lowestMemberPrice === null) {
        promoPcpJual = {
            promo_value: PriceConverter(lowestKoinPrice, false, true),
            icon_url: iconCoin
        };
    } else if (lowestKoinPrice === null && lowestMemberPrice !== null) {
        promoPcpJual = {
            promo_value: PriceConverter(lowestMemberPrice),
            icon_url: iconMember
        };
    } else if (lowestKoinPrice !== null && lowestMemberPrice !== null) {
        promoPcpJual = {
            promo_value: PriceConverter(Math.min(lowestKoinPrice, lowestMemberPrice)),
            icon_url: lowestMemberPrice < lowestKoinPrice ? iconMember : iconCoin
        };
    }

    return (
        <div className={config.environment === 'mobile' ? '' : 'tw-p-2'}>
            <div className='tw-bg-yellow-10 tw-rounded-lg tw-flex tw-w-max tw-p-1 tw-h-6 tw-mt-1'>
                <img className='tw-mx-1' src={promo?.icon_url || promoPcpJual?.icon_url} alt="Promo Icon" /> 
                <p className='tw-font-bold tw-text-yellow-50 tw-mr-1 tw-text-xs'>{promo?.promo_value || promoPcpJual?.promo_value}</p>
            </div>
        </div>
    );
}
