import clsx from 'clsx'
import config from '../../../../config'

const buColorMap = {
  text: {
    ruparupa: clsx('tw-text-ruparupa'),
    ace: clsx('tw-text-ace'),
    informa: clsx('tw-text-informa'),
    toyskingdomonline: clsx('tw-text-toyskingdomonline'),
  },
  textHover: {
    ruparupa: clsx('hover:tw-text-ruparupa'),
    ace: clsx('hover:tw-text-ace'),
    informa: clsx('hover:tw-text-informa'),
    toyskingdomonline: clsx('hover:tw-text-toyskingdomonline'),
  },
  bg: {
    ruparupa: clsx('tw-bg-ruparupa'),
    ace: clsx('tw-bg-ace'),
    informa: clsx('tw-bg-informa'),
    toyskingdomonline: clsx('tw-bg-toyskingdomonline'),
  },
  bgHoverPrimary: {
    ruparupa: clsx('hover:tw-bg-orange-60'),
    ace: clsx('hover:tw-bg-[#ff9292]'),
    informa: clsx('hover:tw-bg-[#076cd1]'),
    toyskingdomonline: clsx('hover:tw-bg-orange-60'),
  },
  bgHover: {
    ruparupa: clsx('hover:tw-bg-orange-60'),
    ace: clsx('hover:tw-bg-ace'),
    informa: clsx('hover:tw-bg-informa'),
    toyskingdomonline: clsx('hover:tw-bg-toyskingdomonline'),
  },
  border: {
    ruparupa: clsx('tw-border-ruparupa'),
    ace: clsx('tw-border-ace'),
    informa: clsx('tw-border-informa'),
    toyskingdomonline: clsx('tw-border-toyskingdomonline'),
  },
}

const setType = (type, companyName) => {
  switch (type) {
    // ? Disabled
    case 'disabled':
      return clsx(
        'tw-text-grey-40 tw-bg-grey-20 tw-border-none tw-cursor-not-allowed',
      )

    // ? PRIMARY
    case 'primary':
      return clsx(
        'tw-text-white tw-border',
        `${buColorMap.bg[companyName]} ${buColorMap.border[companyName]}`,
        `${buColorMap.bgHoverPrimary[companyName]}`,
      )
    case 'primary-border':
      return clsx(
        'tw-bg-white tw-border',
        `${buColorMap.text[companyName]} ${buColorMap.border[companyName]}`,
        `${buColorMap.bgHover[companyName]} hover:tw-text-white`,
      )
    case 'primary-border--ghost':
      return clsx(
        'tw-bg-white tw-border tw-border-grey-20',
        `${buColorMap.text[companyName]}`,
        `${buColorMap.bgHover[companyName]} hover:tw-text-white`,
      )
    case 'primary-outline':
      return clsx(
        'tw-text-white tw-border tw-border-white hover:tw-bg-white',
        `${buColorMap.bg[companyName]}`,
        `${buColorMap.textHover[companyName]}`,
      )

    // ? LINK
    case 'link-primary':
      return clsx(
        'tw-bg-white tw-border-none',
        `${buColorMap.text[companyName]}`,
      )
    case 'link-primary-transparent':
      return clsx(
        'tw-bg-transparent tw-border-none',
        `${buColorMap.text[companyName]}`,
        'disabled:tw-cursor-not-allowed disabled:tw-color-grey-20',
        'tw-py-0 tw-px-0',
      )
    case 'link-secondary':
      return clsx('tw-bg-white tw-text-blue-50 tw-border-none')
    case 'secondary-border_ghost':
      return clsx(
        'tw-bg-white tw-border tw-border-grey-50 tw-text-grey-50',
        config.environment === 'desktop'
          ? 'hover:tw-text-white hover:tw-bg-grey-50'
          : 'focus:tw-bg-grey-50 focus:tw-text-white active:tw-bg-grey-50 active:tw-text-white',
      )
  }
}

const sizeObj = {
  xsmall: clsx('tw-text-xxs'),
  small: clsx('tw-text-xs'),
  medium: clsx('tw-text-sm'),
  big: clsx('tw-text-base tw-font-semibold'),
}

// type (primary, primary-border, secondary, secondary-border, disabled)
// size (big, medium, small)
// additionalClass (out of button scss)
// active (for toggle between initial type and the ghost one [active and not active])

export default function TWButton({
  type = 'primary',
  size = 'big',
  name,
  handleOnClick,
  // active,
  fetching,
  additionalClass,
  width,
  maxWidth,
  children,
  onMouseEnter,
  onMouseLeave,
  onFocus,
  onBlur,
  withCaptcha,
  disabled,
  isSubmit = false,
  customPadding = 'tw-py-2 tw-px-3',
  isRounded = false,
}) {
  const companyName = config.companyNameCSS || 'ruparupa'

  const currentType = setType(type, companyName)
  const currentSize = sizeObj[size]

  return (
    <button
      className={clsx(
        'tw-font-medium tw-rounded tw-leading-normal',
        'disabled:tw-text-grey-40 disabled:tw-bg-grey-20 disabled:tw-border-none disabled:tw-cursor-not-allowed',
        currentType,
        currentSize,
        additionalClass,
        width,
        maxWidth,
        customPadding,
        isRounded ? 'tw-rounded-full' : '',
      )}
      data-action={withCaptcha && 'submit'}
      disabled={fetching || disabled || false}
      id={name}
      name={name}
      type={isSubmit ? 'submit' : 'button'}
      onBlur={onBlur}
      onClick={(e) => {
        if (handleOnClick) {
          handleOnClick(e)
        }
      }}
      onFocus={onFocus}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      {children}
    </button>
  )
}
