import React from 'react'
import clsx from 'clsx'
import config from '../../../config'

const background = {
  'wishlist-empty-stock': clsx('tw-bg-red-50'),
}

const text = {
  'wishlist-empty-stock': 'Stok Habis',
}

const textStyle = {
  desktop: 'button-small-text',
  mobile: 'button-medium-text',
}

export default function TWCardFloatingBadge({ type }) {
  return (
    <div
      className={clsx(
        'tw-absolute tw-flex tw-items-center tw-justify-center tw-w-[137px] tw-h-[29.7px] tw-z-[2] -tw-rotate-45 tw-top-[22px] -tw-left-8',
        background[type],
      )}
    >
      <p className={clsx('tw-text-white', textStyle[config.environment])}>
        {text[type]}
      </p>
    </div>
  )
}
