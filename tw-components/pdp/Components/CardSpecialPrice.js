import React from 'react'
import config from '../../../../../config'
import TWBaseModal from '../../modals/TWBaseModal'
import { useProductDetailContext } from '../../../../../src/pdp/contexts/ProductDetailContext'

export default function CardSpecialPrice({
  collectionPromo,
  onClickCard,
  isOpenModal,
  priceZoneModalContent,
  setIsOpenModal,
  setSelectedPromo,
}) {
  const { state: productDetailContext } = useProductDetailContext()
  const isDesktop = config.environment === 'desktop'
  const flexStyle = collectionPromo?.length === 1 || isDesktop

  return (
    <div>
      <div className='tw-flex tw-gap-2 tw-items-stretch'>
        {collectionPromo?.map((card, index) => {
          if (flexStyle) {
            return (
              <div
                key={index}
                onClick={onClickCard(
                  card,
                  productDetailContext?.results?.productDetail?.url_key,
                  productDetailContext?.results?.productDetail?.variants?.[0]
                    ?.sku,
                )}
                className='tw-w-full tw-min-h-member tw-p-3 tw-cursor-pointer tw-bg-yellow-10 tw-rounded tw-justify-start tw-items-start tw-gap-2 tw-inline-flex'
              >
                <img
                  loading='lazy'
                  src={card?.icon_url}
                  alt='icon promo'
                  className='tw-w-6 tw-h-6 tw-relative'
                />
                <div className='tw-grow tw-shrink tw-basis-0 tw-flex-col tw-justify-start tw-items-start tw-gap-1 tw-inline-flex'>
                  <div className='tw-self-stretch tw-justify-between tw-items-start tw-inline-flex'>
                    <div className='tw-text-gray-800 tw-text-sm tw-font-bold tw-leading-5'>
                      {card?.promo_value}
                    </div>
                    <img
                      loading='lazy'
                      src={card?.arrow_url}
                      className='tw-w-5 tw-h-5'
                      alt=''
                    />
                  </div>
                  <div className='tw-self-stretch tw-text-zinc-600 tw-text-xs tw-font-normal tw-leading-none'>
                    {card?.description}
                  </div>
                </div>
              </div>
            )
          }

          return (
            <div
              className='tw-flex tw-flex-col tw-flex-1 tw-p-2 tw-bg-yellow-10 tw-rounded tw-min-h-[51px] tw-w-full'
              key={index}
              onClick={onClickCard(
                card,
                productDetailContext?.results?.productDetail?.url_key,
                productDetailContext?.results?.productDetail?.variants?.[0]
                  ?.sku,
              )}
            >
              <div className='tw-flex tw-gap-5 tw-justify-between'>
                <img
                  loading='lazy'
                  src={card?.icon_url}
                  alt='icon promo'
                  className='tw-w-6 tw-h-6'
                />
                <img
                  loading='lazy'
                  src={card?.arrow_url}
                  className='tw-w-4 tw-h-4'
                  alt=''
                />
              </div>
              <div className='tw-mt-2 tw-text-xs tw-font-bold tw-leading-5 tw-text-gray-800'>
                {card?.promo_value}
              </div>
              <div className='tw-text-xs tw-leading-4 tw-text-zinc-600'>
                {card?.description}
              </div>
            </div>
          )
        })}
      </div>

      {isOpenModal && isDesktop && (
        <TWBaseModal
          animationType='fade'
          isShow={isOpenModal}
          modalType='middle-small'
        >
          {priceZoneModalContent()}
        </TWBaseModal>
      )}

      {isOpenModal && !isDesktop && (
        <TWBaseModal
          isFullWidth
          needFloatingCloseButtonElement
          animationType='fade'
          isShow={isOpenModal}
          modalType='bottom'
          setIsShow={() => {
            setIsOpenModal(!isOpenModal)
            setSelectedPromo('')
          }}
        >
          {priceZoneModalContent()}
        </TWBaseModal>
      )}
    </div>
  )
}
