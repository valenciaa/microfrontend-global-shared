import { Swiper, SwiperSlide } from 'swiper/react'
import Link from 'next/link'
import config from '../../../../../config'
import Skeleton from 'react-loading-skeleton'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import { Fragment, useState } from 'react'
import ShowroomPlayground from '../SubComponents/ShowroomPlayground'

export default function VirtualShowSwiper({
  showroomData,
  isLoading,
  setProductData,
  setToggleClipboardInfobox,
  setToggleErrorClipboardInfobox,
  setErrMessage,
  setShareProductData,
}) {
  const [activeProduct, setActiveProduct] = useState(null)

  const bottomSection = (vsData, isLoading) => {
    const { link = '' } = vsData?.button || ''

    const onClickLink = () => {
      if (link) {
        mixpanelTrack('Shop The Look', {
          'Section Name': vsData?.title,
          'Click Location': 'image',
          'Location': 'PDP',
          'ITM Device': config?.environment,
        })
      }
    }

    return (
      <div
        className='tw-flex tw-w-full ui-text-2 tw-items-center tw-gap-1'
        onClick={onClickLink}
      >
        {!isLoading ? (
          link ? (
            <>
              <Link
                href={`${link}?itm_source=shop-the-look-title-${vsData?.title}&itm_campaign=PDP&itm_device=${config?.environment}`}
              >
                {vsData.title}
              </Link>
              <svg width='16' height='16' fill='none'>
                <path
                  d='m6 12 4-4-4-4'
                  stroke='currentColor'
                  stroke-width='2'
                  stroke-linecap='round'
                  stroke-linejoin='round'
                />
              </svg>
            </>
          ) : (
            vsData.title
          )
        ) : (
          <Skeleton height={24} width={150} />
        )}
      </div>
    )
  }

  const showRoomWrapper = () => {
    const addProperty = {
      setToggleClipboardInfobox,
      setToggleErrorClipboardInfobox,
      setErrMessage,
      activeProduct,
      setProductData,
      setShareProductData,
    }

    const renderSlide = (vsData, vsIndex, isLoading) => (
      <SwiperSlide key={vsIndex} className='tw-gap-2 tw-flex tw-flex-col'>
        <ShowroomPlayground
          vsData={vsData}
          vsIndex={vsIndex}
          isLoading={isLoading}
          setActiveProduct={setActiveProduct}
          {...addProperty}
        />
        {bottomSection(isLoading ? null : vsData, isLoading)}
      </SwiperSlide>
    )

    return (
      <>
        {isLoading
          ? Array(2)
              ?.fill('')
              ?.map((_, vsIndex) => (
                <Fragment key={vsIndex}>
                  {renderSlide(null, vsIndex, true)}
                </Fragment>
              ))
          : showroomData?.data?.map((vsData, vsIndex) =>
              renderSlide(vsData, vsIndex, false),
            )}
      </>
    )
  }

  const slidePerView = config?.environment === 'mobile' ? 1.2 : 2
  return (
    <div className=' tw-w-full tw-h-full product-slider__sdl'>
      <Swiper
        slidesPerView={slidePerView}
        spaceBetween={16}
        navigation={
          config?.environment === 'desktop' && showroomData?.data?.length > 2
        }
        className='p-unset add-to-cart__success'
      >
        {showRoomWrapper()}
      </Swiper>
    </div>
  )
}
