import config from '../../../../../config'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'

export default function VirtualShowPrice({ productData, setShowShareModal }) {
  return (
    <div
      className={`tw-flex tw-w-full ${
        config?.environment === 'mobile'
          ? 'tw-justify-center tw-items-center'
          : ''
      }`}
    >
      <div
        className={`tw-flex tw-flex-col ${
          config?.environment === 'mobile' ? 'tw-items-start' : ''
        } w-full`}
      >
        {productData?.special_price !== 0 && (
          <div className='tw-flex'>
            <div
              className={`price__initial ${
                config?.environment === 'desktop' ? '!tw-mr-1' : ''
              }`}
            >
              Rp {productData?.price.toLocaleString(['ban', 'id'])}
            </div>
            <div className='price__discount'>
              <span>
                {Math.floor(
                  ((productData?.price - productData?.special_price) /
                    productData?.price) *
                    100,
                )}
                %
              </span>
            </div>
          </div>
        )}

        <div className='price__real left'>
          Rp
          {productData?.special_price !== 0
            ? productData?.special_price?.toLocaleString(['ban', 'id'])
            : productData?.price?.toLocaleString(['ban', 'id'])}{' '}
        </div>
      </div>
      {config?.environment === 'mobile' && (
        <div
          className={`tw-border tw-border-solid tw-p-2 tw-rounded-md tw-border-grey-50 tw-max-h-[36px] ${
            productData?.special_price === 0 ? 'tw-mt-2' : ''
          }`}
          onClick={() => setShowShareModal(true)}
        >
          <CustomLazyLoadImage
            src={`${config?.assetsURL}icon/share-icon.svg`}
            width={18}
            height={18}
            alt='Share Button'
          />
        </div>
      )}
    </div>
  )
}
