import { useState } from 'react'
import Modal from '../../../layouts/templates/Modal'
import isEmpty from 'lodash/isEmpty'
import VirtualShowModal from './VirtualShowModal'
import config from '../../../../../config'
import FloatingInfobox from '../../../layouts/atoms/FloatingInfobox'
import VirtualShowSwiper from './VirtualShowSwiper'
import HR from '../../../utils/common/HR'
import ShowroomHeader from '../SubComponents/ShowroomHeader'

export default function VirtualShowComponent({
  showroomData,
  isLoading,
  showShareModal,
  setShowShareModal,
  setShareProductData = {},
}) {
  const [productData, setProductData] = useState(null)
  const [toggleErrorClipboardInfobox, setToggleErrorClipboardInfobox] =
    useState(false)
  const [toggleClipboardInfobox, setToggleClipboardInfobox] = useState(false)
  const [errMessage, setErrMessage] = useState(
    'Produk dalam keranjang belanja anda mencapai jumlah maksimal. Hapus beberapa untuk menambahkan produk lainnya',
  )

  return (
    <>
      {showroomData && config?.environment === 'mobile' && <HR />}
      <div
        className={`padding-bottom-l sdl--container ${
          config.environment === 'mobile'
            ? 'padding-left-l padding-right-l'
            : ''
        }`}
      >
        {showroomData && (
          <ShowroomHeader isLoading={isLoading} showroomData={showroomData} />
        )}
        <VirtualShowSwiper
          showroomData={showroomData}
          isLoading={isLoading}
          setShareProductData={setShareProductData}
          setProductData={setProductData}
          setToggleClipboardInfobox={setToggleClipboardInfobox}
          setToggleErrorClipboardInfobox={setToggleErrorClipboardInfobox}
          setErrMessage={setErrMessage}
        />
        {!isEmpty(productData) && !showShareModal && (
          <Modal
            headerElement
            contentClass='virtual-showroom'
            dialogClass='virtual-showroom'
            bodyElement={() => (
              <VirtualShowModal
                showroomTitle={showroomData?.title}
                productData={productData}
                setProductData={setProductData}
                setToggleClipboardInfobox={setToggleClipboardInfobox}
                setToggleErrorClipboardInfobox={setToggleErrorClipboardInfobox}
                setErrMessage={setErrMessage}
                setShowShareModal={setShowShareModal}
                setShareProductData={setShareProductData}
              />
            )}
            modalType='bottom'
            onClose={() => setProductData(null)}
            show={!isEmpty(productData)}
            backdropOnClose
          />
        )}

        {toggleClipboardInfobox && (
          <FloatingInfobox
            duration={2500}
            toggleFlag={toggleClipboardInfobox}
            type='success'
            setToggle={setToggleClipboardInfobox}
          >
            Produk berhasil ditambahkan ke keranjang.
          </FloatingInfobox>
        )}

        {toggleErrorClipboardInfobox && (
          <FloatingInfobox
            className='errorAddToCartSTL'
            duration={2500}
            toggleFlag={toggleErrorClipboardInfobox}
            type='error'
            setToggle={setToggleErrorClipboardInfobox}
          >
            {errMessage}
          </FloatingInfobox>
        )}
      </div>
    </>
  )
}
