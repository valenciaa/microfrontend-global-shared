import isEmpty from 'lodash/isEmpty'
import config from '../../../../../config'
import { useGetSocialProof } from '../../../services/api/queries/global'
import { handleCart } from '../../../container/virtualShowroom'
import { useConstantsContext } from '../../../context/ConstantsContext'
import VirtualShowPrice from './VirtualShowPrice'
import HR from '../../../utils/common/HR'
import { useCartAuthContext } from '../../../context/CartAuthContext'
import { useAddMinicartItem } from '../../../services/api/mutations/minicart'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import { internalTrackerData } from '../../../utils/InternalTracking'
import { GetUniqueId } from '../../../utils'
import { GetAuthData } from '../../../../../src/utils/misc/GetAuthData'
import ShowroomImage from '../SubComponents/ShowroomImage'
import ShowroomFooter from '../SubComponents/ShowroomFooter'
import ShowroomLabel from '../SubComponents/ShowroomLabel'
import ShowroomTitle from '../SubComponents/ShowroomTitle'

export default function VirtualShowModal({
  vsData = null,
  productData,
  setProductData,
  setToggleClipboardInfobox,
  setToggleErrorClipboardInfobox,
  setErrMessage,
  setShowShareModal,
}) {
  const { data: basoSocialProof } = useGetSocialProof(productData?.url_key, {
    enabled: !!productData?.url_key,
  })
  const { state: cartAuthContext } = useCartAuthContext()
  const { storeCode } = useConstantsContext()

  const { mutate } = useAddMinicartItem({
    onSuccess: async (res) => {
      if (res?.data) {
        const uuid = await GetUniqueId()
        const user = await GetAuthData()

        setToggleClipboardInfobox(true)
        const mixpanelData = {
          'Section Name': productData?.sectionTitle || vsData?.title || '',
          'Click Location': 'Add To Cart Shop The Look',
          'Location': 'PDP',
        }
        mixpanelTrack('Shop The Look', mixpanelData)

        mixpanelTrack('Tambah Keranjang', {
          Location: 'Shop The Look PDP',
        })

        internalTrackerData(
          'Shop The Look',
          { uuid: uuid, ...user },
          mixpanelData,
        )
      }
    },
  })

  const clickedAddToCart = (e) => {
    handleCart(
      e,
      'mobile',
      productData,
      storeCode,
      '',
      cartAuthContext,
      mutate,
      'PDP',
      setToggleErrorClipboardInfobox,
      setToggleClipboardInfobox,
      setProductData,
      setErrMessage,
    )
  }

  return (
    <div className='tw-flex tw-flex-col tw-items-center tw-h-full tw-text-center tw-justify-evenly'>
      <ShowroomImage productData={productData} vsData={vsData} />

      {!isEmpty(basoSocialProof) && config?.environment === 'mobile' && (
        <div className='tw-w-full tw-text-center tw-h-full tw-bg-[#e5f7ff] ui-text-3 tw-p-2 tw-mb-3'>
          {basoSocialProof} orang memiliki produk ini di keranjang belanja
        </div>
      )}

      <div
        className={`tw-w-full tw-flex tw-px-4 tw-flex-col tw-items-start tw-justify-start tw-text-start ${
          config?.environment === 'mobile' ? ' tw-font-bold' : ''
        }`}
      >
        <ShowroomTitle productData={productData} vsData={vsData} />
        <VirtualShowPrice
          setShowShareModal={setShowShareModal}
          productData={productData}
        />
        <ShowroomLabel />
      </div>

      {config?.environment === 'mobile' && (
        <HR height='!tw-h-[0.1px] tw-mt-4' />
      )}

      <ShowroomFooter
        productData={productData}
        setProductData={setProductData}
        clickedAddToCart={clickedAddToCart}
      />
    </div>
  )
}
