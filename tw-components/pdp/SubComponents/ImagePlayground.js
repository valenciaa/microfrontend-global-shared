import config from '../../../../../config'

export default function ImagePlayground({
  vsProd,
  vsProdIdx,
  onClickMarker,
  cardProperty,
}) {
  return (
    <div
      key={vsProdIdx}
      className={
        'tw-absolute virtual-showroom-1 virtual-showroom__tag-outer ruparupa tw-z-10'
      }
      style={{
        top: vsProd?.style?.top + '%',
        left: vsProd?.style?.left + '%',
      }}
      onClick={() =>
        config?.environment === 'mobile' ? onClickMarker(vsProd) : {}
      }
      {...cardProperty}
    >
      <div class='virtual-showroom__tag-inner' />
    </div>
  )
}
