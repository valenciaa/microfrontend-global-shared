import config from '../../../../../config'
import Skeleton from 'react-loading-skeleton'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import { useRouter } from 'next/router'
import ImagePlayground from './ImagePlayground'
import ProductsPlayground from './ProductsPlayground'

export default function ShowroomPlayground({
  vsData = null,
  isLoading,
  vsIndex = null,
  setToggleClipboardInfobox,
  setToggleErrorClipboardInfobox,
  setErrMessage,
  activeProduct,
  setProductData,
  setActiveProduct,
  setShareProductData,
}) {
  const router = useRouter()
  const { button: { link = '' } = {} } = vsData || {}

  const onClickMarker = (vsProd) => {
    if (config?.environment === 'desktop' && vsData && link) {
      router.push(
        link +
          `&itm_source=shop-the-look-image-${vsData?.title.replaceAll(
            ' ',
            '-',
          )}&itm_campaign=PDP&itm_device=${config?.environment}`,
      )
      return
    }
    if (vsProd) {
      vsProd.sectionTitle = vsData?.title
      setProductData(vsProd)
      if (setShareProductData) {
        setShareProductData(vsProd)
      }
    }
    mixpanelTrack('Shop The Look', {
      'Section Name': vsData?.title,
      'Click Location': 'Tagging Shop The Look',
      'Location': 'PDP',
    })
  }

  let containerSizeing =
    config?.environment === 'mobile'
      ? 'xs:tw-max-h-[10rem] tw-h-[189px]'
      : 'tw-h-[381px]'

  return (
    <div
      className={
        'tw-flex tw-bg-cover tw-bg-center tw-rounded-[4px] tw-relative ' +
        containerSizeing
      }
      style={
        !isLoading
          ? {
              backgroundImage: `url(${
                config.environment === 'desktop'
                  ? vsData?.background_img_desktop
                  : vsData?.background_img_mobile
              })`,
            }
          : {}
      }
    >
      <div
        className='tw-absolute tw-w-full tw-h-full tw-z-0'
        onClick={() => config?.environment === 'desktop' && onClickMarker()}
      />

      {!isLoading ? (
        vsData?.products?.map((vsProd, vsProdIdx) => {
          let cardProperty = {
            onMouseEnter: () => setActiveProduct(vsProdIdx + `${vsIndex}`),
            onMouseLeave: () => setActiveProduct(null),
          }
          if (config.environment === 'mobile') {
            cardProperty = {}
          }

          return (
            <>
              <ImagePlayground
                vsProd={vsProd}
                vsProdIdx={vsProdIdx}
                onClickMarker={onClickMarker}
                cardProperty={cardProperty}
              />
              <ProductsPlayground
                vsData={vsData}
                vsProd={vsProd}
                vsProdIdx={vsProdIdx}
                vsIndex={vsIndex}
                activeProduct={activeProduct}
                cardProperty={cardProperty}
                setToggleClipboardInfobox={setToggleClipboardInfobox}
                setToggleErrorClipboardInfobox={setToggleErrorClipboardInfobox}
                setErrMessage={setErrMessage}
              />
            </>
          )
        })
      ) : (
        <Skeleton
          containerClassName='w-full h-full'
          className='w-full h-full'
        />
      )}
    </div>
  )
}
