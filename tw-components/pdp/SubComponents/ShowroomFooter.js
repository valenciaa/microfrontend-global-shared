import Link from 'next/link'
import config from '../../../../../config'
import Button from '../../../layouts/atoms/Button'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'

export default function ShowroomFooter({
  productData,
  setProductData,
  clickedAddToCart,
}) {
  const onClickButton = () => {
    setProductData(null)
    mixpanelTrack('Shop The Look', {
      'Section Name': productData?.sectionTitle || '',
      'Click Location': 'Product Card',
      'Location': 'PDP',
    })
  }

  return (
    <div className='tw-w-full tw-flex tw-px-4'>
      <div className='tw-flex tw-w-full tw-mt-3'>
        {config?.environment === 'mobile' && (
          <div
            className='button tw-border-primary tw-text-orange-50 tw-border-[1px] tw-w-3/6 tw-border-solid tw-rounded-[4px]'
            onClick={() => onClickButton()}
          >
            <Link
              href={
                config?.baseURL +
                'p/' +
                productData?.url_key +
                `?itm_source=shop-the-look-PDP-${productData?.sectionTitle}&itm_campaign=PDP&itm_term=${productData?.sku}&itm_device=${config?.environment}`
              }
            >
              Lihat Produk
            </Link>
          </div>
        )}
        <Button
          additionalClass={
            config?.environment === 'mobile' ? 'margin-left-xs' : ''
          }
          width={config?.environment === 'mobile' ? '50%' : '100%'}
          type='primary'
          handleOnClick={(e) => clickedAddToCart(e)}
        >
          + Keranjang
        </Button>
      </div>
    </div>
  )
}
