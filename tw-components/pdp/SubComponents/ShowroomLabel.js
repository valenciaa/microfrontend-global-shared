import isEmpty from 'lodash/isEmpty'
import config from '../../../../../config'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'

export default function ShowroomLabel({ productData }) {
  if (isEmpty(productData?.label)) {
    return null
  }

  return (
    <div className='tw-flex tw-gap-1 tw-items-center tw-text-left tw-w-full tw-py-2 tw-px-3'>
      <CustomLazyLoadImage
        src={`${config?.assetsURL}icon/info-red.svg`}
        alt='Info Icon'
        height={13.33}
        width={13.33}
      />
      <span className='tw-text-red-50 tw-font-medium'>
        {productData?.label}
      </span>
    </div>
  )
}
