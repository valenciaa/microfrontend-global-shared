import debounce from 'lodash/debounce'
import config from '../../../../../config'
import VirtualShowModal from '../Components/VirtualShowModal'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import { useEffect } from 'react'
export default function ProductsPlayground({
  vsData,
  vsProd,
  vsProdIdx,
  vsIndex,
  activeProduct,
  cardProperty,
  setToggleClipboardInfobox,
  setToggleErrorClipboardInfobox,
  setErrMessage,
}) {
  useEffect(() => {
    const debouncedMixpanelTrack = debounce(() => {
      mixpanelTrack('Shop The Look', {
        'Section Name': vsData?.title,
        'Click Location': 'Tagging Shop The Look',
        'Location': 'PDP',
      })
    }, 500)

    if (
      activeProduct === vsProdIdx + `${vsIndex}` &&
      config?.environment === 'desktop'
    ) {
      debouncedMixpanelTrack()
    }

    // Clean up the debounce
    return () => {
      debouncedMixpanelTrack.cancel()
    }
  }, [activeProduct, vsData, vsProdIdx, vsIndex])

  if (
    activeProduct === vsProdIdx + `${vsIndex}` &&
    config?.environment === 'desktop'
  ) {
    return (
      <div
        key={vsProdIdx}
        className='tw-max-w-[190px] tw-max-h-[369px] tw-bg-white tw-py-3 tw-flex tw-flex-col tw-absolute tw-top-5 tw-rounded-lg tw-z-20 tw-cursor-pointer'
        style={{
          left: `${
            vsProd?.style?.left + (vsProd?.style?.left < 68 ? 3 : -31)
          }%`,
        }}
        {...cardProperty}
      >
        <VirtualShowModal
          vsData={vsData}
          productData={vsProd}
          setToggleClipboardInfobox={setToggleClipboardInfobox}
          setToggleErrorClipboardInfobox={setToggleErrorClipboardInfobox}
          setErrMessage={setErrMessage}
        />
      </div>
    )
  }

  return null
}
