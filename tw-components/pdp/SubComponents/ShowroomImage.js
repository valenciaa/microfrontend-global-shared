import Link from 'next/link'
import config from '../../../../../config'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import Skeleton from 'react-loading-skeleton'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'

export default function ShowroomImage({ productData, vsData }) {
  const imageDisplay = () => {
    let imageUrl = config?.imageURL + productData?.image

    if (productData?.full_image_url) {
      imageUrl = productData?.full_image_url
    }
    return (
      <CustomLazyLoadImage
        height={config?.environment == 'desktop' ? 150 : '40vh'}
        width={config?.environment == 'desktop' ? 150 : '40vh'}
        src={imageUrl}
        placeholder={
          <Skeleton
            width='100%'
            height={config?.environment == 'desktop' ? 150 : '40vh'}
          />
        }
        className='tw-w-full'
      />
    )
  }

  if (config?.environment === 'desktop' && vsData && productData) {
    const { title = '' } = vsData || {}
    const { url_key = '', sku = '' } = productData || {}

    return (
      <div
        onClick={() =>
          config?.environment === 'desktop' &&
          mixpanelTrack('Shop The Look', {
            'Section Name': vsData?.title || '',
            'Click Location': 'Product Card',
            'Location': 'PDP',
          })
        }
      >
        <Link
          href={
            config?.baseURL +
            'p/' +
            url_key +
            `?itm_source=shop-the-look-PDP-${title}&itm_campaign=PDP&itm_term=${sku}&itm_device=${config?.environment}`
          }
        >
          {imageDisplay()}
        </Link>
      </div>
    )
  }

  return (
    <div className='!tw-w-full tw-px-4 tw-mb-3 tw-flex tw-items-center tw-justify-center'>
      {imageDisplay()}
    </div>
  )
}
