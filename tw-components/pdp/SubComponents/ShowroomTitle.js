import Link from 'next/link'
import config from '../../../../../config'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'

export default function ShowroomTitle({ productData, vsData }) {
  const title = productData?.name || ''
  const truncatedTitle =
    title?.length > 75 ? `${title?.substr(0, 75)}...` : title

  if (config?.environment === 'desktop' && productData && vsData) {
    const { title: vsTitle = '' } = vsData || {}
    const { url_key = '', sku = '' } = productData || {}
    const url = `${config?.baseURL}p/${url_key}?itm_source=shop-the-look-PDP-${vsTitle}&itm_campaign=PDP&itm_term=${sku}&itm_device=${config?.environment}`
    return (
      <div
        className='tw-mt-2'
        onClick={() =>
          mixpanelTrack('Shop The Look', {
            'Section Name': vsData?.title || '',
            'Click Location': 'Product Card',
            'Location': 'PDP',
          })
        }
      >
        <Link href={url}>
          <div
            className={`${
              config?.environment === 'desktop' ? 'tw-line-clamp-2' : ''
            }`}
          >
            {truncatedTitle}
          </div>
        </Link>
      </div>
    )
  }

  return truncatedTitle
}
