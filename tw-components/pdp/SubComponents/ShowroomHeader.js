import Link from 'next/link'
import config from '../../../../../config'
import Skeleton from 'react-loading-skeleton'

export default function ShowroomHeader({ isLoading, showroomData }) {
  return (
    <div className='tw-flex tw-w-full tw-justify-between tw-pt-[24px] tw-pb-[8px]'>
      {isLoading ? (
        <Skeleton height={24} width={240} />
      ) : (
        <span className='ui-text-1 !tw-font-bold'>Shop The Look</span>
      )}
      <div className='ui-text-2 tw-font-semibold tw-text-orange-50'>
        {isLoading ? (
          <Skeleton height={24} width={84} />
        ) : (
          <Link
            href={
              config?.baseURL +
              showroomData?.allVirtualShowLink +
              '?itm_source=pdp-button-all-shop-the-look&itm_campaign=pilihan-shop-the-look-untuk-anda&itm_device=' +
              config?.environment
            }
          >
            Lihat Semua
          </Link>
        )}
      </div>
    </div>
  )
}
