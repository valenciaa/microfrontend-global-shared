import clsx from 'clsx'

const textSizeObj = {
  small: clsx('tw-text-xs'),
  medium: clsx('tw-text-sm'),
  big: clsx('tw-text-base tw-font-semibold'),
}
export default function TWTabs({
  tabs = [],
  chosenTab = '',
  setChosenTab = () => {},
  isLeftSide = false,
  textSize = 'small',
}) {
  return (
    <div
      className={clsx(
        'tw-flex',
        isLeftSide
          ? 'tw-justify-start tw-border-b tw-border-solid tw-border-grey-20'
          : ' tw-justify-between',
      )}
    >
      {tabs.map((tab, i) => {
        return (
          <div
            onClick={() => {
              setChosenTab(tab.value)
              if (tab?.actionOnClick) {
                tab.actionOnClick()
              }
            }}
            key={i}
            className={clsx(
              'tw-py-3 tw-px-2 tw-flex tw-items-center tw-justify-center',
              'tw-transition tw-duration-200',
              'tw-cursor-pointer',
              'tw-border-b tw-border-solid',
              tab.value === chosenTab
                ? 'tw-text-orange-50 tw-font-bold tw-border-orange-50'
                : 'tw-text-grey-50 tw-border-grey-10',
              !isLeftSide ? 'tw-basis-full' : '',
              textSizeObj[textSize],
            )}
          >
            {tab.label}
          </div>
        )
      })}
    </div>
  )
}
