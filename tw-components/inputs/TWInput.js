import clsx from 'clsx'
import AddressNameLabel from '../../tw-container/address/component/misc/AddressNameLabel'
import config from '../../../../config'

const buColorMap = {
  'border-focus': {
    ruparupa: clsx('focus:tw-border-ruparupa'),
    ace: clsx('focus:tw-border-ace'),
    informa: clsx('focus:tw-border-informa'),
    toyskingdomonline: clsx('focus:tw-border-toyskingdomonline'),
  },
}
const companyName = config.companyNameCSS || 'ruparupa'

export default function TWInput({
  onBlur,
  onChange,
  onFocus,
  onKeyDown,
  autoFocus = false,
  defaultValue,
  disabled,
  id,
  icon,
  inputClass,
  label,
  maxLength,
  name,
  placeholder,
  replaceLable,
  type,
  value,
  inputWrapperClass,
  needAddressLabel,
  handleAddressLabel,
  refProps,
}) {
  const isShowInput =
    !needAddressLabel || (needAddressLabel && value?.length > 0)

  return (
    <div
      className={clsx(
        'tw-flex tw-flex-col tw-gap-1 tw-w-full',
        inputWrapperClass,
      )}
    >
      {label ? (
        <label className='tw-text-xs tw-text-grey-100' htmlFor={id}>
          {label}
        </label>
      ) : replaceLable ? (
        <div className='tw-w-full tw-h-4' />
      ) : null}

      {needAddressLabel && (
        <AddressNameLabel
          addressName={value}
          handleAddressLabel={handleAddressLabel}
        />
      )}
      {isShowInput && (
        <div className='tw-flex tw-items-center tw-gap-1 tw-relative'>
          <input
            ref={refProps}
            {...(onBlur ? { onBlur } : {})}
            {...(onChange ? { onChange } : {})}
            {...(onFocus ? { onFocus } : {})}
            {...(onKeyDown ? { onKeyDown } : {})}
            autoFocus={autoFocus}
            className={clsx(
              'tw-text-sm tw-border tw-border-grey-20 tw-rounded-lg tw-p-2 tw-outline-0 tw-w-full',
              inputClass,
              buColorMap['border-focus'][companyName],
              'disabled:tw-bg-grey-10 disabled:tw-bg-opacity-50 disabled:tw-cursor-not-allowed',
            )}
            {...(defaultValue ? { defaultValue } : {})}
            disabled={disabled || false}
            id={id}
            {...(maxLength ? { maxLength } : {})}
            name={name}
            {...(placeholder ? { placeholder } : {})}
            {...(type ? { type } : {})}
            value={value}
          />

          {icon}
        </div>
      )}
    </div>
  )
}
