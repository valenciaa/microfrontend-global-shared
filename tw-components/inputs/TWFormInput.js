import clsx from 'clsx'
import TWErrorText from '../error/TWErrorText'
import TWInput from './TWInput'

export default function TWFormInput({
  blurFunction,
  changeFunction,
  changeFunctionAdditionalAction,
  focusFunction,
  keyDownFunction,
  overrideBlurFunction,
  overrideChangeFunction,
  setValue,
  watchFunction,
  defaultValue,
  disabled,
  errors,
  errTextId,
  inputClass,
  inputWrapperClass,
  name,
  inputPlaceholder,
  label,
  maxLength,
  placeholder,
  replaceLable,
  showCounter = true,
  type = 'text',
  value,
  wrapperClass,
  needAddressLabel,
  handleAddressLabel,
  refProps,
}) {
  const isShowCounter = !!(watchFunction && showCounter && maxLength)
  const isErrorString = typeof errors === 'string'
  const errMessage = isErrorString ? errors : errors?.[name]?.message

  return (
    <>
      <div className={clsx('tw-flex tw-flex-col tw-gap-1', wrapperClass)}>
        <TWInput
          refProps={refProps}
          defaultValue={defaultValue}
          disabled={disabled}
          id={name}
          inputClass={clsx(
            errMessage ? 'tw-border-red-50' : 'focus:tw-border-blue-50',
            inputClass,
          )}
          inputWrapperClass={inputWrapperClass}
          label={label}
          maxLength={maxLength}
          name={name}
          placeholder={inputPlaceholder}
          replaceLable={replaceLable}
          type={type}
          value={value}
          onBlur={(e) => {
            if (overrideBlurFunction) {
              overrideBlurFunction?.(e)
            } else {
              setValue?.(name, e.target.value)
            }

            blurFunction?.(e)
          }}
          onChange={(e) => {
            if (overrideChangeFunction) {
              overrideChangeFunction?.(e)
            } else {
              setValue?.(name, e.target.value)
            }

            changeFunction?.(e)
            changeFunctionAdditionalAction?.()
          }}
          onFocus={() => focusFunction?.()}
          onKeyDown={(e) => keyDownFunction?.(e)}
          needAddressLabel={needAddressLabel}
          handleAddressLabel={handleAddressLabel}
        />

        {(errMessage || isShowCounter || placeholder) && (
          <div
            className={clsx(
              'tw-flex tw-items-center',
              'tw-gap-1 tw-text-grey-40',
              placeholder || errMessage
                ? 'tw-justify-between'
                : 'tw-justify-end',
            )}
          >
            {errMessage ? (
              <TWErrorText
                {...(errTextId ? { id: errTextId } : {})}
                message={errMessage}
              />
            ) : placeholder ? (
              <p className='tw-text-xs'>{placeholder}</p>
            ) : null}

            {isShowCounter && (
              <p className='tw-text-xs'>
                {watchFunction?.(name)?.length}/{maxLength}
              </p>
            )}
          </div>
        )}
      </div>
    </>
  )
}
