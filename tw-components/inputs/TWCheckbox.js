import clsx from 'clsx'
import config from '../../../../config'

const sizeObj = {
  medium: clsx('tw-w-5 tw-h-5'),
  large: clsx(
    config.environment === 'desktop' ? 'tw-w-6 tw-h-6' : 'tw-w-5 tw-h-5',
  ),
}
const colorObj = {
  ruparupa: clsx(
    `checked:tw-bg-ruparupa checked:tw-border-ruparupa checked:tw-ring-ruparupa`,
  ),
  ace: clsx(`checked:tw-bg-ace checked:tw-border-ace checked:tw-ring-ace`),
  informa: clsx(
    `checked:tw-bg-informa checked:tw-border-informa checked:tw-ring-informa`,
  ),
  toyskingdomonline: clsx(
    `checked:tw-bg-toyskingdomonline checked:tw-border-toyskingdomonline checked:tw-ring-toyskingdomonline`,
  ),
}

/**
 * Checbox componentn with Tailwind CSS
 * @param {boolean} disabled if checkbox input disabled
 * @param {boolean} isChecked checkbox value
 * @param {function} handleOnChange onChange handler
 * @param {string} size checkbox size small|medium|large
 * @returns {component}
 */
export default function TWCheckbox({
  disabled = false,
  isChecked,
  handleOnChange,
  size = 'large',
  borderColor = 'tw-border-grey-50',
  className = '',
}) {
  const companyName = config.companyNameCSS || 'ruparupa'

  const currentSize = sizeObj[size]
  const currentColor = colorObj[companyName]

  return (
    <input
      checked={isChecked}
      className={clsx(
        'tw-cursor-pointer',
        'tw-appearance-none',
        'tw-border tw-rounded',
        'tw-bg-center tw-bg-[length:80%] tw-bg-no-repeat checked:tw-bg-check checked:tw-ring-offset-0',
        'disabled:tw-bg-grey-20 disabled:tw-border-grey-20 disabled:tw-cursor-not-allowed',
        borderColor,
        currentColor,
        currentSize,
        className,
      )}
      disabled={disabled}
      type='checkbox'
      onChange={() => handleOnChange && handleOnChange()}
    />
  )
}
