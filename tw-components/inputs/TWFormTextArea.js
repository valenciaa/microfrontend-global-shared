import clsx from 'clsx'
import TWErrorText from '../error/TWErrorText'
import TWTextArea from './TWTextArea'

export default function TWFormTextArea({
  changeFunction,
  blurFunction,
  setValue,
  watchFunction,
  disabled,
  disabledClass,
  defaultValue,
  value,
  errors,
  errTextId,
  inputClass,
  inputWrapperClass,
  name,
  inputPlaceholder,
  label,
  maxLength,
  placeholder,
  rows,
  showCounter = true,
  wrapperClass,
  needInfobox = true,
  refProps,
}) {
  const isShowCounter = !!(watchFunction && showCounter && maxLength)
  const isErrorString = typeof errors === 'string'
  const errMessage = isErrorString ? errors : errors?.[name]?.message

  return (
    <>
      <div className={clsx('tw-flex tw-flex-col tw-gap-1', wrapperClass)}>
        <TWTextArea
          refProps={refProps}
          defaultValue={defaultValue}
          disabled={disabled}
          disabledClass={disabledClass}
          id={name}
          inputClass={clsx(
            errMessage ? 'tw-border-red-50' : 'focus:tw-border-blue-50',
            inputClass,
          )}
          inputWrapperClass={inputWrapperClass}
          label={label}
          maxLength={maxLength}
          name={name}
          placeholder={inputPlaceholder}
          rows={rows}
          onChange={(e) => {
            setValue?.(name, e.target.value)

            changeFunction?.(e)
          }}
          onBlur={(e) => {
            setValue?.(name, e.target.value)

            blurFunction?.(e)
          }}
          value={value}
          needInfobox={needInfobox}
        />

        {(errMessage || isShowCounter) && (
          <div
            className={clsx(
              'tw-flex tw-items-center',
              'tw-gap-1 tw-text-grey-40',
              placeholder || errMessage
                ? 'tw-justify-between'
                : 'tw-justify-end',
            )}
          >
            {errMessage ? (
              <TWErrorText
                {...(errTextId ? { id: errTextId } : {})}
                message={errMessage}
              />
            ) : placeholder ? (
              <p className='tw-text-xs'>{placeholder}</p>
            ) : null}

            {isShowCounter && (
              <p className='tw-text-xs'>
                {watchFunction?.(name)?.length}/{maxLength}
              </p>
            )}
          </div>
        )}
      </div>
    </>
  )
}
