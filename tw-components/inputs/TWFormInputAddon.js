import clsx from 'clsx'
import TWErrorText from '../error/TWErrorText'
import TWInputAddon from './TWInputAddon'

export default function TWFormInputAddon({
  blurFunction,
  changeFunction,
  setValue,
  watchFunction,
  defaultValue,
  errors,
  errTextId,
  inputClass,
  name,
  inputPlaceholder,
  label,
  maxLength,
  placeholder,
  showCounter = true,
  tag,
  type = 'text',
  wrapperClass,
  refProps,
  disabled,
}) {
  const isShowCounter = !!(watchFunction && showCounter && maxLength)
  const isErrorString = typeof errors === 'string'
  const errMessage = isErrorString ? errors : errors?.[name]?.message

  return (
    <>
      <div className='tw-flex tw-flex-col tw-gap-1'>
        <TWInputAddon
          refProps={refProps}
          defaultValue={defaultValue}
          id={name}
          inputClass={clsx(
            errMessage ? 'tw-border-red-50' : 'focus:tw-border-blue-50',
            inputClass,
          )}
          label={label}
          maxLength={maxLength}
          name={name}
          placeholder={inputPlaceholder}
          tag={tag}
          type={type}
          wrapperClass={wrapperClass}
          onBlur={(e) => {
            setValue?.(name, e.target.value)

            blurFunction?.(e)
          }}
          onChange={(e) => {
            setValue?.(name, e.target.value)

            changeFunction?.(e)
          }}
          disabled={disabled}
        />

        {(errMessage || placeholder) && (
          <div
            className={clsx(
              'tw-flex tw-items-center',
              'tw-gap-1 tw-text-grey-40',
              placeholder || errMessage
                ? 'tw-justify-between'
                : 'tw-justify-end',
            )}
          >
            {errMessage ? (
              <TWErrorText
                {...(errTextId ? { id: errTextId } : {})}
                message={errMessage}
              />
            ) : placeholder ? (
              <p className='tw-text-xs'>{placeholder}</p>
            ) : null}

            {isShowCounter && (
              <p className='tw-text-xs'>
                {watchFunction?.(name)?.length}/{maxLength}
              </p>
            )}
          </div>
        )}
      </div>
    </>
  )
}
