import clsx from 'clsx'
import config from '../../../../config'

const colorObj = {
  ruparupa: clsx(`checked:tw-accent-ruparupa`),
  ace: clsx(`checked:tw-accent-ace`),
  informa: clsx(`checked:tw-accent-informa`),
  toyskingdomonline: clsx(`checked:tw-accent-toyskingdomonline`),
}

const sizeObj = {
  large: clsx('tw-w-6 tw-h-6'),
  medium: clsx('tw-w-5 tw-h-5'),
}

export default function TWRadio({
  disabled = false,
  isChecked,
  handleOnChange,
  value,
  name,
  size = 'large',
  register = () => {},
  ...props // including onChange
}) {
  const companyName = config.companyNameCSS || 'ruparupa'

  const currentSize = sizeObj[size]
  const currentColor = colorObj[companyName]
  return (
    <input
      type='radio'
      disabled={disabled}
      checked={isChecked}
      value={value}
      name={name}
      onClick={() => handleOnChange && handleOnChange()} // Trigger a browser error: next-dev.js:20 Warning: You provided a `checked` prop to a form field without an `onChange` handler
      className={clsx(currentSize, currentColor, 'tw-cursor-pointer tw-aspect-square')}
      {...props}
      {...register(name)}
    />
  )
}
