import clsx from 'clsx'
import TWInput from './TWInput'

export default function TWInputAddon({
  onBlur,
  onChange,
  defaultValue,
  disabled,
  id,
  inputClass,
  inputWrapperClass,
  label,
  maxLength,
  name,
  placeholder,
  tag,
  type,
  value,
  wrapperClass,
  refProps,
}) {
  return (
    <div
      className={clsx('tw-flex tw-flex-col tw-gap-0.5 tw-w-full', wrapperClass)}
    >
      {label && (
        <label className='tw-text-xs' htmlFor={id}>
          {label}
        </label>
      )}

      <div className='tw-flex tw-items-center'>
        <span className='tw-text-sm tw-font-bold tw-flex tw-bg-grey-20 tw-border tw-border-grey-20 tw-border-solid tw-p-2 tw-rounded-lg tw-rounded-r-none'>
          {tag}
        </span>

        <TWInput
          refProps={refProps}
          defaultValue={defaultValue}
          disabled={disabled}
          id={id}
          inputClass={clsx('tw-rounded-l-none', inputClass)}
          inputWrapperClass={inputWrapperClass}
          maxLength={maxLength}
          name={name}
          placeholder={placeholder}
          type={type}
          value={value}
          onBlur={onBlur}
          onChange={onChange}
        />
      </div>
    </div>
  )
}
