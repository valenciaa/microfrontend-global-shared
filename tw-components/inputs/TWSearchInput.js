import clsx from 'clsx'
import config from '../../../../config'
import InfoboxErrorIcon from '../../../../public/static/icon/infobox-error-16.svg'
import SearchIcon from '../../../../public/static/icon/search-16.svg'

const buColorMap = {
  ruparupa: clsx('tw-text-ruparupa'),
  ace: clsx('tw-text-ace'),
  informa: clsx('tw-text-informa'),
  toyskingdomonline: clsx('tw-text-toyskingdomonline'),
}

export default function TWSearchInput({ value, setValue, placeholder }) {
  const isMobile = config.environment === 'mobile'

  return (
    <div className='tw-flex tw-relative tw-grow'>
      <input
        className={clsx(
          'tw-border tw-border-grey-20 focus:tw-border-blue-50 tw-grow tw-outline-none tw-rounded-lg tw-pl-9 tw-pr-3 tw-py-2',
          isMobile ? 'tw-text-sm' : 'tw-text-base',
        )}
        placeholder={placeholder}
        type='text'
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />

      <div className='tw-absolute tw-pl-4 tw-py-3'>
        <SearchIcon className={buColorMap[config.companyNameCSS]} />
      </div>

      {value?.length > 0 && (
        <div
          className='tw-absolute tw-cursor-pointer tw-right-0 tw-pr-4 tw-py-3'
          onClick={() => {
            setValue('')
          }}
        >
          <InfoboxErrorIcon className='tw-text-grey-100' />
        </div>
      )}
    </div>
  )
}
