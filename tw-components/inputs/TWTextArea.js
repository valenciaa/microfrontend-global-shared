import { useState } from 'react'
import clsx from 'clsx'
import config from '../../../../config'

const buColorMap = {
  'border-focus': {
    ruparupa: clsx('focus:tw-border-ruparupa'),
    ace: clsx('focus:tw-border-ace'),
    informa: clsx('focus:tw-border-informa'),
    toyskingdomonline: clsx('focus:tw-border-toyskingdomonline'),
  },
}
const companyName = config.companyNameCSS || 'ruparupa'

export default function TWTextArea({
  onChange,
  onBlur,
  defaultValue,
  disabled,
  disabledClass,
  id,
  inputClass,
  label,
  maxLength,
  name,
  placeholder,
  value,
  rows = 3,
  inputWrapperClass,
  needInfobox = false,
  refProps,
}) {
  const [isFocused, setIsFocused] = useState(false)
  const isShowInfobox =
    needInfobox && (isFocused || defaultValue?.length > 0 || value?.length > 0)

  return (
    <div
      className={clsx(
        'tw-flex tw-flex-col tw-gap-0.5 tw-w-full',
        inputWrapperClass,
      )}
    >
      {label && (
        <label className='tw-text-xs' htmlFor={id}>
          {label}
        </label>
      )}

      <textarea
        ref={refProps}
        {...(onChange ? { onChange } : {})}
        {...(onBlur ? { onBlur } : {})}
        className={clsx(
          'tw-outline-0 tw-p-2.5 tw-w-full tw-rounded-lg tw-border tw-border-grey-20 tw-resize-none tw-text-sm',
          inputClass,
          disabledClass ? `disabled: ${disabledClass}` : '',
          needInfobox && 'tw-z-10',
          buColorMap['border-focus'][companyName],
          isShowInfobox && 'tw-z-10',
        )}
        {...(defaultValue ? { defaultValue } : {})}
        disabled={disabled || false}
        id={id}
        {...(maxLength ? { maxLength } : {})}
        name={name}
        {...(placeholder ? { placeholder } : {})}
        rows={rows}
        value={value}
        onFocus={() => setIsFocused(true)}
        onBlur={() => setIsFocused(false)}
      />

      {isShowInfobox && (
        <div className='tw-bg-orange-10 tw-text-xs -tw-mt-2.5 tw-px-3 tw-pb-2 tw-pt-[1.125rem] tw-rounded-br-lg tw-rounded-bl-lg'>
          <p>
            Cantumkan <b>Nama jalan, No. Rumah, & No. RT/RW</b>
          </p>
        </div>
      )}
    </div>
  )
}
