import clsx from 'clsx'
import { useEffect, useState } from 'react'
import { createPortal } from 'react-dom'
import config from '../../../config'

const setType = (type) => {
  switch (type) {
    case 'success-text-white':
    case 'success':
      return clsx('tw-bg-green-50 tw-text-white')
    case 'cart-error':
      return clsx('tw-bg-red-50 tw-text-red-10')
    case 'error-minicart':
      return clsx('tw-bg-red-60 tw-text-red-10')
    case 'error':
      return clsx('tw-bg-red-60 tw-text-white')
    case 'info':
      return clsx('tw-bg-grey-50 tw-text-white')
    default:
      return clsx('tw-bg-green-50 tw-text-white')
  }
}

export default function TWFloatingInfobox({
  children,
  duration,
  setToggle,
  toggleFlag,
  spanText = 'OK',
  type,
  additionalButton,
  additionalButtonClose,
  additionalAction,
  textAction,
}) {
  const [showInfobox, setShowInfobox] = useState(true)
  const [isAnimation, setIsAnimation] = useState(false)
  let timer = null

  useEffect(() => {
    if (duration) {
      timer = setTimeout(() => {
        if (toggleFlag) {
          setShowInfobox(false)
          setToggle(false)
          additionalAction && additionalAction()
        }
      }, duration)
    }

    if (showInfobox && toggleFlag) {
      setTimeout(() => {
        setIsAnimation(true)
      }, 100)
    } else {
      setIsAnimation(false)
    }

    return function cleanup() {
      clearTimeout(timer)
    }
  }, [])

  function handleAdditional() {
    if (additionalButton?.href) {
      window.location.href = additionalButton?.href
    } else if (additionalButton?.action) {
      additionalButton.action()
    }
  }

  const handleCloseInfobox = (e) => {
    e.preventDefault()
    clearTimeout(timer)
    setShowInfobox(false)
    setToggle(false)
  }

  if (showInfobox) {
    return createPortal(
      <>
        {config.environment === 'desktop' ? (
          <div
            className={clsx(
              'tw-fixed tw-transform tw-left-1/2 -tw-translate-x-1/2 tw-z-[2000] tw-duration-500 tw-transition-all',
              isAnimation ? 'tw-top-36' : '-tw-top-20',
            )}
          >
            <div
              className={clsx(
                'tw-px-6 tw-py-3 tw-rounded-lg tw-flex tw-justify-between tw-gap-2',
                setType(type),
              )}
            >
              <span className='tw-font-medium tw-pr-8'>{children}</span>
              {!duration && (
                <span
                  className='tw-cursor-pointer'
                  onClick={(e) => handleCloseInfobox(e)}
                >
                  {spanText}
                </span>
              )}
              {additionalButton?.text && (
                <div
                  className='tw-cursor-pointer'
                  onClick={(e) =>
                    additionalButtonClose
                      ? handleCloseInfobox(e)
                      : handleAdditional()
                  }
                >
                  <b>{additionalButton?.text}</b>
                </div>
              )}
            </div>
          </div>
        ) : (
          <div
            className={clsx(
              'tw-fixed tw-left-1/2 tw-bottom-20 tw-transform -tw-translate-x-1/2',
              'tw-w-full tw-px-4 tw-z-[2000]',
            )}
          >
            <div
              className={clsx(
                'tw-rounded-lg tw-py-2 tw-px-4 tw-text-xs/normal tw-w-full',
                'tw-flex tw-justify-between tw-gap-2',
                setType(type),
              )}
            >
              {![
                'info',
                'success-text-white',
                'warning-pdp',
                'warning-top-pajak',
                'info-modal',
                'error-modal',
                'success-modal',
                'warning-modal',
                'error-minicart',
              ].includes(type) && (
                <img
                  src={`${config.assetsURL}icon/icon-infobox-${type}.svg`}
                  height={16}
                  width={16}
                  alt={`icon-${type}`}
                />
              )}
              <span>{children}</span>
              {textAction && (
                <div
                  className='tw-pointer'
                  onClick={(e) => handleCloseInfobox(e)}
                >
                  {textAction || 'OK'}
                </div>
              )}
              {additionalButton?.text && (
                <div
                  className='tw-flex tw-items-center'
                  onClick={(e) =>
                    additionalButtonClose
                      ? handleCloseInfobox(e)
                      : handleAdditional()
                  }
                >
                  <b>{additionalButton?.text}</b>
                </div>
              )}
            </div>
          </div>
        )}
      </>,
      document.body,
    )
  } else {
    return null
  }
}
