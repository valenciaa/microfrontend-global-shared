import clsx from 'clsx'
import isEmpty from 'lodash/isEmpty'
import InfoboxInfo from '../../../public/static/icon/infobox-info-16.svg'

export default function TWInfobox({
  children,
  className,
  textClassName = clsx('tw-text-xs'),
  size = 'size-default',
  theme,
  text,
  subtext,
  onClickSubText,
  position,
}) {
  const setTheme = () => {
    switch (theme) {
      case 'warning':
        return clsx('tw-bg-yellow-10', 'tw-text-yellow-60')
      case 'error':
        return clsx('tw-bg-red-10', 'tw-text-red-60')
      case 'info':
      default:
        return clsx('tw-bg-blue-10', 'tw-text-blue-60')
    }
  }

  const setSize = () => {
    switch (size) {
      case 'size-sm':
        return clsx('tw-rounded-5xl', 'tw-px-1 tw-py-2')
      case 'size-default':
      default:
        return clsx('tw-rounded-md', 'tw-px-2 tw-py-3')
    }
  }

  const setPosition = () => {
    switch (position) {
      case 'floating':
        return clsx(
          'tw-absolute tw-left-1/2 -tw-top-16 -tw-translate-x-1/2 -tw-translate-y-1/2',
        )
      case 'fixed':
        return clsx(
          'tw-absolute tw-left-1/2 tw-bottom-16 -tw-translate-x-1/2 -tw-translate-y-1/2 tw-z-50',
        )
      default:
        return undefined
    }
  }

  return (
    <div
      className={clsx(
        className,
        'tw-flex tw-items-center',
        setTheme(),
        setSize(),
        setPosition(),
      )}
    >
      {children}
      {text?.length > 0 && (
        <div>
          <div className='tw-flex tw-items-center'>
            <div className='tw-h-5 tw-w-5'>
              <InfoboxInfo height={20} width={20} />
            </div>
            <div className={textClassName}>{text}</div>
          </div>

          {!isEmpty(subtext) && (
            <span className='tw-font-semibold' onClick={onClickSubText}>
              {subtext}
            </span>
          )}
        </div>
      )}
    </div>
  )
}
