import React from 'react'
import { useRouter } from 'next/router'
import startCase from 'lodash/startCase'
import ChevronRightIcon from '../../../public/static/icon/chevron-next.svg'

export default function TWBreadcrumb({ params }) {
  const router = useRouter()

  const decodeParams = (params) =>
    typeof params === 'string' ? params.replace(/-/g, ' ') : params

  const decodedParams = params.map(decodeParams)
  const capitalizedParams = decodedParams.map(startCase)

  return (
    <nav className='tw-flex' aria-label='Breadcrumb'>
      <ol className='tw-inline-flex tw-items-center tw-space-x-1 tw-list-none md:tw-space-x-2 rtl:tw-space-x-reverse'>
        <li className='tw-inline-flex tw-items-center'>
          <button
            onClick={() => router.push(`/${params[0]}`)}
            className='tw-inline-flex tw-items-center tw-text-sm tw-font-medium tw-text-informa tw-cursor-pointer'
          >
            {capitalizedParams[0]}
          </button>
        </li>
        {params.slice(1, params.length).map((param, index) => (
          <li key={index}>
            <div className='tw-flex tw-items-center'>
              <ChevronRightIcon
                className='tw-text-informa'
                height='1em'
                width='1em'
              />
              {index === params.slice(1, params.length).length - 1 ? (
                <span className='tw-ms-1 tw-text-sm tw-font-medium tw-text-gray-500 md:tw-ms-2'>
                  {startCase(decodeParams(param))}
                </span>
              ) : (
                <button
                  onClick={() => router.push(`/${param}`)}
                  className='tw-ms-1 tw-text-sm tw-font-medium tw-text-informa tw-transition-all tw-ease-in-out tw-duration-200 tw-cursor-pointer'
                >
                  {startCase(decodeParams(param))}
                </button>
              )}
            </div>
          </li>
        ))}
      </ol>
    </nav>
  )
}
