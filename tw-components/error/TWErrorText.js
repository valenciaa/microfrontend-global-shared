import clsx from 'clsx'

const TWErrorText = ({
  additionalClass,
  id,
  message = 'Terjadi kesalahan, mohon ulangi beberapa saat lagi',
}) => {
  return (
    <p className={clsx('tw-text-red-50 tw-text-xs', additionalClass)} id={id}>
      {message}
    </p>
  )
}

export default TWErrorText
