import clsx from 'clsx'
import { useEffect, useState } from 'react'
import { createPortal } from 'react-dom'
import config from '../../../../../config'

export default function RefundSuggestionInfo({ duration, action, setToggle }) {
  const [showInfobox, setShowInfobox] = useState(true)
  const [isAnimation, setIsAnimation] = useState(false)

  let timer = null

  useEffect(() => {
    if (duration) {
      timer = setTimeout(() => {
        setShowInfobox(false)

        setToggle?.(false)
      }, duration)
    }

    if (showInfobox) {
      setTimeout(() => {
        setIsAnimation(true)
      }, 100)
    } else {
      setIsAnimation(false)
    }

    return function cleanup() {
      clearTimeout(timer)
    }
  }, [])

  const handleCloseInfobox = (e) => {
    e?.preventDefault()

    clearTimeout(timer)

    setShowInfobox(false)

    setToggle?.(false)
  }

  const handleActionButton = () => {
    if (action) {
      action()

      clearTimeout(timer)

      setShowInfobox(false)

      setToggle?.(false)
    }
  }

  if (showInfobox) {
    return createPortal(
      <>
        {config.environment === 'desktop' ? (
          <div
            className={clsx(
              'tw-fixed tw-transform tw-left-1/2 -tw-translate-x-1/2 tw-z-[2000] tw-duration-500 tw-transition-all',
              isAnimation ? 'tw-top-40' : '-tw-top-20',
            )}
          >
            <div
              className={clsx(
                'tw-px-6 tw-py-3 tw-rounded-lg tw-flex tw-gap-2',
                'tw-border tw-border-solid tw-border-orange-50 tw-bg-orange-10',
              )}
            >
              <img
                src={`${config.assetsURL}icon/warning-box-icon.svg`}
                height={32}
                width={32}
                alt='warning-box-icon'
              />

              <div
                className='tw-flex tw-flex-grow tw-flex-col tw-cursor-pointer'
                onClick={() => handleActionButton()}
              >
                <span className='tw-font-semibold tw-text-orange-50'>
                  Pesanan Kamu Mengalami Kendala?
                </span>
                <span>Klik disini untuk mengajukan pengembalian</span>
              </div>

              <img
                onClick={() => handleCloseInfobox()}
                src={config.assetsURL + 'icon/close-dark.svg'}
                height={16}
                width={16}
                className='tw-cursor-pointer'
                alt='close-dark-icon'
              />
            </div>
          </div>
        ) : (
          <div
            className={clsx(
              'tw-fixed tw-left-1/2 tw-bottom-20 tw-transform -tw-translate-x-1/2',
              'tw-w-full tw-px-4 tw-z-[2000]',
            )}
          >
            <div
              className={clsx(
                'tw-rounded-lg tw-py-2 tw-px-4 tw-text-xs/normal tw-w-full',
                'tw-flex tw-gap-2',
                'tw-border tw-border-solid tw-border-orange-50 tw-bg-orange-10',
              )}
            >
              <img
                src={`${config.assetsURL}icon/warning-box-icon.svg`}
                height={32}
                width={32}
                alt='warning-box-icon'
              />

              <div
                className='tw-flex tw-flex-grow tw-flex-col'
                onClick={() => handleActionButton()}
              >
                <span className='tw-font-semibold tw-text-orange-50'>
                  Pesanan Kamu Mengalami Kendala?
                </span>
                <span>Klik disini untuk mengajukan pengembalian</span>
              </div>

              <img
                onClick={() => handleCloseInfobox()}
                src={config.assetsURL + 'icon/close-dark.svg'}
                height={16}
                width={16}
                alt='close-dark-icon'
              />
            </div>
          </div>
        )}
      </>,
      document.body,
    )
  }

  return null
}
