import StarIcon from '../../../../../public/static/icon/star-icon.svg'

export default function ReviewRatingStoreCapsule({ rating }) {
  if (typeof rating !== 'number' || rating <= 0) {
    return
  }

  return (
    <div className='tw-flex tw-bg-yellow-10 tw-text-yellow-50 tw-rounded-2xl tw-gap-0.5 tw-items-center tw-justify-center tw-py-1 tw-px-2 tw-w-14'>
      <StarIcon height={16} width={16} />
      <p className='tw-text-sm tw-font-semibold'>{rating}</p>
    </div>
  )
}
