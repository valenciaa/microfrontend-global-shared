import { useState } from 'react'
import config from '../../../../../config'
import { useCartAuthContext } from '../../../../../shared/global/context/CartAuthContext'
import CustomLazyLoadImage from '../../../../../shared/global/layouts/atoms/CustomLazyLoadImage'
import { mixpanelTrack } from '../../../../../shared/global/utils/MixpanelWrapper'
import { handleComplainNameForMixpanel } from '../../../../../shared/global/utils/UtilReturnRefund'
import TWButton from '../../../tw-components/buttons/TWButton'
import TWRadio from '../../../tw-components/inputs/TWRadio'

export default function ReviewRatingComplainList({ data, onCloseModal }) {
  const invoiceNo = data?.invoice_no
  const isDcInvoice = data?.store_code?.indexOf('DC') > -1

  const isDesktop = config.environment === 'desktop'

  const { state: authData } = useCartAuthContext()
  const customerEmail = authData?.auth?.user?.email

  const [selectedComplainId, setSelectedComplainId] = useState('')

  const handleRedirectReturnRefundForm = (complainId) => {
    mixpanelTrack('Pilih Jenis Komplain', {
      'Alasan Komplain': handleComplainNameForMixpanel(complainId),
    })

    const url =
      config.mainBaseUrl +
      `return-refund/form?no=${invoiceNo}&email=${customerEmail}&complain_id=${complainId}`

    window.location.href = url
  }

  if (isDesktop) {
    return (
      <div className='tw-flex tw-flex-col tw-gap-4 tw-p-2'>
        <div className='tw-flex tw-items-center tw-gap-2'>
          <div>
            <CustomLazyLoadImage
              src='https://cdn.ruparupa.io/media/promotion/ruparupa/ruparupa-rewards/Icon/Profil-Akun/Arrow-Left.svg'
              alt='Back Icon'
              height={24}
              width={24}
              className='cursor-pointer'
              onClick={() => onCloseModal()}
            />
          </div>
          <div className='tw-text-xl tw-text-grey-100 tw-font-bold'>
            Pilih Jenis Komplain
          </div>
        </div>

        <div className='tw-flex tw-flex-col tw-gap-4'>
          <div
            className='tw-flex tw-flex-row tw-gap-2 tw-cursor-pointer tw-items-center'
            onClick={() => setSelectedComplainId('1')}
          >
            <TWRadio
              isChecked={selectedComplainId === '1'}
              value='1'
              name='complain-list-1'
            />
            <p className='tw-font-semibold tw-text-sm'>
              Produk Rusak/Cacat/Tidak Sesuai
            </p>
          </div>

          {!isDcInvoice && (
            <div
              className='tw-flex tw-flex-row tw-gap-2 tw-cursor-pointer tw-items-center'
              onClick={() => setSelectedComplainId('2')}
            >
              <TWRadio
                isChecked={selectedComplainId === '2'}
                value='2'
                name='complain-list-2'
              />
              <p className='tw-font-semibold tw-text-sm'>
                Jumlah Produk Kurang/Part Tidak Lengkap
              </p>
            </div>
          )}

          <div
            className='tw-flex tw-flex-row tw-gap-2 tw-cursor-pointer tw-items-center'
            onClick={() => setSelectedComplainId('4')}
          >
            <TWRadio
              isChecked={selectedComplainId === '4'}
              value='4'
              name='complain-list-4'
            />
            <p className='tw-font-semibold tw-text-sm'>Alasan Pribadi</p>
          </div>
        </div>

        <TWButton
          type='primary'
          size='big'
          width='w-full'
          disabled={!selectedComplainId}
          handleOnClick={() =>
            handleRedirectReturnRefundForm(selectedComplainId)
          }
        >
          Pilih Alasan
        </TWButton>
      </div>
    )
  }

  return (
    <div className='tw-flex tw-flex-col tw-gap-2'>
      <div
        className='tw-flex tw-flex-row tw-pb-2 tw-text-sm tw-font-semibold tw-border-b tw-border-solid tw-border-grey-10'
        onClick={() => handleRedirectReturnRefundForm('1')}
      >
        Produk Rusak/Cacat/Tidak Sesuai
      </div>
      {!isDcInvoice && (
        <div
          className='tw-flex tw-flex-row tw-py-2  tw-text-sm tw-font-semibold tw-border-b tw-border-solid tw-border-grey-10'
          onClick={() => handleRedirectReturnRefundForm('2')}
        >
          Jumlah Produk Kurang/Part Tidak Lengkap
        </div>
      )}
      <div
        className='tw-flex tw-flex-row tw-py-2  tw-text-sm tw-font-semibold tw-border-b tw-border-solid tw-border-grey-10'
        onClick={() => handleRedirectReturnRefundForm('4')}
      >
        Alasan Pribadi
      </div>
    </div>
  )
}
