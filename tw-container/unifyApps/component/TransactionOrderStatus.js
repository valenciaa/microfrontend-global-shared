import clsx from 'clsx'
import React from 'react'
import config from '../../../../../config'

const statusStyle = {
  danger: clsx('tw-bg-red-20 tw-text-red-60'),
  info: clsx('tw-bg-blue-10 tw-text-blue-50'),
  success: clsx('tw-bg-green-20 tw-text-green-60'),
  warning: clsx('tw-bg-yellow-20 tw-text-yellow-60'),
}

const containerStyle = {
  desktop: 'tw-rounded-5xl tw-p-2',
  mobile: 'tw-rounded-sm tw-p-1',
}

const textStyle = {
  desktop: 'tw-text-sm',
  mobile: 'tw-text-xxs',
}

export default function TransactionOrderStatus({ text, type }) {
  return (
    <div
      className={clsx(containerStyle[config.environment], statusStyle[type])}
    >
      <p className={clsx(textStyle[config.environment])}>{text}</p>
    </div>
  )
}
