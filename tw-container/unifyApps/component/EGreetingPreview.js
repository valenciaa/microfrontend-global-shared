import clsx from 'clsx'
import { useState } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import config from '../../../../../config'

export default function EGreetingPreview({ data }) {
  const isMobile = config.environment === 'mobile'

  const { phone, email, receiver_name, message } = data

  const [isOpenDetail, setIsOpenDetail] = useState(false)

  return (
    <div
      className={clsx(
        'tw-flex tw-flex-col',
        'tw-bg-grey-5 tw-p-3 tw-gap-2 tw-cursor-pointer',
        'tw-border tw-border-solid tw-border-grey-20 tw-rounded-md',
        isMobile ? 'tw-text-xs' : 'tw-text-sm',
      )}
      onClick={() => setIsOpenDetail(!isOpenDetail)}
    >
      <div className='tw-flex tw-flex-row tw-flex-grow'>
        <p className='tw-flex tw-flex-grow tw-font-bold tw-text-grey-100'>
          Kartu Ucapan Digital
        </p>

        <LazyLoadImage
          src={config.assetsURL + 'icon/chevron-down.svg'}
          className={`tw-flex ${isOpenDetail ? 'tw-rotate-180' : ''}`}
          alt='dropdown-icon'
          height={16}
          width={16}
        />
      </div>

      {isOpenDetail && (
        <div className='tw-flex tw-flex-col tw-border-t tw-border-solid tw-border-grey-20 tw-pt-2 tw-gap-1 tw-break-all'>
          <div className='tw-flex tw-flex-row tw-gap-1'>
            <p className='tw-w-32 tw-flex-shrink-0'>Nama</p>
            <p>{receiver_name}</p>
          </div>

          <div className='tw-flex tw-flex-row tw-gap-1'>
            <p className='tw-w-32 tw-flex-shrink-0'>
              {phone ? 'Nomor Whatsapp' : 'Email'}
            </p>
            <p>{phone ? phone : email}</p>
          </div>

          <div className='tw-flex tw-flex-row tw-gap-1'>
            <p className='tw-w-32 tw-flex-shrink-0'>Pesan</p>
            <p>{message}</p>
          </div>
        </div>
      )}
    </div>
  )
}
