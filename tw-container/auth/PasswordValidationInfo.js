import config from '../../../../config'

export function PasswordValidationInfo(password = '') {
  const passwordValidations = [
    {
      label: 'Minimum 8 karakter',
      regex: /.{8,}/,
    },
    {
      label: 'Huruf kapital (A, B, C)',
      regex: /(?=.*?[A-Z])/,
    },
    {
      label: 'Huruf kecil (a, b, c)',
      regex: /(?=.*?[a-z])/,
    },
  ]
  if (password) {
    const passwordString = String(password)

    return (
      <div className='tw-grid tw-grid-cols-2 tw-gap-2'>
        {passwordValidations.map((el, id) => {
          return (
            <div key={id} className='tw-flex tw-gap-1 tw-items-center'>
              <div>
                {passwordString.match(el.regex) ? (
                  <img
                    src={`${config.assetsURL}icon/green-check.svg`}
                    alt='Wrong Icon'
                    className='tw-h-4 tw-w-4'
                  />
                ) : (
                  <img
                    src={`${config.assetsURL}icon/close-ahi.svg`} // ACE_IMAGE
                    alt='Wrong Icon'
                    className='tw-h-4 tw-w-4'
                  />
                )}
              </div>
              <div className='tw-text-grey-40 tw-text-xs'>{el.label}</div>
            </div>
          )
        })}
      </div>
    )
  }
}
