import React from 'react'

import { useGetCmsBlockDetail } from '../../../services/api/queries/homepage'
import { ConvertImageType, ItmGenerator } from '../../../utils'
// import { mixpanelTrack } from '../../../utils/MixpanelWrapper'

import isEmpty from 'lodash/isEmpty'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../config'

const pageType = 'store-location'

export default function BannerSisip() {
  const { data: bannerSisip } = useGetCmsBlockDetail('banner-sisip-promo-major')

  const newUrl = (url, campaign) => {
    return ItmGenerator(pageType + '-banner-sisip', url, campaign)
  }

  return (
    <section className='tw-p-5 tw-bg-white tw-rounded-md tw-flex tw-flex-col tw-gap-7 tw-shadow-md'>
      {!isEmpty(bannerSisip?.header_description) &&
        (!isEmpty(bannerSisip?.layout) || !isEmpty(bannerSisip?.content)) && (
          <div className='tw-flex tw-gap-2 lg:tw-gap-4'>
            <a
              // onClick={() =>
              //   mixpanelTrack('Banner Sisip Store Location', {
              //     Title: bannerSisip?.header_description?.[0]?.title || 'None',
              //   })
              // }
              href={newUrl(
                bannerSisip?.header_description?.[0]?.urlLink,
                bannerSisip?.header_description?.[0]?.title,
              )}
            >
              <CustomLazyLoadImage
                src={ConvertImageType(
                  bannerSisip?.header_description?.[0]?.imageURL,
                )}
                placeholderAspectRatio={
                  config.environment === 'mobile' ? 121 / 339 : 120 / 956
                }
                placeholder={<Skeleton />}
                height={config.environment === 'mobile' ? '100%' : '120'}
                width={config.environment === 'mobile' ? '100%' : '956'}
                className='tw-rounded-md'
                alt={bannerSisip?.header_description?.[0]?.title || 'Banner 1'}
              />
            </a>
            <a
              // onClick={() =>
              //   mixpanelTrack('Banner Sisip Store Location', {
              //     Title:
              //       config.environment === 'mobile'
              //         ? bannerSisip?.content?.[0]?.title || 'None'
              //         : bannerSisip?.layout?.[0]?.content?.[0]?.title || 'None',
              //   })
              // }
              href={
                config.environment === 'mobile'
                  ? newUrl(
                      bannerSisip?.content?.[0]?.target_link,
                      bannerSisip?.content?.[0]?.title,
                    )
                  : newUrl(
                      bannerSisip?.layout?.[0]?.content?.[0]?.target_link,
                      bannerSisip?.layout?.[0]?.content?.title,
                    )
              }
            >
              <CustomLazyLoadImage
                src={
                  config.environment === 'mobile'
                    ? ConvertImageType(bannerSisip?.content?.[0]?.image)
                    : ConvertImageType(
                        bannerSisip?.layout?.[0]?.content?.[0]?.image,
                      )
                }
                placeholderAspectRatio={
                  config.environment === 'mobile' ? 121 / 121 : 120 / 308
                }
                placeholder={<Skeleton />}
                height={config.environment === 'mobile' ? '100%' : '120'}
                width={config.environment === 'mobile' ? '100%' : '308'}
                className='tw-rounded-md'
                alt={
                  config.environment === 'mobile'
                    ? bannerSisip?.content?.[0]?.title || 'Banner 2'
                    : bannerSisip?.layout?.[0]?.content?.[0].title || 'Banner 2'
                }
              />
            </a>
          </div>
        )}
    </section>
  )
}
