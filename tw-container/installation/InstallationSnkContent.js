import clsx from 'clsx'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import TWStickyBottomWrapper from '../../tw-components/TWStickyBottomWrapper'
import TWButton from '../../tw-components/buttons/TWButton'
import { ACE_HARDWARE } from '../../utils/constants/AceConstants'

export default function InstallationSnkContent() {
  const isMobile = config.environment === 'mobile'

  const textSize = isMobile ? 'tw-text-xs' : 'tw-text-sm'

  return (
    <>
      <div className='tw-flex tw-flex-col tw-px-4 tw-overflow-y-auto'>
        <div className='tw-flex tw-py-4 tw-border-b tw-border-solid tw-border-grey-20'>
          <div className='tw-flex tw-items-center tw-gap-3'>
            <LazyLoadImage
              className='tw-flex-shrink-0'
              alt='installation-discount-icon'
              src={`${config.assetsURL}icon/installation/installation-discount-icon.svg`}
              width='32px'
              height='32px'
              placeholder={<Skeleton height={32} width={32} />}
            />
            <p className={textSize}>
              Total{' '}
              <b>
                harga jasa perakitan akan ditentukan setelah pesanan kamu sampai
              </b>{' '}
              ke alamat pengiriman.
            </p>
          </div>
        </div>

        <div className='tw-flex tw-py-4 tw-border-b tw-border-solid tw-border-grey-20'>
          <div className='tw-flex tw-items-center tw-gap-3'>
            <LazyLoadImage
              className='tw-flex-shrink-0'
              alt='installation-cc-icon'
              src={`${config.assetsURL}icon/installation/installation-cc-icon.svg`}
              width='32px'
              height='32px'
              placeholder={<Skeleton height={32} width={32} />}
            />
            <p className={textSize}>
              <b>Untuk produk Informa & Selma, jika alamat melebihi 20km</b>{' '}
              dari lokasi tim jasa rakit, maka{' '}
              <b>biaya jasa perakitan akan dihitung ulang</b> oleh tim kami.{' '}
              <b>Untuk produk {ACE_HARDWARE}, jika alamat melebihi 30km</b> dari
              lokasi tim jasa rakit, maka{' '}
              <b>biaya jasa perakitan akan dihitung ulang</b> oleh tim kami.
            </p>
          </div>
        </div>

        <div className='tw-flex tw-py-4 tw-border-b tw-border-solid tw-border-grey-20'>
          <div className='tw-flex tw-items-center tw-gap-3'>
            <LazyLoadImage
              className='tw-flex-shrink-0'
              alt='installation-calendar-icon'
              src={`${config.assetsURL}icon/installation/installation-calendar-icon.svg`}
              width='32px'
              height='32px'
              placeholder={<Skeleton height={32} width={32} />}
            />
            <p className={textSize}>
              <b>Waktu pembayaran jasa perakitan adalah H+1</b> ketika harga
              jasa perakitan telah keluar.
            </p>
          </div>
        </div>

        <div className='tw-flex tw-py-4 tw-border-b tw-border-solid tw-border-grey-20'>
          <div className='tw-flex tw-items-center tw-gap-3'>
            <LazyLoadImage
              className='tw-flex-shrink-0'
              alt='installation-calendar-icon'
              src={`${config.assetsURL}icon/installation/installation-calendar-icon.svg`}
              width='32px'
              height='32px'
              placeholder={<Skeleton height={32} width={32} />}
            />
            <p className={textSize}>
              Pelanggan{' '}
              <b>
                dapat mengubah tanggal perakitan dan membatalkan jasa perakitan
                paling lambat H-1
              </b>{' '}
              dari tanggal yang telah keluar.
            </p>
          </div>
        </div>

        <div className='tw-flex tw-py-4 tw-border-b tw-border-solid tw-border-grey-20'>
          <div className='tw-flex tw-items-center tw-gap-3'>
            <LazyLoadImage
              className='tw-flex-shrink-0'
              alt='installation-location-icon'
              src={`${config.assetsURL}icon/installation/installation-location-icon.svg`}
              width='32px'
              height='32px'
              placeholder={<Skeleton height={32} width={32} />}
            />
            <p className={textSize}>
              <b>Teknisi akan menuju alamat perakitan</b> setelah tanggal
              perakitan telah keluar.
            </p>
          </div>
        </div>

        <div className='tw-flex tw-py-4 tw-border-b tw-border-solid tw-border-grey-20'>
          <div className='tw-flex tw-items-center tw-gap-3'>
            <LazyLoadImage
              className='tw-flex-shrink-0'
              alt='installation-cancel-icon'
              src={`${config.assetsURL}icon/installation/installation-cancel-icon.svg`}
              width='32px'
              height='32px'
              placeholder={<Skeleton height={32} width={32} />}
            />
            <div className={clsx('tw-flex tw-flex-col tw-gap-2', textSize)}>
              <p>
                <b>Jika pesanan dikirim menggunakan armada toko</b>, jangan
                membuka pesanan sebelum teknisi perakitan datang. Pelanggan
                wajib mendampingi teknisi saat produk dibuka dan selama proses
                perakitan berlangsung.
              </p>
              <p>
                <b>Jika pesanan dikirim menggunakan selain armada toko</b>,
                harap segera membuka pesanan untuk memastikan kondisi barang
                sebelum teknisi datang. Pelanggan wajib mendampingi teknisi
                selama proses perakitan berlangsung.
              </p>
            </div>
          </div>
        </div>

        <div className='tw-flex tw-py-4 tw-border-b tw-border-solid tw-border-grey-20'>
          <div className='tw-flex tw-items-center tw-gap-3'>
            <LazyLoadImage
              className='tw-flex-shrink-0'
              alt='installation-photo-icon'
              src={`${config.assetsURL}icon/installation/installation-photo-icon.svg`}
              width='32px'
              height='32px'
              placeholder={<Skeleton height={32} width={32} />}
            />
            <p className={textSize}>
              <b>Pelanggan wajib melakukan dokumentasi,</b> berupa video atau
              foto saat proses membuka pesanan produk dan memastikan pesanan
              yang diterima telah lengkap, sesuai, dan dalam keadaan yang baik.
            </p>
          </div>
        </div>
      </div>

      <TWStickyBottomWrapper
        additionalClass={clsx(
          'tw-flex tw-flex-col tw-items-center tw-gap-1 tw-pt-4 tw-px-4',
          isMobile && 'tw-pb-4',
        )}
      >
        <p className='tw-text-xs'>
          Untuk lebih jelas tentang jasa perakitan silahkan klik tombol di bawah
          ini
        </p>

        <TWButton
          additionalClass='tw-rounded-md tw-w-full'
          handleOnClick={() =>
            window.open(
              `${config.baseURL}page/syarat-dan-ketentuan?id=instalasi`,
            )
          }
          size='big'
          type='primary'
        >
          Cek Pusat Bantuan
        </TWButton>
      </TWStickyBottomWrapper>
    </>
  )
}
