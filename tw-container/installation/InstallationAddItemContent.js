import clsx from 'clsx'
import isEmpty from 'lodash/isEmpty'
import { useState } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import { useAddressContext } from '../../context/AddressContext'
import { useCreateInstallationAfterOrder } from '../../services/api/mutations/unifyapps'
import TWButton from '../../tw-components/buttons/TWButton'
import TWCheckbox from '../../tw-components/inputs/TWCheckbox'
import TWBaseModal from '../../tw-components/modals/TWBaseModal'
import TWStickyBottomWrapper from '../../tw-components/TWStickyBottomWrapper'
import { mixpanelTrackSubmitInstallation } from '../../utils/MixpanelWrapper'
import {
  handleAddressReceiverNameTemplate,
  handleAddressTemplate,
} from '../../utils/UtilAddress'
import InstallationInfobox from '../../modules/unifyapps/InstallationInfobox'

const buColorMap = {
  bg: {
    ruparupa: clsx('tw-bg-ruparupa'),
    ace: clsx('tw-bg-ace'),
    informa: clsx('tw-bg-informa'),
    toyskingdomonline: clsx('tw-bg-toyskingdomonline'),
  },
  text: {
    ruparupa: clsx('tw-text-ruparupa'),
    ace: clsx('tw-text-ace'),
    informa: clsx('tw-text-informa'),
    toyskingdomonline: clsx('tw-text-toyskingdomonline'),
  },
}

export default function InstallationAddItemContent({
  selectedAddress,
  storeName,
  itemNotInstalled,
  itemCanInstalled,
  setItemCanInstalled,
  referenceInvoice,
  customerEmail,
}) {
  const { address_id, first_name, last_name, phone, address_name, is_default } =
    selectedAddress

  const companyName = config.companyNameCSS || 'ruparupa'

  const isMobile = config.environment === 'mobile'

  const { handleChangeStateObject: changeAddressContext } = useAddressContext()

  const { mutate: createInstallation } = useCreateInstallationAfterOrder({
    onSuccess: (res) => {
      if (res?.data?.data?.cart_id) {
        window.location.assign(
          `${config?.paymentURL}?token=${res?.data?.data?.cart_id}`,
        )
        return
      }

      if (res?.data?.data?.sales_installation_id) {
        changeAddressContext({
          isShowInfoBox: true,
          infoBoxType: 'success',
          infoBoxMessage: 'Berhasil membuat order perakitan',
        })

        window.location.assign(
          `${config?.legacyBaseURL}my-account/transaction/list?tab=installation`,
        )

        return
      }

      const errorMessage =
        res?.data?.message || 'Terjadi kesalahan, silahkan coba lagi'

      changeAddressContext({
        isShowInfoBox: true,
        infoBoxType: 'error',
        infoBoxMessage: errorMessage,
      })
    },
  })

  const [skuInstallation, setSkuInstallation] = useState([])
  const [isOpenConfirmationModal, setIsOpenConfirmationModal] = useState(false)

  const checkAddressHasCoordinate = (data) => {
    if (data?.geolocation) {
      return true
    }
    return false
  }

  const handleAddInstallation = () => {
    const isValidAddress = checkAddressHasCoordinate(selectedAddress)

    if (!isValidAddress) {
      changeAddressContext({
        isShowInfoBox: true,
        infoBoxType: 'error',
        infoBoxMessage:
          'Perakitan hanya dapat dilakukan untuk alamat yang memiliki koordinat, mohon untuk menambahkan koordinat pada alamat yang dipilih',
      })

      return
    }

    const data = {
      customer_email: customerEmail,
      reference_invoice_no: referenceInvoice,
      installation_items: skuInstallation,
      customer_address_id: parseInt(address_id),
    }

    changeAddressContext({
      isShowInfoBox: false,
      infoBoxMessage: '',
    })

    mixpanelTrackSubmitInstallation(skuInstallation, itemNotInstalled)

    createInstallation(data)
  }

  const handleCheckItemInstallation = (data) => {
    if (!data) {
      return
    }

    const skuListData = skuInstallation

    const needInstallationItem = itemCanInstalled?.map((item) => {
      if (item?.sku === data) {
        if (item?.is_selected) {
          item = { ...item, is_selected: false }
        } else {
          item = { ...item, is_selected: true }
        }

        if (item?.is_selected) {
          skuListData.push(data)
          setSkuInstallation(skuListData)
        } else {
          const uncheckedData = skuInstallation?.filter((sku) => sku !== data)
          setSkuInstallation(uncheckedData)
        }
      }

      return item
    })

    setItemCanInstalled(needInstallationItem)
  }

  const renderInstallationItem = (item, index, total) => {
    let imageUrl = item?.image_url
      ? `${config.imageURL}w_360,h_360,f_auto,q_auto${item?.image_url}`
      : 'static/images/no-image.jpg'

    if (item?.full_image_url) {
      imageUrl = item?.full_image_url
    }

    return (
      <div
        className={clsx(
          'tw-p-4',
          total > 1 && 'tw-border-b tw-border-solid tw-border-grey-20',
        )}
        key={`installation-list-item-${index}`}
      >
        <div className='tw-flex tw-items-center tw-gap-2'>
          <div>
            <TWCheckbox
              size='medium'
              isChecked={item?.is_selected}
              handleOnChange={() => handleCheckItemInstallation(item?.sku)}
            />
          </div>

          <LazyLoadImage
            src={imageUrl}
            width={72}
            height={72}
            placeholder={<Skeleton height={72} />}
          />

          <div className='tw-flex tw-flex-col tw-flex-1 tw-gap-2'>
            <div className='tw-flex tw-gap-0.5'>
              <p className='tw-flex-1 tw-text-sm tw-capitalize'>
                {item?.name.toLowerCase()}
              </p>

              <p className='tw-text-sm'>x{item?.qty_ordered}</p>
            </div>
            <div className='tw-flex tw-gap-0.5'>
              <p className='tw-text-xs tw-flex-1'>Jumlah Perakitan</p>

              <p className='tw-text-xs'>{item?.qty_ordered} produk</p>
            </div>
          </div>
        </div>
      </div>
    )
  }

  return (
    <>
      <section className='tw-border-b-[5px] tw-border-solid tw-border-grey-20'>
        <div className='tw-flex tw-flex-col tw-gap-2'>
          <InstallationInfobox />
          <div className='tw-flex tw-justify-between tw-px-4 tw-pt-4'>
            <p className='tw-font-bold'>Alamat Perakitan</p>

            <p
              className={clsx(
                'tw-font-bold tw-text-sm tw-cursor-pointer',
                buColorMap.text[companyName],
              )}
              onClick={
                () => changeAddressContext({ showAddressCardList: true })
                // open address modal
              }
            >
              Pilih Alamat Lain
            </p>
          </div>
          <div className='tw-flex tw-gap-2 tw-px-4 tw-pb-4'>
            <div className='tw-w-8 tw-h-8 tw-flex-shrink-0'>
              <LazyLoadImage
                src={`${config.assetsURL}icon/icon-pin.svg`}
                width={32}
                height={32}
                placeholder={<Skeleton height={32} />}
              />
            </div>

            <div className='tw-flex tw-flex-col tw-flex-grow tw-gap-1'>
              <div className='tw-flex tw-items-center tw-gap-1'>
                <p className='tw-font-bold tw-truncate'>{address_name}</p>

                {is_default === '1' && (
                  <div
                    className={clsx(
                      'tw-flex tw-rounded-3xl tw-items-center tw-py-0.5 tw-px-1 tw-text-xxs tw-font-semibold tw-text-white',
                      buColorMap.bg[companyName],
                    )}
                  >
                    UTAMA
                  </div>
                )}
              </div>

              <div className='tw-flex tw-items-center tw-gap-0.5'>
                <p className='tw-text-sm tw-truncate tw-flex-shrink'>
                  {handleAddressReceiverNameTemplate(first_name, last_name)}
                </p>

                <p className='tw-text-sm'>({phone})</p>
              </div>

              <div className='tw-flex tw-items-center'>
                <p className='tw-text-sm tw-text-grey-40 tw-line-clamp-2 tw-capitalize tw-break-all'>
                  {handleAddressTemplate(selectedAddress)}
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className='tw-py-3 tw-px-4 tw-border-b tw-border-solid tw-border-grey-20'>
        <div className='flex fex-row items-center gap-xs'>
          <LazyLoadImage
            src={`${config.assetsURL}icon/installation/installation-store.svg`}
            height={32}
            width={32}
            alt='store-location'
          />
          <p className='tw-text-sm tw-flex-grow tw-truncate'>{storeName}</p>
        </div>
      </div>

      <section className='tw-border-b-[5px] tw-border-solid tw-border-grey-20 tw-overflow-y-auto'>
        {itemCanInstalled?.map((item, index) =>
          renderInstallationItem(item, index, itemCanInstalled?.length),
        )}
      </section>

      <TWStickyBottomWrapper additionalClass={!isMobile && 'tw-pt-4 tw-px-4'}>
        <TWButton
          additionalClass='tw-rounded-md tw-w-full'
          disabled={isEmpty(skuInstallation)}
          handleOnClick={() => {
            const isNeedConfirmation =
              skuInstallation?.length > 0 &&
              skuInstallation?.length !== itemCanInstalled?.length

            if (isNeedConfirmation) {
              setIsOpenConfirmationModal(true)

              return
            }

            handleAddInstallation()
          }}
          size={isMobile ? 'medium' : 'big'}
          type='primary'
        >
          Tambah Jasa Perakitan
        </TWButton>
      </TWStickyBottomWrapper>

      {isOpenConfirmationModal && (
        <TWBaseModal
          animationType='fade'
          customPadding={clsx(
            'tw-px-4',
            isMobile ? 'tw-py-6 tw-w-[343px]' : 'tw-py-8 tw-w-[416px]',
          )}
          isShow={isOpenConfirmationModal}
          modalType='middle-small'
        >
          <div className='tw-flex tw-flex-col'>
            <p
              className={clsx(
                'tw-flex tw-flex-grow tw-font-bold',
                isMobile ? 'tw-pb-2' : 'tw-text-xl tw-pb-2',
              )}
            >
              Tambah Jasa Perakitan
            </p>

            <p className={clsx('tw-text-grey-50', isMobile && 'tw-text-sm')}>
              Masih ada pesananmu yang membutuhkan perakitan. Jasa perakitan
              lebih hemat jika kamu menjadwalkannya di saat yang sama
            </p>

            <div
              className={clsx(
                'tw-flex',
                isMobile ? 'tw-pt-3 tw-gap-3' : 'tw-pt-4 tw-gap-4',
              )}
            >
              <TWButton
                additionalClass='tw-rounded-md tw-w-full'
                handleOnClick={() => handleAddInstallation()}
                size={isMobile ? 'medium' : 'big'}
                type='primary-border'
              >
                Lanjutkan
              </TWButton>

              <TWButton
                additionalClass='tw-rounded-md tw-w-full'
                handleOnClick={() => setIsOpenConfirmationModal(false)}
                size={isMobile ? 'medium' : 'big'}
                type='primary'
              >
                Tambahkan
              </TWButton>
            </div>
          </div>
        </TWBaseModal>
      )}
    </>
  )
}
