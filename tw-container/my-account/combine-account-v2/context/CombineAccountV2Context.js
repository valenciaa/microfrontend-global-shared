import { useContext, createContext, useMemo, useState } from 'react'
import isEmpty from 'lodash/isEmpty'
import { useGetAllMerchant } from '../../../../services/api/queries/global'

export const CombineAccountV2Context = createContext({})
export const CombineAccountActionV2Context = createContext({})

export const CombineAccountV2Provider = ({ children }) => {
  const [merchants, setMerchants] = useState([])
  const [chosenMerchant, setChosenMerchant] = useState(null)

  useGetAllMerchant(
    {},
    {
      onSuccess: (res) => {
        if (!isEmpty(res)) {
          setMerchants(res)
        }
      },
    },
  )

  const value = useMemo(() => {
    return { merchants, chosenMerchant }
  })

  const actions = useMemo(() => {
    return { setChosenMerchant }
  }, [setChosenMerchant])

  return (
    <CombineAccountV2Context.Provider value={value}>
      <CombineAccountActionV2Context.Provider value={actions}>
        {children}
      </CombineAccountActionV2Context.Provider>
    </CombineAccountV2Context.Provider>
  )
}

export function useCombineAccountV2Context() {
  return useContext(CombineAccountV2Context)
}

export function useCombineAccountActionV2Context() {
  return useContext(CombineAccountActionV2Context)
}
