import config from '../../../../../config'
import TWButton from '../../../tw-components/buttons/TWButton'
import TWBaseModal from '../../../tw-components/modals/TWBaseModal'
import {
  RUPARUPA_BISNIS_EMAIL_DISPLAY,
  RUPARUPA_BISNIS_EMAIL_LINK,
  RUPARUPA_BISNIS_WA_DISPLAY,
  RUPARUPA_BISNIS_WA_LINK,
} from '../utils/b2bConstants'

export default function SuccessModal({ isSuccess }) {
  if (isSuccess) {
    return (
      <TWBaseModal
        modalType='middle-small'
        isShow={isSuccess}
        isFullWidth={config?.environment === 'mobile'}
      >
        <div className='tw-flex tw-flex-col tw-gap-3 tw-px-2'>
          <div className='tw-text-grey-100 tw-text-xl tw-font-bold'>
            Pengajuan Penawaran Berhasil
          </div>
          <div className='tw-text-grey-50 tw-max-w-sm'>
            <div>
              Silakan cek email Anda untuk melihat detail penawaran yang telah
              berhasil dibuat. Jika terdapat pertanyaan lanjutan, kamu bisa
              menghubungi kami di:
            </div>
            <div>
              WhatsApp :{' '}
              <a href={RUPARUPA_BISNIS_WA_LINK} className='tw-text-blue-50'>
                {RUPARUPA_BISNIS_WA_DISPLAY}
              </a>
            </div>
            <div>
              Email :{' '}
              <a href={RUPARUPA_BISNIS_EMAIL_LINK} className='tw-text-blue-50'>
                {RUPARUPA_BISNIS_EMAIL_DISPLAY}
              </a>
            </div>
          </div>

          <TWButton
            type='primary'
            size='medium'
            handleOnClick={() => {
              sessionStorage.removeItem('b2b_quotation')
              window.location.href = config.baseURL + 'page/b2b'
            }}
          >
            Kembali ke ruparupa bisnis
          </TWButton>
        </div>
      </TWBaseModal>
    )
  } else {
    return null
  }
}
