import clsx from 'clsx'
import TWBaseModal from '../../../tw-components/modals/TWBaseModal'
import TWButton from '../../../tw-components/buttons/TWButton'

export default function ConfirmationModal({
  isConfirmation,
  setIsConfirmation,
  onSubmit,
  tempBody,
}) {
  if (isConfirmation) {
    return (
      <TWBaseModal
        modalType='middle-small'
        isShow
        animationType='fade'
        customPadding={clsx('tw-p-4')}
      >
        <div className='tw-flex tw-flex-col tw-gap-4 tw-w-80'>
          <div className='tw-flex tw-justify-between'>
            <div className='tw-text-grey-100 tw-font-bold'>
              Apakah Anda yakin ingin mengirimkan permintaan penawaran ini?
            </div>
          </div>

          <div className='tw-flex tw-justify-between tw-gap-2'>
            <TWButton
              type='primary-border'
              size='medium'
              width={clsx('tw-w-full tw-font-semibold')}
              handleOnClick={() => {
                setIsConfirmation(false)
              }}
            >
              Batal
            </TWButton>
            <TWButton
              type='primary'
              size='medium'
              width={clsx('tw-w-full tw-font-semibold')}
              handleOnClick={() => onSubmit(tempBody)}
            >
              Ya
            </TWButton>
          </div>
        </div>
      </TWBaseModal>
    )
  } else {
    return null
  }
}
