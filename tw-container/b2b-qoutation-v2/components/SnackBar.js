import { useEffect, useState } from 'react'
import TWFloatingInfobox from '../../../tw-components/TWFloatingInfobox'

export default function SnackBar({ infobarType, message, setSnackbar }) {
  const [isShow, setIsShow] = useState(false)

  let clearType
  useEffect(() => {
    clearTimeout(clearType)
    if (infobarType) {
      setIsShow(true)
      clearType = setTimeout(() => setSnackbar(), 2500)
    } else {
      setIsShow(false)
    }
    return function cleanup() {
      if (clearType) {
        clearTimeout(clearType)
      }
    }
  }, [infobarType])

  if (isShow) {
    return (
      <TWFloatingInfobox
        duration={2500}
        toggleFlag={isShow}
        setToggle={setIsShow}
        type={infobarType}
        additionalButton={{
          text: 'Tutup',
        }}
        additionalButtonClose
      >
        {message}
      </TWFloatingInfobox>
    )
  } else {
    return null
  }
}
