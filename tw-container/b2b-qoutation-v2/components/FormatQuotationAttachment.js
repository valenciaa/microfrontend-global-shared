import dayjs from 'dayjs'
import React, { forwardRef } from 'react'

import { NumberWithCommas } from '../../../utils'
import TahuWordingContent from '../../../layouts/atoms/TahuWordingContent'
import {
  RUPARUPA_BISNIS_EMAIL_ORDER,
  RUPARUPA_BISNIS_WA_DISPLAY,
} from '../utils/b2bConstants'

const FormatQuotationAttachment = forwardRef(({ data }, ref) => {
  return (
    <div
      ref={ref}
      className='print tw-px-3 tw-text-sm tw-font-quick tw-absolute tw-left-[-9999px] tw-w-[1123px]'
    >
      <div className='tw-mb-4 tw-text-center tw-text-xl tw-font-semibold'>
        PT OMNI DIGITAMA INTERNUSA
      </div>
      <div className='tw-py-4'>
        <div className='tw-flex tw-gap-4'>
          <div className='tw-w-1/2'>
            <div className='tw-grid tw-grid-cols-3 tw-gap-2'>
              <div>Nama Perusahaan :</div>
              <div className='tw-col-span-2 tw-break-all tw-font-semibold'>
                PT. OMNI DIGITAMA INTERNUSA
              </div>
            </div>
            <div className='tw-grid tw-grid-cols-3 tw-gap-2'>
              <div>Nomor Handphone :</div>
              <div className='tw-col-span-2 tw-reak-all tw-font-semibold'>
                {RUPARUPA_BISNIS_WA_DISPLAY}
              </div>
            </div>
            <div className='tw-grid tw-grid-cols-3 tw-gap-2'>
              <div>Fax :</div>
              <div className='tw-col-span-2 tw-break-all tw-font-semibold'>
                +62215820089
              </div>
            </div>
            <div className='tw-grid tw-grid-cols-3 tw-gap-2'>
              <div>Email :</div>
              <div className='tw-col-span-2 tw-break-all tw-font-semibold'>
                {RUPARUPA_BISNIS_EMAIL_ORDER}
              </div>
            </div>
          </div>
          <div className='tw-w-1/2'>
            <div className='tw-grid tw-grid-cols-4 tw-gap-2'>
              <div>No :</div>
              <div className='tw-col-span-3 tw-break-all tw-font-semibold'>
                {data?.quotation_no || '-'}
              </div>
            </div>
            <div className='tw-grid tw-grid-cols-4 tw-gap-2'>
              <div>Hal :</div>
              <div className='tw-col-span-3 tw-break-all tw-font-semibold'>
                Penawaran Harga
              </div>
            </div>
            <div className='tw-grid tw-grid-cols-4 tw-gap-2'>
              <div>Status :</div>
              <div className='tw-col-span-3 tw-break-all tw-font-semibold'>
                Draf
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='tw-absolute tw-right-0 tw-top-5'>
        <img
          src='https://cdn.ruparupa.io/promotion/ruparupa/b2b-rr/logo-bisnis.png'
          alt='logo-ruparupa'
          width={120}
        />
      </div>
      <div className='tw-py-4'>
        <div className='tw-font-semibold'>Kepada Yth:</div>
        <div className='tw-my-2'>{data?.name}</div>
        <div className='tw-my-2'>{data?.companyName}</div>
        <div className='tw-my-2'>{data?.email}</div>
      </div>
      <div className='tw-w-full tw-py-4'>
        <p className='tw-mb-5'>
          Pada kesempatan ini Kami hendak menawarkan produk Kami berupa:
        </p>
        <table className='tw-border-1 tw-w-full tw-table-auto tw-text-center'>
          <thead>
            <tr className='tw-border-1 tw-mb-4 tw-border-b tw-border-gray-200'>
              <th className='tw-w-[10%]'>SKU</th>
              <th className='tw-w-[25%]'>Nama Produk</th>
              <th className='tw-w-20'>Jumlah</th>
              <th className='tw-text-wrap tw-w-36 tw-px-4'>Harga Normal</th>
              <th className='tw-text-wrap tw-w-36 tw-px-4'>Harga Spesial</th>
              <th className='tw-36'>Diskon (%)</th>
              <th className='tw-w-40'>Total (IDR)</th>
              <th className='tw-w-[15%] tw-py-3'>Gambar</th>
            </tr>
          </thead>
          <tbody>
            {data?.products?.map((item) => {
              return (
                <tr key={item.sku} className='tw-border-b tw-border-gray-200'>
                  <td className='tw-align-middle'>{item.sku}</td>
                  <td className='tw-align-middle'>{item.name}</td>
                  <td className='tw-align-middle'>{item.qty}</td>
                  <td className='tw-align-middle'>
                    {'Rp ' + NumberWithCommas(String(item.normalPrice))}
                  </td>
                  <td className='tw-align-middle'>
                    {'Rp ' + NumberWithCommas(String(item.specialPrice))}
                  </td>
                  <td className='tw-align-middle'>
                    {(
                      (item?.display_discount_total_percentage || 0) * 100
                    ).toFixed(2) + '%'}
                  </td>
                  <td className='tw-align-middle'>
                    {'Rp ' +
                      NumberWithCommas(String(item.qty * item.specialPrice))}
                  </td>
                  <td className='tw-py-4 tw-align-middle tw-flex tw-items-center tw-justify-center'>
                    <img
                      src={item.image}
                      className='tw-rounded-md'
                      alt={item.name}
                      width='80'
                    />
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
      <div className='tw-w-full tw-py-4'>
        <div className='tw-my-8 tw-font-semibold'>Perincian Harga</div>
        <table className='tw-border-1 tw-w-full tw-table-auto tw-text-center'>
          <thead>
            <tr className='tw-border-1 tw-mb-4 tw-border-b tw-border-gray-200'>
              <th className='tw-w-[10%] tw-py-3'>SKU</th>
              <th className='tw-w-[25%]'>Nama Produk</th>
              <th className='tw-w-20'>Jumlah</th>
              <th className='tw-text-wrap tw-w-36 tw-px-4'>Harga Normal</th>
              <th className='tw-text-wrap tw-w-36 tw-px-4'>Harga Spesial</th>
              <th>Total Diskon</th>
              <th className='tw-text-right'>Total (IDR)</th>
            </tr>
          </thead>
          <tbody>
            {data?.products?.map((item) => {
              return (
                <tr key={item.sku} className='tw-border-b tw-border-gray-200'>
                  <td className='tw-py-3'>{item.sku}</td>
                  <td>{item.name}</td>
                  <td>{item.qty}</td>
                  <td>{'Rp ' + NumberWithCommas(String(item?.normalPrice))}</td>
                  <td>
                    {'Rp ' + NumberWithCommas(String(item?.specialPrice))}
                  </td>
                  <td>
                    {'Rp ' +
                      NumberWithCommas(
                        String(item?.display_discount_total_fix || 0),
                      )}
                  </td>
                  <td className='tw-text-right'>
                    {'Rp ' +
                      NumberWithCommas(String(item.specialPrice * item.qty))}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
      <div className='tw-break-after-page'>
        <div className='tw-flex tw-justify-end tw-pt-5 tw-text-sm'>
          <div className='tw-grid tw-w-1/2 tw-grid-cols-2 tw-gap-1 tw-pr-5 tw-text-right tw-font-semibold'>
            <div>Subtotal</div>
            <div>Rp {NumberWithCommas(String(data?.display_subtotal))}</div>
            <div>Biaya Pengiriman</div>
            <div>Rp 0</div>
            <div>Diskon Tambahan</div>
            <div>Rp 0</div>
            <div>Total Diskon</div>
            <div>
              Rp {NumberWithCommas(String(data?.display_discount_total))}
            </div>
          </div>
        </div>
        <div className='tw-mt-3 tw-flex tw-justify-end tw-text-sm'>
          <div className='tw-grid tw-w-1/2 tw-grid-cols-2 tw-gap-1 tw-rounded-md tw-border-2 tw-border-orange-50 tw-border-solid tw-bg-orange-100 tw-pr-5 tw-text-right tw-font-semibold tw-pb-3'>
            <div>Grand Total</div>
            <div>Rp {NumberWithCommas(String(data?.grand_total))}</div>
          </div>
        </div>
      </div>
      <div className='tw-my-5'>
        <TahuWordingContent
          title='t-n-c-wholesale-quotation'
          companyCodeCustom='O2B'
        />
        <p className='tw-mt-4'>Jakarta, {dayjs().format('DD MMMM YYYY')}</p>
        <p className='tw-mt-10'>Adam </p>
        <p>Sales Project</p>
      </div>
    </div>
  )
})

export default FormatQuotationAttachment
