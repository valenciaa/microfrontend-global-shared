import TWLoadingModal from '../../../tw-components/modals/TWLoadingModal'

export default function B2bLoading({ isLoading }) {
  if (isLoading) {
    return <TWLoadingModal />
  } else {
    return null
  }
}
