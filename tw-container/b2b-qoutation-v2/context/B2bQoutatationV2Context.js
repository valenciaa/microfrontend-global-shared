import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import { yupResolver } from '@hookform/resolvers/yup'
import { useFieldArray, useForm, useWatch } from 'react-hook-form'
import debounce from 'lodash/debounce'

import { b2bQuotationV2Schema } from '../utils/b2bQuotationV2Utils'
import {
  useCreateQuotationProductsV2,
  useCreateQuotationUserV2,
  useShareQuotationEmailV2,
} from '../../../services/api/mutations/qoutation'

import SuccessModal from '../components/SuccessModal'
import SnackBar from '../components/SnackBar'
import B2bLoading from '../components/B2bLoading'
import ConfirmationModal from '../components/ConfirmationModal'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import { calculateTotalPriceB2bQuotation } from '../utils/b2bPriceUtils'
import FormatQuotationAttachment from '../components/FormatQuotationAttachment'
import generatePDF, { Resolution } from 'react-to-pdf'
import setEmailDataWholesale from '../utils/setEmailDataWholesale'
import { isEmpty } from 'lodash'

export const B2bQoutatationV2Context = createContext({})
export const B2bQoutatationV2ActionContext = createContext({})

const B2bQoutatationV2Provider = ({ children }) => {
  const [isIdentitySaved, setIsIdentitySaved] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [file, setFile] = useState(null)
  const [infobarType, setinfobarType] = useState('')
  const [message, setMessage] = useState('')
  const [isSuccess, setIsSuccess] = useState(false)
  const [isConfirmation, setIsConfirmation] = useState(false)
  const [tempBody, setTempBody] = useState('null')
  const [emailData, setEmailData] = useState(null)
  const [generatingPdf, setGeneratingPdf] = useState(false)

  const refFormatQuotatitionAttachment = useRef(null)

  const { mutate: mutateQoutationUser, isLoading: isLoadingQuotationUserV2 } =
    useCreateQuotationUserV2()

  const {
    mutate: mutateQoutationProducts,
    isLoading: isLoadingQuotationProductsV2,
  } = useCreateQuotationProductsV2()

  const { mutate: shareQuotationeEmailV2, isLoading: isLoadingShareEmail } =
    useShareQuotationEmailV2({
      onSettled: () => {
        setGeneratingPdf(false)
        setIsSuccess(true)
        setTempBody(null)
        reset()
        setEmailData(null)
      },
    })

  const {
    register,
    watch,
    setValue,
    formState: { errors },
    handleSubmit,
    control,
    getValues,
    reset,
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(b2bQuotationV2Schema()),
    defaultValues: async () => {
      let defaultValue = {
        address: {
          address_name: 'b2b_quotation',
        },
        products: [],
      }
      const savedBody = sessionStorage.getItem('b2b_quotation')
      if (savedBody) {
        defaultValue = {
          ...defaultValue,
          ...JSON.parse(savedBody),
        }
        setIsIdentitySaved(true)
      }
      return defaultValue
    },
  })

  useEffect(() => {
    if (!isEmpty(emailData)) {
      handleShareQuotation()
    }
  }, [emailData])

  const { update } = useFieldArray({
    control,
    name: 'products',
  })

  const value = useMemo(() => {
    return {
      errors,
      control,
      isIdentitySaved,
      file,
    }
  })

  const actions = useMemo(() => {
    const addProduct = (
      sku,
      name,
      attributes,
      normalPrice,
      specialPrice,
      image,
      packagingUom,
      qty = 1,
      location,
    ) => {
      const products = getValues('products')

      if (products.length >= 150) {
        setSnackbar('error', 'Produk sudah mancapai maksimal')
        return
      }

      mixpanelTrack('Tambah Produk ruparupa bisnis', {
        'Location': location,
        'Item ID': sku,
        'Item Name': name,
        'Item Price': specialPrice,
      })

      setSnackbar('success', 'Produk berhasil ditambahkan')

      // ? CHECK IF PRODUCT ALREADY ON THE LIST
      const indexFound = products.findIndex((el) => el.sku === sku)
      if (indexFound >= 0) {
        const newProduct = {
          ...products[indexFound],
          qty: products[indexFound]?.qty + qty,
        }
        update(indexFound, newProduct)
        return
      }

      const body = {
        sku,
        name,
        attributes,
        normalPrice,
        specialPrice,
        qty,
        image,
        packagingUom,
      }
      if (products) {
        const newProducts = [...products, body]
        setValue('products', newProducts, { shouldValidate: true })
      } else {
        setValue('products', [body], { shouldValidate: true })
      }
    }

    const onSubmit = async (data) => {
      if (isIdentitySaved) {
        if (!data.isFileExist && !data.products.length) {
          setSnackbar('error', 'Pilih Produk atau Unggah Lampiran diperlukan')
          return
        }

        const formData = new FormData()
        if (file) {
          formData.append('uploaded-file', file)
        }
        formData.append('note', data.notes)
        formData.append('is_invoice', data.isFaktur)
        formData.append('is_installation', data.isInstallation)

        const products = data.products.map((el) => {
          return {
            sku: el.sku,
            name: el.name,
            qty: el.qty,
            packaging_uom: el.packagingUom,
            zone_id: 1, // Default to 1 for no zone
            image_url: el.image,
            normal_price: el.normalPrice,
            special_price: el.specialPrice,
          }
        })

        const customer = {
          name: data.name,
          email: data.email,
          phone: `0${data.phoneNumber}`,
          company_name: data?.companyName || '',
        }

        formData.append('items', JSON.stringify(products))
        formData.append('customer', JSON.stringify(customer))
        const body = {
          formData,
          quotationToken: data.quotationToken,
        }
        setTempBody(body)
        setIsConfirmation(true)
        submitMixpanel(data, file)
      } else {
        const body = {
          name: data.name,
          email: data.email,
          phone: `0${data.phoneNumber}`,
          type: data.userType,
          industry_sector_id: data?.industryId,
          company_name: data?.companyName,
          address: {
            name: data.address.first_name,
            phone: `0${data.address.phone}`,
            province_id: +data.address.province.province_id,
            city_id: +data.address.city.city_id,
            kecamatan_id: +data.address.kecamatan.kecamatan_id,
            kelurahan_id: +data.address.kelurahan.kelurahan_id,
            full_address: data.address.full_address,
          },
        }
        mutateQoutationUser(body, {
          onSuccess: (res) => {
            if (
              res.status === 201 &&
              res.data?.data?.wholesale_customer_token
            ) {
              mixpanelTrack('Submit Lead ruparupa bisnis', {
                'Tipe Pengguna': data.userType,
                'Nomor Handphone': `0${data.phoneNumber}`,
                'Nama': data.name,
                'Email': data.email,
                'Nama Perusahaan': data?.companyName,
                'Bidang Idustri': data?.industry,
              })
              const quotationToken = res.data.data.wholesale_customer_token
              setValue('quotationToken', quotationToken)
              setIsIdentitySaved(true)
              sessionStorage.setItem(
                'b2b_quotation',
                JSON.stringify({
                  ...data,
                  quotationToken,
                }),
              )
              setSnackbar(
                'info',
                'Mohon isi form RFQ untuk mengajukan penawaran',
              )
            }
          },
        })
      }
    }

    const onSubmitMutateProducts = async (body) => {
      mutateQoutationProducts(body, {
        onSuccess: async (res) => {
          setIsConfirmation(false)
          if (res?.data?.status === 201) {
            sessionStorage.removeItem('b2b_quotation')
            const data = getValues()
            const emailDataModified = setEmailDataWholesale(
              data,
              res?.data?.data,
            )
            setGeneratingPdf(true)
            setEmailData(emailDataModified)
          } else {
            setSnackbar('error', 'Something is wrong')
          }
        },
      })
    }

    const uploadFile = (event) => {
      const currentFile = event.target?.files?.[0]
      if (!currentFile) {
        setSnackbar('error', 'Upload file gagal')
        return
      }

      if (currentFile.size > 4000000) {
        setSnackbar('error', 'Ukuran maksimum file adalah 4 MB')
        return
      }

      setValue('isFileExist', true, { shouldValidate: true })
      setFile(event.target.files[0])
    }

    const setSnackbar = (type = '', message = '') => {
      setinfobarType(type)
      setMessage(message)
    }

    return {
      register,
      watch,
      setValue,
      handleSubmit,
      setIsLoading,
      onSubmit,
      addProduct,
      uploadFile,
      setSnackbar,
      onSubmitMutateProducts,
    }
  })

  const handleShareQuotation = async () => {
    let payloadShareQuotation = {}
    let base64Pdf = ''

    if (emailData?.products?.length) {
      base64Pdf = await handleCreatePdf()
    }

    payloadShareQuotation = {
      pdf: base64Pdf,
      cwt: emailData?.quotationToken,
      quotationNo: emailData?.quotation_no,
    }

    shareQuotationeEmailV2(payloadShareQuotation)
  }

  const handleCreatePdf = async () => {
    if (!refFormatQuotatitionAttachment.current) {
      return
    }

    const PDF = await generatePDF(refFormatQuotatitionAttachment, {
      method: 'build',
      resolution: Resolution.NORMAL,
      page: {
        margin: {
          top: 10,
          bottom: 10,
          left: 5,
          right: 5,
        },
      },
      overrides: {
        pdf: {
          compress: true,
        },
        canvas: {
          useCORS: true,
        },
      },
    })

    return PDF.output('datauristring')
      ?.replace(';filename=generated.pdf', '')
      ?.replace(/^data:application\/\w+;base64,/, '')
  }

  return (
    <B2bQoutatationV2ActionContext.Provider value={actions}>
      <B2bQoutatationV2Context.Provider value={value}>
        <SaveToSessionStorage
          control={control}
          isIdentitySaved={isIdentitySaved}
        />
        <SnackBar
          infobarType={infobarType}
          message={message}
          setSnackbar={actions.setSnackbar}
        />
        <SuccessModal isSuccess={isSuccess} />
        <B2bLoading
          isLoading={
            isLoading ||
            isLoadingQuotationUserV2 ||
            isLoadingQuotationProductsV2 ||
            isLoadingShareEmail ||
            generatingPdf
          }
        />
        <ConfirmationModal
          isConfirmation={isConfirmation}
          setIsConfirmation={setIsConfirmation}
          onSubmit={actions.onSubmitMutateProducts}
          tempBody={tempBody}
        />
        {children}
        <FormatQuotationAttachment
          ref={refFormatQuotatitionAttachment}
          data={emailData}
        />
      </B2bQoutatationV2Context.Provider>
    </B2bQoutatationV2ActionContext.Provider>
  )
}

function SaveToSessionStorage({ control, isIdentitySaved }) {
  const watchedProperties = useWatch({
    control,
    name: ['products', 'notes', 'isFaktur', 'isInstallation', 'paymentMethod'],
  })

  useEffect(() => {
    if (isIdentitySaved) {
      const [products, notes, isFaktur, isInstallation, paymentMethod] =
        watchedProperties
      const savedBody = sessionStorage.getItem('b2b_quotation')
      if (!savedBody) {
        return
      }
      const savedBodyParsed = JSON.parse(savedBody)
      save(
        savedBodyParsed,
        products,
        notes,
        isFaktur,
        isInstallation,
        paymentMethod,
      )
    }
  }, [watchedProperties])

  const save = useCallback(
    debounce(
      (
        initialBody,
        products,
        notes,
        isFaktur,
        isInstallation,
        paymentMethod,
      ) => {
        const newBody = {
          ...initialBody,
          products,
          notes,
          isFaktur,
          isInstallation,
          paymentMethod,
        }
        sessionStorage.setItem('b2b_quotation', JSON.stringify(newBody))
      },
      500,
    ),
    [],
  )
}

function submitMixpanel(data, file) {
  const totalPrice = calculateTotalPriceB2bQuotation(data.products)

  const detail = []
  if (data.products && data.products.length) {
    detail.push('Pilih Produk')
  }
  if (file) {
    detail.push('Unggah Lampiran')
  }
  if (data.notes) {
    detail.push('Catatan')
  }

  const extraService = []
  if (data.isFaktur) {
    extraService.push('Faktur Pajak')
  }
  if (data.isInstallation) {
    extraService.push('Instalasi')
  }

  mixpanelTrack('Submit Quotation ruparupa bisnis', {
    'Detail Permintaan': detail.length ? detail.join(',') : undefined,
    'Layanan Tambahan': extraService.length
      ? extraService.join(',')
      : undefined,
    'Total': totalPrice,
  })
}

export function useB2bQoutatationV2Context() {
  return useContext(B2bQoutatationV2Context)
}

export function useB2bQoutatationV2ActionContext() {
  return useContext(B2bQoutatationV2ActionContext)
}

export default B2bQoutatationV2Provider
