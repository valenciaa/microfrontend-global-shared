import config from '../../../../../config'

export const EXCLUDED_LABEL = ['bundling hemat', 'tebus murah']
export const RUPARUPA_BISNIS_WA_LINK = config.ruparupaBisnisWaLink
export const RUPARUPA_BISNIS_WA_DISPLAY = config.ruparupaBisnisWaDisplay
export const RUPARUPA_BISNIS_EMAIL_ORDER = config.ruparupaBisnisEmailOrder
export const RUPARUPA_BISNIS_EMAIL_DISPLAY = config.ruparupaBisnisEmailDisplay
export const RUPARUPA_BISNIS_EMAIL_LINK = `mailto:${RUPARUPA_BISNIS_EMAIL_DISPLAY}?subject=Pertanyaan untuk ruparupa Bisnis`
