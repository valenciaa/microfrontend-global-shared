const setEmailDataWholesale = (data, response) => {
  data?.products?.forEach((item) => {
    const foundItem = response?.items?.find(({ sku }) => sku === item?.sku)
    if (foundItem) {
      Object.assign(item, foundItem)
    }
  })

  return {
    ...data,
    ...response,
  }
}

export default setEmailDataWholesale
