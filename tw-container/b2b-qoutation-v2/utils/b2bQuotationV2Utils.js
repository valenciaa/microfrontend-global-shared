import { object, string, mixed, number, array, boolean } from 'yup'
import { AddressAddAndEditValidation } from '../../../utils/validation/address'

export const b2bQuotationV2Schema = () => {
  return object({
    userType: mixed()
      .oneOf(['corporate', 'personal'], 'Tipe Pengguna di perlukan')
      .required('Tipe Pengguna di perlukan'),
    name: string().required('Nama Lengkap di perlukan'),
    phoneNumber: string()
      .matches(/^\d+$/, 'Nomor telepon wajib angka')
      .matches(/^8/, 'Nomor telepon tidak valid')
      .min(9, 'Nomor telepon minimal 9 digit')
      .max(12, 'Nomor telepon maksimal 12 digit')
      .required('Nomor Telepon di perlukan'),
    email: string().email('Email tidak valid').required('Email di perlukan'),
    companyName: string().when('userType', {
      is: 'corporate',
      then: string().required('Nama Perusahaan di perlukan'),
    }),
    industryId: number().when('userType', {
      is: 'corporate',
      then: number().required('Bidang Industri di perlukan'),
    }),
    address: AddressAddAndEditValidation.required('Alamat di perlukan'),
    quotationToken: string(),
    isFileExist: boolean().default(false),
    products: array()
      .of(b2bQuotationV2ProductSchema)
      .max(150, 'Maksimal Jumlah Produk 150'),
    notes: string().max(255, 'Catatan maksimal 255 karakter'),
    isFaktur: boolean().default(false),
    isInstallation: boolean().default(false),
  }).required()
}

const b2bQuotationV2ProductSchema = object({
  sku: string().required(),
  name: string().required(),
  attributes: string(),
  normalPrice: number().required(),
  specialPrice: number(),
  qty: number().required(),
  image: string().required(),
  packagingUom: string().required(),
}).required('Produk di perlukan')
