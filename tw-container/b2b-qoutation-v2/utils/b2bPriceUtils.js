export const calculateTotalPriceB2bQuotation = (products = []) => {
  let totalPrice = products.reduce((total, product) => {
    if (product?.specialPrice) {
      return total + product?.specialPrice * product?.qty
    } else {
      return total + product?.normalPrice * product?.qty
    }
  }, 0)
  return `Rp${totalPrice.toLocaleString('id-ID')}`
}

export const calculateTotalQty = (products = []) => {
  let totalQty = products.reduce((total, product) => {
    return total + product?.qty
  }, 0)
  return totalQty
}
