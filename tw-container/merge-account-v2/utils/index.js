import isEmpty from 'lodash/isEmpty'

export const isResponseValid = (legacyMembers, userInfo) => {
  if (isEmpty(userInfo)) {
    return false
  }

  if (isEmpty(legacyMembers) && legacyMembers.length) {
    return false
  }

  return !legacyMembers.some((el) => el.status === 'unknown')
}
