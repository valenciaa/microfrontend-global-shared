import { useContext, createContext, useMemo, useState } from 'react'
import localforage from 'localforage'
import isEmpty from 'lodash/isEmpty'
import { useGetDuplicateAccounts } from '../../../services/api/queries/auth'
import config from '../../../../../config'
import { isResponseValid } from '../utils'
import { useKlkMergeAccountV2 } from '../../../services/api/mutations/auth'
import { mixpanelKlkTrack } from '../../../utils/MixpanelWrapper'

export const MergeAccountV2Context = createContext({})
export const MergeAccountV2ActionContext = createContext({})

export const MergeAccountV2Provider = ({
  children,
  isSplitAccounts = false,
  from = '',
}) => {
  const [accounts, setAccounts] = useState([])
  const [userInfo, setUserInfo] = useState(null)
  const [completeAction, setCompleteAction] = useState('') // ? skip || complete || ''
  const [mergeError, setMergeError] = useState('')

  const [unknownAccounts, setUnknownAccounts] = useState([])
  const [historyAccounts, setHistoryAccounts] = useState([])

  const { isLoading: isLoadingGetAccounts, refetch: refetchGetDuplicates } =
    useGetDuplicateAccounts(
      { is_split: isSplitAccounts ? 10 : 0 },
      {
        onSuccess: (response) => {
          if (response && !isEmpty(response)) {
            const {
              legacy_member: legacyMembers,
              userInfo,
              unknown_member: unknownMembers,
              known_member: knownMembers,
            } = response

            setUserInfo(userInfo)

            if (isSplitAccounts) {
              setUnknownAccounts(
                unknownMembers.map((el) => {
                  return {
                    ...el,
                    isActive: false,
                  }
                }),
              )
              setHistoryAccounts(
                knownMembers.map((el) => {
                  return {
                    ...el,
                    isActive: false,
                  }
                }),
              )
            } else {
              if (isResponseValid(legacyMembers, userInfo)) {
                window.location.href = config?.baseURL
                return
              }
              setAccounts(
                legacyMembers.map((el) => {
                  return {
                    ...el,
                    isActive: false,
                  }
                }),
              )
            }
          }
        },
      },
    )

  const { mutate, isLoading: isLoadingMergeAccounts } = useKlkMergeAccountV2({
    onSuccess: (response) => {
      if (!response.data.error && response.status === 200) {
        mixpanelKlkTrack('Success Gabungkan Rewards', '', {
          type: 'Auto Detect',
          // bu: response?.data?.data?.company_code || '',
        })
        if (from === 'auth') {
          handleLeavePage('complete')
        }
      } else {
        setMergeError(response.data?.message || 'Terjadi Kesalahan')
      }
    },
  })

  const value = useMemo(() => {
    return {
      accounts,
      userInfo,
      isLoadingGetAccounts,
      isLoadingMergeAccounts,
      completeAction,
      unknownAccounts,
      historyAccounts,
      mergeError,
    }
  })

  const actions = useMemo(() => {
    const handleChange = (memberToken) => {
      setAccounts((value) => {
        return value.map((el) => {
          if (el.memberToken === memberToken) {
            el.isActive = !el.isActive
          }
          return el
        })
      })
    }

    const handleSubmit = (accounts) => {
      const body = {
        mergedMembers: accounts
          .filter((el) => el.isActive)
          .map((el) => {
            return {
              memberToken: el.memberToken,
            }
          }),
      }
      mutate(body)
    }

    return {
      handleChange,
      handleSubmit,
      setCompleteAction,
      refetchGetDuplicates,
      mergeAccountsMutate: mutate,
      setMergeError,
      handleLeavePage,
    }
  }, [setCompleteAction, refetchGetDuplicates, mutate, setMergeError])

  async function handleLeavePage(action = '') {
    const lastVerifyStatus =
      window?.sessionStorage?.getItem('last_verify_status') || ''
    const isNeedForceMerging =
      (await localforage.getItem('isNeedForceMerging')) || false

    // this code below only runs after customer want to login / after they activate their account
    // also they haven't verified email / phone
    // also they don't have legacy members or they have legacy members that haven't been merged yet
    if (!isEmpty(lastVerifyStatus)) {
      setCompleteAction(action)
      return
    }

    // this code below only runs after customer want to login
    // also they already verified email & phone
    // also they have legacy members that haven't been merged yet
    if (isEmpty(lastVerifyStatus)) {
      let params = ''
      if (isNeedForceMerging) {
        params += 'welcome=skip'
      } else {
        params += 'welcome=upgrade'
      }
      window.location.href = config.baseURL + `?${params}`
    }
  }

  return (
    <MergeAccountV2Context.Provider value={value}>
      <MergeAccountV2ActionContext.Provider value={actions}>
        {children}
      </MergeAccountV2ActionContext.Provider>
    </MergeAccountV2Context.Provider>
  )
}

export function useMergeAccountV2Context() {
  return useContext(MergeAccountV2Context)
}

export function useMergeAccountV2ActionContext() {
  return useContext(MergeAccountV2ActionContext)
}
