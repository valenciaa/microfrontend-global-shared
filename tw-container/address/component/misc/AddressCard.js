import clsx from 'clsx'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'
import { useAddressContext } from '../../../../context/AddressContext'
import {
  useDeleteAccountAddress,
  useMakePrimaryAccountAddress,
} from '../../../../services/api/mutations/unifyapps'
import TWButton from '../../../../tw-components/buttons/TWButton'
import TWRadio from '../../../../tw-components/inputs/TWRadio'
import TWBaseModal from '../../../../tw-components/modals/TWBaseModal'
import TWLoadingModal from '../../../../tw-components/modals/TWLoadingModal'
import { mixpanelTrack } from '../../../../utils/MixpanelWrapper'
import {
  handleAddressReceiverNameTemplate,
  handleAddressTemplate,
  handleCheckShippingLocation,
} from '../../../../utils/UtilAddress'

const buColorMap = {
  bg: {
    ruparupa: clsx('tw-bg-ruparupa'),
    ace: clsx('tw-bg-ace'),
    informa: clsx('tw-bg-informa'),
    toyskingdomonline: clsx('tw-bg-toyskingdomonline'),
  },
  coordinate: {
    ruparupa: clsx('tw-text-ruparupa tw-bg-orange-10'),
    ace: clsx('tw-text-ace tw-bg-red-10'),
    informa: clsx('tw-text-informa tw-bg-blue-10'),
    toyskingdomonline: clsx('tw-text-toyskingdomonline tw-bg-orange-10'),
  },
}

export default function AddressCard({
  addressData,
  isChosen,
  setChosenAddress,
  pageFrom = 'installation',
}) {
  const {
    address_id,
    first_name,
    last_name,
    phone,
    address_name,
    is_default,
    geolocation,
  } = addressData

  const companyName = config.companyNameCSS || 'ruparupa'
  const isMobile = config.environment === 'mobile'

  const isAddressHasGeo = !!geolocation
  const isAddressPrimary = !!(is_default === '1')

  const {
    state: addressContext,
    handleChangeStateObject: changeAddressContext,
  } = useAddressContext()

  const { mutate: deleteAddress, isLoading: isLoadingDelete } =
    useDeleteAccountAddress({
      onSuccess: (res) => {
        changeAddressContext({ showDeleteConfirmation: false })

        if (!res?.data?.error) {
          changeAddressContext({
            isShowInfoBox: true,
            infoBoxType: 'success',
            isRefetching: true,
            infoBoxMessage: 'Berhasil untuk menghapus alamat',
          })

          return
        }

        changeAddressContext({
          isShowInfoBox: true,
          infoBoxType: 'error',
          infoBoxMessage: 'Terjadi Kesalahan Mohon Menunggu Sesaat Lagi',
        })
      },
    })

  const { mutate: makePrimaryAddress, isLoading: isLoadingMakePrimary } =
    useMakePrimaryAccountAddress({
      onSuccess: (res) => {
        changeAddressContext({ showPrimaryConfirmation: false })

        if (!res?.data?.error) {
          changeAddressContext({
            isShowInfoBox: true,
            infoBoxType: 'success',
            isRefetching: true,
            infoBoxMessage: 'Berhasil untuk menjadikan alamat utama',
          })

          return
        }

        changeAddressContext({
          isShowInfoBox: true,
          infoBoxType: 'error',
          infoBoxMessage: 'Terjadi Kesalahan Mohon Menunggu Sesaat Lagi',
        })
      },
    })

  const handleDeleteAddress = async () => {
    await handleCheckShippingLocation(
      addressContext?.selectedAddressId,
      addressContext?.address,
    )

    deleteAddress({ address_id: addressContext.selectedAddressId })

    changeAddressContext({
      showDeleteConfirmation: false,
      selectedAddressId: '',
    })
  }

  const handleMakePrimaryAddress = (geolocation, pageFrom) => {
    if (geolocation && pageFrom === 'installation') {
      makePrimaryAddress(addressContext.selectedAddressId)

      changeAddressContext({
        showPrimaryConfirmation: false,
        selectedAddressId: '',
      })

      return
    }

    changeAddressContext({
      showPrimaryConfirmation: false,
      selectedAddressId: '',
      isShowInfoBox: true,
      infoBoxType: 'error',
      infoBoxMessage:
        'Perakitan hanya dapat dilakukan untuk alamat yang memiliki koordinat, mohon untuk menambahkan koordinat pada alamat yang dipilih',
    })
  }

  return (
    <>
      <div className='tw-flex tw-rounded-lg tw-shadow-address-list'>
        <div className='tw-flex tw-flex-col tw-p-4'>
          <div className='tw-flex tw-gap-1 tw-pb-2'>
            <div className='tw-flex tw-flex-col tw-gap-2 tw-flex-grow'>
              <div className='tw-flex tw-items-center tw-gap-1'>
                <p className='tw-font-bold tw-truncate'>{address_name}</p>

                {isAddressPrimary && (
                  <div
                    className={clsx(
                      'tw-flex tw-rounded-3xl tw-items-center tw-py-0.5 tw-px-1 tw-text-xxs tw-font-semibold tw-text-white',
                      buColorMap.bg[companyName],
                    )}
                  >
                    UTAMA
                  </div>
                )}
              </div>

              <div className='tw-flex tw-items-center tw-gap-0.5'>
                <p className='tw-text-sm tw-truncate tw-flex-shrink'>
                  {handleAddressReceiverNameTemplate(first_name, last_name)}
                </p>

                <p className='tw-text-sm'>({phone})</p>
              </div>

              <div className='tw-flex tw-items-center'>
                <p className='tw-text-sm tw-text-grey-40 tw-line-clamp-2 tw-capitalize tw-break-all'>
                  {handleAddressTemplate(addressData)}
                </p>
              </div>
            </div>

            <div className='tw-flex tw-items-center'>
              <TWRadio
                isChecked={isChosen}
                value={addressData}
                handleOnChange={() => setChosenAddress(addressData)}
                size='medium'
              />
            </div>
          </div>

          <div
            className={clsx(
              'tw-flex tw-gap-1 tw-items-center tw-justify-center tw-text-xxs tw-font-semibold tw-rounded-2xl tw-px-2 tw-py-1 tw-max-w-max',
              isAddressHasGeo
                ? buColorMap.coordinate[companyName]
                : 'tw-bg-grey-10 tw-border tw-border-solid tw-border-grey-20',
            )}
          >
            <LazyLoadImage
              alt={`${isAddressHasGeo ? `pin-${companyName}` : 'error'}-icon`}
              className='tw-flex'
              height={16}
              placeholder={<Skeleton height={16} width={16} />}
              src={`${config.assetsURL}icon/address/${
                isAddressHasGeo ? `pin-${companyName}` : 'error'
              }.svg`}
              width={16}
            />

            <p>
              {isAddressHasGeo
                ? 'Koordinat Sudah Diatur'
                : 'Koordinat Belum Diatur'}
            </p>
          </div>

          <div className='tw-flex tw-items-center tw-pt-2 tw-gap-2'>
            <TWButton
              additionalClass={clsx(
                'tw-rounded-md',
                isAddressPrimary && 'w-full',
              )}
              handleOnClick={() => {
                mixpanelTrack('View Page Ubah Alamat')

                changeAddressContext({
                  showEdit: true,
                  selectedAddressId: address_id,
                  selectedEdit: addressData,
                })
              }}
              size='small'
              type='secondary-border_ghost'
            >
              Ubah
            </TWButton>

            {!isAddressPrimary && (
              <>
                <TWButton
                  additionalClass='tw-flex tw-rounded-md'
                  handleOnClick={() =>
                    changeAddressContext({
                      showDeleteConfirmation: true,
                      selectedAddressId: address_id,
                    })
                  }
                  size='small'
                  type='secondary-border_ghost'
                >
                  Hapus
                </TWButton>

                <TWButton
                  additionalClass='tw-rounded-md w-full'
                  handleOnClick={() =>
                    changeAddressContext({
                      showPrimaryConfirmation: true,
                      selectedAddressId: address_id,
                    })
                  }
                  size='small'
                  type='primary-border'
                >
                  Jadikan Alamat Utama
                </TWButton>
              </>
            )}
          </div>
        </div>
      </div>

      {isLoadingDelete || (isLoadingMakePrimary && <TWLoadingModal />)}

      {addressContext?.showDeleteConfirmation && (
        <TWBaseModal
          animationType='fade'
          customPadding={clsx(
            'tw-px-4',
            isMobile ? 'tw-py-6 tw-w-[343px]' : 'tw-py-8 tw-w-[416px]',
          )}
          isShow={addressContext?.showDeleteConfirmation}
          modalType='middle-small'
        >
          <div className='tw-flex tw-flex-col'>
            <p
              className={clsx(
                'tw-flex tw-flex-grow tw-font-bold',
                isMobile ? 'tw-pb-2' : 'tw-text-xl tw-pb-3',
              )}
            >
              Hapus Alamat?
            </p>

            <p className={clsx(isMobile && 'tw-text-sm tw-text-grey-50')}>
              Apakah kamu yakin ingin menghapus alamat ini dari daftar alamat?
            </p>

            <div
              className={clsx(
                'tw-flex',
                isMobile ? 'tw-pt-3 tw-gap-3' : 'tw-pt-4 tw-gap-4',
              )}
            >
              <TWButton
                additionalClass='tw-rounded-md tw-w-full'
                handleOnClick={() =>
                  changeAddressContext({
                    showDeleteConfirmation: false,
                    selectedAddressId: '',
                  })
                }
                size={isMobile ? 'medium' : 'big'}
                type='primary-border'
              >
                Batal
              </TWButton>

              <TWButton
                additionalClass='tw-rounded-md tw-w-full'
                handleOnClick={() => handleDeleteAddress()}
                size={isMobile ? 'medium' : 'big'}
                type='primary'
              >
                Ya, Hapus
              </TWButton>
            </div>
          </div>
        </TWBaseModal>
      )}

      {addressContext?.showPrimaryConfirmation && (
        <TWBaseModal
          animationType='fade'
          customPadding={clsx(
            'tw-px-4',
            isMobile ? 'tw-py-6 tw-w-[343px]' : 'tw-py-8 tw-w-[416px]',
          )}
          isShow={addressContext?.showPrimaryConfirmation}
          modalType='middle-small'
        >
          <div className='tw-flex tw-flex-col'>
            <p
              className={clsx(
                'tw-flex tw-flex-grow tw-font-bold',
                isMobile ? 'tw-pb-2' : 'tw-text-xl tw-pb-3',
              )}
            >
              Jadikan Alamat Utama?
            </p>

            <p className={clsx(isMobile && 'tw-text-sm tw-text-grey-50')}>
              Apakah kamu yakin ingin mengubah alamat ini sebagai alamat utama?
            </p>

            <div
              className={clsx(
                'tw-flex',
                isMobile ? 'tw-pt-3 tw-gap-3' : 'tw-pt-4 tw-gap-4',
              )}
            >
              <TWButton
                additionalClass='tw-rounded-md tw-w-full'
                handleOnClick={() =>
                  changeAddressContext({
                    showPrimaryConfirmation: false,
                    selectedAddressId: '',
                  })
                }
                size={isMobile ? 'medium' : 'big'}
                type='primary-border'
              >
                Batal
              </TWButton>

              <TWButton
                additionalClass='tw-rounded-md tw-w-full'
                handleOnClick={() =>
                  handleMakePrimaryAddress(geolocation, pageFrom)
                }
                size={isMobile ? 'medium' : 'big'}
                type='primary'
              >
                Ya, Ubah
              </TWButton>
            </div>
          </div>
        </TWBaseModal>
      )}
    </>
  )
}
