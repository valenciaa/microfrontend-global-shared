import clsx from 'clsx'
import { useState } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'

const buColorMap = {
  ruparupa: clsx('tw-border-ruparupa'),
  ace: clsx('tw-border-ace'),
  informa: clsx('tw-border-informa'),
  toyskingdomonline: clsx('tw-border-toyskingdomonline'),
}

export default function AddressPreviewSelectionMap({ onClick, longLat }) {
  const companyName = config.companyNameCSS || 'ruparupa'
  const isMobile = config.environment === 'mobile'

  const mapURL = `https://maps.google.com/maps?q=${longLat}&output=embed`

  const [isShowOverlay, setIsShowOverlay] = useState(false)

  return (
    <div
      className='tw-relative tw-flex tw-flex-grow'
      onClick={() => {
        if (!isMobile) {
          return
        }

        onClick?.()
      }}
      {...(!isMobile ? { onMouseEnter: () => setIsShowOverlay(true) } : {})}
      {...(!isMobile ? { onMouseLeave: () => setIsShowOverlay(false) } : {})}
      {...(!isMobile ? { onMouseOver: () => setIsShowOverlay(true) } : {})}
    >
      {isMobile && (
        <div
          className={clsx(
            'tw-absolute tw-top-2 tw-right-2 tw-bg-white tw-border-2 tw-border-solid tw-p-2 tw-rounded-lg',
            buColorMap[companyName],
          )}
        >
          <LazyLoadImage
            alt={`address-edit-icon-${companyName}`}
            height={14}
            placeholder={<Skeleton height={14} width={14} />}
            src={`${config.assetsURL}icon/address/address-map-edit-icon-${companyName}.svg`}
            width={14}
          />
        </div>
      )}

      {!isMobile && (
        <div
          className={`tw-absolute tw-top-0 tw-left-0 tw-w-full tw-h-full tw-bg-black/40 tw-rounded-lg tw-transition-opacity tw-duration-500 ${
            isShowOverlay ? 'tw-opacity-100' : 'tw-opacity-0'
          }`}
        />
      )}

      <iframe
        className='tw-rounded-lg tw-pointer-events-none'
        height={isMobile ? 150 : 180}
        src={mapURL}
        width='100%'
      />

      {!isMobile && (
        <div
          className={`tw-flex tw-items-center tw-justify-center tw-gap-2 tw-absolute tw-top-1/2 tw-left-1/2 -tw-translate-x-1/2 -tw-translate-y-1/2 tw-text-xs tw-bg-black tw-text-white tw-font-bold tw-w-auto tw-px-4 tw-py-1.5 tw-rounded-xl tw-cursor-pointer tw-transition-opacity tw-duration-500 ${
            isShowOverlay ? 'tw-opacity-100' : 'tw-opacity-0'
          }`}
          onClick={() => onClick?.()}
        >
          Ubah Koordinat
        </div>
      )}
    </div>
  )
}
