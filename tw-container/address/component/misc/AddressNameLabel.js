import clsx from 'clsx'
import config from '../../../../../../config'
export default function AddressNameLabel({ addressName, handleAddressLabel }) {
  const isDesktop = config.environment === 'desktop'
  const addressNameLabel = ['Rumah', 'Apartemen', 'Kantor', 'Kost']

  return (
    <div
      className={clsx(
        'tw-flex tw-gap-2 tw-py-1.5',
        isDesktop && 'tw-cursor-pointer',
      )}
    >
      {addressNameLabel.map((label, index) => {
        const active =
          label?.toLowerCase() === addressName?.toLowerCase()?.trim()

        return (
          <div
            key={index}
            className={clsx(
              'tw-flex tw-py-1 tw-px-3 tw-rounded-full tw-border tw-border-solid tw-text-xs',
              active
                ? 'tw-bg-orange-10 tw-border-ruparupa'
                : 'tw-bg-white tw-border-grey-20',
            )}
            onClick={() => handleAddressLabel(label)}
          >
            {label}
          </div>
        )
      })}
    </div>
  )
}
