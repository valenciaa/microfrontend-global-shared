import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'
import { useCallback, useEffect, useRef, useState } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Skeleton from 'react-loading-skeleton'
import { v4 as uuidv4 } from 'uuid'
import config from '../../../../../../config'
import {
  useGoogleMapsPlacesAutocomplete,
  useGoogleMapsPlacesDetail,
} from '../../../../services/api/mutations/global'
import TWSearchInput from '../../../../tw-components/inputs/TWSearchInput'

export default function AddressAutocomplete({ additionalClass, onClick }) {
  const [showSuggestion, setShowSuggestion] = useState(false)
  const [searchKey, setSearchKey] = useState('')
  const [isDelay, setIsDelay] = useState(false)
  const [sessiontoken, setSessiontoken] = useState(uuidv4())
  const [isGenerateNewSessionToken, setIsGenerateNewSessionToken] =
    useState(false)

  const lastSearchKey = useRef('')

  // Get Autocomplete suggestions
  const {
    mutate: postAutocompleteSuggestions,
    data: autocompleteSuggestionsData,
    isLoading: isFetchingSuggestions,
  } = useGoogleMapsPlacesAutocomplete()

  // Get Place detail
  const { mutate: getPlacesDetail, data: placesDetailData } =
    useGoogleMapsPlacesDetail({
      onSettled: () => {
        setIsGenerateNewSessionToken(true)
      },
    })

  const handleDebounce = useCallback(
    debounce((input) => {
      setIsDelay(false)

      if (lastSearchKey.current === input) {
        postAutocompleteSuggestions({
          input,
          sessiontoken,
        })
      }
    }, 1500),
    [sessiontoken],
  )

  const handleSelectAutocompletePlace = (placeId) => {
    getPlacesDetail({ id: placeId, sessiontoken })
  }

  useEffect(() => {
    lastSearchKey.current = searchKey

    if (searchKey?.length > 1) {
      setIsDelay(true)

      handleDebounce(searchKey)

      return
    }

    if (showSuggestion) {
      setShowSuggestion(false)
    }
  }, [searchKey])

  useEffect(() => {
    if (isEmpty(autocompleteSuggestionsData?.data?.predictions)) {
      setShowSuggestion(false)

      return
    }

    if (!showSuggestion) {
      setShowSuggestion(true)
    }
  }, [autocompleteSuggestionsData?.data?.predictions])

  useEffect(() => {
    const place = placesDetailData?.data?.result

    if (!isEmpty(place)) {
      onClick?.(place)

      setShowSuggestion(false)
      setSearchKey('')
    }
  }, [placesDetailData?.data])

  useEffect(() => {
    if (isGenerateNewSessionToken) {
      const token = uuidv4()

      setSessiontoken(token)

      setIsGenerateNewSessionToken(false)
    }
  }, [isGenerateNewSessionToken])

  return (
    <div className={`${additionalClass}`}>
      <TWSearchInput
        placeholder='Cari Alamat Di Sini'
        setValue={setSearchKey}
        value={searchKey}
      />

      <div className='tw-relative'>
        {isFetchingSuggestions || isDelay ? (
          <div className='tw-absolute tw-w-full tw-z-50 tw-rounded-lg tw-px-4 tw-pb-2 tw-bg-white'>
            <Skeleton className='my-1' count={3} height={39} />
          </div>
        ) : showSuggestion &&
          !isEmpty(autocompleteSuggestionsData?.data?.predictions) ? (
          <div className='tw-absolute tw-w-full tw-z-50 tw-rounded-lg tw-px-4 tw-bg-white tw-shadow-bottom-menu'>
            {autocompleteSuggestionsData?.data?.predictions.map(
              (suggestion, index) => (
                <div
                  className='tw-text-xs tw-text-grey-40 tw-cursor-pointer'
                  key={index}
                  onClick={() =>
                    handleSelectAutocompletePlace(suggestion?.place_id)
                  }
                >
                  <div className='tw-flex tw-border-b tw-border-grey-20 tw-border-solid tw-gap-2'>
                    <LazyLoadImage
                      alt={`address-location-prediction-icon-${index}`}
                      height={16}
                      placeholder={<Skeleton height={16} width={16} />}
                      src={`${config.assetsURL}icon/address/address-location-icon.svg`}
                      width={16}
                    />

                    <p className='tw-my-3 tw-cursor-pointer tw-line-clamp-2'>
                      <span className='tw-text-sm tw-font-bold'>
                        {suggestion?.structured_formatting?.main_text}
                      </span>{' '}
                      {suggestion?.structured_formatting?.secondary_text}
                    </p>
                  </div>
                </div>
              ),
            )}
          </div>
        ) : null}
      </div>
    </div>
  )
}
