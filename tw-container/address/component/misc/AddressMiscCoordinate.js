import clsx from 'clsx'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'
import { useAddressContext } from '../../../../context/AddressContext'
import AddressPreviewSelectionMap from './AddressPreviewSelectionMap'

const buColorMap = {
  ruparupa: clsx('tw-border-ruparupa'),
  ace: clsx('tw-border-ace'),
  informa: clsx('tw-border-informa'),
  toyskingdomonline: clsx('tw-border-toyskingdomonline'),
}

export default function AddressMiscCoordinate({
  watchFunction,
  withGeo,
  selectedGeo,
}) {
  const { handleChangeStateObject: changeAddressContext } = useAddressContext()

  const companyName = config.companyNameCSS || 'ruparupa'

  const isGeo = !!(selectedGeo || withGeo)
  const isKecamatanSelected = !!watchFunction('kecamatan.kecamatan_name')
  const isKelurahanSelected = !!watchFunction('kelurahan.kelurahan_name')

  return (
    <div className='tw-flex tw-flex-col tw-gap-1'>
      <p className='tw-text-xs'>Koordinat Lokasi (Opsional)</p>

      <div
        className={clsx(
          'tw-flex tw-items-center tw-justify-center tw-bg-grey-10 tw-border-2 tw-border-dashed tw-rounded-lg',
          isGeo ? buColorMap[companyName] : 'tw-border-grey-30 tw-p-5',
        )}
      >
        {isGeo ? (
          <AddressPreviewSelectionMap
            longLat={watchFunction('geolocation')}
            onClick={(e) => {
              e?.preventDefault?.()

              if (!isKecamatanSelected || !isKelurahanSelected) {
                return
              }

              changeAddressContext({ showMap: true })
            }}
          />
        ) : (
          <button
            className={clsx(
              'tw-flex tw-items-center tw-justify-center tw-bg-white tw-font-semibold tw-rounded tw-text-xxs tw-py-1 tw-px-4 tw-border tw-border-black',
              !isKelurahanSelected ? 'tw-opacity-50 tw-cursor-not-allowed' : '',
            )}
            onClick={(e) => {
              e?.preventDefault?.()

              if (!isKecamatanSelected || !isKelurahanSelected) {
                return
              }

              changeAddressContext({ showMap: true })
            }}
          >
            <LazyLoadImage
              alt='pin-default'
              height={16}
              placeholder={<Skeleton height={16} width={16} />}
              src={`${config.assetsURL}icon/address/pin-default.svg`}
              width={16}
            />

            <span>Pilih Koordinat</span>
          </button>
        )}
      </div>
    </div>
  )
}
