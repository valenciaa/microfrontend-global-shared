import clsx from 'clsx'
import config from '../../../../../../config'
import WarningIcon from '../../../../../../public/static/icon/warning-icon.svg'
import { useAddressContext } from '../../../../context/AddressContext'
import TWButton from '../../../../tw-components/buttons/TWButton'
import TWBaseModal from '../../../../tw-components/modals/TWBaseModal'
import TWInfobox from '../../../../tw-components/TWInfobox'

export default function AddressFullAddressConfirmation({ onClick }) {
  const {
    state: addressContext,
    handleChangeStateObject: changeAddressContext,
  } = useAddressContext()

  const isMobile = config.environment === 'mobile'

  return (
    <TWBaseModal
      animationType='fade'
      customPadding={clsx(
        'tw-px-4',
        isMobile ? 'tw-py-6 tw-w-[343px]' : 'tw-py-8 tw-w-[416px]',
      )}
      isShow={addressContext?.showConfirmationFullAddress}
      modalType='middle-small'
    >
      <div className='tw-flex tw-flex-col'>
        <p
          className={clsx(
            'tw-flex tw-flex-grow tw-font-bold tw-pb-2',
            !isMobile && 'tw-text-xl',
          )}
        >
          Alamat Pengiriman Belum Lengkap
        </p>

        <p
          className={clsx(isMobile && 'tw-text-sm tw-text-grey-50')}
          dangerouslySetInnerHTML={{
            __html: addressContext?.fullAddressErrorMessage,
          }}
        />

        <TWInfobox className='tw-pt-3 tw-mt-2' theme='warning'>
          <WarningIcon
            className='tw-grow-0 tw-shrink-0 tw-basis-4'
            width='16px'
            height='16px'
          />

          <span
            className={clsx('tw-ml-2', isMobile ? 'tw-text-xs' : 'tw-text-sm')}
          >
            Pesanan berpotensi gagal kirim jika alamat yang kamu masukkan tidak
            lengkap
          </span>
        </TWInfobox>

        <div
          className={clsx(
            'tw-flex',
            isMobile ? 'tw-pt-3 tw-gap-3' : 'tw-pt-4 tw-gap-4',
          )}
        >
          <TWButton
            additionalClass='tw-rounded-md tw-w-full'
            handleOnClick={() =>
              changeAddressContext({
                showConfirmationFullAddress: false,
                fullAddressErrorMessage: '',
              })
            }
            size={isMobile ? 'medium' : 'big'}
            type='primary-border'
          >
            Lengkapi Alamat
          </TWButton>

          <TWButton
            additionalClass='tw-rounded-md tw-w-full'
            handleOnClick={() => {
              onClick?.()

              changeAddressContext({
                showConfirmationFullAddress: false,
                fullAddressErrorMessage: '',
              })
            }}
            size={isMobile ? 'medium' : 'big'}
            type='primary'
          >
            Simpan Alamat
          </TWButton>
        </div>
      </div>
    </TWBaseModal>
  )
}
