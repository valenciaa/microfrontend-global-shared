import clsx from 'clsx'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'
import { useAddressContext } from '../../../../context/AddressContext'
import TWErrorText from '../../../../tw-components/error/TWErrorText'
import {
  handleAddressMiscAllSelected,
  handleAddressMiscAnySelected,
  handleAddressMiscTemplate,
} from '../../../../utils/UtilAddress'

export default function AddressMiscDropdown({
  onClick,
  watchFunction,
  errors,
  mode = 'add',
}) {
  const { state } = useAddressContext()

  const errorProvince = errors?.province
  const errorProvinceName = errorProvince?.province_name
  const errorProvinceNameMessage = errorProvinceName?.message

  const errorCity = errors?.city
  const errorCityName = errorCity?.city_name
  const errorCityNameMessage = errorCityName?.message

  const errorKecamatan = errors?.kecamatan
  const errorKecamatanName = errorKecamatan?.kecamatan_name
  const errorKecamatanNameMessage = errorKecamatanName?.message

  const errorKelurahan = errors?.kelurahan
  const errorKelurahanName = errorKelurahan?.kelurahan_name
  const errorKelurahanNameMessage = errorKelurahanName?.message

  const invalidMessage = state.invalidWarning

  const isError = !!(
    errorProvinceName ||
    errorCityName ||
    errorKecamatanName ||
    errorKelurahanName ||
    invalidMessage
  )

  const isNotDefault = !!(mode === 'add'
    ? handleAddressMiscAllSelected(watchFunction)
    : handleAddressMiscAnySelected(watchFunction))

  return (
    <div className='tw-flex tw-flex-col tw-gap-1'>
      <p className='tw-text-xs'>Pilih Wilayah Administratif</p>

      <div
        className={clsx(
          'tw-flex tw-justify-between tw-p-2.5 tw-border tw-border-solid tw-rounded-lg tw-cursor-pointer',
          isError ? 'tw-border-red-50' : 'tw-border-grey-20',
        )}
        onClick={onClick}
      >
        {isNotDefault ? (
          <p className='tw-text-sm'>
            {handleAddressMiscTemplate(watchFunction)}
          </p>
        ) : (
          <p className='tw-text-grey-30 tw-text-sm'>
            Pilih provinsi, kota, kecamatan dan kelurahan
          </p>
        )}

        <LazyLoadImage
          alt='arrow-caret-right-icon'
          height={10}
          placeholder={<Skeleton height={10} width={6} />}
          src={`${config.assetsURL}icon/arrow-caret-right-icon.svg`}
          width={6}
        />
      </div>

      {isError && (
        <TWErrorText
          message={
            errorProvinceNameMessage ||
            errorCityNameMessage ||
            errorKecamatanNameMessage ||
            errorKelurahanNameMessage ||
            invalidMessage
          }
        />
      )}
    </div>
  )
}
