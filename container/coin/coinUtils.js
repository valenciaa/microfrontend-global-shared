function coinHistoryMutate(currentHistory, newHistory) {
  // currentHistory means the histories we rendered now
  // newHistory means new history response from api that we just get
  let historyLength = 0
  if (currentHistory?.length === 0) {
    // we need to count of how much current data rendered now for passing value to props dataLength for react-infinite-scroll-component
    for (const lengthPerMonth of newHistory) {
      historyLength += lengthPerMonth.data.length
    }

    return {
      data: newHistory,
      historyLength,
    }
  } else {
    const finalData = [...currentHistory]
    for (const coinData of newHistory) {
      const findIndex = finalData.findIndex(
        (data) => data.month === coinData.month,
      )
      if (findIndex === -1) {
        // this means when there is a new month, immediately just push it into the array temp
        finalData.push({ month: coinData.month, data: coinData.data })
      } else {
        // this means when the month is already exists, we push the data of newHistory parameter to the data of existing month
        let tempNewHistory = finalData[findIndex].data
        tempNewHistory = [...tempNewHistory, ...coinData.data]
        finalData[findIndex].data = tempNewHistory
      }
    }

    // we need to count of how much current data rendered now for passing value to props dataLength for react-infinite-scroll-component
    for (const lengthPerMonth of finalData) {
      historyLength += lengthPerMonth.data.length
    }

    return {
      data: finalData,
      historyLength,
    }
  }
}

function checkBoxPrioritySwitcher(
  nonCheckAction,
  originalBrandList,
  uncheckBrandsToBeFiltered,
) {
  // nonCheckAction means that the checkbox is checked not from onChange handle check

  const reconstructBrandChecked = [] // variable that will be used to hold the values of all checked brands
  const finalReconstructBrandNotChecked = [] // variable that will be used to hold the values of all unchecked brands

  // if nonCheckAction is false then we need to separate the value of brands in that we haven't checked with brands that we've had checked
  // if nonCheckAction os true then we need to make new array of the value of brands that will not have data from parameters filterTitle in deleteFilterStockpiles function
  const reconstructBrandNotChecked = originalBrandList.filter(function (brand) {
    return !uncheckBrandsToBeFiltered.find(function (brandSelected) {
      if (!nonCheckAction) {
        return brand.merchant_name === brandSelected.filter
      } else {
        return brand.merchant_name === brandSelected.brand
      }
    })
  })

  // we need to separate and push the data of values that we already checked
  uncheckBrandsToBeFiltered.map((brand) =>
    reconstructBrandChecked.push({
      merchant_name: !nonCheckAction ? brand.filter : brand.brand,
      stamps_merchant_id: brand.value,
    }),
  )
  // we need to adjust the data inside of reconstructBrandNotChecked with reconstructBrandChecked so we separate it with a new variable
  reconstructBrandNotChecked.map((brand) =>
    finalReconstructBrandNotChecked.push({
      merchant_name: brand.merchant_name,
      stamps_merchant_id: brand.stamps_merchant_id.toString(),
    }),
  )

  // we need to sort the not checked values by ascending based on their merchant_name
  finalReconstructBrandNotChecked.sort(function (a, b) {
    return a.merchant_name.localeCompare(b.merchant_name)
  })

  // after we done with separating values, we combine the selected brands with the non-selected ones for render options later
  return [...reconstructBrandChecked, ...finalReconstructBrandNotChecked]
}
export { coinHistoryMutate, checkBoxPrioritySwitcher }
