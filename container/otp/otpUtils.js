import localforage from 'localforage'
import config from '../../../../config'
import getParam from '../../utils/GetParam'
import AES from 'crypto-js/aes'

async function manageOtpActions(
  courseAction = '',
  sendMethod = 'email',
  length,
  otp,
  setOtp,
  manageOtp,
  updateAndVerifyEmailOrPhone,
  verifyEmailOrPhone,
  isNeedMerging = false,
  getAuthData,
  auth = null
) {
  const action = getParam('action')
  let body = {}
  if (courseAction === 'onSuccessValidate') {
    const from = getParam('from') // from is used to handle show snackbar and indicate where the param action came from
    const tab = getParam('tab') // specific case for /klk/redemption page
    const isEmailVerify = await localforage.getItem('isEmailVerifyv2') // if true, then skip verify email
    switch (action) {
      case 'reset-password':
        {
          let redirect = 'auth/login?component=reset-password' // by default redirect to login page (if reset password when not logged in)
          if (
            [
              'password',
              'pin',
              'profile',
              'change-profile',
              'redemption',
              'inbox',
            ].includes(from)
          ) {
            redirect = `my-account/password?action=reset-password&from=${from}`
            if (config.environment === 'desktop') {
              switch (from) {
                case 'change-profile':
                case 'password':
                  redirect = `my-account?tab=change-profile&action=reset-password&from=${from}`
                  break
                case 'pin':
                  redirect = `my-account?tab=pin&action=reset-password&from=${from}`
                  break
                case 'redemption':
                  redirect = `klk/redemption?action=reset-password&from=${from}`
                  break
              }
            }
            if (tab !== null) {
              redirect += `&tab=${tab}`
            }
          } else if (from === 'combine-reward') {
            redirect = 'auth/login?component=reset-password&from=combine-reward'
          }
          window.location.href = config.baseURL + redirect
        }
        break
      case 'modify-email':
        {
          localforage.removeItem('changeEmail')
          // show snackbar success change email in edit-profile page if action contains modify-email and from contains change-profile
          let redirect =
            'my-account/change-profile?action=modify-email&from=change-profile'
          if (config.environment === 'desktop') {
            redirect = 'my-account?tab=change-profile&action=modify-email'
          }
          window.location.href = config.baseURL + redirect
        }
        break
      case 'modify-phone':
        {
          localforage.removeItem('changePhone')
          // show snackbar success change phone in edit-profile page if action contains modify-phone and from contains change-profile
          let redirect =
            'my-account/change-profile?action=modify-phone&from=change-profile'
          if (config.environment === 'desktop') {
            redirect = 'my-account?tab=change-profile&action=modify-phone'
          }
          window.location.href = config.baseURL + redirect
        }
        break
      case 'verify-email': {
        localforage.removeItem('otpEmail')
        const isNeedForceMerging =
          await localforage.getItem('isNeedForceMerging')

        if (from === 'profile' || from === 'change-profile') {
          let redirect = `my-account/${from}?action=verify-email&from=${from}`
          if (config.environment === 'desktop') {
            redirect = 'my-account?tab=change-profile&action=verify-email'
          }
          window.location.href = config.baseURL + redirect
        } else {
          const isActivate = getParam('is_activate')
          const lastVerifyStatus =
            window?.sessionStorage?.getItem('last_verify_status')
          if (
            isNeedForceMerging ||
            (isActivate && isActivate === 'true' && isNeedMerging)
          ) {
            window.location.href = config.baseURL + 'auth/merge-account'
          } else if (
            ['none-profile', 'phone-profile'].includes(lastVerifyStatus)
          ) {
            window.location.href = config.baseURL
          } else {
            window.location.href = config.baseURL
          }
        }
        break
      }
      case 'verify-phone': {
        localforage.removeItem('otpPhone')
        const lastVerifyStatus =
          window?.sessionStorage?.getItem('last_verify_status')
        const isNeedForceMerging =
          await localforage.getItem('isNeedForceMerging')

        if (isEmailVerify) {
          localforage.removeItem('isEmailVerifyv2')

          if (isNeedForceMerging || isNeedMerging) {
            window.location.href = config.baseURL + 'auth/merge-account'
          } else {
            window.location.href = config.baseURL
          }
        } else if (from === 'profile' || from === 'change-profile') {
          let redirect = `my-account/${from}?action=verify-phone&from=${from}`
          if (config.environment === 'desktop') {
            redirect = 'my-account?tab=change-profile&action=verify-phone'
          }
          window.location.href = config.baseURL + redirect
        } else {
          const isActivate = getParam('is_activate')
          if (isActivate && isActivate === 'true') {
            window.location.href =
              config.baseURL +
              'auth/otp-verification?action=verify-email&is_activate=true'
          } else if (['email', 'email-profile'].includes(lastVerifyStatus)) {
            if (isNeedForceMerging || isNeedMerging) {
              window.location.href = config.baseURL + 'auth/merge-account'
            } else {
              window.location.href = config.baseURL
            }
          } else {
            window.location.href =
              config.baseURL +
              'auth/otp-verification?action=verify-email&is_activate=false'
          }
        }
        break
      }
      case 'reset-password-odi':
        {
          const isActivate = getParam('is_activate')
          const isPartialRegister = getParam('is_partial_register')
          const isLoginLegacy = getParam('is_login_legacy')

          if (from === 'password-form') {
            if (isActivate && isActivate === 'true') {
              window.location.href =
                config.baseURL +
                'auth/login?component=reset-password&action=activate'
            } else if (isPartialRegister && isPartialRegister === 'true') {
              window.location.href =
                config.baseURL +
                'auth/login?component=reset-password&action=partial-register'
            } else if (isLoginLegacy && isLoginLegacy === 'true') {
              window.location.href =
                config.baseURL +
                'auth/login?component=reset-password&action=login_legacy'
            } else {
              window.location.href =
                config.baseURL + 'auth/login?component=reset-password'
            }
          } else if (from === 'merge-account') {
            // TODO NO LONGER VALID, NEED TO REMOVE SOMEDAY
            window.location.href =
              config.baseURL +
              'auth/login?component=reset-password&from=merge-account&action=activate'
          } else if (from === 'merge-account-v2') {
            let redirect =
              'auth/login?component=reset-password&from=merge-account-v2&action=activate'
            // if (config.environment === 'desktop') {
            //   redirect =
            //     'my-account?tab=combine-rewards&action=reset-password-odi&from=combine-reward'
            // }
            await localforage.removeItem('chosenMemberToken')
            await localforage.removeItem('identifier')
            window.location.href = config.baseURL + redirect
          } else if (from === 'manage-combine-rewards') {
            // only in desktop
            window.location.href =
              config.baseURL +
              'my-account?tab=manage-account&action=reset-password-odi&from=manage-combine-rewards'
          } else {
            window.location.href = config.baseURL
          }
        }
        break
      case 'forgot-passkey':
      case 'manual-merge': {
        const identifier = await localforage.getItem('identifier')
        const phoneCountryCode = await localforage.getItem('code')
        const isActivate = getParam('is_activate') || false
        const social = getParam('social') || false
        const isPartialRegister = getParam('is_partial_register') || false
        let redirect

        if (action === 'forgot-passkey') {
          redirect = `auth/register?component=profile-form-1&code=${phoneCountryCode}`
          if (identifier && !identifier.includes('@')) {
            redirect += `&phone=${identifier.replace('+', '')}`
          }

          if (isActivate) {
            redirect += '&action=activate'
          } else if (isPartialRegister) {
            redirect += '&action=partial-register'
          }
        } else if (action === 'manual-merge') {
          localforage.removeItem('activateMemberToken')
          redirect = 'my-account/combine-reward?action=manual-merge'

          if (config.environment === 'desktop') {
            redirect = 'my-account?tab=combine-rewards&action=manual-merge'
          }
        }

        if (social) {
          redirect += `&social=${social}`
        }

        window.location.href = config.baseURL + redirect
        break
      }
      case 'reset-pin-ms-stamps': {
        const identifier = await localforage.getItem('identifier')
        const phoneCountryCode = await localforage.getItem('code')
        window.location.href =
          config.baseURL +
          `auth/register?component=profile-form-1&action=activate_partner&phone=${identifier.replace(
            '+',
            '',
          )}&code=${phoneCountryCode}`
        break
      }
      case 'washopping': {
        getAuthData()
        break
      }
    }
    setOtp(new Array(length).fill(''))
  } else {
    const from = getParam('from')
    if (courseAction === 'onMountOrResend') {
      body = { otpRequestType: action, action: courseAction } // body by default for verify-email
      switch (action) {
        case 'verify-email':
          body = { ...body }
          break
        case 'verify-phone':
          body = { ...body, channel: sendMethod }
          break
        case 'modify-email': {
          const changeEmail = await localforage.getItem('changeEmail')
          body = { ...body, email: changeEmail }
          break
        }
        case 'modify-phone': {
          const changePhone = await localforage.getItem('changePhone')
          const changePhoneCountryCode =
            await localforage.getItem('countryCode')
          body = {
            ...body,
            new_phone: changePhone,
            international_country_code: changePhoneCountryCode,
            channel: sendMethod,
          }
          break
        }
        case 'reset-password': {
          await localforage.removeItem('rsToken')
          const identifier = await localforage.getItem('identifier')
          if (identifier) {
            body = {
              ...body,
              identifier,
            }
          } else {
            window.location.href = config.baseURL
          }
          break
        }
        case 'reset-password-odi': {
          await localforage.removeItem('rsToken')
          const identifier = await localforage.getItem('identifier')
          const chosenMemberToken =
            await localforage.getItem('chosenMemberToken')
          const userInfo = await localforage.getItem('userInfo')
          const memberToken =
            chosenMemberToken || userInfo?.memberToken || undefined

          if (identifier) {
            body = { ...body, identifier, memberToken }
          } else {
            window.location.href = config.baseURL
          }
          break
        }
        case 'forgot-passkey':
        case 'manual-merge': {
          if (['password-form', 'merge-account-v2'].includes(from)) {
            const chosenMemberToken =
              await localforage.getItem('chosenMemberToken')
            const userInfo = await localforage.getItem('userInfo')
            const memberToken =
              chosenMemberToken || userInfo?.memberToken || undefined
            body = {
              ...body,
              memberToken,
              channel: sendMethod.trim().length === 0 ? '' : sendMethod,
            }
          }

          if (sendMethod !== 'email') {
            const phoneCountryCode = await localforage.getItem('code')
            body = {
              ...body,
              international_country_code: phoneCountryCode,
            }
          }
          break
        }
        case 'reset-pin-ms-stamps': {
          await localforage.removeItem('rsToken')
          const identifier = await localforage.getItem('identifier')
          const userInfo = await localforage.getItem('userInfo')
          const phoneCountryCode = await localforage.getItem('code')
          const memberToken = userInfo?.memberToken || undefined

          if (identifier) {
            body = {
              ...body,
              identifier,
              channel: sendMethod,
              international_country_code: phoneCountryCode,
              memberToken,
            }
          } else {
            window.location.href = config.baseURL
          }
          break
        }
        case 'washopping': {
          const waNumber = getParam('waba')
          const unformatedCustId = getParam('remck')
          if (!waNumber && !unformatedCustId) {
            window.location.href = config.baseURL
            return
          }

          const custId = unformatedCustId.substring(
            2,
            unformatedCustId.length - 2,
          )
          const mdid = AES.encrypt(custId, 'accessCombineReward').toString()
          body = {
            ...body,
            otpRequestType: 'reset-password-odi',
            identifier: waNumber,
            mdid,
          }
          break
        }
        case 'membership-lainnya' : {          
           body = {
            member_number: getParam('memberID'),
            company_code: 'HCI',
            company_name: 'informa',
            action: 'verify-member',
            phone: auth?.user?.phone,
          }          
         
        }
        break
      }
    } else if (courseAction === 'onValidate') {
      body = { otpSubmit: true, otp: otp.join(''), action: courseAction }
      switch (action) {
        case 'modify-email': {
          const changeEmail = await localforage.getItem('changeEmail')
          delete body.otpSubmit
          body = { ...body, email: changeEmail, updateType: 'change-email' }
          break
        }
        case 'modify-phone': {
          const changePhone = await localforage.getItem('changePhone')
          const changePhoneCountryCode =
            await localforage.getItem('countryCode')
          delete body.otpSubmit
          body = {
            ...body,
            new_phone: changePhone,
            updateType: 'change-mobile-number',
            international_country_code: changePhoneCountryCode,
          }
          break
        }
        case 'verify-email': {
          const email = await localforage.getItem('otpEmail')
          const isActivate = getParam('is_activate')
          const isPartialRegister = getParam('is_partial_register')
          delete body.otpSubmit

          body = {
            ...body,
            verifType: 'verify-email',
            email,
            isCheckMerge:
              (isActivate && isActivate === 'true') ||
              (isPartialRegister && isPartialRegister === 'true'),
          }
          break
        }
        case 'verify-phone': {
          const mobileNumber = await localforage.getItem('otpPhone')
          const isActivate = getParam('is_activate')
          const isPartialRegister = getParam('is_partial_register')
          const social = getParam('social') || false
          const bpMethod = window?.sessionStorage?.getItem('bp_method') || null
          delete body.otpSubmit

          body = {
            ...body,
            verifType: 'verify-mobile-number',
            mobileNumber,
          }

          // case to show suggestion merging if customer forgot passkey using method email
          // later after activate / partial regis from ProfileForm1.js, they will be redirect to otp verify phone
          // and after otp verify phone, this logics below will bring them to page /auth/merge-account
          if (
            ((isActivate && isActivate === 'true') ||
              (isPartialRegister && isPartialRegister === 'true')) &&
            (bpMethod || social === 'google-cohesive')
          ) {
            body = {
              ...body,
              isCheckMerge: true,
            }
          }
          break
        }
        case 'reset-password':
          body = { ...body, otpRequestType: 'otp-reset-password' }
          break
        case 'reset-password-odi':
          body = { ...body, otpRequestType: 'otp-reset-password-odi' }
          break
        case 'forgot-passkey':
        case 'manual-merge': {
          const from = getParam('from')
          body = {
            ...body,
            otpRequestType: `otp-${action}`,
            channel: sendMethod,
          }

          if (['password-form', 'merge-account-v2'].includes(from)) {
            const fpToken = await localforage.getItem('activateMemberToken')
            body = {
              ...body,
              memberToken: fpToken,
            }

            if (from === 'merge-account-v2') {
              const mergedMembers =
                window?.sessionStorage?.getItem('chosen-accounts-legacy-v2') ||
                null
              body = {
                ...body,
                mergedMembers: [JSON.parse(mergedMembers)] || [],
              }
            }
          }

          if (sendMethod !== 'email') {
            const phoneCountryCode = await localforage.getItem('code')
            body = {
              ...body,
              international_country_code: phoneCountryCode,
            }
          }
          break
        }
        case 'reset-pin-ms-stamps': {
          const msToken = await localforage.getItem('activateMemberToken')
          body = {
            ...body,
            otpRequestType: 'otp-reset-pin-ms-stamps',
            channel: sendMethod,
            memberToken: msToken,
          }
          break
        }
        case 'washopping': {
          body = { ...body, otpRequestType: 'otp-reset-password-odi' }
          break
        }
        
        case 'membership-lainnya' : {
         const payloadInforma = JSON.parse( localStorage.getItem('payload-informa'))
           body = {
            user: {
              ...getAuthData,
              group: {
                ...getAuthData?.group,
                HCI: getParam('memberID'),
                ...payloadInforma
              }
            },
            companeName: 'informa',
            validate: {
              phone: auth?.user?.phone,
              otp: otp.join(''),
            },
            phone: auth?.user?.phone,
            otp: otp.join(''),
          }          
          break
        }

      }
    }
    if (
      courseAction === 'onValidate' &&
      (action === 'modify-email' || action === 'modify-phone')
    ) {
      updateAndVerifyEmailOrPhone(body)
    } else if (
      courseAction === 'onValidate' &&
      (action === 'verify-email' || action === 'verify-phone')
    ) {
      verifyEmailOrPhone(body)
    } else {
      manageOtp(body)
    }
  }
}

function handleChange(e, index, otp, setOtp) {
  const next = e.target.tabIndex
  const newRgx = new RegExp(
    /^[a-zA-Z!@#$%^&*()_+\-=[\]{};':"`\\|,.<>/?]*$/,
    'g',
  )
  const prohibitedInputRegexDetector = e.target.value.match(newRgx)
  if (prohibitedInputRegexDetector !== null || e.target.value === '') {
    return null
  } else {
    const tempOtp = [...otp]
    tempOtp.splice(index, 1, e.target.value)
    setOtp(tempOtp)
    focusNextOTPField(e, next, otp)
  }
}

function inputFocusBesidesNumberKey(e, index, otp, setOtp) {
  const pressedKey = e.key
  const next = e.target.tabIndex
  switch (pressedKey) {
    case 'Backspace':
    case 'Delete': {
      e.preventDefault()
      if (otp[index]) {
        const tempOtp = [...otp]
        tempOtp.splice(index, 1, '')
        setOtp(tempOtp)
      } else {
        focusPrevOTPField(e, next)
      }
      break
    }
    case 'ArrowLeft': {
      e.preventDefault()
      focusPrevOTPField(e, next)
      break
    }
    case 'ArrowRight': {
      e.preventDefault()
      focusNextOTPField(e, next, otp)
      break
    }
  }
}

function handlePaste(setOtp) {
  navigator.clipboard.readText().then((pastedText) => {
    const removeCharsBesidesDigits = pastedText.replace(/[^0-9]/g, '')
    const finalPasteOtp = []
    for (let i = 0; i < removeCharsBesidesDigits.length; i++) {
      if (finalPasteOtp.length !== 6) {
        finalPasteOtp.push(removeCharsBesidesDigits[i])
      }
    }
    if (finalPasteOtp.length) {
      setOtp(finalPasteOtp)
    }
  })
}

function focusNextOTPField(e, next, otp) {
  if (next < otp.length - 1) {
    e.target.form.elements[next + 1].focus()
  } else if (next === otp.length - 1) {
    e.target.form.elements[otp.length - 1].focus()
  }
}

function focusPrevOTPField(e, next) {
  if (next > 0) {
    e.target.form.elements[next - 1].focus()
  } else {
    e.target.form.elements[0].focus()
  }
}

function calculateExpiredOTPTimer(
  respExpiredUnix,
  respResendUnix,
  setExpiredOTPTimer,
  setTimerResend,
) {
  const calculateOtpTimer = (respUnix, stateSetter) => {
    if (respUnix <= 0) {
      stateSetter(0)
    } else {
      stateSetter(respUnix)
    }
  }
  calculateOtpTimer(respExpiredUnix, setExpiredOTPTimer) // calculate otp expired timer
  calculateOtpTimer(respResendUnix, setTimerResend) // calculate otp resend timer
}

function resendStatus(timerResend, counterAttempt, manageOtpLoading, err) {
  let disabled = false
  if (manageOtpLoading) {
    disabled = true
  } else {
    if (err.includes('1x24 jam')) {
      disabled = true
    } else if (timerResend > 0) {
      disabled = true
    } else if (counterAttempt > 4) {
      disabled = true
    }
  }
  return disabled
}

function renderResendTimer(timerResend, err) {
  let finalReturn
  if (err.includes('1x24 jam')) {
    finalReturn = null
  } else {
    if (timerResend !== null && timerResend > 0) {
      finalReturn = `(${timerResend})`
    } else {
      finalReturn = null
    }
  }
  return finalReturn
}

function submitButtonStatus(fetchingOtp, otp, err) {
  let disabled = false
  if (
    fetchingOtp ||
    err?.includes('Percobaan verifikasi sudah mencapai batas maksimal')
  ) {
    disabled = true
  } else {
    const checkOtpEmpty = otp.some((field) => {
      if (field === '') {
        return true
      }
    })
    if (checkOtpEmpty) {
      disabled = true
    }
  }
  return disabled
}

// function censorEmail(email = '', uncensoredChars = 2) {
//   const emailSplit = email ? email?.split('@') : []
//   let finalCensored = emailSplit[0]
//   let censor = ''

//   finalCensored =
//     finalCensored && finalCensored?.length > 0
//       ? finalCensored?.substring(0, finalCensored.length - uncensoredChars)
//       : ''
//   for (let i = 0; i < finalCensored?.length; i++) {
//     censor += '*'
//   }

//   const domain = emailSplit.length > 1 ? `@${emailSplit[1]}` : ''
//   return censor + emailSplit[0]?.slice(uncensoredChars * -1) + domain
// }

// function censorPhone(phone = '', uncensoredChars = 2) {
//   const finalCensored =
//     phone && phone?.length > 0
//       ? phone?.substring(0, phone.length - uncensoredChars)
//       : ''
//   let censor = ''
//   for (let i = 0; i < finalCensored?.length; i++) {
//     censor += '*'
//   }
//   return censor + phone?.slice(uncensoredChars * -1)
// }

async function censorChannel(
  auth,
  setCensoredValue,
  setDefaultSendMethod,
  setIdentifier,
) {
  // channel can only be from either phone or email
  const action = getParam('action')
  const identifier = await localforage.getItem('identifier')

  if (['verify-phone', 'modify-phone'].includes(action)) {
    setDefaultSendMethod('WhatsApp')
    switch (action) {
      case 'verify-phone':
        {
          const mobileNumber = await localforage.getItem('otpPhone')
          // take out masking indentifier for now
          // setCensoredValue(censorPhone(mobileNumber))
          setCensoredValue(mobileNumber || '')
        }
        break
      case 'modify-phone': {
        const displayedChangePhone = await localforage.getItem(
          'displayedChangePhone',
        )
        // take out masking indentifier for now
        // setCensoredValue(censorPhone(changePhone))
        setCensoredValue(displayedChangePhone || '')
      }
    }
  } else if (['verify-email', 'modify-email'].includes(action)) {
    setDefaultSendMethod('email')
    switch (action) {
      case 'verify-email':
        {
          const email = await localforage.getItem('otpEmail')
          // take out masking indentifier for now
          // setCensoredValue(censorEmail(email))
          setCensoredValue(email || '')
        }
        break
      case 'modify-email':
        {
          const changeEmail = await localforage.getItem('changeEmail')
          // take out masking indentifier for now
          // setCensoredValue(censorEmail(changeEmail))
          setCensoredValue(changeEmail || '')
        }
        break
    }
  } else if (action === 'reset-password-odi') {
    if (identifier?.match(/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/)) {
      setDefaultSendMethod('email')
      // take out masking indentifier for now
      // setCensoredValue(censorEmail(identifier))
      setCensoredValue(identifier || '')
    } else {
      setDefaultSendMethod('WhatsApp')
      // take out masking indentifier for now
      // setCensoredValue(censorPhone(identifier))
      setCensoredValue(identifier || '')
    }
  } else if (action === 'reset-password') {
    setDefaultSendMethod('email')
    if (identifier?.match(/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/)) {
      // take out masking indentifier for now
      // setCensoredValue(censorEmail(identifier))
      setCensoredValue(identifier || '')
    }
  } else if (action === 'forgot-passkey') {
    // set space to trigger manageOtpActions() in useEffect OtpVerificationWrapper since the real default send method should be returned for the 1st time from wrapper
    setDefaultSendMethod(' ')
  } else if (action === 'manual-merge') {
    let mergedMembers = window?.sessionStorage?.getItem(
      'chosen-accounts-legacy-v2',
    )

    if (mergedMembers) {
      mergedMembers = await JSON.parse(mergedMembers)
      if (mergedMembers?.email === '') {
        setDefaultSendMethod('WhatsApp')
        setCensoredValue(mergedMembers?.mobile_number)
      } else {
        setDefaultSendMethod('email')
        setCensoredValue(mergedMembers?.email)
      }
    } else {
      let redirect = 'my-account/combine-reward'

      if (config.environment === 'desktop') {
        redirect = 'my-account?tab=combine-rewards'
      }

      window.location.href = config.baseURL + redirect
    }
  } else if (action === 'washopping') {
    const waNumber = getParam('waba')
    setIdentifier(waNumber)
    setDefaultSendMethod('WhatsApp')
  } else if (action === 'reset-pin-ms-stamps') {
    setCensoredValue(identifier)
    setDefaultSendMethod('WhatsApp')
  } else if (action === 'membership-lainnya') {
      setDefaultSendMethod('SMS')
    
      setCensoredValue( '*'.repeat(auth?.user?.phone.length - 3) + auth?.user?.phone.slice(-3))
  } else {
    // take out masking indentifier for now
    // setCensoredValue(censorEmail(auth?.user?.email))
    setCensoredValue(auth?.user?.email || '')
  }
}

function convertTimeToSeconds(time) {
  // Pisahkan string berdasarkan tanda titik dua (:)
  const [hours, minutes, seconds] = time.split(':').map(Number);

  // Konversi jam, menit, dan detik ke total detik
  const totalSeconds = (hours * 3600) + (minutes * 60) + seconds;

  return totalSeconds;
}


export {
  manageOtpActions,
  handleChange,
  inputFocusBesidesNumberKey,
  handlePaste,
  calculateExpiredOTPTimer,
  renderResendTimer,
  resendStatus,
  submitButtonStatus,
  censorChannel,
  convertTimeToSeconds,
}
