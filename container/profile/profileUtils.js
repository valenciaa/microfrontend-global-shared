import GetParam from '../../utils/GetParam'

function handleSnackbar(setInfoboxMsg) {
  const action = GetParam('action')
  if (
    [
      'reset-with-password',
      'reset-password',
      'verify-email',
      'verify-phone',
    ].includes(action)
  ) {
    // reset-with-password means reset new pin after unblock it with password validation
    let wording
    if (action === 'reset-with-password') {
      wording = 'PIN berhasil disimpan'
    } else if (action === 'reset-password') {
      wording = 'Kata sandi baru berhasil disimpan'
    } else if (action === 'verify-email') {
      wording = 'Email anda berhasil terverifikasi'
    } else if (action === 'verify-phone') {
      wording = 'Nomor telepon anda berhasil terverifikasi'
    }
    window.history.replaceState(
      null,
      null,
      window.location.href.replace(`?action=${action}&from=profile`, ''),
    )
    setInfoboxMsg(wording)
  }
}

export { handleSnackbar }
