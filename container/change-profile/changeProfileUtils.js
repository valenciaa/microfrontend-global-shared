import dayjs from 'dayjs'
import config from '../../../../config'
import GetParam from '../../utils/GetParam'

function handleSnackbar(router, setInfoboxMsg) {
  const action = GetParam('action')
  if (
    [
      'modify-email',
      'modify-phone',
      'reset-with-otp',
      'reset-with-password',
      'reset-password',
      'verify-email',
      'verify-phone',
    ].includes(action)
  ) {
    let wording
    let redirect = 'my-account/change-profile'

    if (config.environment === 'desktop') {
      redirect = 'my-account?tab=change-profile'
    }

    if (action === 'modify-email') {
      wording = 'Email baru sudah berhasil disimpan dan terverifikasi'
    } else if (action === 'modify-phone') {
      wording = 'Nomor handphone baru sudah berhasil disimpan dan terverifikasi'
    } else if (
      action === 'reset-with-otp' ||
      action === 'reset-with-password'
    ) {
      wording = 'PIN berhasil disimpan'
    } else if (action === 'reset-password') {
      wording = 'Kata sandi baru berhasil disimpan'
    } else if (action === 'verify-email') {
      wording = 'Email anda berhasil terverifikasi'
    } else if (action === 'verify-phone') {
      wording = 'Nomor telepon anda berhasil terverifikasi'
    }
    router.push(config.baseURL + redirect, undefined, { shallow: true })
    setInfoboxMsg(wording)
  }
}

function checkBirthDate(birthIndividual, dateSelected) {
  const now = dayjs().format('YYYY-MM-DD')
  const dateDifferent = dayjs(now).diff(dateSelected, 'day')
  const yearDifferent = dayjs(now).diff(dateSelected, 'year')
  let errAge
  if (dateDifferent < 0) {
    errAge = 'Usia yang didaftarkan tidak boleh lebih dari tanggal hari ini'
  } else {
    if (birthIndividual === 'children' && yearDifferent > 16) {
      errAge = 'Usia anak yang dapat didaftarkan maks.16 tahun'
    } else if (birthIndividual === 'pets' && yearDifferent > 30) {
      errAge = 'Usia hewan peliharaan yang dapat didaftarkan maks.30 tahun'
    } else if (birthIndividual === 'user' && yearDifferent < 12) {
      errAge = 'Usia harus min.12 tahun'
    }
  }
  return errAge
}

export { handleSnackbar, checkBirthDate }
