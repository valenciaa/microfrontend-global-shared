import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import config from '../../../../config'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import FloatingInfobox from '../../layouts/atoms/FloatingInfobox'
import { userAgentChecker } from '../../utils/UserAgentChecker'

const TabBar = dynamic(() => import('../../layouts/moleculs/TabBar'))

export default function UnifyAppsHeaderContainer(props) {
  const unifyAppsContext = useUnifyAppsContext()
  const { headerTitle } = unifyAppsContext
  const [isScrolled, setIsScrolled] = useState(false)
  const [toggleClipboardInfobox, setToggleClipboardInfobox] = useState(false)
  const router = useRouter()

  useEffect(() => {
    function scrollListener() {
      setIsScrolled(window.pageYOffset >= 1)
    }
    window.addEventListener('scroll', scrollListener, { passive: true })
    return () => {
      window.removeEventListener('scroll', scrollListener)
    }
  }, [])

  const renderHeaderContent = () => {
    const renderLeftHeaderIcon = (
      leftIcon = 'https://cdn.ruparupa.io/media/promotion/ruparupa/ruparupa-rewards/Icon/Profil-Akun/Arrow-Left.svg',
    ) => {
      return (
        <CustomLazyLoadImage
          alt='Close Icon'
          height={24}
          src={leftIcon}
          width={24}
        />
      )
    }
    const renderRightHeaderIcon = (
      rightIcon = 'https://cdn.ruparupa.io/media/promotion/ruparupa/asset-unify/tab-icon.svg',
    ) => {
      return (
        <CustomLazyLoadImage
          alt='Menu'
          height={30}
          src={rightIcon}
          width={30}
        />
      )
    }
    return (
      <>
        <div className='header-content'>
          {!props.hideBackButton && (
            <div
              className='flex items-center cursor-pointer'
              onClick={() => handleClickTopLeft()}
            >
              {renderLeftHeaderIcon()}
            </div>
          )}
          <div
            className='base-title margin-left-xs flex-1'
            onClick={
              props.onlineTransaction && props.transactionNo
                ? () => handleClickTitle()
                : null
            }
          >
            <span>{headerTitle}</span>
          </div>
          {props.rightHeader && (
            <div
              className='flex items-center cursor-pointer'
              onClick={() => props.handleClickTopRight()}
            >
              {renderRightHeaderIcon()}
            </div>
          )}
        </div>
      </>
    )
  }

  async function handleClickTopLeft() {
    // by default sekarang dibuat untuk hanya back sekali ketika icon ditekan
    // jika ingin menambah logic yang spesifik disini silahkan, bisa menggunakan pageFrom yang merupakan salah satu value dari unifyAppsContext
    if (config.environment === 'mobile') {
      if (document.referrer && document.referrer !== '' && !props.noReferBack) {
        window.history.back()
      } else {
        // for moapp
        const isApps = userAgentChecker() !== 'mobile'
        if (isApps) {
          props.setDestinationURL(
            '/my-account/transaction/list?tab=online-transaction',
          )
          return
        }
        router.replace('/my-account/transaction/list?tab=online-transaction')
      }
    } else {
      window.history.back()
    }
  }

  async function handleClickTitle() {
    // change to invoice data
    navigator.clipboard.writeText(props.transactionNo)
    setToggleClipboardInfobox(true)
  }

  return (
    <div
      className={`
      ${
        config.environment === 'mobile' &&
        `header-unifyapps-top header-shadow
      ${isScrolled ? 'header-unifyapps-top-sticky' : 'header-unifyapps-top'}`
      }
      margin-bottom-xs
    `}
    >
      {toggleClipboardInfobox && (
        <FloatingInfobox
          additionalButtonClose
          additionalButton={{ text: 'Tutup' }}
          duration={2500}
          setToggle={setToggleClipboardInfobox}
          toggleFlag={toggleClipboardInfobox}
          type='info'
        >
          Berhasil Tersalin
        </FloatingInfobox>
      )}
      {renderHeaderContent()}
      {props.showTabBar && <TabBar tabOptions={props.tabOptions} />}
    </div>
  )
}
