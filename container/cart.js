import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'
import config from '../../../config'

import { handleSendFacebookConversionAddToCartRequest } from '../utils/FacebookConversionWrapper'
import { handleSendTiktokConversionAddToCartRequest } from '../utils/TiktokConversionWrapper'
import {
  mixpanelTrack,
  mixpanelTrackCart,
  mixpanelTrackCartItem,
} from '../utils/MixpanelWrapper'

import { calculateCartItem } from '../utils/CheckCartItem'

function addToCart(
  activeVariant,
  type,
  quantity,
  storeCode,
  pickupCode,
  deliveryMethod,
  product,
  cart,
  auth,
  addToCartOriginLocation,
  setToggleErrorClipboardInfobox,
  cartData,
  setErrMessage,
) {
  const utm = Cookies.get('utm')
  const itmOrigin = Cookies.get('itm_origin')

  const locationHref = `${
    utm !== undefined && !['utm_'].includes(window.location.href)
      ? window.location.href.indexOf('?') !== -1
        ? window.location.href + '&' + utm
        : window.location.href + '?' + utm
      : window.location.href
  }`
  const getItmOrigin = `${
    itmOrigin !== undefined
      ? locationHref + '&itm_origin=' + itmOrigin
      : locationHref
  }`

  const items = cart?.items
  const isItemOnCart = items?.find((item) => item?.sku === activeVariant?.sku)
  if (
    setToggleErrorClipboardInfobox &&
    Array.isArray(items) &&
    isEmpty(isItemOnCart) &&
    items.length >= config?.cartMaxQty &&
    !config.ignoreCartMaxQtyCustomerIDList?.includes(
      parseInt(auth?.user?.customer_id),
    )
  ) {
    setToggleErrorClipboardInfobox(true)
    return null
  } else if (!isEmpty(cartData) && !isEmpty(activeVariant)) {
    // comparison of the number of incoming qty and existing qty
    quantity = calculateCartItem(
      cartData,
      activeVariant,
      quantity,
      setToggleErrorClipboardInfobox,
      setErrMessage,
      null,
    )

    if (!quantity) {
      return null
    }
  }

  let pickupQuery = {
    pickup_code: pickupCode,
  }

  if (type === 'membership') {
    pickupQuery = {
      ...pickupQuery,
      pickup_code: 'pickup_HO_ACE', // ACE_HARDWARE
      pickup_name: 'ACE Hardware Indonesia', //ACE_HARDWARE
    }
  }

  const data = [
    {
      sku: activeVariant.sku,
      qty_ordered: 0,
      qty_increment: quantity,
      shipping: {
        pickup: {
          ...pickupQuery,
        },
        delivery_method: deliveryMethod,
        store_code: storeCode,
      },
      utm_parameter: getItmOrigin,
      is_checked: true,
    },
  ]

  // Mixpanel tracking
  mixpanelTrackCart(cart?.items, {
    'Location': addToCartOriginLocation, // eslint-disable-line
    'Item Total': cart?.total_qty || 0,
    'Subtotal': cart?.grand_total || 0, // eslint-disable-line
  })
  mixpanelTrackCartItem(cart?.items, {
    Location: addToCartOriginLocation,
  })

  let visitorEmail = ''
  let customerID = ''
  if (auth?.user?.email) {
    visitorEmail = auth?.user?.email
    customerID = auth?.user?.customer_id
  }
  const metaPrice =
    activeVariant?.prices?.[0]?.special_price > 0
      ? parseInt(activeVariant?.prices?.[0]?.special_price)
      : parseInt(activeVariant?.prices?.[0]?.price)

  window.dataLayer.push({
    event: 'addToCart',
    productID: `${activeVariant?.sku}`,
    CriteoProductID: `${activeVariant?.sku}`,
    email: `${visitorEmail}`,
    customerID: `${customerID}`,
    ecommerce: {
      currencyCode: 'IDR',
      add: {
        products: [
          {
            name: `${product?.name}`,
            id: `${activeVariant?.sku}`,
            price: `${metaPrice}`,
            brand: `${
              (product?.brand && product?.brand?.name.replace("'", '')) || ''
            }`,
            category: `${
              (product?.categories &&
                product?.categories?.length > 0 &&
                product?.categories?.[0]?.name.replace("'", '')) ||
              ''
            }`,
            quantity: quantity,
          },
        ],
      },
    },
  })

  // Handle send Facebook Conversion API
  handleSendAddToCartCapi(
    { price: metaPrice, sku: activeVariant?.sku },
    product?.title,
    quantity,
    type,
  )

  // Handle send Tiktok Conversion API
  handleSendTiktokAddToCartCapi(
    { price: metaPrice, sku: activeVariant?.sku },
    product?.title,
    quantity,
    type,
  )

  return {
    minicart_id: cart?.minicart_id,
    items: data,
  }
}

// Handle send Facebook Conversion API (AddToCart)
function handleSendAddToCartCapi(activeVariant, product, quantity, type) {
  if (config.isLiveSite) {
    let pageFrom = type || ''
    handleSendFacebookConversionAddToCartRequest(
      pageFrom,
      activeVariant,
      product,
      quantity,
    )
  }
}

// Handle send Tiktok Conversion API (AddToCart)
function handleSendTiktokAddToCartCapi(activeVariant, product, quantity, type) {
  if (config.isLiveSite) {
    let pageFrom = type || ''
    handleSendTiktokConversionAddToCartRequest(
      pageFrom,
      activeVariant,
      product,
      quantity,
    )
  }
}

function onIncreaseQty(cart, item) {
  let minimumIncrease = 1
  if (item?.is_apply_multiple) {
    minimumIncrease = item.minimum_order
  }

  if (item.qty_ordered + minimumIncrease <= item.max_qty) {
    const qty = parseInt(item.qty_ordered + minimumIncrease)
    // we need to dispatch and update qty
    const data = [
      {
        sku: item.sku,
        qty_ordered: qty,
      },
    ]

    mixpanelTrack('Additional Item', {
      'Item Name': item.name,
      'Item ID': item.sku,
      'Location': 'Header Cart', // eslint-disable-line
    })

    return {
      minicart_id: cart.minicart_id,
      items: data,
    }
  }
}

function onDecreaseQty(cart, item) {
  let minimumOrder = 1

  if (item?.is_apply_multiple) {
    minimumOrder = item.minimum_order
  }

  let qty
  if (item.qty_ordered - minimumOrder <= 0) {
    // don't update
  } else {
    if (item.qty_ordered - minimumOrder <= item.max_qty) {
      qty = parseInt(item.qty_ordered - minimumOrder)
    } else {
      qty = parseInt(item.max_qty - item.qty_ordered)
    }

    // we need to dispatch and update qty
    const data = [
      {
        sku: item.sku,
        qty_ordered: qty,
      },
    ]

    mixpanelTrack('Deduct Item', {
      'Item Name': item.name,
      'Item ID': item.sku,
      'Location': 'Header Cart', // eslint-disable-line
    })

    return {
      minicart_id: cart.minicart_id,
      items: data,
    }
  }
}

function onDeleteItem(cart, item) {
  // we need to dispatch and update qty
  let data = [
    {
      sku: item.sku,
    },
  ]

  // delete a bundling
  if (item && item?.package?.package_id !== 0) {
    let removedProduct = []
    if (!isEmpty(item.package?.sku_detail)) {
      for (const product of item.package.sku_detail) {
        removedProduct?.push({
          sku: product?.sku,
          package: {
            package_id: item?.package?.package_id,
          },
        })
      }
    }
    data = removedProduct
  }

  return {
    minicart_id: cart.minicart_id,
    items: data,
  }
}

export {
  addToCart,
  onDecreaseQty,
  onIncreaseQty,
  onDeleteItem,
  handleSendAddToCartCapi,
  handleSendTiktokAddToCartCapi,
}
