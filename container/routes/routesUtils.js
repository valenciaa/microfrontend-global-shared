import config from '../../../../config'
import ItmCompanyName from '../../utils/ItmCompanyName'

/* eslint-disable camelcase */
function redirectSSR(urlPath, rule_base, product_title) {
  let redirectUrl = '/cc'
  if (rule_base === 0) {
    redirectUrl = '/c'
  }
  /** "st" is product status */

  let itmSauce = urlPath?.split('/')
  itmSauce = itmSauce?.[itmSauce.length - 1]?.replace('.html', '')
  const companyName = ItmCompanyName()
  const itmSource = 'redirect-' + itmSauce + '-' + (companyName || 'rr')
  redirectUrl +=
    '/' +
    urlPath +
    '?st=inactive' +
    '&itm_source=' +
    itmSource +
    '&itm_term=' +
    product_title +
    '&itm_campaign=' +
    itmSauce +
    '&itm_device=' +
    config?.environment

  const results = { redirectUrl, productTitle: product_title }

  return {
    props: {
      results,
    },
  }
}

function routesCheckPDP(resRoute) {
  if (resRoute && resRoute.data) {
    if (!resRoute?.data?.data) {
      return { notFound: true }
    }

    if (resRoute?.data?.data) {
      const {
        redirect = true,
        product_status,
        category_level_3_url_key,
        product_title,
        category_level_3_rule_based,
      } = resRoute?.data?.data

      if (!redirect || product_status === 10) {
        return false
      }

      if (redirect) {
        if (!category_level_3_url_key) {
          return { notFound: true }
        }

        const urlPath = category_level_3_url_key

        return redirectSSR(urlPath, category_level_3_rule_based, product_title)
      }
    }
  }
}

export { routesCheckPDP }
