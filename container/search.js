import Cookies from 'js-cookie'
import capitalize from 'lodash/capitalize'
import config from '../../../config'
import {
  appsflyerEventNames,
  appsflyerTrackEvent,
} from '../utils/AppsflyerWrapper'
import { mixpanelTrack } from '../utils/MixpanelWrapper'
import ItmCompanyName from '../utils/ItmCompanyName'
import GetParam from '../utils/GetParam'
import router from 'next/router'
import { ACE_HARDWARE } from '../utils/constants/AceConstants'

const handleSearchOnType = (text, setKeyword, dispatch) => {
  setKeyword(text)
  return dispatch
}

const searchProductByKeywordAlgolia = (keyword, dispatch) => {
  return dispatch
}
const searchPageRedirect = (
  keyword,
  itmFrom = '',
  setIsSubmitEvent,
  setKeyword,
  setItmFrom,
) => {
  setIsSubmitEvent(true)
  setKeyword(keyword)
  setItmFrom(itmFrom)
  if (keyword && keyword !== '') {
    handleCookies(keyword)
  }
  // this.mixpanelTrackSearch(itmFrom, keyword)
}

function handleDeleteLatest(keyword, latestSearch, setLatestSearch) {
  let latestModified = []
  const cookiesName = `search-history${
    config.companyNameCSS !== 'ruparupa' ? '-' + config.companyNameCSS : ''
  }`
  latestModified = latestSearch?.filter((item) => item !== keyword)
  setLatestSearch(latestModified)
  if (latestModified.length > 0) {
    Cookies.set(cookiesName, latestModified.reverse().toString())
  } else {
    Cookies.remove(cookiesName)
  }
}

const analyticsTrack = (keyword, itmFrom, itmCampaign, companyName) => {
  if (keyword) {
    keyword = keyword?.replace(/-/g, ' ')
  }
  mixpanelTrack('Search', {
    'Keyword': keyword, // eslint-disable-line
    'Search Suggestion?': itmFrom === 'suggestion',
    'ITM Campaign': itmCampaign || 'None',
    'ITM Source': (companyName ? companyName + '-' : '') + 'search',
    'ITM Term': keyword || 'None',
  })

  appsflyerTrackEvent(appsflyerEventNames.SEARCH, {
    af_search_string: keyword,
  })
}

const pushRouterSearch = (
  keyword,
  searchData,
  type,
  categoryUrl = null,
  storeName = null,
  shopInShop,
  tahuUrl,
  pageFrom = '',
  productSku = '',
) => {
  const showAll = false
  const companyName = ItmCompanyName()
  let itmReferral = ''
  if (
    (companyName === 'toys' || companyName === '') &&
    GetParam('itm_referral') === 'tkid'
  ) {
    itmReferral = 'itm_referral=tkid&'
  }
  if (keyword) {
    let word = keyword.replace(/ +/g, ' ').toLowerCase().trim()
    if (word.charAt(0) === '-' || word.charAt(0) === ' ') {
      word = word.replace(/-+/g, '')
    }
    word = word.replace(/[^0-9A-Za-z ]/gi, ' ').replace(/ +/g, '-')
    let itm =
      `?itm_source=${companyName ? companyName + '-' : ''}search` +
      '&itm_campaign=direct-search&itm_term=' +
      word +
      '&itm_device=' +
      config.environment +
      '&' +
      itmReferral

    if (type === 'suggestion') {
      itm =
        `?itm_source=search+suggestion+keyword+homepage+${config.companyCode}` +
        '&itm_campaign=suggestion+keyword&itm_term=' +
        word +
        '&itm_device=' +
        config.environment +
        '&' +
        itmReferral
    } else if (type === 'latest') {
      itm =
        `?itm_source=${companyName ? companyName + '-' : ''}search` +
        '&itm_origin=search' +
        '&itm_campaign=suggestion-last-search&itm_term=' +
        word +
        '&itm_device=' +
        config.environment +
        '&' +
        itmReferral
    } else if (type === 'popular') {
      itm =
        `?itm_source=${companyName ? companyName + '-' : ''}search` +
        '&itm_campaign=suggestion-popular-search&itm_term=' +
        word +
        '&itm_device=' +
        config.environment +
        '&' +
        itmReferral
    } else if (type === 'view-all-product') {
      itm =
        `?itm_source=${companyName ? companyName + '-' : ''}search` +
        '&itm_campaign=suggestion&itm_term=' +
        word +
        '&itm_device=' +
        config.environment +
        '&' +
        itmReferral
    } else if (type === 'auto-complete') {
      itm =
        `?itm_source=${companyName ? companyName + '-' : ''}search` +
        '&itm_campaign=auto-complete&itm_term=' +
        word +
        '&itm_device=' +
        config.environment +
        '&' +
        itmReferral
    } else if (type === 'direct-search') {
      itm =
        `?itm_source=${companyName ? companyName + '-' : ''}search` +
        '&itm_campaign=direct_search&itm_term=' +
        word +
        '&itm_device=' +
        config.environment +
        (pageFrom === 'product-detail' ? '-&-' + productSku : '') +
        '&' +
        itmReferral
    } else if (type === 'tahu') {
      itm =
        `?itm_source=${companyName ? companyName + '-' : ''}search` +
        '&itm_campaign=suggestion&itm_term=' +
        word +
        '&itm_device=' +
        config.environment +
        '&' +
        itmReferral
    } else if (type === 'suggestion-keyword') {
      itm =
        `?itm_source=${companyName ? companyName + '-' : ''}search` +
        '&itm_campaign=suggestion-keyword&itm_term=' +
        word +
        '&itm_device=' +
        config.environment +
        '&' +
        itmReferral
    }

    if (showAll) {
      itm =
        `?itm_source=${
          companyName ? companyName + '-' : ''
        }search&itm_campaign=showAll&itm_term=` +
        keyword +
        '&itm_device=' +
        config.environment +
        '&' +
        itmReferral
    }

    if (type === 'tahu') {
      window.location.href =
        config.legacyBaseURL +
        'brands/' +
        tahuUrl +
        itm +
        'keyword=' +
        encodeURIComponent(keyword) +
        '&query=' +
        encodeURIComponent(keyword)
    } else if (categoryUrl) {
      window.location.href =
        config.legacyBaseURL +
        `${categoryUrl}` +
        itm +
        'keyword=' +
        encodeURIComponent(keyword) +
        '&query=' +
        encodeURIComponent(keyword)
    } else if (storeName) {
      window.location.href =
        config.legacyBaseURL +
        'store/' +
        `${storeName}` +
        '?keyword=' +
        encodeURIComponent(keyword) +
        `&query=${encodeURIComponent(keyword)}`
    } else {
      if (window.location.href.includes('jual')) {
        delete router?.router?.query?.brands
        delete router?.router?.query?.categories3
        delete router?.router?.query?.city
        delete router?.router?.query?.deliveryMethod
        delete router?.router?.query?.colour
        delete router?.router?.query?.label
        delete router?.router?.query?.price
        delete router?.router?.query?.store
        delete router?.router?.query?.sortBy
        delete router?.router?.query?.page
        router.push(
          {
            pathname: '/jual/' + encodeURIComponent(word),
            query: {
              ...router?.router?.query,
              query: encodeURIComponent(word),
            },
          },
          undefined,
          { shallow: true },
        )
      } else {
        window.location.href =
          config.legacyBaseURL +
          'jual/' +
          encodeURIComponent(word) +
          itm +
          '&query=' +
          encodeURIComponent(word) +
          '&searchMethod=typing&searchMethodLocation=home'
      }
    }
  }
}
function handleCookies(keyword) {
  const cookiesName = `search-history${
    config.companyNameCSS !== 'ruparupa' ? '-' + config.companyNameCSS : ''
  }`
  let searchHistory = Cookies.get(cookiesName)
  let arr
  if (searchHistory) {
    if (searchHistory.indexOf(keyword) === -1) {
      searchHistory += `,${keyword}`
      Cookies.set(cookiesName, searchHistory, { expires: 1095 })
      if (searchHistory.match(/,/g).length > 2) {
        arr = searchHistory.split(',')
        arr.splice(0, arr.length - 3)
        searchHistory = arr.toString()
        Cookies.set(cookiesName, searchHistory, { expires: 1095 })
      }
    }
  } else {
    Cookies.set(cookiesName, keyword, { expires: 1095 })
  }
}

function randomPlaceholder(keyword) {
  let finalSearchSuggestion
  if (typeof keyword === 'object' && keyword.length > 0) {
    const keywordRandomList = keyword
    finalSearchSuggestion = `Cari ${capitalize(
      keywordRandomList[Math.floor(Math.random() * keywordRandomList.length)],
    )}`
  } else {
    finalSearchSuggestion = `Cari di ${
      config.companyCode === 'AHI'
        ? ACE_HARDWARE
        : config.companyCode === 'HCI'
          ? 'Informa'
          : config.companyName
    }`
  }
  return finalSearchSuggestion
}
const handleSearchEmptyString = (
  searchKeyword,
  keyword,
  selectedPlaceholder,
  setType,
) => {
  if (selectedPlaceholder.split(' ')[1] !== 'di') {
    setType('suggestion')
    return selectedPlaceholder.replace('Cari ', '')
  } else {
    return null
  }
}

const getLinkSearchEmptyString = (
  selectedPlaceholder,
  itmSource,
  from,
  searchKeyword,
  keyword,
) => {
  if (selectedPlaceholder.split(' ')[1] !== 'di') {
    searchKeyword = selectedPlaceholder.replace('Cari ', '')
    itmSource = `search+suggestion+keyword+homepage+${config.companyCode}`
    from = 'suggestion+keyword' // need itm generator
  } else {
    // arahin ke semua promosi
    searchKeyword = keyword
  }
}

export {
  handleSearchOnType,
  searchPageRedirect,
  searchProductByKeywordAlgolia,
  handleDeleteLatest,
  pushRouterSearch,
  handleCookies,
  analyticsTrack,
  randomPlaceholder,
  handleSearchEmptyString,
  getLinkSearchEmptyString,
}
