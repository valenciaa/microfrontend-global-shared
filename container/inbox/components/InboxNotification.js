import React, { useState } from 'react'
import config from '../../../../../config'
import VoucherDot from '../../../layouts/atoms/VoucherDot'
import HtmlModal from '../../../layouts/templates/HtmlModal'
import Modal from '../../../layouts/templates/Modal'
import { ConvertImageType, ItmGenerator } from '../../../utils'
import { mixpanelKlkTrack } from '../../../utils/MixpanelWrapper'
import { usePinContext, usePinUpdateContext } from '../../../context/PinContext'
import { useRedemptionUpdateContext } from '../../../context/RedemptionContext'

export default function InboxNotification({
  notificationData,
  isRead,
  markAsRead,
  type,
}) {
  const pinData = usePinContext()
  const pinHandler = usePinUpdateContext()
  const redemptionHandler = useRedemptionUpdateContext()
  const [showModal, setShowModal] = useState(false)

  function handleAfterClick() {
    if (notificationData.id && !isRead) {
      markAsRead(notificationData.id)
    }
  }

  function approvalCourseAction() {
    if (!pinData.pinStatus.has_pin) {
      pinHandler.toggleModalForPin(
        'toggleModalSetNewPin',
        true,
        `inbox:${notificationData.url_key}`,
        false,
      )
    } else if (!pinData.pinStatus.pin_is_blocked) {
      redemptionHandler.getPendingRedemptionDataInbox(notificationData.url_key)
    } else if (pinData.pinStatus.pin_is_blocked) {
      pinHandler.toggleModalForPin(
        'toggleModalBlockedPin',
        true,
        'inboxInfo',
        pinData.toggleModalBlockedPin.success,
      )
    } else {
      return null
    }
  }

  function handleClickNotification() {
    mixpanelKlkTrack('Click Notification', '')
    if (
      config.environment === 'mobile' &&
      notificationData.cta_url === 'redemption_approval'
    ) {
      // handle click notification if notification is from redemption message
      approvalCourseAction()
    } else {
      const modUrl =
        notificationData.cta_page_type &&
        notificationData.cta_page_type === 'PCP' &&
        notificationData.url
          ? notificationData.url
          : notificationData.cta_url
      let newUrl = ItmGenerator(
        'inbox-promo-inbox',
        modUrl,
        notificationData.name,
        notificationData.name,
        '',
        false,
      )

      // mixpanelTrack('Click Notification', {
      //   'Notification Type': notificationData?.cta_page_type || 'None',
      //   'Notification Name': notificationData?.name || 'None'
      // })

      if (type === 'info') {
        // the cta_url give the url for the guest order-detail version, so we need to reconstruct the link to the my-account
        if (notificationData.url_key && notificationData.url_key !== '') {
          const splitUrlKey = notificationData.url_key.split(',')
          if (splitUrlKey && splitUrlKey.length > 1) {
            const isOffline = splitUrlKey[1] === 'offline'
            // offline sample: UBV.01.20240710.1,offline,HCI
            const orderId = splitUrlKey[0]
            const email = splitUrlKey[1]
            const bu = splitUrlKey[2]
            let url = ''
            if (config.environment === 'desktop') {
              if (isOffline) {
                url =
                  config.baseURL +
                  'my-account?tab=installation&child_tab=detail&type=offline&bu=' +
                  bu +
                  '&receipt_id=' +
                  orderId
              } else {
                url =
                  config.baseURL +
                  'my-account?tab=my-order-detail&orderId=' +
                  orderId +
                  '&email=' +
                  email
              }

              newUrl = ItmGenerator(
                'inbox-promo-inbox',
                url,
                orderId,
                orderId,
                false,
              )
            } else {
              newUrl = ItmGenerator(
                'inbox-promo-inbox',
                notificationData.cta_url,
                orderId,
                orderId,
                false,
              )
            }
          }
        }
      }
      if (notificationData.cta_url) {
        window.open(newUrl, '_blank')
      }
    }
    handleAfterClick()
  }

  function handleShowModal(e) {
    e.stopPropagation()
    setShowModal(true)
    handleAfterClick()
  }

  const htmlModalBody = (functionInsideModal) => (
    <HtmlModal
      onClose={functionInsideModal}
      content={notificationData.syarat_ketentuan}
      title='Syarat & Ketentuan'
    />
  )

  return (
    <>
      <div
        className={`notification-container ${
          !isRead ? 'notification-unread' : ''
        }`}
        onClick={() => handleClickNotification()}
      >
        <div className='title'>
          {notificationData.title ? notificationData.title : ''}
        </div>
        {notificationData.content_exist &&
        notificationData.content_exist > 0 &&
        notificationData.content_image ? (
          <div className='image-content'>
            <img
              src={ConvertImageType(notificationData.content_image)}
              alt='thumbnail-notif'
            />
          </div>
        ) : null}
        {notificationData.content_exist &&
        notificationData.content_exist > 0 &&
        notificationData.voucher_code ? (
          <VoucherDot
            code={notificationData.voucher_code}
            afterClick={handleAfterClick}
          />
        ) : null}
        <div className='ui-text-3 margin-top-xs'>
          {notificationData.subtitle ? notificationData.subtitle : ''}
        </div>
        {notificationData.syarat_ketentuan_status &&
        notificationData.content_exist &&
        notificationData.syarat_ketentuan_status > 0 &&
        notificationData.content_exist > 0 ? (
          <div className='notification-view-tnc margin-top-s'>
            <a href='#' onClick={handleShowModal}>
              Lihat Syarat dan Ketentuan
            </a>
          </div>
        ) : null}
      </div>

      {showModal && (
        <Modal
          bodyClass='modal-body-html modal-body-html__tnc-floating-voucher'
          contentClass='modal-content-html__tnc-floating-voucher'
          dialogClass='modal-dialog-html__tnc-floating-voucher'
          bodyElement={htmlModalBody}
          onClose={() => setShowModal(false)}
          show={showModal}
        />
      )}
    </>
  )
}
