import React, { useEffect, useState } from 'react'
import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'
import Skeleton from 'react-loading-skeleton'
import { useCartAuthContext } from '../../../context/CartAuthContext'
import config from '../../../../../config'
import Button from '../../../layouts/atoms/Button'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { useGetInbox } from '../../../services/api/queries/global'
import { useMarkInboxAsRead } from '../../../services/api/mutations/global'
import TryParseJSONObject from '../../../utils/TryParseJSONObject'
import InboxNotification from './InboxNotification'

export default function NotificationTabContent({ type }) {
  const { state: cartAuthContext } = useCartAuthContext()
  const [readNotifications, setReadNotifications] = useState([])
  const [displayedNotifications, setDisplayedNotifications] = useState([])
  const [page, setPage] = useState(1)
  const { data, isLoading } = useGetInbox({ type, page })
  const { mutate: markInboxAsRead } = useMarkInboxAsRead()

  useEffect(() => {
    setReadNotifications(getCookieReadNotifications())
  }, [])

  useEffect(() => {
    if (!data || isEmpty(data?.inbox)) {
      return
    }
    setDisplayedNotifications((prev) => [...prev, ...data.inbox])
  }, [data])

  // function updateReadNotifications(notifications_) {
  //   if (!notifications_) return
  //   const newReadNotifications = []
  //   notifications_.forEach((notification) => {
  //     if (notification.read_receipt === 1)
  //       newReadNotifications.push(notification.id)
  //   })
  //   setReadNotifications((readNotifications_) => [
  //     ...readNotifications_,
  //     ...newReadNotifications,
  //   ])
  // }

  const cookieName = 'inbox_read_' + type
  function getCookieReadNotifications() {
    let inboxNotificationRead = Cookies.get(cookieName)
    if (inboxNotificationRead) {
      inboxNotificationRead = TryParseJSONObject(inboxNotificationRead)
    }
    return inboxNotificationRead || []
  }

  async function markAsRead(id) {
    const inboxNotificationRead = getCookieReadNotifications()
    if (!inboxNotificationRead.includes(id)) {
      inboxNotificationRead.push(id)
    }
    Cookies.set(cookieName, JSON.stringify(inboxNotificationRead), {
      expires: 300,
    })
    setReadNotifications((readNotifications_) => [...readNotifications_, id])

    markInboxAsRead({ inbox_list: [{ inbox_id: id, type }] })
  }

  async function markAllAsRead(e) {
    e.preventDefault()

    // mixpanelTrack('Tandai semua dibaca', {
    //   'Click Location': `Tab ${type}`
    // })

    const unreadNotifications = []
    const inboxList = []
    displayedNotifications.forEach((notification) => {
      if (!readNotifications.includes(notification.id)) {
        unreadNotifications.push(notification.id)
        inboxList.push({ inbox_id: notification.id, type })
      }
    })

    let inboxNotificationRead = getCookieReadNotifications()
    inboxNotificationRead = [...inboxNotificationRead, ...unreadNotifications]
    Cookies.set(cookieName, JSON.stringify(inboxNotificationRead), {
      expires: 300,
    })

    setReadNotifications((readNotifications_) => [
      ...readNotifications_,
      ...unreadNotifications,
    ])

    markInboxAsRead({ inbox_list: inboxList })
  }

  // function mixpanelClickShowMore () {
  //   mixpanelTrack('Lihat lebih banyak inbox', {
  //     'Click Location': `Tab ${type}`
  //   })
  // }

  function renderLoadMoreButton() {
    if (isLoading) {
      return <>Silakan menunggu...</>
    } else {
      const buttonSize = config.environment === 'desktop' ? 'big' : 'small'
      const buttonType =
        config.environment === 'desktop' ? 'secondary-border' : 'ghost-black'
      if (type === 'info') {
        if (config.environment === 'desktop') {
          return (
            <a
              href={`${config.baseURL}my-account?tab=my-order-list`}
              onClick={() => {
                // mixpanelClickShowMore()
                setPage((prev) => prev + 1)
              }}
            >
              <Button size={buttonSize} type={buttonType}>
                Lihat Lebih Banyak
              </Button>
            </a>
          )
        } else if (config.environment === 'mobile') {
          return (
            <Button
              size={buttonSize}
              type={buttonType}
              handleOnClick={() => {
                // mixpanelClickShowMore()
                setPage((prev) => prev + 1)
              }}
            >
              Lihat Lebih Banyak
            </Button>
          )
        }
      } else {
        return (
          <Button
            size={buttonSize}
            type={buttonType}
            handleOnClick={() => {
              // mixpanelClickShowMore()
              setPage((prev) => prev + 1)
            }}
          >
            Lihat Lebih Banyak
          </Button>
        )
      }
    }
  }

  if (
    isLoading ||
    (displayedNotifications && !isEmpty(displayedNotifications))
  ) {
    const isLoggedIn = cartAuthContext?.auth && !isEmpty(cartAuthContext?.auth)
    return (
      <>
        <div className='notification-list'>
          <div
            className={config.environment === 'mobile' ? 'body-scroll' : ''}
            style={
              config.environment === 'mobile' ? { paddingBottom: '3em' } : {}
            }
          >
            {displayedNotifications &&
              !isEmpty(displayedNotifications) &&
              displayedNotifications.map((notificationData) => (
                <InboxNotification
                  key={notificationData.id}
                  notificationData={notificationData}
                  isRead={
                    (readNotifications &&
                      readNotifications.includes(notificationData.id)) ||
                    notificationData.read_receipt === 1
                  }
                  markAsRead={markAsRead}
                  type={type}
                />
              ))}
            <div
              className={
                config.environment === 'mobile'
                  ? 'padding-top-s padding__horizontal--m'
                  : 'padding-s'
              }
            >
              {!isEmpty(data?.inbox) && renderLoadMoreButton()}
            </div>
          </div>
        </div>
        {isLoggedIn &&
          displayedNotifications &&
          !isEmpty(displayedNotifications) &&
          (config.environment === 'desktop' ? (
            <div className='notification-mark-read padding-s'>
              <a href='#' onClick={markAllAsRead}>
                Tandai Semua dibaca
              </a>
            </div>
          ) : (
            <div className='button-inbox-bottom'>
              <div className='margin-top-s padding__horizontal--m'>
                <Button
                  size='medium'
                  type='primary'
                  width='100%'
                  isSubmit
                  handleOnClick={(e) => markAllAsRead(e)}
                >
                  Tandai semua dibaca
                </Button>
              </div>
            </div>
          ))}
      </>
    )
  } else {
    return (
      <div className='notification-empty padding-xxl'>
        <CustomLazyLoadImage
          src={config.assetsURL + 'images/no-inbox.svg'}
          height={116}
          width={161}
          alt='Inbox empty'
          placeholder={<Skeleton height={116} width={161} />}
        />
        <div className='padding-top-m'>
          Kamu belum mempunyai transaksi, Belanja yuk!
        </div>
      </div>
    )
  }
}
