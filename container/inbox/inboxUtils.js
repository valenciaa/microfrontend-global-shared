import config from '../../../../config'
import GetParam from '../../utils/GetParam'

function handleSnackbar(router, setCurrentTab, setInfoboxMsg) {
  const action = GetParam('action')
  if (action === 'reset-with-otp' || action === 'reset-password') {
    let wording
    // original query params for action 'reset-with-otp' => ?action=reset-with-otp&from=inbox&tab=${tab}
    // original query params for action 'reset-password' => ?action=reset-password&from=inbox&tab=${tab}
    // tab value for now is always => info
    // setVoucherDetail for now only used in mobile web
    const tab = GetParam('tab')
    setCurrentTab(tab)
    if (action === 'reset-with-otp') {
      wording = 'PIN berhasil disimpan'
    } else if (action === 'reset-password') {
      wording = 'Kata sandi berhasil diubah'
    }
    setInfoboxMsg(wording)
    router.push(config.baseURL + 'inbox', undefined, { shallow: true })
  }
}

export { handleSnackbar }
