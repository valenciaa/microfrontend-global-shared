import localforage from 'localforage'
import config from '../../../../config'
import GetParam from '../../utils/GetParam'
import isEmpty from 'lodash/isEmpty'

function voucherCatalogMutate(currentVoucherCatalog, newVoucherCatalog) {
  // currentVoucherCatalog means the vouchers we rendered now
  // newVoucherCatalog means new vouchers response from api that we just get
  let voucherCatalogLength = 0
  if (currentVoucherCatalog?.length === 0) {
    // we need to count of how much current data rendered now for passing value to props dataLength for react-infinite-scroll-component
    voucherCatalogLength += newVoucherCatalog?.vouchers?.length

    return {
      data: newVoucherCatalog?.vouchers || [],
      voucherCatalogLength,
    }
  } else {
    let finalData = [...currentVoucherCatalog]
    if (
      !Array.isArray(newVoucherCatalog) &&
      !isEmpty(newVoucherCatalog.vouchers)
    ) {
      finalData = [...finalData, ...newVoucherCatalog.vouchers] // else newVoucherCatalog is object and it means that there are no more vouchers data
    }

    // we need to count of how much current data rendered now for passing value to props dataLength for react-infinite-scroll-component
    voucherCatalogLength += finalData?.length

    return {
      data: finalData,
      voucherCatalogLength,
    }
  }
}

async function handleSnackbar(
  defaultHeaderHandler,
  router,
  setTab,
  setInfoboxMsg,
  setVoucherDetail,
) {
  const action = GetParam('action')
  if (action === 'reset-with-otp' || action === 'reset-password') {
    let wording
    // original query params for action 'reset-with-otp' => ?action=reset-with-otp&from=redemption&tab=${tab}
    // original query params for action 'reset-password' => ?action=reset-password&from=redemption&tab=${tab}
    // tab value could be voucherRedemption or shopRedemption and for now only used in mobile web
    const tab = GetParam('tab')
    if (tab === 'voucherRedemption' || tab === null) {
      // null means from desktop
      const currentAvailableVoucher = await localforage.getItem(
        'lastAvailableVouchers',
      )
      if (config.environment === 'mobile') {
        defaultHeaderHandler.setHeaderType('voucher-detail')
      }
      setVoucherDetail(JSON.parse(currentAvailableVoucher)) // set voucher detail value to change the render immediately to voucher detail render
    }
    if (config.environment === 'mobile') {
      setTab(tab)
    }
    if (action === 'reset-with-otp') {
      wording = 'PIN berhasil disimpan'
    } else if (action === 'reset-password') {
      wording = 'Kata sandi berhasil diubah'
    }
    setInfoboxMsg(wording)
    if (config.environment === 'mobile') {
      router.push(config.baseURL + 'klk/redemption', undefined, {
        shallow: true,
      })
    }
  }
}

export { voucherCatalogMutate, handleSnackbar }
