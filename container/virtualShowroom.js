import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import { GetUniqueId, ItmConverter, ItmGenerator } from '../utils'
// import { addToCart } from '../container/cart.js'
import { mixpanelTrack } from '../utils/MixpanelWrapper'
// import { CheckCartItem } from '../utils/CheckCartItem'
import { micrositeItmGenerator } from '../utils/micrositeItmGenerator'
import {
  addToCartDataLayer,
  productDetailDatalayer,
} from '../utils/DataLayerGoogle'
import { internalTrackerData } from '../utils/InternalTracking'
import { GetAuthData } from '../../../src/utils/misc/GetAuthData'
import { addToCartv3Variant } from '../utils/AddToCartv3'

export const getUrl = (name, urlKey, type, pageType, sku, urlMicrosite) => {
  const url = `${urlKey}`

  /* eslint-disable no-useless-escape */
  const title = ItmConverter(name).replace(/[^0-9A-Za-z\-]/g, '')
  let itm = ''
  const company = config?.companyNameCSS

  if (type === 'category') {
    if (!isEmpty(urlMicrosite)) {
      itm = 'shop-the-look-card-Microsite'
    } else {
      itm = 'shop-the-look-card-' + company
    }
  } else if (type === 'button') {
    if (!isEmpty(urlMicrosite)) {
      itm = 'shop-the-look-button-Microsite'
    } else {
      itm = 'shop-the-look-button-' + company
    }
  } else {
    if (!isEmpty(urlMicrosite)) {
      itm = 'shop-the-look-Microsite'
    } else {
      itm = 'shop-the-look-' + company
    }
  }

  let newUrl
  if (!isEmpty(urlMicrosite) || urlMicrosite !== 'Homepage') {
    newUrl =
      type === 'button'
        ? micrositeItmGenerator(itm, urlMicrosite, url, '', title)
        : micrositeItmGenerator(itm, urlMicrosite, url, sku, title, true)
  } else {
    newUrl = ItmGenerator(
      itm,
      url,
      'shop-the-look-' + pageType + '-' + title,
      '',
      sku,
    )
  }

  return newUrl
}

export const handleCart = async (
  e,
  device,
  product,
  storeCode,
  tabValue,
  cartAuthContext,
  addMinicartItem,
  location,
  setToggleErrorClipboardInfobox,
  setToggleClipboardInfobox,
  setIsClickProduct,
  setErrMessage,
) => {
  e.preventDefault()
  const uuid = await GetUniqueId()
  const user = await GetAuthData()

  let activeVariant = []
  let quantity = 1
  let data = {}

  if (product?.multivariants?.attributes?.top) {
    activeVariant =
      product?.variants?.[product?.multivariants?.default_selected_sku_index]
  } else if (product?.variants) {
    activeVariant = product?.variants?.[0]
  }

  if (product?.is_apply_multiple) {
    quantity = product?.minimum_order
  }

  data = await addToCartv3Variant(
    activeVariant,
    'shopTheLook',
    quantity,
    storeCode,
    'delivery',
    product,
    cartAuthContext?.auth,
    location,
  )

  if (device === 'mobile') {
    let setValue = false
    if (location === 'PDP') {
      setValue = null
    }
    if (setIsClickProduct) {
      setIsClickProduct(setValue)
    }
    // remove overflow hidden on body element
    document.body.style.overflow = null
    document.body.classList.remove('modal-open')
  }
  if (!isEmpty(data)) {
    addToCartDataLayer({ ...product, ...activeVariant }, cartAuthContext)
    const addtoCart = [
      {
        ...activeVariant,
        trackBreadcrumb: product.trackBreadcrumb,
        name: product?.name,
        brand: product?.brand,
        store_available: product?.store_available,
      },
    ]

    productDetailDatalayer(addtoCart, 'add_to_cart')
    await addMinicartItem(data, {
      onSuccess: (res) => {
        if (res?.ok && !res?.data?.error) {
          setToggleClipboardInfobox(true)
          const mixpanelData = {
            'Section Name': tabValue?.title,
            'Click Location': 'Add To Cart Shop The Look',
            'Location': window.location.href.includes('/ms')
              ? 'Microsite'
              : 'Homepage',
          }

          if (location !== 'PDP') {
            mixpanelTrack('Shop The Look', mixpanelData)
          }

          internalTrackerData(
            'Shop The Look',
            { uuid: uuid, ...user },
            mixpanelData,
          )
        } else {
          setToggleErrorClipboardInfobox(true)
          if (res?.data?.error?.message) {
            setErrMessage(res.data.error.message)
          }
        }
      },
    })
  }
}
