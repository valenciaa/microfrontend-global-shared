import isEmpty from 'lodash/isEmpty'
import { useEffect } from 'react'
import config from '../../../config'
import { GetAuthDataB2b } from '../utils/GetAuthDataB2b'

export default function GuardContainer({
  children,
  isLoggedOut = false,
  isCheckCustomerStatus = false,
  isMustLogin = false,
}) {
  useEffect(() => {
    handleAuth()
  }, [])

  const handleAuth = async () => {
    const auth = await GetAuthDataB2b()

    if (isLoggedOut) {
      if (auth) {
        window.location.href = config.baseURL
      }
    }

    if (isMustLogin && !isCheckCustomerStatus) {
      if (!auth) {
        window.location.href = config.baseURL
      }
    }

    if (isCheckCustomerStatus) {
      if (isEmpty(auth) || !auth.email) {
        window.location.href = config.baseURL
      } else if (!isEmpty(auth)) {
        if (auth?.company_status && auth?.company_status !== 0) {
          window.location.href = config.baseURL
        }
      }
    }
  }
  return <>{children}</>
}
