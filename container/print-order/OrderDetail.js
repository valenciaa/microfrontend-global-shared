import React from 'react'
import { NumberWithCommas } from '../../utils'
import TableHeader from './components/Table/TableHeader'
import TableData from './components/Table/TableData'
import TableSubTotal from './components/Table/TableSubTotal'
import TableFooter from './components/Table/TableFooter'
import Separator from './components/Separator'
import Text from './components/Text'

export default function OrderDetail(props) {
  const {
    data,
    dataIndex,
    dataSubtotal,
    dataVoucherInv,
    dataServiceFee,
    isInstallationOrder,
  } = props

  const setVoucher = (data, index) => {
    const discountAmount = parseInt(data?.details?.[index]?.discount_amount)
    const giftCardsAmount = parseInt(data?.details?.[index]?.gift_cards_amount)
    const totalAmount = discountAmount + giftCardsAmount

    if (totalAmount > 0) {
      return `Rp${NumberWithCommas(totalAmount)}`
    }
    return 0
  }

  const dataHeader = data?.invoice_no
  const dataDetail = data?.details
  const dataFooter = {
    deliveryFee:
      data?.biaya_kirim > 0 && data?.total > 0
        ? `Rp${NumberWithCommas(data?.biaya_kirim)}`
        : 'Gratis',
    zonePriceDifferent: data?.biaya_layanan
      ? `Rp${NumberWithCommas(data?.biaya_layanan)}`
      : 'Gratis',
    total: `Rp${NumberWithCommas(data?.total)}`,
    voucher: dataHeader?.includes('INV')
      ? dataVoucherInv
      : setVoucher(data, dataIndex),
    serviceFee: dataServiceFee,
  }

  return (
    <div className='printOrder-order-detail'>
      <Header data={dataHeader} isInstallationOrder={isInstallationOrder} />
      <Detail
        data={dataDetail}
        dataSubtotal={dataSubtotal}
        isInstallationOrder={isInstallationOrder}
      />
      <Footer data={dataFooter} isInstallationOrder={isInstallationOrder} />
    </div>
  )
}

const Header = (props) => {
  const { data, isInstallationOrder } = props

  const invoiceNumber = data
  const title = isInstallationOrder ? 'Perakitan' : 'Pengiriman'

  return (
    <Text
      className='printOrder-order-detail__title'
      text={`${title} ${invoiceNumber}`}
      variant='bold'
    />
  )
}

const Detail = (props) => {
  const { data, dataSubtotal, isInstallationOrder } = props

  return (
    <div className='printOrder-order-detail__table'>
      <TableHeader isInstallationOrder={isInstallationOrder} />
      <Separator />
      <TableData data={data} />
      <TableSubTotal data={dataSubtotal} />
    </div>
  )
}

const Footer = (props) => {
  const { data, isInstallationOrder } = props

  return <TableFooter data={data} isInstallationOrder={isInstallationOrder} />
}
