import React from 'react'
import Text from './Text'

export default function Row(props) {
  const { className, color, size, title, value, variant } = props

  const textClassname = className || ''
  const textColor = color || ''
  const textSize = size || 'medium'
  const textVariant = variant || 'bold'

  const classes = `
    printOrder-row
    ${textClassname}
  `

  return (
    <div className={classes}>
      <Text size={textSize} text={title} variant={textVariant} />
      <Text
        color={textColor}
        size={textSize}
        text={value}
        variant={textVariant}
      />
    </div>
  )
}
