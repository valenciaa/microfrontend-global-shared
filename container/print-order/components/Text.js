import React from 'react'

export default function Text(props) {
  const { className, color, size, text, variant } = props

  const textClassName = className || ''
  const textColor = color || 'grey'
  const textSize = size || 'medium'
  const textVariant = variant || 'reguler'

  const classes = `
    printOrder-text
    printOrder-text__${textColor}
    printOrder-text__${textSize}
    printOrder-text__${textVariant}
    ${textClassName}
  `

  return <div className={classes}>{text}</div>
}
