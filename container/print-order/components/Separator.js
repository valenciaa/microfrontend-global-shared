import React from 'react'

export default function Separator(props) {
  const { type } = props

  const separatorType = type || 'line'

  const classes = `
    printOrder-separator
    printOrder-separator__${separatorType}
  `

  return <div className={classes} />
}
