import React from 'react'
import { NumberWithCommas } from '../../../../utils'
import Text from '../Text'
import Separator from '../Separator'

export default function TableData(props) {
  const { data } = props

  const sellingPrice = (index) => {
    const item = data[index]
    const firstConditionBundling = item?.normal_price !== item?.selling_price
    const secondConditionBundling =
      item?.package_id !== 0 && item?.normal_price !== `Rp ${item?.subtotal}`
    const isBundling = firstConditionBundling || secondConditionBundling

    return isBundling ? `Rp${item?.subtotal}` : item?.selling_price
  }

  const setPrice = (index) => {
    const item = data[index]
    const price = sellingPrice(index)

    if (item?.is_free_item === '1') {
      return 'Gratis'
    }
    return NumberWithCommas(price).replace(' ', '')
  }

  const setSubTotal = (index) => {
    const item = data[index]
    const price = sellingPrice(index)

    if (item?.is_free_item === '1') {
      return ''
    }
    if (price.indexOf('Rp') === -1) {
      return price
    }
    return `Rp${NumberWithCommas(price.substr(2) * item.qty_ordered)}`
  }

  return data.map((itm, idx) => (
    <div key={idx}>
      <div className='printOrder-table-data'>
        <Text text={itm?.name} />
        <Text text={`${itm?.qty_ordered} Pcs`} />
        <Text text={setPrice(idx)} />
        <Text text={setSubTotal(idx)} />
      </div>

      <div className='flex flex-col tw-gap-1 printOrder-table-data__note-wrapper'>
        <Note lable='Note Customer:' note={itm?.customer_note} />
        <Note lable='Note ruparupa:' note={itm?.internal_note} />
      </div>

      <Separator type='dashed' />
    </div>
  ))
}

const Note = (props) => {
  const { lable, note } = props

  if (!note) {
    return null
  }

  return (
    <div key={lable} className='flex tw-gap-1'>
      <Text
        className='printOrder-table-data__note-lable'
        size='small'
        text={lable}
      />

      <Text
        className='printOrder-table-data__note-text'
        size='small'
        text={note}
      />
    </div>
  )
}
