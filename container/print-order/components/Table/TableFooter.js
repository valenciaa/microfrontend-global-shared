import React from 'react'
import Row from '../Row'

export default function TableFooter(props) {
  const { data, isInstallationOrder } = props

  const deliveryFee = data?.deliveryFee
  const zonePriceDifferent = data?.zonePriceDifferent
  const serviceFee = data?.serviceFee
  const voucher = `- ${data?.voucher}`
  const total = data?.total

  const additionalField = [
    { title: 'Biaya Kirim', value: deliveryFee },
    { title: 'Perbedaan Harga Zona', value: zonePriceDifferent },
  ]

  return (
    <div className='printOrder-table-footer'>
      <AdditionalField hide={isInstallationOrder} list={additionalField} />

      <Row title='Biaya Layanan' value={serviceFee} />

      {data?.voucher !== 0 && (
        <Row color='red' title='Potongan Voucher' value={voucher} />
      )}

      <Row
        className='printOrder-table-footer__total'
        title='Total'
        value={total}
      />
    </div>
  )
}

const AdditionalField = (props) => {
  const { hide, list } = props

  if (hide) {
    return ''
  }

  return (
    <>
      {list.map((itm, idx) => (
        <Row key={idx} title={itm?.title} value={itm?.value} />
      ))}
    </>
  )
}
