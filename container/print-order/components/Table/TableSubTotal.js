import React from 'react'
import { NumberWithCommas } from '../../../../utils'
import Text from '../Text'

export default function TableSubTotal(props) {
  const { data } = props

  return (
    <div className='printOrder-table-subtotal'>
      <div className='printOrder-table-subtotal__data'>
        <Text text='Subtotal Harga' variant='bold' />
        <Text text={`Rp${NumberWithCommas(data)}`} variant='bold' />
      </div>
    </div>
  )
}
