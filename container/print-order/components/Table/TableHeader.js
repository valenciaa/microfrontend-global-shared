import React from 'react'
import Text from '../Text'

export default function TableHeader(props) {
  const { isInstallationOrder } = props

  return (
    <div className='printOrder-table-data'>
      <Text
        text={`Nama ${isInstallationOrder ? 'Jasa' : 'Produk'}`}
        variant='bold'
      />
      <Text text='Jumlah' variant='bold' />
      <Text text='Harga' variant='bold' />
      <Text text='Sub Total' variant='bold' />
    </div>
  )
}
