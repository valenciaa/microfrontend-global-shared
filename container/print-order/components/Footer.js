import React from 'react'
import config from '../../../../../config'
import { useRouter } from 'next/router'

export default function Footer(props) {
  const { email, orderNo } = props

  const router = useRouter()
  const query = router?.query
  const env = config?.environment
  const isDesktop = env === 'desktop'

  const isHideFooter = query?.hideFooter || 'false'

  const textClasses = `
  printOrder-footer__info
  printOrder-footer__info--${isDesktop ? 'desktop' : 'mobile'}
  printOrder-footer__info--${isHideFooter === 'false' ? 'show-button' : ''}
  `

  const handleClick = () => {
    router.push(
      config.baseURL +
        `my-account?tab=transaction&child_tab=detail&transaction_no=${orderNo}&type=online&email=${email}`,
      undefined,
      { shallow: true },
    )
  }

  return (
    <div className='printOrder-footer'>
      <div className={textClasses}>
        <img
          alt='Exclamation Icon'
          className='printOrder-footer__info-icon'
          src={config.assetsURL + 'icon/icon-exclamation-white.svg'}
        />
        <div className='printOrder-footer__info-text'>
          Tunggu 5 detik untuk mencetak, jika tidak terjadi apa-apa tolong
          refresh browser anda.
        </div>
      </div>
      {isHideFooter === 'false' && (
        <div className='printOrder-footer__button' onClick={handleClick}>
          <div className='printOrder-footer__button-text'>
            Kembali ke Pesanan Saya
          </div>
        </div>
      )}
    </div>
  )
}
