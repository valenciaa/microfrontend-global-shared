import React from 'react'
import Text from '../Text'

export default function HeaderNumber(props) {
  const { data } = props

  return (
    <div className='printOrder-header-number'>
      <Text
        className='printOrder-header-number__title'
        size='large'
        text='Nomor Pesanan :'
        variant='bold'
      />
      <Text color='ruparupa' size='large' text={data} variant='semibold' />
    </div>
  )
}
