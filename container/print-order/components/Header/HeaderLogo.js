import React from 'react'
import { LOGO_RUPARUPA_DESKTOP } from '../../../../utils/constants'

export default function HeaderLogo() {
  const logoSrc = LOGO_RUPARUPA_DESKTOP

  return (
    <div className='printOrder-header-logo'>
      <img alt='logo-ruparupa' src={logoSrc} />
    </div>
  )
}
