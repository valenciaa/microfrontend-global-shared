import React from 'react'

export default function HeaderDetail() {
  const logoSrc =
    'https://res.cloudinary.com/ruparupa-com/image/upload/v1556514848/2.1/paid-stamp-invoice.png'

  return (
    <div className='printOrder-header-detail'>
      <img className='printOrder-header-detail__image' src={logoSrc} />
    </div>
  )
}
