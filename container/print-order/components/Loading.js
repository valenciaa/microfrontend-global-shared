import React from 'react'

export default function Loading(props) {
  const { text } = props

  return (
    <div className='printOrder-loading'>
      <div className='printOrder-loading__text'>{text}</div>
    </div>
  )
}
