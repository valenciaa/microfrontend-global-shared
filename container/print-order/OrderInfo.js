import React from 'react'
import isEmpty from 'lodash/isEmpty'
import FormatDate from '../../utils/FormatDate'
import Text from './components/Text'

export default function OrderInfo(props) {
  const { data, isInstallationOrder } = props

  const infoLists = () => {
    let info = [
      {
        label: 'Tgl Pembayaran : ',
        value: data?.infoPaymentDate?.length
          ? FormatDate(data?.infoPaymentDate)
          : '-',
        isLabelBold: true,
        isBold: false,
      },
      {
        label: 'Pembayaran :',
        value: data?.infoPaymentMethod,
        isLabelBold: true,
        isBold: true,
      },
      {
        ...(data?.infoInstallment > 0
          ? {
              isLabelBold: false,
              label: `Cicilan ${data.infoInstallment} bulan`,
            }
          : {}),
      },
    ]

    if (!isInstallationOrder) {
      info = [
        {
          label: setLabelPickup()?.label,
          value: setLabelPickup()?.value,
          isBold: true,
        },
        ...info,
      ]
    }

    return info
  }

  const setLabelPickup = () => {
    if (data?.infoPickupLocation && data?.deliveryMethod === 'pickup') {
      return { label: 'Diambil di :', value: data?.infoPickupLocation }
    }
    if (data?.infoPickupLocation && data?.deliveryMethod !== 'pickup') {
      return { label: 'Dari :', value: data?.infoPickupLocation }
    }
    return { label: 'Dari DC Warehouse', value: '' }
  }

  return (
    <div className='printOrder-order-info'>
      <div className='printOrder-order-info__wrapper'>
        {infoLists().map((itm, idx) => (
          <div className='printOrder-order-info__section' key={idx}>
            <Text text={itm?.label} variant={itm?.isLabelBold ? 'bold' : ''} />
            <Text text={itm?.value} variant={itm?.isBold ? 'bold' : ''} />
          </div>
        ))}
      </div>
      <UserInfo
        data={data?.infoAddress}
        isInstallationOrder={isInstallationOrder}
        isShow={data?.deliveryMethod !== 'pickup'}
      />
    </div>
  )
}

const UserInfo = (props) => {
  const { data, isInstallationOrder, isShow } = props

  const capitalizeFirstLetter = (str) => {
    return str?.charAt(0)?.toUpperCase() + str?.slice(1)?.toLowerCase()
  }

  const addressName = data?.address_name
  const firstName = capitalizeFirstLetter(data?.first_name)
  const lastName = capitalizeFirstLetter(data?.last_name)
  const phone = data?.phone
  const fullAddress = capitalizeFirstLetter(data?.full_address)
  const subDistrict = capitalizeFirstLetter(data?.kecamatan?.kecamatan_name)
  const city = capitalizeFirstLetter(data?.city?.city_name)
  const province = data?.province?.province_name
  const postCode = data?.post_code

  if (isEmpty(data) || !isShow) {
    return ''
  }

  return (
    <div className='printOrder-order-info__wrapper'>
      <Text
        size='extra-large'
        text={isInstallationOrder ? 'Perakitan untuk :' : 'Dikirim ke :'}
        variant='bold'
      />
      <Text size='extra-large' text={addressName} variant='bold' />
      <Text text={`${firstName} ${lastName} - ${phone}`} />
      <Text text={fullAddress} />
      <Text text={`${subDistrict}, ${city}`} />
      <Text text={`${province}, ${postCode}`} />
    </div>
  )
}
