import React from 'react'
import { NumberWithCommas } from '../../utils'
import HeaderDetail from './components/Header/HeaderDetail'
import HeaderLogo from './components/Header/HeaderLogo'
import HeaderNumber from './components/Header/HeaderNumber'
import OrderDetail from './OrderDetail'
import OrderInfo from './OrderInfo'
import Row from './components/Row'
import Separator from './components/Separator'

export default function PrintOrder(props) {
  const {
    className,
    dataHeaderNumber,
    dataOrderDetail,
    dataGrandTotal,
    dataVoucherInv,
    dataServiceFee,
    isInstallationOrder,
    setDataOrderInfo,
    setDataSubtotal,
  } = props

  return (
    <div className={className}>
      <div className='printOrder__header'>
        <HeaderLogo />
        <HeaderNumber data={dataHeaderNumber} />
      </div>
      <div className='printOrder__content'>
        {dataOrderDetail?.map((data, idx) => (
          <div className='printOrder__detail' key={idx}>
            <HeaderDetail />
            <OrderInfo
              data={setDataOrderInfo(data, idx)}
              isInstallationOrder={isInstallationOrder}
            />
            <Separator />
            <OrderDetail
              data={data}
              dataSubtotal={setDataSubtotal(idx)}
              dataVoucherInv={dataVoucherInv}
              dataServiceFee={dataServiceFee}
              dataIndex={idx}
              isInstallationOrder={isInstallationOrder}
            />
          </div>
        ))}
      </div>
      <div className='printOrder__grand-total'>
        <Row
          size='large'
          title='Grand Total'
          value={`Rp${NumberWithCommas(dataGrandTotal)}`}
        />
      </div>
    </div>
  )
}
