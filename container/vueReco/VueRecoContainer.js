import { useVueContext } from '../../context/VueContext'

import SpecialRecommendationWrapper from '../../../global/modules/homepage/containers/SpecialRecommendationWrapper'

import CategoryRecommend from '../../../../src/homepage/containers/CategoryRecommend'
import { useEffect, useState } from 'react'
import VueAiRecommendation from '../../../../src/pcp/components/VueAiRecommendation'
import config from '../../../../config'
import isEmpty from 'lodash/isEmpty'

export default function VueRecoContainer() {
  // the reco data is get from previous context
  const vueContext = useVueContext()
  const { vueRecommedation = {} } = vueContext || {}

  if (!vueRecommedation?.data_module) {
    return
  }

  return vueRecommedation?.data_module?.map((recoData, recoIndex) => {
    const { module_name } = recoData

    if (module_name?.toLowerCase()?.includes('categories')) {
      return (
        <CategoryRecommend
          pageFrom={'404'}
          useVue
          key={recoIndex}
          location={'page-not-found'}
          dataVueReco={recoData || null}
        />
      )
    }

    return (
      <SpecialRecommendationWrapper
        key={recoIndex}
        location='page-not-found'
        pageFrom='404'
        useVue
      />
    )
  })
}

export function RecommendationTemplate({
  location,
  template,
  micrositeName = '',
}) {
  // the reco data is get from previous context
  const vueContext = useVueContext()
  const { vueRecommedation = {}, setRecoContext = () => {} } = vueContext || {}
  vueContext.micrositeName = micrositeName || ''

  const [dataReco, setDataReco] = useState(null)

  useEffect(() => {
    if (
      location === 'microsite' &&
      template?.current_schedule?.vue_attributes
    ) {
      let tahuSlotPosition = Number(
        template?.current_schedule?.vue_attributes?.slotId,
      )

      if (vueRecommedation?.data_module && tahuSlotPosition) {
        setDataReco(vueRecommedation?.data_module?.[tahuSlotPosition - 1])
      }
    }
  }, [vueRecommedation])

  useEffect(() => {
    if (!isEmpty(dataReco) && location === 'microsite') {
      // this fucntion for recommendationView in Microsite
      const { correlationID: correlationId = '' } = vueContext || {}

      if (correlationId) {
        const dataChecker = dataReco?.module_name
        if (isEmpty(dataChecker)) {
          return
        }
        setRecoContext((prevContext) => ({
          ...prevContext,
          tahuModule: {
            pageFrom: 'microsite',
            module: dataReco,
          },
        }))
      }
    }
  }, [dataReco])

  if (isEmpty(dataReco)) {
    return
  }

  if (dataReco?.module_name?.toLowerCase()?.includes('categories')) {
    return (
      <CategoryRecommend
        pageFrom='microsite'
        location='microsite'
        dataVueReco={dataReco || {}}
        micrositeName={micrositeName || ''}
      />
    )
  }

  const recoPosition =
    template?.current_schedule?.[
      `position_${config?.environment?.toLowerCase()}`
    ]?.toLowerCase()

  return (
    <>
      <VueAiRecommendation
        title='Rekomendasi Spesial Untukmu'
        location='tahu'
        dataReco={dataReco}
        position={recoPosition}
      />
    </>
  )
}
