import React, { useState } from 'react'
import Help from '../../../src/help/Help'

export default function HelpWrapper({ pageFrom }) {
  const [render, setRender] = useState('')
  const [searchBarOverlay, setSearchBarOverlay] = useState(false)
  const [faqSearch, setFaqSearch] = useState('')
  const [likeAction, setLikeAction] = useState(false)
  const [dislikeAction, setDislikeAction] = useState(false)
  return (
    <Help
      pageFrom={pageFrom}
      render={render}
      setRender={setRender}
      searchBarOverlay={searchBarOverlay}
      setSearchBarOverlay={setSearchBarOverlay}
      faqSearch={faqSearch}
      setFaqSearch={setFaqSearch}
      likeAction={likeAction}
      setLikeAction={setLikeAction}
      dislikeAction={dislikeAction}
      setDislikeAction={setDislikeAction}
    />
  )
}
