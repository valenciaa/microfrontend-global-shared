import dynamic from 'next/dynamic'
import { useGetActivateTactical } from '../../../shared/global/services/api/queries/global'
import isEmpty from 'lodash/isEmpty'

const TahuNotFound = dynamic(
  () => import('../../../src/tahu/containers/TahuNotFound'),
)

export default function GuardTahuContainer({ children }) {
  const { data: dataTactical } = useGetActivateTactical()

  if (!isEmpty(dataTactical) && dataTactical === null) {
    return <TahuNotFound />
  } else {
    return <>{children}</>
  }
}
