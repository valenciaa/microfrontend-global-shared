import { useEffect, useState } from 'react'
import ComponentWrapper from '../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../layouts/moleculs/ErrorBoundary'
import { useInView } from 'react-intersection-observer'
import VirtualShowComponent from '../tw-components/pdp/Components/VirtualShowComponent'

export default function PdpVirtualShowroomWrapper({
  configuration,
  setShareProductData = {},
  showShareModal = false,
  setShowShareModal,
  isLoading = false,
  virtualShowData = null,
}) {
  const [virtualShowRoomView, setVirtualShowRoomView] = useState(false)

  const [refInspirationPromoBrand, inViewInspirationPromoBrand] = useInView({
    ...configuration,
  })

  useEffect(() => {
    if (inViewInspirationPromoBrand) {
      setVirtualShowRoomView(true)
    }
  }, [inViewInspirationPromoBrand])

  return (
    <ErrorBoundary>
      {virtualShowRoomView ? (
        <div id='vsr-tour-1'>
          <VirtualShowComponent
            showroomData={virtualShowData}
            isLoading={isLoading}
            showShareModal={showShareModal}
            setShareProductData={setShareProductData}
            setShowShareModal={setShowShareModal}
          />
        </div>
      ) : (
        <div id='vsr-tour-1' className='tw-min-h-[35em] tw-w-full'>
          <ComponentWrapper ref={refInspirationPromoBrand} height={522} />
        </div>
      )}
    </ErrorBoundary>
  )
}
