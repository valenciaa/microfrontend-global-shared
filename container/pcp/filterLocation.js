/**
 * 1. Menampung object 'lokasi' yang dipilih pada variable tmp
 *    yang selanjutnya akan disisipkan pada 'dataList' yang merupakan data 5 rekomendasi
 * 2. Melakukan pengecekan apakah 'lokasi' yg dipilih tidak ada pada 'dataList'
 * 3. Jika tidak ada, akan dilakukan pengecekan secara manual terhadap 'dataList' dari index terakhir ke index pertama
 * 4. Apabila item pada 'dataList' tidak ada pada 'lokasi' yang dipilih, maka item tersebut akan diremove (karena max length yang ditampilkan hanya 5)
 * 5. Kemudian, 'lokasi' yg dipilih dimasukkan pada 'dataList' di index pertama
 *
 * Case:
 * tmp; [F, E]
 * dataList: [A, B, C, D, E]
 *
 * Goal:
 * dataList: [F, A, B, C, E]
 */

const handleSelectionLocation = (locationList, value, dataList) => {
  const tmp = []
  Object.entries(value).forEach(([key]) => {
    const item = locationList.find((d) => d.value === key)
    if (item) {
      tmp.push({ ...item, is_active: true })
    }
  })

  tmp.forEach((selectedLocation) => {
    const item = dataList.find((d) => d.value === selectedLocation.value)
    if (!item) {
      for (let i = dataList.length - 1; i >= 0; i--) {
        if (!dataList.is_active) {
          dataList.splice(i, 1)
          dataList.unshift(selectedLocation)
          break
        }
      }
    }
  })

  return dataList
}

const RECOMMENDATION_LOCATION = [
  {
    city_id: '',
    city_name: '',
    label: 'DKI Jakarta',
    province_id: '490',
    province_name: 'DKI JAKARTA',
    status: '1',
    value: '490',
    is_active: false,
    type: 'province',
  },
  {
    city_id: '910',
    city_name: 'KOTA. TANGERANG',
    label: 'Kota. Tangerang',
    province_id: '487',
    province_name: 'BANTEN',
    status: '1',
    value: '910',
    is_active: false,
    type: 'city',
  },
  {
    city_id: '513',
    city_name: 'KOTA. BANDUNG',
    label: 'Kota. Bandung',
    province_id: '493',
    province_name: 'JAWA BARAT',
    status: '1',
    value: '513',
    is_active: false,
    type: 'city',
  },
  {
    city_id: '738',
    city_name: 'KOTA. MEDAN',
    label: 'Kota. Medan',
    province_id: '517',
    province_name: 'SUMATERA UTARA',
    status: '1',
    value: '738',
    is_active: false,
    type: 'city',
  },
  {
    city_id: '907',
    city_name: 'KOTA. SURABAYA',
    label: 'Kota. Surabaya',
    province_id: '495',
    province_name: 'JAWA TIMUR',
    status: '1',
    value: '907',
    is_active: false,
    type: 'city',
  },
]

export { handleSelectionLocation, RECOMMENDATION_LOCATION }
