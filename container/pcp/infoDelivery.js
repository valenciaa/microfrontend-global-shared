export const INFO_DELIVERY = [
  {
    title: 'Delivery to Home - Regular',
    titleName: 'delivery-to-home-regular',
  },
  {
    title: 'Delivery to Home - Instant',
    titleName: 'delivery-to-home-instant',
  },
  {
    title: 'Instant Delivery',
    titleName: 'instant-delivery',
  },
  {
    title: 'Same Day Delivery',
    titleName: 'same-day-delivery',
  },
  {
    title: 'Syarat & Ketentuan Mendapatkan Gratis Ongkir GO-SEND',
    titleName: 'syarat-dan-ketentuan-mendapatkan-gratis-ongkir-go-send',
  },
]
