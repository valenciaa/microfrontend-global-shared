import { ACE_HARDWARE } from '../../utils/constants/AceConstants'

export const KEY_EXPRESS_DELIVERY = {
  sameDay: { label: 'sameDay', value: '1' },
  instant: { label: 'instant', value: '2' },
  reguler: { label: 'reguler', value: '3' },
  armadaToko: { label: 'armadaToko', value: '4' },
}

export const LIST_DELIVERY = {
  sameDay: {
    label: 'Same Day Delivery',
    desc: '(Gosend)',
    value: KEY_EXPRESS_DELIVERY.sameDay.label,
    value_id: KEY_EXPRESS_DELIVERY.sameDay.value,
  },
  instant: {
    label: 'Instant Delivery',
    desc: '(Gosend)',
    value: KEY_EXPRESS_DELIVERY.instant.label,
    value_id: KEY_EXPRESS_DELIVERY.instant.value,
  },
  reguler: {
    label: 'Reguler',
    desc: '(JNE Reguler, JTR, NCS)',
    value: KEY_EXPRESS_DELIVERY.reguler.label,
    value_id: KEY_EXPRESS_DELIVERY.reguler.value,
  },
  armadaToko: {
    label: 'Armada Toko',
    desc: `(${ACE_HARDWARE}, Informa)`,
    value: KEY_EXPRESS_DELIVERY.armadaToko.label,
    value_id: KEY_EXPRESS_DELIVERY.armadaToko.value,
  },
}

export const KEY_ADDRESS = {
  prov: 'prov',
  kab: 'kab',
  kec: 'kec',
}

export const LIST_ADDRESS = [
  {
    key: KEY_ADDRESS.prov,
    default: 'Pilih Provinsi',
    value: null,
  },
  {
    key: KEY_ADDRESS.kab,
    default: 'Pilih Kota',
    value: null,
  },
  {
    key: KEY_ADDRESS.kec,
    default: 'Pilih Kecamatan',
    value: null,
  },
]
