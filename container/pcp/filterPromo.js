import { capitalize } from '../../utils/String'

export const DEFAULT_PROMO = {
  ELASTIC: ['Buy_1_Get_1', 'Spesial_Online', 'New_Arrivals'],
  ALGOLIA: ['Buy 1 Get 1', 'Spesial Online', 'New Arrivals'],
  ELASTIC_CLEARANCE: ['Clearance_Sale', 'Near_Perfect'],
  ALGOLIA_CLEARANCE: ['Clearance Sale', 'Near-Perfect'],
}

export const GroupItems = {
  price: 'price',
  maxprice: 'maxprice',
  minprice: 'minprice',
  maximumPrice: 'maximumPrice',
  minimumPrice: 'minimumPrice',
  brand: 'brand',
  promo: 'promo',
  color: 'color',
  category: 'category',
  store: 'store',
  locations: 'locations',
  city_id: 'city_id',
  province_id: 'province_id',
  expressCourier: 'expressCourier',
}

export const handleRemovePromoFilter = (value, promo) => {
  const tmpValue = value.split(',')
  return promo.filter((b) => !tmpValue.includes(b))
}

export const handleTransformItemPromo = (itemsAlgolia) => {
  const tmpPromo = []
  let bestOfferValue = []
  let clearanceSaleValue = []
  const tmpBestOfferValue = []
  const tmpClearanceSaleValue = []
  let isBestOfferRefined = false
  let isClearanceSaleRefined = false

  itemsAlgolia.forEach((item) => {
    if (DEFAULT_PROMO.ALGOLIA.includes(item.label)) {
      tmpPromo.push(item)
    } else if (DEFAULT_PROMO.ALGOLIA_CLEARANCE.includes(item.label)) {
      if (item.isRefined) {
        isClearanceSaleRefined = true
      }
      clearanceSaleValue.push(...item.value)
      tmpClearanceSaleValue.push(item.label)
    } else {
      if (item.isRefined) {
        isBestOfferRefined = true
      }
      bestOfferValue.push(...item.value)
      tmpBestOfferValue.push(item.label)
    }
  })

  if (tmpClearanceSaleValue.length > 0) {
    clearanceSaleValue = [...new Set(clearanceSaleValue)]

    if (isClearanceSaleRefined) {
      clearanceSaleValue = clearanceSaleValue.filter(
        (item) => !tmpClearanceSaleValue.includes(item),
      )
    }

    tmpPromo.push({
      label: 'Clearance Sale',
      value: clearanceSaleValue,
      isRefined: isClearanceSaleRefined,
    })
  }

  if (tmpBestOfferValue.length > 0) {
    bestOfferValue = [...new Set(bestOfferValue)]

    if (isBestOfferRefined) {
      bestOfferValue = bestOfferValue.filter(
        (item) => !tmpBestOfferValue.includes(item),
      )
    }

    tmpPromo.push({
      label: 'Best Offer',
      value: bestOfferValue,
      isRefined: isBestOfferRefined,
    })
  }

  return tmpPromo
}

export const handleSelectedPromoFilterAlgolia = (filterPromo) => {
  const tmpPromo = []
  const clearanceSaleValue = []
  const bestOfferValue = []

  filterPromo.forEach((item) => {
    if (DEFAULT_PROMO.ALGOLIA.includes(item)) {
      tmpPromo.push({ text: item, value: item, group: GroupItems.promo })
    } else if (DEFAULT_PROMO.ALGOLIA_CLEARANCE.includes(item)) {
      clearanceSaleValue.push(item)
    } else {
      bestOfferValue.push(item)
    }
  })

  if (clearanceSaleValue.length > 0) {
    tmpPromo.push({
      text: 'Clearance Sale',
      value: clearanceSaleValue.join(','),
      group: GroupItems.promo,
    })
  }

  if (bestOfferValue.length > 0) {
    tmpPromo.push({
      text: 'Best Offer',
      value: bestOfferValue.join(','),
      group: GroupItems.promo,
    })
  }

  return tmpPromo
}

export const handleSelectedPromoFilter = (filterPromo, promoValue) => {
  const tmpPromo = []
  const clearanceSaleValue = []
  const bestOfferValue = []

  filterPromo.forEach((item) => {
    if (promoValue.includes(item.value_id)) {
      if (DEFAULT_PROMO.ELASTIC.includes(item.value_id)) {
        tmpPromo.push({
          text: capitalize(item.value),
          group: GroupItems.promo,
          value: item.value_id,
        })
      } else if (DEFAULT_PROMO.ELASTIC_CLEARANCE.includes(item.value_id)) {
        clearanceSaleValue.push(item.value_id)
      } else {
        bestOfferValue.push(item.value_id)
      }
    }
  })

  if (clearanceSaleValue.length > 0) {
    tmpPromo.push({
      text: 'Clearance Sale',
      group: GroupItems.promo,
      value: clearanceSaleValue.join(','),
    })
  }

  if (bestOfferValue.length > 0) {
    tmpPromo.push({
      text: 'Best Offer',
      group: GroupItems.promo,
      value: bestOfferValue.join(','),
    })
  }

  return tmpPromo
}
