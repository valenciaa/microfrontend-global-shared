import React from 'react'
import isEmpty from 'lodash/isEmpty'

export default function GroupingSku({ listSku = [] }) {
  return (
    <div className='row group-sku'>
      <div className='margin-s'>
        <div className='col-xs-12 padding-bottom-s padding-top-s'>
          <h4>
            {' '}
            Nih grouping SKU per-halaman yah. Jangan lupa hapus spasi! :){' '}
          </h4>
        </div>
        <div className='col-xs-12 padding-bottom-s'>
          {!isEmpty(listSku)
            ? listSku?.map((sku, idx) => {
                if (listSku.length !== idx + 1) {
                  return sku + ', '
                } else {
                  return sku
                }
              })
            : null}
        </div>
      </div>
    </div>
  )
}
