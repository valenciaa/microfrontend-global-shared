import { useQueryRules } from 'react-instantsearch-core'

const QueryRuleCustomData = ({ getBanner, redirect }) => {
  const { items } = useQueryRules()
  redirect(items)
  getBanner(items)
  return null
}

export default QueryRuleCustomData
