import config from '../../../../config'

export const newUrlFormat = (url, pageFrom) => {
  if (pageFrom === 'store') {
    return encodeURIComponent(
      'https://ruparupa.page.link/?link=' +
        config.baseURL +
        'store/' +
        encodeURIComponent(url),
    )
  }

  if (pageFrom === 'help') {
    return 'https://ruparupa.page.link/?link=' + config.baseURL + url
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return 'https://ruparupa.page.link/?link=' + config.baseURL + 'p/' + url
  }
  if (pageFrom === 'tahu') {
    return (
      'https://ruparupa.page.link/?link=' +
      config.baseURL +
      'ms/' +
      url.split('ms/').pop()
    )
  }
  if (pageFrom === 'catalog') {
    const prefix = url.includes('/c/') ? 'c/' : 'cc/'
    return url.includes('/brands/')
      ? 'https://ruparupa.page.link/?link=' +
          config.baseURL +
          prefix +
          '/brands/' +
          url.split('brands/').pop()
      : 'https://ruparupa.page.link/?link=' +
          config.baseURL +
          prefix +
          url.split(prefix).pop()
  }
  if (pageFrom === 'jual') {
    return (
      'https://ruparupa.page.link/?link=' +
      config.baseURL +
      'jual/' +
      url.split('jual/').pop()
    )
  }
}
const whatsappUrl = (url, title, pageFrom) => {
  if (pageFrom === 'store') {
    return (
      'https://api.whatsapp.com/send?text=' +
      encodeURIComponent(
        `Anda mungkin tertarik dengan produk ini: "${title}" `,
      ) +
      newUrlFormat(url, pageFrom)
    )
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return (
      'https://api.whatsapp.com/send?text=' +
      encodeURIComponent('Anda mungkin tertarik dengan produk ini ') +
      newUrlFormat(url, pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=sharing+button&itm_campaign=Whatsapp&itm_term=' + title,
        ),
      )
    )
  }
  if (pageFrom === 'help') {
    return (
      'https://api.whatsapp.com/send?text=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url, pageFrom)
    )
  }
  if (pageFrom === 'tahu') {
    return (
      'https://api.whatsapp.com/send?text=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share+microsite&itm_campaign=Whatsapp&itm_term=' + title,
        ),
      )
    )
  }
  if (['catalog', 'jual'].includes(pageFrom)) {
    return (
      'https://api.whatsapp.com/send?text=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share&itm_campaign=Whatsapp&itm_term=' + title,
        ),
      )
    )
  }
}
const telegramUrl = (url, title, pageFrom) => {
  if (['store', 'help'].includes(pageFrom)) {
    return (
      'https://telegram.me/share/url?url=' +
      newUrlFormat(url, pageFrom) +
      '&text=' +
      encodeURIComponent(title)
    )
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return (
      'https://telegram.me/share/url?url=' +
      newUrlFormat(url, pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=sharing+button&itm_campaign=Telegram&itm_term=' + title,
        ),
      ) +
      '&text=' +
      encodeURIComponent(title)
    )
  }
  if (pageFrom === 'tahu') {
    return (
      'https://telegram.me/share/url?url=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share+microsite&itm_campaign=Telegram&itm_term=' + title,
        ),
      ) +
      '&text=' +
      encodeURIComponent(title)
    )
  }
  if (['catalog', 'jual'].includes(pageFrom)) {
    return (
      'https://telegram.me/share/url?url=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share&itm_campaign=Telegram&itm_term=' + title,
        ),
      ) +
      '&text=' +
      encodeURIComponent(title)
    )
  }
}
const twitterUrl = (url, title, pageFrom) => {
  if (['store', 'help'].includes(pageFrom)) {
    return (
      'https://twitter.com/intent/tweet?url=' +
      newUrlFormat(url, pageFrom) +
      '&text=' +
      encodeURIComponent(title)
    )
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return (
      'https://twitter.com/intent/tweet?url=' +
      newUrlFormat(url, pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=sharing+button&itm_campaign=Twitter&itm_term=' + title,
        ),
      ) +
      '&text=' +
      encodeURIComponent(title)
    )
  }
  if (pageFrom === 'tahu') {
    return (
      'https://twitter.com/intent/tweet?url=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share+microsite&itm_campaign=Twitter&itm_term=' + title,
        ),
      ) +
      '&text=' +
      encodeURIComponent(title)
    )
  }
  if (['catalog', 'jual'].includes(pageFrom)) {
    return (
      'https://twitter.com/intent/tweet?url=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share&itm_campaign=Twitter&itm_term=' + title,
        ),
      ) +
      '&text=' +
      encodeURIComponent(title)
    )
  }
}
const facebookUrl = (url, title, pageFrom) => {
  if (['store', 'help'].includes(pageFrom)) {
    return (
      'https://www.facebook.com/sharer/sharer.php?u=' +
      newUrlFormat(url, pageFrom) +
      '&quote=' +
      encodeURIComponent(title)
    )
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return (
      'https://www.facebook.com/sharer/sharer.php?u=' +
      newUrlFormat(url, pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=sharing+button&itm_campaign=Facebook&itm_term=' + title,
        ),
      ) +
      '&quote=' +
      encodeURIComponent(title)
    )
  }
  if (pageFrom === 'tahu') {
    return (
      'https://www.facebook.com/sharer/sharer.php?u=' +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share+microsite&itm_campaign=Facebook&itm_term=' + title,
        ),
      ) +
      '&quote=' +
      encodeURIComponent(title)
    )
  }
  if (['catalog', 'jual'].includes(pageFrom)) {
    return (
      'https://www.facebook.com/sharer/sharer.php?u=' +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share&itm_campaign=Facebook&itm_term=' + title,
        ),
      ) +
      '&quote=' +
      encodeURIComponent(title)
    )
  }
}
const lineUrl = (url, title, pageFrom) => {
  if (pageFrom === 'store') {
    return (
      'https://line.me/R/share?text=' +
      encodeURIComponent(
        `Anda mungkin tertarik dengan produk ini: "${title}" `,
      ) +
      newUrlFormat(url, pageFrom)
    )
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return (
      'https://line.me/R/share?text=' +
      encodeURIComponent('Anda mungkin tertarik dengan produk ini ') +
      newUrlFormat(url, pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=sharing+button&itm_campaign=Line&itm_term=' + title,
        ),
      )
    )
  }
  if (pageFrom === 'help') {
    return (
      'https://line.me/R/share?text=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(encodeURIComponent('itm_term=' + title))
    )
  }
  if (pageFrom === 'tahu') {
    return (
      'https://line.me/R/share?text=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share+microsite&itm_campaign=Line&itm_term=' + title,
        ),
      )
    )
  }
  if (['catalog', 'jual'].includes(pageFrom)) {
    return (
      'https://line.me/R/share?text=' +
      encodeURIComponent('Anda mungkin tertarik dengan halaman ini ') +
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share&itm_campaign=Line&itm_term=' + title,
        ),
      )
    )
  }
}

const copyUrl = (url, title, pageFrom) => {
  if (['store', 'help'].includes(pageFrom)) {
    return newUrlFormat(url, pageFrom)
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return (
      newUrlFormat(url, pageFrom) +
      '?' +
      encodeURIComponent(
        `itm_source=sharing+button&itm_campaign=CopyUrl&itm_term=${title}`,
      )
    )
  }
  if (pageFrom === 'tahu') {
    return (
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent('itm_source=share+microsite&itm_campaign=CopyUrl')
    )
  }
  if (['catalog', 'jual'].includes(pageFrom)) {
    return (
      newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom) +
      '?' +
      encodeURIComponent('itm_source=share&itm_campaign=CopyUrl')
    )
  }
}
const mailUrl = (url, title, pageFrom) => {
  if (pageFrom === 'store') {
    return (
      'mailto:?subject=' +
      'Anda mungkin tertarik dengan produk ini' +
      `&body="${title}"%0D%0A` +
      newUrlFormat(url, pageFrom)
    )
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return (
      'mailto:?subject=' +
      'Anda mungkin tertarik dengan produk ini&body=' +
      encodeURIComponent(newUrlFormat(url, pageFrom)) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=sharing+button&itm_campaign=Whatsapp&itm_term=' + title,
        ),
      )
    )
  }
  if (pageFrom === 'help') {
    return (
      'mailto:?subject=' +
      'Anda mungkin tertarik dengan halaman ini&body=' +
      encodeURIComponent(
        newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom),
      ) +
      '?' +
      encodeURIComponent(encodeURIComponent('itm_term=' + title))
    )
  }
  if (pageFrom === 'tahu') {
    return (
      'mailto:?subject=' +
      'Anda mungkin tertarik dengan halaman ini&body=' +
      encodeURIComponent(
        newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom),
      ) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share+microsite&itm_campaign=Mail&itm_term=' + title,
        ),
      )
    )
  }
  if (['catalog', 'jual'].includes(pageFrom)) {
    return (
      'mailto:?subject=' +
      'Anda mungkin tertarik dengan halaman ini&body=' +
      encodeURIComponent(
        newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom),
      ) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share&itm_campaign=Mail&itm_term=' + title,
        ),
      )
    )
  }
}
const smsUrl = (url, title, pageFrom) => {
  if (pageFrom === 'store') {
    return (
      `sms:?&body=Anda mungkin tertarik dengan produk ini: "${title}" ` +
      newUrlFormat(url, pageFrom)
    )
  }
  if (pageFrom === 'product-detail' || pageFrom === 'review-rating') {
    return (
      'sms:?&body=Anda mungkin tertarik dengan produk ini: ' +
      newUrlFormat(url, pageFrom) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=sharing+button&itm_campaign=Line&itm_term=' + title,
        ),
      )
    )
  }
  if (pageFrom === 'help') {
    return (
      'sms:?&body=Anda mungkin tertarik dengan halaman ini: ' +
      encodeURIComponent(
        newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom),
      ) +
      '?' +
      encodeURIComponent(encodeURIComponent('itm_term=' + title))
    )
  }
  if (pageFrom === 'tahu') {
    return (
      'sms:?&body=Anda mungkin tertarik dengan halaman ini: ' +
      encodeURIComponent(
        newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom),
      ) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share+microsite&itm_campaign=Line&itm_term=' + title,
        ),
      )
    )
  }
  if (['catalog', 'jual'].includes(pageFrom)) {
    return (
      'sms:?&body=Anda mungkin tertarik dengan halaman ini: ' +
      encodeURIComponent(
        newUrlFormat(url.substring(0, url.indexOf('?')), pageFrom),
      ) +
      '?' +
      encodeURIComponent(
        encodeURIComponent(
          'itm_source=share&itm_campaign=Line&itm_term=' + title,
        ),
      )
    )
  }
}

export const SOCIAL_MEDIA_SHARE = [
  {
    icon: 'icon/social-media/icon-whatsapp.svg',
    label: 'WhatsApp',
    platform: 'whatsapp',
    link: (url, title, pageFrom) => whatsappUrl(url, title, pageFrom),
  },
  {
    icon: 'icon/social-media/icon-telegram.svg',
    label: 'Telegram',
    link: (url, title, pageFrom) => telegramUrl(url, title, pageFrom),
  },
  {
    icon: 'icon/social-media/icon-twitter.svg',
    label: 'Twitter',
    link: (url, title, pageFrom) => twitterUrl(url, title, pageFrom),
  },
  {
    icon: 'icon/social-media/icon-facebook.svg',
    label: 'Facebook',
    link: (url, title, pageFrom) => facebookUrl(url, title, pageFrom),
  },
  {
    icon: 'icon/social-media/icon-line.svg',
    label: 'Line',
    link: (url, title, pageFrom) => lineUrl(url, title, pageFrom),
  },
]

export const OTHER_SHARE = [
  {
    icon: 'icon/social-media/icon-link.svg',
    label: 'Salin Link',
    link: (url, title, pageFrom) => copyUrl(url, title, pageFrom),
  },
  {
    icon: 'icon/social-media/icon-mail.svg',
    label: 'Email',
    link: (url, title, pageFrom) => mailUrl(url, title, pageFrom),
  },
  {
    icon: 'icon/social-media/icon-sms.svg',
    label: 'SMS',
    link: (url, title, pageFrom) => smsUrl(url, title, pageFrom),
  },
]
