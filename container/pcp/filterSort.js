export const RECOMMENDATION_SORT = [
  {
    label: 'Paling Sesuai',
    value: 'matching',
    algoliaValue: 'productsvariant',
  },
  {
    label: 'Produk Populer',
    value: 'mostPopular',
    algoliaValue: 'productsvariant_popular_sort',
  },
  {
    label: 'Ulasan Terbaik',
    value: 'highestRatingSum',
    algoliaValue: 'productsvariant_rating_sum_desc',
  },
  {
    label: 'Harga Tertinggi',
    value: 'highestPrice',
    algoliaValue: 'productsvariant_price_desc',
  },
  {
    label: 'Harga Terendah',
    value: 'lowestPrice',
    algoliaValue: 'productsvariant_price_asc',
  },
]

export const SORT_DATA = [
  {
    label: 'Paling Sesuai',
    value: 'matching',
    algoliaValue: 'productsvariant',
  },
  {
    label: 'Produk Terbaru',
    value: 'newArrival',
    algoliaValue: 'productsvariant_publishdate',
  },
  {
    label: 'Produk Populer',
    value: 'mostPopular',
    algoliaValue: 'productsvariant_popular_sort',
  },
  {
    label: 'Ulasan Terbaik',
    value: 'highestRatingSum',
    algoliaValue: 'productsvariant_rating_sum_desc',
  },
  {
    label: 'Harga Terendah',
    value: 'lowestPrice',
    algoliaValue: 'productsvariant_price_asc',
  },
  {
    label: 'Harga Tertinggi',
    value: 'highestPrice',
    algoliaValue: 'productsvariant_price_desc',
  },
  {
    label: 'Diskon Tertinggi',
    value: 'highestDiscount',
    algoliaValue: 'productsvariant_discount_desc',
  },
]
