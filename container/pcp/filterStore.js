import isEmpty from 'lodash/isEmpty'

const OFFICIAL_STORE = [
  {
    name: 'ace ', //ACE_HARDWARE
    logo: 'icon/store-logo/AHI.webp', // ACE_IMAGE
    company_code: 'AHI',
  },
  {
    name: 'acepresso',
    logo: 'icon/store-logo/Acepresso.webp',
    company_code: 'ODI',
  },
  { name: 'ataru', logo: 'icon/store-logo/Ataru.webp', company_code: 'ODI' },
  {
    name: 'bikeco fit',
    logo: 'icon/store-logo/BikeCoFit.webp',
    company_code: 'ODI',
  },
  {
    name: 'chatime',
    logo: 'icon/store-logo/Chatime.webp',
    company_code: 'ODI',
  },
  { name: 'dr kong', logo: 'icon/store-logo/DrKong.webp', company_code: 'ODI' },
  {
    name: 'eyesoul',
    logo: 'icon/store-logo/Eyesoul.webp',
    company_code: 'ODI',
  },
  {
    name: 'informa',
    logo: 'icon/store-logo/Informa.webp',
    company_code: 'HCI',
  },
  {
    name: 'pendopo',
    logo: 'icon/store-logo/Pendopo.webp',
    company_code: 'ODI',
  },
  {
    name: 'pet kingdom',
    logo: 'icon/store-logo/PetKingdom.webp',
    company_code: 'ODI',
  },
  { name: 'selma', logo: 'icon/store-logo/Selma.webp', company_code: 'ODI' },
  { name: 'susen', logo: 'icon/store-logo/Susen.webp', company_code: 'ODI' },
  {
    name: 'toys kingdom',
    logo: 'icon/store-logo/ToysKingdom.webp',
    company_code: 'TGI',
  },
  {
    name: 'travelink',
    logo: 'icon/store-logo/Travelink.webp',
    company_code: 'ODI',
  },
]

export const convertStoreName = (string) => {
  if (string?.indexOf('---') > 1) {
    return string?.replace(/---/g, '_').replace(/-/g, ' ').replace(/_/g, ' - ')
  } else {
    return string?.replace(/[()]/g, '')
  }
}

export const convertStoreNameToUrlKey = (string) => {
  if (string) {
    return string
      .replace(/[()]/g, '')
      .replace(' - ', '---')
      .replace(/ +/g, '-')
      .toLowerCase()
  } else {
    return string
  }
}

export const splitStoreName = (string) => {
  if (!string) {
    return
  }
  const storeName = string?.replace(/[()]/g, '')
  const arr = storeName?.split(' - ')
  return {
    store_location: arr[0],
    store_name: arr[1],
    company_code: getCompanyCodeByStoreName(arr[1]),
  }
}

export const urlKeyStore = (string) => {
  const str = string.replace(/[()]*( -)*/g, '')
  let arr = str.split(' ')
  arr = arr.filter((item) => item)
  return arr.join('-').toLowerCase()
}

export const handleSelectStore = (brandStore, storeCode, isAlgolia = false) => {
  const findStore = brandStore?.find((store) =>
    isAlgolia ? store.url_key === storeCode : store.store_code === storeCode,
  )
  return { ...findStore, ...splitStoreName(findStore?.name) }
}

export const handleStoreByLocation = (
  currentStore,
  resultStore,
  listCityId,
  listProvinceId,
) => {
  const selectedStore = []
  const validResultStore = Array.isArray(resultStore) ? resultStore : []
  for (const store of validResultStore) {
    const availableStore = currentStore?.find(
      (d) => d.store_code === store.store_code,
    )
    if (availableStore) {
      selectedStore.push(store)
    }
  }
  if (listCityId?.length > 0 || listProvinceId?.length > 0) {
    return handleStoreDataFilter(selectedStore)
  } else {
    return handleStoreDataFilter(currentStore)
  }
}

export const handleStoreDataFilter = (data) => {
  return (
    data?.map((item) => ({
      label: convertStoreName(item.store_name || item.name),
      value: item.store_code,
    })) || []
  )
}

export const compareStoreAlgolia = (
  storeAlgolia,
  allStore,
  isSelectLocation,
  search,
) => {
  if (!isEmpty(allStore) || isSelectLocation) {
    const selectedStore = []

    for (const store of allStore) {
      const availableStore = storeAlgolia?.find(
        (d) => store.url_key === d.label,
      )

      if (!isEmpty(availableStore)) {
        // disini harus balikin find dari store algolia bukan store dari elastic
        selectedStore.push({
          ...availableStore,
          label: convertStoreName(store.url_key),
        })
      }
    }

    selectedStore
      .sort((a, b) => {
        return a.label < b.label ? -1 : 1
      })
      .sort((a, b) => {
        return a.isRefined > b.isRefined ? -1 : 1
      })

    return selectedStore
  }

  let dataFilter

  if (search) {
    dataFilter = storeAlgolia
    .filter(item => item.label.includes(search))
    .map(item => ({
      ...item,
      label: convertStoreName(item.label),
      value: [item.label],
    }));
  } else {
    dataFilter = storeAlgolia.map((item) => ({
      ...item,
      label: convertStoreName(item.label),
      value: [item.label],
    }))
  }

return dataFilter;
}

export const compareStoreElastic = (storeElastic, allStore) => {
  const selectedStore = []
  for (const store of allStore) {
    const availableStore = storeElastic?.find(
      (d) => store.store_code === d.store_code,
    )
    if (availableStore) {
      selectedStore.push({
        label: convertStoreName(availableStore.store_name),
        value: availableStore.store_code,
      })
    }
  }
  return selectedStore
}

export const renderStoreLogo = (storeName) => {
  let logo = ''
  OFFICIAL_STORE.forEach((item) => {
    if (storeName?.toLowerCase().indexOf(item.name) >= 0) {
      logo = item.logo
    }
  })
  return logo
}

export const getCompanyCodeByStoreName = (storeName) => {
  let companyCode = ''
  OFFICIAL_STORE.forEach((item) => {
    if (storeName?.toLowerCase().indexOf(item.name) >= 0) {
      companyCode = item.company_code
    }
  })
  return companyCode
}
