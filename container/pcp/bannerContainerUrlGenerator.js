import isEmpty from 'lodash/isEmpty'
import config from '../../../../config'
import { clearHtmlTag } from '../../utils/String'

export const bannerContainerUrlGenerator = (item) => {
  // we need to extract the board name for injecting inside the itm source and itm campaign
  // the value using the mini block board category
  let board = ''
  const params = item?.target_link?.split('?')
  if (params.length > 1) {
    const queryParams = params[1]
    const checkMultipletQueryParams = queryParams?.split('&')

    checkMultipletQueryParams.map((query) => {
      if (query.includes('board')) {
        const splitQuery = query.split('=')?.[1] || ''
        // after we get it the value is using + as the seperator, so we need to replace it with -
        board = splitQuery.replace(/[+]/g, '-')
      }
    })

    // there some link that still using m.ruparupa.com, and because the domain of our moweb and desktop web is the same
    // we need to replace it so there will be no redirect process
    return (
      item.target_link.replace('m.ruparupa.com', 'www.ruparupa.com') +
      `&itm_source=mini-category-category-${board}&itm_campaign=${board}`
    )
  } else {
    return item.target_link
  }
}

export const urlMiniblockCategory = (
  router,
  category,
  itmSource,
  eventName,
  from = '',
  keyword = '',
) => {
  let itmSourceConstruct = '&itm_source='
  let itmCampaignConstrcut = '&itm_campaign='
  const isTitleEmpty = isEmpty(category?.title)
  // If category title is empty, we will make category title from target_link'
  let categoryTitleFallBack = ''

  if (isTitleEmpty) {
    const targetLink = category.target_link
    // Getting the last substring after the last '/' in the targetLink
    const lastSubstring = targetLink?.substring(targetLink.lastIndexOf('/') + 1)
    // Getting the first substring before the '?' character from lastSubstring
    const firstSubstring = lastSubstring?.split('?')[0]
    // Removing the '.html' extension from the firstSubstring
    const result = firstSubstring?.replace('.html', '')
    categoryTitleFallBack = result
  }
  if (from === 'algolia') {
    itmSourceConstruct += 'search'
    itmCampaignConstrcut += `${category?.itm}&`
    const itmDevice = `itm_device=${config.environment}&`
    const itmTerm = `itm_term=${clearHtmlTag(
      category?.title?.toLowerCase().replace(/ /g, '-'),
    )}&`
    const manualConstructLink =
      category?.url +
      '?' +
      itmSourceConstruct.substring(1).replace(/ /g, '-') +
      itmCampaignConstrcut +
      itmDevice +
      itmTerm +
      'keyword=' +
      keyword.toLowerCase().replace(/ /g, '-')
    return manualConstructLink
  }
  if (router?.query?.categories?.length === 1) {
    itmCampaignConstrcut += 'second-level'
  } else if (router?.query?.categories?.length === 2) {
    itmCampaignConstrcut += 'third-level'
  } else if (router?.query?.categories?.length === 3) {
    itmCampaignConstrcut += 'fourth-level'
  }
  if (eventName === 'Mini Block Category' || eventName === 'Miniblock Search') {
    if (eventName === 'Miniblock Search') {
      itmSourceConstruct += 'mini-category-category-'
    }
    if (itmCampaignConstrcut === '&itm_campaign=') {
      !isTitleEmpty
        ? (itmCampaignConstrcut += category?.title
            ?.toLowerCase()
            .replace(' ', '-'))
        : (itmCampaignConstrcut += categoryTitleFallBack
            .toLowerCase()
            .replace(' ', '-'))
    }
    !isTitleEmpty
      ? (itmSourceConstruct += `${clearHtmlTag(
          category?.title?.toLowerCase().replace(/ /g, '-'),
        )}`)
      : (itmSourceConstruct += `${clearHtmlTag(
          categoryTitleFallBack.toLowerCase().replace(/ /g, '-'),
        )}`)
  } else {
    if (itmCampaignConstrcut === '&itm_campaign=') {
      itmCampaignConstrcut += router?.query?.itm_campaign || category?.url_key
    }
    !isTitleEmpty
      ? (itmSourceConstruct += itmSource
          ? `${itmSource}${clearHtmlTag(
              category?.title.toLowerCase().replace(/ /g, '-'),
            )}`
          : '')
      : (itmSourceConstruct += itmSource
          ? `${itmSource}${clearHtmlTag(
              categoryTitleFallBack.toLowerCase().replace(/ /g, '-'),
            )}`
          : '')
  }
  const itmDeviceConstruct = `&itm_device=${config.environment}`
  const manualConstructLink =
    category?.target_link +
    itmCampaignConstrcut +
    itmSourceConstruct +
    itmDeviceConstruct
  // miniblock search always return manualConstructLink because miniblock search link must contain itmsrc, itmcampaign and itmdevice
  if (eventName === 'Miniblock Search') {
    return manualConstructLink
  }
  // why we use target_url, because the one who called the component Categories already construct the url as target_url
  return category.target_url || manualConstructLink
}
