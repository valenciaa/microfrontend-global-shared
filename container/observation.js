import Cookies from 'js-cookie'
import localforage from 'localforage'
import isEmpty from 'lodash/isEmpty'
import React, { useEffect } from 'react'
import { checkAuthUser } from '../services/AlternateLogin'
import {
  useGetUserProfile,
  useGetCustomerMembershipAlt,
} from '../services/api/queries/global'
import { getRupaUID } from '../utils/GetRupaUID'
import { sha256 } from 'js-sha256'
import { useGetMinicartItems } from '../services/api/queries/minicart'
import config from '../../../config'
// import config from '../../../config'
import { postSetAttributeMoengage } from '../utils/MoengageSetAttributes'

export default function Observation({
  handleChangeState,
  state: cartAuth,
  setState,
  children,
}) {
  // const [paymentCartId, setPaymentCartId] = useState('')
  // const [shouldLoginWithCart, setShouldLoginWithCart] = useState(false)
  const minicartId = cartAuth?.minicart_id
  const isAuthRefetched = cartAuth?.isAuthRefetched

  const commonOnError = () => setAuthState(null)
  // Effects & Queries
  useEffect(() => {
    getAuthUser()
    setB2bcData()
    getRupaUID()
    // we will clean up all the matched personal lastseen  cookies
    if (Cookies.get('matched-personal-lastseen')) {
      Cookies.remove('matched-personal-lastseen')
    }
  }, [])

  const { refetch: getUserProfile } = useGetUserProfile({
    enabled: false,
    onSuccess: (data) => {
      if (!data) {
        setAuthState(null)
        localforage.removeItem('access_token')
        Cookies.remove('access_token')
        Cookies.remove('algolia_ssr')
        Cookies.remove('ugid')
        Cookies.remove('sessionrupaUID')
        Cookies.remove('rr-sid')
        Cookies.remove('geolocation')
        localStorage.removeItem('SHIPPING_LOCATION')
        return
      }

      const hashUser = data?.customer_id
        ? sha256(data.customer_id.toString()).toString()
        : ''
      if (Cookies.get('algolia_ssr') !== hashUser && data) {
        if (hashUser) {
          Cookies.set('algolia_ssr', hashUser)
        }
      }

      localforage.setItem(
        'persist:ruparupa',
        JSON.stringify({ auth: JSON.stringify({ user: data }) }),
      )
      setAuthState(data)
      getCartData()
    },
    onError: (error) => {
      if (error?.message?.toLowerCase() === 'token not valid') {
        setAuthState(null)
        localforage.clear()
        Cookies.remove('access_token')
        Cookies.remove('algolia_ssr')
        Cookies.remove('ugid')
        Cookies.remove('sessionrupaUID')
        Cookies.remove('rr-sid')
        Cookies.remove('geolocation')
        localStorage.removeItem('SHIPPING_LOCATION')
        return
      } else {
        commonOnError()
      }
    },
  })

  const { refetch: getUserProfileKLK } = useGetCustomerMembershipAlt({
    enabled: false,
    onSuccess: (data) => {
      if (!data) {
        getUserProfile()
        Cookies.remove('sessionrupaUID')
        Cookies.remove('algolia_ssr')
        return
      }

      // console.log(sha256(1.toString()))
      const hashUser = data?.user.customer_id
        ? sha256(data.user.customer_id.toString()).toString()
        : ''
      if (Cookies.get('algolia_ssr') !== hashUser && data.user) {
        if (hashUser) {
          Cookies.set('algolia_ssr', hashUser)
        }
      }

      // set attributes for moengage
      const moengagePayload = {
        member_no: data?.user?.member_ids,
        attributes: {
          is_activated: true,
          rrr_id: data?.membership?.primary_card?.number,
        },
      }

      postSetAttributeMoengage(moengagePayload)
      localforage.setItem(
        'persist:ruparupa',
        JSON.stringify({
          auth: JSON.stringify({
            user: { ...data.user },
            isKlk: true,
          }),
        }),
      )
      setAuthState(data.user)
      getCartData()
    },
    onError: () => {
      getUserProfile()
    },
  })

  const setIsGroupMinicartItem = () => {
    if (config.environment === 'desktop') {
      return 10
    } else {
      const groupMinicarPage = ['minicart', 'wishlist'] // ? just add current page if you want to group cart item according to store
      const currentPath = cartAuth.currentPage
      return groupMinicarPage.includes(currentPath) ? 10 : 0
    }
  }

  const {
    isLoading: isLoadingGetCart,
    isFetching: isFetchingGetCart,
    isSuccess: isSuccessGetCart,
    refetch: refetchMinicartItems,
    isFetched: isFetchedGetCart,
  } = useGetMinicartItems(
    { is_group: setIsGroupMinicartItem() },
    {
      enabled: !!minicartId,
      onSuccess: (data) => {
        let cart = data
        if (!data) {
          cart = {}
        }
        setState((prev) => ({
          ...prev,
          cart,
          isCartRefetched: true,
          totalCartQty: cart.total_item_qty,
        }))
      },
      onError: commonOnError,
    },
  )

  useEffect(() => {
    setState((prev) => ({
      ...prev,
      isLoadingGetCart,
      isFetchingGetCart,
      isSuccessGetCart,
      refetchMinicartItems,
      isFetchedGetCart,
    }))
  }, [
    isLoadingGetCart,
    isFetchingGetCart,
    isSuccessGetCart,
    refetchMinicartItems,
    isFetchedGetCart,
  ])

  // const cartAuthOnSuccess = (data) => {
  //   if (data) {
  //     const userAndToken = storeDataAfterLoginCart(data, handleChangeState)
  //     setAuthState(data, userAndToken)
  //   } else {
  //     setAuthState(null)
  //   }
  //   const minicartId_ = data?.minicart_id
  //   if (minicartId_ && minicartId_ !== minicartId) {
  //     afterChangeMinicartFromPaymentCartAuth.current = true
  //   }
  //   handleChangeState('minicart_id', minicartId_)
  // }
  // const { refetch: getMinicartAuth } = useGetMinicartAuth({ minicart_id: minicartId }, {
  //   enabled: false,
  //   onSuccess: cartAuthOnSuccess,
  //   onError: commonOnError
  // })
  // const { refetch: getCartAuth } = useGetCartAuth({ cart_id: paymentCartId }, {
  //   enabled: false,
  //   onSuccess: cartAuthOnSuccess,
  //   onError: () => {
  //     commonOnError()
  //     handleChangeState('isCartRefetched', true)
  //   }
  // })

  // useEffect(() => {
  //   if (!cartData) return
  //   handleChangeState('isCartRefetched', true)

  // check if customer is guest or not
  // const isCustomerGuest = cartData?.customer?.customer_is_guest > 0
  // if (!isCustomerGuest) {
  //   // login with cart using minicart_id
  //   console.log('INFO: Initialize login with mini cart')
  //   getMinicartAuth()
  //   handleChangeState('isCartRefetched', true)
  //   return
  // }

  // // login with payment_cart_id
  // if (paymentCartId) {
  //   console.log('INFO: Initialize Login with payment cart')
  //   getCartAuth()
  // } else {
  //   console.log("ERR: There's no payment cart id")
  //   setAuthState(null, { isCartRefetched: true })
  // }
  // }, [cartData])

  useEffect(() => {
    if (minicartId !== null && isAuthRefetched) {
      handleChangeState('isCartRefetched', true)
    }
  }, [minicartId, isAuthRefetched])

  // Helpers
  const setAuthState = (user, otherChange = {}) => {
    const change = { isAuthRefetched: true, ...otherChange }
    if (user) {
      change.auth = { user }
      const userMinicartId = user?.cart_id
      if (config.isB2b) {
        change.minicart_id = userMinicartId
      }
      if (userMinicartId && !config.isB2b) {
        localforage.setItem('minicart_id', userMinicartId)
        Cookies.set('minicart_id', userMinicartId)
        change.minicart_id = userMinicartId
      }
    }

    // use batch update if the change not just isAuthRefetched,
    // else use handleChangeState because its optimized for updating 1 attr
    if (Object.values(change).length > 1) {
      setState((prev) => ({ ...prev, ...change }))
    } else {
      handleChangeState('isAuthRefetched', true)
    }
  }

  async function getAuthUser() {
    const accessToken = await localforage.getItem('access_token')
    if (accessToken && !Cookies.get('access_token')) {
      Cookies.set('access_token', accessToken, { expires: 365 })
    }
    // HF refer to ticket RR2020-3543
    // this is only HF, if you have any insight for permanently fix this please contact the person assigned to the ticket
    if (accessToken) {
      // check if there are access token or not, access token only stored if you login
      const isUserDataStored = await localforage
        .getItem('persist:ruparupa')
        .then((data) => {
          const user = checkAuthUser(data)
          if (user) {
            setAuthState(user)
            getCartData()
            return true
          } else {
            return false
          }
        })
      if (!isUserDataStored) {
        // we tried to get the user data one more time and persist its once again
        // true for flaging this from Observation purpose so there's a chance that you will get logout if we didn't get the user data from BE
        getUserProfileKLK()
        // localforage.setItem('persist:ruparupa', JSON.stringify({ auth: user }))
      }
    } else {
      if (config.isB2b) {
        return setB2bData()
      }
      // const hasMinicartId = await getCartData()
      // if (!hasMinicartId) {
      setAuthState(null)
      // } else {
      //   setShouldLoginWithCart(true)
      // }
    }
  }

  async function getCartData() {
    const minicartId_ = await localforage.getItem('minicart_id')
    // const paymentCartId_ = await localforage.getItem('payment_cart_id')
    // if (paymentCartId_) {
    //   setPaymentCartId(paymentCartId_)
    // }
    if (minicartId_ && !isEmpty(minicartId_)) {
      handleChangeState('minicart_id', minicartId_)
      return true
    } else {
      // engga perlu di login in
      return null
    }
  }

  async function setB2bcData() {
    const b2bc = await localforage.getItem('b2bc')
    if (b2bc) {
      Cookies.set('b2bc', b2bc, { expires: 365 })
    }
  }

  async function setB2bData() {
    const autB2b = await localforage.getItem(
      `persist:ruparupa_b2b_${config.companyNameCSS}`,
    )
    const user = checkAuthUser(autB2b)
    if (user) {
      setAuthState(user)
    }
  }

  return <>{children}</>
}
