import dayjs from 'dayjs'
import isEmpty from 'lodash/isEmpty'
import config from '../../../config'

export const getImageCcBank = (paymentMethod, type) => {
  if (paymentMethod === 'bank_transfer' || paymentMethod === 'debit_card') {
    if (
      [
        'bca',
        'bni',
        'bri',
        'maybank',
        'cimb',
        'danamon',
        'mandiri',
        'permata',
      ].indexOf(type) !== -1
    ) {
      return (
        config.assetsURL + 'images/footer-logo/bank-pdp/logo-' + type + '.png'
      )
    }
  }

  if (paymentMethod === 'credit_card') {
    if (
      [
        'bca',
        'bni',
        'bri',
        'maybank',
        'cimb',
        'danamon',
        'mandiri',
        'permata',
      ].indexOf(type) !== -1
    ) {
      return (
        config.assetsURL + 'images/footer-logo/bank-pdp/logo-' + type + '.png'
      )
    }
  }

  if (paymentMethod === 'e_money') {
    if (['ovo', 'gopay'].indexOf(type) !== -1) {
      return config.assetsURL + 'images/footer-logo/' + type + '.png'
    }
  }

  if (paymentMethod === 'credit_non_cc') {
    if (type.indexOf(['kredivo']) !== -1) {
      return config.assetsURL + 'images/footer-logo/logo-' + type + '.png'
    }
  }

  return null
}

export const getCompany = (company) => {
  switch (company) {
    case 'TGI':
      return 'toyskingdomonline'
    case 'AHI':
      return 'azko' //ACE_HARDWARE
    case 'HCI':
      return 'informa'
    default:
      return 'ruparupa'
  }
}

export const handleCopyClipboard = (voucherCode, setToggleClipboard) => {
  navigator.clipboard.writeText(voucherCode)
  setToggleClipboard(true)
}

export const handleStackedCard = (count, index, typeCopy) => {
  if (count > 1 && count - index > 1) {
    return (
      <>
        {count > 2 && count - index > 2 && (
          <div className={`card-${typeCopy ? 'copy-' : ''}dummy__third-card`} />
        )}
        <div className={`card-${typeCopy ? 'copy-' : ''}dummy__second-card`} />
      </>
    )
  }
}
export const handlePersonalisedInformationChecking = (
  payload,
  trackBreadcrumb,
) => {
  if (payload?.last_seen) {
    let matched = {}
    const availableCampaign = payload?.last_seen.data
    const currentCategories = trackBreadcrumb?.[1]?.category_id // match with level2 category
    if (currentCategories && availableCampaign) {
      matched = availableCampaign.find((el) => {
        return el.categories.some((el) => Number(el) === currentCategories)
      })
    }

    //  the old logic is to save to cookies, we change it to localstorage because the data size is too much for cookies
    if (!isEmpty(matched)) {
      const validDate = localStorage.getItem(
        'matched_personal_last_seen_valid_date',
      ) // eslint-disable-line
      if (!validDate || dayjs(validDate) <= dayjs()) {
        if (matched?.categories?.length > 0) {
          delete matched.categories
        }
        localStorage.setItem(
          'matched_personal_last_seen_valid_date',
          dayjs().add(1, 'day'),
        ) // eslint-disable-line
        localStorage.setItem(
          'matched_personal_last_seen',
          JSON.stringify(matched),
        ) // eslint-disable-line
      }
    }
  }
}
