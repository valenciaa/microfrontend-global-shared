import { useContext, createContext, useState, useEffect } from 'react'
import { useGetProductRedeemByCoinByUrlKey } from '../../../services/api/queries/coin'
import { useCreateInstantCheckout } from '../../../services/api/mutations/cart'
import config from '../../../../../config'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import { GetParam } from '../../../utils'
import GetShopbackUrl from '../../../utils/GetShopbackUrl'

export const RedemptionDetailContext = createContext({})

export const RedemptionDetailProvider = ({ children }) => {
  const [urlKey, setUrlKey] = useState('')
  const [membership, setMembership] = useState(null)
  const [product, setProduct] = useState(null)
  const [actionInfo, setActionInfo] = useState(null)
  const [tncMarketing, setTncMarketing] = useState('')
  const [infoboxErr, setInfoboxErr] = useState('')

  useEffect(() => {
    setUrlKey(window.location.href.split('/').pop())
  }, [])

  const { isLoading: isLoadingGetProduct } = useGetProductRedeemByCoinByUrlKey(
    urlKey,
    {
      enabled: !!urlKey,
      onSuccess: (response) => {
        const { product, membership, action_info: actionInfo, tnc } = response
        setMembership(membership)

        const { variants } = product
        const dataProduct = {
          name: product.name,
          imageUrl: variants?.[0].images?.[0].image_url,
          attributesLabel: variants?.[0].attributes
            ? variants[0].attributes
                .filter((el) => el.is_group_attribute === 10)
                .map((el) => el.attribute_value)
                .join(', ')
            : null,
          coinPrice: variants?.[0]?.price_promo?.[0]?.price || 0,
          normalPrice: variants?.[0]?.prices?.[0]?.price || 0,
          sku: variants?.[0]?.sku,
          urlKey: product.url_key,
        }
        setProduct(dataProduct)

        setActionInfo({
          maxQuantity: actionInfo?.max_stock_detail_page || 0,
          minQuantity: actionInfo?.min_stock_detail_page || 0,
          modalType: actionInfo?.modal_type || null,
          buttonText: actionInfo?.button_text || '',
          isButtonDisabled: actionInfo?.is_button_disabled || false,
        })

        if (tnc) {
          setTncMarketing(tnc)
        }

        mixpanelTrack('View produk koin', {
          'ITM Source': GetParam('itm_source') || 'None',
          'ITM Term': variants?.[0]?.sku,
          'ITM Device': config.environment,
          'Item ID': variants?.[0]?.sku,
          'Item Price': variants?.[0]?.price_promo?.[0]?.price,
          'Selling Price': variants?.[0]?.prices?.[0]?.price,
        })
      },
      onError: (err) => {
        if (err.message === 'Not Found') {
          window.location.href = config.baseURL + 'page/404'
        } else {
          setInfoboxErr('Promo tidak berlaku untuk produk ini')
        }
      },
    },
  )

  const { mutate: createInstantCheckout, isLoading: isLoadingInstantCheckout } =
    useCreateInstantCheckout()

  const handleInstantCheckout = () => {
    const InstantUpgradedata = {
      items: [
        {
          sku: membership?.sku_instant_upgrade,
          qty_ordered: 1,
          shipping: {
            delivery_method: 'pickup',
            is_update_store_code: true,
            store_code: membership?.store_code_instant_upgrade || 'S702',
            default_store_code:
              membership?.store_code_instant_upgrade || 'S702',
          },
        },
      ],
      device: config.environment,
      cart_type: 'instant',
      company_code: config.companyCode,
    }
    createInstantCheckout(InstantUpgradedata, {
      onSuccess: (res) => {
        if (res?.data?.data && res?.data?.data?.cart_id) {
          const { cart_id: cartId } = res.data.data

          window.location.href =
            config.paymentURL +
            '?token=' +
            cartId +
            '&coin_url_key=' +
            urlKey +
            GetShopbackUrl()
        } else {
          setInfoboxErr(
            res?.data?.errors?.message
              ? res?.data?.error?.message
              : 'Promo tidak berlaku untuk produk ini',
          )
        }
      },
    })
  }

  const handleBuyItem = (qtyOrdered = 1) => {
    const InstantUpgradedata = {
      items: [
        {
          sku: product?.sku,
          qty_ordered: qtyOrdered,
          shipping: {},
        },
      ],
      device: config.environment,
      cart_type: 'coin_exchange',
      company_code: config.companyCode,
    }
    createInstantCheckout(InstantUpgradedata, {
      onSuccess: (res) => {
        if (res?.data?.data && res?.data?.data?.cart_id) {
          const { cart_id: cartId } = res.data.data

          window.location.href =
            config.paymentURL + '?token=' + cartId + GetShopbackUrl()
        } else {
          setInfoboxErr(
            res?.data?.errors?.message
              ? res?.data?.error?.message
              : 'Promo tidak berlaku untuk produk ini',
          )
        }
      },
    })
  }

  return (
    <RedemptionDetailContext.Provider
      value={{
        isLoadingGetProduct,
        isLoadingInstantCheckout,
        membership,
        product,
        handleInstantCheckout,
        handleBuyItem,
        actionInfo,
        tncMarketing,
        infoboxErr,
        setInfoboxErr,
      }}
    >
      {children}
    </RedemptionDetailContext.Provider>
  )
}

export function useRedemptionDetailContext() {
  return useContext(RedemptionDetailContext)
}
