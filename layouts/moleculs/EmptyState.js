import React from 'react'
import Skeleton from 'react-loading-skeleton'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'

export default function EmptyState({
  children,
  className,
  imgSrc,
  imgAlt,
  imgHeight,
  imgWidth,
  ...props
}) {
  return (
    <div className={className}>
      <CustomLazyLoadImage
        src={imgSrc}
        alt={imgAlt}
        placeholder={<Skeleton />}
        height={imgHeight}
        width={imgWidth}
        {...props}
      />
      {children}
    </div>
  )
}
