import React from 'react'
import config from '../../../../config'
import ArrowLeft from '../../../../public/static/icon/arrow-left.svg'
// import CloseIcon from '../../../../public/static/icon/close.svg'

export default function LightboxNavigaton({
  closeButtonComponent,
  closeLightbox,
  pagination,
}) {
  return (
    <div className='lightbox__navigation'>
      <div className='lightbox__navigation-wrapper'>
        {config.environment === 'mobile' && (
          <>
            <ArrowLeft
              className='color-white'
              onClick={() => closeLightbox()}
            />
            <div className='lightbox__pagination'>
              <p className='lightbox__pagination-text ui-text-1'>
                {pagination}
              </p>
            </div>
          </>
        )}
        {config.environment === 'desktop' && (
          <>
            <div className='lightbox__pagination'>
              <p className='lightbox__pagination-text ui-text-1'>
                {pagination}
              </p>
            </div>
            {closeButtonComponent}
            {/* <div className='lightbox__close-button'>
              <div className='flex items-center justify-center lightbox__close-button--icon'>
                <CloseIcon alt='Close Icon' />
              </div>
            </div> */}
          </>
        )}
      </div>
    </div>
  )
}
