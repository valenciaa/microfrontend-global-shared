import React from 'react'
import config from '../../../../config'

export default function InputBar({
  action,
  name,
  id = '',
  placeholder = '',
  type,
  icon,
  length,
  value,
  autoComplete = 'off',
  phoneCode,
  additionalClass,
  checked = false,
  disabled = false,
  actionOnBlur,
  innerRef,
  inputmode,
  maxDate,
  readOnly = false,
}) {
  return (
    <div className={`input-bar${config.isB2b ? '-b2b' : ''}-container`}>
      <input
        className={'input-bar' + (additionalClass || ' input-field-desktop')}
        type={type}
        name={name}
        id={id}
        placeholder={placeholder}
        maxLength={length > 0 ? length : undefined}
        onChange={(e) => action && action(e)}
        onBlur={(e) => actionOnBlur && actionOnBlur(e)}
        value={value}
        autoComplete={autoComplete}
        checked={checked}
        disabled={disabled}
        ref={innerRef}
        inputMode={inputmode}
        max={maxDate}
        onKeyDown={type === 'date' ? (e) => e.preventDefault() : null}
        readOnly={readOnly}
      />
      {icon && (
        <img
          src={config.assetsURL + icon}
          className='icon'
          width={20}
          height={20}
        />
      )}
      {/* phoneCode from landing page download mobile apps */}
      {phoneCode && <span className='phone-code ui-text-3'>+62</span>}
    </div>
  )
}
