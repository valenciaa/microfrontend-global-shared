import Skeleton from 'react-loading-skeleton'

export default function SkeletonWrapper({
  height,
  width,
  count,
  isCircle = false,
  isLoading = false,
  children,
}) {
  return (
    <>
      {!isLoading ? (
        children
      ) : (
        <div style={{ height, zIndex: 0 }}>
          <Skeleton
            style={{ verticalAlign: 'text-top' }}
            height={height}
            width={width}
            count={count}
            circle={isCircle}
          />
        </div>
      )}
    </>
  )
}
