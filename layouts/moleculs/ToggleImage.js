import React from 'react'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'
import { ItmGenerator } from '../../utils'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import config from '../../../../config'
import { useConstantsContext } from '../../context/ConstantsContext'
import { getMixpanelLocationProperty } from '../../utils/GetMixpanelLocationProperty'

export default function ToggleImage({
  type,
  isGuest,
  title,
  link,
  primaryImage,
  secondaryImage,
  alt,
  condition,
  className,
  height,
  width,
  from,
  count,
  onClick,
  itmCampaign = '',
  style,
  isBadge = false,
  divId = '',
}) {
  const constantsContext = useConstantsContext()
  const pageType = constantsContext?.pageType
  const pageFrom = constantsContext?.pageFrom

  function handleClickIcon(e) {
    if (alt === 'inbox-icon') {
      mixpanelTrack('Inbox Promo', {
        Location: 'Homepage',
      })
    } else if (alt === 'cart-icon') {
      mixpanelTrack('Click Icon Cart', {
        Location: getMixpanelLocationProperty(pageFrom),
      })
    }

    if (alt === 'wishlist-icon') {
      mixpanelTrack('View Wishlist', {
        Location: 'Homepage',
      })
    }

    if (onClick) {
      onClick(e)
    }
  }

  let newUrl
  let mixpanelEvent = ''
  let mixpanelProperties = {}
  switch (type) {
    case 'header-top':
      if (alt !== 'Search' && link) {
        newUrl = ItmGenerator(
          (pageFrom === 'home'
            ? 'homepage-'
            : pageFrom === 'product-detail'
              ? 'pdp-'
              : 'pcp-') + alt,
          link,
          alt,
          itmCampaign !== '' ? itmCampaign : '',
        )
      }
      if (alt === 'cart-icon') {
        newUrl = link
      }
      return (
        <div
          className='col-xs icons padding__horizontal--xs'
          style={
            config.companyCode === 'TGI'
              ? {
                  ...style,
                  position:
                    alt === 'inbox-icon' || alt === 'cart-icon'
                      ? 'relative'
                      : 'static',
                }
              : alt === 'inbox-icon' || alt === 'cart-icon'
                ? { position: 'relative' }
                : undefined
          }
          id={divId || undefined}
        >
          {newUrl && (
            <a onClick={handleClickIcon} href={newUrl}>
              {condition ? (
                <CustomLazyLoadImage
                  src={primaryImage}
                  id={alt.replace(/\s+/g, '-').toLowerCase()}
                  className={className}
                  alt={alt + 'Primary Icon'}
                  height={height || 20}
                  width={width || 20}
                  name={`${alt}Button`}
                />
              ) : (
                <CustomLazyLoadImage
                  src={secondaryImage}
                  id={alt.replace(/\s+/g, '-').toLowerCase()}
                  className={className}
                  alt={alt + 'Secondary Icon'}
                  height={height || 20}
                  width={width || 20}
                  name={`${alt}Button`}
                />
              )}
              {count && count !== 0 ? (
                <div className='count'>{count}</div>
              ) : null}
            </a>
          )}
          {!newUrl && (
            <div onClick={handleClickIcon}>
              {condition ? (
                <CustomLazyLoadImage
                  src={primaryImage}
                  id={alt.replace(/\s+/g, '-').toLowerCase()}
                  className={className}
                  alt={alt + 'Primary Icon'}
                  height={height || 20}
                  width={width || 20}
                  name={`${alt}Button`}
                />
              ) : (
                <CustomLazyLoadImage
                  src={secondaryImage}
                  id={alt.replace(/\s+/g, '-').toLowerCase()}
                  className={className}
                  alt={alt + 'Secondary Icon'}
                  height={height || 20}
                  width={width || 20}
                  name={`${alt}Button`}
                />
              )}
              {count && count !== 0 ? (
                <div className='count'>{count}</div>
              ) : null}
            </div>
          )}
        </div>
      )
    case 'header-bottom':
      newUrl = ItmGenerator(
        (pageType ? pageType + '-' : '') + 'sticky-' + title,
        link,
        'homepage',
        '',
        '',
      )
      switch (title) {
        case 'Inspirasi':
          mixpanelEvent = 'Ide dan Inspirasi'
          mixpanelProperties = { 'Click Location': 'navigation' }
          break
        case 'Transaksi':
          if (isGuest) {
            mixpanelEvent = 'Cek Status Pesanan Saya'
          } else {
            mixpanelEvent = 'Cek Status Pesanan Guest'
          }
          mixpanelProperties = { 'Click Location': 'navigation' }
          break
        case 'Akun Saya':
          mixpanelEvent = 'Profil Saya'
          if (config.environment === 'mobile' && !config.isB2b) {
            newUrl = config.baseURL + 'my-account/profile'
          }
          break
        case 'Beranda':
          mixpanelEvent = 'View Beranda 3.0'
          break
        default:
          mixpanelEvent = 'View Beranda 3.0'
          break
      }
      return (
        <a
          onClick={() => mixpanelTrack(mixpanelEvent, mixpanelProperties)}
          href={newUrl}
          className={className}
        >
          <div className='icon'>
            <CustomLazyLoadImage
              id={alt.replace(/\s+/g, '-').toLowerCase()}
              src={primaryImage}
              className={className}
              alt={alt + ' Icon'}
              height={height || 20}
              width={width || 20}
              name={`${title}Button`}
            />
            {isBadge && <div className='badge' />}
          </div>
          {constantsContext?.pageFrom === from && <hr className='hr-bottom' />}
          <div className='button-small-text title'>{title}</div>
        </a>
      )

    default:
      return null
  }
}
