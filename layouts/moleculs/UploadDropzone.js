import React, { useCallback, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import config from '../../../../config'
import { useUploadReviewImage } from '../../services/api/mutations/global'

export default function UploadDropzone({
  id,
  handleModifiedImage,
  folderName,
  handleErrorMsg,
}) {
  const [file, setFile] = useState('')
  // add the upload endpoint here, for now this only for review rating
  const { mutate: uploadImageReviewRating, isLoading } = useUploadReviewImage({
    onSuccess: (response) => {
      if (response?.data?.data?.imageUrl) {
        handleModifiedImage(id, response?.data?.data?.imageUrl, 'insert')
        setFile(response?.data?.data?.imageUrl)
      }
    },
    onError: () => {
      handleErrorMsg('Terjadi kesalahan, mohon ulangi beberapa saat lagi')
    },
  })

  function deleteImage() {
    if (window.confirm('Delete this image?')) {
      handleModifiedImage(id, '', 'delete')
      setFile('')
    }
  }

  const onDrop = useCallback((acceptedFiles) => {
    handleErrorMsg()
    acceptedFiles.forEach((file) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      // check error here
      if (file) {
        if (file.size >= 2 * 1024000) {
          handleErrorMsg('Ukuran foto tidak boleh lebih dari 2MB')
        } else {
          // reader.onabort = () => console.log('file reading was aborted')
          // reader.onerror = () => console.log('file reading has failed')
          reader.onload = () => {
            // Do whatever you want with the file contents
            const data = {
              image: reader.result,
              folder: folderName,
              uploadId: id,
            }
            // add the folder name here
            if (folderName === 'reviewRating') {
              uploadImageReviewRating({ data })
            }
          }
        }
      }
    })
  }, [])
  function showImage() {
    if (isLoading) {
      return (
        <img
          src={config.assetsURL + 'icon/loading-ruparupa.gif'}
          width='100%'
          height='100%'
        />
      )
    } else {
      if (file) {
        return <img src={file} width='100%' height='100%' />
      }
    }
  }

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: 'image/jpeg',
  })

  return (
    <div className='dropzone-area'>
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        <div className=' dropzone-area__upload'>
          <div className='icon-plus'>+</div>
          <div
            className='dropzone-area__preview'
            style={isLoading ? { backgroundColor: 'white' } : {}}
          >
            {showImage()}
          </div>
        </div>
      </div>
      {file && (
        <div
          className='dropzone-area__delete'
          onClick={() => {
            deleteImage()
          }}
        >
          <img src={config.assetsURL + 'icon/trash.svg'} />
        </div>
      )}
    </div>
  )
}
