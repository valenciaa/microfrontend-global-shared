import { useEffect, useState } from 'react'
import { useCartAuthContext } from '../../context/CartAuthContext'
import Button from '../atoms/Button'
import Modal from '../templates/Modal'
import config from '../../../../config'
import isEmpty from 'lodash/isEmpty'
import Cookies from 'js-cookie'
import { NumberWithCommas } from '../../utils'

export default function SignUpEventModal() {
  const { state: cartAuthContext } = useCartAuthContext()
  const auth = cartAuthContext?.auth
  const [onClickedSignUpEventModal, setOnClickedSignUpEventModal] =
    useState(false)

  useEffect(() => {
    const voucherRegister = Cookies.get('voucher-register')
    if (voucherRegister) {
      setOnClickedSignUpEventModal(true)
      Cookies.remove('voucher-register')
    }
  }, [])

  const handleCloseSignUpEventModal = (e, onClose) => {
    e.preventDefault()
    onClose()
    Cookies.remove('voucher-register')
  }

  function renderSignUpEventModalBodyContent(functionInsideModal) {
    let countVoucher = 6
    let price = 150000
    if (config.companyCode === 'ODI') {
      price = 150000
    }
    if (
      ['toys kingdom indonesia', 'google toys'].includes(
        auth.user.registered_by,
      )
    ) {
      countVoucher = 1
      price = 50000
    }
    return (
      // <div className={homepageStyles['deals-tnc']}>
      <div className=''>
        <div className='row'>
          <div className='col-xs-12 text-center'>
            <img
              src={`${config.assetsURL}images/image-after-registration.png`}
              alt='sign-up-event'
            />
          </div>
          <div className='margin-top-xl text-center'>
            Selamat registrasi Anda telah berhasil! {countVoucher} voucher
            dengan total Rp{NumberWithCommas(price)} telah berhasil ditambahkan
            ke akun Anda. Yuk cek sekarang!
          </div>
          <div className='row'>
            <div className='col-xs-12 margin-top-xl'>
              <a href={`${config.baseURL}my-account/my-voucher`}>
                <Button size='large' type='primary' width='100%'>
                  Lihat Voucher Saya
                </Button>
              </a>
            </div>
            <div className='col-xs-12 margin-top-s'>
              <Button
                size='large'
                type='primary-border'
                width='100%'
                handleOnClick={(e) =>
                  handleCloseSignUpEventModal(e, functionInsideModal)
                }
              >
                Kembali ke Home
              </Button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  return (
    <>
      {onClickedSignUpEventModal && !isEmpty(auth) && (
        <Modal
          headerElement={`Hi, ${
            (auth?.user?.name || auth?.user?.first_name) +
            (auth?.user?.last_name ? ' ' + auth?.user?.last_name : '')
          }`}
          bodyElement={renderSignUpEventModalBodyContent}
          onClose={() => setOnClickedSignUpEventModal(false)}
          show={onClickedSignUpEventModal}
        />
      )}
    </>
  )
}
