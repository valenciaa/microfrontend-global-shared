import React from 'react'
import config from '../../../../config'

export default function InputRadioCheckbox({
  type = 'radio',
  checked = false,
  action,
  name,
  id,
  label,
  labelLeft = false,
  labelRight = false,
  labelSpacing,
  value,
  additionalClass = '',
  labelClass = '',
  disabled = false,
  htmlFor = '',
  pageFrom = '',
  manipulateLabel,
  fontWeight = 400,
}) {
  // jason - name dan htmlfor akan selalu sama
  // if you want to able to check on label or its row, then id and htmlfor must have the same value
  if (pageFrom) {
    htmlFor = id
  }
  return (
    <label htmlFor={htmlFor}>
      <div
        className={`input-radio-checkbox input-radio-checkbox__${
          config.companyNameCSS
        } ${
          labelSpacing
            ? 'input-radio-checkbox__spacing'
            : 'input-radio-checkbox__inline'
        } ${additionalClass}`}
      >
        {labelLeft && (
          <label
            className={labelClass}
            htmlFor={htmlFor}
            style={{ fontWeight }}
          >
            {label}
            {manipulateLabel || null}
          </label>
        )}
        <input
          className={labelSpacing ? '' : ''}
          type={type}
          name={name}
          id={id}
          checked={checked}
          onChange={(e) => action && action(e)}
          value={value}
          disabled={disabled}
        />
        {labelRight && (
          <label
            className={labelClass}
            htmlFor={htmlFor}
            style={{ fontWeight }}
          >
            {label}
            {manipulateLabel || null}
          </label>
        )}
      </div>
    </label>
  )
}
