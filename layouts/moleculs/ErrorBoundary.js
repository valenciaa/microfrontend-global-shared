import React from 'react'
// import { TrackJS } from 'trackjs'
import config from '../../../../config'

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = { error: null }
  }

  // componentDidMount () {
  //   window.addEventListener('unhandledrejection', function (event) {
  //   //   TrackJS.track(event.reason)
  //   })
  // }

  componentDidCatch(error) {
    // if (errorInfo && errorInfo.componentStack) {
    //   console.log(errorInfo.componentStack)
    // }

    // TrackJS.track(error)
    if (!config.isLiveSite) {
      console.log('Error', error)
    }
    this.setState({ error })
  }

  render() {
    if (this.state.error) {
      if (this.props.customErrMsg) {
        console.log(this.props.customErrMsg)
      }
      // You can render any custom fallback UI
      return (
        <div className='error-fallback'>
          <div className='heading-2'>Something went wrong.</div>
          <div className='ui-text-2'>
            Sorry, something went wrong and we couldn`t process your request.
          </div>
        </div>
      )
    }

    return this.props.children
  }
}

export default ErrorBoundary
