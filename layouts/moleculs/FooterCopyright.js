import dayjs from 'dayjs'
import React from 'react'
import CustomLazyLoadImage from '../../../global/layouts/atoms/CustomLazyLoadImage'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'

export default function FooterCopyright() {
  const yearNow = dayjs().year()

  return (
    <>
      <div className='footer-copyright-top'>
        <a href='https://www.kawanlamagroup.com/'>
          <CustomLazyLoadImage
            className='margin-left-xs'
            src={config.assetsURL + 'images/logo-kl.png'}
            width='182'
            height='12'
            alt='Logo KL'
            name='logoKL'
            placeholder={
              <Skeleton className='margin-left-xs' height={18} width={300} />
            }
          />
        </a>
      </div>
      <div className='footer-copyright-bottom'>
        <p> &copy; {yearNow} PT Omni Digitama Internusa</p>
        {config.debugVersion ? (
          <p className='debug-version footer-debug'>{config.debugVersion}</p>
        ) : null}
      </div>
    </>
  )
}
