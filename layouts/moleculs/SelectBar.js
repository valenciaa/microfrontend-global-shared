import isEmpty from 'lodash/isEmpty'
import React from 'react'
export default function SelectBar({
  action,
  options,
  name,
  placeholder,
  value,
  autoComplete = 'off',
  additionalClass,
  id,
  disabled = false,
}) {
  return (
    <div className='input-bar-container'>
      <select
        className={'select-bar' + (additionalClass || '')}
        disabled={disabled}
        name={name}
        placeholder={placeholder || ''}
        onChange={(e) => action && action(e)}
        value={value}
        autoComplete={autoComplete}
        id={id || ''}
      >
        {value}
        <option value='' />
        {!isEmpty(options) &&
          options?.map((opt, idx) => {
            return (
              <option
                key={idx}
                value={opt?.value || ''}
                disabled={opt?.disabled || false}
              >
                {opt.label || ''}
              </option>
            )
          })}
      </select>
    </div>
  )
}
