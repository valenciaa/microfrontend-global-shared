import React, { useEffect, useState } from 'react'
import { SwiperSlide } from 'swiper/react'
import config from '../../../../config'
import ProductDetailImageItem from '../../../../src/pdp/components/media-section/ProductDetailImageItem'
import { MemoizedProductDetailVideoItem } from '../../../../src/pdp/components/media-section/ProductDetailVideoItem'
import ProductDetailSlider from '../../../../src/pdp/components/ProductDetailSlider'
import setViewportHeightWithoutAddressBar from '../../../../src/utils/SetViewportHeightWithoutAddressBar'
import LightboxNavigaton from './LightboxNavigation'

export default function Lightbox({
  children,
  closeButtonComponent,
  closeLightbox,
  data,
  isThumbnail,
  lightboxPagination,
  productName,
  productDetailStyles,
  productSku,
  from = '',
  ...props
}) {
  const [currentIndex, setCurrentIndex] = useState()
  useEffect(() => {
    if (config.environment === 'mobile') {
      setViewportHeightWithoutAddressBar()
    }

    window.addEventListener('resize', setViewportHeightWithoutAddressBar)

    return () => {
      window.removeEventListener('resize', setViewportHeightWithoutAddressBar)
    }
  }, [])
  return (
    <div className='lightbox'>
      <LightboxNavigaton
        closeButtonComponent={closeButtonComponent}
        closeLightbox={closeLightbox}
        pagination={lightboxPagination}
      />
      <div className='lightbox__content'>
        <ProductDetailSlider
          onSlideChange={(swiper) => setCurrentIndex(swiper?.realIndex)}
          preloadImages={true}
          {...props}
        >
          {data?.map((data, index) => {
            return (
              <SwiperSlide key={index}>
                {data?.type === 'image' && (
                  <ProductDetailImageItem
                    index={index}
                    slide={data}
                    productName={productName}
                    className={
                      productDetailStyles['product-media__slide'] +
                      ' product-media__slide'
                    }
                    isWatermark
                    from={from}
                  />
                )}
                {data?.type === 'video' && (
                  <MemoizedProductDetailVideoItem
                    index={index}
                    currentIndex={currentIndex}
                    video={data}
                    productName={productName}
                    productSku={productSku}
                    className={
                      config.environment === 'mobile'
                        ? productDetailStyles['product-media__slide-video']
                        : 'product-media__slide-video'
                    }
                    isThumbnail={isThumbnail}
                  />
                )}
              </SwiperSlide>
            )
          })}
        </ProductDetailSlider>
        {children}
      </div>
    </div>
  )
}
