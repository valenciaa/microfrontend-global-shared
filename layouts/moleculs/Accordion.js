import React, { useRef, useState, useEffect } from 'react'
import Chevron from '../atoms/Chevron'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import { useHomepageContext } from '../../../../src/homepage/context/HomepageContext'
import { useConstantsContext } from '../../context/ConstantsContext'

export default function Accordion(props) {
  const [setActive, setActiveState] = useState('')
  const [setHeight, setHeightState] = useState('0px')
  const [setRotate, setRotateState] = useState('accordion__icon')
  const link = props.link || false
  const page = props.page || false
  const homepageContext = useHomepageContext()
  const homepageStyles = homepageContext?.homepageStyles
  const content = useRef(null)
  const constantsContext = useConstantsContext()

  useEffect(() => {
    if (props?.autoActive) {
      toggleAccordion()
    }
  }, [])

  function toggleAccordion() {
    if (!props.url) {
      setActiveState(setActive === '' ? 'active' : '')
      setHeightState(
        setActive === 'active'
          ? '0px'
          : `${content.current.scrollHeight + 10}px`,
      )
      setRotateState(
        setActive === 'active'
          ? 'accordion__icon'
          : page === 'hyde-living'
            ? 'accordion__icon rotate ' + homepageStyles.rotate
            : 'accordion__icon rotate',
      )
    } else {
      window.open(props.url)
    }
  }
  return (
    <div
      className={'accordion__section ' + props?.sectionClass || ''}
      onClick={() => {
        if (!props?.extendedOnButton) {
          if (link) {
            window.location.href = link
          } else {
            toggleAccordion()
          }
        }
      }}
    >
      <div
        className={`accordion__container ${props?.containerClass || ''} ${
          page === 'hyde-living' ? homepageStyles.accordion__container : ''
        }`}
      >
        <button
          className={`accordion ${setActive} ${props?.buttonClass || ''} ${
            page === 'hyde-living' ? homepageStyles.accordion : ''
          }`}
          name={`${props.title.replace(/\s+/g, '-').toLowerCase()}`}
          onClick={() => {
            if (props?.extendedOnButton) {
              if (link) {
                window.location.href = link
              } else {
                toggleAccordion()
              }
            }
            if (props.title === 'E-catalogue') {
              mixpanelTrack('View E-catalogue')
              mixpanelTrack('E-Catalogue', {
                Location:
                  constantsContext?.pageFrom === 'home' ? 'Homepage' : 'PCP',
              })
            }
          }}
        >
          {props?.titleContent ? (
            props?.titleContent
          ) : (
            <p
              className={`accordion__title ${
                page === 'hyde-living' ? homepageStyles.accordion__title : ''
              }`}
            >
              {props.title}
            </p>
          )}
          <Chevron
            className={`${setRotate} ${
              props.customFillChevron ? props.customFillChevron : ''
            }`}
            width={10}
            fill={!props.customFillChevron ? '#777' : ''}
          />
        </button>
        <div
          ref={content}
          style={{ maxHeight: `${setHeight}` }}
          className={`accordion__content ${props?.contentClass} ${
            page === 'hyde-living' ? homepageStyles.accordion__content : ''
          }`}
        >
          <div
            className={`accordion__text ${props?.textClass} ${
              page === 'hyde-living' ? homepageStyles.accordion__text : ''
            }`}
          >
            {props.content}
          </div>
        </div>
      </div>
    </div>
  )
}
