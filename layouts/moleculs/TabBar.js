import Link from 'next/link'
import { useRouter } from 'next/router'
import isEmpty from 'lodash/isEmpty'
import config from '../../../../config'
import { useEffect, useState } from 'react'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'

/**
 * Tab Bar that designed for unify project
 * @param {array} tabOptions Options data for tab component, each object data has property name & tabParamValue
 * @returns {component}
 */
export default function TabBar({ tabOptions }) {
  const router = useRouter()
  const [paramTab, setParamTab] = useState('')
  const [paramChildTab, setParamChildTab] = useState('')

  useEffect(() => {
    if (router.isReady) {
      if (config.environment === 'mobile') {
        setParamTab(router.query['tab'] || tabOptions?.[0].tabParamValue || '')
      } else {
        setParamTab(router.query.tab || tabOptions?.[0].tabParamValue || '')
        setParamChildTab(
          router.query['child_tab'] || tabOptions?.[0].childTabParamValue || '',
        )
      }
    }
  }, [router])

  return (
    <ul
      className={`flex tab-bar ${
        config.environment === 'desktop' ? 'tab-bar__container--desktop' : ''
      }`}
    >
      {!isEmpty(tabOptions) &&
        tabOptions.map((value, index) => {
          let isActive
          let redirectUrl
          if (config.environment === 'mobile') {
            isActive = paramTab === value.tabParamValue
            redirectUrl = `${router.pathname}?tab=${value.tabParamValue}`
          } else {
            isActive =
              paramTab === value.tabParamValue &&
              paramChildTab === value.childTabParamValue
            redirectUrl = `${router.pathname}?tab=${value.tabParamValue}&child_tab=${value.childTabParamValue}`
          }
          if (paramTab === 'installation' && paramTab === value.tabParamValue) {
            mixpanelTrack('Tab Perakitan Pesanan Saya')
          }
          return (
            <li
              key={`tab-${index}`}
              className={`${
                config.environment === 'mobile' ? 'flex-1' : 'margin-right-m'
              } padding-xs text-center tab-bar__item${
                isActive ? `--active-${config.companyNameCSS}` : ''
              } cursor-pointer`}
            >
              <Link href={redirectUrl} replace shallow>
                <div className={`text-${config.environment}`}>{value.name}</div>
              </Link>
              {typeof value?.badge === 'number' && value.badge > 0 && (
                <div
                  className={`flex items-center justify-center tab-bar__badge-${config.companyNameCSS}`}
                >
                  {value.badge}
                </div>
              )}
            </li>
          )
        })}
    </ul>
  )
}
