import React from 'react'
import { ProgressBar, Step } from 'react-step-progress-bar'
import 'react-step-progress-bar/styles.css'

/**
 * Custom Progress Bar Component
 * @param {string} stepClass Custom className for accomplished
 * @param {array} steps List of steps status
 * @param {number} currentStep current steps status
 * @param {string} customLabelStyle Custom className for labelname
 * @param {string} customContainerStyle Custom className for container progress bar
 * @returns {component}
 */

export default function TrackingProgressBar({
  currentStep,
  steps,
  stepClass,
  customLabelStyle,
  labelColor,
  customContainerStyle,
}) {
  return (
    <div className={`progress-bar ${customContainerStyle}`}>
      <ProgressBar
        height='2px'
        percent={(currentStep * 100) / (steps?.length - 1)}
        filledBackground='#F26A24'
      >
        {steps?.map((label, index) => (
          <Step key={index} label={label}>
            {({ accomplished }) => (
              <>
                <p
                  className={`progress-bar-label ${
                    accomplished ? 'color-grey-100' : 'color-grey-30'
                  } ${customLabelStyle} ${labelColor}`}
                >
                  {label}
                </p>
                <div
                  className={`step ${
                    accomplished ? `step--completed ${stepClass}` : null
                  } `}
                />
              </>
            )}
          </Step>
        ))}
      </ProgressBar>
    </div>
  )
}
