import Modal from '../templates/Modal'

export default function TnCModalV2({ onClose, show, tierName }) {
  const modalTnCBody = () => {
    return (
      <ol>
        <li>Voucher Cashback berlaku 14 hari setelah voucher dikirimkan</li>
        <li>
          Voucher Cashback dapat digunakan tanpa minimum transaksi, dengan
          maksimum penggunaan voucher 5 kali per karyawan dalam satu bulan
        </li>
        <li>
          Penggunaan Voucher Cashback tidak dapat digabung dengan promo maupun
          voucher cashback lainnya
        </li>
        <li>Promo ini tidak mendapatkan poin untuk member</li>
        <li>
          Voucher Cashback hanya berlaku untuk pembelanjaan produk yang
          dikirimkan oleh Ruparupa, kecuali produk Flash Sale, produk bertkamu
          `Voucher tidak berlaku`, produk di kategori Voucher & Jasa, dan
          pembelanjaan yang bersifat komersil/project
        </li>
        <li>
          Apabila terjadi kegagalan saat melakukan transaksi atau order
          dibatalkan maka voucher akan hangus dan tidak dapat digunakan kembali
        </li>
        <li>
          Voucher jika pembelian Kamu melebihi nominal yang tertera di voucher,
          maka Kamu harus membayar biaya kekurangannya
        </li>
        <li>Promo/Voucher tidak berlaku untuk pembayaran menggunakan GoPay</li>
        <li>Voucher Cashback berlaku 14 hari setelah voucher dikirimkan</li>
        <li>Produk yang sudah dibeli tidak dapat ditukar atau dikembalikan</li>
        <li>
          Dengan menggunakan promo ini, berarti pelanggan dianggap mengerti dan
          menyetujui syarat dan ketentuan yang berlaku di Ruparupa
        </li>
        <li>
          Promo hanya berlaku untuk penggunaan pribadi dan tidak boleh
          diperjualbelikan untuk mendapatkan keuntungan
        </li>
      </ol>
    )
  }

  return (
    <>
      {show && (
        <Modal
          modalType='top'
          customStyled='p-center'
          backdropOnClose
          headerElement={`Syarat dan Ketentuan ${tierName}`}
          contentClass='mw-40 pl-2 pr-2 padding-bottom-xs'
          dialogClass='mw-none'
          headerClass='bd-none pl-none pr-none'
          bodyClass='modalv2-body'
          headerSecClass='flex justify-between'
          headerElementClass='mw-27 text-left'
          isV2
          bodyElement={modalTnCBody}
          onClose={onClose}
          show={show}
        />
      )}
    </>
  )
}
