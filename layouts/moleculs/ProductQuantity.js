import { useMemo } from 'react'
import QuantityPlus from '../../../../public/static/icon/icon-plus.svg'
import QuantityMinus from '../../../../public/static/icon/icon-minus.svg'
import isEmpty from 'lodash/isEmpty'
import clsx from 'clsx'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import { useProductDetailMutableContext } from '../../../../src/pdp/contexts/ProductDetailMutableContext'
import dayjs from 'dayjs'

export default function ProductQuantity({
  errorStore,
  selectedStoreStock,
  productMaxStock,
  productDetail,
  productDetailStyles,
  quantity,
  setAdditionalError,
  setError,
  setErrorStore,
  setQuantity,
  ...props
}) {
  const { state: productDetailMutableContext } =
    useProductDetailMutableContext()
  // const events = productDetail?.events
  // const isFlashSaleOn =
  //   !isEmpty(events) &&
  //   dayjs().isSameOrAfter(events?.start_date) &&
  //   dayjs().isSameOrBefore(events?.end_date)

  const events = useMemo(() => productDetail?.events, [productDetail])
  const isFlashSaleOn = useMemo(() => {
    if (!isEmpty(events)) {
      const filteredEvent = events.filter((event) => {
        return (
          event.url_key.includes('flash-sale') &&
          dayjs().isSameOrAfter(event?.start_date) &&
          dayjs().isSameOrBefore(event?.end_date)
        )
      })

      return filteredEvent.length > 0
    }
    return false
  }, [events])

  const getDisableDecreaseButton = () => {
    return parseInt(quantity) <= 1 || quantity === '' || isFlashSaleOn
  }

  const getDisableIncreaseButton = () => {
    //   if (selectedStoreStock && selectedStoreStock <= 10) {
    //     return quantity === selectedStoreStock
    //   }
    return isFlashSaleOn
  }

  const getDisableDirectInput = () => {
    return isFlashSaleOn
  }

  const checkMaximumStock = (maxStock, stock, from) => {
    const {
      is_apply_multiple: isApplyMultiple = 0,
      minimum_order: minimumOrder = 0,
    } = productDetail || {}
    const isHaveMininumOrder = minimumOrder > 1

    // This one get from what you choose in secation Choose Store Location
    const maxStockStore = !isEmpty(productDetailMutableContext?.selectedStore)
      ? productDetailMutableContext?.selectedStore?.qty
      : 0

    if (
      stock <= 0 ||
      (isHaveMininumOrder && stock < minimumOrder) ||
      (stock % minimumOrder !== 0 && isApplyMultiple)
    ) {
      if (isHaveMininumOrder) {
        const errorMessage = `Produk hanya dapat dibeli minimal ${minimumOrder} buah${
          isApplyMultiple ? ' atau kelipatan' : ''
        }`
        setError(errorMessage)
      }
      setQuantity(minimumOrder)
    } else {
      if (stock > maxStock || (maxStockStore !== 0 && stock > maxStockStore)) {
        // max stock should be more than max stock store code, because it take the higher stock from store
        if (maxStock !== maxStockStore) {
          // if its not same we need to check the max store code first so alert that show up first is store code info box
          // check max stock store first
          if (maxStockStore !== 0 && stock > maxStockStore) {
            if (isApplyMultiple) {
              const errorMessage = `Produk hanya dapat dibeli minimal ${minimumOrder} buah${
                isApplyMultiple ? ' atau kelipatan' : ''
              }`
              const additionalErrorMessage = `Produk ini hanya tersedia ${maxStockStore} buah`
              setError(errorMessage)
              setAdditionalError(additionalErrorMessage)
              if (selectedStoreStock) {
                const errorMessage =
                  'Jumlah stok toko tidak mencukupi. Alihkan ke toko terdekat untuk membeli jumlah yang kamu inginkan.'
                setErrorStore({
                  ...errorStore,
                  isApplyMultiple,
                  message: errorMessage,
                })
              }
            } else {
              const errorMessage =
                'Jumlah stok toko tidak mencukupi. Alihkan ke toko terdekat untuk membeli jumlah yang kamu inginkan.'
              setErrorStore({ ...errorStore, message: errorMessage })
              setQuantity(maxStockStore)
            }
          } else {
            // check max stock global second
            if (maxStock === 0) {
              setQuantity(1)
            } else {
              if (isApplyMultiple) {
                if (from === 'input') {
                  setQuantity(minimumOrder)
                }
                const errorMessage = `Produk hanya dapat dibeli minimal ${minimumOrder} buah${
                  isApplyMultiple ? ' atau kelipatan' : ''
                }`
                const additionalErrorMessage = `Produk ini hanya tersedia ${maxStock} buah`
                setError(errorMessage)
                setAdditionalError(additionalErrorMessage)
                if (selectedStoreStock) {
                  const errorMessage =
                    'Jumlah stok toko tidak mencukupi. Alihkan ke toko terdekat untuk membeli jumlah yang kamu inginkan.'
                  setErrorStore({
                    ...errorStore,
                    isApplyMultiple,
                    message: errorMessage,
                  })
                }
              } else {
                if (selectedStoreStock) {
                  const errorMessage =
                    'Jumlah stok toko tidak mencukupi. Alihkan ke toko terdekat untuk membeli jumlah yang kamu inginkan.'
                  setErrorStore({ ...errorStore, message: errorMessage })
                } else {
                  const errorMessage = `Produk ini hanya tersedia ${maxStock} buah`
                  setError(errorMessage)
                  setQuantity(maxStock)
                }
                setQuantity(maxStock)
              }
            }
          }
        } else {
          if (maxStock === 0) {
            setQuantity(1)
          } else {
            if (isApplyMultiple) {
              if (from === 'input') {
                setQuantity(minimumOrder)
              }
              const errorMessage = `Produk hanya dapat dibeli minimal ${minimumOrder} buah${
                isApplyMultiple ? ' atau kelipatan' : ''
              }`
              const additionalErrorMessage = `Produk ini hanya tersedia ${maxStock} buah`
              setError(errorMessage)
              setAdditionalError(additionalErrorMessage)
            } else {
              const errorMessage = `Produk ini hanya tersedia ${maxStock} buah`
              setError(errorMessage)
              setQuantity(maxStock)
            }
          }
        }
      } else {
        setQuantity(stock)
        setError('')
        setAdditionalError('')
        setErrorStore({ ...errorStore, isApplyMultiple: false, message: null })
      }
    }
    // else if (stock > maxStock) {
    //   if (maxStock === 0) {
    //     setQuantity(1)
    //   } else {
    //     if (isApplyMultiple) {
    //       if (from === 'input') setQuantity(minimumOrder)
    //       const errorMessage = `Produk hanya dapat dibeli minimal ${minimumOrder} buah${isApplyMultiple ? ' atau kelipatan' : ''}`
    //       const additionalErrorMessage = `Produk ini hanya tersedia ${maxStock} buah`
    //       setError(errorMessage)
    //       setAdditionalError(additionalErrorMessage)
    //       if (selectedStoreStock) {
    //         const errorMessage = 'Jumlah stok toko tidak mencukupi. Alihkan ke toko terdekat untuk membeli jumlah yang kamu inginkan.'
    //         setErrorStore({ ...errorStore, isApplyMultiple, message: errorMessage })
    //       }
    //     } else {
    //       if (selectedStoreStock) {
    //         const errorMessage = 'Jumlah stok toko tidak mencukupi. Alihkan ke toko terdekat untuk membeli jumlah yang kamu inginkan.'
    //         setErrorStore({ ...errorStore, message: errorMessage })
    //       } else {
    //         const errorMessage = `Produk ini hanya tersedia ${maxStock} buah`
    //         setError(errorMessage)
    //       }
    //       setQuantity(maxStock)
    //     }
    //   }
    // } else {
    //   setQuantity(stock)
    //   setError('')
    //   setAdditionalError('')
    //   setErrorStore({ ...errorStore, isApplyMultiple: false, message: null })
    // }
  }

  const handleClickQuantityStepperButton = (command, itemName, sku) => {
    const {
      is_apply_multiple: isApplyMultiple = 0,
      minimum_order: minimumOrder = 0,
    } = productDetail || {}
    const minimumOrderQty = parseInt(isApplyMultiple) === 1 ? minimumOrder : 1
    const stock =
      command === 'Deduct'
        ? quantity - minimumOrderQty
        : quantity + minimumOrderQty
    // checkMaximumStock(selectedStoreStock ?? productMaxStock?.max_stock, stock)
    checkMaximumStock(productMaxStock?.max_stock, stock)
    mixpanelTrack(command + ' Item', {
      'Item ID': sku,
      'Item Name': itemName.toLowerCase() || 'None',
      'Location': 'PDP',
    })
  }

  const handleChangeQuantity = (e, itemName, sku) => {
    const stock = +quantity

    // setQuantity(e.target.value)
    // checkMaximumStock(selectedStoreStock ?? productMaxStock?.max_stock, stock, 'input')
    checkMaximumStock(productMaxStock?.max_stock, stock, 'input')
    mixpanelTrack('Update Item Product', {
      'Item ID': sku,
      'Item Name': itemName.toLowerCase() || 'None',
      'Quantity': parseInt(e.target.value),
    })
  }

  return (
    <div
      className={
        'flex item product-quantity ' + productDetailStyles['product-quantity']
      }
    >
      <button
        disabled={getDisableDecreaseButton()}
        onClick={() =>
          handleClickQuantityStepperButton(
            'Deduct',
            productDetail?.name,
            productDetail?.variants?.[0]?.sku,
          )
        }
      >
        <QuantityMinus
          className={clsx(
            { 'color-grey-100': !getDisableDecreaseButton() },
            { 'color-grey-30': getDisableDecreaseButton() },
          )}
          {...props}
        />
      </button>
      <input
        className={clsx('color-grey-100 product-quantity__input ui-text-3')}
        min={productDetail?.minimumOrder > 1 ? productDetail?.minimumOrder : 1}
        max={selectedStoreStock ?? productMaxStock?.max_stock}
        value={quantity}
        name={`${productDetail?.variants?.[0]?.sku}-qty`}
        pattern='([0-9])'
        onBlur={(e) =>
          handleChangeQuantity(
            e,
            productDetail?.name,
            productDetail?.variants?.[0]?.sku,
          )
        }
        onChange={(e) => {
          const cleanedValue = (e.target.value + '')
            .replace(/\D/g, '')
            .substring(0, 4)
          setQuantity(cleanedValue)
        }}
        onKeyPress={(e) => {
          if (e.key === 'e' || e.key === '.') {
            e.preventDefault()
          }
        }}
        disabled={getDisableDirectInput()}
      />
      <button
        disabled={getDisableIncreaseButton()}
        onClick={() =>
          handleClickQuantityStepperButton(
            'Additional',
            productDetail?.name,
            productDetail?.variants?.[0]?.sku,
          )
        }
      >
        {/* className={clsx({ 'color-grey-100': !getDisableIncreaseButton() }, { 'color-grey-30': getDisableIncreaseButton() })} */}
        <QuantityPlus
          className={clsx(
            { 'color-grey-100': !getDisableIncreaseButton() },
            { 'color-grey-30': getDisableIncreaseButton() },
          )}
          {...props}
        />
      </button>
    </div>
  )
}
