import { useRef } from 'react'
import dayjs from 'dayjs'
import ReactDatePicker from 'react-datepicker'
import config from '../../../../config'
import Chevron from '../atoms/Chevron'

import 'react-datepicker/dist/react-datepicker.css'

export default function CalendarDateRangePicker({
  startDate,
  endDate,
  onChange,
  minDate = null,
}) {
  const calendarRef = useRef()

  const handleNavigateMonth = (type = 'decrease') => {
    const { decreaseMonth, increaseMonth } =
      calendarRef.current?.calendar?.instanceRef ?? {}
    if (type === 'decrease') {
      decreaseMonth?.(1)
    } else {
      increaseMonth?.(1)
    }
  }

  return (
    <div
      className={`flex items-center calendar-date-range-picker calendar-date-range-picker--${config.companyNameCSS}`}
    >
      <div
        className='padding-xs margin-right-m chevron-left'
        onClick={() => handleNavigateMonth('decrease')}
      >
        <Chevron width={12} />
      </div>
      <ReactDatePicker
        ref={calendarRef}
        inline
        selectsRange
        disabledKeyboardNavigation
        calendarClassName='calendar-date-range-picker__container'
        maxDate={new Date()}
        minDate={minDate}
        monthsShown={2}
        calendarStartDay={1}
        onChange={onChange}
        selected={startDate}
        startDate={startDate}
        endDate={endDate}
        renderCustomHeader={({ monthDate }) => (
          <div className='button-medium-text margin-bottom-xs'>
            {dayjs(monthDate.toDateString()).format('MMMM YYYY').toString()}
          </div>
        )}
        renderDayContents={(day, date) => (
          <span
            className={`button-regular-text ${
              dayjs(date).day() === 0 ? 'color-red-50' : ''
            }`}
          >
            {day}
          </span>
        )}
      />
      <div
        className='padding-xs margin-left-m'
        onClick={() => handleNavigateMonth('increase')}
      >
        <Chevron width={12} />
      </div>
    </div>
  )
}
