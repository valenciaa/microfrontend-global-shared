import React from 'react'

export default function DealsTnCModal({ homepageStyles, title, tnc }) {
  return (
    <div className={homepageStyles['deals-tnc']}>
      <div className={homepageStyles.header}>
        <div className='heading-2'>Syarat dan Ketentuan {title}</div>
      </div>
      <div className='ui-text-2' dangerouslySetInnerHTML={{ __html: tnc }} />
    </div>
  )
}
