import config from '../../../../config'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'

export default function ToolTip({ title = '', content = '', iconPath }) {
  return (
    <div className='tooltip'>
      {iconPath && (
        <CustomLazyLoadImage
          src={config.assetsURL + iconPath}
          className='icon'
        />
      )}
      <div className='tooltiptext'>
        <p className='title'>{title}</p>
        <p className='content'>{content}</p>
      </div>
    </div>
  )
}
