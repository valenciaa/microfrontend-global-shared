import React from 'react'
import { TourProvider } from '@reactour/tour'
import Cookies from 'js-cookie'
import config from '../../../../config'

const CoachMark = ({
  children,
  name,
  popoverLeft = 'unset',
  showDots,
  additionalClass = '',
  ...props
}) => {
  const disableBodyScroll = () => {
    document.body.style.overflow = 'hidden'
  }

  const enableBodyScroll = () => {
    document.body.style.overflow = ''

    Cookies.set('coachmark-' + name + '-' + config.companyNameCSS, 1, {
      expires: 60,
    })
  }

  return (
    <TourProvider
      disableInteraction
      scrollSmooth
      showBadge={false}
      className={additionalClass}
      showNavigation={false}
      styles={{
        close: (base) => ({
          ...base,
          color: '#FFFFFF',
          padding: 3,
          height: 16,
          width: 16,
          top: 14,
          right: 12,
        }),
        maskArea: (base) => ({ ...base, rx: 4 }),
        maskWrapper: (base) => ({ ...base, color: 'rgba(0, 0, 0, 0.55)' }),
        popover: (base) => ({
          ...base,
          backgroundColor: '#313339',
          borderRadius: 8,
          color: '#FFFFFF',
          padding: 12,
          left: popoverLeft,
          top: 12,
          showDots,
        }),
      }}
      afterOpen={disableBodyScroll}
      beforeClose={enableBodyScroll}
      onClickHighlighted={(e) => e.stopPropagation()}
      onClickMask={() => {}}
      {...props}
    >
      {children}
    </TourProvider>
  )
}

export default CoachMark
