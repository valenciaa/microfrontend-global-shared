import React from 'react'

export default function TextareaBar({
  action,
  name,
  placeholder,
  type,
  length,
  value,
  autoComplete = 'off',
  additionalClass,
  id,
  disabled = false,
  rows = 4,
}) {
  return (
    <div className='input-bar-b2b-container'>
      <textarea
        className={'input-bar' + (additionalClass || '')}
        disabled={disabled}
        type={type}
        name={name}
        placeholder={placeholder || ''}
        maxLength={length > 0 ? length : undefined}
        onChange={(e) => action && action(e)}
        value={value}
        autoComplete={autoComplete}
        id={id || ''}
        rows={rows}
      />
    </div>
  )
}
