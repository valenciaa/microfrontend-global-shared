import React from 'react'
import config from '../../../../config'
import FloatingCloseButton from '../atoms/FloatingCloseButton'

import Modal from '../templates/Modal'
import DealsTnCModal from './DealsTnCModal'

export default function TnCModal({
  eventTitle,
  from,
  homepageStyles,
  onClose,
  show,
  tnc,
  needCloseOnDesktop,
  scroll,
}) {
  // from: deals -> Flash Sale and Daily Deals
  const modalTnCBody = (functionInsideModal) => {
    switch (from) {
      case 'deals':
        return (
          <DealsTnCModal
            homepageStyles={homepageStyles}
            onClose={functionInsideModal}
            title={eventTitle}
            tnc={tnc}
          />
        )
    }
  }

  const floatingButtonModalAddToCart = (onClick) => (
    <FloatingCloseButton
      height={40}
      width={40}
      src={config.assetsURL + 'icon/close.svg'}
      onClick={() => onClick()}
    />
  )

  // Use switch case/ if else to change modalType and dialogClass
  return (
    <>
      {show && (
        <Modal
          backdropOnClose
          modalType={
            from === 'deals' && config.environment === 'desktop'
              ? 'deals-center'
              : 'bottom'
          }
          contentClass={
            config.environment === 'mobile' &&
            homepageStyles['modal-content-tnc__mobile'] + ' margin-top-xxxl'
          }
          dialogClass={
            from === 'deals' &&
            homepageStyles['modal-dialog-tnc__' + config.environment]
          }
          needFloatingCloseButtonElement={
            config.environment === 'mobile' || needCloseOnDesktop
          }
          floatingCloseButtonElement={
            (config.environment === 'mobile' || needCloseOnDesktop) &&
            floatingButtonModalAddToCart
          }
          bodyElement={modalTnCBody}
          onClose={onClose}
          show={show}
          addScroller={scroll && homepageStyles['modal-scroller']}
        />
      )}
    </>
  )
}
