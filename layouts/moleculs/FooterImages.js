import dayjs from 'dayjs'
import React from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'

export default function FooterImages({ data, title, type }) {
  const yearNow = dayjs().year()

  let companyName = 'PT. Omni Digitama Internusa'
  if (config.companyCode === 'HCI') {
    companyName = 'PT. Home Center Indonesia'
  } else if (config.companyCode === 'AHI') {
    companyName = 'PT. Aspirasi Hidup Indonesia'
  } else if (config.companyCode === 'TGI') {
    companyName = ' PT. Toys Kingdom Indonesia'
  }
  return (
    <div className='col-xs-12'>
      <div
        className='footer-section'
        style={type === 'copyright' ? { paddingBottom: 70 } : {}}
      >
        <div className='footer-title heading-3'>
          {type === 'copyright' && title === '' ? (
            <p>
              Copyright &copy; {yearNow} {companyName}
            </p>
          ) : (
            <b>{title}</b>
          )}
        </div>

        <div className='row footer-display-grid'>
          {data?.map((list, idx) => {
            let imageUrl = 'images/footer-logo/' + list.url + '.png'

            if (title === 'Cicilan 0%') {
              imageUrl = 'images/footer-logo/' + list.url
            }

            return (
              <div key={idx} className='logo logo__company'>
                <CustomLazyLoadImage
                  src={config.assetsURL + imageUrl}
                  height={30}
                  width={72}
                  alt={list.title}
                  placeholder={<Skeleton height={30} width={72} />}
                />
              </div>
            )
          })}
        </div>

        {type === 'copyright' &&
        config.environment === 'mobile' &&
        config.debugVersion ? (
          <div className='row margin-top-l text-right debug-version'>
            <div className='col-xs-12 margin-top-l'>{config.debugVersion}</div>
          </div>
        ) : null}
      </div>
    </div>
  )
}

export const MemoizedFooterImages = React.memo(FooterImages)
