import React, { useState } from 'react'
import config from '../../../../config'
import ChevronRight from '../../../../public/static/icon/chevron-right.svg'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import clsx from '../../utils/Clsx'

import Chevron from '../atoms/Chevron'
import HorizontalLine from '../atoms/HorizontalLine'
import isEmpty from 'lodash/isEmpty'

/**
 * Custom label component with handling accordion
 * ----------------------------------------------
 * @param {string} label Custom upper-left side text of the label
 * @param {string} subLabel Custom bottom-left side text of the label
 * @param {string} installationButtonLabel Custom value button for installation that have more than 1 button with position left of value
 * @param {string} installationDetailValue Custom add detail value for installation with position text top of value
 * @param {string} value Custom right side text of the value
 * @param {string} valueClassName Custom right side className of the value
 * @param {string} containerClassName Custom className for container of labelSection
 * @param {string} fontWeightLabel Custom className for font weight label
 * @param {string} fontWeightValue Custom className for font weight value
 * @param {string} fontSizeLabel Custom className for font size label
 * @param {string} fontSizeValue Custom className for font size value
 * @param {string} colorLabel Custom className for color label
 * @param {string} colorValue Custom className for color value
 * @param {string} colorInstallationValue Custom className for color value instalation
 * @param {string} colorInstallationButtonValue Custom className for button value installation
 * @param {string} type Custom type of border
 * @param {boolean} icon Show chevron at right side of value
 * @param {boolean} installationButtonIcon Show chevron at right side of value for installation
 * @param {function} handleClick Handle show/hide accordion and handle navigation
 * @param {boolean} showDetail Show detail accordion
 * @param {component} contentDetail Component for detail accordion
 * @param {boolean} hideLabelContentBottomLine To determine bottom line of label content (like dropdown) is show or not
 * @param {string} maxWidth
 * @param {string} labelMaxWidth
 * @param {component} children Component inside dropdown's tag
 * @param {component} warrantyDetail Niche conditional, used only at Warranty detail
 * @param {string} classSubLabel Custom subLabel className
 * @param {string} subImage source image
 * @param {string} classSubImage Custom subImage className
 * @returns {component}
 */
export default function LabelSection(props) {
  const {
    label = '',
    subLabel = '',
    installationButtonLabel,
    installationDetailValue,
    value = '',
    valueClassName = '',
    fontWeightLabel = 'text-medium',
    fontWeightValue = 'text-semi-bold',
    fontSizeLabel = 'ui-text-3',
    fontSizeValue = 'ui-text-3',
    colorLabel = 'color-grey-40',
    colorValue = 'color-grey-100',
    colorInstallationValue = 'color-grey-100',
    colorInstallationButtonValue = 'color-ruparupa',
    type = 'borderSolid',
    containerClassName = '',
    icon,
    installationButtonIcon,
    handleClick,
    showDetail,
    contentDetail,
    hideLabelContentBottomLine = false,
    maxWidth = '',
    labelMaxWidth = '58%',
    children = null,
    warrantyDetail,
    classSubLabel = 'color-grey-40',
    subImage,
    classSubImage,
  } = props
  const [isActive, setIsActive] = useState(false)
  const unifyBaseStyles = useUnifyAppsContext().unifyBaseStyles

  const getClassName = () => {
    const paddingHorizontalText =
      config.environment === 'mobile' ? 'container-card' : ''
    let className
    switch (type) {
      case 'borderSolid':
        className = `border border--solid ${paddingHorizontalText}`
        break
      case 'borderDashed':
        className = `border border--dashed ${paddingHorizontalText}`
        break
      case 'nonBorder':
        className = `non-border ${paddingHorizontalText}`
        break
      case 'nonBorderPadding':
        className = `non-border ${paddingHorizontalText} padding__vertical--xs`
        break
      case 'borderNoPadding':
        className = `border border--solid ${paddingHorizontalText} padding__vertical--xs`
        break
      case 'borderDashedTop':
        className = `border border--dashed-top ${paddingHorizontalText} margin-top-xs`
        break
      default:
        className = `non-border ${paddingHorizontalText}`
        break
    }
    const additionalCondition = isActive
      ? warrantyDetail
        ? 'non-border padding__vertical--l'
        : 'non-border padding-top-l'
      : className
    return additionalCondition
  }

  const handleValueClick = () => {
    if (!handleClick && showDetail) {
      setIsActive(!isActive)
    } else if (typeof handleClick === 'function') {
      handleClick()
    }
  }

  return (
    <>
      <div className={clsx(getClassName(), containerClassName)}>
        <div className='flex row justify-between'>
          <div
            className={`flex flex-col align-${
              !isEmpty(subLabel) ? 'left' : 'center'
            }`}
            style={{ maxWidth: labelMaxWidth }}
          >
            <p
              className={`${unifyBaseStyles['ellipsis-lines']} ${fontSizeLabel} ${fontWeightLabel} ${colorLabel}`}
            >
              {' '}
              {label}{' '}
            </p>
            {subLabel && (
              <div className={(subImage && 'flex gap-xs margin-top-xxs') || ''}>
                {' '}
                {/* eslint-disable-line */}
                {subImage && (
                  <img
                    src={subImage}
                    className={classSubImage}
                    width={16}
                    height={16}
                  />
                )}
                <p className={`ui-text-4 ${classSubLabel}`}> {subLabel} </p>
              </div>
            )}
          </div>
          <div className={`flex items-center ${valueClassName}`}>
            {installationDetailValue && (
              <div
                className={`label-installation ${fontSizeValue} ${fontWeightValue} ${colorInstallationValue}`}
              >
                {installationDetailValue}
              </div>
            )}
            <div
              className={`flex flex-row justify-between ${
                props?.handleClickInstallation || handleClick || showDetail
                  ? 'text-value-enabled'
                  : ''
              }`}
            >
              {installationButtonLabel && (
                <div
                  className={`${fontSizeValue} ${fontWeightValue} ${colorInstallationButtonValue} flex padding__horizontal--xs padding__vertical--none border-right`}
                  onClick={props?.handleClickInstallation}
                  style={{ width: 'auto' }}
                >
                  {installationButtonLabel}
                  {installationButtonIcon && (
                    <ChevronRight className='color-ruparupa' />
                  )}
                </div>
              )}
              {installationButtonLabel && value && (
                <div className='border-button' />
              )}
              <div
                className={clsx(
                  `${fontSizeValue} ${fontWeightValue} text-right ${colorValue} ${
                    installationButtonLabel
                      ? 'padding-left-xs padding-right-none'
                      : 'padding__horizontal--none'
                  } padding__vertical--none items-center`,
                  'tw-flex',
                )}
                style={{ maxWidth: maxWidth, width: 'auto' }}
                onClick={handleValueClick}
              >
                {value}
                {icon && ( // show icon chevron
                  <Chevron
                    className={`${
                      showDetail && `toggle-${isActive ? 'active' : 'inactive'}`
                    } color-${config.companyNameCSS} margin-left-xs`} // if showDetail true then will show toggle active inactive
                    width={7}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
        {children}
      </div>
      {icon && isActive && (
        <div className='flex-col'>
          {warrantyDetail && <HorizontalLine />}
          {contentDetail}
          {!hideLabelContentBottomLine && <HorizontalLine />}
        </div>
      )}
    </>
  )
}
