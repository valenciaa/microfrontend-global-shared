import React from 'react'

export default function InputTextArea({
  action,
  name,
  placeholder,
  maxLength,
  value,
  additionalClass,
  disabled = false,
  rows,
  cols,
}) {
  return (
    <textarea
      className={
        'input-textarea input-bar' + (additionalClass || ' input-field-desktop')
      }
      name={name}
      placeholder={placeholder || ''}
      value={value}
      maxLength={maxLength > 0 ? maxLength : undefined}
      onChange={(e) => action && action(e)}
      disabled={disabled}
      rows={rows}
      cols={cols}
    />
  )
}
