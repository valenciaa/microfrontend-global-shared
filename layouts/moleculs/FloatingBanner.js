import React from 'react'

export default function FloatingBanner({ imgUrl, link }) {
  return (
    <div id='floating-button' className='floating-button' style={{ width: 80 }}>
      <a href={link}>
        <img src={imgUrl} width='80' height='80' />
      </a>
    </div>
  )
}
