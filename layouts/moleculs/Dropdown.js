import React, { useState, useEffect } from 'react'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import clsx from 'clsx'
import isEmpty from 'lodash/isEmpty'
import Chevron from '../atoms/Chevron'
import config from '../../../../config'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'

/**
 * Custom dropdown component with custom delete/check icon
 * @param {component} children Component inside dropdown's tag
 * @param {string} className Custom className for dropdown's container
 * @param {string} dropdownListClassName Custom className for dropdown's list container
 * @param {string} textClassName Custom className for placeholder text
 * @param {array} data List of dropdown options
 * @param {string} dataTextKey Object key to get & show as dropdown label
 * @param {string} dataValueKey Object key to get & show as dropdown value
 * @param {string} dataNameKey Object key to get & show as dropdown name
 * @param {string} isDropdownOpen Flag to show dropdown list
 * @param {string} placeholder Placeholder text
 * @param {function} setIsDropdownOpen Handle show/hide the dropdown list
 * @param {object} selectedData Selected dropdown option
 * @param {function} setSelectedData Set selected dropdown option
 * @param {boolean} iconDelete Show delete icon at right side
 * @param {function} showAction Called after tap delete icon
 * @param {boolean} filterDropdownData Remove selected option after select one of dropdown option
 * @param {boolean} iconCheck Show check icon at right side
 * @param {boolean} disabled Flag to disable the dropdown list
 * @param {string} extraButton Object key to add button in dropdown list
 * @param {function} handleExtraButton Called after click extraButton
 * @returns {component}
 */
export default function Dropdown({
  className,
  dropdownListClassName,
  textDefaultClassName = 'ui-text-3',
  textClassName,
  data,
  dataTextKey,
  dataValueKey,
  dataNameKey,
  isDropdownOpen,
  placeholder,
  setIsDropdownOpen,
  selectedData,
  setSelectedData,
  iconDelete,
  showAction,
  filterDropdownData,
  iconCheck,
  disabled,
  extraButton,
  handleExtraButton,
  placeholderImage,
  dropdownCardClassName = 'flex items-center padding-xs justify-between',
  maxWidth = '',
}) {
  const unifyStyle = useUnifyAppsContext().unifyBaseStyles

  const [dropdownData, setDropdownData] = useState(data)

  useEffect(() => {
    setDropdownData(data)
  }, [data])

  const handleSelect = (selectedData) => {
    setSelectedData(selectedData)
    filterDropdownData &&
      setDropdownData(data.filter((item) => item !== selectedData))
    setIsDropdownOpen(false)
  }

  return (
    <div
      className={clsx(
        'w-100 flex-1',
        unifyStyle[`dropdown-container${disabled ? '__disabled' : ''}`],
      )}
      style={{ maxWidth }}
    >
      <div
        className={clsx(
          'flex items-center justify-between',
          className,
          unifyStyle['dropdown'],
          unifyStyle[
            `${
              isDropdownOpen && !disabled
                ? 'dropdown__active'
                : 'dropdown__inactive'
            }`
          ],
        )}
        onClick={() => setIsDropdownOpen(!isDropdownOpen)}
      >
        <div className='flex items-center gap-xs'>
          {placeholderImage && (
            <CustomLazyLoadImage
              src={placeholderImage}
              width={16}
              height={16}
              alt='placeholder-image'
            />
          )}
          <p className={clsx(textDefaultClassName, textClassName)}>
            {placeholder}
          </p>
        </div>
        <Chevron
          className={
            unifyStyle[
              `${
                isDropdownOpen && !disabled
                  ? 'dropdown__active--icon'
                  : 'dropdown__inactive--icon'
              }`
            ]
          }
          width={8}
          fill='color-grey-40'
        />
      </div>
      {isDropdownOpen && !disabled && (
        <div
          className={clsx(unifyStyle['dropdown-list'], dropdownListClassName)}
        >
          {!isEmpty(dropdownData) &&
            dropdownData?.map((item, index) => {
              return (
                <div
                  className={clsx(
                    dropdownCardClassName,
                    unifyStyle['dropdown-card'],
                  )}
                  key={index}
                >
                  <div
                    className={unifyStyle['dropdown-card-list']}
                    onClick={() => {
                      handleSelect(item)
                    }}
                  >
                    <div className={clsx(textDefaultClassName, textClassName)}>
                      {item?.[dataTextKey]}
                      {dataValueKey && ` - ${item?.[dataValueKey]}`}
                      {dataNameKey && ` - ${item?.[dataNameKey]}`}
                    </div>
                  </div>
                  {iconDelete && (
                    <div onClick={() => showAction(item)}>
                      <img
                        src={config.assetsURL + 'icon/trash.svg'}
                        width={14}
                        height={14}
                      />
                    </div>
                  )}
                  {iconCheck &&
                    selectedData?.[dataTextKey] === item?.[dataTextKey] && (
                      <img
                        src={config.assetsURL + 'icon/check.svg'}
                        width={14}
                        height={14}
                      />
                    )}
                </div>
              )
            })}
          {extraButton && (
            <div
              className={clsx(
                'flex items-center padding-m justify-between ',
                unifyStyle['dropdown-card'],
              )}
              onClick={() => {
                setIsDropdownOpen(false)
                handleExtraButton()
              }}
            >
              <div className={textDefaultClassName}>{extraButton}</div>
            </div>
          )}
        </div>
      )}
    </div>
  )
}
