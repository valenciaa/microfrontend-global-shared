import { useEffect, useRef, useState } from 'react'
import Button from '../../layouts/atoms/Button'

/**
 * Component to show context menu to bottom of wrapped menu icon
 * @param {component} children Menu Icon
 * @param {array} menuList Menu list information. Form = [{ value, onPress  }]
 * @param {function} onClickOutsideMenu Function that called after click outsite menu
 * @param {function} onClickMenuIcon Function that called after click menu icon
 * @param {number} menuPopupWidth Menu pop up width
 */
export default function ContextMenu({
  children,
  menuList,
  onClickOutsideMenu = () => {},
  onClickMenuIcon = () => {},
  menuPopupWidth = 224,
}) {
  const menuRef = useRef()
  const [showContextMenu, setShowContextMenu] = useState(false)

  useEffect(() => {
    document.addEventListener('mousedown', handleMouseDown)
    return () => {
      document.removeEventListener('mousedown', handleMouseDown)
    }
  }, [showContextMenu])

  const handleMouseDown = (e) => {
    if (menuRef && !menuRef?.current?.contains(e.target) && showContextMenu) {
      onClickOutsideMenu()
      setShowContextMenu(false) // close dropdown when tap outside the dropdown menu
    }
  }

  const handleOnClickMenuIcon = (e) => {
    e.stopPropagation()
    onClickMenuIcon(showContextMenu)
    setShowContextMenu((prev) => !prev)
  }

  const handleOnClickMenuOption = (e, menuItem) => {
    e.stopPropagation()
    menuItem.onPress(e)
    setShowContextMenu(false)
  }

  return (
    <div ref={menuRef} className='context-menu__wrapper cursor-pointer'>
      <div onClick={handleOnClickMenuIcon}>{children}</div>
      {showContextMenu && (
        <div
          className='context-menu__container'
          style={{ width: menuPopupWidth }}
        >
          {menuList.map((menuItem, index) => (
            <Button
              key={`menu-item-${index}`}
              type='link-primary-transparent'
              size='medium'
              handleOnClick={(e) => handleOnClickMenuOption(e, menuItem)}
              additionalClass='flex padding-top-m'
            >
              <div className='heading-4 color-grey-100'>{menuItem.value}</div>
            </Button>
          ))}
        </div>
      )}
    </div>
  )
}
