import React from 'react'
import config from '../../../../config'
import { NumberWithCommas } from '../../../global/utils'
import GetB2bType from '../../utils/GetB2bType'

export default function CardPrice({ isFlashSaleStart, prices, isFlashSale }) {
  if (prices?.special_price === 0) {
    // inital
    return (
      <div className='row middle-xs price' id='homepagePromoBrandPrice'>
        <div
          className={`${
            !isFlashSaleStart && typeof isFlashSaleStart !== 'undefined'
              ? 'price__initial'
              : 'price__real'
          }`}
          id='homepagePromoBrandPrice'
        >
          Rp{NumberWithCommas(prices?.price)}
        </div>
        {!isFlashSaleStart && typeof isFlashSaleStart !== 'undefined' && (
          <>
            <div className='price__discount'>
              <span>??%</span>
            </div>
            <div className='col-xs-12 price__real'>Rp??</div>
          </>
        )}
      </div>
    )
  } else {
    // for informa b2b item 'flash sale' only show normal price
    if (GetB2bType() === 'informa_b2b' && isFlashSale) {
      return (
        <div
          className={`row middle-xs${
            config.environment === 'desktop' ? ' price' : ''
          }`}
          id='homepagePromoBrandPrice'
        >
          <div className='col-xs-12 price__real'>
            Rp {NumberWithCommas(prices?.price)}
          </div>
        </div>
      )
    } else {
      const discount = Math.floor(
        ((prices?.price - prices?.special_price) / prices?.price) * 100,
      )
      return (
        <div
          className={`row middle-xs${
            config.environment === 'desktop' ? ' price' : ''
          }`}
          id='homepagePromoBrandPrice'
        >
          <div className='price__initial'>
            Rp{NumberWithCommas(prices?.price)}
          </div>
          <div className='price__discount'>
            <span>
              {!isFlashSaleStart && typeof isFlashSaleStart !== 'undefined'
                ? '??'
                : discount}
              %
            </span>
          </div>
          <div className='col-xs-12 price__real'>
            Rp
            {!isFlashSaleStart && typeof isFlashSaleStart !== 'undefined'
              ? '??'
              : NumberWithCommas(prices?.special_price)}
          </div>
        </div>
      )
    }
  }
}
