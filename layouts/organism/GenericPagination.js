import React, { Component } from 'react'
import config from '../../../../config'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'

const createArrayFromRange = (from, to) => {
  let arr = []
  for (let i = from; i <= to; i++) {
    arr.push(i)
  }
  return arr
}

export default class GenericPagination extends Component {
  constructor(props) {
    super(props)
    this.state = {
      displayedPages: [],
    }
  }

  static getDerivedStateFromProps(props) {
    const { pagesCount, currentPage, limit } = props
    const totalPages = Math.round(pagesCount / (limit || 30))
    const lastPage = totalPages

    if (!lastPage) {
      return
    }

    const low = Math.max(1, currentPage - 2)
    const high = Math.min(currentPage + 2, lastPage)

    const displayedPages = createArrayFromRange(low, high)

    return { displayedPages }
  }

  render() {
    const { displayedPages } = this.state
    const { currentPage, setCurrentPage, pagesCount, limit } = this.props
    const totalPages = Math.round(pagesCount / (limit || 30))

    return (
      <ul className='generic-pagination'>
        <li
          className='arrow-li'
          onClick={() =>
            !(currentPage === 1) ? setCurrentPage(currentPage - 1) : null
          }
        >
          <CustomLazyLoadImage
            className={`icon${currentPage === 1 ? '' : '--active'}`}
            src={config.assetsURL + 'icon/arrow-prev.svg'}
          />
        </li>
        {displayedPages[0] > 1 && (
          <>
            <li
              key={1}
              className={`${currentPage === 1 ? 'active' : ''}`}
              onClick={() => setCurrentPage(1)}
            >
              {1}
            </li>
            <li>...</li>
          </>
        )}
        {displayedPages.map((page) => (
          <li
            key={page}
            onClick={() => setCurrentPage(page)}
            className={`${currentPage === page ? 'active' : ''}`}
          >
            {page}
          </li>
        ))}
        {displayedPages[displayedPages.length - 1] < totalPages && (
          <>
            <li>...</li>
            <li
              key={totalPages}
              className={`${currentPage === totalPages ? 'active' : ''}`}
              onClick={() => setCurrentPage(totalPages)}
            >
              {totalPages}
            </li>
          </>
        )}
        <li
          className='arrow-li'
          onClick={() =>
            !(currentPage === totalPages)
              ? setCurrentPage(currentPage + 1)
              : null
          }
        >
          <CustomLazyLoadImage
            className={`icon${currentPage === totalPages ? '' : '--active'}`}
            src={config.assetsURL + 'icon/arrow-next.svg'}
          />
        </li>
      </ul>
    )
  }
}
