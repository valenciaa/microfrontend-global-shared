import React, { useEffect, useRef } from 'react'
import dayjs from 'dayjs'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'

import FloatingBanner from '../moleculs/FloatingBanner'
import { onScrollListener } from '../../utils/ScrollListener'
import { useGetOngoingEvent } from '../../services/api/queries/global'

dayjs.extend(isSameOrAfter)
dayjs.extend(isSameOrBefore)

export default function FloatingButtonPromo() {
  const { data: ongoingEvent } = useGetOngoingEvent({ floating_icon: true })

  const timer = useRef(null)
  useEffect(() => {
    const scrollListener = () => {
      timer.current = onScrollListener(
        timer.current,
        'floating-button',
        '-85px',
        '10px',
      )
    }

    window.addEventListener('scroll', scrollListener)

    return () => {
      window.removeEventListener('scroll', scrollListener)
    }
  }, [])

  if (
    !ongoingEvent ||
    !ongoingEvent.floating_image_url ||
    !ongoingEvent.floating_image_link
  ) {
    return null
  }

  const { floating_image_url: image, floating_image_link: buttonHref } =
    ongoingEvent

  return (
    <FloatingBanner id='floating-button' link={buttonHref} imgUrl={image} />
  )
}
