import React from 'react'
import config from '../../../../../config'
import { pushRouterSearch, searchPageRedirect } from '../../../container/search'
import SearchProductCard from './SearchProductCard'
import { useConstantsContext } from '../../../context/ConstantsContext'
import { useLayoutContext } from '../../../../../src/context/LayoutContext'

export default function ProductRelated({
  keyword,
  onHandleSetType,
  searchData,
  showSomeSearchSections,
  setIsSubmitEvent,
  setKeyword,
  setItmFrom,
  redirectSearchResult,
  isBot,
}) {
  const constantsContext = useConstantsContext()
  const { state: layoutContext } = useLayoutContext()

  if (searchData && searchData.prd) {
    return (
      <div className='col-xs-12 search-result'>
        <div className='section-header'>
          <div className='section-header__title'>
            <div className='heading-3 title margin-right-m'>
              {showSomeSearchSections
                ? 'Produk Pilihan'
                : `Produk Populer di ${layoutContext?.textLabel}`}
            </div>
            {showSomeSearchSections && (
              <div className='view-btn'>
                <a
                  onClick={(e) => {
                    e.preventDefault()
                    onHandleSetType('view-all-product')
                    searchPageRedirect(
                      keyword,
                      'suggestion',
                      setIsSubmitEvent,
                      setKeyword,
                      setItmFrom,
                      // dispatch(
                      //   SearchActions.searchPageRedirectRequest(
                      //     encodeURIComponent(keyword),
                      //   ),
                      // ),
                    )
                    pushRouterSearch(keyword, null, 'view-all-product')
                  }}
                >
                  <p
                    className={`${
                      config.environment === 'mobile'
                        ? 'button-medium-text'
                        : 'heading-4'
                    } __${config.companyNameCSS}`}
                  >
                    Lihat Semua
                  </p>
                </a>
              </div>
            )}
            {constantsContext?.pageFrom === 'tahu' && (
              <div className='view-btn'>
                <a
                  onClick={(e) => {
                    e.preventDefault()
                    onHandleSetType('view-all-product')
                    redirectSearchResult(keyword)
                  }}
                >
                  <p
                    className={`${
                      config.environment === 'mobile'
                        ? 'button-medium-text'
                        : 'heading-4'
                    } __${config.companyNameCSS}`}
                  >
                    Lihat Semua
                  </p>
                </a>
              </div>
            )}
          </div>
          {showSomeSearchSections && (
            <div className='sub-title ui-text-3'>
              <div className='description'>
                <span className='bold heading-4'>
                  {searchData.total} produk
                </span>{' '}
                dalam kategori{' '}
                <span className='bold heading-4'>Semua Kategori</span> dengan
                kata kunci <span className='bold heading-4'>{keyword}</span>
              </div>
            </div>
          )}
        </div>
        <ul>
          {searchData?.prd?.map((product, index) => {
            let label = ''
            if (product?.is_flashsale) {
              label = 'Flash Sale'
            } else if (config.companyCode === 'HCI') {
              label = product.label.HCI || ''
            } else if (config.companyCode === 'AHI') {
              label = product.label.AHI || ''
            } else {
              label = product.label.ODI || ''
            }
            if (index < 5) {
              return (
                <SearchProductCard
                  position={index + 1}
                  keyword={keyword}
                  key={index}
                  label={label}
                  product={product}
                  searchData={searchData}
                  isBot={isBot}
                />
              )
            }
          })}
        </ul>
      </div>
    )
  } else {
    return null
  }
}
