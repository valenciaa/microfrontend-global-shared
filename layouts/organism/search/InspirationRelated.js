import config from '../../../../../config'
import { analyticsTrack } from '../../../container/search'
import ItmCompanyName from '../../../utils/ItmCompanyName'
import CustomLazyLoadImage from '../../atoms/CustomLazyLoadImage'

export default function InspirationRelated({ keyword, searchData }) {
  const companyName = ItmCompanyName()
  if (searchData && searchData?.inspirations) {
    return (
      <div className='col-xs-12 search-result'>
        <div className='section-header'>
          <div className='heading-3 title'>Inspirasi Pilihan</div>
        </div>
        <ul>
          {searchData?.inspirations?.map((inspiration, index) => {
            if (index < 5) {
              return (
                <li
                  className='search-result-list inspiration-search'
                  key={index}
                >
                  <a
                    onClick={() =>
                      analyticsTrack(
                        keyword,
                        'suggestion',
                        'inspiration-search',
                        companyName,
                      )
                    }
                    href={`${config.legacyBaseURL}${
                      inspiration.url || inspiration.url_path
                    }?itm_source=${
                      companyName ? companyName + '-' : ''
                    }search&itm_campaign=inspiration-search&itm_term=${keyword}`}
                  >
                    <div className='search-result-list__name heading-4'>
                      {inspiration.name}
                    </div>
                    <div className='search-result-list__icon'>
                      <CustomLazyLoadImage
                        src={
                          config.assetsURL + 'icon/icon-inspiration-search.svg'
                        }
                        alt='Inspiration Search Icon'
                        width={70}
                        height={16}
                      />
                    </div>
                  </a>
                </li>
              )
            }
          })}
        </ul>
      </div>
    )
  } else {
    return null
  }
}
