import React from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../config'
import ItmCompanyName from '../../../utils/ItmCompanyName'
import { analyticsTrack } from '../../../container/search'
import CustomLazyLoadImage from '../../atoms/CustomLazyLoadImage'

export default function CategoriesRelated({ keyword, searchData }) {
  if (searchData && searchData.facets) {
    return (
      <div className='col-xs-12 search-result'>
        <div className='section-header'>
          <div className='heading-3 title'>Berdasarkan Kategori</div>
        </div>
        <ul>
          {searchData.facets?.map((facet, index) => {
            const newUrl = `${config.legacyBaseURL}c/${
              searchData.url[index]
            }?itm_source=search&itm_campaign=suggestion-category-${facet}&itm_term=${keyword}-&-${facet}&keyword=${encodeURIComponent(
              keyword,
            )}&query=${encodeURIComponent(keyword)}`
            if (facet !== 'Temporary') {
              return (
                <li className='search-result-list category' key={index}>
                  <a
                    onClick={() =>
                      analyticsTrack(
                        keyword,
                        'suggestion',
                        'suggestion-category',
                        ItmCompanyName(),
                      )
                    }
                    href={newUrl}
                  >
                    {config.environment === 'desktop' && (
                      <div className='search-result-list__category-content'>
                        <p className='search-result-list__wording ui-text-3'>
                          <span className='color-orange'>{keyword}</span> di
                          Kategori{' '}
                          <span className='search-result-list__name heading-4'>
                            {facet}
                          </span>
                        </p>
                        <div className='icon'>
                          <CustomLazyLoadImage
                            src={
                              config.assetsURL + 'icon/icon-arrow-up-right.svg'
                            }
                            alt='Search Icon'
                            height={16}
                            width={16}
                            placeholder={<Skeleton />}
                          />
                        </div>
                      </div>
                    )}
                    {config.environment === 'mobile' && (
                      <div className='search-result-list__category-content'>
                        <div className='category-result'>
                          <p className='first-row ui-text-3'>
                            Cari{' '}
                            <span
                              className={`heading-4 search-keyword__${config.companyNameCSS}`}
                            >
                              {keyword}
                            </span>
                          </p>
                          <p className='second-row ui-text-3'>
                            di Kategori{' '}
                            <span className='category heading-4'>{facet}</span>
                          </p>
                        </div>
                        <div className='icon'>
                          <CustomLazyLoadImage
                            src={
                              config.assetsURL + 'icon/icon-arrow-up-right.svg'
                            }
                            alt='Search Icon'
                            height={16}
                            width={16}
                            placeholder={<Skeleton />}
                          />
                        </div>
                      </div>
                    )}
                  </a>
                </li>
              )
            } else {
              return null
            }
          })}
        </ul>
      </div>
    )
  } else {
    return null
  }
}
