import React, { useEffect, useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import { Swiper, SwiperSlide } from 'swiper/react'
import config from '../../../../../config'
import { ConvertImageType } from '../../../utils'
import { getLastSeenProduct } from '../../../utils/GetLastSeenProduct'
import ItmCompanyName from '../../../utils/ItmCompanyName'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import CustomLazyLoadImage from '../../atoms/CustomLazyLoadImage'

export default function LastSeenProductSearch({ selectPages }) {
  const [lastSeenSearch, setLastSeenSearch] = useState([])
  const itmSource = (ItmCompanyName() ? ItmCompanyName() + '-' : '') + 'search'

  useEffect(() => {
    const setup = async () => {
      const lastSeen = await getLastSeenProduct()
      setLastSeenSearch(lastSeen)
    }
    setup()
  }, [])
  const renderLastSearch = () => {
    return (
      <div className='col-xs-12 padding__horizontal--m padding__vertical--s'>
        <div className='last-seen-search'>
          <div className='row'>
            <div
              className={`lastseen-product-search heading-${
                config.environment === 'desktop' ? '3' : '2'
              }`}
            >
              Terakhir Dilihat
            </div>
          </div>

          <Swiper
            className='padding-top-xs last-seen-search__container'
            resistanceRatio={0}
            spaceBetween={5}
            slidesPerColumnFill='row'
            slidesPerView='auto'
          >
            {lastSeenSearch?.slice(0, 12).map((product, index) => {
              if (
                product?.company_code?.[`${config.companyCode}`] ===
                parseInt(10)
              ) {
                let imageUrl = ConvertImageType(
                  config.imageURL +
                    'w_73,h_73' +
                    product?.variants?.[0]?.images?.[0]?.image_url,
                )
                if (product?.variants?.[0]?.images?.[0]?.full_image_url) {
                  imageUrl = product?.variants?.[0]?.images?.[0]?.full_image_url
                }
                return (
                  <SwiperSlide key={index}>
                    <a
                      onClick={() =>
                        mixpanelTrack(
                          'Click Riwayat Product Terakhir Dilihat',
                          {
                            'Location': selectPages, // eslint-disable-line
                            'Product Name': product?.name,
                            'Product SKU': product?.variants?.[0]?.sku,
                            'ITM Source': itmSource,
                            'ITM Campaign': 'Product Last Seen',
                            'ITM Term': product?.variants?.[0]?.sku,
                            'ITM Device': config.environment,
                          },
                        )
                      }
                      href={
                        config.legacyBaseURL +
                        product?.url_key +
                        '?itm_source=' +
                        itmSource +
                        '&itm_campaign=product-last-seen' +
                        '&itm_term=' +
                        product?.variants?.[0]?.sku +
                        '&itm_device=' +
                        config.environment
                      }
                      key={index}
                      className='last-seen-search__item'
                    >
                      <CustomLazyLoadImage
                        src={imageUrl}
                        alt={product?.name?.replace(/\s+/g, '-').toLowerCase()}
                        id='LastSeenProduct'
                        height='73'
                        width='73'
                        placeholderAspectRatio={1}
                        placeholder={<Skeleton width={73} height={73} />}
                      />
                    </a>
                  </SwiperSlide>
                )
              }

              return null
            })}
          </Swiper>
        </div>
      </div>
    )
  }

  if (Array.isArray(lastSeenSearch) && lastSeenSearch?.length >= 4) {
    return renderLastSearch()
  } else {
    return null
  }
}
