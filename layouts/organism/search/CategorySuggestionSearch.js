import isEmpty from 'lodash/isEmpty'
import shuffle from 'lodash/shuffle'
import React, { useEffect, useMemo, useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../config'
import { ConvertImageType, ItmConverter } from '../../../utils'
import { getLastSeenProduct } from '../../../utils/GetLastSeenProduct'
import useAuthenticationData from '../../../utils/hooks/useAuthenticationData'
import CustomLazyLoadImage from '../../atoms/CustomLazyLoadImage'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import ItmCompanyName from '../../../utils/ItmCompanyName'

export default function CategorySuggestionSearch({ selectPages }) {
  const { isLogin, user } = useAuthenticationData()
  const [lastSeenSearch, setLastSeenSearch] = useState([])
  const [categorySearch, setCategorySearch] = useState([])
  useEffect(() => {
    const setup = async () => {
      const lastSeen = await getLastSeenProduct()
      setLastSeenSearch(lastSeen)
    }
    setup()
  }, [])
  const categoryLevelThreeIndex = useMemo(() => {
    if (
      !isEmpty(lastSeenSearch) &&
      lastSeenSearch.length > 0 &&
      lastSeenSearch[0] &&
      lastSeenSearch?.[0]?.[`breadcrumb_last_seen_${config.companyCode}`]
    ) {
      return lastSeenSearch?.[0]?.[
        `breadcrumb_last_seen_${config.companyCode}`
      ]?.findIndex((element) => element?.breadcrumb_position === 3)
    }
  }, [lastSeenSearch])
  useEffect(() => {
    if (!isEmpty(lastSeenSearch) && categoryLevelThreeIndex >= 0) {
      let categoryLastSeenContainer = lastSeenSearch
        ?.map((el) => {
          return {
            category: el?.[`breadcrumb_last_seen_${config.companyCode}`],
            image: el?.variants?.[0]?.images?.[0],
          }
        })
        ?.filter((item) => item?.category?.length > 2)
      categoryLastSeenContainer = [
        ...new Map(
          categoryLastSeenContainer?.map((item) => [
            item?.category?.[categoryLevelThreeIndex]?.name,
            item,
          ]),
        ).values(),
      ] // distinct list of Last Seen Product based on Category lv. 3
      setCategorySearch(categoryLastSeenContainer?.slice(0, 15)) // max 15 category items
    }
  }, [categoryLevelThreeIndex, lastSeenSearch])
  const randomCategoryList = () => {
    if (!isEmpty(categorySearch)) {
      const shuffleListCategory = shuffle(categorySearch)
      setCategorySearch(shuffleListCategory)
      mixpanelTrack('Cari Lagi Kategori Pilihan', {
        Location: selectPages, // eslint-disable-line
      })
    }
  }

  const renderCategoryItems = (item, idx, itmSource) => {
    if (!isEmpty(item)) {
      const itmTerm = ItmConverter(
        item?.category?.[categoryLevelThreeIndex]?.name,
      ).replace('&', 'dan')
      let imageUrl = ConvertImageType(
        config?.imageURL +
          `${config.environment === 'desktop' ? 'w_35,h_35' : 'w_40,h_40'}` +
          item?.image?.image_url,
      )
      if (item?.image?.full_image_url) {
        imageUrl = item?.image?.full_image_url
      }
      return (
        <div
          key={idx}
          className={`category-suggestion-search${
            config.environment === 'desktop'
              ? '__desktop col-xs-4'
              : '__mobile col-xs-6'
          } cursor-pointer`}
        >
          <a
            href={
              config.legacyBaseURL +
              'c/' +
              item?.category?.[categoryLevelThreeIndex]?.url_key +
              '?itm_source=' +
              itmSource +
              '&itm_campaign=' +
              'recommendation-category' +
              '&itm_term=' +
              itmTerm +
              '&itm_device=' +
              config.environment
            }
            className='category-list flex text__grey100'
            onClick={() =>
              mixpanelTrack('Click Kategori Pilihan', {
                'Location': selectPages, // eslint-disable-line
                'Category Name': item?.category?.[categoryLevelThreeIndex].name,
                'ITM Source': itmSource,
                'ITM Campaign': 'Recommendation Category',
                'ITM Term': itmTerm,
                'ITM Device': config.environment,
              })
            }
          >
            {item.image ? (
              <CustomLazyLoadImage
                src={imageUrl}
                alt={item?.category?.name
                  ?.replace(/ |#|\+/g, '-')
                  ?.toLowerCase()}
                id='homepagePromoBrandProductImage'
                height={config.environment === 'desktop' ? 35 : 40}
                width={config.environment === 'desktop' ? 35 : 40}
                placeholderAspectRatio={1}
                placeholder={<Skeleton />}
              />
            ) : null}
            <span className='margin-left-xs align-center category-list__text'>
              {item?.category?.[categoryLevelThreeIndex]?.name}
            </span>
          </a>
        </div>
      )
    }
  }
  const renderCategorySuggestion = () => {
    if (categorySearch?.length > 2) {
      // minimal 2 category items
      const itmSource =
        (ItmCompanyName() ? ItmCompanyName() + '-' : '') + 'search'
      return (
        <div className='col-xs-12 padding__horizontal--m padding-top-xs padding-bottom-s'>
          <div className='row padding-bottom-xs'>
            <div
              className={`col-xs category-search heading-${
                config.environment === 'desktop' ? '3' : '2'
              }`}
            >
              Kategori Pilihan untuk{' '}
              {isLogin ? user?.name || user?.first_name : 'Kamu'}
            </div>
            {categorySearch?.length > 5 ? (
              <div
                className={`col-xs-${
                  config.environment === 'desktop' ? '2' : '3'
                } button-medium-text cursor-pointer align-center text-right text__${
                  config.companyNameCSS
                }`}
              >
                <p
                  onClick={() => {
                    randomCategoryList()
                  }}
                >
                  {' '}
                  Cari Lagi
                </p>
              </div>
            ) : null}
          </div>
          <div className='row category-suggestion-search'>
            {categorySearch
              ?.slice(0, 6)
              ?.map((item, idx) => renderCategoryItems(item, idx, itmSource))}
          </div>
        </div>
      )
    } else {
      return null
    }
  }

  return renderCategorySuggestion()
}
