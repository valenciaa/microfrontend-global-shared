import React from 'react'
import config from '../../../../../config'
import {
  pushRouterSearch,
  searchPageRedirect,
} from '../../../../global/container/search'
import { GetParam } from '../../../utils'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'

export default function AutoSuggestion({
  searchData,
  setIsSubmitEvent,
  setKeyword,
  setItmFrom,
  keyword,
  onHandleSetType,
  handleChangeLayoutContext,
}) {
  if (searchData && searchData.sug) {
    return (
      <div className='col-xs-12 search-result'>
        <div className='section-header'>
          <div className='heading-3 title'>Pencarian Rekomendasi</div>
        </div>
        <ul>
          {searchData.sug?.map((product, index) => {
            if (index < 5) {
              return (
                <li
                  className='search-result-list icon-left'
                  id='recommendationSearch'
                  key={index}
                  onClick={(e) => {
                    e.preventDefault()
                    onHandleSetType('auto-complete')
                    searchPageRedirect(
                      product.query,
                      'suggestion',
                      setIsSubmitEvent,
                      setKeyword,
                      setItmFrom,
                      // dispatch(
                      //   SearchActions.searchPageRedirectRequest(
                      //     encodeURIComponent(product.query),
                      //   ),
                      // ),
                    )
                    pushRouterSearch(product.query, null, 'auto-complete')
                    // searchProductByKeywordAlgolia(
                    //   encodeURIComponent(product.query),
                    //   dispatch,
                    // )
                    window.scrollTo(0, 0)
                    handleChangeLayoutContext('isOverlayOn', false)
                    mixpanelTrack('Search', {
                      'Search Keyword': keyword,
                      'Search Suggestions': product.query,
                      'Location': 'Search Bar',
                      'ITM Campaign':
                        GetParam('itm_campaign') || 'suggestion-keyword',
                      'ITM Source':
                        GetParam('itm_source') ||
                        (config.companyName ? config.companyName + '-' : '') +
                          'search',
                      'ITM Term': GetParam('itm_term') || keyword,
                    })
                  }}
                >
                  {config.environment === 'mobile' && (
                    <div className='search-result-list__icon'>
                      <img
                        src={`${config.assetsURL}icon/icon-search-grey.svg`}
                        alt='Search Icon'
                        height={16}
                        width={16}
                      />
                    </div>
                  )}
                  {config.environment === 'desktop' && (
                    <div className='search-result-list__icon'>
                      <img
                        src={`${config.assetsURL}icon/search-grey.svg`}
                        alt='Redirect Icon'
                        height={16}
                        width={16}
                      />
                    </div>
                  )}
                  <div
                    className='search-result-list__name search-result-list__name-suggestion heading-4'
                    dangerouslySetInnerHTML={{
                      __html: product._highlightResult.query.value,
                    }}
                  />
                </li>
              )
            } else {
              return null
            }
          })}
        </ul>
      </div>
    )
  } else {
    return null
  }
}
