import isEmpty from 'lodash/isEmpty'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../config'
import { useGetPopularBrandSearch } from '../../../services/api/queries/global'
import ItmCompanyName from '../../../utils/ItmCompanyName'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import CustomLazyLoadImage from '../../atoms/CustomLazyLoadImage'

export default function PopularBrand({ selectPages }) {
  const { data: listBrand } = useGetPopularBrandSearch({
    companyCode: config.companyCode,
    is_b2b: config.isB2b || false,
  })
  const itmSource = (ItmCompanyName() ? ItmCompanyName() + '-' : '') + 'search'
  const listItem = () => {
    return (
      <div
        className={`row d-flex justify-content-center ${
          config.environment === 'mobile' ? 'popular-brand__scroller' : ''
        }`}
      >
        {listBrand?.map((el, idx) => {
          return (
            <div
              className={`padding-bottom-xs ${
                config.environment === 'mobile'
                  ? 'col-xs-3 popular-brand__item'
                  : 'col-xs-2'
              }`}
              key={idx}
            >
              <div className='popular-brand-list'>
                <a
                  onClick={() =>
                    mixpanelTrack(
                      'Click Popular Brand',
                      {
                        'Location': selectPages, // eslint-disable-line
                        'Brand Name': el?.detail?.brandName,
                        'ITM Source': itmSource,
                        'ITM Campaign': 'popular-brand-search',
                        'ITM Term': el?.detail?.brandName,
                        'ITM Device': config.environment,
                      },
                      'PBrand',
                    )
                  }
                  href={
                    (el?.detail?.urlLink.slice(-1) === '/'
                      ? el.detail.urlLink.replace(/.$/, '').toLowerCase()
                      : el.detail.urlLink
                    ).toLowerCase() +
                    '?itm_source=' +
                    itmSource +
                    '&itm_campaign=popular-brand-search' +
                    '&itm_term=' +
                    el?.detail?.brandName +
                    '&itm_device=' +
                    config.environment
                  }
                >
                  <CustomLazyLoadImage
                    src={el?.detail?.imageUrl}
                    alt={'popular-brand-' + (idx + 1)}
                    id={'popular-brand_' + (idx + 1)}
                    width={config.environment === 'mobile' ? 77 : 70}
                    height={config.environment === 'mobile' ? 52 : 50}
                    placeholder={<Skeleton />}
                  />
                </a>
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  if (!isEmpty(listBrand)) {
    return (
      <div className='col-xs-12 padding__horizontal--m popular-brand padding-bottom-xs'>
        <div
          className={`row ${
            config.environment === 'desktop' ? 'padding-bottom-xs' : ''
          }`}
        >
          <div
            className={`popular-brand-search heading-${
              config.environment === 'desktop' ? '3' : '2'
            }`}
          >
            Koleksi Brand Terpopuler
          </div>
        </div>
        {config.environment === 'mobile' ? (
          <div className='popular-brand__container'>{listItem()}</div>
        ) : (
          listItem()
        )}
      </div>
    )
  } else {
    return null
  }
}
