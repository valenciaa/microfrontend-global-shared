import Skeleton from 'react-loading-skeleton'
import config from '../../../../../config'
import { analyticsTrack } from '../../../container/search'
import { ItmGenerator } from '../../../utils'
import ItmCompanyName from '../../../utils/ItmCompanyName'
import CustomLazyLoadImage from '../../atoms/CustomLazyLoadImage'

export default function BrandsAndOfficialStore({
  keyword,
  searchData,
  searchTitleSection,
  type,
}) {
  let typeSuggestion = ''
  if (type === 'brands') {
    typeSuggestion = 'brand-'
  } else if (type === 'stores') {
    typeSuggestion = 'official-store-'
  }
  if (searchData && searchData[type]) {
    return (
      <div className='col-xs-12 search-result'>
        <div className='section-header'>
          <div className='heading-3 title'>{searchTitleSection}</div>
        </div>
        <ul>
          {searchData?.[type]?.map((data, index) => {
            const newUrl = ItmGenerator(
              'search',
              config.legacyBaseURL + data.url,
              'suggestion-' + typeSuggestion + data.name,
              '',
              data.name,
            )
            if (index < 3) {
              return (
                <li className='search-result-list category' key={index}>
                  <a
                    onClick={() =>
                      analyticsTrack(
                        keyword,
                        'suggestion',
                        'suggestion-' + type,
                        ItmCompanyName(),
                      )
                    }
                    href={newUrl}
                  >
                    {config.environment === 'desktop' && (
                      <div className='search-result-list__category-content'>
                        <p className='search-result-list__wording ui-text-3'>
                          {data.name}
                        </p>
                        <div className='icon'>
                          <CustomLazyLoadImage
                            src={
                              config.assetsURL + 'icon/icon-arrow-up-right.svg'
                            }
                            alt='Search Icon'
                            height={16}
                            width={16}
                            placeholder={<Skeleton />}
                          />
                        </div>
                      </div>
                    )}
                    {config.environment === 'mobile' && (
                      <div className='search-result-list__category-content'>
                        <div className='category-result'>
                          <p className='official heading-4'>{data.name}</p>
                        </div>
                        <div className='icon'>
                          <CustomLazyLoadImage
                            src={
                              config.assetsURL + 'icon/icon-arrow-up-right.svg'
                            }
                            alt='Search Icon'
                            height={16}
                            width={16}
                            placeholder={<Skeleton />}
                          />
                        </div>
                      </div>
                    )}
                  </a>
                </li>
              )
            }
          })}
        </ul>
      </div>
    )
  } else {
    return null
  }
}
