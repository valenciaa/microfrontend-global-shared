import startCase from 'lodash/startCase'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../config'
import { analyticsTrack } from '../../../container/search'
import { useCreateLog } from '../../../services/api/mutations/global'
import { ConvertImageType, NumberWithCommas } from '../../../utils'
import ItmCompanyName from '../../../utils/ItmCompanyName'
import CustomLazyLoadImage from '../../atoms/CustomLazyLoadImage'
import { submitClickProductSearchBarAAEvent } from '../../../utils/AlgoliaAnalytics'
import GetB2bType from '../../../utils/GetB2bType'

export default function SearchProductCard({
  label,
  keyword,
  product,
  searchData,
  position,
  isBot,
}) {
  const companyName = ItmCompanyName()
  const { mutate: createLog } = useCreateLog()
  const onProductClick = () => {
    analyticsTrack(
      startCase(product?.name?.toLowerCase()),
      'suggestion',
      'suggestion+product',
      companyName,
    )
    if (!isBot) {
      submitClickProductSearchBarAAEvent(
        searchData?.indexName,
        product,
        searchData?.queryId,
        position,
      )
    }
    createLog({
      searchTerm: keyword,
      totalResult: searchData?.total,
      name: product?.name,
      sku: product?.sku,
      imageUrl:
        product?.images?.length > 0 ? product?.images?.[0]?.image_url : '',
      urlKey: product?.url_key,
    })
  }
  let imageUrl = ConvertImageType(
    config.imageURL +
      (config.environment === 'mobile' ? 'w_94,h_94' : 'w_70,h_70') +
      product?.images?.[0]?.image_url,
  )

  if (product?.images?.[0]?.full_image_url) {
    imageUrl = product?.images?.[0]?.full_image_url
  }

  const isUseNormalPrice =
    product?.events?.length > 0 && GetB2bType() === 'informa_b2b'

  return (
    <li className='search-result__product-list'>
      <a
        onClick={onProductClick}
        href={`${config.legacyBaseURL}p/${product.url_key}?itm_source=${
          companyName ? companyName + '-' : ''
        }search&itm_campaign=suggestion-product&itm_term=${keyword}-&-${
          product.sku
        }${
          searchData?.queryId !== '' ? '&queryId=' + searchData?.queryId : ''
        }`}
      >
        <div className='row search-product-card'>
          <div className='col-xs-2 product-image'>
            <CustomLazyLoadImage
              src={imageUrl}
              alt='Product Image'
              height={config.environment === 'desktop' ? 64 : 68}
              width={config.environment === 'desktop' ? 64 : 68}
            />
          </div>
          <div className='col-xs-10 product-detail'>
            <div
              className={`product-detail__name ui-text-${
                config.environment === 'desktop' ? '4' : '2'
              }`}
            >
              {product?.name}
            </div>
            <div className='product-detail__price row middle-xs'>
              {product?.selling_price === product?.default_price ? (
                <div className='heading-3 price__real'>
                  Rp{NumberWithCommas(product?.default_price)}
                </div>
              ) : (
                <>
                  {config.environment === 'desktop' ? (
                    <>
                      <div className='price__real'>
                        Rp
                        {NumberWithCommas(
                          isUseNormalPrice
                            ? product?.default_price
                            : product?.selling_price,
                        )}
                      </div>
                      {!isUseNormalPrice && (
                        <>
                          <div className='price__initial ui-text-4'>
                            Rp{NumberWithCommas(product?.default_price)}
                          </div>
                          <div className='price__discount'>
                            <p className='ui-text-4'>
                              {Math.floor(product?.discount)}%
                            </p>
                          </div>
                        </>
                      )}
                    </>
                  ) : (
                    <>
                      <div className='price__initial ui-text-4'>
                        Rp{NumberWithCommas(product?.default_price)}
                      </div>
                      <div className='price__discount'>
                        <p className='ui-text-4'>
                          {Math.floor(product?.discount)}%
                        </p>
                      </div>
                      <div className='col-xs-12 heading-3 price__real-with-discount'>
                        Rp{NumberWithCommas(product?.selling_price)}
                      </div>
                    </>
                  )}
                </>
              )}
            </div>
            {label !== '' && (
              <div className='product-detail__label'>
                <div className='row'>
                  <div className='icon'>
                    <CustomLazyLoadImage
                      src={config.assetsURL + 'icon/info-red.svg'}
                      alt='Label Icon'
                      height={16}
                      width={16}
                      placeholderAspectRatio={16 / 16}
                      placeholder={<Skeleton />}
                    />
                  </div>
                  <div className='product-label ui-text-3'>{label}</div>
                </div>
              </div>
            )}
          </div>
        </div>
      </a>
    </li>
  )
}
