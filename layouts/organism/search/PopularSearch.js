import React from 'react'
import config from '../../../../../config'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import {
  searchPageRedirect,
  pushRouterSearch,
} from '../../../../global/container/search'
import { capitalize } from '../../../utils/String'

export default function PopularSearch({
  popularSearch,
  setIsSubmitEvent,
  setKeyword,
  setItmFrom,
  onHandleSetType,
  getPopularList,
}) {
  const getRedirectLink = (item) => {
    if (!['algolia', 'mixpanel'].includes(item.type)) {
      window.location.href =
        item.url_link +
        '?' +
        `itm_source=search&itm_campaign=suggestion-popular-search&itm_term=${item.url_key}&itm_device=${config.environment}`
    } else {
      searchPageRedirect(
        item.keyword,
        'suggestion-popular-search',
        setIsSubmitEvent,
        setKeyword,
        setItmFrom,
        // dispatch(
        //   SearchActions.searchPageRedirectRequest(
        //     encodeURIComponent(item.keyword),
        //   ),
        // ),
      )
      pushRouterSearch(item.keyword, null, 'suggestion-popular-search')
    }
    // searchProductByKeywordAlgolia(
    //   encodeURIComponent(item.keyword),
    //   dispatch(
    //     SearchActions.searchPageRedirectRequest(
    //       encodeURIComponent(item.keyword),
    //     ),
    //   ),
    // )
  }

  function popularList() {
    if (popularSearch) {
      return popularSearch?.map((item, index) => {
        return (
          <div
            className={`popular-keyword${
              config.environment === 'desktop'
                ? '__desktop col-xs-4'
                : '__mobile col-xs-6'
            } cursor-pointer`}
            key={index}
          >
            <a
              className={`flex popular-list text__grey100`}
              onClick={() => {
                onHandleSetType('popular')
                mixpanelTrack('Click Pencarian Populer', {
                  'ITM Campaign': 'suggestion-popular-search',
                  'ITM Source': 'search',
                  'Keyword': item?.keyword || 'None',
                })
                getRedirectLink(item)
              }}
            >
              {item.img_url && (
                <img
                  src={item.img_url}
                  height={config.environment === 'desktop' ? 35 : 40}
                  width={config.environment === 'desktop' ? 35 : 40}
                  className='margin-right-xs'
                />
              )}
              <div className='align-center margin-left-xs popular-list__text'>
                {capitalize(item.keyword)}
              </div>
            </a>
          </div>
        )
      })
    } else {
      return null
    }
  }

  return (
    <div className='col-xs-12 padding__horizontal--m padding__vertical--s'>
      <div className='row padding-bottom-xs'>
        <div className='col-xs-6'>
          <p
            className={
              'popular-search ' +
              (config.environment === 'desktop' ? 'heading-3' : 'heading-2')
            }
          >
            Pencarian Populer
          </p>
        </div>

        {popularSearch.length === 6 && (
          <div className='col text-right heading-3 flex justify-end items-center'>
            <p
              className={
                'button-medium-text cursor-pointer text__' +
                config.companyNameCSS
              }
              onClick={() => {
                mixpanelTrack('Cari Lagi Pencarian Populer')
                getPopularList()
              }}
            >
              Cari Lagi
            </p>
          </div>
        )}
      </div>
      <div className='row popular-keyword'>{popularList()}</div>
    </div>
  )
}
