import { Configure, useInstantSearch } from 'react-instantsearch'
import { GetParam } from '../../../utils'
import { useEffect } from 'react'
import {
  mixpanelPCPTrack,
  sendFilterPcpAlgoliaMixpanel,
} from '../../../utils/MixpanelWrapper'
import { usePCPContext } from '../../../../../src/pcp/context/PCPContext'
import config from '../../../../../config'

const SearchState = ({
  userToken,
  reRankingApplyFilter,
  analyticsTags,
  filters,
  clickAnalytics,
  hitsPerPage,
  unmatchedKeyword,
  setUnmatchedKeyword,
  keywordProps,
  geoloc,
  geolocationSource,
  setModifiedPersonalizedFilter,
  indexName,
  maxValuesPerFacet,
  selectedStore = '',
}) => {
  const { state: pcpContext } = usePCPContext() || {}
  const { results: searchResults, status, uiState } = useInstantSearch()

  const setModifiedFilter = () => {
    let modifiedPersonalizedFilter = {}
    if (pcpContext?.personalizedQuickFilter) {
      for (const [key, value] of Object.entries(
        pcpContext?.personalizedQuickFilter,
      )) {
        if (
          key === 'brand' &&
          searchResults?.disjunctiveFacets?.find(
            (data) => data.name === 'brand.name',
          )?.data?.[`${value?.brand_name}`]
        ) {
          modifiedPersonalizedFilter[`${key}`] = value
        } else if (
          key === 'promo' &&
          searchResults?.disjunctiveFacets?.find(
            (data) => data.name === `label.${config.companyCode}`,
          )?.data?.[`${value?.promo_name}`]
        ) {
          modifiedPersonalizedFilter[`${key}`] = value
        } else if (
          key === 'kategori' &&
          searchResults?.disjunctiveFacets?.find(
            (data) =>
              data.name ===
              `bread_crumb3_${config.companyCode.toLowerCase()}.name`,
          )?.data?.[`${value?.category_name}`]
        ) {
          modifiedPersonalizedFilter[`${key}`] = value
        } else if (
          key === 'warna' &&
          searchResults?.disjunctiveFacets?.find(
            (data) => data.name === 'colour',
          )?.data?.[`${value?.hex_code + '|' + value?.color_name}`]
        ) {
          modifiedPersonalizedFilter[`${key}`] = value
        } else if (
          key === 'lokasi_pengiriman' &&
          searchResults?.disjunctiveFacets?.find(
            (data) => data.name === 'city_name',
          )?.data?.[`${value?.name}`]
        ) {
          modifiedPersonalizedFilter[`${key}`] = value
        } else if (
          key === 'official_store_partner' &&
          searchResults?.disjunctiveFacets?.find(
            (data) => data.name === 'store_name',
          )?.data?.[`${value?.url_store}`]
        ) {
          modifiedPersonalizedFilter[`${key}`] = value
        }
      }
      setModifiedPersonalizedFilter(modifiedPersonalizedFilter)
    }
  }

  useEffect(() => {
    if (status !== 'loading' && geolocationSource !== '') {
      const pageName = searchResults.query || 'None'
      const pageNumber = searchResults.page + 1
      let mixPanelParam = {
        'gideon_name': pageName === 'None' ? 'Store' : 'Search',
        'Keyword': pageName,
        'Page Number': pageNumber,
        'Redirect Result?': GetParam('redirect'),
        'Search Results?': searchResults?.nbHits,
        'Unmatched_Keyword': searchResults.queryAfterRemoval?.includes('<em>')
          ? searchResults.queryAfterRemoval
              .split(' ')?.[1]
              ?.replace('<em>', '')
              .replace('</em>', '')
          : 'None',
        'Geo Location Source': geolocationSource,
      }

      mixpanelPCPTrack('View PCP', pageName, mixPanelParam)
    }
    if (config.environment === 'mobile') {
      setModifiedFilter()
    }
  }, [
    searchResults?.nbHits,
    searchResults?.page,
    searchResults?.query,
    geolocationSource,
  ])

  useEffect(
    () => {
      if (status === 'loading') {
        if (uiState?.[`${indexName}`].refinementList) {
          sendFilterPcpAlgoliaMixpanel(
            uiState?.[`${indexName}`].refinementList,
            selectedStore,
          )
        }
        if (uiState?.[`${indexName}`].range) {
          sendFilterPcpAlgoliaMixpanel(
            uiState?.[`${indexName}`].range,
            selectedStore,
          )
        }
      } else if (status !== 'loading' && selectedStore !== '') {
        sendFilterPcpAlgoliaMixpanel(null, selectedStore)
      }
    },
    [
      uiState?.[`${indexName}`]?.range,
      uiState?.[`${indexName}`]?.refinementList,
      uiState?.[`${indexName}`]?.query,
    ],
    selectedStore,
  )

  let temp = analyticsTags
  const keyword = keywordProps || searchResults?.query || ''
  if (searchResults && searchResults.queryAfterRemoval?.includes('<em>')) {
    const arrayString = searchResults.queryAfterRemoval.split(' ')
    if (arrayString[0] && unmatchedKeyword !== arrayString[0]) {
      setUnmatchedKeyword(arrayString[0])
    }

    if (!temp.includes('unmatched-keyword')) {
      temp.push('unmatched-keyword')
    }
  } else {
    const index = temp.indexOf('unmatched-keyword')
    setUnmatchedKeyword('')
    if (index > -1) {
      temp.splice(index, 1)
    }
  }

  let consecutiveNumber = 0

  for (let i = 0; i < keyword?.length; i++) {
    if (keyword[i] >= '0' && keyword[i] <= '9') {
      consecutiveNumber++
    } else {
      consecutiveNumber = 0
    }
  }
  let configureParams = {
    reRankingApplyFilter,
    analyticsTags: temp,
    filters,
    clickAnalytics,
    maxValuesPerFacet,
    hitsPerPage,
  }

  if (consecutiveNumber >= 5) {
    configureParams = {
      ...configureParams,
      analyticsTags : ['sku'],
      clickAnalytics: false,
    }
  }
  if (
    userToken !==
      'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855' &&
    userToken !== ''
  ) {
    configureParams = {
      ...configureParams,
      userToken,
    }
  }

  if (geoloc && geoloc !== '') {
    configureParams = {
      ...configureParams,
      aroundLatLng: geoloc,
      aroundPrecision: 100000,
      aroundRadius: 'all',
    }
  }

  if (keyword?.split(' ')?.length === 1) {
    configureParams = {
      ...configureParams,
      queryType: 'prefixNone',
    }
  }

  return <Configure {...configureParams} />
}
export default SearchState
