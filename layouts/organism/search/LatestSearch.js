import React from 'react'
import Cookies from 'js-cookie'
import config from '../../../../../config'
import {
  pushRouterSearch,
  searchPageRedirect,
} from '../../../../global/container/search'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
// handleDeleteLatest,
export default function LatestSearch({
  latestSearch,
  setLatestSearch,
  setIsSubmitEvent,
  setKeyword,
  setItmFrom,
  onHandleSetType,
}) {
  const handleDeleteLatestSearch = () => {
    const cookiesName = `search-history${
      config.companyNameCSS !== 'ruparupa' ? '-' + config.companyNameCSS : ''
    }`
    Cookies.remove(cookiesName)
    setLatestSearch([])
  }
  return (
    <div className='col-xs-12 latest-search'>
      <div className='section-header'>
        <div
          className={
            'margin-right-m title ' +
            (config.environment === 'desktop' ? 'heading-3' : 'heading-2')
          }
        >
          Pencarian Terakhir
        </div>
        <div
          className={
            'button-medium-text delete-btn text__' + config.companyNameCSS
          }
          onClick={() => handleDeleteLatestSearch()}
        >
          Hapus Semua
        </div>
      </div>
      <ul>
        {latestSearch?.length > 0 &&
          latestSearch?.map((keyword, index) => {
            return (
              <li
                className='delete-latest'
                key={index}
                onClick={() => {
                  onHandleSetType('latest')
                  mixpanelTrack('Click Pencarian Terakhir', {
                    'ITM Campaign': 'suggestion-last-search',
                    'ITM Source': 'search',
                    'Keyword': keyword || 'None',
                  }),
                    searchPageRedirect(
                      keyword,
                      'suggestion-last-search',
                      setIsSubmitEvent,
                      setKeyword,
                      setItmFrom,
                      // dispatch(
                      //   SearchActions.searchPageRedirectRequest(
                      //     encodeURIComponent(keyword),
                      //   ),
                      // ),
                    )
                  pushRouterSearch(keyword, null, 'suggestion-last-search')
                }}
              >
                <div>{keyword}</div>
                <div className='delete-latest__icon'>
                  {/* onClick={() => { handleDeleteLatest(keyword, latestSearch, setLatestSearch) }} */}
                  <img
                    src={`${config.assetsURL}icon/icon-arrow-up-right.svg`}
                    alt='Redirect Icon'
                    height={16}
                    width={16}
                  />
                </div>
              </li>
            )
          })}
      </ul>
    </div>
  )
}
