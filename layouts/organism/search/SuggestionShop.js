import React from 'react'
import config from '../../../../../config'

export default function SuggestionShop({ searchData, keyword }) {
  if (searchData && searchData.sis && config.companyCode === 'ODI') {
    return (
      <div className='col-xs-12 search-result'>
        <div className='section-header'>
          <div className='heading-3 title'>Shop in Shop</div>
        </div>
        <ul>
          {searchData.sis?.map((item, index) => {
            if (index < 3) {
              return (
                <div className='row shop' key={index}>
                  <div className={`shopinshop-space__${config.environment}`}>
                    <img height={40} width={40} src={item.icon} />
                  </div>
                  <div>
                    <a
                      className={`shopinshop-space__${config.environment}`}
                      style={{ color: '#313339' }}
                      href={
                        item.url +
                        '?itm_source=search&itm_campaign=suggestion-shop-in-shop&itm_term=' +
                        keyword +
                        '&query=' +
                        keyword
                      }
                    >
                      {item.name}
                    </a>
                  </div>
                </div>
              )
            } else {
              return null
            }
          })}
        </ul>
      </div>
    )
  } else {
    return null
  }
}
