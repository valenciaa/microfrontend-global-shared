import React from 'react'
import config from '../../../../config'
import { NumberWithCommas } from '../../utils'
export default function CardPriceVoucher({
  isFlashSaleStart,
  prices,
  tabStats,
  tahuStyles,
}) {
  if (prices.special_price === 0) {
    // inital
    return (
      <div className='row middle-xs price' id='homepagePromoBrandPrice'>
        <div
          className={`${
            !isFlashSaleStart && typeof isFlashSaleStart !== 'undefined'
              ? 'price__initial'
              : tahuStyles.price__real
          }`}
          id='homepagePromoBrandPrice'
        >
          Rp{NumberWithCommas(prices.price)}
        </div>
        {!isFlashSaleStart && typeof isFlashSaleStart !== 'undefined' && (
          <>
            <div className='price__discount'>
              <span>??%</span>
            </div>
            <div className='col-xs-12 price__real'>Rp??</div>
          </>
        )}
      </div>
    )
  } else {
    let discount = Math.floor(
      ((prices?.price - prices?.special_price) / prices?.price) * 100,
    )
    if (tabStats === 0) {
      discount = '??'
    }
    return (
      <div
        className={`row middle-xs${
          config.environment === 'desktop' ? ' price' : ''
        }`}
        id='homepagePromoBrandPrice'
      >
        <div className={`col-xs-12 price__real ${tahuStyles?.price__real}`}>
          Rp
          {!isFlashSaleStart && typeof isFlashSaleStart !== 'undefined'
            ? '??'
            : NumberWithCommas(prices.special_price)}
        </div>
        <div className={`price__initial ${tahuStyles?.price__initial}`}>
          Rp{NumberWithCommas(prices.price)}
        </div>
        <div className={`price__discount ${tahuStyles?.price__discount}`}>
          <span>
            {!isFlashSaleStart && typeof isFlashSaleStart !== 'undefined'
              ? '??'
              : discount}
            %
          </span>
        </div>
        <span className={tahuStyles.price__text}>setelah voucher</span>
      </div>
    )
  }
}
