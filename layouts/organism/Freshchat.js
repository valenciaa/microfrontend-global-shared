/* eslint-disable no-unused-expressions */

import React, { useEffect, useRef, useState } from 'react'
import config from '../../../../config'
import isEmpty from 'lodash/isEmpty'
import {
  useGetFreshchatRestoreId,
  usePostFreshchatRestoreId,
} from '../../services/api/mutations/global'
import { useCartAuthContext } from '../../context/CartAuthContext'
import { useConstantsContext } from '../../context/ConstantsContext'
import { onScrollListener } from '../../utils/ScrollListener'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import { getMixpanelLocationProperty } from '../../utils/GetMixpanelLocationProperty'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'
import localforage from 'localforage'
import { useGetTierUpgradeRequirements } from '../../services/api/queries/global'

const desktopStyle = {
  bottom: '30px',
  right: '30px',
}

const mobileStyle = {
  bottom: '75px',
  right: '10px',
  left: '100px,',
  height: '40px',
}

export default function Freshchat(props) {
  const {
    children,
    className = 'freshchat-widget',
    isFreshchatByParams,
    productSku,
    useFreshChatPopUp = false,
    setToggleModalLiveChat = {},
    toggleClickWidget = false,
    setToggleLiveChat = () => {},
    hideIcon = false,
    templateMessage = '',
  } = props

  const [display, setDisplay] = useState(false)
  const [isFetchTier, setisFetchTier] = useState(false)

  const { pageFrom } = useConstantsContext()
  const { state: cartAuthContext } = useCartAuthContext()

  const auth = cartAuthContext?.auth
  const isAuthRefetched = cartAuthContext?.isAuthRefetched

  const authUser = auth?.user
  const timer = useRef(null)
  const isMobile = config?.environment === 'mobile'
  const isDevelopment = config?.node_env === 'development'

  useGetTierUpgradeRequirements(
    {},
    {
      enabled: isFetchTier && !config.isB2b,
      onSuccess: (res) => {
        if (res?.[0]?.level && !config.isB2b) {
          localforage.setItem('klk_membership_level_text', res[0].level)
        }
      },
    },
  )

  const { mutate: mutateGetFreshchatRestoreId } = useGetFreshchatRestoreId({
    onSuccess: (res) => {
      if (!res.data?.error && res.data?.data?.restore_id) {
        localforage.setItem('freshchat_restore_id', res.data.data.restore_id)
      }

      loadScriptFreshchat()
    },
  })

  const { mutate: mutateUsePostFreshchatRestoreId } =
    usePostFreshchatRestoreId()

  const handleClassName = () => {
    if (display) {
      return `${className} active`
    }

    return className
  }

  const handleStyle = () => {
    if (!children && isMobile) {
      let mutatedStyle = { ...mobileStyle }
      if (pageFrom === 'home') {
        mutatedStyle.bottom = '125px'
      }
      return mutatedStyle
    }

    if (!children && !isMobile) {
      return desktopStyle
    }

    return {}
  }

  useEffect(() => {
    if (toggleClickWidget) {
      handleOpenFreshchatDialog()
      mixpanelTrack('Click Ruparupa Care', {
        Location: getMixpanelLocationProperty(pageFrom),
        ...productSku,
      })
    }
  }, [toggleClickWidget])

  const handleOnClickWidget = () => {
    if (useFreshChatPopUp) {
      setToggleModalLiveChat(true)
      return
    }
    handleOpenFreshchatDialog()
    mixpanelTrack('Click Ruparupa Care', {
      Location: getMixpanelLocationProperty(pageFrom),
      ...productSku,
    })
  }

  const handleOpenFreshchatDialog = () => {
    if (
      !config?.activeLenna &&
      window.fcWidget &&
      typeof window.fcWidget.open === 'function'
    ) {
      if (
        ['/jual', '/help'].some((path) =>
          window?.location?.pathname.includes(path),
        ) ||
        (config.environment === 'mobile' && window?.location?.pathname.includes('/p')) 
      ) {
        // directly open channel Chat with us!
        const chat = {
          name: 'Chat with us!', // this value MUST refer to the channel name
        }

        if (!isEmpty(templateMessage)) {
          chat.name = 'Hubungi Kami'
          chat.replyText = templateMessage
          // !!! Use this property to directly send freshchat template message to freshchat widget!!!
          chat.send = true
        }

        window.fcWidget.open(chat)
      } else {
        window.fcWidget.open()
      }
      return
    }

    if (document?.querySelector('.sc-launcher')) {
      document.querySelector('.sc-launcher').click()
    }
  }

  const handleChangeDisplay = (display) => {
    setDisplay(display)
  }

  const initListeners = () => {
    if (!window.fcWidget) {
      return
    }

    window.fcWidget.on('widget:closed', () => {
      handleChangeDisplay(true)
      setToggleLiveChat()
    })

    window.fcWidget.on('widget:opened', () => {
      handleChangeDisplay(false)
    })
  }

  const loadScriptFreshchat = () => {
    if (!window?.fcWidget) {
      const script = document.createElement('script')
      script.src = 'https://wchat.freshchat.com/js/widget.js'
      script.defer = true
      script.rel = 'dns-prefetch'
      document.body.appendChild(script)
      script.onload = initFreshchat

      return
    }

    initFreshchat()
  }

  const initFreshchat = async () => {
    if (!window?.fcWidget) {
      return
    }

    if (!isEmpty(authUser)) {
      loggedInSetup()
    } else {
      window?.fcWidget?.user?.clear()
      window?.fcWidget?.destroy()
      window?.fcWidget?.init(config?.freshchatConfig)
    }

    if (isFreshchatByParams === 'true') {
      handleOpenFreshchatDialog()
    }

    window.fcWidget.on('widget:loaded', () => {
      setTimeout(() => {
        handleChangeDisplay(true)
        initListeners()
      }, 2500)
    })
  }

  const loggedInSetup = async () => {
    const userRestoreId =
      (await localforage.getItem('freshchat_restore_id')) || ''
    const userMembershipLevel =
      (await localforage.getItem('klk_membership_level_text')) || ''
    const userMembershipId = authUser.id || ''
    const userMembershipStampsUrl = stampsUrl(userMembershipId)

    const initConfig = {
      ...config?.freshchatConfig,
      firstName: authUser?.name,
      email: authUser?.email,
      phone: authUser?.phone,
      externalId: authUser?.email,
      restoreId: userRestoreId,
      meta: {
        membership: userMembershipLevel,
        stamps_url: userMembershipStampsUrl,
      },
    }

    window?.fcWidget?.init(initConfig)

    window?.fcWidget?.on('user:created', (res) => {
      const isSuccess = res?.status === 200
      const createdRestoreId = res?.data?.restoreId

      if (isSuccess && authUser?.email && createdRestoreId !== userRestoreId) {
        mutateUsePostFreshchatRestoreId({
          external_id: authUser.email,
          restore_id: createdRestoreId,
        })

        localforage.setItem('freshchat_restore_id', createdRestoreId)
      }
    })
  }

  const stampsUrl = (customerId) => {
    const crm = isDevelopment ? 'crmstaging' : 'crm'
    const url = `https://${crm}-klg.stamps.id`
    const params = `login?next=/merchants/customers/${customerId}/`

    return `${url}/${params}`
  }

  useEffect(() => {
    if (!isAuthRefetched) {
      return
    }

    if (isEmpty(authUser)) {
      loadScriptFreshchat()
      return
    }

    if (authUser.id) {
      localforage.setItem('klk_user_id', authUser.id)
    }

    localforage.getItem('klk_membership_level_text').then((membershipLevel) => {
      if (!membershipLevel) {
        setisFetchTier(true)
      }
    })

    localforage.getItem('freshchat_restore_id').then((restoreId) => {
      if (restoreId) {
        loadScriptFreshchat()
        return
      }

      mutateGetFreshchatRestoreId({
        externalId: authUser?.email,
      })
    })
  }, [isAuthRefetched])

  useEffect(() => {
    const scrollListener = () => {
      timer.current = onScrollListener(
        timer.current,
        'freshchat-widget',
        '-130px',
        '10px',
      )
    }
    window.addEventListener('scroll', scrollListener)

    return () => {
      window.removeEventListener('scroll', scrollListener)
    }
  }, [])

  return (
    <div
      id='freshchat-widget'
      className={handleClassName()}
      style={handleStyle()}
      onClick={handleOnClickWidget}
    >
      <WidgetIcon showIcon={!children} hideIcon={hideIcon} />
      {children}
    </div>
  )
}

const WidgetIcon = (props) => {
  const { showIcon, hideIcon } = props
  const isDesktop = config?.environment === 'desktop'

  const iconAlt = 'Message Icon'
  const iconHeight = isDesktop ? '70' : '60'
  const iconSource = `${config?.assetsURL}icon/customer-care.png`
  const iconWidth = isDesktop ? '70' : '60'

  if (!showIcon || hideIcon) {
    return ''
  }
  return (
    <CustomLazyLoadImage
      alt={iconAlt}
      height={iconHeight}
      src={iconSource}
      width={iconWidth}
    />
  )
}
