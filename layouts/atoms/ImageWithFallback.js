import React, { useState, useEffect } from 'react'
import Skeleton from 'react-loading-skeleton'
import CustomLazyLoadImage from './CustomLazyLoadImage'

const ImageWithFallback = ({ src, fallbackSrc, alt, width, height }) => {
  const [imageSrc, setImageSrc] = useState(src)

  useEffect(() => {
    // Creating a new Image object to preload the image and listen for load/error events.
    const img = new Image()
    img.src = src

    const handleLoad = () => setImageSrc(src)
    const handleError = () => setImageSrc(fallbackSrc)

    // Assigning event handlers to the img object.
    img.onload = handleLoad
    img.onerror = handleError
  }, [src, fallbackSrc])

  // Rendering an img element with the current imageSrc state. This will initially attempt to display the original image, but will switch to the fallback image if an error occurs
  return (
    <CustomLazyLoadImage
      src={imageSrc}
      alt={alt}
      width={width}
      height={height}
      placeholder={<Skeleton height={height} width={width} />}
    />
  )
}

export default ImageWithFallback
