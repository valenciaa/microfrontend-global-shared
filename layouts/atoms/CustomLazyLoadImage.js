import React from 'react'
import { PropTypes } from 'prop-types'
import { LazyLoadComponent } from 'react-lazy-load-image-component'

class CustomLazyLoadImage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
    }
  }

  onImageLoad() {
    if (this.state.loaded) {
      return null
    }

    return () => {
      this.props.afterLoad()

      this.setState({
        loaded: true,
      })
    }
  }

  getImg(hide) {
    const { ...imgProps } = this.props

    let modifiedImgProps = { ...imgProps }
    // remove placeholder because img property cannot contain placeholder with object value
    // placeholder and wrapperClassName value is already used for lazyload below and not useful here
    delete modifiedImgProps.placeholder
    delete modifiedImgProps.wrapperClassName

    return (
      <img
        style={
          hide
            ? { opacity: 0, position: 'absolute', top: 0, left: 0 }
            : undefined
        }
        onLoad={this.onImageLoad()}
        {...modifiedImgProps}
      />
    )
  }

  getLazyLoadImage(hide) {
    const {
      beforeLoad,
      className,
      delayMethod,
      delayTime,
      height,
      scrollPosition,
      style,
      threshold,
      useIntersectionObserver,
      visibleByDefault,
      width,
    } = this.props

    return (
      <LazyLoadComponent
        beforeLoad={beforeLoad}
        className={className}
        delayMethod={delayMethod}
        delayTime={delayTime}
        height={height}
        placeholder={<span style={{ position: 'absolute', zIndex: '-1' }} />}
        scrollPosition={scrollPosition}
        style={style}
        threshold={threshold}
        useIntersectionObserver={useIntersectionObserver}
        visibleByDefault={visibleByDefault}
        width={width}
      >
        {this.getImg(hide)}
      </LazyLoadComponent>
    )
  }

  getWrappedLazyLoadImage(lazyLoadImage) {
    const {
      effect,
      height,
      placeholderSrc,
      width,
      wrapperClassName,
      wrapperProps,
      placeholder,
      placeholderAspectRatio,
      onClick = undefined,
    } = this.props
    const { loaded } = this.state
    const loadedClassName = loaded ? ' lazy-load-image-loaded' : ''

    return (
      <span
        onClick={onClick}
        className={
          'custom-lazy-load-image ' +
          wrapperClassName +
          ' lazy-load-image-background ' +
          effect +
          loadedClassName
        }
        style={{
          backgroundImage:
            loaded || !placeholderSrc ? '' : `url(${placeholderSrc})`,
          backgroundSize: loaded || !placeholderSrc ? '' : '100% 100%',
          color: 'transparent',
          display: 'flex',
          height: height,
          width: width,
          position: 'relative',
        }}
        {...wrapperProps}
      >
        {placeholder && !loaded && (
          <div
            style={{
              position: 'relative',
              width: '100%',
              paddingBottom: placeholderAspectRatio
                ? `${placeholderAspectRatio * 100}%`
                : '0',
            }}
          >
            <div
              className='custom-lazyload-container'
              style={
                placeholderAspectRatio
                  ? {
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                      paddingBottom: '8px',
                    }
                  : undefined
              }
            >
              {placeholder}
            </div>
          </div>
        )}
        {lazyLoadImage}
      </span>
    )
  }

  render() {
    const {
      effect,
      placeholderSrc,
      visibleByDefault,
      wrapperClassName,
      wrapperProps,
      placeholder,
    } = this.props

    const needsWrapper =
      (effect || placeholderSrc || placeholder) && !visibleByDefault
    const lazyLoadImage = this.getLazyLoadImage(
      needsWrapper && !this.state.loaded,
    )

    if (!needsWrapper && !wrapperClassName && !wrapperProps) {
      return lazyLoadImage
    }

    return this.getWrappedLazyLoadImage(lazyLoadImage)
  }
}

CustomLazyLoadImage.propTypes = {
  afterLoad: PropTypes.func,
  beforeLoad: PropTypes.func,
  delayMethod: PropTypes.string,
  delayTime: PropTypes.number,
  effect: PropTypes.string,
  placeholderSrc: PropTypes.string,
  threshold: PropTypes.number,
  useIntersectionObserver: PropTypes.bool,
  visibleByDefault: PropTypes.bool,
  wrapperClassName: PropTypes.string,
  wrapperProps: PropTypes.object,
}

CustomLazyLoadImage.defaultProps = {
  afterLoad: () => ({}),
  beforeLoad: () => ({}),
  delayMethod: 'throttle',
  delayTime: 300,
  effect: '',
  placeholderSrc: null,
  threshold: 100,
  useIntersectionObserver: true,
  visibleByDefault: false,
  wrapperClassName: '',
}

export default CustomLazyLoadImage
