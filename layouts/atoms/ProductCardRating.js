import React from 'react'
import config from '../../../../config'
import { numberExtFormater } from '../../utils/NumberWithExtFormater'
import CustomLazyLoadImage from './CustomLazyLoadImage'

export default function ProductCardRating({ reviewRating, fromVue = false }) {
  return (
    <div
      className={`col-xs-12 items-center ${fromVue ? '' : 'product__review'}`}
      style={{ display: 'flex' }}
    >
      <CustomLazyLoadImage
        src={`${config.assetsURL}icon/star-rating.svg`}
        alt='star-rate'
        id='star-rate'
        width={14}
        height={14}
      />
      <span className='ui-text-4 text__grey50' style={{ marginLeft: 4 }}>
        {reviewRating.rating} |
      </span>
      <span className='ui-text-4 text__grey50'>
        &nbsp;{numberExtFormater(reviewRating.total)}
        {numberExtFormater(reviewRating.total).includes('rb') ? '+' : null}{' '}
        (ulasan)
      </span>
    </div>
  )
}
