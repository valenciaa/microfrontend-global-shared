import ReactPaginate from 'react-paginate'

/**
 * Custom component for pagination use based on react-paginate library
 * @param {*} param0
 * @returns
 */
export default function Pagination({ containerClassName, ...props }) {
  return (
    <div className={containerClassName}>
      <ReactPaginate
        pageRangeDisplayed={2}
        renderOnZeroPageCount={null}
        previousLabel='<'
        nextLabel='>'
        pageClassName='pagination-item'
        pageLinkClassName='pagination-link'
        previousClassName='pagination-item'
        previousLinkClassName='pagination-link'
        nextClassName='pagination-item'
        nextLinkClassName='pagination-link'
        breakLabel='...'
        breakClassName='pagination-item'
        breakLinkClassName='pagination-link'
        marginPagesDisplayed={2}
        containerClassName='pagination-container flex-wrap'
        activeClassName='active'
        {...props}
      />
    </div>
  )
}
