import isBoolean from 'lodash/isBoolean'
import React from 'react'
// import config from '../../../../config'
import getColorTheme from '../../utils/GetColorTheme'

// type (primary, primary-border, secondary, secondary-border, disabled)
// size (big, medium, small)
// additionalClass (out of button scss)
// active (for toggle between initial type and the ghost one [active and not active])
export default function Button({
  type,
  size,
  name,
  handleOnClick,
  active,
  fetching,
  additionalClass,
  width,
  maxWidth,
  children,
  onMouseEnter,
  onMouseLeave,
  onFocus,
  onBlur,
  withCaptcha,
  disabled,
  isSubmit = false,
  backgroundColor = '',
  color = '',
  forceTheme = '',
}) {
  const status = isBoolean(active) ? active : null
  let buttonType = ' button__' + type
  if (status !== null) {
    if (!status) {
      buttonType = ' button__' + type + '-border--ghost'
    }
  }
  return (
    <button
      className={
        ' button ' +
        (additionalClass || '') +
        ' button--' +
        size +
        buttonType +
        ' ' +
        buttonType +
        '-' +
        getColorTheme(forceTheme) +
        (fetching || disabled ? ' button__disabled' : '')
      }
      // bg color and color used in FlashSaleContainer, must check this
      style={{
        width: width || '100%',
        backgroundColor: backgroundColor,
        color: color,
        maxWidth: maxWidth || '',
      }}
      onClick={(e) => {
        if (handleOnClick) {
          handleOnClick(e)
        }
      }}
      disabled={fetching || disabled || false}
      type={isSubmit ? 'submit' : 'button'}
      name={name}
      id={name}
      data-action={withCaptcha && 'submit'}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onFocus={onFocus}
      onBlur={onBlur}
    >
      {children}
    </button>
  )
}
