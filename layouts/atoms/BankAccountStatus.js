import clsx from 'clsx'
import React from 'react'
import config from '../../../../config'

const textStyle = {
  desktop: 'ui-text-5',
  mobile: 'button-small-text',
}

export default function BankAccountStatus() {
  return (
    <div
      className={`tw-bg-${config.companyNameCSS} tw-flex tw-items-center tw-justify-center tw-py-0.5 tw-px-1 tw-rounded-[20px] tw-max-w-[46px] tw-text-white`}
    >
      <p className={clsx(textStyle[config.environment])}>UTAMA</p>
    </div>
  )
}
