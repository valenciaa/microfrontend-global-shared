import React from 'react'
import clsx from 'clsx'
import config from '../../../../config'

export default function Dots({
  className = 'dots__default',
  backgroundColor = 'tw-bg-' + config.companyNameCSS,
  deactive,
  completed,
}) {
  if (deactive) {
    return (
      <div className={clsx('dots_icon', 'dots_icon__deactive', className)} />
    )
  }
  if (completed) {
    return (
      <div className={clsx('dots_icon', 'dots_icon__completed', className)} />
    )
  }
  return <div className={clsx(backgroundColor, className, 'dots')} />
}

export function DotsRefundInstallation({ customDotsClass, deactive }) {
  if (deactive) {
    return (
      <div
        className={clsx(
          'dots_icon_installation',
          'dots_icon_installation__deactive',
          customDotsClass,
        )}
      />
    )
  }

  return <div className={clsx('dots_icon_installation', customDotsClass)} />
}

export function Line({ customLineClass, deactive, completed }) {
  if (deactive) {
    return (
      <div
        className={clsx(
          'line_progress',
          'line_progress__deactive',
          customLineClass,
        )}
      />
    )
  }

  if (completed) {
    return (
      <div
        className={clsx(
          'line_progress',
          'line_progress__completed',
          customLineClass,
        )}
      />
    )
  }

  return <div className={clsx('line_progress', customLineClass)} />
}

export function LineRefundInstallation({ customLineClass, deactive }) {
  if (deactive) {
    return (
      <div
        className={clsx(
          'line_progress_installation',
          'line_progress_installation__deactive',
          customLineClass,
        )}
      />
    )
  }

  return <div className={clsx('line_progress_installation', customLineClass)} />
}
