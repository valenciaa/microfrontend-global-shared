import clsx from 'clsx'
import React from 'react'

const background = {
  new: 'bg-red-50',
}

const textColor = {
  new: 'color-white',
}

export default function BadgeIcon({ className, text, type }) {
  return (
    <div
      className={clsx(
        background[type],
        'badge-icon flex items-center justify-center',
        className,
      )}
    >
      <p className={clsx(textColor[type])}>{text}</p>
    </div>
  )
}
