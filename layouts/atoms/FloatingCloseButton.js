import React from 'react'
import CustomLazyLoadImage from './CustomLazyLoadImage'

// Div button with svg image
export default function FloatingCloseButton({ height, width, src, ...props }) {
  return (
    <div className='close-button__floating' {...props}>
      <CustomLazyLoadImage
        src={src}
        id='close-btn'
        alt='close-btn'
        height={height}
        width={width}
      />
    </div>
  )
}
