import React from 'react'
import config from '../../../../config'

export default function InfoBoxText({ color, children }) {
  return (
    <div className={'info-box info-box--' + color}>
      <div className='info-box__icon-space'>
        <img src={config.assetsURL + 'icon/info-stroke-grey.svg'} />
      </div>
      <div className='info-box__text-space'>{children}</div>
    </div>
  )
}
