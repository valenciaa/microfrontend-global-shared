import React, { useEffect } from 'react'
import config from '../../../../config'
import isEmpty from 'lodash/isEmpty'
import { useGetTahuWordingContent } from '../../services/api/queries/homepage'
import GetB2bType from '../../utils/GetB2bType'

export default function TahuWordingContent(props) {
  const { title, companyCodeAll, companyCodeCustom, pageType, setShowFooter } =
    props
  const companyCode = companyCodeAll
    ? 'All'
    : companyCodeCustom || config.companyCode

  let companyCodeModified = companyCode
  let titleModified = title

  if (GetB2bType() === 'ace_b2b') {
    // ACE_HARDWARE
    companyCodeModified = 'A2B'
  }

  if (config.isB2b && config.companyCode === 'HCI') {
    companyCodeModified = 'H2B'
  }

  if (pageType === 'ruparupab2b') {
    companyCodeModified = 'O2B'
    titleModified = 'footer-b2b-rr'
  }

  const { data: _payload, isLoading } = useGetTahuWordingContent(
    { companyCode: companyCodeModified, title: titleModified },
    {},
  )
  const payload = !isEmpty(_payload) ? _payload : null

  function renderWordingContent() {
    let finalWordingContent
    if (!isLoading && payload && payload.status === 10) {
      if (!isEmpty(payload[config.environment].body)) {
        finalWordingContent = payload[config.environment].body
      } else if (!isEmpty(payload.globalBody)) {
        finalWordingContent = payload.globalBody
      }
    }

    return finalWordingContent
  }

  useEffect(() => {
    if (
      !isLoading &&
      payload == null &&
      pageType === 'aceb2b' &&
      config.environment !== 'mobile'
    ) {
      setShowFooter(false)
    }
  }, [isLoading])
  if (renderWordingContent()) {
    return <div dangerouslySetInnerHTML={{ __html: renderWordingContent() }} />
  } else {
    return null
  }
}
