import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import config from '../../../../config'
import ComponentWrapper from '../templates/ComponentWrapper'

export default function HorizontalLine({ type, className = '' }) {
  const [refHorizontalLine, inViewHorizontalLine] = useInView({
    threshold: 1,
    triggerOnce: true,
  })
  const [horizontalLineViewPort, setHorizontalLineViewport] = useState(false)
  useEffect(() => {
    if (inViewHorizontalLine) {
      setHorizontalLineViewport(true)
    }
  }, [inViewHorizontalLine])
  return (
    <>
      {horizontalLineViewPort ? (
        type === 'search' ? (
          <div className={`margin-left-m margin-right-m ${className}`}>
            <hr className='normal' />
          </div>
        ) : (
          <hr
            className={`${
              config.environment === 'mobile' ? 'bold bold-no-space' : 'normal'
            } ${className}`}
          />
        )
      ) : (
        <ComponentWrapper
          ref={refHorizontalLine}
          height={config.environment === 'mobile' ? 4 : 1}
        />
      )}
    </>
  )
}
