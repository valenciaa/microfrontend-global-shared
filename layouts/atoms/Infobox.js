import clsx from 'clsx'
import React from 'react'
import isEmpty from 'lodash/isEmpty'

// Look at _infobox.scss
// theme => info(blue), error(red)
export default function Infobox({
  children,
  className,
  textClassName = 'margin-left-xs ui-text-3',
  size = 'size-default',
  theme,
  text,
  subtext,
  onClickSubText,
}) {
  return (
    <div
      className={clsx(
        'items-center infobox',
        'infobox__' + theme,
        size,
        className,
      )}
    >
      {children}
      {text?.length > 0 && (
        <p className={textClassName}>
          {text}
          {!isEmpty(subtext) && (
            <span className='text-semi-bold' onClick={onClickSubText}>
              {subtext}
            </span>
          )}
        </p>
      )}
    </div>
  )
}
