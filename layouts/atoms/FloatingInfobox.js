import { useEffect, useState } from 'react'
import clsx from 'clsx'
import config from '../../../../config'

export default function FloatingInfobox({
  children,
  duration,
  setToggle,
  toggleFlag,
  spanText = 'OK',
  type,
  className = '',
  wrapperClassName,
  textAction,
  additionalButton,
  additionalButtonClassName,
  additionalButtonClose,
  additionalAction,
  contentSpaceClassName = 'gap-s',
  textClassName = '',
  useSpanText = false,
}) {
  const [showInfobox, setShowInfobox] = useState(true)
  let timer = null
  useEffect(() => {
    if (duration) {
      timer = setTimeout(() => {
        if (toggleFlag) {
          setShowInfobox(false)
          setToggle(false)
          additionalAction && additionalAction()
        }
      }, duration)
    }
    return function cleanup() {
      clearTimeout(timer)
    }
  }, [])

  function handleAdditional() {
    if (additionalButton?.href) {
      window.location.href = additionalButton?.href
    } else if (additionalButton?.action) {
      additionalButton.action()
    }
  }

  const handleCloseInfobox = (e) => {
    e.preventDefault()
    clearTimeout(timer)
    setShowInfobox(false)
    setToggle(false)
  }

  return (
    <>
      {showInfobox && (
        <div className={clsx('infobox-wrapper', wrapperClassName)}>
          {config.environment === 'desktop' ? (
            <div
              className={clsx(
                `floating-infobox__${config.environment} floating-infobox__${type}`,
                className,
              )}
              style={
                additionalButton?.text
                  ? { pointerEvents: 'unset', zIndex: 10001 }
                  : { zIndex: 10001 }
              }
            >
              <div
                className={`infobox-content tw-items-center ${
                  additionalButton?.text ? 'justify-between' : ''
                }`}
              >
                <span className={'ui-text-2-medium ' + textClassName || ''}>
                  {children}
                </span>
                {(!duration || useSpanText) && (
                  <span
                    className='infobox-btn ui-text-2-medium'
                    onClick={(e) => handleCloseInfobox(e)}
                  >
                    {spanText}
                  </span>
                )}
                {additionalButton?.text && (
                  <div
                    className='ui-text-2-medium cursor-pointer margin-left-xl'
                    onClick={(e) =>
                      additionalButtonClose
                        ? handleCloseInfobox(e)
                        : handleAdditional()
                    }
                  >
                    <b>{additionalButton?.text}</b>
                  </div>
                )}
              </div>
            </div>
          ) : (
            <div
              className={clsx(
                'floating-infobox',
                `floating-infobox__${type}`,
                className,
              )}
              style={additionalButton?.text ? { pointerEvents: 'unset' } : {}}
            >
              <div
                className={clsx(
                  'infobox-content',
                  additionalButton?.text ? 'justify-between' : '',
                  contentSpaceClassName,
                )}
              >
                {![
                  'info',
                  'success-text-white',
                  'warning-pdp',
                  'warning-top-pajak',
                  'info-modal',
                  'error-modal',
                  'success-modal',
                  'warning-modal',
                  'error-minicart',
                ].includes(type) && (
                  <img
                    src={`${config.assetsURL}icon/icon-infobox-${type}.svg`}
                    height={16}
                    width={16}
                    alt={`icon-${type}`}
                  />
                )}
                <span className='ui-text-3'>{children}</span>
                {textAction && (
                  <div
                    className='infobox-btn ui-text-3'
                    onClick={(e) => handleCloseInfobox(e)}
                  >
                    {textAction || 'OK'}
                  </div>
                )}
                {additionalButton?.text && (
                  <div
                    className={clsx(
                      'flex items-center',
                      additionalButtonClassName ?? 'ui-text-3',
                    )}
                    onClick={(e) =>
                      additionalButtonClose
                        ? handleCloseInfobox(e)
                        : handleAdditional()
                    }
                  >
                    <b>{additionalButton?.text}</b>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
      )}
    </>
  )
}
