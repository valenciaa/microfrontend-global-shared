import React from 'react'
import clsx from 'clsx'
import config from '../../../../config'

const contentWrapperStyle = {
  desktop: 'gap-xxs',
  mobile: 'gap-xs',
}

const titleStyle = {
  desktop: 'text-left price-normal',
  mobile: 'heading-3',
}

export default function CoachmarkContent({ children, title }) {
  return (
    <div
      className={clsx('flex flex-col', contentWrapperStyle[config.environment])}
    >
      <p className={clsx(titleStyle[config.environment])}>{title}</p>
      {children}
    </div>
  )
}
