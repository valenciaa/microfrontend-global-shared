import React from 'react'
import { useTour } from '@reactour/tour'
import Button from './Button'
import Cookies from 'js-cookie'
import config from '../../../../config'

export default function CoachmarkContentV2({
  children,
  title,
  additionalClass = '',
}) {
  const { steps, setCurrentStep, currentStep } = useTour()

  const handleCoachMark = (stepType) => {
    setCurrentStep((prevStep) => {
      if (prevStep === steps.length - 1) {
        Cookies.set(
          'coachmark-' + 'home-help-layout' + '-' + config?.companyNameCSS,
          1,
          {
            expires: 1095,
          },
        ) // 3 years
      }
      let coachStep = prevStep - 1
      if (stepType === 'next') {
        coachStep = prevStep + 1
      }
      return coachStep
    })

    if (currentStep + 1 === steps.length) {
      document.body.style.overflow = ''
    }
  }

  const handleDotsPagination = () => {
    const collection = []
    for (let index = 0; index < steps.length; index++) {
      const isActive = currentStep === index
      collection.push(
        isActive ? (
          <div
            key={'pagination-' + index}
            className='w-xs h-xs rounded-full bg-orange-50'
          />
        ) : (
          <div
            key={'pagination -' + index}
            className='w-xs h-xs rounded-full bg-white'
          />
        ),
      )
    }
    return collection
  }
  return (
    <div className='gap-2xs flex column w-full'>
      <p className='text-sm font-bold ui-text-2'>{title}</p>
      {children}
      <div className={'flex justify-between ' + additionalClass}>
        <div className='flex flex-row space-x-2 mt-4 w-full align-end gap-2xs'>
          {handleDotsPagination()}
        </div>

        <div
          className={`flex item-center  
          ${currentStep > 0 ? 'mw-42 justify-between' : 'justify-end '}
           w-full `}
        >
          {currentStep > 0 && (
            <p className='ui-text-4' onClick={() => handleCoachMark('prev')}>
              Kembali
            </p>
          )}
          <Button
            size='small'
            type='primary button__primary-ruparupa ui-text-4 font-semibold'
            width={81}
            isSubmit
            additionalClass='padding-y-xs padding-x-s'
            handleOnClick={() => handleCoachMark('next')}
          >
            Lanjut
          </Button>
        </div>
      </div>
    </div>
  )
}
