import React, { useEffect } from 'react'
// import config from '../../../../config'
import { useLayoutContext } from '../../../../src/context/LayoutContext'
import CloseButton from '../../../../public/static/icon/close.svg'

// Usually used for desktop
export default function HeaderDetailModal({
  size,
  posLeft,
  posRight,
  posTop,
  type,
  title,
  content,
  isOnHover,
  additionalClass,
}) {
  const { state: layoutContext, handleChangeState: handleChangeLayoutContext } =
    useLayoutContext()
  useEffect(() => {
    if (type === 'fullscreen') {
      document.getElementsByTagName('body')[0].style.overflow = 'hidden'
    }
  }, [])
  return (
    <div
      className={`header-detail header-detail__container ${
        title === 'Cek Pesanan Saya' || title === 'Pesanan Saya'
          ? isOnHover
            ? 'show-modal'
            : 'hide-modal'
          : ''
      }`}
      style={{
        width: size,
        top: posTop || '',
        right: posRight || '',
        height: type === 'fullscreen' ? '100vh' : '',
        left: posLeft || undefined,
      }}
    >
      {/* // Cek Pesanan Saya is only temporary */}
      <div className='row'>
        {title && (
          <div className='col-xs-12 title'>
            <div className='header-detail__top border-bottom'>
              <div className={`header-detail__title ${additionalClass || ''}`}>
                {title}
              </div>
              <div
                className='header-detail__close'
                onClick={() => {
                  handleChangeLayoutContext(
                    'isOverlayOn',
                    !layoutContext?.isOverlayOn,
                  )
                }}
              >
                {/* <img src={config.assetsURL + 'icon/close.svg'} width={24} height={24} alt='close' /> */}
                <CloseButton
                  className='color-grey-100 cursor-pointer'
                  height='1.5em'
                  width='1.5em'
                  alt='Close Button'
                />
              </div>
            </div>
          </div>
        )}
        <div className='col-xs-12'>
          <div className='row header-detail__body'>{content}</div>
        </div>
      </div>
    </div>
  )
}
