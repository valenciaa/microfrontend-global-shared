import Skeleton from 'react-loading-skeleton'
import { ConvertImageType, GetUniqueId } from '../../utils'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'
import {
  bodyTrack,
  setDefaultData,
  setMetaData,
  vueRecoLabel,
} from '../../utils/VueAIUtil'
import { useSendTrackEvent } from '../../services/api/mutations/global'
import {
  generateTrackingPayload,
  mixpanelTrack,
  mixpanelVueRecommendationTrack,
} from '../../utils/MixpanelWrapper'
import VueItmGenerator from '../../utils/VueItmGenerator'
import config from '../../../../config'
import ProductCardRating from '../atoms/ProductCardRating'
import isEmpty from 'lodash/isEmpty'
import { useRouter } from 'next/router'

export default function CardVueAiWrapper({
  product,
  dataVueReco,
  index,
  title,
  location,
  productDetailVue,
  templateType = 'simple-grid',
  pcpCategory,
  useKeyword,
  correlationID = '',
  vueIndex = '',
  from = '',
  position = 'swiper',
}) {
  const { mutate: sendTracking } = useSendTrackEvent()
  let locationManipulate = location
  const router = useRouter()
  const keyword = router?.query?.keyword || ''

  if (location === 'PCP Brand') {
    locationManipulate = 'pcp-brand'
  }
  if (keyword) {
    locationManipulate = 'search'
  }
  if (location === 'tahu') {
    locationManipulate = 'microsite'
  }
  const sendEventProduct = async (product, newUrl, index) => {
    const uuid = await GetUniqueId()
    const productDetail = [
      {
        product_id: product?.id,
        price: product?.price?.toString(),
      },
    ]
    const postReco = index + 1 // to avoid start from 0
    const url = window.location.href
    const defaultData = setDefaultData(
      url,
      window?.navigation?.activation?.from?.url || '',
      'recommendedProductView',
      'click',
      'false',
      uuid,
    )

    let strategyId = dataVueReco?.data?.[index]?.strategy_id

    if (correlationID && vueIndex !== '') {
      dataVueReco.module_id = dataVueReco[vueIndex].module_id

      strategyId = dataVueReco[vueIndex]?.data?.[index]?.strategy_id
      dataVueReco.module_name = dataVueReco[vueIndex].module_name
    }
    const metaData = setMetaData(
      null,
      postReco,
      null,
      null,
      locationManipulate,
      productDetail,
      dataVueReco?.module_id,
      dataVueReco?.module_name,
      'inline',
      templateType,
      dataVueReco?.data?.length,
      strategyId,
      productDetailVue,
    )

    if (!isEmpty(correlationID)) {
      defaultData.correlation_id = correlationID
    }

    if (from === 'cat' && metaData?.page_type === 'search') {
      metaData.page_type = 'pcp-search'
    }

    sendTracking(
      bodyTrack(defaultData, dataVueReco?._journey_metadata, metaData),
    )

    if (location === 'cat') {
      location = 'PCP'
    }

    if (location === 'tahu') {
      location = 'microsite'
    }

    mixpanelVueRecommendationTrack(
      location,
      title,
      location,
      product,
      dataVueReco?.module_name,
      newUrl,
    )

    //tracking click PDP
    mixpanelTrack(
      'Click PDP',
      generateTrackingPayload({
        'Location': location || 'None',
        'Section': title?.includes('Produk Pilihan Untuk')
          ? 'Produk Pilihan Untukmu'
          : title || 'None',
        'Sub Section': 'None',
        'Position': index + 1 || 'None',
        'Item ID': product?.id || 'None',
        'Item Name': product?.title || 'None',
      }),
    )
    // window.open(newUrl)
  }

  let isPdpRedirect = false
  if (router?.query?.st) {
    isPdpRedirect = true
  }

  const newUrl = VueItmGenerator(
    product?.link,
    product?.id,
    title,
    locationManipulate,
    pcpCategory,
    useKeyword,
    isPdpRedirect,
  )

  const reviewRate =
    product?.c_rating && product?.c_review_count !== '0'
      ? { rating: product?.c_rating, total: product?.c_review_count }
      : null

  let additionalAtt = {}
  if (!isEmpty(product)) {
    additionalAtt = {
      href: newUrl,
    }
  }

  const labelProduct = vueRecoLabel(
    product?.[`label_${config?.companyCode?.toLowerCase()}`],
  )

  let cardClassName = ''
  if (position === 'grid' && config.environment === 'mobile') {
    cardClassName = ' !tw-min-w-[11.5em]'
  }

  return (
    <span
      data-scarabitem={product?.id}
      className={'col-xs-2 margin-bottom-m' + cardClassName}
      key={product?.id}
    >
      <a
        {...additionalAtt}
        className={`h-full ${isEmpty(product) ? 'pointer-unset' : ''}`}
        onClick={() => sendEventProduct(product, newUrl, index)}
        style={{ cursor: 'pointer' }}
      >
        <div className='row product-card justify-content-center'>
          <div className='col-xs-12 content__product '>
            <div className='row middle-xs'>
              <div className='col-xs-12'>
                <CustomLazyLoadImage
                  src={ConvertImageType(
                    product?.image?.replace(
                      ('https://res.cloudinary.com/ruparupa-com/image/upload/',
                      'https://res.cloudinary.com/ruparupa-com/image/upload/f_auto,q_auto/'),
                    ),
                  )}
                  id={product?.title?.replace('-', '').replace(/ +/g, '-')}
                  alt={product?.title?.replace('-', '').replace(/ +/g, '-')}
                  width='100%'
                  placeholder={
                    <Skeleton width={config.environment === 'mobile' && 155} />
                  }
                  className='tw-h-[165px]'
                />
              </div>
              {product?.product_label && (
                <div className='col-xs-12 product__grouping margin-top-xs'>
                  {product?.product_label}
                </div>
              )}
              <div className='col-xs-12 margin-top-xs'>
                <span className='product__name'>
                  {product?.title || <Skeleton height={20} />}
                </span>
              </div>
              <div className='col-xs-12'>
                <div className='row middle-xs price'>
                  {(product?.discount > 0 ||
                    product?.msrp !== product?.price) && (
                    <>
                      <div className='price__initial'>
                        Rp{product?.msrp?.toLocaleString(['ban', 'id'])}
                      </div>
                      <div className='price__discount'>
                        <span>{product?.discount}%</span>
                      </div>
                    </>
                  )}
                  <br />
                  <div className='col-xs-12 price__real'>
                    {product?.price ? (
                      `Rp${product?.price?.toLocaleString(['ban', 'id'])}`
                    ) : (
                      <Skeleton height={20} />
                    )}
                  </div>

                  {!isEmpty(labelProduct) && (
                    <div className='flex-row flex items-center margin-top-s w-full'>
                      <CustomLazyLoadImage
                        src={`${config.assetsURL}icon/info-red.svg`}
                        alt='info-online-exclusive'
                        width={14}
                        height={14}
                        placeholder={<Skeleton height={14} />}
                      />
                      <span className='text__error ui-text-3 font-xs margin-left-xxs'>
                        {labelProduct}
                      </span>
                    </div>
                  )}
                  {reviewRate && reviewRate?.total !== 0 && (
                    <div className='margin-top-xs'>
                      <ProductCardRating reviewRating={reviewRate} fromVue />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </span>
  )
}
