import React from 'react'
import { SwiperSlide } from 'swiper/react'
import ProductDetailSlider from '../../../../src/pdp/components/ProductDetailSlider'
import { useConstantsContext } from '../../context/ConstantsContext'
import CardWrapper from './CardWrapper'
import {
  generateTrackingPayload,
  mixpanelTrack,
} from '../../utils/MixpanelWrapper'

// Components for
// PDP (Produk yang Mungkin Anda Suka, Lengkapi Belanjaanmu, Produk yang terakhir dilihat)
export default function ProductSlider({
  location,
  products,
  propsItmSource,
  onClick,
  sectionTitle,
  type,
  ...props
}) {
  const constantsContext = useConstantsContext()
  return (
    <ProductDetailSlider {...props}>
      {products?.map((product, index) => {
        return (
          <SwiperSlide key={index}>
            <CardWrapper
              devices={constantsContext?.devices}
              onClick={() => {
                onClick && onClick(product, location, sectionTitle)
                mixpanelTrack(
                  'Click PDP',
                  generateTrackingPayload({
                    'Location': 'PDP', // eslint-disable-line
                    'Section': 'Produk Terakhir di Lihat', // eslint-disable-line
                    'Sub Section': 'None',
                    'Position': index + 1 || 'None', // eslint-disable-line
                    'Item ID': product?.variants?.[0]?.sku || 'None',
                    'Item Name': product?.name || 'None',
                  }),
                )
              }}
              product={product}
              propsItmSource={propsItmSource}
              type={type}
              index={index}
            />
          </SwiperSlide>
        )
      })}
    </ProductDetailSlider>
  )
}
