import React from 'react'
import config from '../../../../config'
import { ConvertImageType } from '../../utils'

export default function DomLoading() {
  return (
    <div id='splash-screen' className='splash-screen'>
      <img
        src={ConvertImageType(
          config.assetsURL + 'images/logo-ruparupa-landscape.svg',
        )}
        width='65%'
        height='100%'
      />
    </div>
  )
}
