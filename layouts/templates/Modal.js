import React, { useEffect, useRef, useState } from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import config from '../../../../config'
import clsx from '../../utils/Clsx'
import { usePrevious } from '../../utils/UsePrevious'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'

/*
  Reference
    Medium: https://medium.com/@lucksp_22012/pure-react-modal-6e562a317b85
    Sandbox: https://codesandbox.io/s/5xrxwr0nlk?from-embed=&file=/src/components/Modal/index.js
*/
const ModalBackdrop = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: ${(props) => {
    switch (props.modalDepth) {
      case 2:
        return 1051
      case 3:
        return 1061
      default:
        return 1040
    }
  }};
  background-color: black;

  &.fade {
    opacity: 0;
  }

  &.show {
    opacity: 0.5 !important;
  }
`

const StyledModal = styled.div`
  position: fixed;
  top: ${(props) => {
    switch (props.modalType) {
      case 'bottom':
        return 'unset !important;'
      case 'center':
        return '5vh !important;'
      case 'deals-center':
        return '15% !important;'
      case 'middle-small':
        return '40vh !important;'
      case 'custom':
        return props.customTop + ' !important;'
      default:
        return 0
    }
  }};
  ${(props) => {
    switch (props.modalType) {
      case 'deals-center':
        return {
          display: 'flex !important',
          height: 'fit-content',
          margin: '40px auto',
          width: '632px',
        }
    }
  }}
  right: 0;
  bottom: 0;
  left: 0;
  z-index: ${(props) => {
    switch (props.modalDepth) {
      case 2:
        return 1060
      case 3:
        return 1070
      default:
        return 1050
    }
  }};
  display: none;
  overflow: hidden;
  outline: 0;
  opacity: 0;
  transition: opacity 0.15s linear;

  .modal-open & {
    overflow-x: hidden;
    overflow-y: auto;
  }

  &.fade-in {
    opacity: 1;
    transition: opacity linear 0.15s;
  }

  &.fade-out {
    opacity: 0;
    transition: opacity linear 0.15s;
  }
`

const Modal = ({
  bodyClass,
  bodyElement,
  contentClass,
  dialogClass,
  footer,
  footerClass,
  footerElement,
  headerClass,
  headerElement = false,
  modalType,
  onClose,
  show,
  backdropOnClose = false,
  needFloatingCloseButtonElement,
  floatingCloseButtonElement,
  customTop,
  addScroller = '',
  customStyled = '',
  isV2 = false,
  headerSecClass,
  headerElementClass,
  dialogStyle = {},
  extraModalStyle = '',
  modalBodyStyle,
  isPopUpModal = false,
  popUpModalHeader,
  modalDepth = 1,
}) => {
  const prevShow = usePrevious(show)
  const mounted = useRef(true)
  const [fadeType, setFadeType] = useState(null)
  useEffect(() => {
    window.addEventListener('keydown', onEscKeyDown, false)
    setTimeout(() => {
      setFadeType('in')
    }, 0)
    return () => window.removeEventListener('keydown', onEscKeyDown, false)
  }, [])

  // add class and style to body like bootstrap
  useEffect(() => {
    if (show) {
      document.body.classList.toggle('modal-open', show)
      document.body.style.overflow = 'hidden'
    }
    return () => {
      document.body.style.removeProperty('overflow')
      document.body.classList.remove('modal-open')
    }
  }, [show])

  // componentDidUpdate(prevProps)
  useEffect(() => {
    if (mounted.current) {
      mounted.current = false
    }
    if (!show && prevShow) {
      setFadeType('out')
    }
  }, [show])
  const transitionEnd = (e) => {
    if (e.propertyName !== 'opacity' || fadeType === 'in') {
      return
    }
    if (fadeType === 'out') {
      onClose()
      document.body.style.overflow = null
      document.body.classList.remove('modal-open')
    }
  }
  const onEscKeyDown = (e) => {
    if (e.key !== 'Escape') {
      return
    }
    setFadeType('out')
  }
  const handleClose = () => {
    setFadeType('out')
  }
  return ReactDOM.createPortal(
    <>
      {!isV2 && (
        <ModalBackdrop
          // className='fade show index-1'
          className='fade show'
          modalDepth={modalDepth}
          onClick={() => (backdropOnClose ? handleClose() : null)}
        />
      )}
      <StyledModal
        aria-modal
        className={`fade-${fadeType} ${customStyled}`}
        modalType={`${modalType}`}
        customTop={`${customTop}`}
        modalDepth={modalDepth}
        onTransitionEnd={transitionEnd}
        role='dialog'
        style={{ display: 'block', ...extraModalStyle }}
        tabIndex={-1}
      >
        <div
          className={`modal-dialog ${dialogClass || ''}`}
          style={dialogStyle}
        >
          <div className={`modal-content ${contentClass || ''}`}>
            {headerElement && !isPopUpModal && (
              <div className={`modal-header  ${headerClass || ''}`}>
                <div
                  className={`heading-2 padding-top-s text-center ${
                    headerSecClass || ''
                  }`}
                  style={{ position: 'relative', width: '100%' }}
                >
                  {!isV2 && (
                    <>
                      {headerElement}
                      {headerElement && (
                        <div
                          className='close'
                          style={{
                            position: 'absolute',
                            right: 0,
                            top: '0.15vh',
                          }}
                          onClick={() => handleClose()}
                        >
                          <CustomLazyLoadImage
                            src={config.assetsURL + 'icon/close-fill.svg'}
                            className='close-btn'
                            id='close-btn'
                            alt='close-btn'
                            height={24}
                            width={24}
                          />
                        </div>
                      )}
                    </>
                  )}
                  {isV2 && (
                    <>
                      <div className={headerElementClass || ''}>
                        {headerElement}
                      </div>
                      <div className='close-icon' onClick={() => handleClose()}>
                        <CustomLazyLoadImage
                          className={clsx('icon')}
                          src={config?.assetsURL + 'icon/close-dark.svg'}
                        />
                      </div>
                    </>
                  )}
                </div>
              </div>
            )}
            {isPopUpModal && popUpModalHeader()}
            {/*
              floatingCloseButtonElement is only used by mobile web view
              Check on DeliveryAndPickupSection
              You should pass margin-top-xxxl to contentClass to provide extra space,
              needFloatingCloseButtonElement, and a function that contain close button
            */}
            {needFloatingCloseButtonElement &&
              floatingCloseButtonElement(handleClose)}
            <div
              className={`modal-body ${bodyClass || ''} ${addScroller}`}
              style={modalBodyStyle}
            >
              {bodyElement(handleClose)}
            </div>
            {footer && (
              <div className={`modal-footer ${footerClass || ''}`}>
                {footerElement}
              </div>
            )}
          </div>
        </div>

        {isV2 && (
          <ModalBackdrop
            className='fade show index-1'
            onClick={() => (backdropOnClose ? handleClose() : null)}
          />
        )}
      </StyledModal>
    </>,
    document.body,
  )
}

export default Modal
