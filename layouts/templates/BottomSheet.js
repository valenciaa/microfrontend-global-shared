import React from 'react'

export default function BottomSheet({ template }) {
  return (
    <div className='modal'>
      <div className='title margin-bottom-s heading-2'>
        {template.current_schedule.title}
      </div>
      <div
        className='description'
        style={{
          color: template.current_schedule.text_desc_color,
        }}
        dangerouslySetInnerHTML={{
          __html: `${template.current_schedule.description
            .replace(/<ol>/g, '<ol style="padding: 0px 30px">')
            .replace(/<ul>/g, '<ul style="padding: 0px 30px">')
            .replace(/�/g, '')
            .replace(
              /float:none;/g,
              'float:none;margin-left:auto;margin-right: auto;display:block;',
            )}`,
        }}
      />
    </div>
  )
}
