import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'
import config from '../../../../config'

export default function HtmlModal({ onClose, title, content }) {
  if (content) {
    return (
      <>
        <div className='heading-2 padding-bottom-m'>
          {title}
          <div onClick={() => onClose()}>
            <CustomLazyLoadImage
              src={config.assetsURL + 'icon/close-fill.svg'}
              className='close-btn'
              id='close-btn'
              alt='close-btn'
              height={24}
              width={24}
            />
          </div>
        </div>
        <div dangerouslySetInnerHTML={{ __html: content || '' }} />
      </>
    )
  } else {
    return null
  }
}
