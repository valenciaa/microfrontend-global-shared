import React, { useEffect } from 'react'
import clsx from '../../utils/Clsx'

export const ModalHeader = ({ children, className }) => (
  <div className='modal__header'>
    <div className={clsx('modal__header-row', className)}>{children}</div>
  </div>
)
export const ModalBody = ({ children, className }) => (
  <div className={clsx('modal__body', className)}>{children}</div>
)
export const ModalFooter = ({ children, className }) => (
  <div className={clsx('modal__footer', className)}>{children}</div>
)

export const ModalV2 = ({
  className,
  style,
  isShow = false,
  onClose,
  children,
}) => {
  useEffect(() => {
    const modal = document.querySelectorAll('.modal__page.is-active')
    document.body.style.overflow = modal.length > 0 ? 'hidden' : 'auto'
    return function cleanUp() {
      document.body.removeAttribute('style')
    }
  }, [isShow])
  return (
    <>
      <div className={clsx('modal__page', { 'is-active': isShow })}>
        <div className={clsx('modal__dialog', className)} style={style}>
          {children}
        </div>
      </div>
      <div
        className={clsx('modal__backdrop', { 'is-active': isShow })}
        onClick={onClose}
      />
    </>
  )
}

export const ModalPull = ({ onClick }) => {
  return (
    <div className='drawer__pull' onClick={onClick}>
      <div className='drawer__icon'>
        <span className='icon__pull' />
        <span className='icon__pull' />
      </div>
    </div>
  )
}

export default ModalV2
