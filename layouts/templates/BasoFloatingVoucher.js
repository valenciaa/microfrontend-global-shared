import React, { useEffect, useState } from 'react'

import isEmpty from 'lodash/isEmpty'

import Cookies from 'js-cookie'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'

import config from '../../../../config'
import Button from '../atoms/Button'
import CopyActive from '../../../../public/static/icon/copy-active.svg'
import { useLayoutContext } from '../../../../src/context/LayoutContext'
import { useGetFloatingVoucher } from '../../services/api/queries/global'

import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import { addfloatingVoucher } from '../../utils/DataLayerGoogle'

export default function BasoFloatingVoucher({
  urlKey,
  setToggleClipboardInfobox,
}) {
  const { handleChangeState: handleChangeLayoutContext } = useLayoutContext()
  const [activeVoucher, setActiveVoucher] = useState({})
  const [isVoucherShow, setIsVoucherShow] = useState(false)
  const [isShowFloatingVoucher, setIsShowFloatingVoucher] = useState(1)

  const { data: floatingVoucherPayload } = useGetFloatingVoucher({
    companyCode: config.companyCode.toLowerCase(),
    platform: config.environment,
  })

  useEffect(() => {
    setIsShowFloatingVoucher(
      parseInt(Cookies.get(`floating-voucher-${config.companyCode}`) || 1),
    )
  }, [])

  useEffect(() => {
    if (isEmpty(activeVoucher)) {
      handleSetVoucher()
    }
  }, [floatingVoucherPayload])

  function handleSetVoucher() {
    const isIndexRgx = /^.*(.com|.io|.my.id)(\/|)$/
    let activeVoucher = {}
    /*
    forceBreak flag is needed because we only can break the loop if the voucher already chosen,
    Right now there is an added condition which mention that if the current url key is included on
    specific url list and the page_type is all means the current url key is excluded (voucher isn't shown),
    then we have to force it to break from the loop and return empty voucher instead
    */
    let forceBreak = false
    const floatingVoucherPayload_ = floatingVoucherPayload || {}

    // Decide which voucher that is going to be shown
    for (let i = 0; i < floatingVoucherPayload_.length; i++) {
      if (!isEmpty(floatingVoucherPayload_[i].specific_urls)) {
        const urlLength = floatingVoucherPayload_[i].specific_urls.length
        for (let j = 0; j < urlLength; j++) {
          const currentUrl = floatingVoucherPayload_[i].specific_urls[j]
          let found = false // the original logic is to check the URL KEY searching for index, if its in index then it will return true
          // === THIS IS THE ORIGINAL LOGIC === //
          // Check if currentUrl include urlkey or currentUrl is index page (/)
          if (
            (!isEmpty(urlKey) && currentUrl.includes(urlKey)) ||
            (isIndexRgx.test(currentUrl) && isEmpty(urlKey))
          ) {
            found = true
          }
          // ========== //

          // Decide activeVoucher value
          if (floatingVoucherPayload_[i].page_type === 'all') {
            /*
                1. found = true && pagetype = all => urlkey is excluded, forcebreak need to set to true
                2. found = false && pagetype = all => urlkey isn't excluded
              */
            activeVoucher = found ? {} : floatingVoucherPayload_[i]
            forceBreak = found
          } else {
            /*
                1. found = true && pagetype = specific => urlkey is the specific url that voucher must be shown
                2. found = false && pagetype = specific => urlkey isn't the specific url
              */
            activeVoucher = found ? floatingVoucherPayload_[i] : {}
          }

          // Break from inside loop if url found, either it's excluded or it's the specific url
          if (found) {
            break
          }
        }
      } else {
        // Means that it's voucher with page_type all with no excluded url
        activeVoucher = floatingVoucherPayload_[i]
      }

      // Check whether voucher already found
      if (!isEmpty(activeVoucher) || forceBreak) {
        break
      }
    }
    setActiveVoucher(activeVoucher)
  }

  function handleSetCookies() {
    // to make sure the floating will gone for 1 day
    Cookies.set(`floating-voucher-${config.companyCode}`, 0, { expires: 1 }) // 1 days
    setIsShowFloatingVoucher(0)
  }
  function handleClipboard() {
    // put fcuntion handle
    setToggleClipboardInfobox(true)
    navigator.clipboard.writeText(activeVoucher.voucher_code)
  }

  function handleSeePromoProduct(url, activeVoucher) {
    mixpanelTrack('Click Promo Button Side Coupon', {
      'Coupon Code': activeVoucher.voucher_code,
    })
    addfloatingVoucher(activeVoucher)
    window.location.href = url
  }

  if (!isEmpty(activeVoucher) && isShowFloatingVoucher === 1) {
    const url = `${activeVoucher?.url}${
      activeVoucher?.url?.indexOf('?') !== -1 ? '&' : '?'
    }itm_source=floating+voucher+${config.companyCode.toLowerCase()}&itm_medium=notification+center&itm_device=${
      config.platform
    }&itm_campaign=${activeVoucher?.name
      ?.replace(/\s+/g, '+')
      .replace('%', '+persen')}`
    return (
      <div
        className={`baso-voucher__container ${isVoucherShow ? 'active' : ''}`}
      >
        <div className='row'>
          <div className='content'>
            <div className='col-xs-12'>
              <p>{activeVoucher?.content}</p>
            </div>
            <div className='col-xs-12'>
              <p
                className='baso-voucher__tnc-trigger'
                onClick={() =>
                  handleChangeLayoutContext(
                    'contentTnCBasoFloatingVoucher',
                    activeVoucher?.syarat_ketentuan,
                  )
                }
              >
                {activeVoucher?.syarat_ketentuan_status && (
                  <u>Syarat dan ketentuan</u>
                )}
              </p>
            </div>
            {activeVoucher?.voucher_code && (
              <div className='col-xs-12 baso-voucher__code'>
                <div className='baso-voucher__code--voucher-code'>
                  <div className='row'>
                    <div className='col-xs-10'>
                      {activeVoucher?.voucher_code}
                    </div>
                    <div className='col'>
                      <div
                        className='row baso-voucher__code--copy'
                        onClick={() => handleClipboard()}
                      >
                        <div className='col-5'>
                          <CopyActive />
                        </div>
                        <div className='col'>Salin</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
            <div className='col-xs-12'>
              <Button
                type='secondary'
                size='large'
                name='baso-floating-voucher'
                width='100% '
                href={url}
                handleOnClick={() => handleSeePromoProduct(url, activeVoucher)}
              >
                Lihat Produk Promo
              </Button>
            </div>
          </div>
          <div
            className='close-btn-container'
            onClick={() => handleSetCookies()}
          >
            <CustomLazyLoadImage
              src={config.assetsURL + 'icon/close-fill.svg'}
              className='close-btn'
              id='close-btn'
              alt='close-btn'
              height={24}
              width={24}
            />
          </div>
          <div
            onClick={() => setIsVoucherShow(!isVoucherShow)}
            className='tag heading-2'
          >
            {activeVoucher?.title.toLowerCase()}
          </div>
        </div>
      </div>
    )
  } else {
    return null
  }
}
