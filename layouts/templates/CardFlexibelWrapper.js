import React from 'react'
import config from '../../../../config'
import { ConvertImageType } from '../../utils'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'

export default function CardFlexWrapper({
  product,
  itmSource,
  itmCampaign,
  itmTerm,
  index,
}) {
  // CHECK HERE!
  return (
    <div className='col-xs-6 margin-bottom-m' key={index}>
      {/* <a href="{{= link + "?itm_source=product-detail-page-recommendation-homepage&itm_campaign=homepage&itm_term="+p.item+"&itm_device=${config.environment}" }}"> */}
      <a
        href={
          product.link +
          '?itm_source=' +
          itmSource +
          '&itm_campaign=' +
          itmCampaign +
          '&itm_term=' +
          itmTerm +
          '&itm_device=' +
          config.environment
        }
      >
        <div className='row product-card justify-content-center'>
          <div className='col-xs-12 content__product '>
            <div className='row middle-xs'>
              <div className='col-xs-12'>
                <CustomLazyLoadImage
                  src={ConvertImageType(
                    product.image.replace(
                      ('https://res.cloudinary.com/ruparupa-com/image/upload/',
                      'https://res.cloudinary.com/ruparupa-com/image/upload/f_auto,q_auto/'),
                    ),
                  )}
                  id={product?.title?.replace('-', '').replace(/ +/g, '-')}
                  alt={product?.title?.replace('-', '').replace(/ +/g, '-')}
                  width='100%'
                  height='100%'
                />
              </div>
              <div className='col-xs-12'>
                <span className='product__name'>{product.title} </span>
              </div>
              <div className='col-xs-12'>
                <div className='row middle-xs price'>
                  {(product?.discount > 0 ||
                    product.msrp !== product.price) && (
                    <>
                      <div className='price__initial'>
                        Rp{product.msrp.toLocaleString(['ban', 'id'])}
                      </div>
                      <div className='price__discount'>
                        <span>{product.discount}%</span>
                      </div>
                    </>
                  )}
                  <br />
                  <div className='col-xs-12 price__real'>
                    Rp{product.price.toLocaleString(['ban', 'id'])}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  )
}
