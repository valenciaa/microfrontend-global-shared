import React, { forwardRef } from 'react'

const ComponentWrapper = forwardRef((props, ref) => {
  return <div ref={ref} style={{ height: props.height, width: '100%' }} />
})

export default ComponentWrapper
