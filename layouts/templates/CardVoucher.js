import startCase from 'lodash/startCase'
import { Fragment } from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import { ConvertImageType } from '../../utils'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'
import Button from '../../../../shared/global/layouts/atoms/Button'
import CardPriceVoucher from '../organism/CardPriceVoucher'
import { micrositeItmGenerator } from '../../utils/micrositeItmGenerator'
import { micrositeMixpanelGenerator } from '../../utils/MicrositeMixpanelGenerator'
import { productClickDatalayer } from '../../utils/DataLayerGoogle'

export default function CardVoucher({
  identifier,
  isFlashSaleStart,
  product,
  type,
  voucher,
  swiper,
  changeInfoBox,
  index,
  additionalClass,
  tabStats,
  tahuStyles,
  micrositeName,
}) {
  const prices = product?.variants[0]?.prices[0]

  const copyClipboard = (voucherCode) => {
    const additionalParams = { voucherCode }
    micrositeMixpanelGenerator(
      'Salin Kode Voucher',
      'copy-voucher',
      null,
      null,
      micrositeName,
      null,
      additionalParams,
      product,
    )
    navigator.clipboard.writeText(voucherCode)
    changeInfoBox(true)
  }

  const handleClick = () => {
    micrositeMixpanelGenerator(
      'Product Container Microsite',
      type,
      product?.name,
      product?.variants?.[0]?.sku,
      micrositeName,
      itm,
      product,
    )
    productClickDatalayer(product, index)
  }

  const removeTags = (str) => {
    if (str === null || str === '') {
      return false
    } else {
      str = str.toString()
    }
    return str.replace(/(<([^>]+)>)/gi, '')
  }

  let usedVoucher = voucher?.limit_used - voucher?.remaining
  if (usedVoucher < voucher?.manipulate_value) {
    usedVoucher = voucher?.manipulate_value
  }
  let progressBarStat = Math.round((usedVoucher / voucher?.limit_used) * 100)
  let statusprogress

  if (progressBarStat) {
    if (progressBarStat <= 49 && progressBarStat >= 0) {
      statusprogress = 'positive'
    } else if (progressBarStat <= 79 && progressBarStat >= 50) {
      statusprogress = 'warning'
    } else if (progressBarStat <= 100 && progressBarStat >= 80) {
      statusprogress = 'negative'
    }
  }

  const isOutStock =
    voucher?.remaining <= 0 ||
    product?.variants[0]?.status === -1 ||
    tabStats !== 10 ||
    progressBarStat === 100

  let companyDefaultColor = '#F26A24'

  if (config.companyCode === 'HCI') {
    companyDefaultColor = '#193E6D'
  } else if (config.companyCode === 'AHI') {
    companyDefaultColor = '#EF3829'
  } else if (config.companyCode === 'TGI') {
    companyDefaultColor = '#FB5A0F'
  }

  const newUrl = micrositeItmGenerator(
    type,
    micrositeName,
    '',
    product.variants[0].sku,
    '',
    true,
  )
  const itm = newUrl?.replace(' ', '')
  let imageUrl = ConvertImageType(
    config.imageURL +
      'w_144,h_144' +
      product?.variants[0]?.images[0]?.image_url,
  )
  if (product?.variants[0]?.images[0]?.full_image_url) {
    imageUrl = product?.variants[0]?.images[0]?.full_image_url
  }
  return (
    <Fragment>
      {prices?.special_price === prices?.price ? (
        <div
          className={`swiper ${
            config.environment === 'desktop' && !swiper && index > 3
              ? 'margin-top-s'
              : ''
          } ${tahuStyles['product-col']} ${tahuStyles?.['voucher-dropdown']} ${
            config.environment === 'desktop' ? `voucher margin-left-m` : ''
          } ${additionalClass ? additionalClass : ''} ${
            isOutStock && tabStats !== 0
              ? tahuStyles?.['outStock']
              : tabStats === 0
                ? `${tahuStyles?.outStockSec}`
                : ''
          } ${swiper ? tahuStyles?.['isSwiper'] : ''}`}
        >
          <div className={tahuStyles['row']}>
            <div className={`${tahuStyles?.['voucher-card-small']}`}>
              <div
                className={`${tahuStyles?.['row']} ${tahuStyles?.['middle-xs']}`}
              >
                <div className='col-xs-12 margin-top-s'>
                  <a
                    onClick={() => handleClick()}
                    href={`${config.baseURL}p/${product.url_key}${itm}`}
                    id={`${identifier}ProductCard`}
                  >
                    <span
                      style={{ color: companyDefaultColor }}
                      className={tahuStyles?.['voucher-small-title']}
                      id='homepagePromoBrandProductName'
                    >
                      {startCase(removeTags(product.description))}
                    </span>
                  </a>
                  <p className={tahuStyles?.['voucher-category']}>
                    {product.name}
                  </p>
                  <label className={tahuStyles?.['voucher-desc']}>
                    {product.variants[0].label_spec || '(Min Pembelian 50rb)'}
                  </label>
                  <div className={tahuStyles?.['wrapper-indicator']}>
                    <div className={tahuStyles?.['progress']}>
                      <div
                        className={`${tahuStyles?.bar} ${tahuStyles[statusprogress]}`}
                        style={{ width: `${progressBarStat}%` }}
                      />
                    </div>
                  </div>
                  <div className={tahuStyles?.['voucher-code-small']}>
                    <label
                      style={{ color: companyDefaultColor }}
                      id={`kdKupon${voucher?.voucher_code}`}
                      className={tahuStyles?.['voucher-text']}
                    >
                      {voucher?.voucher_code}
                    </label>
                    <span
                      className={`${tahuStyles?.['copy-btn']} ${
                        tabStats === 0 ? tahuStyles['disable'] : ''
                      }`}
                      onClick={() => copyClipboard(voucher?.voucher_code)}
                      style={{ color: companyDefaultColor }}
                    >
                      {isOutStock && tabStats !== 0 ? 'Habis' : 'Salin'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div
          className={`col-xs-${config.environment === 'mobile' ? 12 : 4} ${
            tahuStyles['product-col']
          } margin-top-s ${
            isOutStock && tabStats !== 0
              ? tahuStyles['outStock']
              : tabStats === 0
                ? `${tahuStyles?.outStockSec}`
                : ''
          } ${tahuStyles?.['voucher-product-mobile']} ${
            swiper ? tahuStyles?.['voucher-product-mobile-slider'] : ''
          }`}
        >
          <div className='column'>
            <div className={`${tahuStyles?.['voucher-card']}`}>
              <div className={`row middle-xs `}>
                <div
                  className={`col-xs-4 ${
                    config.environment === 'mobile'
                      ? 'center ' + tahuStyles?.['voucher-card-image']
                      : ''
                  }`}
                  style={{
                    height: '175px',
                    width: '144px',
                    borderRight:
                      config.environment === 'desktop'
                        ? ''
                        : '1px solid #C6C7CB',
                  }}
                >
                  <a
                    onClick={() =>
                      micrositeMixpanelGenerator(
                        'Product Container Microsite',
                        type,
                        product?.name,
                        product?.variants?.[0]?.sku,
                        micrositeName,
                        itm,
                      )
                    }
                    href={`${config.baseURL}p/${product.url_key}${itm}`}
                    id={`${identifier}ProductCard`}
                  >
                    <div className='image-card'>
                      <CustomLazyLoadImage
                        src={imageUrl}
                        alt={product.name.replace(/\s+/g, '-').toLowerCase()}
                        id='homepagePromoBrandProductImage'
                        placeholderAspectRatio={1}
                        placeholder={<Skeleton />}
                      />
                    </div>
                  </a>
                </div>
                <div
                  className={`col-xs-${
                    config.environment === 'mobile'
                      ? '7 ' + tahuStyles?.['voucher-card-detail']
                      : 8
                  } padding-top-xs`}
                  style={
                    config.environment === 'desktop'
                      ? { borderRight: '1px solid #C6C7CB', height: 210 }
                      : {}
                  }
                >
                  <a
                    onClick={() => handleClick()}
                    href={`${config.baseURL}p/${product.url_key}${itm}`}
                    id={`${identifier}ProductCard`}
                  >
                    <span
                      className={`${tahuStyles['product__name']} voucher product__name`}
                      id='homepagePromoBrandProductName'
                    >
                      {product.name}
                    </span>
                  </a>
                  <CardPriceVoucher
                    prices={prices}
                    isFlashSaleStart={isFlashSaleStart}
                    tabStats={tabStats}
                    tahuStyles={tahuStyles}
                  />
                  <div className={tahuStyles?.['wrapper-indicator']}>
                    <div className={tahuStyles?.['progress']}>
                      <div
                        className={`${tahuStyles?.bar} ${tahuStyles?.[statusprogress]}`}
                        style={{ width: `${progressBarStat}%` }}
                      />
                    </div>
                  </div>
                  <div className={tahuStyles?.['voucher-code']}>
                    <p
                      id={`kdKupon${voucher?.voucher_code}`}
                      className={tahuStyles?.['voucher-text']}
                    >
                      {/* BD1212D */}
                      {voucher?.voucher_code}
                    </p>
                    <Button
                      additionalClass={`product-and-voucher ${
                        tabStats === 0 ? `${tahuStyles?.outStockSec}` : ''
                      }`}
                      type='primary'
                      size='small'
                      width='80px'
                      handleOnClick={() => copyClipboard(voucher?.voucher_code)}
                    >
                      {isOutStock && tabStats !== 0 ? 'Habis' : 'Salin'}
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  )
}
