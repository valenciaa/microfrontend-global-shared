import clsx from 'clsx'
import dayjs from 'dayjs'
import capitalize from 'lodash/capitalize'
import isEmpty from 'lodash/isEmpty'
import { useRouter } from 'next/router'
import { useState } from 'react'

import config from '../../../../config'
import { useConstantsContext } from '../../context/ConstantsContext'
import { useSeoContext } from '../../context/SeoContext'
import CardSpecialMember from '../../tw-components/pdp/Components/CardSpecialMember'
import {
  CheckCleanUrl,
  ConvertImageType,
  GetParam,
  ItmConverter,
} from '../../utils'
import { productClickDatalayer } from '../../utils/DataLayerGoogle'
import ItmCompanyName from '../../utils/ItmCompanyName'
import { micrositeItmGenerator } from '../../utils/micrositeItmGenerator'
import { micrositeMixpanelGenerator } from '../../utils/MicrositeMixpanelGenerator'
// import CustomLazyLoadImageList from '../atoms/CustomLazyLoadImageList'
import ProductCardRating from '../atoms/ProductCardRating'
import CardPrice from '../organism/CardPrice'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'
import GetB2bType from '../../utils/GetB2bType'
import { ACE_HARDWARE } from '../../utils/constants/AceConstants'

export default function CardWrapper({
  campaign,
  category,
  devices,
  event,
  identifier,
  isFlashSaleStart,
  isRuleBased,
  itmFrom,
  keyword,
  product,
  propsItmSource,
  sid = '',
  type,
  onClick,
  storeCode,
  storeName,
  tahuStyles,
  useMicrositeItm,
  micrositeName,
  mixpanelEvent,
  tab,
  dataType,
  promoName,
  bannerName,
  awsSourcePage,
  from = '',
  isDisabledAnchorLink = false, // ? to disable anchor link to pdp
  additionalComponent,
}) {
  const constantsContext = useConstantsContext()
  const { urlSeo } = useSeoContext()
  const pageType = constantsContext?.pageType
  const router = useRouter()
  const promoSilver =
    (product?.price_koin_silver?.price > 0 &&
      product?.price_koin_silver?.status === 10) ||
    (product?.price_member_silver?.price > 0 &&
      product?.price_member_silver?.status === 10)
  const promoGold =
    (product?.price_koin_gold?.price > 0 &&
      product?.price_koin_gold?.status === 10) ||
    (product?.price_member_gold?.price > 0 &&
      product?.price_member_gold?.status === 10)
  const promoPlatinum =
    (product?.price_koin_platinum?.price > 0 &&
      product?.price_koin_platinum?.status === 10) ||
    (product?.price_member_platinum?.price > 0 &&
      product?.price_member_platinum?.status === 10)

  const [isHovered, setIsHovered] = useState(false)

  const logLimited = router.query.limited === 'true'
  const logCustom = router.query.custom === 'true'
  const prices = product?.variants?.[0]?.prices?.[0]
  const promoMember = product?.variants?.[0]?.price_promo
  const promoPcpAlgolia = promoSilver || promoGold || promoPlatinum
  const reviewRate = product?.review?.ODI // somehow review only passed ODI only
  const isInStock =
    typeof product?.is_in_stock === 'object'
      ? product?.is_in_stock?.[config.companyCode]
      : product?.is_in_stock
  const sort = GetParam('sort')
    ? GetParam('sort').replace('productsvariant_', '')
    : ''
  // migrate to s3 cdn
  let selectedImage = product?.variants?.[0]?.images?.[0]
  let finalImageUrl = ConvertImageType(
    config.imageURL + 'w_200,h_200' + selectedImage.image_url,
  )
  if (selectedImage['full_image_url']) {
    finalImageUrl = selectedImage['full_image_url']
  }
  let productName = product?.name || ''
  // check if first word product name is in alphabet or symbol
  // if symbol, then use directly from product.name because product.name is already capitalized from the response (ex: [free instalasi] air conditioner)
  // if alphabet, then capitalize first (ex: air conditioner)
  if (
    productName.split('')?.[0].toLowerCase() ===
    productName.split('')?.[0].toUpperCase()
  ) {
    productName = product?.name
  } else {
    productName = productName?.split(' ').map(capitalize).join(' ')
  }

  let isFlashSale = product?.checkFlashSale || false
  let textLabel = ''
  if (isInStock === 0) {
    if (
      product?.variants?.[0] &&
      product?.variants?.[0].label === 'New Arrivals'
    ) {
      textLabel = 'Coming Soon'
    } else {
      textLabel = 'Stok Habis'
    }
  }

  const setImageHovered = (isHovered = false) => {
    let images = product?.variants?.[0]?.images || []

    if (images.length > 1) {
      // migrate to s3 cdn
      let selectedImage = images[1]
      let imageHoveredUrl = ConvertImageType(
        config.imageURL + 'w_200,h_200' + selectedImage.image_url,
      )
      if (selectedImage['full_image_url']) {
        imageHoveredUrl = selectedImage['full_image_url']
      }
      return (
        <div
          className={clsx(
            'tw-absolute tw-top-0 tw-left-0',
            'tw-transition-opacity tw-duration-100 tw-ease-out',
            isHovered ? 'tw-opacity-100' : 'tw-opacity-0',
          )}
        >
          <CustomLazyLoadImage
            src={imageHoveredUrl}
            alt={product?.name?.replace(/\s+/g, '-').toLowerCase()}
            id='homepagePromoBrandProductImageHovered'
            width={180}
            height={165}
            cleanUrl={urlSeo ? CheckCleanUrl(urlSeo) : false}
          />
        </div>
      )
    } else {
      return null
    }
  }

  const handleClick = () => {
    if (useMicrositeItm) {
      micrositeClick(getItm(product?.variants?.[0], product?.sku, category))
    } else {
      if (onClick) {
        onClick()
      }
    }
    productClickDatalayer(product)
  }

  const getItm = (variants, sku, categoryName) => {
    let getUrlKey
    if (category) {
      const getCategory = category.split('.html')[0].split('/')
      getUrlKey = getCategory[getCategory.length - 1].split('?')[0]
    }
    let itm = ''
    let itmSource = ''
    let itmCampaign = ''
    let itmReferral = ''
    let itmTermCategoryName = ''
    let companyName = ItmCompanyName()

    if (['flash-sale', 'daily-deals'].indexOf(type) !== -1) {
      // itmSource = `${type}-${pageType}`
      itmSource =
        (companyName ? companyName + '-' : '') + pageType + '-' + type + '+item'
      itmCampaign = type
      // switch (config.companyCode) {
      //   case 'ODI': itmCampaign = pageType; break
      //   case 'HCI': itmCampaign = `${pageType}-informa`; break
      //   case 'AHI': itmCampaign = `${pageType}-ace`; break
      //   case 'TGI': itmCampaign = `${pageType}-toys`; break
      //   default: itmCampaign = pageType; break
      // }
    } else if (type === 'promo-brand') {
      companyName = campaign?.includes('ACE') //ACE_HARDWARE
        ? ACE_HARDWARE
        : campaign?.includes('Informa')
          ? 'informa'
          : campaign?.includes('Toys Kingdom')
            ? 'toys-kingdom'
            : ''
      itmSource =
        (companyName ? companyName + '-' : '') +
        pageType +
        '-toko-official-item-' +
        companyName
      itmCampaign = variants?.sku || ''
    } else if (type === 'promo-major') {
      itmSource =
        (companyName ? companyName + '-' : '') +
        pageType +
        '-promo-major-' +
        ItmConverter(campaign)
      itmCampaign = variants?.sku
    } else if (type === 'product-popular-informa-b2b') {
      itmSource = 'product-popular-informa-b2b-' + ItmConverter(campaign)
      itmCampaign = 'product-detail-popular-informa-b2b-homepage'
    } else if (type === 'last-seen') {
      itmSource =
        (companyName ? companyName + '-' : '') +
        pageType +
        '-product-detail-page-last-seen'
      itmCampaign = variants?.sku || ''
    } else if (type === 'pcp-store') {
      // Can be used in another time
      // if (categoryName) {
      //   const getCategory = categoryName.split('.html')[0].split('/')
      //   itmTermCategoryName = getCategory[getCategory.length - 1]
      // }
      itmSource = `${
        GetParam('itm_campaign') || categoryName
      }-pcp-store-${storeName}`
      itmCampaign = categoryName || storeName
    } else if (
      ['inspiration', 'promo-product', 'promo-voucher'].includes(type)
    ) {
      itmSource = type + '-' + campaign
      itmCampaign = campaign
    } else if (type === 'pdp') {
      if (propsItmSource) {
        propsItmSource = propsItmSource.replace('mini-category-category-', '')
      }
      itmSource = 'megamenu-' + propsItmSource

      if (router?.query?.st) {
        itmSource = propsItmSource
      }

      itmCampaign = campaign
    } else if (keyword) {
      itmSource = 'search'
      if (category) {
        itmCampaign = category + '&keyword=' + keyword + '&query=' + keyword
      } else if (itmFrom) {
        itmCampaign = itmFrom + '&keyword=' + keyword + '&query=' + keyword
      } else {
        itmCampaign = 'search_product&keyword=' + keyword + '&query=' + keyword
      }
    } else if (type === 'wishlist') {
      itmSource = 'wishlist'
      itmCampaign = 'wishlist'
    } else if (isRuleBased === 'true') {
      const paramitmSource = GetParam('itm_source')
      if (paramitmSource) {
        itmSource = paramitmSource
      } else {
        itmSource = 'splashpage'
      }
      itmCampaign = getUrlKey
    } else if (category) {
      const paramitmSource = GetParam('itm_source')
      if (paramitmSource) {
        itmSource = paramitmSource
      } else {
        itmSource = getUrlKey
      }
      itmCampaign = getUrlKey
    } else if (type === 'aceb2b') {
      itmSource = 'homepage-azko-b2b-product-recommendation' //ACE_HARDWARE
    } else if (type === 'ruparupab2b') {
      itmSource = 'homepage-b2b-rr-product-recommendation'
    }

    if (itmSource !== '') {
      if (config.companyCode === 'HCI') {
        itmSource = 'informa-' + itmSource
      } else if (
        config.companyCode === 'AHI' &&
        type !== 'aceb2b' &&
        type !== 'ruparupab2b'
      ) {
        itmSource = 'AZKO-' + itmSource //ACE_HARDWARE
      } else if (config.companyCode === 'TGI') {
        itmSource = 'toys-' + itmSource
      }
    }
    // we detect if the url already have itm, if yes then we used itm from url
    const urlItmCampaign = GetParam('itm_campaign')
    if (event) {
      itmCampaign = pageType
    } else if (!isEmpty(urlItmCampaign)) {
      // we want to override card itm if it comes from PCP
      itmCampaign = urlItmCampaign
    }

    if (type === 'last-seen-pcp') {
      itmSource = 'product-last-seen-pcp'
      itmCampaign = 'search-no-result'
    }

    if (type === 'unmatched-keyword') {
      itmCampaign = 'unmatched-keyword'
    }

    if (
      (companyName === 'toys' || companyName === '') &&
      GetParam('itm_referral') === 'tkid'
    ) {
      itmReferral = '&itm_referral=tkid'
    }

    // If used in any case
    // if (type === 'pdp') {
    //   itmCampaign = product?.url_key
    // }

    if (type === 'productRecoAws') {
      itmSource = `${awsSourcePage}-${companyName || 'rr'}`
      itmCampaign = campaign
    }
    if (itmSource !== '') {
      itm +=
        'itm_source=' +
        itmSource +
        '&itm_campaign=' +
        itmCampaign +
        '&itm_term=' +
        (keyword || itmTermCategoryName
          ? `${keyword || itmTermCategoryName}-&-${variants?.sku || sku}`
          : variants?.sku || sku) +
        '&itm_device=' +
        config.environment
      const query = GetParam('query')
      if (itmCampaign === 'redirect' && query) {
        itm += '&keyword=' + query
      }
      if (itmReferral !== '') {
        itm += itmReferral
      }
    }

    if (useMicrositeItm) {
      let secondaryParam = ''
      if (
        ['product-wishlist-microsite', 'product-last-seen-microsite']?.includes(
          type,
        )
      ) {
        secondaryParam = variants.sku
      }
      const newUrl = micrositeItmGenerator(
        type,
        micrositeName,
        '',
        variants.sku,
        secondaryParam,
        true,
      )
      itm = newUrl?.replace(' ', '')
    }

    if (storeCode) {
      itm += `${itmSource !== '' ? '&' : '?'}store_code=${storeCode}`
    }
    return itm
  }

  let queryParams = ''
  if (
    getItm(
      product?.variants?.[0],
      product.sku,
      product?.[`bread_crumb3_${config.companyCode.toLowerCase()}`]?.url,
    )
  ) {
    if (queryParams === '') {
      queryParams +=
        '?' +
        getItm(
          product?.variants?.[0],
          product.sku,
          product?.[`bread_crumb3_${config.companyCode.toLowerCase()}`]?.url,
        )
    } else {
      queryParams += getItm(
        product?.variants?.[0],
        product.sku,
        product?.[`bread_crumb3_${config.companyCode.toLowerCase()}`]?.url,
      )
    }
  }

  if (product?.__queryID) {
    if (queryParams === '') {
      queryParams += '?queryId=' + product?.__queryID
    } else {
      queryParams += '&queryId=' + product?.__queryID
    }
  }

  if (sort) {
    if (queryParams === '') {
      queryParams += '?sort=' + sort
    } else {
      queryParams += '&sort=' + sort
    }
  }

  if (sid && sid !== '') {
    if (queryParams === '') {
      queryParams += '?sid=' + sid
    } else {
      queryParams += '&sid=' + sid
    }
  }

  const urlProduct = !isDisabledAnchorLink
    ? `${config.baseURL}p/${product.url_key}${queryParams}`
    : null

  const micrositeClick = (itm) => {
    let additionalParams
    if (
      [
        'product-last-seen-microsite',
        'product-wishlist-microsite',
        'promo-brand',
      ].includes(type)
    ) {
      additionalParams = {
        tab,
        dataType,
      }
    }
    if (type === 'promo-brand') {
      additionalParams = {
        ...additionalParams,
        storeName: storeName,
        bannerName: bannerName,
      }
    }

    if (['flash-sale', 'daily-deals'].includes(type)) {
      additionalParams = {
        promoName,
      }
    }

    micrositeMixpanelGenerator(
      mixpanelEvent,
      type,
      product?.name,
      product?.variants?.[0]?.sku,
      micrositeName,
      itm,
      additionalParams,
    )
  }

  // for algolia
  if (!isFlashSale) {
    const dateTimeNow = dayjs()
    if (!isEmpty(product.events)) {
      product?.events?.map((event) => {
        if (
          event.title === 'Flash Sale' &&
          dateTimeNow > dayjs(event.start_date) &&
          dateTimeNow < dayjs(event.end_date)
        ) {
          return (isFlashSale = true)
        }
      })
    }
  }

  const labelLog = () => {
    let backgroundLogColor = 'yellow'

    if (
      logLimited &&
      (product?.is_limited_stock || product?.variants?.[0]?.is_limited_stock)
    ) {
      backgroundLogColor = 'red'
    }

    return (
      <div
        className='col-xs-12 label-log'
        style={{ backgroundColor: backgroundLogColor }}
      >
        SKU : <b>{product?.variants?.[0]?.sku || product?.sku || ''}</b>
      </div>
    )
  }

  return (
    <div
      className='row product-card'
      style={
        from === 'tahu' && config?.environment === 'desktop'
          ? { maxWidth: '10em' }
          : {}
      }
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <div
        className={`tw-flex tw-flex-col tw-justify-between content__product ${
          type === 'productRecoAws'
            ? 'padding-left-none padding-right-none'
            : ''
        }`}
      >
        <div
          className={
            'tw-cursor-pointer container-card__product container-card__product--' +
            devices
          }
          id={`${identifier}Carousel`}
        >
          {logLimited || logCustom ? labelLog() : null}
          <a
            onClick={() => handleClick()}
            href={urlProduct}
            id={`${identifier}ProductCard`}
          >
            <div className='row middle-xs'>
              <div className='image col-xs-12'>
                {textLabel !== '' && (
                  <div className='out-of-stock-ribbon'>{textLabel}</div>
                )}
                {isFlashSale && GetB2bType() !== 'informa_b2b' && (
                  <img
                    className='flash-sale-ribbon'
                    src={config.assetsURL + 'images/ribbon-flash-sale.svg'}
                  />
                )}
                {!isFlashSale && product?.tahuRibbonUrl && (
                  <img
                    className='tahu-ribbon'
                    src={product?.tahuRibbonUrl}
                    alt='Ribbon Banner'
                  />
                )}
                <div className='tw-relative'>
                  <CustomLazyLoadImage
                    src={finalImageUrl}
                    alt={product?.name?.replace(/\s+/g, '-').toLowerCase()}
                    id='homepagePromoBrandProductImage'
                    width={180}
                    height={165}
                    cleanUrl={urlSeo ? CheckCleanUrl(urlSeo) : false}
                  />
                  {setImageHovered(isHovered)}
                </div>
              </div>
              <div className='col-xs-12 product__grouping margin-top-xs'>
                {product?.product_label ||
                  product?.variants?.[0]?.product_group?.label}
              </div>
              <div className='col-xs-12 margin-top-xs'>
                <span
                  className='product__name'
                  id='homepagePromoBrandProductName'
                >
                  {productName}
                </span>
              </div>
              <div className='col-xs-12'>
                <CardPrice
                  prices={prices}
                  isFlashSale={isFlashSale}
                  isFlashSaleStart={isFlashSaleStart}
                />
              </div>
              {(!isEmpty(promoMember) || !!promoPcpAlgolia) && (
                <div className='col-xs-12'>
                  <CardSpecialMember promo={promoMember} product={product} />
                </div>
              )}
              {
                <div
                  className={clsx(
                    `col-xs-12 label ${
                      product?.variants?.[0]?.label !== ''
                        ? 'margin-bottom-xs'
                        : ''
                    }`,
                    `${
                      config.environment === 'mobile' ? '!tw-px-0' : '!tw-px-4'
                    }`,
                  )}
                >
                  {product?.variants?.[0]?.label && (
                    <div
                      className={clsx(
                        'tw-flex',
                        tahuStyles ? tahuStyles['label-text'] : 'label-text',
                      )}
                    >
                      <img
                        src={`${config.assetsURL}icon/info-red.svg`}
                        alt='Info Icon'
                        height={14}
                        width={14}
                        style={{ top: 0 }}
                      />
                      <span className='tw-line-clamp-1 tw-text-xs tw-text-red-50'>
                        {product?.variants?.[0]?.label}
                      </span>
                    </div>
                  )}
                </div>
              }
              {reviewRate && reviewRate.total !== 0 ? (
                <ProductCardRating reviewRating={reviewRate} />
              ) : null}
            </div>
          </a>
        </div>
        {additionalComponent ? additionalComponent : null}
      </div>
    </div>
  )
}
