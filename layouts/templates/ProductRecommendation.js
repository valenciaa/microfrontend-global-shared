import React from 'react'
import clsx from 'clsx'
import config from '../../../../config'
import ProductSlider from './ProductSlider'

export default function ProductRecomendation({
  className = '',
  spaceClassName = 'margin-top-s',
  products,
  sectionTitle,
  sectionType,
  ...props
}) {
  return (
    <section
      className={clsx(className, 'product-slider__' + config.environment)}
    >
      <div className='flex'>
        <p className='color-grey-100 heading-2'>{sectionTitle}</p>
      </div>
      <div className={spaceClassName}>
        <ProductSlider
          products={products}
          sectionTitle={sectionTitle}
          sectionType={sectionType}
          {...props}
        />
      </div>
    </section>
  )
}
