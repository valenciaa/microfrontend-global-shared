import React, { useEffect, useState } from 'react'
import isEmpty from 'lodash/isEmpty'
import CardWrapper from '../../layouts/templates/CardWrapper'
import Button from '../atoms/Button'
import CustomLazyLoadImage from '../atoms/CustomLazyLoadImage'
import config from '../../../../config'
import {
  generateTrackingPayload,
  mixpanelTrack,
} from '../../utils/MixpanelWrapper'
import { useConstantsContext } from '../../context/ConstantsContext'
import ArrowSlider from '../../utils/ArrowSlider'
import { checkIsLastSeenValid } from '../../utils/UpdateProductLastSeen'
import { getLastSeenProduct } from '../../utils/GetLastSeenProduct'

export default function LastSeenProduct({ useSlider, tab, micrositeName }) {
  const constantsContext = useConstantsContext()
  const [lastSeenProducts, setLastSeenProducts] = useState([])
  const [fetchMoreProduct, setFetchMoreProduct] = useState(false)
  useEffect(() => {
    const setup = async () => {
      let lastSeen = await getLastSeenProduct()
      lastSeen = await checkIsLastSeenValid(lastSeen)
      setLastSeenProducts(lastSeen)
    }
    setup()
  }, [])

  const showMore = () => {
    setFetchMoreProduct(true)
  }
  let itmType = 'last-seen-pcp'
  if (constantsContext?.pageFrom === 'home') {
    itmType = 'last-seen'
  }
  if (micrositeName) {
    itmType = 'product-last-seen-microsite'
  }

  const renderCardWrapper = (product, index) => {
    return (
      <CardWrapper
        onClick={() => {
          mixpanelTrack(
            'Rekomendasi Produk',
            generateTrackingPayload({
              'Click Location': 'Product',
              'Section Name': 'Last Seen',
              'Item Name': product?.name || 'None',
              'Item ID': product?.variants?.[0]?.sku || 'None',
              'Location ': useSlider ? 'Microsite' : 'Home Page',
              'Microsite Name': useSlider ? micrositeName : 'None',
              'Tab ': tab || 'None',
            }),
          )
          mixpanelTrack(
            'Click PDP',
            generateTrackingPayload({
              'Location': 'Homepage',
              'Section': 'Rekomendasi Spesial Untukmu',
              'Sub Section': 'Terakhir Dilihat',
              'Position': index + 1 || 'None',
              'Item ID': product?.variants?.[0]?.sku || 'None',
              'Item Name': product?.name || 'None',
            }),
          )
        }}
        index={index}
        product={product}
        devices={constantsContext?.devices}
        identifier='lastSeen'
        type={itmType}
        useMicrositeItm={micrositeName}
        micrositeName={micrositeName}
        mixpanelEvent='Rekomendasi Produk'
        tab={tab}
        dataType='Last Seen'
      />
    )
  }

  function handleRenderProduct() {
    if (lastSeenProducts?.length > 0) {
      if (config.environment === 'mobile') {
        return (
          <div className={`row ${useSlider ? 'overflow-scroll' : ''}`}>
            {lastSeenProducts?.slice(0, 6).map((product, index) => {
              if (
                product?.company_code[`${config.companyCode}`] === parseInt(10)
              ) {
                if (!isEmpty(product)) {
                  return (
                    <div
                      className='col-xs-6 margin-bottom-m'
                      key={product?.variants?.[0]?.sku}
                    >
                      {renderCardWrapper(product, index)}
                    </div>
                  )
                }
              } else {
                return null
              }
            })}

            {fetchMoreProduct &&
              lastSeenProducts?.slice(6).map((product, index) => {
                if (
                  product?.company_code[`${config.companyCode}`] ===
                  parseInt(10)
                ) {
                  return (
                    <div
                      className='col-xs-6 margin-bottom-m'
                      key={product.variants[0].sku}
                    >
                      <CardWrapper
                        index={index}
                        onClick={() =>
                          mixpanelTrack(
                            'Rekomendasi Produk',
                            generateTrackingPayload({
                              'Click Location': 'Product',
                              'Section Name': 'Last Seen',
                              'Item Name': product?.name || 'None',
                              'Item ID': product?.variants?.[0]?.sku || 'None',
                            }),
                          )
                        }
                        product={product}
                        devices={constantsContext?.devices}
                        identifier='lastSeen'
                        type={itmType}
                        useMicrositeItm={micrositeName}
                        micrositeName={micrositeName}
                      />
                    </div>
                  )
                } else {
                  return null
                }
              })}
          </div>
        )
      } else if (config.environment === 'desktop') {
        return (
          <div
            className={`row ${useSlider ? 'overflow-scroll' : ''}`}
            id='product-lastSeen'
          >
            <ArrowSlider
              id='product-lastSeen'
              dataLength={lastSeenProducts?.length}
              micrositeName={micrositeName}
            >
              {lastSeenProducts?.map((product, index) => {
                if (
                  product?.company_code?.[`${config.companyCode}`] ===
                  parseInt(10)
                ) {
                  return (
                    <div className='col-xs-2' key={product?.variants?.[0]?.sku}>
                      {renderCardWrapper(product, index)}
                    </div>
                  )
                }
              })}
            </ArrowSlider>
          </div>
        )
      } else {
        return null
      }
    } else {
      return (
        <div className='row empty-product-container'>
          <div className='col-xs-12'>
            <CustomLazyLoadImage
              src={config.assetsURL + 'images/image-empty-state-desktop.png'}
              width='100%'
              height={config.environment === 'mobile' ? '100%' : '408px'}
              style={{ objectFit: 'contain' }}
            />
          </div>
          <div className='col-xs-12'>
            <h4 className={`__${config.environment} hero-text`}>
              Maaf, kamu belum memilih barang
            </h4>
            <p className={`__${config.environment} ui-text-1`}>
              Yuk cari barang yang kamu butuhkan sekarang
            </p>
          </div>
        </div>
      )
    }
  }

  return (
    <section>
      <div className='container-card'>
        <div className='margin-top-s'>
          <div className='container-emarsys__product'>
            {handleRenderProduct()}
            {config.environment === 'mobile' &&
              !useSlider &&
              lastSeenProducts?.length > 6 &&
              !fetchMoreProduct && (
                <Button
                  size='big'
                  type='primary-border'
                  width='100%'
                  handleOnClick={() => showMore()}
                >
                  Muat Lebih Banyak Produk
                </Button>
              )}
          </div>
        </div>
      </div>
    </section>
  )
}
