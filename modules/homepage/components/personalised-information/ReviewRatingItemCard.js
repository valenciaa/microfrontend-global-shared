import clsx from 'clsx'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'
import CustomLazyLoadImage from '../../../../layouts/atoms/CustomLazyLoadImage'
import TWButton from '../../../../tw-components/buttons/TWButton'
import { mixpanelTrack } from '../../../../utils/MixpanelWrapper'
import ReviewRatingSingleProductModal from '../../../reviewrating/ReviewRatingSingleProductModal'
import ReviewServiceModal from '../../../reviewrating/ReviewServiceModal'

export default function ReviewRatingItemCard({
  product,
  rating,
  setRating,
  showModal,
  setShowModal,
  mixpanelPersonalised,
}) {
  const router = useRouter()
  const isDesktop = config.environment === 'desktop'
  const imageSize = isDesktop ? 36 : 32

  const {
    count_product,
    image_url,
    invoice_no,
    product_name,
    receipt_id,
    is_product_reviewed,
    is_service_reviewed,
    store_code,
    shopping_type,
  } = product

  const [ratingCard, setRatingCard] = useState(0)
  const [isOpenSingleProductReviewModal, setIsOpenSingleProductReviewModal] =
    useState(false)
  const [isOpenReviewServiceModal, setIsOpenReviewServiceModal] =
    useState(false)
  const isOnlineOrder = invoice_no.length > 0
  const orderId = isOnlineOrder ? invoice_no : receipt_id

  const isNeedReviewProduct = !![0, 5].includes(is_product_reviewed)
  const needReviewProductButton =
    is_product_reviewed === 5 ? 'Perbarui Ulasan Produk' : 'Beri Ulasan Produk'

  const payloadMixpanel = {
    'Star Rating': ratingCard,
    'Click Location': 'Review Rating',
  }

  useEffect(() => {
    if (ratingCard !== 0 && rating === 0) {
      setShowModal(true)

      setRating(ratingCard)

      mixpanelPersonalised(payloadMixpanel)
    }
  }, [ratingCard])

  useEffect(() => {
    if (!showModal) {
      setRatingCard(0)
    }
  }, [showModal])

  const handleClickReviewProduct = () => {
    if (is_product_reviewed === 5) {
      mixpanelTrack('Click Button Perbarui Ulasan', { Location: 'Homepage' })
    } else {
      mixpanelTrack('Click Button Beri Ulasan Produk', { Location: 'Homepage' })
    }

    if (count_product) {
      if (isDesktop) {
        router.push({
          pathname: 'my-account',
          query: {
            tab: 'review-rating',
            child_tab: 'review',
            id: orderId,
            product_reviewed: is_product_reviewed,
          },
        })

        return
      }

      router.push({
        pathname: '/my-account/review-rating/review/products/' + orderId,
        query: { product_reviewed: is_product_reviewed },
      })

      return
    }

    setIsOpenSingleProductReviewModal(true)
  }

  const handleClickReviewService = () => {
    mixpanelTrack('Click Button Beri Ulasan Pelayanan', {
      'Location': 'Homepage',
      'Store Code': store_code,
      'Store': shopping_type === 'online' ? 'Online' : 'Offline',
    })

    if (isDesktop) {
      setIsOpenReviewServiceModal(true)

      return
    }

    router.push({
      pathname: `/my-account/review-rating/review/service/` + orderId,
      query: { product_reviewed: is_service_reviewed },
    })
  }

  return (
    <>
      <div className='tw-flex tw-flex-col tw-gap-1.5'>
        <div className='tw-flex tw-gap-1 tw-max-h-9'>
          <div
            className={clsx(
              'tw-flex tw-flex-shrink-0',
              isDesktop ? 'tw-w-9 tw-h-9' : 'tw-w-8 tw-h-8',
            )}
          >
            <CustomLazyLoadImage
              alt={`${product_name}-image`}
              height={imageSize}
              placeholder={<Skeleton height={imageSize} width={imageSize} />}
              src={image_url}
              width={imageSize}
            />
          </div>

          <div className='tw-flex tw-flex-col tw-grow'>
            <p
              className={clsx(
                'tw-font-bold tw-line-clamp-1',
                isDesktop ? 'tw-text-sm' : 'tw-text-xs',
              )}
            >
              {product_name}
            </p>

            {count_product > 0 && (
              <p
                className={
                  (clsx('tw-text-grey-40'),
                  isDesktop ? 'tw-text-xs' : 'tw-text-xxs')
                }
              >
                + {count_product} produk lainnya
              </p>
            )}
          </div>
        </div>

        <div className='tw-flex tw-gap-2'>
          {is_service_reviewed === 0 && (
            <TWButton
              additionalClass='tw-rounded-md tw-flex-grow'
              customPadding='tw-px-1 tw-py-2'
              handleOnClick={handleClickReviewService}
              type='primary'
            >
              <p className='button-small-text'>Beri Ulasan Pelayanan</p>
            </TWButton>
          )}

          {isNeedReviewProduct && (
            <TWButton
              additionalClass='tw-rounded-md tw-flex-grow'
              customPadding='tw-px-1 tw-py-2'
              handleOnClick={handleClickReviewProduct}
              type={is_product_reviewed === 0 ? 'primary' : 'primary-border'}
            >
              <p className='button-small-text tw-line-clamp-1'>
                {needReviewProductButton}
              </p>
            </TWButton>
          )}
        </div>
      </div>

      {isOpenReviewServiceModal && (
        <ReviewServiceModal
          isOnline={isOnlineOrder}
          show={isOpenReviewServiceModal}
          transactionNumber={invoice_no}
          transactionOfflineNumber={receipt_id}
          onClose={() => setIsOpenReviewServiceModal(false)}
        />
      )}

      {isOpenSingleProductReviewModal && (
        <ReviewRatingSingleProductModal
          isDesktop={isDesktop}
          isOnline={isOnlineOrder}
          isReviewed={is_product_reviewed}
          show={isOpenSingleProductReviewModal}
          transactionNumber={invoice_no}
          transactionOfflineNumber={receipt_id}
          onClose={() => setIsOpenSingleProductReviewModal(false)}
        />
      )}
    </>
  )
}
