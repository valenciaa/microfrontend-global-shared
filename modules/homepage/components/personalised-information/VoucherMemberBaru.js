import React, { useState } from 'react'
import config from '../../../../../../config'
import Button from '../../../../layouts/atoms/Button'
import CustomLazyLoadImage from '../../../../layouts/atoms/CustomLazyLoadImage'
import Modal from '../../../../layouts/templates/Modal'
import HtmlModal from '../../../../layouts/templates/HtmlModal'
import ClockIcon from '../../../../../../public/static/icon/icon-clock-white.svg'
import PersonalisedTimer from '../PersonalisedTimer'

export default function VoucherMemberBaru({
  val,
  currentIndex,
  keys,
  removable,
  handleRemoveCard,
  handleCopyClipboard,
  setToggleClipboard,
  mixpanelPersonalised,
}) {
  const payloadMixpanel = {
    'Voucher Name': val?.[currentIndex]?.voucher_code_title,
    'Voucher Code': val[currentIndex]?.voucher_code,
    'Klik TNC': false,
    'Salin Voucher': false,
    'Click Location': 'Voucher Registrasi',
  }
  const htmlModalBody = (functionInsideModal) => (
    <HtmlModal
      onClose={functionInsideModal}
      content={val[currentIndex]?.snk}
      title='Syarat dan Ketentuan Voucher'
    />
  )

  const [onClickedTermsAndConditionModal, setOnClickedTermsAndConditionModal] =
    useState(false)
  if (currentIndex !== -1 && currentIndex !== null) {
    return (
      <>
        <div className='personalised-card__voucher-copy voucher-Copy cursor-pointer'>
          <div className='voucher-Copy-infobox items-center'>
            <div
              className='voucher-Copy-infobox-header'
              onClick={() => {
                setOnClickedTermsAndConditionModal(true)
                mixpanelPersonalised({ ...payloadMixpanel, 'Klik TNC': true })
              }}
            >
              <div className='voucher-Copy-infobox-header-content'>
                <div
                  className={`voucher-Copy-infobox-header-content__left margin-top-xs ${
                    config.environment === 'desktop' ? 'col-xs-2' : ''
                  }`}
                >
                  {
                    <CustomLazyLoadImage
                      src={config.assetsURL + config.companyName + '-logo.svg'}
                      width={config.environment === 'desktop' ? 30 : 32}
                      height={config.environment === 'desktop' ? 30 : 32}
                    />
                  }
                </div>

                <div className='voucher-Copy-infobox-header-content__right'>
                  <div className='voucher-Copy-infobox-header-content__right-promoBU'>
                    {val?.[currentIndex]?.voucher_code_title}
                  </div>

                  <div className='voucher-Copy-infobox-header-content__right-promoText'>
                    {val?.[currentIndex]?.benefit}
                  </div>
                </div>
              </div>
              <div className='voucher-Copy-infobox-header-additional'>
                <div
                  className={`col voucher-Copy-infobox-header-additional__text ${
                    config.environment === 'desktop' ? 'padding-left-xs' : ''
                  }`}
                >
                  {val?.[currentIndex]?.promo_description}
                </div>
              </div>
            </div>
            <div className='voucher-Copy-infobox-body flex justify-center'>
              <div
                onClick={() => {
                  handleCopyClipboard(
                    val[currentIndex].voucher_code,
                    setToggleClipboard,
                  )
                  mixpanelPersonalised({
                    ...payloadMixpanel,
                    'Salin Voucher': true,
                  })
                }}
                className={`voucher-Copy-infobox-body-button ${
                  config.environment === 'desktop' ? 'flex justify-center' : ''
                }`}
              >
                <Button type='primary' size='medium'>
                  Salin
                </Button>
              </div>
              <div
                className={`voucher-Copy-infobox-body-snk ui-text-3 ${
                  config.environment === 'desktop' ? 'cursor-pointer' : ''
                }`}
              >
                <span className='icon-clock-white'>
                  <ClockIcon className='color-grey-50' />
                </span>
                <PersonalisedTimer
                  deadline={val?.[currentIndex]?.expired_date}
                  interval={60000}
                />
              </div>
            </div>
          </div>
          {onClickedTermsAndConditionModal && (
            <Modal
              bodyClass='modal-body-html'
              bodyElement={htmlModalBody}
              onClose={() => setOnClickedTermsAndConditionModal(false)}
              show={onClickedTermsAndConditionModal}
            />
          )}
        </div>
        {removable === 10 && (
          <div className='close-icon' onClick={() => handleRemoveCard(keys)}>
            <CustomLazyLoadImage
              src={
                config.assetsURL +
                `icon/close${
                  config.environment === 'desktop' ? '-white' : ''
                }.svg`
              }
              id='close-btn'
              alt='close-btn'
              height={30}
              width={30}
            />
          </div>
        )}
      </>
    )
  } else {
    return null
  }
}
