import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'
import CustomLazyLoadImage from '../../../../layouts/atoms/CustomLazyLoadImage'
import CardPrice from '../../../../layouts/organism/CardPrice'
import { ConvertImageType } from '../../../../utils'
import startCase from 'lodash/startCase'
import isEmpty from 'lodash/isEmpty'
import toLower from 'lodash/toLower'
import { WISHLIST_MOBILE_URL } from '../../../../utils/constants'

export default function WishlistAndCart({
  val,
  currentIndex,
  keys,
  handleRemoveCard,
  removable,
  redirectCard,
  mixpanelPersonalised,
}) {
  const redirectedWishCart = (type) => {
    if (type === 'wishlist') {
      if (config.environment === 'mobile') {
        return WISHLIST_MOBILE_URL()
      } else {
        return 'my-account?tab=my-wishlist'
      }
    }
    return 'cart'
  }

  const payloadMixpanel = {
    'Item ID': val[currentIndex]?.sku || 'none',
    'Item Name': val[currentIndex]?.name || 'none',
    'Click Location': 'Wishlist & Cart',
    'ITM Source': 'personalized-info-drop-price-homepage',
    'ITM Campaign':
      `${val[currentIndex]?.sku}-${val[currentIndex]?.name}` || 'none',
    'ITM Device': config.environment,
  }
  let imageUrl = ConvertImageType(
    config.imageURL + 'w_72,h_72' + val[currentIndex]?.image_url,
  )

  if (val[currentIndex]?.full_image_url) {
    imageUrl = val[currentIndex]?.full_image_url
  }
  return (
    <>
      <div
        onClick={() => {
          redirectCard(
            config.legacyBaseURL +
              `${redirectedWishCart(val[currentIndex]?.type)}`,
          )
          mixpanelPersonalised(payloadMixpanel)
        }}
        className={`personalised-card__wishlist-cart ${
          config.environment === 'desktop' ? 'cursor-pointer' : ''
        }`}
      >
        <div className='col-xs-4 flex justify-center'>
          <CustomLazyLoadImage
            src={imageUrl}
            alt={val[currentIndex]?.name.replace(/\s+/g, '-').toLowerCase()}
            height='72'
            width='72'
            placeholderAspectRatio={1}
            placeholder={<Skeleton />}
          />
        </div>
        <div className='card-content col-xs-8'>
          <div className='card-content__title-card heading-3'>
            Diskon Untukmu!
          </div>
          <div className='card-content__product-title ui-text-4'>
            {startCase(toLower(val[currentIndex]?.name))}
          </div>
          <div className='card-content__product-price'>
            {!isEmpty(val[currentIndex]?.price) && (
              <CardPrice prices={val[currentIndex]?.price} />
            )}
          </div>
        </div>
      </div>
      {removable === 10 && (
        <div
          className='close-icon cursor-pointer'
          onClick={() => handleRemoveCard(keys)}
        >
          <CustomLazyLoadImage
            src={
              config.assetsURL +
              `icon/close${
                config.environment === 'desktop' ? '-white' : ''
              }.svg`
            }
            id='close-btn'
            alt='close-btn'
            height={30}
            width={30}
          />
        </div>
      )}
    </>
  )
}
