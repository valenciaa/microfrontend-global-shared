import dayjs from 'dayjs'
import React from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'
import CustomLazyLoadImage from '../../../../layouts/atoms/CustomLazyLoadImage'
import { ConvertImageType } from '../../../../utils'

export default function StatusOrder({
  val,
  currentIndex,
  keys,
  removable,
  handleRemoveCard,
  redirectCard,
  emailUser,
  mixpanelPersonalised,
}) {
  const payloadMixpanel = {
    'Click Location': 'Status Pengiriman',
  }
  let imageUrl = ConvertImageType(
    config.imageURL + 'w_72,h_72' + val[currentIndex]?.image_url,
  )

  if (val[currentIndex]?.full_image_url) {
    imageUrl = val[currentIndex]?.full_image_url
  }

  return (
    <>
      <div
        className='personalised-card__status-order cursor-pointer'
        onClick={() => {
          redirectCard(
            config.legacyBaseURL +
              `my-account?tab=my-order-detail&orderId=${val[currentIndex].order_no}&email=${emailUser}`,
          )
          mixpanelPersonalised(payloadMixpanel)
        }}
      >
        <CustomLazyLoadImage
          src={imageUrl}
          alt=''
          height='72'
          width='72'
          placeholderAspectRatio={1}
          placeholder={<Skeleton />}
        />
        <div className='card-content margin-left-xs'>
          <div className='card-content__status ui-text-3'>
            {val?.[currentIndex]?.to_render_status}
          </div>
          <div className='card-content__invoice-id heading-3 margin-left-xxs margin-top-s'>
            {val?.[currentIndex]?.order_no}
          </div>
          <div className='card-content__estimated-time ui-text-3 margin-left-xxs'>
            {dayjs(val?.[currentIndex]?.created_at)
              .locale('id')
              .format('DD MMMM YYYY hh:mm')}
          </div>
        </div>
      </div>
      {removable === 10 && (
        <div className='close-icon' onClick={() => handleRemoveCard(keys)}>
          <CustomLazyLoadImage
            src={
              config.assetsURL +
              `icon/close${
                config.environment === 'desktop' ? '-white' : ''
              }.svg`
            }
            id='close-btn'
            alt='close-btn'
            height={30}
            width={30}
          />
        </div>
      )}
    </>
  )
}
