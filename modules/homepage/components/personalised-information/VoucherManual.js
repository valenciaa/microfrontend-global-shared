import React, { useState } from 'react'
import config from '../../../../../../config'
import Button from '../../../../layouts/atoms/Button'
import CustomLazyLoadImage from '../../../../layouts/atoms/CustomLazyLoadImage'
import PersonalisedTimer from '../PersonalisedTimer'
import ClockIcon from '../../../../../../public/static/icon/icon-clock-white.svg'
import isEmpty from 'lodash/isEmpty'
import Modal from '../../../../layouts/templates/Modal'
import HtmlModal from '../../../../layouts/templates/HtmlModal'

export default function VoucherManual({
  val,
  currentIndex,
  removable,
  keys,
  getCompany,
  handleRemoveCard,
  handleCopyClipboard,
  setToggleClipboard,
  mixpanelPersonalised,
}) {
  const [onClickedTermsAndConditionModal, setOnClickedTermsAndConditionModal] =
    useState(false)
  const payloadMixpanel = {
    'Voucher Name': val[currentIndex]?.title || 'none',
    'Voucher Code': val[currentIndex]?.voucher_code || 'none',
    'Klik TNC': false,
    'Salin Voucher': false,
    'Click Location': 'Voucher Promo',
  }
  const htmlModalBody = (functionInsideModal) => (
    <HtmlModal
      onClose={functionInsideModal}
      content={val[currentIndex]?.terms_and_conditions}
      title='Syarat dan Ketentuan Voucher'
    />
  )
  const imageFile = (company) => {
    if (val?.[currentIndex]?.company_image) {
      return val?.[currentIndex]?.company_image
    } else {
      if (config.environment === 'desktop' && company === 'toyskingdomonline') {
        return 'https://cdn.ruparupa.io/filters:format(webp)/promotion/ruparupa/production-3-0/desktop-3-0/official-partner/tgi.png'
      } else {
        return config.assetsURL + 'images/' + company + '-online-logo.svg'
      }
    }
  }
  if (!isEmpty(val)) {
    return (
      <div className='personalised-card__voucher-copy voucher-Copy'>
        <div className='voucher-Copy-infobox items-center'>
          <div
            className='voucher-Copy-infobox-header cursor-pointer'
            onClick={() => {
              setOnClickedTermsAndConditionModal(true)
              mixpanelPersonalised({ ...payloadMixpanel, 'Klik TNC': true })
            }}
          >
            <div className='voucher-Copy-infobox-header-content'>
              <div
                className={`voucher-Copy-infobox-header-content__left margin-top-xs ${
                  config.environment === 'desktop'
                    ? 'col-xs-2 margin-top-xs '
                    : ''
                }`}
              >
                <CustomLazyLoadImage
                  src={imageFile(getCompany(val?.[currentIndex]?.company_code))}
                  alt='Brand Icon'
                  height={config.environment === 'desktop' ? 30 : 32}
                  width={config.environment === 'desktop' ? 30 : 32}
                />
              </div>

              <div className='voucher-Copy-infobox-header-content__right'>
                <div
                  className={`voucher-Copy-infobox-header-content__right-promoBU ${
                    config.environment === 'desktop' ? 'ui-text-5' : 'ui-text-4'
                  }`}
                >
                  {val?.[currentIndex]?.title}
                </div>

                <div className='voucher-Copy-infobox-header-content__right-promoText'>
                  {val?.[currentIndex]?.benefit}
                </div>
              </div>
            </div>
            <div className='voucher-Copy-infobox-header-additional'>
              <div
                className={`col voucher-Copy-infobox-header-additional__text ${
                  config.environment === 'desktop' ? 'padding-left-xs' : ''
                } `}
              >
                {val?.[currentIndex]?.description}
              </div>
            </div>
          </div>
          <div className='voucher-Copy-infobox-body flex justify-center'>
            <div
              onClick={() => {
                handleCopyClipboard(
                  val[currentIndex].voucher_code,
                  setToggleClipboard,
                )
                mixpanelPersonalised({
                  ...payloadMixpanel,
                  'Salin Voucher': true,
                })
              }}
              className={`voucher-Copy-infobox-body-button ${
                config.environment === 'desktop' ? 'flex justify-center' : ''
              }`}
            >
              <Button type='primary' size='medium'>
                Salin
              </Button>
            </div>
            <div
              className={`voucher-Copy-infobox-body-snk ui-text-3 ${
                config.environment === 'desktop' ? 'cursor-pointer' : ''
              }`}
            >
              <span className='icon-clock-white'>
                <ClockIcon className='color-grey-50' />
              </span>
              <PersonalisedTimer
                deadline={val?.[currentIndex]?.expiration_date}
                interval={60000}
              />
            </div>
          </div>
        </div>
        {removable === 10 && (
          <div className='close-icon' onClick={() => handleRemoveCard(keys)}>
            <CustomLazyLoadImage
              src={
                config.assetsURL +
                `icon/close${
                  config.environment === 'desktop' ? '-white' : ''
                }.svg`
              }
              id='close-btn'
              alt='close-btn'
              height={30}
              width={30}
            />
          </div>
        )}
        {onClickedTermsAndConditionModal && (
          <Modal
            bodyClass='modal-body-html'
            bodyElement={htmlModalBody}
            onClose={() => setOnClickedTermsAndConditionModal(false)}
            show={onClickedTermsAndConditionModal}
          />
        )}
      </div>
    )
  } else {
    return null
  }
}
