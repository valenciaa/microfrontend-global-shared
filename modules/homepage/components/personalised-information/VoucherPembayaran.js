import React from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'
import CustomLazyLoadImage from '../../../../layouts/atoms/CustomLazyLoadImage'

export default function VoucherPembayaran({
  val,
  currentIndex,
  getImageCcBank,
  redirectCard,
  mixpanelPersonalised,
}) {
  const payloadMixpanel = {
    'Voucher Name': val[currentIndex]?.title || 'none',
    'Voucher Code': val[currentIndex]?.voucher_code || 'none',
    'Click Location': 'Voucher Pembayaran',
  }
  if (currentIndex !== -1 && currentIndex !== null) {
    return (
      <div
        className={`personalised-card__voucher-payment ${
          config.environment === 'desktop' ? 'cursor-pointer' : ''
        }`}
        onClick={() => {
          redirectCard(val?.[currentIndex]?.target_link)
          mixpanelPersonalised(payloadMixpanel)
        }}
      >
        <div
          className={`col-xs-4 ${
            config.environment === 'desktop' ? 'flex justify-center' : ''
          }`}
        >
          <CustomLazyLoadImage
            src={getImageCcBank(
              val?.[currentIndex]?.payment_method,
              val?.[currentIndex]?.payment_type,
            )}
            alt={val?.[currentIndex]?.payment_type
              ?.replace(/\s+/g, '-')
              .toLowerCase()}
            placeholderAspectRatio={1}
            placeholder={<Skeleton />}
          />
        </div>
        <div className='col-xs-8'>
          <div
            className={`card-content ${
              config.environment === 'desktop' ? 'padding-right-s' : ''
            }`}
          >
            <div className='card-content__title ui-text-4'>
              {val[currentIndex]?.title}
            </div>
            <div className='card-content__description heading-3'>
              {val[currentIndex]?.description}
            </div>
            <div className='card-content__voucher-code ui-text-4 row'>
              <div
                className={`col-xs-${
                  config.environment === 'desktop' ? '5' : '4'
                }`}
              >
                Kode Promo:
              </div>
              <div
                className={`col card-content__voucher-code_detail padding-left-xs  ${
                  config.environment === 'desktop' ? '' : 'padding-left-xs'
                }`}
              >
                {val[currentIndex]?.voucher_code}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  } else {
    return null
  }
}
