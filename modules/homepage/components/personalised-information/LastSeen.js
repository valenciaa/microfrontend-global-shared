import React from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../../config'
import CustomLazyLoadImage from '../../../../layouts/atoms/CustomLazyLoadImage'

import startCase from 'lodash/startCase'
import toLower from 'lodash/toLower'
export default function LastSeen({
  val,
  currentIndex,
  keys,
  redirectCard,
  removable,
  handleRemoveCard,
  mixpanelPersonalised,
}) {
  const payloadMixpanel = {
    'Title': val[currentIndex]?.title || 'none', // eslint-disable-line
    'ITM Source': val[currentIndex]?.url_key
      ? 'personalized-info-shop-in-shop-homepage-' +
        val[currentIndex]?.url_key?.replace('.html', '')
      : 'none',
    'ITM Campaign': val[currentIndex]?.url_key?.replace('.html', '') || 'none',
    'ITM Device': config.environment,
    'Click Location': 'Last Seen',
  }
  return (
    <div
      className='personalised-card__last-seen cursor-pointer'
      onClick={() => {
        redirectCard(val[currentIndex].target_link)
        mixpanelPersonalised(payloadMixpanel)
      }}
    >
      <div className='col-xs-5 margin-right-xs flex justify-content-center'>
        <CustomLazyLoadImage
          src={val[currentIndex]?.img_url}
          alt={val[currentIndex]?.title?.replace(/\s+/g, '-').toLowerCase()}
          id='homepagePromoBrandProductImage'
          height='72'
          width='72'
          placeholderAspectRatio={1}
          placeholder={<Skeleton />}
        />
      </div>
      <div className='col-xs-7 card-content'>
        <div
          className={`card-content__title-card ui-text-${
            config.environment === 'desktop' ? '4' : '3'
          }`}
        >
          Berdasarkan Pencarianmu
        </div>
        <div
          className={`card-content__product-title heading-${
            config.environment === 'desktop' ? '3' : '2'
          }`}
        >
          {startCase(toLower(val[currentIndex]?.title))}
        </div>
      </div>
      {removable === 10 && (
        <div className='close-icon' onClick={() => handleRemoveCard(keys)}>
          <CustomLazyLoadImage
            src={config.assetsURL + 'icon/close-fill.svg'}
            id='close-btn'
            alt='close-btn'
            height='32'
            width='32'
          />
        </div>
      )}
    </div>
  )
}
