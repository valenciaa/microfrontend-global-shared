import React, { useEffect, useState } from 'react'

export default function CountdownTimer({
  date,
  timesUpReminder = () => {},
  translateTime,
}) {
  const calculateTimeLeft = (date) => {
    const difference = +new Date(date?.replace(/ /g, 'T')) - +new Date()
    let timeLeft = {}

    if (difference > 0) {
      timeLeft = {
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      }
    } else {
      timeLeft = {
        hours: 0,
        minutes: 0,
        seconds: 0,
      }
    }

    return timeLeft
  }

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(date))
  const [hydrated, setHydrated] = useState(false); // NEW: Added hydrated state

  useEffect(() => {
    setHydrated(true); // NEW: Marks hydration as complete

    const timer = setTimeout(() => {
      setTimeLeft(calculateTimeLeft(date))
    }, 1000)

    if (
      timeLeft.hours === 0 &&
      timeLeft.minutes === 0 &&
      timeLeft.seconds === 0
    ) {
      clearTimeout(timer)
      timesUpReminder()
    }
  })

  if (!hydrated) {
    return null; // NEW: Prevent rendering until hydration
  }

  const timerComponents = []
  Object.keys(timeLeft).forEach((interval, index) => {
    if (interval === 'seconds') {
      timerComponents.push(
        <span key={index}>
          {/* {timeLeft[interval] < 10 ? `0${timeLeft[interval]}` : timeLeft[interval]} */}
          {translateTime
            ? `${timeLeft.hours > 0 ? timeLeft.hours + ' jam ' : ''}${
                timeLeft.minutes > 0 ? timeLeft.minutes + ' menit ' : ''
              }${timeLeft.seconds > 0 ? timeLeft.seconds + ' detik ' : ''}`
            : `${
                timeLeft[interval] < 10
                  ? `0${timeLeft[interval]}`
                  : timeLeft[interval]
              }`}
        </span>,
      )
    } else {
      timerComponents.push(
        <span key={index}>
          {/* {timeLeft[interval] < 10 ? `0${timeLeft[interval]}` : timeLeft[interval]}: */}
          {translateTime
            ? ''
            : `${
                timeLeft[interval] < 10
                  ? `0${timeLeft[interval]}`
                  : timeLeft[interval]
              }` + ':'}
        </span>,
      )
    }
  })

  return <>{timerComponents}</>
}
