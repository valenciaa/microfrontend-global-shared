import React from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../config'
import { useConstantsContext } from '../../../context/ConstantsContext'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import ItmGenerator from '../../../utils/ItmGenerator'
import { CURRENT_OFFER } from '../../../utils/constants'

export default function TrendingItemsCard({
  content,
  identifier,
  index,
  height,
  homepageStyles,
  width,
  onClick,
  placeholderAspectRatio,
}) {
  const constantsContext = useConstantsContext()
  const pageType = constantsContext?.pageType
  const newUrl = ItmGenerator(
    pageType + CURRENT_OFFER,
    content.target_link,
    content.title,
    '',
  )
  //! CHECK HERE
  return (
    <>
      {config.environment === 'mobile' ? (
        <div
          className={`${homepageStyles.content}${
            index % 2 !== 0 ? ' margin-left-m' : ''
          }`}
        >
          <a onClick={onClick} href={newUrl} id={`${identifier}${index}`}>
            <CustomLazyLoadImage
              src={content.image.replace(
                'https://res.cloudinary.com/ruparupa-com/image/upload/',
                'https://res.cloudinary.com/ruparupa-com/image/upload/w_480,h_270,q_auto:low/',
              )}
              alt={content.title || 'Banner Penawaran Saat Ini'}
              placeholderAspectRatio={placeholderAspectRatio}
              height={height}
              width={width}
              placeholder={<Skeleton />}
            />
          </a>
        </div>
      ) : (
        <a
          onClick={onClick}
          href={`${newUrl}`}
          id={`imgPenawaranSaatIni${index}`}
        >
          <CustomLazyLoadImage
            src={content.image}
            alt={content.title}
            placeholder={<Skeleton />}
            placeholderAspectRatio={200 / 416}
            height='200'
            width='416'
          />
        </a>
      )}
    </>
  )
}
