import React from 'react'
import Skeleton from 'react-loading-skeleton'
import { useConstantsContext } from '../../../context/ConstantsContext'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { ItmGenerator } from '../../../utils'
import { CURRENT_OFFER } from '../../../utils/constants'

export default function HeaderDescriptionCard({ content, index, onClick }) {
  const constantsContext = useConstantsContext()
  const pageType = constantsContext?.pageType
  const newUrl = ItmGenerator(
    pageType + CURRENT_OFFER,
    content?.urlLink,
    content?.title,
    '',
  )
  return (
    <a
      onClick={onClick}
      href={`${newUrl}`}
      id={`imgHeaderDescriptionPenawaranSaatIni${index}`}
    >
      <CustomLazyLoadImage
        placeholder={<Skeleton />}
        placeholderAspectRatio={200 / 632}
        height='200'
        width='632'
        src={content?.imageURL}
        alt={content?.title}
      />
    </a>
  )
}
