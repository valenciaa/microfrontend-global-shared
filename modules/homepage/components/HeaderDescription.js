import React from 'react'
import Skeleton from 'react-loading-skeleton'
import HeaderDescriptionCard from './HeaderDescriptionCard'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { ItmGenerator } from '../../../utils'
import config from '../../../../../config'
import { useConstantsContext } from '../../../context/ConstantsContext'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import { CURRENT_OFFER } from '../../../utils/constants'

export default function HeaderDescription({
  content,
  homepageStyles,
  onClick,
  location,
}) {
  const constantsContext = useConstantsContext()
  const pageType = constantsContext?.pageType
  const newUrl = ItmGenerator(
    pageType + CURRENT_OFFER,
    content?.urlLink,
    content?.title,
    '',
  )

  if (content) {
    if (config.environment === 'mobile') {
      return (
        <div className={'col-xs-12 ' + homepageStyles['header-description']}>
          <a onClick={onClick} href={newUrl}>
            <CustomLazyLoadImage
              placeholder={<Skeleton />}
              placeholderAspectRatio={183 / 500}
              src={content?.imageURL}
              alt={content?.title}
              height='100%'
              width='100%'
            />
          </a>
        </div>
      )
    } else {
      return content?.map((content, indexContent) => {
        return (
          <HeaderDescriptionCard
            onClick={() => {
              mixpanelTrack('Penawaran Saat Ini', {
                Title: content?.title || 'None',
                Location: location,
              })

              if (onClick) {
                onClick()
              }
            }}
            content={content}
            key={`${indexContent}`}
            index={indexContent}
          />
        )
      })
    }
  } else {
    return null
  }
}
