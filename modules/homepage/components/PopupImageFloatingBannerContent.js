import { useState } from 'react'
import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'
import Skeleton from 'react-loading-skeleton'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import config from '../../../../../config'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { ConvertImageType, ResizeImage } from '../../../utils'

export default function PopupImageFloatingBannerContent({
  content,
  homepageStyles,
  onClose,
  url,
}) {
  const [imageError, setImageError] = useState(false)
  const handleCloseWelcomeModal = (e) => {
    e.preventDefault()
    setModalCookies()
    onClose()
  }

  const setModalCookies = () => {
    const in1days = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
    Cookies.set(`welcome-modal-${config.baseURL}`, '1', { expires: in1days })
  }

  const isLinkNewTab = content?.open_link && content?.open_link === 'linknewtab'

  return (
    !isEmpty(content) && (
      <div className={homepageStyles['floating-welcome-banner']}>
        <div
          className={homepageStyles.close__button}
          onClick={(e) => handleCloseWelcomeModal(e)}
        >
          <LazyLoadImage
            src={config.assetsURL + 'icon/close-fill.svg'}
            className='close-btn'
            id='close-btn'
            alt='close-btn'
            height={24}
            width={24}
          />
        </div>
        <div className={homepageStyles.welcome__content}>
          {!imageError ? (
            <a
              href={url}
              target={isLinkNewTab ? '_blank' : '_self'}
              rel={isLinkNewTab ? 'noopener noreferrer' : undefined}
              onClick={() => setModalCookies()}
            >
              {/* Above the fold image, doesn't need lazy load source: https://medium.com/ne-digital/how-to-improve-lcp-and-speed-index-for-next-js-websites-f129ae776835 */}
              <img
                src={
                  config.environment === 'mobile'
                    ? ResizeImage(ConvertImageType(content.img_src), 400, 400)
                    : ConvertImageType(content.img_src)
                }
                alt={content.img_alt}
                height={config.environment === 'mobile' ? 400 : '600'}
                width={config.environment === 'mobile' ? 400 : '600'}
                onError={() => setImageError(true)}
              />
            </a>
          ) : (
            <CustomLazyLoadImage
              className={homepageStyles['image__' + config.environment]}
              src={`${config.assetsURL}images/no-image.jpg`}
              placeholderAspectRatio={400 / 400}
              placeholder={<Skeleton />}
              alt='no-image'
              height='100%'
              width='100%'
            />
          )}
        </div>
      </div>
    )
  )
}
