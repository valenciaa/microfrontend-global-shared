import { useEffect, useState } from 'react'
import config from '../../../../../config'

const SECOND = 1000
const MINUTE = SECOND * 60
// const HOUR = MINUTE * 60
// const DAY = HOUR * 24
export default function PersonalisedTimer({ deadline, interval = MINUTE }) {
  const timeState = new Date(deadline)
  const calculateTimeLeft = (date) => {
    const difference = new Date(date) - new Date()
    let time = {}
    if (difference > 0) {
      time = {
        day: Math.floor(difference / (24 * 1000 * 60 * 60)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      }
    } else {
      time = {
        day: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
      }
    }

    return time
  }
  const [timeLeft, setTimeLeft] = useState({})

  useEffect(() => {
    if (deadline) {
      setTimeLeft(calculateTimeLeft(deadline))
    }
  }, [deadline])

  useEffect(() => {
    const timer = setInterval(() => {
      setTimeLeft(calculateTimeLeft(deadline))
    }, interval)

    if (
      timeLeft?.hours === 0 &&
      timeLeft?.minutes === 0 &&
      timeLeft?.seconds === 0
    ) {
      clearTimeout(timer)
    }

    return () => clearTimeout()
  }, [deadline, interval])

  const renderTimeLeft = () => {
    if (timeLeft?.day) {
      return `${timeState.getDate()} ${timeState.toLocaleString('default', {
        month: 'short',
      })}`
    }

    if (timeLeft?.hours) {
      return `${timeLeft.hours} jam`
    }

    if (timeLeft?.minutes) {
      return `${timeLeft.minutes} menit`
    }

    return 'Habis'
  }
  return (
    <span
      className={`${timeLeft?.day ? 'color-grey-50' : 'color-red-50'} ${
        config.environment === 'desktop' ? 'ui-text-4' : 'ui-text-5'
      } bold display-flex align-center`}
    >
      {renderTimeLeft()}
    </span>
  )
}
