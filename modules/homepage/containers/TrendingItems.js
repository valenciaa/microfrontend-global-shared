import isEmpty from 'lodash/isEmpty'
import React from 'react'
import HeaderDescription from '../components/HeaderDescription'
import { useHomepageContext } from '../../../../../src/homepage/context/HomepageContext'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import TrendingItemsCard from '../components/TrendingItemsCard'
import config from '../../../../../config'
import { useConstantsContext } from '../../../context/ConstantsContext'

export default function TrendingItems() {
  const homepageContext = useHomepageContext()
  const { pageFrom, pageType } = useConstantsContext()
  const location =
    pageFrom === 'static-shoped' && pageType === 'static'
      ? 'Shoped WA'
      : 'Homepage'
  const penawaranSaatIni = homepageContext?.results?.penawaranSaatIni
  const homepageStyles = homepageContext?.homepageStyles

  return (
    <>
      {!isEmpty(penawaranSaatIni) &&
        (config.environment === 'mobile' ? (
          <section
            className={
              'container-card penawaran-saat-ini penawaran-saat-ini__mobile ' +
              homepageStyles['penawaran-saat-ini']
            }
          >
            <div className='heading-2'>{penawaranSaatIni?.layout_title}</div>
            <div className={homepageStyles['current-offer'] + ' margin-top-s'}>
              <HeaderDescription
                onClick={() =>
                  mixpanelTrack('Penawaran Saat Ini', {
                    Title:
                      penawaranSaatIni?.header_description[0]?.title || 'None', //eslint-disable-line
                    Location: location,
                  })
                }
                content={penawaranSaatIni?.header_description?.[0]}
                height='100%'
                width='100%'
                homepageStyles={homepageStyles}
              />
              {penawaranSaatIni?.content?.map((content, indexContent) => (
                <TrendingItemsCard
                  onClick={() =>
                    mixpanelTrack('Penawaran Saat Ini', {
                      Title: content?.title || 'None', //eslint-disable-line
                      Location: location,
                    })
                  }
                  placeholderAspectRatio={107 / 226}
                  content={content}
                  identifier='penawaranSaatIni'
                  index={indexContent}
                  key={indexContent}
                  height='100%'
                  width='100%'
                  homepageStyles={homepageStyles}
                />
              ))}
            </div>
          </section>
        ) : (
          <section
            className={
              'penawaran-saat-ini penawaran-saat-ini__desktop ' +
              homepageStyles['penawaran-saat-ini']
            }
          >
            <div className='section-title'>
              <div className='heading-2'>{penawaranSaatIni?.layout_title}</div>
            </div>
            <div className='margin-top-m'>
              <div
                className={
                  'header-description ' + homepageStyles['header-description']
                }
              >
                <HeaderDescription
                  location={location}
                  content={penawaranSaatIni?.header_description}
                />
              </div>
              <div
                className={
                  'margin-top-m section-content ' +
                  homepageStyles['section-content']
                }
              >
                {penawaranSaatIni?.layout?.[0]?.content.map(
                  (content, indexContent) => (
                    <TrendingItemsCard
                      onClick={() =>
                        mixpanelTrack('Penawaran Saat Ini', {
                          Title: content?.title || 'None', //eslint-disable-line
                          Location: location,
                        })
                      }
                      content={content}
                      index={indexContent}
                      key={indexContent}
                    />
                  ),
                )}
              </div>
            </div>
          </section>
        ))}
    </>
  )
}
