import { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import { useInView } from 'react-intersection-observer'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import { VueInViewWrapper } from './VueRecoWrapper'
const SpecialRecommendationContainer = dynamic(
  () => import('../../../../../src/homepage/containers/SpecialRecommendation'),
)

export default function SpecialRecommendationWrapper({
  configuration,
  from,
  total,
  pageFrom,
  location,
  useVue,
}) {
  const [refSpecialRecommendation, inViewSpecialRecommendation] = useInView({
    ...configuration,
    triggerOnce: true,
  })
  const [specialRecommendation, setSpecialRecommendation] = useState(false)

  useEffect(() => {
    if (inViewSpecialRecommendation) {
      setSpecialRecommendation(true)
    }
  }, [inViewSpecialRecommendation])
  return (
    <ErrorBoundary>
      {specialRecommendation ? (
        <VueInViewWrapper>
          <SpecialRecommendationContainer
            from={from}
            total={total}
            pageFrom={pageFrom}
            location={location}
            useVue={useVue}
          />
        </VueInViewWrapper>
      ) : (
        <ComponentWrapper ref={refSpecialRecommendation} height={500} />
      )}
    </ErrorBoundary>
  )
}
