import React from 'react'
import isEmpty from 'lodash/isEmpty'
import Skeleton from 'react-loading-skeleton'
import { useHomepageContext } from '../../../../../src/homepage/context/HomepageContext'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { ConvertImageType, ItmGenerator } from '../../../utils'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import config from '../../../../../config'
import { useConstantsContext } from '../../../context/ConstantsContext'

export default function BannerSisip() {
  const constantsContext = useConstantsContext()
  const pageType = constantsContext?.pageType
  const homepageContext = useHomepageContext()
  const bannerSisip = homepageContext?.results?.bannerSisip
  const homepageStyles = homepageContext?.homepageStyles

  const newUrl = (url, campaign) => {
    return ItmGenerator(pageType + '-banner-sisip', url, campaign)
  }
  return (
    <>
      {config.environment === 'mobile'
        ? !isEmpty(bannerSisip?.header_description) &&
          !isEmpty(bannerSisip?.content) && (
            <section
              className={'container-card ' + homepageStyles['banner-sisip']}
            >
              <div className={homepageStyles.content}>
                <div className='first-banner'>
                  <a
                    onClick={() =>
                      mixpanelTrack('Banner Sisip Homepage', {
                        Title:
                          bannerSisip?.header_description?.[0]?.title || 'None', //eslint-disable-line
                      })
                    }
                    href={newUrl(
                      bannerSisip?.header_description?.[0]?.urlLink,
                      bannerSisip?.header_description?.[0]?.title,
                    )}
                  >
                    <CustomLazyLoadImage
                      src={ConvertImageType(
                        bannerSisip?.header_description?.[0]?.imageURL,
                      )}
                      placeholderAspectRatio={121 / 339}
                      placeholder={<Skeleton />}
                      height='100%'
                      width='100%'
                      alt={
                        bannerSisip?.header_description?.[0]?.title ||
                        'Banner 1'
                      }
                    />
                  </a>
                </div>
                <div className='second-banner'>
                  <a
                    onClick={() =>
                      mixpanelTrack('Banner Sisip Homepage', {
                        Title: bannerSisip?.content?.[0]?.title || 'None', //eslint-disable-line
                      })
                    }
                    href={newUrl(
                      bannerSisip?.content?.[0]?.target_link,
                      bannerSisip?.content?.[0]?.title,
                    )}
                  >
                    <CustomLazyLoadImage
                      src={ConvertImageType(bannerSisip?.content?.[0]?.image)}
                      placeholderAspectRatio={121 / 121}
                      placeholder={<Skeleton />}
                      height='100%'
                      width='100%'
                      alt={bannerSisip?.content?.[0]?.title || 'Banner 2'}
                    />
                  </a>
                </div>
              </div>
            </section>
          )
        : !isEmpty(bannerSisip?.header_description) &&
          !isEmpty(bannerSisip?.layout) && (
            <section
              className={homepageStyles['banner-sisip'] + ' margin-top-m'}
            >
              <div className={homepageStyles.content}>
                <a
                  onClick={() =>
                    mixpanelTrack('Banner Sisip Homepage', {
                      Title:
                        bannerSisip?.header_description?.[0]?.title || 'None', //eslint-disable-line
                    })
                  }
                  href={newUrl(
                    bannerSisip?.header_description?.[0]?.urlLink,
                    bannerSisip?.header_description?.[0]?.title,
                  )}
                >
                  <CustomLazyLoadImage
                    src={ConvertImageType(
                      bannerSisip?.header_description?.[0]?.imageURL,
                    )}
                    placeholderAspectRatio={120 / 956}
                    placeholder={<Skeleton />}
                    height='120'
                    width='956'
                    alt={
                      bannerSisip?.header_description?.[0]?.title || 'Banner 1'
                    }
                  />
                </a>
                <a
                  onClick={() =>
                    mixpanelTrack('Banner Sisip Homepage', {
                      Title:
                        bannerSisip?.layout?.[0]?.content?.[0]?.title || 'None', //eslint-disable-line
                    })
                  }
                  href={newUrl(
                    bannerSisip?.layout?.[0]?.content?.[0]?.target_link,
                    bannerSisip?.layout?.[0]?.content?.title,
                  )}
                >
                  <CustomLazyLoadImage
                    src={ConvertImageType(
                      bannerSisip?.layout?.[0]?.content?.[0]?.image,
                    )}
                    placeholderAspectRatio={120 / 308}
                    placeholder={<Skeleton />}
                    height='120'
                    width='308'
                    alt={
                      bannerSisip?.layout?.[0]?.content?.[0].title || 'Banner 2'
                    }
                  />
                </a>
              </div>
            </section>
          )}
    </>
  )
}
