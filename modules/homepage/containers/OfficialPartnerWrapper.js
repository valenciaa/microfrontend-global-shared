import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
const OfficialPartner = dynamic(
  () =>
    import('../../../../../src/homepage/containers/cms-block/OfficialPartner'),
)

export default function OfficialPartnerWrapper({ configuration }) {
  const [refOfficialPartner, inViewOfficialPartner] = useInView({
    ...configuration,
  })
  const [officialPartnerViewPort, setOfficialPartnerViewport] = useState(false)
  useEffect(() => {
    if (inViewOfficialPartner) {
      setOfficialPartnerViewport(true)
    }
  }, [inViewOfficialPartner])
  return (
    <ErrorBoundary>
      {officialPartnerViewPort ? (
        <OfficialPartner />
      ) : (
        <ComponentWrapper
          ref={refOfficialPartner}
          height={config.environment === 'desktop' ? 244 : 218}
        />
      )}
    </ErrorBoundary>
  )
}
