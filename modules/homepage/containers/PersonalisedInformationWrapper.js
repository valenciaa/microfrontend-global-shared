import dynamic from 'next/dynamic'
import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import { ReviewRatingProvider } from '../../../../../src/unifyapps/context/ReviewRatingContext'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
const PersonalisedInformation = dynamic(
  () =>
    import('../../../../../src/homepage/containers/PersonalisedInformation'),
)

export default function PersonalisedInformationWrapper({ configuration }) {
  // this is for rendering when inside the view port
  const [refPersonalisedInformation, inViewPersonalisedInformation] = useInView(
    { ...configuration },
  )
  const [personalisedInformationViewPort, setPersonalisedInformationViewport] =
    useState(false)
  useEffect(() => {
    if (inViewPersonalisedInformation) {
      setPersonalisedInformationViewport(true)
    }
  }, [inViewPersonalisedInformation])
  return (
    <ErrorBoundary>
      {personalisedInformationViewPort ? (
        <ReviewRatingProvider>
          <PersonalisedInformation configuration={configuration} />
        </ReviewRatingProvider>
      ) : (
        <ComponentWrapper height={642} ref={refPersonalisedInformation} />
      )}
    </ErrorBoundary>
  )
}
