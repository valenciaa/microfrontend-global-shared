import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'

const FlashSaleContainer = dynamic(
  () => import('../../../../../src/homepage/containers/FlashSaleContainer'),
)

export default function FlashSaleWrapper({ configuration }) {
  const [refFlashSale, inViewFlashSale] = useInView({ ...configuration })
  const [flashSaleViewPort, setFlashSaleViewport] = useState(false)

  useEffect(() => {
    if (inViewFlashSale) {
      setFlashSaleViewport(true)
    }
  }, [inViewFlashSale])
  return (
    <ErrorBoundary>
      {flashSaleViewPort ? (
        <FlashSaleContainer />
      ) : (
        <ComponentWrapper ref={refFlashSale} height={408} />
      )}
    </ErrorBoundary>
  )
}
