import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'
import React, { useEffect, useState } from 'react'
import { SwiperSlide } from 'swiper/react'
import { useHomepageContext } from '../../../../../src/homepage/context/HomepageContext'
import ReviewRatingItemCard from '../components//personalised-information/ReviewRatingItemCard'

export default function ReviewRatingContainer({
  product,
  idx,
  mixpanelPersonalised,
}) {
  const [showModal, setShowModal] = useState(false)
  const homepageContext = useHomepageContext()
  const homepageStyles = homepageContext?.homepageStyles

  const [rating, setRating] = useState(0)
  const [reviewProduct, setReviewProduct] = useState([])
  const [flag, setFlag] = useState(0)

  useEffect(() => {
    let totalShowProduct = 0

    const getCookiesHomepageReviewed = Cookies.get('homepage_reviewed')

    const cookiesHomepageReviewed = getCookiesHomepageReviewed
      ? JSON.parse(getCookiesHomepageReviewed)
      : []

    if (isEmpty(reviewProduct) && product && flag === 0) {
      const collection = []

      for (let j = 0; j < product?.length; j++) {
        const orderProduct = product?.[j]

        let canInsert = true

        if (cookiesHomepageReviewed && cookiesHomepageReviewed.length > 0) {
          // this is for checking if the data already reviewd
          for (let k = 0; k < cookiesHomepageReviewed?.length; k++) {
            if (
              cookiesHomepageReviewed[k].sku === orderProduct.sku &&
              cookiesHomepageReviewed[k].invoice_no === orderProduct.invoice_no
            ) {
              canInsert = false

              return
            }
          }
        }
        if (canInsert && totalShowProduct < 3) {
          totalShowProduct = totalShowProduct + 1

          collection.push(orderProduct)
        }
      }

      setFlag(1)

      return setReviewProduct(collection)
    }
  }, [product, reviewProduct])

  if (!isEmpty(product)) {
    return (
      <SwiperSlide className='personal' key={idx}>
        <div
          className={`tw-justify-center ${homepageStyles['review-rating__container-wrapper']}`}
        >
          <ReviewRatingItemCard
            mixpanelPersonalised={mixpanelPersonalised}
            product={product?.[0]}
            rating={rating}
            setRating={setRating}
            setShowModal={setShowModal}
            showModal={showModal}
          />
        </div>
      </SwiperSlide>
    )
  }

  return null
}
