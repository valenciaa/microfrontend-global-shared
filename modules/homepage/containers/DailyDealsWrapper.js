import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'

const DailyDealsContainer = dynamic(
  () => import('../../../../../src/homepage/containers/DailyDealsContainer'),
)

export default function DailyDealsWrapper({ configuration }) {
  const [refDailyDeals, inViewDailyDeals] = useInView({ ...configuration })
  const [dailyDealsViewPort, setDailyDealsViewport] = useState(false)

  useEffect(() => {
    if (inViewDailyDeals) {
      setDailyDealsViewport(true)
    }
  }, [inViewDailyDeals])
  return (
    <ErrorBoundary>
      {dailyDealsViewPort ? (
        <DailyDealsContainer />
      ) : (
        <ComponentWrapper ref={refDailyDeals} height={348} />
      )}
    </ErrorBoundary>
  )
}
