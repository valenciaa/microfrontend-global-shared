import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import localforage from 'localforage'

import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import { useHyperPersonalize } from '../../../services/api/queries/global'
const PromoBank = dynamic(
  () => import('../../../../../src/homepage/containers/cms-block/PromoBank'),
)

export default function PromoBankWrapper({ configuration }) {
  // this is for rendering when inside the view port
  const [refPromoBank, inViewPromoBank] = useInView({ ...configuration })
  const [promoBankViewPort, setPromoBankViewport] = useState(false)
  const [accessToken, setAccessToken] = useState(undefined)

  useEffect(() => {
    const setup = async () => {
      const accessToken = await localforage.getItem('access_token')
      if (accessToken) {
        setAccessToken(accessToken)
      }
    }
    setup()
  }, [])

  const { data } = useHyperPersonalize(
    'promo-bank',
    config.companyCode === 'HCI' ? 'promo-partner' : 'promo-bank',
    accessToken,
    undefined,
    {
      enabled: promoBankViewPort,
    },
  )

  useEffect(() => {
    if (inViewPromoBank) {
      setPromoBankViewport(true)
    }
  }, [inViewPromoBank])
  return (
    <ErrorBoundary>
      {promoBankViewPort ? (
        <PromoBank withSectionTitle promoBank={data} />
      ) : (
        <ComponentWrapper
          ref={refPromoBank}
          height={config.environment === 'desktop' ? 332 : 224}
        />
      )}
    </ErrorBoundary>
  )
}
