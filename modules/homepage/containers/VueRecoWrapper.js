import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import VueAiRecommendation from '../../../../../src/pcp/components/VueAiRecommendation'

export default function VueRecoWrapper({ title, from }) {
  const configuration = {
    threshold: 0.3,
    triggerOnce: true,
  }

  const [refInspirationPromoMajor, inViewInspirationPromoMajor] = useInView({
    ...configuration,
  })
  const [vueRecoWrapperViewport, setVueRecoWrapperViewport] = useState(false)

  useEffect(() => {
    if (inViewInspirationPromoMajor) {
      setVueRecoWrapperViewport(true)
    }
  }, [inViewInspirationPromoMajor])

  // This handler below will manage the Algolia wrapper to display Vue components
  return (
    <ErrorBoundary>
      {vueRecoWrapperViewport ? (
        <>
          <VueAiRecommendation
            title={title}
            isBackground
            location='keyword'
            isSearchResult={true}
            from={from}
          />
          <hr className='tw-absolute tw-left-0 tw-w-full tw-h-[5px] tw-border-none tw-bg-[#f3f4f5]' />
        </>
      ) : (
        <ComponentWrapper ref={refInspirationPromoMajor} height={522} />
      )}
    </ErrorBoundary>
  )
}

export function VueInViewWrapper({ children, wrapperHeight = 522 }) {
  const configuration = {
    threshold: 0.3,
    triggerOnce: true,
  }

  const [refVueReco, inViewVue] = useInView({
    ...configuration,
  })
  const [vueRecoWrapperViewport, setVueRecoWrapperViewport] = useState(false)

  useEffect(() => {
    if (inViewVue) {
      setVueRecoWrapperViewport(true)
    }
  }, [inViewVue])

  // This handler below will manage All Vue Component that using inView to display
  return (
    <ErrorBoundary>
      {vueRecoWrapperViewport ? (
        <>{children}</>
      ) : (
        <ComponentWrapper ref={refVueReco} height={wrapperHeight} />
      )}
    </ErrorBoundary>
  )
}
