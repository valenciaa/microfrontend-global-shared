import dynamic from 'next/dynamic'
import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
const InspirationPromoMajorContainer = dynamic(
  () =>
    import(
      '../../../../../src/homepage/containers/InspirationPromoMajorContainer'
    ),
)

export default function InspirationPromoMajorWrapper({ configuration }) {
  const [refInspirationPromoMajor, inViewInspirationPromoMajor] = useInView({
    ...configuration,
  })
  const [inspirationPromoMajorViewport, setInspirationPromoMajorViewport] =
    useState(false)
  useEffect(() => {
    if (inViewInspirationPromoMajor) {
      setInspirationPromoMajorViewport(true)
    }
  }, [inViewInspirationPromoMajor])

  return (
    <ErrorBoundary>
      {inspirationPromoMajorViewport ? (
        <InspirationPromoMajorContainer />
      ) : (
        <ComponentWrapper ref={refInspirationPromoMajor} height={522} />
      )}
    </ErrorBoundary>
  )
}
