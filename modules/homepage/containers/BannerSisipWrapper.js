import dynamic from 'next/dynamic'
import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
const BannerSisip = dynamic(() => import('./BannerSisip'))

export default function BannerSisipWrapper() {
  const [refBannerSisip, inViewBannerSisip] = useInView({
    threshold: 1,
    triggerOnce: true,
  })
  const [bannerSisipViewport, setBannerSisipViewport] = useState(false)
  useEffect(() => {
    if (inViewBannerSisip) {
      setBannerSisipViewport(true)
    }
  }, [inViewBannerSisip])

  return (
    <ErrorBoundary>
      {bannerSisipViewport ? (
        <BannerSisip />
      ) : (
        <ComponentWrapper
          ref={refBannerSisip}
          height={config.environment === 'desktop' ? 152 : 88}
        />
      )}
    </ErrorBoundary>
  )
}
