import { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import { useInView } from 'react-intersection-observer'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
const PromoRecommendationContainer = dynamic(
  () =>
    import(
      '../../../../../src/homepage/containers/PromoRecommendationContainer'
    ),
)

export default function PromoRecommendationWrapper({
  configuration,
  isCSR = false,
}) {
  const [refPromoRecommendation, inViewPromoRecommendation] = useInView({
    ...configuration,
  })
  const [promoRecommendationViewport, setPromoRecommendation] = useState(false)

  useEffect(() => {
    if (inViewPromoRecommendation) {
      setPromoRecommendation(true)
    }
  }, [inViewPromoRecommendation])
  return (
    <ErrorBoundary>
      {promoRecommendationViewport ? (
        <PromoRecommendationContainer isCSR={isCSR} />
      ) : (
        <ComponentWrapper
          ref={refPromoRecommendation}
          height={config?.environment === 'desktop' ? 332 : 224}
        />
      )}
    </ErrorBoundary>
  )
}
