import dynamic from 'next/dynamic'
import { useInView } from 'react-intersection-observer'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import { useEffect, useState } from 'react'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import { useVueRecommendationContext } from '../../../context/VueRecommedationContext'

import isEmpty from 'lodash/isEmpty'
import { useVueContext } from '../../../../global/context/VueContext'

const CategoryRecommendContainer = dynamic(
  () => import('../../../../../src/homepage/containers/CategoryRecommend'),
)

import { VueInViewWrapper } from '../../../../global/modules/homepage/containers/VueRecoWrapper'

export default function CategoryRecommendWrapper({
  configuration,
  location = '',
  useVue = false,
  pageFrom = '',
}) {
  const [categoryRecommend, setCategoryRecommend] = useState(false)
  const [refCategoryRecommend, inViewCategoryRecommend] = useInView({
    ...configuration,
    triggerOnce: true,
  })

  useEffect(() => {
    if (inViewCategoryRecommend) {
      setCategoryRecommend(true)
    }
  }, [inViewCategoryRecommend])

  const vueRecommendationContext = useVueRecommendationContext()
  const [dataVueReco, setDataVueReco] = useState({})

  const vueContext = useVueContext() || {}
  const { vueRecommedation = {} } = vueContext || {}

  useEffect(() => {
    if (
      (!isEmpty(vueRecommendationContext) || !isEmpty(vueRecommedation)) &&
      isEmpty(dataVueReco)
    ) {
      const data =
        vueRecommendationContext?.vueRecommedation || vueRecommedation
      const journeyMetadata = data?._journey_metadata || {}
      const dataModule = data?.data_module || []
      if (dataModule?.length > 0) {
        // make sure the includes recommended for category recommendation
        const catData = dataModule?.filter((val) =>
          val?.module_name?.toLowerCase()?.includes('categories'),
        )
        if (catData?.length > 0) {
          setDataVueReco({ ...catData[0], _journey_metadata: journeyMetadata })
        }
      }
    }
  }, [vueRecommendationContext, vueRecommedation])

  return (
    <ErrorBoundary>
      {categoryRecommend && !isEmpty(dataVueReco?.data) ? (
        <VueInViewWrapper>
          <CategoryRecommendContainer
            pageFrom={pageFrom}
            useVue={useVue}
            location={location}
            dataVueReco={dataVueReco}
          />
        </VueInViewWrapper>
      ) : (
        <ComponentWrapper
          ref={refCategoryRecommend}
          height={dataVueReco?.data ? 290 : 0}
        />
      )}
    </ErrorBoundary>
  )
}
