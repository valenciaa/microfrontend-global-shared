import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ExploreByTrending from '../../../../../src/homepage/containers/cms-block/ExploreByTrending'

export default function ExploreByTrendingWrapper({ configuration }) {
  // this is for rendering when inside the view port
  const [refExploreByTrending, inViewExploreByTrending] = useInView({
    ...configuration,
  })
  const [exploreByTrendingViewPort, setExploreByTrendingViewport] =
    useState(false)

  useEffect(() => {
    if (inViewExploreByTrending) {
      setExploreByTrendingViewport(true)
    }
  }, [inViewExploreByTrending])
  return (
    <ErrorBoundary>
      {exploreByTrendingViewPort ? (
        <ExploreByTrending />
      ) : (
        <ComponentWrapper
          ref={refExploreByTrending}
          height={config.environment === 'desktop' ? 276 : 416}
        />
      )}
    </ErrorBoundary>
  )
}
