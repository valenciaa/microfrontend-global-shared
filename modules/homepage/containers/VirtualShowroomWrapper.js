import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const VirtualShowroom = dynamic(
  () => import('../../../../../src/homepage/containers/VirtualShowroom'),
)
const PdpVirtualShowroomWrapper = dynamic(
  () => import('../../../container/PdpVirtualShowroomWrapper'),
)

export default function VirtualShowroomWrapper({
  configuration,
  urlKey,
  dataMicrosite,
  tahuStyles,
}) {
  // this is for rendering when inside the view port
  const [refVirtualShowroom, inViewVirtualShowroom] = useInView({
    ...configuration,
  })
  const [virtualShowroomViewPort, setVirtualShowroomViewport] = useState(false)
  const chooseVirtualShowroomLayout = () => {
    if (dataMicrosite?.banner_only) {
      return (
        <section
          className={`virtual-showroom virtual-showroom__${
            config.environment
          } ${
            config.environment === 'desktop' ? tahuStyles['banner-only'] : ''
          }`}
        >
          <PdpVirtualShowroomWrapper
            isLoading={false}
            virtualShowData={dataMicrosite}
          />
        </section>
      )
    }
    return <VirtualShowroom urlKey={urlKey} dataMicrosite={dataMicrosite} />
  }
  useEffect(() => {
    if (inViewVirtualShowroom) {
      setVirtualShowroomViewport(true)
    }
  }, [inViewVirtualShowroom])
  return (
    <ErrorBoundary>
      {virtualShowroomViewPort ? (
        chooseVirtualShowroomLayout()
      ) : (
        <ComponentWrapper ref={refVirtualShowroom} height={642} />
      )}
    </ErrorBoundary>
  )
}
