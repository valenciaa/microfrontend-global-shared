import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
const ExploreByCategoryB2b = dynamic(
  () =>
    import(
      '../../../../../src/homepage/containers/cms-block/ExploreByCategoryB2b'
    ),
)

export default function ExploreByCategoryB2bWrapper({
  configuration,
  identifier,
}) {
  // this is for rendering when inside the view port
  const [refExploreByCategory, inViewExploreByCategory] = useInView({
    ...configuration,
  })
  const [exploreByCategoryViewPort, setExploreByCategoryViewport] =
    useState(false)
  useEffect(() => {
    if (inViewExploreByCategory) {
      setExploreByCategoryViewport(true)
    }
  }, [inViewExploreByCategory])
  return (
    <ErrorBoundary>
      {exploreByCategoryViewPort ? (
        <ExploreByCategoryB2b identifier={identifier} withSectionTitle />
      ) : (
        <ComponentWrapper
          ref={refExploreByCategory}
          height={config.environment === 'desktop' ? 332 : 208}
        />
      )}
    </ErrorBoundary>
  )
}
