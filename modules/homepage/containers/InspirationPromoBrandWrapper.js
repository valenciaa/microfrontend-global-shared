import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
const InspirationPromoBrandContainer = dynamic(
  () =>
    import(
      '../../../../../src/homepage/containers/InspirationPromoBrandContainer'
    ),
)

export default function InspirationPromoBrandWrapper({ configuration }) {
  const [refInspirationPromoBrand, inViewInspirationPromoBrand] = useInView({
    ...configuration,
  })
  const [inspirationPromoBrandViewPort, setInspirationPromoBrandViewport] =
    useState(false)
  useEffect(() => {
    if (inViewInspirationPromoBrand) {
      setInspirationPromoBrandViewport(true)
    }
  }, [inViewInspirationPromoBrand])

  return (
    <ErrorBoundary>
      {inspirationPromoBrandViewPort ? (
        <InspirationPromoBrandContainer />
      ) : (
        <ComponentWrapper ref={refInspirationPromoBrand} height={522} />
      )}
    </ErrorBoundary>
  )
}
