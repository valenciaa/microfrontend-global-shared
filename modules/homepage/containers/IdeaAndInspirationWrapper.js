import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
const IdeaAndInspiration = dynamic(
  () => import('../../../../../src/homepage/containers/IdeaAndInspiration'),
)

export default function IdeaAndInspirationWrapper({
  configuration,
  hideSeeMore,
  accessToken,
}) {
  // this is for rendering when inside the view port
  const [refIdeaAndInspiration, inViewIdeaAndInspiration] = useInView({
    ...configuration,
  })
  const [ideaAndInspirationViewPort, setIdeaAndInspirationViewport] =
    useState(false)
  useEffect(() => {
    if (inViewIdeaAndInspiration) {
      setIdeaAndInspirationViewport(true)
    }
  }, [inViewIdeaAndInspiration])
  return (
    <ErrorBoundary>
      {ideaAndInspirationViewPort ? (
        <IdeaAndInspiration
          hideSeeMore={hideSeeMore}
          accessToken={accessToken}
        />
      ) : (
        <ComponentWrapper ref={refIdeaAndInspiration} height={642} />
      )}
    </ErrorBoundary>
  )
}
