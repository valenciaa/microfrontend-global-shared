import { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import { useInView } from 'react-intersection-observer'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
const InspirationShopByStyleContainer = dynamic(
  () =>
    import(
      '../../../../../src/homepage/containers/InspirationShopByStyleContainer'
    ),
)

export default function InspirationShopByStyleWrapper({ configuration }) {
  const [refInspirationShopByStyle, inViewInspiationShopByStyle] = useInView({
    ...configuration,
  })
  const [inspirationShopByStyleViewport, setInspirationShopByStyle] =
    useState(false)
  useEffect(() => {
    if (inViewInspiationShopByStyle) {
      setInspirationShopByStyle(true)
    }
  }, [inViewInspiationShopByStyle])

  return (
    <ErrorBoundary>
      {inspirationShopByStyleViewport ? (
        <InspirationShopByStyleContainer />
      ) : (
        <ComponentWrapper ref={refInspirationShopByStyle} height={522} />
      )}
    </ErrorBoundary>
  )
}
