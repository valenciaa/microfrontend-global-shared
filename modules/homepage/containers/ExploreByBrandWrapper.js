import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
const ExploreByBrand = dynamic(
  () =>
    import('../../../../../src/homepage/containers/cms-block/ExploreByBrand'),
)

export default function ExploreByBrandWrapper({ configuration }) {
  // this is for rendering when inside the view port
  const [refExploreByBrand, inViewExploreByBrand] = useInView({
    ...configuration,
  })
  const [exploreByBrandViewport, setExploreByBrandViewport] = useState(false)
  useEffect(() => {
    if (inViewExploreByBrand) {
      setExploreByBrandViewport(true)
    }
  }, [inViewExploreByBrand])
  return (
    <ErrorBoundary>
      {exploreByBrandViewport ? (
        <ExploreByBrand />
      ) : (
        <ComponentWrapper
          ref={refExploreByBrand}
          height={config.environment === 'desktop' ? 534 : 332}
        />
      )}
    </ErrorBoundary>
  )
}
