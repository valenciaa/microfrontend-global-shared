import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'

import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
const ExploreByCategory = dynamic(
  () =>
    import(
      '../../../../../src/homepage/containers/cms-block/ExploreByCategory'
    ),
)

export default function ExploreByCategoryWrapper({ configuration }) {
  // this is for rendering when inside the view port
  const [refExploreByCategory, inViewExploreByCategory] = useInView({
    ...configuration,
  })
  const [exploreByCategoryViewPort, setExploreByCategoryViewport] =
    useState(false)

  useEffect(() => {
    if (inViewExploreByCategory) {
      setExploreByCategoryViewport(true)
    }
  }, [inViewExploreByCategory])

  return (
    <ErrorBoundary>
      {exploreByCategoryViewPort ? (
        <ExploreByCategory withSectionTitle />
      ) : (
        <ComponentWrapper
          ref={refExploreByCategory}
          height={config.environment === 'desktop' ? 332 : 208}
        />
      )}
    </ErrorBoundary>
  )
}
