import React, { useEffect, useState } from 'react'
import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'
import dynamic from 'next/dynamic'
import config from '../../../../../config'
import { ItmGenerator } from '../../../utils'
const PopupImageFloatingBannerContent = dynamic(
  () => import('../components/PopupImageFloatingBannerContent'),
)
const Modal = dynamic(() => import('../../../layouts/templates/Modal'))

export default function PopupImageFloatingBanner({
  popUpImage,
  homepageStyles,
}) {
  // const [payload, setPayload] = useState({})
  // const [error, setError] = useState(null)
  const [onClickedWelcomeModal, setOnClickedWelcomeModal] = useState(false)
  const [userHasScroll, setuserHasScroll] = useState(false)
  const welcomeModalCookies = Cookies.get(`welcome-modal-${config.baseURL}`)

  useEffect(() => {
    if (welcomeModalCookies) {
      setOnClickedWelcomeModal(false)
    }
    function scrollListener() {
      if (window.pageYOffset > 180 && !userHasScroll) {
        setuserHasScroll(true)
      }
    }
    window.addEventListener('scroll', scrollListener, { passive: true })
    return () => {
      window.removeEventListener('scroll', scrollListener)
    }
  }, [])

  useEffect(() => {
    if (
      !isEmpty(popUpImage) &&
      !isEmpty(popUpImage[0]) &&
      !welcomeModalCookies &&
      userHasScroll === true
    ) {
      setOnClickedWelcomeModal(true)
    }
  }, [userHasScroll])

  const newUrl = ItmGenerator(
    'banner-popup-homepage',
    popUpImage?.[0]?.url_link,
  )
  const popUpImageWelcomeModalBody = (functionInsideModal) => (
    <PopupImageFloatingBannerContent
      content={popUpImage?.[0]}
      onClose={functionInsideModal}
      url={newUrl}
      homepageStyles={homepageStyles}
    />
  )

  return (
    <>
      {onClickedWelcomeModal && (
        <Modal
          bodyClass='modal-body-floating-banner'
          contentClass={homepageStyles['modal-content-floating-banner']}
          dialogClass={
            homepageStyles['modal-dialog-floating-banner'] +
            ' ' +
            homepageStyles[
              'modal-dialog-floating-banner__' + config.environment
            ]
          }
          bodyElement={popUpImageWelcomeModalBody}
          onClose={() => setOnClickedWelcomeModal(false)}
          show={onClickedWelcomeModal}
        />
      )}
    </>
  )
}
