import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import { useGetB2bCmsRequest } from '../../../services/api/queries/global'
import isEmpty from 'lodash/isEmpty'
const BenefitB2b = dynamic(
  () => import('../../../../../src/homepage/containers/cms-block/BenefitB2b'),
)

export default function ExploreByCategoryB2bWrapper({
  configuration,
  identifier,
  floatingBar,
}) {
  // this is for rendering when inside the view port
  const [refExploreByCategory, inViewExploreByCategory] = useInView({
    ...configuration,
  })
  const [exploreByCategoryViewPort, setExploreByCategoryViewport] =
    useState(false)
  useEffect(() => {
    if (inViewExploreByCategory) {
      setExploreByCategoryViewport(true)
    }
  }, [inViewExploreByCategory])

  const { data: cmsB2b } = useGetB2bCmsRequest(identifier, {
    enabled: !isEmpty(identifier),
  })

  return (
    <ErrorBoundary>
      {exploreByCategoryViewPort && cmsB2b ? (
        <div className='margin-top-xxxl'>
          <BenefitB2b
            withSectionTitle
            cmsB2b={cmsB2b}
            floatingBar={floatingBar}
          />
        </div>
      ) : (
        <ComponentWrapper
          ref={refExploreByCategory}
          height={config.environment === 'desktop' ? 332 : 208}
        />
      )}
    </ErrorBoundary>
  )
}
