import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const ShopInShop = dynamic(
  () => import('../../../../../src/homepage/containers/cms-block/ShopInShop'),
)

export default function ShopInShopWrapper({ configuration }) {
  const [refShopInShop, inViewShopInShop] = useInView({ ...configuration })
  const [shopInShopViewPort, setShopInShopViewport] = useState(false)
  useEffect(() => {
    if (inViewShopInShop) {
      setShopInShopViewport(true)
    }
  }, [inViewShopInShop])
  return (
    <ErrorBoundary>
      {shopInShopViewPort ? (
        <ShopInShop />
      ) : (
        <ComponentWrapper
          ref={refShopInShop}
          height={config.environment === 'desktop' ? 332 : 232}
        />
      )}
    </ErrorBoundary>
  )
}
