import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'

const TrendingItems = dynamic(() => import('./TrendingItems'))

export default function TrendingItemsWrapper({ configuration }) {
  const [refTrendingItems, inViewTrendingItems] = useInView({
    ...configuration,
  })
  const [trendingItemsViewPort, setTrendingItemsViewport] = useState(false)
  useEffect(() => {
    if (inViewTrendingItems) {
      setTrendingItemsViewport(true)
    }
  }, [inViewTrendingItems])
  return (
    <ErrorBoundary>
      {trendingItemsViewPort ? (
        <TrendingItems />
      ) : (
        <ComponentWrapper ref={refTrendingItems} height={408} />
      )}
    </ErrorBoundary>
  )
}
