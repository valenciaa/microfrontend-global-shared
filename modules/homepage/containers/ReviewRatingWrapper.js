import dynamic from 'next/dynamic'
import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import config from '../../../../../config'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'

const ReviewRatingContainer = dynamic(() => import('./ReviewRatingContainer'))

export default function ReviewRatingWrapper({
  configuration,
  currentIndex,
  product,
  idx,
  mixpanelPersonalised,
}) {
  // this is for rendering when inside the view port
  const [refReviewrating, inReviewRating] = useInView({ ...configuration })
  const [reviewRatingViewPort, setReviewRatingViewport] = useState(false)

  useEffect(() => {
    if (inReviewRating) {
      setReviewRatingViewport(true)
    }
  }, [inReviewRating])

  return (
    <ErrorBoundary>
      {reviewRatingViewPort ? (
        <ReviewRatingContainer
          currentIndex={currentIndex}
          idx={idx}
          mixpanelPersonalised={mixpanelPersonalised}
          product={product}
        />
      ) : (
        <ComponentWrapper
          height={config.environment === 'desktop' ? 332 : 224}
          ref={refReviewrating}
        />
      )}
    </ErrorBoundary>
  )
}
