import isEmpty from 'lodash/isEmpty'
import FileUploaded from '../../../../../src/b2b/components/FileUploaded'
import FileUploadB2b from '../../../../../src/b2b/components/FileUploadB2b'

const renderView = (fileValueWatch, field, stepName) => {
  if (!isEmpty(fileValueWatch)) {
    return (
      <FileUploaded
        fieldName={field.name}
        value={fileValueWatch}
        stepName={stepName}
      />
    )
  } else if (isEmpty(fileValueWatch)) {
    return <FileUploadB2b field={field} stepName={stepName} />
  }
}

export default renderView
