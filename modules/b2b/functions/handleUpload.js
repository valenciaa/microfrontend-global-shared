import config from '../../../../../config'
import { GetInfoDocument } from '../../../utils/GetInfoDocument'
import { AHI_B2B } from '../../../utils/WordingB2b'

const handleUpload = async (e, field, setError, uploadFileB2b) => {
  const fileSelected = e.target.files[0]

  if (!fileSelected) {
    return
  }

  if (fileSelected.type !== 'application/pdf') {
    return setError(
      `customerProfile.${field.name}`,
      'Hanya hanya dapat mengunggah file PDF.',
    )
  }

  if (fileSelected.size > 4000000) {
    return setError(
      `customerProfile.${field.name}`,
      'File melebihi batas ukuran maksimal 4MB',
    )
  }

  const fileBase64 = await GetInfoDocument(fileSelected)

  let payload = {
    file: fileBase64,
    fileFormat: 'pdf',
  }

  let fileName = ''

  switch (field.name) {
    case 'npwp':
      payload.fileFolder = `${
        config.companyCode === 'AHI' ? AHI_B2B + 'B2B' : 'INFORMAB2B'
      }-ACCOUNT/NPWP`
      fileName = `FILE_NPWP`
      break
    case 'sppkp':
      payload.fileFolder = `${
        config.companyCode === 'AHI' ? AHI_B2B + 'B2B' : 'INFORMAB2B'
      }-ACCOUNT/SPPKP`
      fileName = `FILE_SPPKP`
      break

    default:
      break
  }

  payload.fileName = fileName + `_${new Date().getTime()}.pdf`
  uploadFileB2b(payload)
}

export default handleUpload
