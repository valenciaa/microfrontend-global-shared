import isEmpty from 'lodash/isEmpty'

const getClassnameFile = (
  b2bStyles,
  fileValueWatch,
  errors,
  field,
  stepName,
) => {
  let mainClass = b2bStyles['input-file-b2b']

  if (!isEmpty(fileValueWatch)) {
    mainClass = b2bStyles['input-file-b2b__file']
  }

  return `${mainClass} ${
    errors?.[stepName]?.[field.name]?.message
      ? b2bStyles['input-file-b2b--error']
      : ''
  }`
}

export default getClassnameFile
