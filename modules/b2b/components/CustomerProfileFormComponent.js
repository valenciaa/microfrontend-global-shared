import { useB2bContext } from '../../../context/B2bContext'
import { useFormContext } from 'react-hook-form'
import { useCheckAuthDataB2b } from '../../../services/api/mutations/authB2b'
import { useGetTaxClass } from '../../../services/api/queries/global'
import GetB2bType from '../../../utils/GetB2bType'
import { useEffect, useMemo } from 'react'
import { debounce } from 'lodash'
import Infobox from '../../../layouts/atoms/Infobox'
import config from '../../../../../config'
import { formB2bMapper } from '../../../utils/FormB2b'
import FileTypeForm from '../../../../../src/b2b/components/register-form-b2b/form-type/FileTypeForm'
import SelectB2bForm from '../../../../../src/b2b/components/register-form-b2b/form-type/SelectB2bForm'
import TextAreaB2bComponent from '../../../../../src/b2b/components/register-form-b2b/form-type/TextAreaB2bComponent'
import InputB2bComponent from '../../../../../src/b2b/components/register-form-b2b/form-type/InputB2bComponent'

export default function CustomerProfileForm({ city }) {
  const { b2bStyles } = useB2bContext()
  const stepName = 'customerProfile'

  const {
    watch,
    setValue,
    formState: { isDirty },
  } = useFormContext()

  const { mutate: checkAuthDataNpwp } = useCheckAuthDataB2b({
    onSuccess: (res) => {
      if (res.ok && res?.data?.data?.is_used === 1) {
        setValue('customerProfile.isNpwpUsed', true, { shouldValidate: true })
      } else {
        setValue('customerProfile.isNpwpUsed', false, { shouldValidate: true })
      }
    },
  })

  const { data: taxClasses } = useGetTaxClass()

  const getOptionProps = (field) => {
    switch (field.name) {
      case 'city':
        return {
          option: city,
          labelKey: 'city_name',
          valueKey: 'city_id',
        }

      case 'typeOfPkp':
        return {
          option: taxClasses,
          labelKey: 'description',
          valueKey: 'code',
        }

      default:
        return {
          option: field.option,
        }
    }
  }

  const handleCheckNpwp = () => {
    checkAuthDataNpwp({
      npwpNumber: watch('customerProfile.npwpNumber'),
      action: 'change_npwp',
      registeredBy: GetB2bType(),
    })
  }

  const debounceCheckNpwp = useMemo(() => {
    if (!isDirty) {
      return
    }
    return debounce(handleCheckNpwp, 300)
  }, [watch('customerProfile.npwpNumber')])

  useEffect(() => {
    if (!isDirty && watch('customerProfile.npwpNumber') !== '') {
      return
    }
    debounceCheckNpwp()
    return () => {
      debounceCheckNpwp.cancel()
    }
  }, [watch('customerProfile.npwpNumber')])

  return (
    <>
      <div className={b2bStyles['form-b2b-container']}>
        <div className={b2bStyles['form-wrapper']}>
          <div className='tw-mb-4'>
            <Infobox
              text='Kolom bertanda (*) wajib diisi. Mohon lengkapi sebelum
                melanjutkan.'
              theme='info'
              className='margin-top-s'
            >
              <div className='flex'>
                <img src={config.assetsURL + 'icon/info-blue.svg'} />
              </div>
            </Infobox>
          </div>
          <div className={b2bStyles['title-text']}>Profil Pengguna</div>
          <div className={b2bStyles['form-container']}>
            {formB2bMapper.customerProfile.map((field, index) => {
              switch (field.type) {
                case 'file':
                  return (
                    <FileTypeForm
                      key={index}
                      field={field}
                      stepName={stepName}
                    />
                  )
                case 'select':
                  return (
                    <SelectB2bForm
                      key={index}
                      field={field}
                      {...getOptionProps(field)}
                      stepName={stepName}
                    />
                  )
                case 'textarea':
                  return (
                    <TextAreaB2bComponent
                      key={index}
                      field={field}
                      stepName={stepName}
                    />
                  )

                default:
                  return (
                    <InputB2bComponent
                      key={'input-type-' + index}
                      field={field}
                      stepName={stepName}
                    />
                  )
              }
            })}
          </div>
        </div>
      </div>
    </>
  )
}
