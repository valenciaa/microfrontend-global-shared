import React from 'react'
import { useB2bContext } from '../../../context/B2bContext'
import Link from 'next/link'
import Button from '../../../layouts/atoms/Button'

const ThankyouPage = () => {
  const { b2bStyles } = useB2bContext()
  return (
    <div className={b2bStyles.thankyou}>
      <div className={b2bStyles.title}>Terimakasih!</div>
      <hr />
      <div className={b2bStyles.text}>
        Permintaan Anda telah diproses. Mohon tunggu 1-2 hari kerja untuk kami
        memvalidasi permintaan Anda.
      </div>
      <div className={b2bStyles.btn}>
        <Link href={'/'}>
          <a>
            <Button type='primary'>Kembali ke Homepage</Button>
          </a>
        </Link>
      </div>
    </div>
  )
}

export default ThankyouPage
