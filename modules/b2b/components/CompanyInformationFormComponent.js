import React from 'react'
import { useB2bContext } from '../../../context/B2bContext'
import { useFormContext } from 'react-hook-form'
import {
  useGetAnnualPurchase,
  useGetBuildingOwnerShip,
  useGetLob,
  useGetRevenue,
  useGetSubLineOfBusiness,
} from '../../../services/api/queries/global'
import isEmpty from 'lodash/isEmpty'
import SelectB2bForm from '../../../../../src/b2b/components/register-form-b2b/form-type/SelectB2bForm'
import { formB2bMapper } from '../../../utils/FormB2b'
import TextAreaB2bComponent from '../../../../../src/b2b/components/register-form-b2b/form-type/TextAreaB2bComponent'
import InputB2bComponent from '../../../../../src/b2b/components/register-form-b2b/form-type/InputB2bComponent'
import CheckBoxFormComponent from '../../../../../src/b2b/components/register-form-b2b/form-type/CheckBoxFormComponent'

export default function CompanyInformationForm() {
  const { b2bStyles } = useB2bContext()
  const { watch } = useFormContext()
  const stepName = 'companyInformation'

  const { data: revenue } = useGetRevenue()
  const { data: buildingOwnership } = useGetBuildingOwnerShip()
  const { data: annualPurchase } = useGetAnnualPurchase()
  const { data: lob } = useGetLob()
  const { data: subLob } = useGetSubLineOfBusiness(
    watch(`${stepName}.lineOfBusiness`),
    { enabled: !isEmpty(watch(`${stepName}.lineOfBusiness`)) },
  )

  const getOptions = (field) => {
    const config = {
      labelKey: 'description',
      valueKey: 'code',
    }

    switch (field.name) {
      case 'revenuePerYear':
        config.option = revenue
        break
      case 'buildingOwnership':
        config.option = buildingOwnership
        break
      case 'annualPurchasedEstimation':
        config.option = annualPurchase
        break
      case 'lineOfBusiness':
        config.option = lob
        break
      case 'mainProduct':
        config.option = subLob
        break
      default:
        config.labelKey = 'label'
        config.valueKey = 'value'
        config.option = field.option
        break
    }

    return config
  }

  return (
    <div className={b2bStyles['form-b2b-container']}>
      <div className={b2bStyles['form-wrapper']}>
        <div className={b2bStyles['title-text']}>Informasi Perusahaan</div>
        <div className={b2bStyles['form-container']}>
          {formB2bMapper.companyInformation.map((field, index) => {
            switch (field.type) {
              case 'select':
                if (field.name === 'mainProduct' && isEmpty(subLob)) {
                  return null
                }
                return (
                  <SelectB2bForm
                    field={field}
                    key={index}
                    {...getOptions(field)}
                    stepName={stepName}
                  />
                )
              case 'checkbox':
                return (
                  <CheckBoxFormComponent
                    field={field}
                    key={index}
                    stepName={stepName}
                  />
                )
              case 'textarea':
                return (
                  <TextAreaB2bComponent
                    field={field}
                    key={index}
                    stepName={stepName}
                  />
                )
              default:
                return (
                  <InputB2bComponent
                    field={field}
                    key={index}
                    stepName={stepName}
                  />
                )
            }
          })}
        </div>
      </div>
    </div>
  )
}
