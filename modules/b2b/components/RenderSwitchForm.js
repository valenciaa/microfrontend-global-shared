import BillingInformationFormComponent from './BillingInformationForm'
import CompanyInformationForm from './CompanyInformationFormComponent'
import CustomerProfileForm from './CustomerProfileFormComponent'

const RenderSwitchForm = ({ step, city }) => {
  switch (step) {
    case 1:
      return <CustomerProfileForm city={city} />
    case 2:
      return <BillingInformationFormComponent city={city} />
    case 3:
      return <CompanyInformationForm />
  }
}

export default RenderSwitchForm
