import React from 'react'
import Infobox from '../../../layouts/atoms/Infobox'
import config from '../../../../../config'
import { useB2bContext } from '../../../context/B2bContext'
import { formB2bMapper } from '../../../utils/FormB2b'
import SelectB2bForm from '../../../../../src/b2b/components/register-form-b2b/form-type/SelectB2bForm'
import TextAreaB2bComponent from '../../../../../src/b2b/components/register-form-b2b/form-type/TextAreaB2bComponent'
import InputB2bComponent from '../../../../../src/b2b/components/register-form-b2b/form-type/InputB2bComponent'
import clsx from 'clsx'
import CheckBoxFormComponent from '../../../../../src/b2b/components/register-form-b2b/form-type/CheckBoxFormComponent'

const InfoboxTermOfPayment = ({ b2bStyles }) => {
  return (
    <div className='tw-mb-4'>
      <div className='tw-my-4'>
        <Infobox theme='info' className='tw-mt-2' subtext='Hubungi kami.'>
          <div className='flex'>
            <img src={config.assetsURL + 'icon/info-blue.svg'} />
            <p
              className={clsx(
                'tw-ml-3',
                config.environment === 'mobile' ? 'tw-text-xs' : '',
              )}
            >
              Pilihan 7 hari, 14 hari, 21 hari, dan 30 hari hanya tersedia
              setelah Anda melakukan minimal 3 transaksi. Untuk informasi lebih
              lanjut,
              <span className='tw-ml-1 tw-font-semibold tw-underline'>
                <a
                  href={`mailto:${
                    config.companyCode === 'HCI'
                      ? config.picInformaB2b
                      : config.picAceB2b
                  }`}
                >
                  hubungi kami.
                </a>
              </span>
            </p>
          </div>
        </Infobox>
      </div>
      <hr />
      <div className={`${b2bStyles['title-text']} tw-mt-4`}>
        Informasi Pengiriman
      </div>
    </div>
  )
}

export default function BillingInformationFormComponent({ city }) {
  const { b2bStyles } = useB2bContext()
  const stepName = 'billingInformation'

  const getOptionList = (field) => {
    switch (field.name) {
      case 'city':
      case 'city1':
      case 'city2':
        return {
          option: city,
          labelKey: 'city_name',
          valueKey: 'city_id',
        }

      default:
        return {
          option: field.option,
        }
    }
  }

  return (
    <>
      <div className={b2bStyles['form-b2b-container']}>
        <div className={b2bStyles['form-wrapper']}>
          <div className={b2bStyles['title-text']}>Informasi Penagihan</div>
          <div className={b2bStyles['form-container']}>
            {formB2bMapper.billingInformation.map((field, index) => {
              switch (field.type) {
                case 'select':
                  return (
                    <>
                      <SelectB2bForm
                        key={index}
                        field={field}
                        {...getOptionList(field)}
                        stepName={stepName}
                      />
                      {field.name === 'termOfPayment' && (
                        <InfoboxTermOfPayment b2bStyles={b2bStyles} />
                      )}
                    </>
                  )
                case 'checkbox':
                  return (
                    <CheckBoxFormComponent
                      key={'checkbox-type-' + index}
                      field={field}
                      stepName={stepName}
                    />
                  )
                case 'textarea':
                  return (
                    <TextAreaB2bComponent
                      key={'textarea-type-' + index}
                      field={field}
                      stepName={stepName}
                    />
                  )
                default:
                  return (
                    <InputB2bComponent
                      key={'input-type-' + index}
                      field={field}
                      stepName={stepName}
                    />
                  )
              }
            })}
          </div>
        </div>
      </div>
    </>
  )
}
