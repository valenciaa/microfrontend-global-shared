import React, { useMemo } from 'react'
import clsx from 'clsx'
import { useRouter } from 'next/router'

import ChevronLeft from '../../../../public/static/icon/chevron-prev.svg'
import ChevronRight from '../../../../public/static/icon/chevron-next.svg'
import config from '../../../../config'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import { WISHLIST_ITEM_LIMIT } from '../../utils/constants'

const paginationStyle = {
  desktop: clsx('tw-h-11 tw-w-11 tw-p-[10px]'),
  mobile: clsx('tw-h-9 tw-w-9 tw-p-[6px]'),
}

const createArrayFromRange = (from, to) => {
  let arr = []
  for (let i = from; i <= to; i++) {
    arr.push(i)
  }
  return arr
}

export default function Pagination({
  className,
  currentPage,
  location,
  pagesCount,
  setCurrentPage,
  ...props
}) {
  const router = useRouter()
  const { query } = router
  const mixpanelTrackPagination = (page) => {
    mixpanelTrack('Pagination', {
      'Page Number': page,
      'Location': location,
      'ITM Source': 'wishlist-rr',
    })
  }
  const displayedPages = useMemo(() => {
    if (pagesCount) {
      const lastPage = pagesCount
      if (!lastPage) {
        return
      }

      const low = Math.max(1, currentPage - 2)
      const high = Math.min(currentPage + 2, lastPage)

      return createArrayFromRange(low, high)
    }
  }, [pagesCount, currentPage])

  const isFirstPage = currentPage === 1
  const isLastPage = currentPage === pagesCount

  const getCommonButtonProps = (page) => {
    const isActive = currentPage === page
    const buttonClasses = clsx(
      'tw-flex tw-items-center tw-justify-center tw-rounded-[4px] tw-border tw-border-grey-20 tw-border-solid disabled:tw-cursor-not-allowed disabled:tw-text-grey-30 disabled:tw-border-grey-20',
      paginationStyle[config.environment],
    )
    const interactiveButtonClasses = clsx(
      'hover:tw-border-blue-40 tw-text-grey-50',
    )
    return {
      className: clsx(
        buttonClasses,
        isActive ? 'tw-bg-blue-50 tw-text-white' : interactiveButtonClasses,
      ),
      color: isActive ? 'secondary' : 'gray',
      variant: isActive ? 'filled' : 'outlined',
      noInteractive: !isActive,
    }
  }
  const setUpdatedUrl = (page) => {
    const params = { ...query, page }
    router.push(
      {
        pathname:
          config.environment === 'desktop' ? '/my-account' : '/wishlist',
        query: params,
      },
      undefined,
      { shallow: true },
    )
  }

  return (
    <ul
      className={clsx('tw-flex tw-gap-0.5 tw-items-stretch', className)}
      {...props}
    >
      {
        <li className='tw-flex tw-list-none'>
          <button
            {...getCommonButtonProps()}
            disabled={isFirstPage}
            onClick={() => {
              const nextPage = currentPage - 1
              setCurrentPage('currentPage', nextPage)
              mixpanelTrackPagination(nextPage)
              if (nextPage === 1) {
                setCurrentPage('offset', 0)
              } else {
                const nextOffset =
                  nextPage * WISHLIST_ITEM_LIMIT - WISHLIST_ITEM_LIMIT
                setCurrentPage('offset', nextOffset)
              }
              window.scrollTo(0, 0)
              setUpdatedUrl(nextPage)
            }}
          >
            <div>
              <ChevronLeft
                className={clsx(
                  isFirstPage ? 'tw-text-grey-20' : 'tw-text-grey-50',
                )}
                height={24}
                width={24}
              />
            </div>
          </button>
        </li>
      }
      {displayedPages?.[0] > 1 && (
        <>
          <li
            className='tw-flex tw-list-none'
            onClick={() => {
              mixpanelTrackPagination(1)
              setCurrentPage('currentPage', 1)
              setCurrentPage('offset', 0)
              window.scrollTo(0, 0)
              setUpdatedUrl(1)
            }}
          >
            <button {...getCommonButtonProps()}>
              <p className={clsx('heading-4')}>1</p>
            </button>
          </li>
          <li className='tw-flex tw-list-none'>
            <button {...getCommonButtonProps()} disabled>
              ...
            </button>
          </li>
        </>
      )}
      {displayedPages?.map((page, idx) => (
        <li
          className='tw-flex tw-list-none'
          key={idx}
          onClick={() => {
            const nextOffset = page * WISHLIST_ITEM_LIMIT - WISHLIST_ITEM_LIMIT
            mixpanelTrackPagination(page)
            setCurrentPage('currentPage', page)
            setCurrentPage('offset', nextOffset)
            window.scrollTo(0, 0)
            setUpdatedUrl(page)
          }}
        >
          <button {...getCommonButtonProps(page)}>
            <p className={clsx('heading-4')}>{page}</p>
          </button>
        </li>
      ))}
      {displayedPages?.[displayedPages?.length - 1] < pagesCount && (
        <>
          <li className='tw-flex tw-list-none'>
            <button {...getCommonButtonProps()} disabled>
              ...
            </button>
          </li>
          <li
            className='tw-flex tw-list-none'
            onClick={() => {
              const nextOffset =
                pagesCount * WISHLIST_ITEM_LIMIT - WISHLIST_ITEM_LIMIT
              mixpanelTrackPagination(pagesCount)
              setCurrentPage('currentPage', pagesCount)
              setCurrentPage('offset', nextOffset)
              window.scrollTo(0, 0)
              setUpdatedUrl(pagesCount)
            }}
          >
            <button {...getCommonButtonProps()}>
              <p className={clsx('heading-4')}>{pagesCount}</p>
            </button>
          </li>
        </>
      )}
      {
        <li className='tw-flex tw-list-none'>
          <button
            {...getCommonButtonProps()}
            disabled={isLastPage}
            onClick={() => {
              const nextPage = currentPage + 1
              mixpanelTrackPagination(nextPage)
              setCurrentPage('currentPage', nextPage)
              const nextOffset =
                nextPage * WISHLIST_ITEM_LIMIT - WISHLIST_ITEM_LIMIT
              setCurrentPage('offset', nextOffset)
              window.scrollTo(0, 0)
              setUpdatedUrl(nextPage)
            }}
          >
            <div>
              <ChevronRight
                className={clsx(
                  isLastPage ? 'tw-text-grey-20' : 'tw-text-grey-50',
                )}
                height={24}
                width={24}
              />
            </div>
          </button>
        </li>
      }
    </ul>
  )
}
