import React from 'react'
import EmptyState from '../../layouts/moleculs/EmptyState'
import config from '../../../../config'
import {
  BANK_ACCOUNT_EMPTY_STATE_DESCRIPTION,
  BANK_ACCOUNT_EMPTY_STATE_TITLE,
  BANK_ACCOUNT_REFUND_HISTORY_EMPTY_STATE_TITLE,
  WISHLIST_EMPTY_STATE_DESCRIPTION,
  WISHLIST_EMPTY_STATE_TITLE,
} from '../../utils/constants'

const height = {
  desktop: '288',
  mobile: '200',
}

const width = {
  desktop: '550',
  mobile: '343',
}

const imageSrc = {
  desktop: 'logo-error.jpeg',
  mobile: 'empty-state.svg',
}

const emptyStateTitle = {
  'Wishlist': WISHLIST_EMPTY_STATE_TITLE,
  'My Bank Account': BANK_ACCOUNT_EMPTY_STATE_TITLE,
  'My Bank Account Refund History':
    BANK_ACCOUNT_REFUND_HISTORY_EMPTY_STATE_TITLE,
}

const emptyStateDescription = {
  'Wishlist': WISHLIST_EMPTY_STATE_DESCRIPTION,
  'My Bank Account': BANK_ACCOUNT_EMPTY_STATE_DESCRIPTION,
}

export default function WishlistEmptyState({
  isNeedDescription = true,
  isSearchResult = false,
  pageFrom = 'Wishlist',
}) {
  return (
    <EmptyState
      className=' tw-flex tw-flex-col tw-gap-2 tw-items-center tw-justify-center tw-text-center'
      imgWidth={width[config.environment]}
      imgHeight={height[config.environment]}
      imgSrc={config.assetsURL + 'images/' + imageSrc[config.environment]}
      imgAlt={pageFrom + ' Empty State'}
      id={pageFrom + ' Empty State'}
    >
      {!isSearchResult && (
        <>
          <p className='tw-text-grey-100 heading-2'>
            {emptyStateTitle[pageFrom]}
          </p>
          {isNeedDescription && (
            <p className='tw-text-grey-40 ui-text-2'>
              {emptyStateDescription[pageFrom]}
            </p>
          )}
        </>
      )}
    </EmptyState>
  )
}
