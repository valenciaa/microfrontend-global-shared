import React from 'react'
import WishlistEmptyState from './WishlistEmptyState'

export default function WishlistSearchNoResult({ wishlistMutableContext }) {
  return (
    <div className='tw-flex tw-flex-col tw-gap-6'>
      <div className='tw-flex tw-flex-col tw-gap-1 tw-text-grey-100'>
        <p className='heading-2'>Hasil Pencarian</p>
        <p className='ui-text-2'>
          Menampilkan <strong>0 produk</strong> dalam <strong>Wishlist</strong>{' '}
          dengan kata kunci{' '}
          <strong>{wishlistMutableContext?.productName}</strong>
        </p>
      </div>
      <div className='tw-flex tw-flex-col tw-items-center tw-gap-2'>
        <WishlistEmptyState isSearchResult />
        <div className='heading-2 tw-flex'>
          <p className='tw-text-grey-100'>Maaf &quot;</p>
          <p className='tw-text-blue-50'>
            {wishlistMutableContext?.productName}
          </p>
          <p className='tw-text-grey-100'>&quot; tidak ditemukan</p>
        </div>
        <p className='tw-text-grey-40 ui-text-2'>Coba ubah kata pencariannya</p>
      </div>
    </div>
  )
}
