import { useEffect } from 'react'
import clsx from 'clsx'
import { Swiper, SwiperSlide } from 'swiper/react'
import Skeleton from 'react-loading-skeleton'

import config from '../../../../config'

import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import Link from 'next/link'
import { mixpanelVueRecommendationTrack } from '../../utils/MixpanelWrapper'
import useAuthenticationData from '../../utils/hooks/useAuthenticationData'
import { useSendTrackEvent } from '../../services/api/mutations/global'
import { fetchTracking, vueClick, vueSwiper } from '../../utils/VueAIUtil'

const imageLength = config.environment === 'desktop' ? 164 : 112
const swiperStyle =
  config.environment === 'desktop' ? { position: 'unset' } : {}
const swiperSlideClass = {
  desktop: clsx('tw-max-w-[180px]'),
  mobile: clsx('tw-max-w-[128px]'),
}
const titleWrapperClassName = {
  desktop: clsx('tw-pb-3 tw-pt-8'),
  mobile: clsx('tw-pt-6 tw-px-4 tw-pb-2'),
}

export default function WishlistVueAiCatModule({
  className,
  module,
  swiperAdditionalClassName,
  correlationID,
}) {
  const { mutate: sendTracking } = useSendTrackEvent()

  const { user } = useAuthenticationData()
  const { name } = user || 'Kamu'

  useEffect(() => {
    if (correlationID) {
      module.correlationID = correlationID
    }

    fetchTracking(
      'recommendationView',
      module,
      sendTracking,
      'false',
      'pageView',
      'wishlist',
    )
  }, [])

  const onClickWishlistCategoryRecommend = (category, url, index) => {
    mixpanelVueRecommendationTrack(
      'Wishlist',
      'Kategori Pilihan Buat Kamu',
      'PCP',
      category,
      module?.module_name,
      url,
    )

    vueClick({
      module: module,
      correlationID: correlationID,
      sendTracking,
      pageFrom: 'wishlist',
      recoData: category,
      clickPosition: index + 1,
    })
  }
  return (
    <div className={clsx('tw-flex tw-flex-col', className)}>
      <div
        className={clsx(
          'tw-text-grey-100',
          titleWrapperClassName[config.environment],
        )}
      >
        <p className='heading-2'>Kategori Pilihan untuk {name}</p>
      </div>
      <div
        className={clsx(
          config.environment === 'desktop' ? 'tw-relative' : 'tw-pl-4',
        )}
      >
        <Swiper
          allowTouchMove={config.environment === 'mobile'}
          className={swiperAdditionalClassName}
          resistanceRatio={0}
          slidesPerColumnFill='row'
          slidesPerView='auto'
          spaceBetween={24}
          slidesPerColumn={1}
          navigation={config.environment === 'desktop'}
          style={swiperStyle}
          onSlideChange={(swiper) =>
            vueSwiper({
              previousIndex: swiper?.previousIndex,
              realIndex: swiper?.realIndex,
              direction: swiper?.swipeDirection,
              module: module,
              correlationID: correlationID,
              sendTracking,
              pageFrom: 'wishlist',
            })
          }
        >
          {module.data.map((category, index) => {
            const url =
              config.baseURL +
              'c/' +
              (config.environment === 'ODI'
                ? category?.c_category_url
                : category?.category_url) +
              '?itm_source=category-recommendation-wishlist-rr' +
              '&itm_campaign=' +
              encodeURIComponent(category?.l3_category) +
              '&itm_device=' +
              config.environment

            return (
              <SwiperSlide
                key={index}
                className={swiperSlideClass[config.environment]}
              >
                <Link href={url}>
                  <a
                    onClick={() =>
                      onClickWishlistCategoryRecommend(category, url, index)
                    }
                  >
                    <div className='tw-cursor-pointer tw-flex tw-flex-col tw-gap-2 tw-p-2 tw-rounded-lg tw-shadow-card tw-mb-0.5'>
                      <div className='tw-flex'>
                        <CustomLazyLoadImage
                          className='tw-rounded-lg'
                          src={category?.image}
                          height={imageLength}
                          width={imageLength}
                          alt={category?.ontology}
                          placeholder={<Skeleton />}
                        />
                      </div>
                      <p
                        className={clsx(
                          'tw-flex tw-items-center tw-justify-center tw-min-h-[32px] tw-text-grey-100 tw-text-center',
                          config.environment === 'desktop'
                            ? 'button-small-text'
                            : 'button-medium-text',
                        )}
                      >
                        {category?.l3_category}
                      </p>
                    </div>
                  </a>
                </Link>
              </SwiperSlide>
            )
          })}
        </Swiper>
      </div>
    </div>
  )
}
