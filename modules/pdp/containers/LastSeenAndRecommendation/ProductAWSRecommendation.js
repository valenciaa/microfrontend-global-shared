import React, { useMemo } from 'react'
import mixpanel from 'mixpanel-browser'
import ProductRecomendation from '../../../../layouts/templates/ProductRecommendation'
import { useProductDetailContext } from '../../../../../../src/pdp/contexts/ProductDetailContext'
import config from '../../../../../../config'
import { mixpanelTrack } from '../../../../utils/MixpanelWrapper'
import { getMixpanelTrackViewPDPRecommendationAWS } from '../../../../utils/MixpanelPDP'
import { useGetAwsRecommended } from '../../../../services/api/queries/global'

export default function ProductAWSRecommendation({
  enabled = {},
  products,
  sectionTitle,
  sectionType,
  ...props
}) {
  const { state: productDetailContext } = useProductDetailContext()
  // const [refProductAWSRecommendation, inViewProductAWSRecommendation] = useInView({ ...configuration })
  const sku = productDetailContext?.results?.productDetail?.variants?.[0]?.sku
  const params = useMemo(
    () => ({
      itemId: sku,
      type: sectionType,
      userId: mixpanel.get_distinct_id(),
    }),
    [],
  )
  const { data: productAWSRecommendation } = useGetAwsRecommended(
    params,
    enabled,
  )
  const onClickAWSProductRecommendation = (product, location, section) => {
    mixpanelTrack(
      'View PDP Recommendation',
      getMixpanelTrackViewPDPRecommendationAWS(product, location, section),
    )
  }
  return (
    ((products?.length >= 2 ||
      productAWSRecommendation?.products?.length >= 2) && (
      <ProductRecomendation
        allowTouchMove={
          config.environment === 'desktop'
            ? !(
                products?.length > 6 ||
                productAWSRecommendation?.products?.length > 6
              )
            : undefined
        }
        navigation={
          config.environment === 'desktop'
            ? products?.length > 6 ||
              productAWSRecommendation?.products?.length > 6
            : undefined
        }
        onClick={onClickAWSProductRecommendation}
        products={
          products?.slice(0, 30) ??
          productAWSRecommendation?.products?.slice(0, 30)
        }
        sectionTitle={sectionTitle}
        sectionType={sectionType}
        type='pdp'
        {...props}
      />
    )) ||
    null
  )
}
