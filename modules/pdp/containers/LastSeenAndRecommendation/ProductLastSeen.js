import React from 'react'
import { useInView } from 'react-intersection-observer'
import ErrorBoundary from '../../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../../layouts/templates/ComponentWrapper'
import ProductRecomendation from '../../../../layouts/templates/ProductRecommendation'

export default function ProductLastSeen({ configuration, products, ...props }) {
  const [refProductLastSeen, inViewProductLastSeen] = useInView({
    ...configuration,
  })
  return (
    <ErrorBoundary>
      {inViewProductLastSeen ? (
        <ProductRecomendation
          propsItmSource='product-last-seen-'
          products={products}
          sectionTitle='Produk Terakhir Dilihat'
          sectionType='last-seen'
          type='pdp'
          {...props}
        />
      ) : (
        <ComponentWrapper ref={refProductLastSeen} height={344} />
      )}
    </ErrorBoundary>
  )
}
