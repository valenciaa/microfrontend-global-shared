import { useInView } from 'react-intersection-observer'
import ErrorBoundary from '../../../../layouts/moleculs/ErrorBoundary'
import ComponentWrapper from '../../../../layouts/templates/ComponentWrapper'
import ProductAWSRecommendation from './ProductAWSRecommendation'

export default function ProductAWSRecommendationContainer({
  configuration,
  ...props
}) {
  const [refProductAWSRecommendation, inViewProductAWSRecommendation] =
    useInView({ ...configuration })
  return (
    <ErrorBoundary>
      {inViewProductAWSRecommendation ? (
        <ProductAWSRecommendation {...props} />
      ) : (
        <ComponentWrapper ref={refProductAWSRecommendation} height={344} />
      )}
    </ErrorBoundary>
  )
}
