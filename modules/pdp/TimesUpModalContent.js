import React from 'react'
import config from '../../../../config'
import Button from '../../layouts/atoms/Button'

export default function TimesUpModalContent({ productDetailStyles }) {
  return (
    <div>
      <div>
        <b>Waktu Sudah Berakhir</b>
      </div>
      <div className={productDetailStyles.desc}>
        Periode promo telah selesai. Harga produk kembali menjadi harga normal
      </div>
      <Button
        type='primary'
        size={config?.environment === 'desktop' ? 'full' : 'big'}
        handleOnClick={() => window.location.reload()}
      >
        Refresh Halaman
      </Button>
    </div>
  )
}
