import { useEffect, useState } from 'react'

const SECOND = 1000
const MINUTE = SECOND * 60
export default function PromoVoucherTimer({ deadline, interval = MINUTE }) {
  const timeState = new Date(deadline)
  const calculateTimeLeft = (date) => {
    const difference = new Date(date) - new Date()
    let timeLeft = {}
    if (difference > 0) {
      timeLeft = {
        day: Math.floor(difference / (24 * 1000 * 60 * 60)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      }
    } else {
      timeLeft = {
        day: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
      }
    }

    return timeLeft
  }

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(deadline))
  useEffect(() => {
    const timer = setInterval(() => {
      setTimeLeft(calculateTimeLeft(deadline))
    }, interval)

    if (
      timeLeft.hours === 0 &&
      timeLeft.minutes === 0 &&
      timeLeft.seconds === 0
    ) {
      clearTimeout(timer)
    }
  }, [deadline, interval])

  const renderTimeLeft = () => {
    if (timeLeft.day) {
      return `${timeState.getDate()} ${timeState.toLocaleString('default', {
        month: 'short',
      })}`
    }

    if (timeLeft.hours) {
      return `${timeLeft.hours} jam`
    }

    if (timeLeft.minutes) {
      return `${timeLeft.minutes} menit`
    }

    return 'Habis'
  }
  return (
    <span
      className={`${
        timeLeft.day ? 'color-grey-50' : 'color-red-50'
      } ui-text-4 bold display-flex align-center`}
    >
      {renderTimeLeft()}
    </span>
  )
}
