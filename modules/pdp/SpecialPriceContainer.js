import React, { useState } from 'react'
import dayjs from 'dayjs'
import isEmpty from 'lodash/isEmpty'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'

import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import Button from '../../layouts/atoms/Button'
import Modal from '../../layouts/templates/Modal'
import CountdownTimer from '../homepage/components/CountdownTimer'
import TimesUpModalContent from './TimesUpModalContent'

import { useProductDetailContext } from '../../../../src/pdp/contexts/ProductDetailContext'
import FloatingInfobox from '../../layouts/atoms/FloatingInfobox'

dayjs.extend(isSameOrAfter)
dayjs.extend(isSameOrBefore)

export default function SpecialPriceContainer(props) {
  const { state: productDetailContext } = useProductDetailContext()
  const productDetailStyles = productDetailContext?.productDetailStyles
  const activeVariant =
    productDetailContext?.results?.productDetail?.variants?.[0]
  const voucher = props?.voucherPDP?.voucherPDP?.[0] || {}
  const voucherDetail = props?.voucherPDP?.voucherDetail?.data || {}
  const [toggleClipboardInfobox, setToggleClipboardInfobox] = useState(false)
  const [showModalTimesUp, setShowModalTimesUp] = useState(false)

  function copyClipboard() {
    navigator.clipboard.writeText(voucher?.voucher)
    setToggleClipboardInfobox(true)

    mixpanelTrack('Salin Kode Voucher PDP', {
      'Voucher Code': voucher?.voucher,
      'Item ID': activeVariant?.sku,
      'Item Name': productDetailContext?.results?.productDetail?.name,
    })
  }
  if (
    !isEmpty(voucher) &&
    !isEmpty(voucherDetail) &&
    dayjs().isSameOrAfter(
      `${voucher.date} ${voucher.start_time}`,
      'dd-mm-yy h:mm:ss',
    ) &&
    dayjs().isSameOrBefore(
      `${voucher?.date} ${voucher?.end_time}`,
      'dd-mm-yy h:mm:ss',
    ) &&
    Number(voucherDetail?.times_used) < Number(voucherDetail?.limit_used)
  ) {
    return (
      <section
        className={`${productDetailStyles['product-special-price-container']}`}
      >
        {toggleClipboardInfobox && (
          <FloatingInfobox
            duration={2500}
            toggleFlag={toggleClipboardInfobox}
            type='info'
            setToggle={setToggleClipboardInfobox}
            additionalButton={{ text: 'Tutup' }}
            additionalButtonClose
          >
            Kode promo berhasil disalin
          </FloatingInfobox>
        )}
        {showModalTimesUp && (
          <Modal
            modalType='middle-small'
            bodyClass={
              productDetailStyles['modal-body'] +
              ' ' +
              productDetailStyles['pdp-modal-times-up']
            }
            bodyElement={() => (
              <TimesUpModalContent productDetailStyles={productDetailStyles} />
            )}
            show={showModalTimesUp}
            onClose={() => setShowModalTimesUp(false)}
          />
        )}
        <div
          className={
            productDetailStyles['product-special-price-container-top-section']
          }
        >
          Harga Special Hanya Rp
          {Number(voucher?.disc_price).toLocaleString(['ban', 'id'])}
        </div>
        <div
          className={
            productDetailStyles[
              'product-special-price-container-bottom-section'
            ]
          }
        >
          <div
            className={
              productDetailStyles[
                'product-special-price-container-bottom-section-left'
              ]
            }
          >
            <div className={productDetailStyles['product-special-price-text']}>
              {voucher?.voucher}
            </div>
            <div className={productDetailStyles['product-special-price-time']}>
              <CountdownTimer
                date={`${voucher?.date} ${voucher?.end_time}`}
                timesUpReminder={() => setShowModalTimesUp(true)}
              />
            </div>
          </div>

          <div
            className={
              productDetailStyles[
                'product-special-price-container-bottom-section-right'
              ]
            }
          >
            <Button
              type='primary'
              size='medium'
              handleOnClick={() => copyClipboard()}
            >
              Salin
            </Button>
          </div>
        </div>
      </section>
    )
  } else {
    return null
  }
}
