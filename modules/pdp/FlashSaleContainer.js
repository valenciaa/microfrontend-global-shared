import React, { useState } from 'react'
import isEmpty from 'lodash/isEmpty'
import dayjs from 'dayjs'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'

import Modal from '../../layouts/templates/Modal'
import CountdownTimer from '../homepage/components/CountdownTimer'
import TimesUpModalContent from './TimesUpModalContent'

import { useProductDetailContext } from '../../../../src/pdp/contexts/ProductDetailContext'

dayjs.extend(isSameOrAfter)
dayjs.extend(isSameOrBefore)

export default function FlashSaleContainer() {
  const { state: productDetailContext } = useProductDetailContext()
  const productDetailStyles = productDetailContext?.productDetailStyles
  let event = {}
  if (productDetailContext?.results?.productDetail?.checkFlashSale) {
    event =
      productDetailContext?.results?.productDetail?.events?.find(
        (event) =>
          event?.url_key.includes('flash-sale') &&
          dayjs().isSameOrAfter(event?.start_date, 'dd-mm-yy h:mm:ss') &&
          dayjs().isSameOrBefore(event?.end_date, 'dd-mm-yy h:mm:ss'),
      ) || {}
  }
  const [showModalTimesUp, setShowModalTimesUp] = useState(false)

  if (
    !isEmpty(event) &&
    dayjs().isSameOrAfter(event?.start_date, 'dd-mm-yy h:mm:ss') &&
    dayjs().isSameOrBefore(event?.end_date, 'dd-mm-yy h:mm:ss')
  ) {
    return (
      <section
        className={`${productDetailStyles['product-flash-sale-container']}`}
      >
        {showModalTimesUp && (
          <Modal
            modalType='middle-small'
            bodyClass={
              productDetailStyles['modal-body'] +
              ' ' +
              productDetailStyles['pdp-modal-times-up']
            }
            bodyElement={() => (
              <TimesUpModalContent productDetailStyles={productDetailStyles} />
            )}
            show={showModalTimesUp}
            onClose={() => setShowModalTimesUp(false)}
          />
        )}
        <div
          className={
            productDetailStyles['product-flash-sale-container-right-section']
          }
        >
          <div
            className={
              productDetailStyles[
                'product-flash-sale-container-right-section-left'
              ]
            }
          >
            Sisa Waktu
          </div>
          <div
            className={
              productDetailStyles[
                'product-flash-sale-container-right-section-right'
              ]
            }
          >
            <div className={productDetailStyles['flashSale-time']}>
              <CountdownTimer
                date={event?.end_date}
                timesUpReminder={() => setShowModalTimesUp(true)}
              />
            </div>
          </div>
        </div>
      </section>
    )
  } else {
    return null
  }
}
