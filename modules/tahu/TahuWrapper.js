import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../layouts/moleculs/ErrorBoundary'
import { useRef } from 'react'
const ImageContainer = dynamic(
  () => import('../../../../src/tahu/containers/Images/ImageContainer'),
)
const VideoContainer = dynamic(
  () => import('../../../../src/tahu/containers/Videos/VideoContainer'),
)
const TextContainer = dynamic(
  () => import('../../../../src/tahu/containers/Texts/TextContainer'),
)
const ProductContainer = dynamic(
  () => import('../../../../src/tahu/containers/Products/ProductContainer'),
)
const DividerContainer = dynamic(
  () => import('../../../../src/tahu/containers/Dividers/DividerContainer'),
)
const EventContainer = dynamic(
  () => import('../../../../src/tahu/containers/Events/EventContainer'),
)
const SpesialContainer = dynamic(
  () => import('../../../../src/tahu/containers/Spesials/SpesialContainer'),
)

export default function TahuWrapper({
  configuration,
  component,
  template,
  index,
  url = '',
  templateComponents,
  getAll,
}) {
  // this is for rendering when inside the view port

  const [refTahu, inViewTahu] = useInView({ ...configuration })
  const refTitle = useRef()
  const scrollToTarget = (targetRef) => {
    window.scrollTo({
      top: targetRef.current.offsetTop - 130,
      behavior: 'smooth',
    })
  }

  function selectComponent() {
    switch (component) {
      case 'image':
        // Task Felicia
        return (
          <ImageContainer
            template={template}
            indexTemplate={index}
            templateComponents={templateComponents}
            url={url}
            getAll={getAll}
          />
        )
      case 'video':
        // Task Felicia
        return <VideoContainer template={template} index={index} />
      case 'divider':
        // Task Lukas
        return <DividerContainer template={template} />
      case 'event':
        // Task Anton
        return <EventContainer template={template} url={url} />
      case 'text':
        // Task Lukas
        return <TextContainer template={template} url={url} />
      case 'product':
        // Task Jason
        return (
          <ProductContainer
            refTitle={refTitle}
            template={template}
            url={url}
            getAll={getAll}
            scrollToTarget={scrollToTarget}
          />
        )
      case 'special':
        // Task Irvan
        return <SpesialContainer template={template} url={url} />
      default:
        return <div>Tidak ada tahu</div>
    }
  }
  if (templateComponents.length <= 10 || getAll) {
    return selectComponent()
  } else {
    return (
      <ErrorBoundary>
        {inViewTahu ? (
          selectComponent()
        ) : (
          <ComponentWrapper ref={refTahu} height={500} />
        )}
      </ErrorBoundary>
    )
  }
}
