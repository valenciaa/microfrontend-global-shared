import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../layouts/moleculs/ErrorBoundary'

const TahuNotFound = dynamic(
  () => import('../../../../src/tahu/containers/TahuNotFound'),
)

export default function TahuNotFoundWrapper({ configuration }) {
  // this is for rendering when inside the view port
  const [refTahuNotFound, inViewTahuNotFound] = useInView({ ...configuration })

  return (
    <ErrorBoundary>
      {inViewTahuNotFound ? (
        <TahuNotFound configuration={configuration} />
      ) : (
        <ComponentWrapper ref={refTahuNotFound} height={400} />
      )}
    </ErrorBoundary>
  )
}
