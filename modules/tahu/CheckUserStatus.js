import { useEffect } from 'react'
import isEmpty from 'lodash/isEmpty'
import { useGetTransactionListWithCache } from '../../services/api/queries/unifyapps'
import { useCartAuthContext } from '../../context/CartAuthContext'

export default function CheckUserStatus({ setOnlyForNewCustomer }) {
  const { state: cartAuthContext } = useCartAuthContext()
  const user = cartAuthContext?.auth?.user
  const params = {
    limit: 1,
    type: 'online',
  }

  const { data } = useGetTransactionListWithCache(params, {
    enabled: !!user,
  })

  useEffect(() => {
    if (!isEmpty(data?.transactions)) {
      setOnlyForNewCustomer(true)
    }
  }, [data])

  return null
}
