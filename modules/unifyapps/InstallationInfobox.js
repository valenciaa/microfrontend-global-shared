import React from 'react'
import Infobox from '../../layouts/atoms/Infobox'
import InfoboxInfo from '../../../../public/static/icon/infobox-info-16.svg'

export default function InstallationInfobox() {
  return (
    <div className='tw-px-4 tw-pt-4'>
      <Infobox theme='info'>
        <div className='tw-flex tw-items-center tw-gap-2'>
          <div className='tw-flex'>
            <InfoboxInfo className='color-blue-60' />
          </div>
          <p className='ui-text-3'>
            Jika pengajuan instalasi sudah melebihi <b>7 hari</b> dari tanggal
            barang diterima, maka segala kerusakan produk akan menjadi tanggung
            jawab pelanggan
          </p>
        </div>
      </Infobox>
    </div>
  )
}
