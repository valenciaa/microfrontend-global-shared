import { useEffect, useState } from 'react'
import isEmpty from 'lodash/isEmpty'

import clsx from '../../utils/Clsx'
import config from '../../../../config'
import Button from '../../layouts/atoms/Button'
import CheckBoxRadioList from '../../../../src/unifyapps/components/CheckBoxRadioList'

export default function OfflineTransactionChooseBuContainer({
  isLoading,
  storeFilterData,
  onSelectBu,
}) {
  const [transformedFilterData, setTransformedFilterData] = useState([])
  const buttonDisabled = !transformedFilterData.some((e) => e.isActive)

  useEffect(() => {
    if (!isLoading && !isEmpty(storeFilterData)) {
      setTransformedFilterData(
        storeFilterData.map((e, index) => ({
          ...e,
          id: index,
          isActive: false,
        })),
      )
    }
  }, [isLoading, storeFilterData])

  const handleSelectBuOption = (selectedBuData) =>
    setTransformedFilterData((prev) =>
      prev.map((e) => ({
        ...e,
        isActive: selectedBuData.id === e.id ? !e.isActive : false,
      })),
    )

  const handleApplySelectBu = () => {
    const companyCode = transformedFilterData.find((e) => e.isActive)?.value
    onSelectBu(companyCode)
  }

  return (
    <div
      className={`padding-top-xl padding__horizontal--${
        config.environment === 'mobile' ? 'm' : 'xs'
      }`}
    >
      <p
        className={
          config.environment === 'mobile' ? 'heading-3' : 'heading-4 text-bold'
        }
      >
        Pilih Toko Fisik
      </p>
      <p className='ui-text-3 margin-top-xxs margin-bottom-xs'>
        Lihat riwayat belanjamu di toko berikut
      </p>
      {isLoading ? (
        <div className='flex flex-1 justify-center items-center'>
          <img
            src={config.assetsURL + 'images/loading-ruparupa.gif'}
            width='50px'
            height='50px'
            alt='loading'
          />
        </div>
      ) : (
        <CheckBoxRadioList
          groupName='bu'
          data={transformedFilterData}
          type='radio'
          onChange={handleSelectBuOption}
        />
      )}
      <Button
        type='primary'
        size={config.environment === 'mobile' ? 'medium' : 'big'}
        additionalClass={clsx({ disabled: buttonDisabled }, 'margin-top-xl')}
        handleOnClick={() => !buttonDisabled && handleApplySelectBu()}
      >
        Pilih Toko
      </Button>
    </div>
  )
}
