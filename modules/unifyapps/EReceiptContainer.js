import isEmpty from 'lodash/isEmpty'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import config from '../../../../config'
import {
  useGetOfflineInstallationReceipt,
  useGetTransactionDetail,
} from '../../services/api/queries/unifyapps'

export default function EReceiptContainer() {
  const router = useRouter()
  const [params, setParams] = useState()
  const isMobile = config.environment === 'mobile'
  const isInstallationOffline =
    params?.transactionType === 'installasi-offline' ? true : false
  const { data: transactionDetailOnline, refetch: refetchOnline } =
    useGetTransactionDetail({ method: 'print', ...params }, { enabled: false })

  const { data: installationDetailOffine, refetch: refetchOffline } =
    useGetOfflineInstallationReceipt({ ...params }, { enabled: false })

  useEffect(() => {
    if (isEmpty(params)) {
      return
    }

    if (params.transactionType === 'installasi-offline') {
      refetchOffline()
      return
    }

    refetchOnline()
  }, [params])

  useEffect(() => {
    if (router.isReady) {
      const query = router.query
      const newParams = {
        transactionNo: query?.transaction_no,
        transactionType: query?.type,
        companyCode: query?.company_code,
      }
      setParams(newParams)
    }
  }, [router.isReady, router.query])

  return (
    <pre
      style={{ overflow: 'scroll' }}
      className={`container-card justify-center flex ${
        isMobile ? 'ui-text-4' : 'ui-text-2'
      }`}
    >
      {isInstallationOffline
        ? installationDetailOffine?.attachment_digital_receipt
        : transactionDetailOnline?.e_receipt}
    </pre>
  )
}
