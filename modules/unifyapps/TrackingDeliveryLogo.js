import React, { useState } from 'react'
import Button from '../../layouts/atoms/Button'
import FloatingInfobox from '../../layouts/atoms/FloatingInfobox'
import isEmpty from 'lodash/isEmpty'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import config from '../../../../config'

export default function TrackingDeliveryLogo({ splitDelivery }) {
  const [toggleClipboardInfobox, setToggleClipboardInfobox] = useState(false)
  const { state: detailTransactionContext } = useDetailTransactionContext()
  const orderData = detailTransactionContext.results
  const isOnline = detailTransactionContext?.isOnline
  const shipment = orderData?.shipment
  const isDesktop = config.environment === 'desktop'
  // show driver name in instant/sameday and ownfleet carrier other than shopee and tokopedia
  const carrierCode = shipment?.carrier?.carrier_code
  const showDriverName =
    ((carrierCode?.includes('gosend') || carrierCode?.includes('grab')) &&
      (carrierCode?.includes('instant') || carrierCode?.includes('sameday')) &&
      !carrierCode?.includes('shopee') &&
      !carrierCode.includes('tokopedia')) ||
    carrierCode?.includes('ownfleet_informa') ||
    carrierCode?.includes('ownfleet_ace')
  const includesOwnfleet =
    !isEmpty(carrierCode) &&
    ['ownfleet_informa', 'ownfleet_ace'].includes(carrierCode)

  let shipmentStatus
  if (!isEmpty(splitDelivery)) {
    shipmentStatus = splitDelivery
  } else if (!isEmpty(shipment?.shipment_tracking)) {
    shipment?.shipment_tracking.map((i) => (shipmentStatus = i.status))
  }

  const manifestData = shipmentStatus?.manifest?.[0]
  const infoBoxForOfflineOrder =
    isOnline || (!isOnline && ['4a', '4b'].includes(manifestData?.status_code))

  // Untuk Tracking Offline, request dari user untuk mengganti logo dengan ownfleet yg lebih universal tidak bergantung pada BU
  const universalOwnfleetLogo =
    'https://cdn.ruparupa.io/media/promotion/ruparupa/own-fleet-icon-01.svg'
  const shipmentLogoUrl =
    !isOnline && includesOwnfleet
      ? universalOwnfleetLogo
      : shipment?.carrier?.logo_url

  const copyToClipboardSnackbar = () => {
    navigator.clipboard.writeText(shipment?.track_number)
    setToggleClipboardInfobox(true)
  }

  const handleLiveTrack = () => {
    mixpanelTrack('Lihat Live Tracking', {
      'Courier Name': manifestData?.courier_name || 'None',
      'Order No': orderData?.sales_order?.order_no || 'None',
      'Invoice No': orderData?.invoice_no || 'None',
      'Store Code': orderData?.store?.store_code || 'None',
      'Store Name': orderData?.store?.name || 'None',
      'Driver Name': manifestData?.driver_info?.driver_name || 'None',
    })

    window.location.href = manifestData.live_tracking
  }

  const handleButton = () => {
    if (
      !isEmpty(manifestData.live_tracking) ||
      !manifestData?.courier_name?.includes('Ownfleet')
    ) {
      return (
        <Button
          type='primary-border'
          size='medium'
          width={isDesktop ? '30%' : '50%'}
          additionalClass='items-center'
          handleOnClick={() =>
            !isEmpty(manifestData.live_tracking)
              ? handleLiveTrack()
              : copyToClipboardSnackbar()
          }
        >
          <div className='ui-text-3 text-semi-bold'>
            {!isEmpty(manifestData.live_tracking) ? 'Live Tracking' : 'Salin'}
          </div>
        </Button>
      )
    }
    return null
  }

  if (
    shipment?.track_number &&
    !shipment?.track_number?.includes('ODI-') &&
    !shipment?.track_number?.includes('BC-') &&
    !(includesOwnfleet && ['SR', 'UT'].includes(manifestData?.status_code)) &&
    infoBoxForOfflineOrder
  ) {
    return (
      <div
        className={`flex row ${
          isDesktop ? 'padding-top-s' : 'padding-xs'
        } padding-bottom-none`}
      >
        <div className='flex col padding-s bg-grey-10 border-grey-20 justify-between items-center'>
          <div className='row items-center'>
            {/* logo kurir */}
            <img
              src={shipmentLogoUrl}
              style={{ objectFit: 'contain' }}
              alt='Logo'
              height='50'
              width='50'
            />
            <div className='col padding__horizontal--xs'>
              <div className='ui-text-3 color-grey-100'>
                {!isEmpty(manifestData?.driver_info?.driver_name) &
                showDriverName
                  ? manifestData?.driver_info?.driver_name
                  : 'No. Resi'}
              </div>
              {/* show track number */}
              {!manifestData.courier_name.includes('Ownfleet') && (
                <div className='ui-text-3 color-grey-40 margin-top-xxs'>
                  {shipment?.track_number}
                </div>
              )}
            </div>
          </div>
          {handleButton()}
        </div>
        {toggleClipboardInfobox && (
          <FloatingInfobox
            duration={2500}
            toggleFlag={toggleClipboardInfobox}
            type='info'
            setToggle={setToggleClipboardInfobox}
            additionalButton={{ text: 'Tutup' }}
            additionalButtonClose
          >
            Berhasil Tersalin
          </FloatingInfobox>
        )}
      </div>
    )
  } else {
    return null
  }
}
