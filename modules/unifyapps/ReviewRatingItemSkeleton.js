import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import ReviewRatingItemStyle from '../../scss/5-modules/unifyapps/_review-rating-item.module.scss'

export default function ReviewRatingItemSkeleton({ isReviewed }) {
  const storeImageSize = config.environment === 'mobile' ? 36 : 60
  const productImageSize = config.environment === 'mobile' ? 68 : 60

  return (
    <div
      className={`${
        ReviewRatingItemStyle[`review-rating-item__card--${config.environment}`]
      } ${
        config.environment === 'mobile' ? 'margin-top-s' : 'margin-bottom-xl'
      }`}
    >
      <div
        className={`flex justify-between items-center bg-grey-10 ${
          config.environment === 'mobile'
            ? 'padding-xs'
            : 'padding__horizontal--m padding__vertical--xs'
        }`}
      >
        <Skeleton height={12} width={100} />
        <Skeleton height={12} width={100} />
      </div>
      <div
        className={`padding__horizontal--m${
          config.environment === 'mobile' ? '' : ' padding-bottom-xs'
        }`}
      >
        {!isReviewed ? (
          <div
            className={`flex margin-top-xs ${
              config.environment === 'mobile'
                ? 'margin-bottom-s'
                : 'margin-bottom-xs items-center'
            }`}
          >
            <Skeleton
              className={`${
                ReviewRatingItemStyle[
                  `review-rating-item__store-image-${config.environment}`
                ]
              } ${
                config.environment === 'mobile'
                  ? 'margin-right-xs'
                  : 'margin-right-m'
              }`}
              height={storeImageSize}
              width={storeImageSize}
            />
            <div>
              <Skeleton height={12} width={100} />
              <Skeleton height={16} width={200} />
            </div>
          </div>
        ) : (
          <></>
        )}
        <div
          className={`flex flex-1 ${
            config.environment === 'mobile'
              ? 'padding__vertical--s'
              : 'padding__vertical--xs'
          } ${
            !isReviewed
              ? ReviewRatingItemStyle[
                  `review-rating-item__product-${config.environment}`
                ]
              : ''
          }`}
        >
          <Skeleton
            className={`margin-right-m ${ReviewRatingItemStyle['review-rating-item__product-image']}`}
            height={productImageSize}
            width={productImageSize}
          />
          <div
            className={`flex-1 ${
              config.environment === 'mobile' ? 'flex-col' : 'flex items-center'
            }`}
          >
            <div
              className={`flex-col flex-1 justify-between${
                config.environment === 'mobile' ? '' : ' margin-right-m'
              }`}
            >
              <Skeleton height={14} width='100%' />
              <Skeleton height={14} width={100} />
              {isReviewed ? (
                <div
                  className={`flex margin-bottom-xs color-white ${ReviewRatingItemStyle['review-rating-item__product-star']}`}
                >
                  <Skeleton
                    height={14}
                    width={14}
                    className='margin-right-xs'
                  />
                  <Skeleton
                    height={14}
                    width={14}
                    className='margin-right-xs'
                  />
                  <Skeleton
                    height={14}
                    width={14}
                    className='margin-right-xs'
                  />
                  <Skeleton
                    height={14}
                    width={14}
                    className='margin-right-xs'
                  />
                  <Skeleton
                    height={14}
                    width={14}
                    className='margin-right-xs'
                  />
                </div>
              ) : (
                <></>
              )}
            </div>
            <div>
              <Skeleton
                height={28}
                width={config.environment === 'mobile' ? '100%' : 100}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
