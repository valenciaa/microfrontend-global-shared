import React from 'react'
import Dots, { Line } from '../../layouts/atoms/Dots'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import isEmpty from 'lodash/isEmpty'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'
import dayjs from 'dayjs'
import 'dayjs/locale/id'

dayjs.locale('id')

export default function VerticalProgressTrackStatus({
  shipmentTracking,
  isInstallation,
  statusInstallation,
  isOnline,
}) {
  const unifyBaseStyle = useUnifyAppsContext().unifyBaseStyles
  const { state: detailTransactionContext } = useDetailTransactionContext()

  const renderOwnfleet = (manifest) => {
    const estStart = manifest?.eta_sent?.start
    const estEnd = manifest?.eta_sent?.end
    const rcvTime = manifest?.received_time
    const labelStatus = [
      {
        status: '2',
        details: [],
      },
      {
        status: '4a',
        details: [
          {
            label: 'Armada',
            value: manifest?.driver_info?.vehicle_number || '-',
          },
          { label: 'Driver', value: manifest?.driver_info?.driver_name || '-' },
          ...(estStart === '00:00' && estEnd === '00:00'
            ? []
            : [
                {
                  label: 'Estimasi Tiba',
                  value: `${estStart || '-'} - ${estEnd || '-'} WIB`,
                },
              ]),
        ],
      },
      {
        status: '4b',
        details: [
          {
            label: 'Armada',
            value: manifest?.driver_info?.vehicle_number || '-',
          },
          { label: 'Driver', value: manifest?.driver_info?.driver_name || '-' },
          {
            label: 'Estimasi Tiba',
            value: `${estStart || '-'} - ${estEnd || '-'} WIB`,
          },
        ],
      },
      {
        status: '5',
        details: [
          {
            label: 'Nama Penerima',
            value: shipmentTracking?.info?.shipping_to || '-',
          },
          { label: 'Waktu Diterima', value: rcvTime || '-' },
        ],
      },
    ]

    return (
      <>
        {labelStatus.map((status, index) => {
          if (manifest?.status_code === status.status) {
            return (
              <div key={index}>
                <div className='row flex-wrap'>
                  <div className='flex-wrap'>
                    {status.details.map((detail, i) => (
                      <div key={i}>
                        <span className={unifyBaseStyle['label-status']}>
                          {detail.label}
                        </span>
                        <span>: {detail.value}</span>
                      </div>
                    ))}
                  </div>
                </div>
                <p
                  className='margin-top-xxs'
                  style={{ whiteSpace: 'pre-wrap', wordBreak: 'keep-all' }}
                >
                  {manifest?.disclaimer}
                </p>
                {!isEmpty(manifest?.live_tracking) &&
                  manifest.status_code === '4b' && (
                    <a
                      onClick={() => {
                        mixpanelTrack('Lihat Live Tracking', {
                          'Courier Name': manifest?.courier_name || 'None',
                          'Order No':
                            detailTransactionContext?.results?.sales_order
                              ?.order_no || 'None',
                          'Invoice No':
                            detailTransactionContext?.results?.invoice_no ||
                            'None',
                          'Store Code':
                            detailTransactionContext?.results?.store
                              ?.store_code || 'None',
                          'Store Name':
                            detailTransactionContext?.results?.store?.name ||
                            'None',
                          'Driver Name':
                            manifest?.driver_info?.driver_name || 'None',
                        })
                      }}
                      href={manifest.live_tracking}
                      className='margin-top-xxs text-semi-bold color-ruparupa'
                    >
                      Lihat Live Tracking
                    </a>
                  )}
                {!isOnline &&
                  manifest?.url_image &&
                  manifest?.status_code === '5' && (
                    <a
                      href={manifest.url_image}
                      className='margin-top-xxs text-semi-bold color-ruparupa'
                    >
                      Lihat Bukti Pengiriman
                    </a>
                  )}
              </div>
            )
          } else {
            return null
          }
        })}
      </>
    )
  }

  const renderProgressBarInstallation = (index, status, activeStatus) => {
    let deactive = true
    let completed = false

    if (status === activeStatus && index === 0) {
      deactive = false
    }

    if (['completed', 'canceled'].includes(status)) {
      completed = true
    }

    return (
      <>
        <Dots className='dots_icon' deactive={deactive} completed={completed} />
        {index !== shipmentTracking?.length - 1 && (
          <Line deactive={deactive} completed={completed} />
        )}
      </>
    )
  }

  if (isInstallation) {
    return (
      <div>
        {shipmentTracking?.map((installationStatus, index) => {
          return (
            <div
              className='container_vertical_progress'
              key={`installation-status-${index}`}
            >
              <div className='container_progress_bar'>
                {renderProgressBarInstallation(
                  index,
                  installationStatus?.status,
                  statusInstallation,
                )}
              </div>
              <div className='flex-col margin-left-xs padding-bottom-s'>
                <p className='ui-text-3 color-grey-40'>
                  {installationStatus?.date}
                </p>
                <div className='margin-top-xxs'>
                  <b className='content_progress ui-text-3 color-grey-100'>
                    {installationStatus?.history_status}
                  </b>
                  <p
                    className='content_progress ui-text-3 color-grey-40'
                    dangerouslySetInnerHTML={{
                      __html: installationStatus?.detail,
                    }}
                  ></p>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  // Non Installation
  const tracking = shipmentTracking?.manifest
  const splitTracking = detailTransactionContext?.splitData?.manifest ?? null
  const mergedTracking = splitTracking ? splitTracking : tracking
  return (
    <div>
      {mergedTracking?.map((manifest, index) => {
        return (
          <div
            className='container_vertical_progress'
            key={`manifest-${index}`}
          >
            <div className='container_progress_bar'>
              <Dots className='dots_icon' />
              {index !== mergedTracking?.length - 1 && <Line />}
            </div>
            <div className='flex-col margin-left-xs padding-bottom-s'>
              <p className='ui-text-4 color-grey-40'>
                {isEmpty(manifest.courier_name)
                  ? 'ruparupa'
                  : manifest?.courier_name.includes('Ownfleet') && !isOnline
                    ? 'Ownfleet'
                    : `${manifest.courier_name}`}{' '}
                - {manifest?.manifest_date}
              </p>
              <div className='margin-top-xxs'>
                <p className='content_progress ui-text-3 color-grey-100'>
                  {manifest?.status_code === 'UT'
                    ? `Pesanan anda akan dikirimkan ulang pada ${dayjs(
                        manifest?.delivery_date,
                      ).format('dddd, DD MMMM YYYY')}`
                    : manifest?.keterangan}
                  {['2', '4a', '4b', '5'].includes(manifest?.status_code) &&
                    (isOnline ||
                      (!isOnline &&
                        shipmentTracking?.info?.pickup_at_store !== 'Y')) &&
                    renderOwnfleet(manifest)}
                </p>
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}
