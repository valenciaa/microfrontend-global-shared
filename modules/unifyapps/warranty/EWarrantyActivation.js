import React, { useEffect, useState } from 'react'
import { WarrantyItemList } from './WarrantyItemList'
import { useUnifyAppsUpdateContext } from '../../../context/UnifyAppsContext'
import { useForm } from 'react-hook-form'
import { ActivateWarrantyForm } from './ActivateWarrantyForm'
import { useWarrantyActivation } from '../../../services/api/mutations/unifyapps'
import { warrantyDataModifier } from '../../../utils/warrantyDataModifier'
import { useRouter } from 'next/router'
// import { useGetUserProfile } from '../../../services/api/queries/global'
// import LabelSection from '../../components/LabelSection'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import capitalize from 'lodash/capitalize'
import isEmpty from 'lodash/isEmpty'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../../config'

export const EwarrantyActivation = ({ refetch = () => {}, customerID }) => {
  const unifyUpdateContext = useUnifyAppsUpdateContext()
  const [itemData, setItemData] = useState([])
  const [errorMessage, setErrorMessage] = useState('')
  // const [customerID, setCustomerID] = useState('')
  const router = useRouter()

  const setItem = async () => {
    const localState = await JSON.parse(
      localStorage.getItem('item-select-warranty'),
    ) // eslint-disable-line
    setItemData(localState)
  }

  useEffect(() => {
    unifyUpdateContext.setDisabledBaseFooter(true)
    unifyUpdateContext.setIsChatScrollBypass(true)
    setItem()
  }, [])

  const [warrantyCardActive, setWarrantyCardActive] = useState(false)

  const { mutate: activateWarranty, isLoading } = useWarrantyActivation({
    onSuccess: (res) => {
      if (res?.data?.message === 'failed' || res?.data?.error === true) {
        mixpanelTrack('Gagal Aktivasi Garansi', {
          Reason: `${
            warrantyCardActive ? 'Nomor kartu garansi' : 'Serial number'
          } sudah digunakan`,
        })
        return setErrorMessage(
          `${
            warrantyCardActive ? 'Nomor kartu garansi' : 'Serial number'
          } sudah digunakan`,
        )
        // return setErrorMessage(res?.data?.errors?.messages || res?.data?.message)
      }
      if (config.environment !== 'desktop') {
        return router.back()
      }
      return refetch()
    },
    onError: (res) => {
      mixpanelTrack('Gagal Aktivasi Garansi', {
        Reason: res?.data?.errors?.messages,
      })
      setErrorMessage(res?.data?.errors?.messages)
    },
  })

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    defaultValues: {
      serial_number: '',
      warranty_card: '',
    },
  })

  const onSubmit = async (data) => {
    try {
      mixpanelTrack('Click Aktifkan Garansi', {
        'Item Name': itemData?.sku_name,
        'Item ID': itemData?.sku_number,
        'Serial Number': data.serial_number || 'none',
        'Nomor Garansi': data.warranty_card || 'none',
        'Transaction': capitalize(router?.query?.type),
      })
      activateWarranty({
        transactionType: router?.query?.type === 'offline' ? 'offline' : '',
        data: warrantyDataModifier(
          data,
          customerID,
          warrantyCardActive,
          router?.query?.type,
          itemData,
        ),
      })
    } catch (e) {
      console.log('err', e)
    }
  }

  return (
    <>
      <div className='flex row'>
        <div className='padding-m padding-bottom-none'>
          <p className='ui-text-3'>
            Kamu akan mengaktifkan garansi untuk produk berikut
          </p>
        </div>
        <WarrantyItemList data={itemData} type='activation' />
        {isEmpty(itemData) && (
          <div className='padding-s'>
            <Skeleton height={56} width={60} />
          </div>
        )}
        <ActivateWarrantyForm
          onSubmit={onSubmit}
          register={register}
          handleSubmit={handleSubmit}
          warrantyBool={warrantyCardActive}
          setWarrantyBool={setWarrantyCardActive}
          setValue={setValue}
          errors={errors}
          errorAfterPost={errorMessage}
          setErrorAfterPost={setErrorMessage}
          isLoadingFetch={isLoading}
        />
      </div>
    </>
  )
}
