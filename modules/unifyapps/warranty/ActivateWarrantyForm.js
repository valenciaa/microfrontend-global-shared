import React, { useEffect, useState } from 'react'
// import { useUnifyAppsContext } from '../../../context/UnifyAppsContext'
import Button from '../../../layouts/atoms/Button'
import {
  warrantySerialErrorHandler,
  warrantyCardErrorHandler,
  warrantyRequiredHandler,
  postErrorHandler,
} from '../../../utils/warrantyFormConditionalHandler'
import config from '../../../../../config'
import isEmpty from 'lodash/isEmpty'
import WarrantyBarStyle from '../../../scss/5-modules/unifyapps/_warranty-bar.module.scss'

export const ActivateWarrantyForm = ({
  onSubmit,
  register,
  handleSubmit,
  warrantyBool,
  setWarrantyBool,
  setValue,
  errors,
  errorAfterPost,
  setErrorAfterPost,
  isLoadingFetch,
}) => {
  //   const unifyStyle = useUnifyAppsContext().unifyBaseStyles
  const [formEmpty, setFormEmpty] = useState(true)

  useEffect(() => {
    setErrorAfterPost(errorAfterPost)
  }, [errorAfterPost])

  const envImage = config.environment === 'desktop' ? 'images' : 'icon'

  const mobileViewText =
    config.environment !== 'desktop' ? 'ui-text-3 color-grey-40' : 'ui-text-4'

  return (
    <div className='padding-s w-100'>
      <form onSubmit={handleSubmit(onSubmit)} autoComplete='off'>
        <p className={`${mobileViewText}`}>Masukkan Serial Number</p>
        <input
          {...register('serial_number', warrantyRequiredHandler(warrantyBool))}
          type='text'
          placeholder='Contoh: 1234567890'
          //   className={`${unifyStyle['warranty-input-bar']} ${errors.serial_number ? unifyStyle['warranty-input-bar__error'] : ''}`}
          className={`${WarrantyBarStyle['warranty-input-bar']} ${
            errors.serial_number
              ? WarrantyBarStyle['warranty-input-bar__error']
              : ''
          }`}
          disabled={warrantyBool}
          aria-invalid={errors.serial_number ? 'true' : 'false'}
          onChange={(e) => {
            isEmpty(e.currentTarget.value)
              ? setFormEmpty(true)
              : setFormEmpty(false)
          }}
        />
        {warrantySerialErrorHandler(errors)}
        {errorAfterPost !== '' && !warrantyBool
          ? postErrorHandler(errorAfterPost)
          : null}
        <div>
          <span className={`${mobileViewText} color-grey-40`}>
            Ingin memasukkan nomor kartu garansi?
          </span>
          <Button
            size='small'
            type='link-secondary'
            width='auto'
            handleOnClick={() => {
              // what if, after they push a button 'klik di sini', it'll wipe out the field vice versa
              !warrantyBool
                ? setValue('serial_number', '')
                : setValue('warranty_card', '')
              // and after that, it changes
              setWarrantyBool(!warrantyBool)
              setFormEmpty(true)
            }}
            additionalClass={`padding-left-s padding__horizontal--none padding__vertical--none ui-text-${
              config?.environment === 'desktop' ? '4' : '3'
            } text-semi-bold`}
          >
            Klik di sini &#9662;
          </Button>
        </div>
        {warrantyBool && (
          <>
            <input
              {...register('warranty_card', { required: true })}
              type='text'
              placeholder='Contoh: ABC12345'
              //  className={`${unifyStyle['warranty-input-bar']} ${errors.warranty_card ? unifyStyle['warranty-input-bar__error'] : ''}`}
              className={`${WarrantyBarStyle['warranty-input-bar']} ${
                errors.warranty_card
                  ? WarrantyBarStyle['warranty-input-bar__error']
                  : ''
              }`}
              aria-invalid={errors.warranty_card ? 'true' : 'false'}
              onChange={(e) => {
                isEmpty(e.currentTarget.value)
                  ? setFormEmpty(true)
                  : setFormEmpty(false)
              }}
            />
            {warrantyCardErrorHandler(errors)}
            {errorAfterPost !== '' && warrantyBool
              ? postErrorHandler(errorAfterPost)
              : null}
          </>
        )}
        <Button
          size='medium'
          type='primary'
          width='100%'
          isSubmit
          fetching={isLoadingFetch || formEmpty}
          additionalClass='text-semi-bold margin-top-s'
        >
          {isLoadingFetch ? (
            <img
              src={config.assetsURL + `${envImage}/loading-ruparupa.gif`}
              width='14px'
              height='14px'
              alt='loading'
            />
          ) : (
            'Aktifkan Garansi'
          )}
        </Button>
      </form>
      <p className={`${mobileViewText} text-center margin-top-l`}>
        Butuh bantuan lebih lanjut? Silahkan hubungi customer service kami&nbsp;
        <Button
          size='small'
          type='link-primary-transparent'
          width='auto'
          handleOnClick={() => {
            if (!config.activeLenna) {
              if (
                window.fcWidget &&
                typeof window.fcWidget.open === 'function'
              ) {
                window.fcWidget.open()
              }
            } else {
              if (document?.querySelector('.sc-launcher')) {
                // launch Lenna while click icon web chat
                document.querySelector('.sc-launcher').click()
              }
            }
          }}
          additionalClass={`padding__horizontal--none padding__vertical--none ui-text-${
            config?.environment === 'desktop' ? '4' : '3'
          } text-semi-bold`}
        >
          di sini!
        </Button>
      </p>
    </div>
  )
}
