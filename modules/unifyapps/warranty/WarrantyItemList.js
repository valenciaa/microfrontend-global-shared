import React from 'react'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import config from '../../../../../config'
import Skeleton from 'react-loading-skeleton'
import Button from '../../../layouts/atoms/Button'
// import { useUnifyAppsContext } from '../../../context/UnifyAppsContext'
import WarrantyListStyle from '../../../scss/5-modules/unifyapps/_ewarranty-product-container.module.scss'
import { useRouter } from 'next/router'
import {
  buttonTypes,
  buttonTextTypes,
  buttonStatusChecker,
} from '../../../utils/warrantyButtonSwitcher'
import { mixpanelTrack } from '../../../utils/MixpanelWrapper'
import capitalize from 'lodash/capitalize'
import isEmpty from 'lodash/isEmpty'

export const WarrantyItemList = ({
  data,
  type,
  transType,
  buttonHandlerChecker,
}) => {
  // const unifyStyle = useUnifyAppsContext().unifyBaseStyles
  // const unifyStyle = WarrantyListStyle
  const router = useRouter()

  const routeWarper = async () => {
    if (buttonHandlerChecker() === true) {
      return false
    }
    mixpanelTrack('Click Detail Garansi', {
      'Status Garansi': capitalize(buttonStatusChecker(data)),
      'Item Name': data?.sku_name,
      'Transaction': capitalize(router?.query?.type),
    })
    if (buttonStatusChecker(data) === 'activation') {
      localStorage.setItem('item-select-warranty', JSON.stringify(data)) // eslint-disable-line
      const url = `/my-account/warranty/activation?type=${transType}`
      router.push(url)
    } else if (buttonStatusChecker(data) === 'active') {
      localStorage.setItem('item-detail-warranty', JSON.stringify(data)) // eslint-disable-line
      const url = `/my-account/warranty/detail?warranty_id=${data?.ace_warranty_id}` // ACE_HARDWARE
      router.push(url)
    }
  }

  const statusArea = () => {
    return (
      <div
        className={`${WarrantyListStyle['ewarranty-status']} text-right col-xs-3 padding-xs`}
      >
        <p
          className={`ui-text-${config?.environment === 'desktop' ? '5' : '4'}`}
        >
          Status Garansi
        </p>
        <p
          className={`${WarrantyListStyle['ewarranty-status__active-text']} ui-text-3`}
        >
          AKTIF
        </p>
      </div>
    )
  }

  const buttonArea = () => {
    return (
      <div className='col-md-3'>
        <Button
          size='small'
          type={buttonTypes[buttonStatusChecker(data)]}
          width='100%'
          handleOnClick={async () => {
            await routeWarper()
          }}
        >
          {buttonTextTypes[buttonStatusChecker(data)]}
        </Button>
      </div>
    )
  }

  const imageChecker = type !== 'button' ? 60 : 40

  const rightPartDecision = {
    button: buttonArea(),
    status: statusArea(),
    activation: null,
  }

  let imageUrl = !isEmpty(data?.sku_image)
    ? config.imageURL + data?.sku_image
    : 'static/images/no-image.jpg'

  if (data?.full_image_url) {
    imageUrl = data?.full_image_url
  }
  return (
    <div className='flex row'>
      {/* double div avoiding padding not covering the line break */}
      {!isEmpty(data?.sku_number) && (
        <>
          <div className='flex padding-s col justify-between items-center'>
            {/* left part: image, product name & sku */}
            <div className='row items-center'>
              <CustomLazyLoadImage
                src={imageUrl}
                alt='Logo'
                height={imageChecker}
                width={imageChecker}
                placeholder={<Skeleton />}
              />
              <div className='col padding__horizontal--xs'>
                <p className='ui-text-3'>{data?.sku_name || '-'}</p>
                <p className='ui-text-4 color-grey-40'>
                  SKU: {data?.sku_number}
                </p>
              </div>
            </div>
            {/* right part */}
            {rightPartDecision[type]}
          </div>
          <div
            className={`${
              WarrantyListStyle[
                `ewarranty-list__bottom-break${
                  config?.environment === 'desktop' ? '-desktop' : ''
                }`
              ]
            }`}
          />
        </>
      )}
    </div>
  )
}
