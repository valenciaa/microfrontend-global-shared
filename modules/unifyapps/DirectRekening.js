import React from 'react'
import Infobox from '../../layouts/atoms/Infobox'
import InfoboxInfo from '../../../../public/static/icon/infobox-info-16.svg'
import Button from '../../layouts/atoms/Button'
import config from '../../../../config'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import isEmpty from 'lodash/isEmpty'

export default function DirectRekening() {
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const orderData = detailTransactionContext.results

  const { refund: refundData, refund_dc: refundDataDc, shipment } = orderData
  const customer = orderData?.sales_order?.customer

  const showOption =
    orderData.payment &&
    (orderData.payment.method === 'qris' ||
      orderData.payment.method === 'ovo' ||
      orderData.payment.type === 'debit_card' ||
      orderData.payment.type === 'bank_transfer')
  const isContainAccount =
    customer.is_already_have_rekening && customer.is_already_default_rekening
  const isContainRefund = shipment.is_contain_refund
  const isContainReorder =
    shipment.reorder &&
    !isEmpty(shipment.reorder) &&
    shipment.reorder.status === 'customer_confirmation'
  const isContainVoucher =
    (!isEmpty(refundData) &&
      refundData.some((data) => data.invoice_no === orderData.invoice_no)) ||
    (!isEmpty(refundDataDc) &&
      refundDataDc.some((data) => data.invoice_no === orderData.invoice_no))
  const isContainActiveVoucher =
    isContainVoucher &&
    !isEmpty(refundData) &&
    refundData.some((data) =>
      data.invoice_no === orderData.invoice_no
        ? data?.voucher_product_status === '10'
        : false,
    )
  const isContainActiveVoucherDc =
    isContainVoucher &&
    !isEmpty(refundDataDc) &&
    refundDataDc.some((data) =>
      data.invoice_no === orderData.invoice_no
        ? data?.voucher_product_status.toString() === '10'
        : false,
    )
  const isContainBothVoucher =
    isContainVoucher &&
    ((!isEmpty(refundData) &&
      refundData.some((data) =>
        data.invoice_no === orderData.invoice_no
          ? ['7', '10'].includes(data.voucher_product_status)
          : false,
      )) ||
      (!isEmpty(refundDataDc) &&
        refundDataDc.some((data) =>
          data.invoice_no === orderData.invoice_no
            ? ['7', '10'].includes(data.voucher_product_status)
            : false,
        )))
  const isVRT =
    isContainVoucher &&
    (refundData.filter((data) =>
      data.invoice_no === orderData.invoice_no
        ? data.voucher_product?.includes('VRT')
        : false,
    ) ||
      refundDataDc.filter((data) =>
        data.invoice_no === orderData.invoice_no
          ? data.voucher_product?.includes('VRT')
          : false,
      ))
  const isVRC =
    isContainVoucher &&
    (refundData.filter((data) =>
      data.invoice_no === orderData.invoice_no
        ? data.voucher_product?.includes('VRC')
        : false,
    ) ||
      refundDataDc.filter((data) =>
        data.invoice_no === orderData.invoice_no
          ? data.voucher_product?.includes('VRC')
          : false,
      ))
  const isContainVRT =
    !isEmpty(isVRT) &&
    isVRT.filter((data) => data.voucher_product.includes('VRT')).length > 0
  const isContainVRC =
    !isEmpty(isVRC) &&
    isVRC.filter((data) => data.voucher_product.includes('VRC')).length > 0
  // const isVRTDC = isContainVoucher && (refundDataDc.filter(data => data.invoice_no === orderData.invoice_no ? data.voucher_product?.includes('VRT') : false))
  const isVRCDC =
    isContainVoucher &&
    refundDataDc?.filter((data) =>
      data.invoice_no === orderData.invoice_no
        ? data.voucher_product?.includes('VRC')
        : false,
    )
  const isContainNotRefundedRdrItem =
    refundDataDc &&
    refundDataDc?.filter((data) => data.status === 'waiting_for_rekening')
  const isContainVRCNeedOptions =
    !isEmpty(isVRCDC) &&
    isVRCDC.filter(
      (data) =>
        data.voucher_product.includes('VRC') &&
        data.voucher_product_status?.toString() === '7',
    ).length > 0
  const checkButtonConditionVoucher =
    (isContainVRT && isContainVRC) ||
    (!isEmpty(isContainNotRefundedRdrItem) && isContainVRCNeedOptions)

  const handleButton = (textButton = '', onClick) => {
    return (
      <Button
        type='secondary'
        size='medium'
        handleOnClick={onClick}
        additionalClass='margin-top-m'
      >
        <div className='ui-text-3 text-semi-bold'>{textButton}</div>
      </Button>
    )
  }
  const handleInfoBox = (textInfo = '') => {
    return (
      <Infobox text={textInfo} theme='info' className='margin-top-s'>
        <div className='flex'>
          <InfoboxInfo className='color-blue-60' />
        </div>
      </Infobox>
    )
  }

  const openRefundHistory = () => {
    if (config.environment === 'mobile') {
      window.location.href =
        config.baseURL + 'account/refund-history?limit=10&offset=0'
    } else {
      window.location.href =
        config.baseURL + 'my-account?tab=my-bank-accounts&limit=10&offset=0'
    }
  }
  const openVoucherPage = () =>
    (window.location.href = config.baseURL + 'my-account/my-voucher')

  return (
    <>
      {showOption &&
        ((!isContainAccount && checkButtonConditionVoucher) ||
          (!isContainVRT &&
            isContainVRC &&
            !isContainActiveVoucher &&
            !isContainAccount)) &&
        isContainRefund &&
        !isContainReorder && (
          <div className='container-card'>
            {handleButton('Pilih Opsi Pengembalian Dana', () =>
              handleChangeState('RDRModal', 'refundOption'),
            )}
          </div>
        )}
      {((!showOption && isContainRefund) ||
        (isContainActiveVoucher && checkButtonConditionVoucher) ||
        (!isContainVRT && isContainActiveVoucher && isContainBothVoucher) ||
        !isContainActiveVoucher) &&
        isContainRefund &&
        isContainAccount &&
        !isContainReorder && (
          <div className='container-card'>
            {handleInfoBox(
              'Mohon maaf, terdapat barang Anda yang sedang dalam proses pengembalian. Pengembalian dana Anda akan diproses sesuai dengan metode pembayaran yang digunakan.',
            )}
            {handleButton('Lihat Status Pengembalian Dana', () =>
              openRefundHistory(),
            )}
          </div>
        )}
      {(isContainVRT ||
        (isContainVoucher && isContainActiveVoucher) ||
        (isContainVoucher && isContainActiveVoucherDc)) && (
        <div className='container-card'>
          {handleInfoBox(
            'Anda dapat melihat voucher pengembalian dana melalui tombol dibawah ini.',
          )}
          {handleButton('Lihat Voucher Saya', () => openVoucherPage())}
        </div>
      )}
    </>
  )
}
