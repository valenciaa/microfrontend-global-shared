import dynamic from 'next/dynamic'
import dayjs from 'dayjs'
import Skeleton from 'react-loading-skeleton'

import config from '../../../../config'
import { NumberWithCommas } from '../../utils'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import WaitingPaymentItemStyle from '../../scss/5-modules/unifyapps/_waiting-payment-item.module.scss'
import { useWaitingPaymentListContext } from '../../../../src/unifyapps/context/WaitingPaymentListContext'

const CopyIcon = dynamic(
  () => import('../../../../public/static/icon/copy-solid.svg'),
)
const ScanIcon = dynamic(
  () => import('../../../../public/static/icon/scan.svg'),
)
const Infobox = dynamic(
  () => import('../../../../shared/global/layouts/atoms/Infobox'),
)
const ExpireTimeIcon = dynamic(
  () => import('../../../../public/static/icon/expire-time.svg'),
)
const ContextMenu = dynamic(() => import('../../layouts/moleculs/ContextMenu'))

export default function WaitingPaymentItem({
  data,
  onOpenDetail,
  onCopyVaNumber,
  onShowModal,
  onCloseModal,
}) {
  const { handleChangeState } = useWaitingPaymentListContext()

  const handleOpenModal = (e, modalName) => {
    if (typeof e?.stopPropagation === 'function') {
      e.stopPropagation()
    }
    handleChangeState('selectedOrderData', data)
    onShowModal(modalName)
  }

  const handleCopyVaNumber = (e) => {
    e.stopPropagation()
    onCopyVaNumber(data)
  }

  const handleClickMenuIcon = (previousState) => {
    if (previousState) {
      onCloseModal()
    } else {
      handleOpenModal(null, 'menu')
    }
  }

  const getMenuContext = () => {
    const menus = [
      { value: 'Lihat Detail Pembayaran', onPress: () => onOpenDetail(data) },
      { value: 'Lihat Cara Bayar', onPress: () => onShowModal('howToPay') },
    ]
    if (data?.can_change_payment) {
      menus.push({
        value: 'Ubah Metode Pembayaran',
        onPress: () => onShowModal('changePayment'),
      })
    }
    if (data?.can_cancel) {
      menus.push({
        value: 'Batalkan Pesanan',
        onPress: () => onShowModal('cancelOrder'),
      })
    }
    return menus
  }

  return (
    <div
      className={`padding-m ${
        WaitingPaymentItemStyle[
          `waiting-payment-item__container--${config.environment}`
        ]
      }`}
      onClick={() => onOpenDetail(data)}
    >
      <div
        className={`flex items-center justify-between ${
          WaitingPaymentItemStyle[
            `waiting-payment-item__upper-container--${config.environment}`
          ]
        }`}
      >
        <div>
          <p className='ui-text-4 margin-bottom-xxs color-grey-50'>
            PESANAN ONLINE
          </p>
          <p className='heading-3 margin-bottom-xxs'>{data.order_no}</p>
          <p className='ui-text-4 margin-bottom-xs color-grey-40'>
            {dayjs(data.created_at).format('D MMMM YYYY')}
          </p>
        </div>
        <div className='flex items-center'>
          <CustomLazyLoadImage
            wrapperClassName='margin-right-xl'
            src={`${config.assetsURL}images/logo-bank-no-border/logo-${
              data.payment?.type === 'bank_transfer'
                ? data.payment?.va_bank
                : data.payment?.method
            }.svg`}
            alt='Gambar Bank'
            height={config.environment === 'mobile' ? 30 : 32}
            placeholder={
              <Skeleton height={config.environment === 'mobile' ? 30 : 32} />
            }
          />
          {config.environment === 'mobile' ? (
            <CustomLazyLoadImage
              wrapperClassName='cursor-pointer'
              src={`${config.assetsURL}icon/more-horizontal.svg`}
              alt='Menu Opsi'
              height={16}
              width={16}
              onClick={(e) => handleOpenModal(e, 'menu')}
            />
          ) : (
            <ContextMenu
              onClickMenuIcon={handleClickMenuIcon}
              onClickOutsideMenu={onCloseModal}
              menuList={getMenuContext()}
            >
              <CustomLazyLoadImage
                src={`${config.assetsURL}icon/more.svg`}
                alt='Menu Opsi'
                height={16}
                width={16}
              />
            </ContextMenu>
          )}
        </div>
      </div>
      <div className='flex padding__vertical--xs'>
        <div
          className={`margin-right-xs ${
            WaitingPaymentItemStyle[
              `waiting-payment-item__lower-container-left--${config.environment}`
            ]
          }`}
        >
          <div className='flex-1'>
            <p
              className={`ui-text-4 color-grey-40 ${WaitingPaymentItemStyle['margin-bottom-2']}`}
            >
              Metode Pembayaran
            </p>
            <p className='ui-text-3 margin-bottom-xs'>
              {data.payment?.label || '-'}
            </p>
          </div>
          <div className='flex-1'>
            <p
              className={`ui-text-4 color-grey-40 ${WaitingPaymentItemStyle['margin-bottom-2']}`}
            >
              Total Belanja
            </p>
            <p className='heading-3 color-grey-50 text-medium'>
              Rp{NumberWithCommas(parseInt(data.grand_total))}
            </p>
          </div>
        </div>
        {data.payment?.method === 'qris' ? (
          <div
            className={
              WaitingPaymentItemStyle[
                `waiting-payment-item__lower-container-right--${config.environment}`
              ]
            }
          >
            <p
              className={`ui-text-4 margin-bottom-xxs color-grey-40 ${WaitingPaymentItemStyle['margin-bottom-2']}`}
            >
              Kode Pembayaran
            </p>
            <div
              className='flex items-center cursor-pointer'
              onClick={(e) => handleOpenModal(e, 'qris')}
            >
              <ScanIcon className={`color-${config.companyNameCSS}`} />
              <p
                className={`button-small-text color-${config.companyNameCSS} ${WaitingPaymentItemStyle['margin-left-2']}`}
              >
                Tampilkan Kode QRIS
              </p>
            </div>
          </div>
        ) : data.payment?.va_number &&
          data.payment?.type === 'bank_transfer' ? (
          <div
            className={
              WaitingPaymentItemStyle[
                `waiting-payment-item__lower-container-right--${config.environment}`
              ]
            }
          >
            <p
              className={`ui-text-4 margin-bottom-xxs color-grey-40 ${WaitingPaymentItemStyle['margin-bottom-2']}`}
            >
              Kode Pembayaran
            </p>
            <div className='flex items-center'>
              <p
                className={`ui-text-3 margin-right-xs ${WaitingPaymentItemStyle['ellipsize-text']}`}
              >
                {data.payment.va_number}
              </p>
              <div
                className='flex items-center cursor-pointer'
                onClick={handleCopyVaNumber}
              >
                <CopyIcon className={`color-${config.companyNameCSS}`} />
                <p
                  className={`button-small-text color-${config.companyNameCSS} ${WaitingPaymentItemStyle['margin-left-2']}`}
                >
                  Salin
                </p>
              </div>
            </div>
          </div>
        ) : (
          <></>
        )}
      </div>
      {data.payment?.expire_transaction ? (
        <Infobox
          text='Selesaikan transaksi sebelum '
          subtext={`${dayjs(
            data.payment.expire_transaction,
            'YYYY-MM-DD HH:mm',
          ).format('dddd, DD MMM YYYY - HH:mm')} WIB`}
          theme='error'
          textClassName='margin-left-xs ui-text-4'
        >
          <ExpireTimeIcon />
        </Infobox>
      ) : (
        <></>
      )}
    </div>
  )
}
