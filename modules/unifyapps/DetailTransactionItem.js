import React from 'react'
import { useRouter } from 'next/router'
import config from '../../../../config'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import { NumberWithCommas } from '../../utils'
import Skeleton from 'react-loading-skeleton'
import isEmpty from 'lodash/isEmpty'
import Button from '../../layouts/atoms/Button'
import { renderStoreLogo } from '../../utils/GetItemListElement'

export default function DetailTransactionItem({
  item,
  index,
  isRefund,
  isLastProductOnPackage,
}) {
  const { name, sku, subtotal, is_warranty: isWarranty } = item
  const unifyStyle = useUnifyAppsContext().unifyBaseStyles
  const { state: detailTransactionContext } = useDetailTransactionContext()
  const orderData = detailTransactionContext?.results
  const { shipment, invoice_no: invoiceNo, sales_order: salesOrder } = orderData
  const isOnline = detailTransactionContext.isOnline
  const router = useRouter()
  const isMobile = config.environment === 'mobile'

  /* syarat isContainerReorder
    1. shipment.reorder.status = 'customer_confirmation'
    2. status_fulfillment = 'incomplete'
  */
  const isReorder =
    shipment?.reorder &&
    !isEmpty(shipment?.reorder) &&
    shipment?.reorder?.status === 'customer_confirmation' &&
    item?.status_fulfillment === 'incomplete'

  const colorTextName =
    isRefund && item?.is_free_item === 0 ? 'color-red-60' : 'color-grey-100'
  const colorText =
    isRefund && item?.is_free_item === 0 ? 'color-red-60' : 'color-grey-40'
  const titlePill = isRefund ? 'Pesanan Direfund' : 'Pesanan Dialihkan'

  const handleClickItem = () =>
    router.push(
      config.baseURL +
        `p/${item?.url_key}?itm_source=detail-transaction&itm_campaign=detai-transaction-${item?.sku}&itm_term=${item?.sku}`,
    )

  const renderItem = () => {
    let modifiedSubtotal = subtotal
    if (item?.package_id !== 0) {
      modifiedSubtotal =
        Number(item?.subtotal ?? 0) - Number(item?.discount_amount ?? 0)
    }

    const subtotalItem = `Rp${
      modifiedSubtotal?.toString().indexOf('Rp') === -1
        ? NumberWithCommas(modifiedSubtotal)
        : NumberWithCommas(modifiedSubtotal?.substr(3) * item?.qty_ordered)
    }`
    if (item?.is_free_item === 1) {
      // item gratis
      return (
        <div className={`flex-1`}>
          <div className='ui-text-3 text-semi-bold color-grey-100'>Gratis</div>
          <div className={`ui-text-3 text-left ${colorTextName}`}>
            {item?.name}
          </div>
          {item.selling_price === 'refunded' ? (
            <div className={`ui-text-4 ${colorText} margin-top-xxs`}>
              Jumlah: {item?.qty_refunded}/{item?.qty_ordered} pc(s)
            </div>
          ) : (
            <div className={`ui-text-4 ${colorText} margin-top-xxs`}>
              Jumlah: {item?.qty_ordered} pc(s)
            </div>
          )}
        </div>
      )
    } else {
      return (
        <div className={`flex-1`}>
          {(isRefund || isReorder) && (
            <div
              className={
                isRefund
                  ? unifyStyle['pill-refund']
                  : unifyStyle['pill-reorder']
              }
            >
              {titlePill}
            </div>
          )}
          <div className={`ui-text-3 text-left ${colorTextName}`}>{name}</div>
          <div className={`ui-text-4 text-left margin-top-xxs ${colorText}`}>
            SKU: {sku}
          </div>
          <div className='row items-center'>
            {isRefund ? (
              <>
                <div className='ui-text-4 text-left color-blue-60 margin-top-xxs flex-1 text-semi-bold'>
                  {item?.selling_price}
                </div>
                <div className='ui-text-4 text-right color-red-60 margin-top-xxs text-semi-bold'>
                  Jumlah: {item?.qty_refunded} / {item?.qty_ordered}
                </div>
              </>
            ) : (
              <>
                <div className='ui-text-4 text-left color-grey-40 margin-top-xxs flex-1'>
                  {item?.qty_ordered} x {subtotalItem}
                </div>

                <div className='ui-text-3 text-right color-grey-100 margin-top-xxs'>
                  {NumberWithCommas(
                    item?.package_id !== 0
                      ? subtotalItem
                      : 'Rp' + item?.row_total,
                  )}
                </div>
              </>
            )}
          </div>
          {isWarranty && (
            <div className='row item-center padding-top-xxs'>
              <div className='tw-flex ui-text-3 tw-px-1 tw-py-1 grey-10 rounded-l'>
                <CustomLazyLoadImage
                  src={config?.assetsURL + 'icon/pdp/garansi.svg'}
                  width={20}
                  height={20}
                  alt='attribute-icon'
                  placeholder={<Skeleton height={20} width={20} />}
                />
                <span className='margin-left-xxs font-size-m'>
                  Produk Bergaransi
                </span>
              </div>
            </div>
          )}
        </div>
      )
    }
  }

  const bgProductItem = isOnline
    ? `${
        isRefund
          ? 'bg-red-10 border--solid__white'
          : isReorder
            ? 'bg-grey-10'
            : item?.is_free_item === 1
              ? 'bg-blue-10'
              : ''
      }`
    : 'no-interaction'

  // styling border item product
  const lastIndex = orderData?.items?.length - index === 1
  let classNameBorder = 'border'
  if (isMobile || (!isMobile && lastIndex)) {
    classNameBorder += ' border--solid'
  } else if (!isMobile && (isRefund || isReorder || item?.is_free_item === 1)) {
    classNameBorder += ' border--solid padding__horizontal--m'
  } else {
    classNameBorder += ' border--dashed margin-left-m margin-right-m'
  }

  const handleReorderConfirmation = (e) => {
    e.stopPropagation()
    window.location.href =
      config.baseURL +
      `confirmation?invoiceId=${invoiceNo}&email=${salesOrder?.customer?.customer_email}`
  }

  const renderButtonReorder = () => {
    return (
      <div className='margin-right-m margin-top-s'>
        <Button
          type='primary'
          size='medium'
          handleOnClick={handleReorderConfirmation}
        >
          <div className='ui-text-3'>Konfirmasi Perubahan Pick Up point</div>
        </Button>
      </div>
    )
  }

  let imageUrl = item?.primary_image_url
    ? config.imageURL + 'w_170,h_170' + item?.primary_image_url
    : isOnline
      ? 'static/images/no-image.jpg'
      : config.assetsURL + renderStoreLogo(orderData?.store_name)

  if (item?.full_image_url) {
    imageUrl = item?.full_image_url
  }

  return (
    <div
      key={`detail-transaction-item-${index}`}
      onClick={handleClickItem}
      className={`${handleClickItem && 'text-value-enabled'} border
        ${
          isOnline && item.package_id !== 0 && !isLastProductOnPackage
            ? ''
            : classNameBorder
        } 
        ${bgProductItem}`}
    >
      <div className={`col-xs-12 ${isMobile && 'margin-left-m'}`}>
        <div
          className={`row ${
            !isMobile && lastIndex && 'padding__horizontal--m'
          }`}
        >
          <div className={`${isMobile ? 'col-xs-2' : 'col-xs-1'}`}>
            <CustomLazyLoadImage
              width={60}
              height={60}
              src={imageUrl}
              alt='product'
              placeholder={<Skeleton height={60} width={60} />}
            />
          </div>
          <div
            className={`items-center flex flex-1 margin-left-m ${
              isMobile && 'col-xs-9'
            }`}
          >
            {renderItem()}
          </div>
        </div>

        {/* Take Out [STD-957] */}
        {/* {(isRefund || isReorder) &&
          <Infobox
            text={isRefund ? 'Mohon maaf, barang ini tidak dapat diproses lebih lanjut.' : 'Pesanan untuk barang ini dialihkan ke toko lain'}
            theme='info'
            className='margin-top-m margin-right-m'
          >
            <div className='flex'>
              <InfoboxInfo className='color-blue-60' />
            </div>
          </Infobox>} */}
        {isReorder && renderButtonReorder()}
        {item?.customer_note && (
          <div
            className={`padding__horizontal--m ui-text-4 color-grey-40 margin-top-xxs ${colorText}`}
          >
            {item?.customer_note}
          </div>
        )}
      </div>
    </div>
  )
}
