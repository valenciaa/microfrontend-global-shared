import isEmpty from 'lodash/isEmpty'
import router from 'next/router'
import React, { useEffect, useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import Button from '../../../../shared/global/layouts/atoms/Button'
import CustomLazyLoadImage from '../../../../shared/global/layouts/atoms/CustomLazyLoadImage'
import NotFoundStyle from '../../scss/5-modules/unifyapps/_not-found.module.scss'

export default function NotFoundComponent({
  type,
  useFilter,
  searchQuery,
  data,
  changeFocus,
  showFilterModal,
  onOpenSelectBu,
}) {
  const [pageType] = useState(type)
  const [buttonText, setButtonText] = useState('')
  const [titleText, setTitleText] = useState('')
  const [descriptionText, setDescriptionText] = useState('')

  const typeOrganizer = () => {
    switch (pageType) {
      case 'installation':
        setTitleText('Tidak ada pesanan Jasa Perakitan')
        break
      case 'installation-detail':
        setTitleText('Tidak ada pesanan Jasa Perakitan')
        setButtonText('Kembali ke List')
        setDescriptionText(
          'Kamu tidak memiliki Jasa Perakitan dengan nomor ini.',
        )
        break
      case 'installation-errors':
        setTitleText('Terjadi kesalahan, mohon mencoba sesaat lagi')
        break
      case 'service':
        setTitleText('Belum ada Jadwal Servis')
        setButtonText('Mulai Belanja')
        setDescriptionText(
          'Kamu tidak memiliki barang yang sedang diservis. Yuk, belanja sekarang!',
        )
        break
      case 'service-detail':
        setTitleText('Servis Tidak Ditemukan')
        setButtonText('Mulai Belanja')
        setDescriptionText(
          'Yuk, temukan produk dari brand favoritmu hanya di ruparupa',
        )
        break
      case 'online-transaction':
        setTitleText('Belum ada Transaksi Yang Berlangsung')
        setButtonText('Mulai Belanja')
        setDescriptionText(
          'Yuk, temukan produk dari brand favoritmu hanya di ruparupa',
        )
        break
      case 'offline-transaction':
        setTitleText('Belum Ada Transaksi Belanja')
        setButtonText('Mulai Belanja')
        setDescriptionText(
          'Kamu belum pernah belanja di toko ini. Pilih toko fisik lain atau mulai belanja sekarang!',
        )
        break
      case 'waiting-payment':
        setTitleText('Belum Ada Transaksi Tertunda')
        setButtonText('Mulai Belanja')
        setDescriptionText(
          'Yuk, temukan produk dari brand favoritmu hanya di ruparupa',
        )
        break
      case 'unreviewed':
        setTitleText('Belum Ada Produk Yang Perlu Diulas')
        setButtonText('Mulai Belanja')
        setDescriptionText(
          'Kamu belum memiliki barang yang perlu diulas. Yuk, belanja sekarang!',
        )
        break
      case 'reviewed':
        setTitleText('Belum Ada Riwayat Ulasan')
        setButtonText('Mulai Belanja')
        setDescriptionText(
          'Kamu belum memiliki riwayat ulasan. Yuk, belanja sekarang!',
        )
        break
      case 'detail-transaction':
        setTitleText('Maaf Data Transaksi Anda Tidak Ditemukan.')
        break
      case 'warranty-list-empty':
        setTitleText('Kamu Tidak Memiliki Garansi Produk')
        setButtonText('')
        setDescriptionText(
          'Produk yang kamu beli tidak memiliki garansi atau masa garansi telah habis',
        )
        break
      case 'sparepart-warranty':
        setTitleText('Komponen Tidak Tersedia')
        setButtonText('')
        setDescriptionText(
          'Mohon maaf, produk ini tidak memiliki komponen yang masuk dalam garansi',
        )
        break
      default:
        break
    }
  }

  useEffect(() => {
    if (pageType) {
      typeOrganizer()
    }
  }, [pageType])

  useEffect(() => {
    if (useFilter && pageType === 'online-transaction') {
      setTitleText('Transaksi yang kamu cari tidak ditemukan')
      setDescriptionText('Coba atur kembali filter yang digunakan')
      setButtonText('Atur Ulang Filter')
    } else if (useFilter && pageType === 'installation') {
      setTitleText('Instalasi yang kamu cari tidak ditemukan')
      setDescriptionText('Coba atur kembali filter yang digunakan')
      setButtonText('Atur Ulang Filter')
    } else if (!isEmpty(searchQuery)) {
      setDescriptionText('Coba ubah kata pencarianmu')
      setButtonText('Ubah Kata Pencarian')
      if (isEmpty(data)) {
        setTitleText('Maaf')
      }
    } else if (pageType === 'detail-transaction') {
      setButtonText('Kembali ke Halaman Transaksi')
    } else {
      typeOrganizer()
      setButtonText('Mulai Belanja')
    }
  }, [searchQuery, useFilter])

  const handleClick = () => {
    if (
      useFilter &&
      ['online-transaction', 'installation'].includes(pageType)
    ) {
      showFilterModal()
    } else if (!isEmpty(searchQuery)) {
      changeFocus()
    } else if (pageType === 'detail-transaction') {
      if (config.environment === 'mobile') {
        router.push('/my-account/transaction/list?tab=online-transaction')
      } else {
        router.push(
          'my-account?tab=transaction&child_tab=online-list',
          undefined,
          { shallow: true },
        )
      }
    } else {
      router.push({ pathname: '/' })
    }
  }

  return (
    <div className='text-center margin-xl'>
      <div className='margin-bottom-xs flex justify-center'>
        {pageType === 'installation' ? (
          <CustomLazyLoadImage
            src={config.assetsURL + 'icon/installation/no-installation.png'}
            height={200}
            width={343}
            alt='Installation Not Found'
            placeholder={<Skeleton height={200} width={343} />}
          />
        ) : (
          <CustomLazyLoadImage
            src='https://cdn.ruparupa.io/media/promotion/ruparupa/asset-unify/mobile/empty-states/mobile-empty-states.svg'
            height={200}
            width={343}
            alt='Transaction Not Found'
            placeholder={<Skeleton height={200} width={343} />}
          />
        )}
      </div>
      {!isEmpty(searchQuery) && !useFilter ? (
        <p
          className={`${NotFoundStyle['not-found-title-word-break']} heading-2 text__grey100 margin-bottom-xs`}
        >
          {titleText}
          <span className='text__blue'>{` "${searchQuery}" `}</span>
          <span className='text__grey100'>tidak ditemukan</span>
        </p>
      ) : (
        <p className='heading-2 text__grey100 margin-bottom-xs'>{titleText}</p>
      )}

      <p
        className={`ui-text-2 text__grey40 ${
          config.environment === 'mobile'
            ? 'margin-bottom-s'
            : 'margin-bottom-m'
        }`}
      >
        {descriptionText}
      </p>

      <div className='flex justify-center'>
        {pageType === 'offline-transaction' && (
          <Button
            size='medium'
            type='primary-border'
            width='60%'
            isSubmit
            additionalClass='text-semi-bold margin-right-s'
            handleOnClick={() => onOpenSelectBu()}
          >
            Pilih Toko Fisik
          </Button>
        )}
        {!['installation-errors', 'sparepart-warranty'].includes(pageType) && (
          <Button
            size='medium'
            type='primary'
            width='60%'
            isSubmit
            additionalClass='text-semi-bold'
            handleOnClick={() => handleClick()}
          >
            {buttonText}
          </Button>
        )}
      </div>
    </div>
  )
}
