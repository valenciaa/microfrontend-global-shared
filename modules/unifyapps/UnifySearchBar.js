import { forwardRef, useRef, useImperativeHandle } from 'react'
import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import UnifySearchBarStyle from '../../scss/5-modules/unifyapps/_unify-search-bar.module.scss'

const UnifySearchBar = (props, ref) => {
  const {
    value = '',
    additionalClass = '',
    placeholder,
    onChangeText,
    onClearText = () => onChangeText(''),
    onKeyPress,
  } = props
  const inputRef = useRef(null)

  useImperativeHandle(ref, () => ({
    focus: () => inputRef.current && inputRef.current.focus(),
    blur: () => inputRef.current && inputRef.current.blur(),
  }))

  return (
    <div className={`search-input w-100 ${additionalClass}`}>
      <input
        ref={inputRef}
        className={`search-bar border-grey-20 ui-text-3 ${UnifySearchBarStyle['unify-search-bar']}`}
        placeholder={placeholder}
        value={value}
        onChange={({ target: { value } }) => onChangeText(value)}
        onKeyPress={(event) => {
          if (event.key === 'Enter') {
            onKeyPress()
          }
        }}
      />
      <CustomLazyLoadImage
        className={`${UnifySearchBarStyle['unify-search-bar__icon']} ${UnifySearchBarStyle['unify-search-bar__icon--left']}`}
        src={`${config.assetsURL}icon/search-primary-${config.companyNameCSS}.svg`}
        alt='Search Icon'
        height={16}
        width={16}
      />
      {value.length > 0 && (
        <CustomLazyLoadImage
          className={`${UnifySearchBarStyle['unify-search-bar__icon']} ${UnifySearchBarStyle['unify-search-bar__icon--right']}`}
          src={`${config.assetsURL}icon/close-fill.svg`}
          alt='Clear Button'
          height={16}
          width={16}
          onClick={onClearText}
        />
      )}
    </div>
  )
}

export default forwardRef(UnifySearchBar)
