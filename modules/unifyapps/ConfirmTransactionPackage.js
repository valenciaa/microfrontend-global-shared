import dynamic from 'next/dynamic'
import React, { useState } from 'react'
import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import Modal from '../../layouts/templates/Modal'
import { useTransactionStatusUpdate } from '../../services/api/mutations/unifyapps'

const Button = dynamic(() => import('../../layouts/atoms/Button'))

export default function ConfirmTransactionPackage({
  orderData,
  isOnline,
  invoiceNo,
}) {
  const shipment = orderData?.shipment
  const checkStatusPicked =
    isOnline &&
    (shipment?.shipment_status === 'picked' ||
      shipment?.shipment_status === 'shipped' ||
      shipment?.shipment_status === 'pending_delivered')
  const checkStatusPending =
    orderData?.supplier_alias &&
    orderData?.supplier_alias?.toUpperCase()?.startsWith('MP') &&
    shipment?.shipment_status === 'pending_delivered'
  const [clickModal, setClickModal] = useState(false)
  const [statusType, setStatusType] = useState('')

  const { mutate: statusUpdate, isLoading } = useTransactionStatusUpdate({
    onSuccess: (res) => {
      if (res.ok) {
        window.location.reload()
      }
    },
  })

  const updateShipment = (shipmentId, newShipmentStatus, invoiceNo) => {
    const params = {
      shipment_id: shipmentId,
      shipment_status: newShipmentStatus,
      invoice_no: invoiceNo,
    }
    statusUpdate(params)
  }

  const modalConfirmation = (type) => {
    const bodyModalText = (
      <div className='ui-text-3 text-center modal-body color-grey-50'>
        {type === 'confirmation' ? (
          <p>
            Dengan klik {<strong>Selesai</strong>} Anda telah mengkonfirmasi
            penerimaan paket
          </p>
        ) : (
          'Apabila paket belum Anda terima, tim support kami akan segera melakukan investigasi dan menghubungi Anda'
        )}
      </div>
    )

    return (
      <div className='container-card'>
        {type === 'confirmation' && (
          <div className='tw-flex tw-justify-center'>
            <CustomLazyLoadImage
              src='https://res.cloudinary.com/ruparupa-com/image/upload/v1557827081/2.1/svg/konfirmasi-pesanan.svg'
              className='margin-top-m'
              width='25%'
              height='25%'
              alt='Logo'
            />
          </div>
        )}
        {bodyModalText}
        <div className='row justify-between'>
          <Button
            type='primary-border'
            size='medium'
            handleOnClick={() => {
              setClickModal(false)
              document.body.style.overflow = null
              document.body.classList.remove('modal-open')
            }}
            additionalClass='col margin-right-xs'
          >
            <div className='ui-text-3'>Kembali</div>
          </Button>
          <Button
            type='primary'
            size='medium'
            fetching={isLoading}
            handleOnClick={() => {
              updateShipment(
                shipment?.shipment_id,
                type === 'confirmation' ? 'received' : 'pending_received',
                invoiceNo,
              )
            }}
            additionalClass='col'
          >
            {isLoading ? (
              <img
                src={
                  config.assetsURL +
                  `${
                    config.environment === 'mobile' ? 'icon' : 'images'
                  }/loading-ruparupa.gif`
                }
                width='24px'
                height='24px'
                alt='loading'
              />
            ) : (
              <div className='ui-text-3'>
                {type === 'confirmation' ? 'Selesai' : 'Paket Belum Diterima'}
              </div>
            )}
          </Button>
        </div>
      </div>
    )
  }
  return (
    <>
      {checkStatusPicked && (
        <Button
          type='primary'
          size='medium'
          handleOnClick={() => {
            setClickModal(true)
            setStatusType('confirmation')
          }}
          additionalClass='margin-top-m'
        >
          <div className='ui-text-3'>Pesanan Selesai</div>
        </Button>
      )}
      {checkStatusPending && (
        <Button
          type='primary'
          size='medium'
          handleOnClick={() => {
            setClickModal(true)
            setStatusType('not_received')
          }}
          additionalClass='margin-top-m'
        >
          <div className='ui-text-3'>Pesanan Belum Diterima</div>
        </Button>
      )}
      {clickModal && (
        <Modal
          modalType='center'
          backdropOnClose
          bodyElement={() => modalConfirmation(statusType)}
          onClose={() => setClickModal(false)}
          show={clickModal}
          headerElement={
            statusType === 'confirmation'
              ? 'Konfirmasi Terima Paket'
              : 'Terjadi kendala dalam pengiriman?'
          }
          bodyClass='padding__horizontal--none margin-top-xs'
        />
      )}
    </>
  )
}
