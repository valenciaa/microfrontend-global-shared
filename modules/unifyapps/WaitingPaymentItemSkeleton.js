import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import WaitingPaymentItemStyle from '../../scss/5-modules/unifyapps/_waiting-payment-item.module.scss'

export default function WaitingPaymentItemSkeleton() {
  return (
    <div
      className={`padding-m ${
        WaitingPaymentItemStyle[
          `waiting-payment-item__container--${config.environment}`
        ]
      }`}
    >
      <div
        className={`flex items-center justify-between ${
          WaitingPaymentItemStyle[
            `waiting-payment-item__upper-container--${config.environment}`
          ]
        }`}
      >
        <div>
          <Skeleton height={12} width={115} className='margin-bottom-xxs' />
          <Skeleton height={20} width={150} className='margin-bottom-xxs' />
          <Skeleton height={12} width={75} />
        </div>
        <div className='flex items-center'>
          <Skeleton height={22} width={62} className='margin-right-xl' />
          <Skeleton height={16} width={16} />
        </div>
      </div>
      <div className='flex padding__vertical--xs'>
        <div
          className={`margin-right-xs ${
            WaitingPaymentItemStyle[
              `waiting-payment-item__lower-container-left--${config.environment}`
            ]
          }`}
        >
          <div className='flex-1'>
            <Skeleton
              height={12}
              width={100}
              className={WaitingPaymentItemStyle['margin-bottom-2']}
            />
            <Skeleton height={16} width={110} className='margin-bottom-xs' />
          </div>
          <div className='flex-1'>
            <Skeleton
              height={12}
              width={75}
              className={WaitingPaymentItemStyle['margin-bottom-2']}
            />
            <Skeleton height={16} width={100} />
          </div>
        </div>
        <div
          className={
            WaitingPaymentItemStyle[
              `waiting-payment-item__lower-container-right--${config.environment}`
            ]
          }
        >
          <Skeleton
            height={12}
            width={100}
            className={WaitingPaymentItemStyle['margin-bottom-2']}
          />
          <Skeleton height={16} width='75%' />
        </div>
      </div>
      <Skeleton height={32} width='100%' />
    </div>
  )
}
