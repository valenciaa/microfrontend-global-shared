import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import WaitingPaymentIcon from '../../../../public/static/icon/waiting-payment.svg'
import Button from '../../layouts/atoms/Button'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import WaitingPaymentButtonStyle from '../../scss/5-modules/unifyapps/_waiting-payment-button.module.scss'

export default function WaitingPaymentButton({ count, onClick }) {
  return (
    <Button
      additionalClass={`flex items-center ${
        config.environment === 'mobile'
          ? 'padding-m'
          : 'padding__vertical--xs padding__horizontal--s'
      } border-grey-20 ${WaitingPaymentButtonStyle['waiting-payment-button']}`}
      handleOnClick={onClick}
    >
      <WaitingPaymentIcon className={`color-${config.companyNameCSS}`} />

      <div className='flex flex-1 justify-start margin-left-m'>
        <span
          className={
            WaitingPaymentButtonStyle[
              `waiting-payment-button__text--${config.environment}`
            ]
          }
        >
          Menunggu Pembayaran
        </span>

        {count && count > 0 ? (
          <span
            className={`margin-left-xxs ${
              WaitingPaymentButtonStyle[
                `waiting-payment-button__text--${config.environment}`
              ]
            }`}
          >
            ({count})
          </span>
        ) : null}
      </div>

      <CustomLazyLoadImage
        src={config.assetsURL + 'icon/triangle-arrow-right.svg'}
        alt='Arrow Right'
        width='auto'
        height={24}
        placeholder={<Skeleton height={24} />}
      />
    </Button>
  )
}
