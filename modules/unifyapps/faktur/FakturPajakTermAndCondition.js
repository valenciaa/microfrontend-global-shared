import React, { useState } from 'react'

import HelpCircleIcon from '../../../../../public/static/icon/help-circle-icon.svg'
import Modal from '../../../layouts/templates/Modal'
import FakturPajakTermAndConditionModalBody from './FakturPajakTermAndConditionModalBody'
import config from '../../../../../config'
import FloatingCloseButton from '../../../layouts/atoms/FloatingCloseButton'

export default function FakturPajakTermAndCondition() {
  const [isOpenModal, setIsOpenModal] = useState(false)
  const fakturPajakTermAndConditionBody = (onClose) => (
    <FakturPajakTermAndConditionModalBody onClose={onClose} />
  )

  const floatingCloseButtonFaktur = (onClick) => (
    <FloatingCloseButton
      src={config.assetsURL + 'icon/close.svg'}
      height={40}
      width={40}
      onClick={onClick}
    />
  )

  return (
    <>
      <div
        className='tw-cursor-pointer tw-flex'
        onClick={() => setIsOpenModal(true)}
      >
        <HelpCircleIcon className='tw-text-grey-100' height={24} width={24} />
      </div>
      {isOpenModal && (
        <Modal
          bodyElement={fakturPajakTermAndConditionBody}
          contentClass={
            config.environment === 'mobile'
              ? 'margin-top-xxxl !tw-rounded-none'
              : ''
          }
          customStyled={
            config.environment === 'mobile' ? '' : '!tw-flex tw-items-center'
          }
          dialogClass={config.environment === 'mobile' ? '!tw-m-0' : ''}
          floatingCloseButtonElement={floatingCloseButtonFaktur}
          modalDepth={2}
          modalType={config.environment === 'mobile' ? 'bottom' : ''}
          needFloatingCloseButtonElement={config.environment === 'mobile'}
          show={isOpenModal}
          onClose={() => setIsOpenModal(false)}
        />
      )}
    </>
  )
}
