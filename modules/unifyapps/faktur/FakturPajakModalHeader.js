import React from 'react'
import clsx from 'clsx'

import FakturPajakTermAndCondition from './FakturPajakTermAndCondition'

import config from '../../../../../config'
import ArrowLeft from '../../../../../public/static/icon/arrow-left.svg'
import { useRouter } from 'next/router'

const headerStyle = {
  desktop: 'tw-gap-2 tw-pb-3 tw-pt-8 tw-px-4',
  mobile: 'tw-gap-3 tw-px-4 tw-py-3 tw-shadow-header-menu',
}

const headerContentStyle = {
  desktop: 'tw-gap-4',
  mobile: 'tw-gap-3',
}

export default function FakturPajakModalHeader({
  headerIcon,
  onClose,
  fromProfile,
}) {
  const router = useRouter()
  return (
    <div className={clsx('tw-flex', headerStyle[config.environment])}>
      {fromProfile ? (
        <ArrowLeft
          className='tw-text-grey-100'
          height={24}
          width={24}
          onClick={() => router.back()}
        />
      ) : (
        <div className='tw-flex tw-cursor-pointer' onClick={onClose}>
          {headerIcon}
        </div>
      )}
      <div
        className={clsx(
          'tw-flex tw-grow tw-items-center tw-justify-between',
          headerContentStyle[config.environment],
        )}
      >
        <div className='tw-text-grey-100'>
          <p className='heading-2'>Faktur Pajak</p>
        </div>
        <FakturPajakTermAndCondition />
      </div>
    </div>
  )
}
