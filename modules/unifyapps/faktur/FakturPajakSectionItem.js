import React from 'react'
import clsx from 'clsx'

import TWRadio from '../../../tw-components/inputs/TWRadio'
import TWCheckbox from '../../../tw-components/inputs/TWCheckbox'
import config from '../../../../../config'

const containerStyle = {
  desktop: 'tw-py-4',
  mobile: 'tw-py-3',
}

export default function FakturPajakSectionItem({
  faktur,
  isEdit,
  isFakturSelected,
  setSelectedFaktur,
  fromProfile,
}) {
  return (
    <div
      className={clsx(
        'tw-border-b tw-border-solid tw-border-grey-20 tw-cursor-pointer tw-flex tw-items-center tw-justify-between',
        fromProfile && config.environment === 'desktop' ? 'tw-px-4' : '',
      )}
      key={faktur?.customer_company_id}
      onClick={() => setSelectedFaktur(faktur)}
    >
      <div className='tw-grow'>
        <p
          className={clsx(
            containerStyle[config.environment],
            'heading-4 tw-text-grey-100',
          )}
        >
          {faktur?.company_name}
        </p>
      </div>
      {!isEdit && !fromProfile && (
        // onChange to prevent browser error
        <TWRadio isChecked={isFakturSelected} onChange={() => {}} />
      )}
      {isEdit && (
        <TWCheckbox className='!tw-h-6 !tw-w-6' isChecked={isFakturSelected} />
      )}
    </div>
  )
}
