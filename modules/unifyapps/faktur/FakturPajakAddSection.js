import React from 'react'
import clsx from 'clsx'

import config from '../../../../../config'

import DocumentIcon from '../../../../../public/static/icon/document-icon.svg'
import PlusIcon from '../../../../../public/static/icon/icon-plus.svg'

const containerStyle = {
  desktop: 'tw-pb-3 tw-px-4',
  mobile: 'tw-p-4',
}

const sectionTitleStyle = {
  desktop: 'button-medium-text',
  mobile: 'heading-5',
}

export default function FakturPajakAddSection({ onClick }) {
  return (
    <div className={clsx(containerStyle[config.environment])}>
      <div
        className='tw-cursor-pointer tw-flex tw-gap-3 tw-items-center tw-px-4 tw-py-3 tw-rounded-xl tw-shadow-card'
        onClick={onClick}
      >
        <div className='tw-bg-grey-10 tw-flex tw-items-center tw-justify-center tw-h-8 tw-w-8 tw-p-2 tw-rounded-5xl'>
          <DocumentIcon className='tw-text-grey-100' />
        </div>
        <div className='tw-flex tw-flex-col tw-gap-0.5 tw-grow'>
          <p
            className={clsx(
              `tw-text-${config.companyNameCSS}`,
              sectionTitleStyle[config.environment],
            )}
          >
            Faktur Pajak Baru
          </p>
          <p className='tw-text-grey-40 ui-text-3'>Tambah faktur pajak baru</p>
        </div>
        <div className='tw-flex'>
          <PlusIcon className='tw-text-blue-50' height={24} width={24} />
        </div>
      </div>
    </div>
  )
}
