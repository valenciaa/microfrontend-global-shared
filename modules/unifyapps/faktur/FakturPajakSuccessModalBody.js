import React from 'react'
import CloseIcon from '../../../../../public/static/icon/close.svg'
import config from '../../../../../config'

export default function FakturPajakSuccessModalBody({ onClose }) {
  return (
    <div className='tw-flex tw-flex-col tw-gap-3 tw-py-4 tw-text-grey-100'>
      <div className='tw-flex tw-items-center tw-justify-between'>
        <p className='heading-2'>Faktur Pajak Terpilih</p>
        {config.environment === 'desktop' && (
          <div className='tw-flex tw-cursor-pointer' onClick={onClose}>
            <CloseIcon className='tw-text-grey-100' height={24} width={24} />
          </div>
        )}
      </div>
      <p className='ui-text-2'>
        Faktur pajak kamu akan segera diproses dalam 14 hari kerja. Faktur pajak
        yang akan kamu terima akan terpisah berdasarkan invoice dari
        masing-masing toko yang mengirimkan barang yang kamu pesan.
      </p>
    </div>
  )
}
