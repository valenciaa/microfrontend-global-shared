import React from 'react'
import Modal from '../../../layouts/templates/Modal'
import AddFakturPajakConfirmationModalContent from './AddFakturPajakConfirmationModalContent'

export default function AddFakturPajakConfirmationModal({
  dataFaktur,
  handleSubmit,
  refetchFakturData,
  resetSelectedFaktur,
  onCloseForm,
  ...props
}) {
  const fakturPajakAddConfirmationModalBody = (onClose) => (
    <AddFakturPajakConfirmationModalContent
      dataFaktur={dataFaktur}
      handleSubmit={handleSubmit}
      refetchFakturData={refetchFakturData}
      resetSelectedFaktur={resetSelectedFaktur}
      onClose={onClose}
      onCloseForm={onCloseForm}
    />
  )

  return (
    <Modal
      bodyElement={fakturPajakAddConfirmationModalBody}
      customStyled='!tw-flex tw-items-center'
      {...props}
    />
  )
}
