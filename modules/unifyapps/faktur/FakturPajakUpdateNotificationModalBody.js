import React from 'react'
import TWButton from '../../../tw-components/buttons/TWButton'

export default function FakturPajakUpdateNotificationModalBody({
  onClose,
  setIsOpenAddFakturModal,
}) {
  const handleClickUpdateFaktur = () => {
    setIsOpenAddFakturModal(true)
    onClose()
  }
  return (
    <div className='tw-flex tw-flex-col tw-gap-3 tw-py-4'>
      <p className='heading-2 tw-text-grey-100'>Perbarui Nomor NPWP</p>
      <div className='tw-flex tw-flex-col tw-gap-4'>
        <p className='tw-text-grey-100 ui-text-2'>
          Dengan adanya perubahan format NPWP yang berjalan di Januari 2024,
          Kamu harus memperbarui nomor NPWP untuk menggunakan faktur pajak
          tersimpan
        </p>
        <TWButton
          additionalClass='tw-rounded-lg'
          handleOnClick={handleClickUpdateFaktur}
        >
          <p className='button-big-text'>Perbarui</p>
        </TWButton>
      </div>
    </div>
  )
}
