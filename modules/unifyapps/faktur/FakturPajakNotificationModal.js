import React from 'react'

import FakturPajakSuccessModalBody from './FakturPajakSuccessModalBody'
import FakturPajakUpdateNotificationModalBody from './FakturPajakUpdateNotificationModalBody'

import FloatingCloseButton from '../../../layouts/atoms/FloatingCloseButton'
import Modal from '../../../layouts/templates/Modal'

import config from '../../../../../config'

export default function FakturPajakNotificationModal({
  type = 'success-request',
  setIsOpenAddFakturModal,
  ...props
}) {
  const FakturPajakUpdateModalBody = (onClose) => (
    <FakturPajakUpdateNotificationModalBody
      onClose={onClose}
      setIsOpenAddFakturModal={setIsOpenAddFakturModal}
    />
  )

  const FakturPajakRequestSuccessModalBody = (onClose) => (
    <FakturPajakSuccessModalBody onClose={onClose} />
  )

  const floatingCloseButtonFakturNotification = (onClick) => (
    <FloatingCloseButton
      src={config.assetsURL + 'icon/close.svg'}
      height={40}
      width={40}
      onClick={onClick}
    />
  )

  const isMobileAndSuccessRequest =
    config.environment === 'mobile' && type === 'success-request'

  return (
    <Modal
      bodyElement={
        type === 'update'
          ? FakturPajakUpdateModalBody
          : FakturPajakRequestSuccessModalBody
      }
      contentClass={
        isMobileAndSuccessRequest ? 'margin-top-xxxl !tw-rounded-none' : ''
      }
      customStyled={isMobileAndSuccessRequest ? '' : '!tw-flex tw-items-center'}
      dialogClass={isMobileAndSuccessRequest ? '!tw-m-0' : ''}
      floatingCloseButtonElement={floatingCloseButtonFakturNotification}
      modalDepth={2}
      modalType={isMobileAndSuccessRequest ? 'bottom' : ''}
      needFloatingCloseButtonElement={isMobileAndSuccessRequest}
      {...props}
    />
  )
}
