import React from 'react'
import isEmpty from 'lodash/isEmpty'

import {
  useAddCompanyData,
  useUpdateCompanyData,
} from '../../../services/api/mutations/unifyapps'
import { handleNpwpFormatToDigit } from '../../../utils/UtilNpwp'
import TWButton from '../../../tw-components/buttons/TWButton'

export default function AddFakturPajakConfirmationModalContent({
  dataFaktur = {},
  handleSubmit,
  refetchFakturData,
  resetSelectedFaktur,
  onClose,
  onCloseForm,
}) {
  const [isSubmitButtonDisabled, setIsSubmitButtonDisabled] =
    React.useState(false)
  const { mutate: addFakturPajak } = useAddCompanyData({
    onSuccess: (res) => {
      if (!res?.data?.error) {
        refetchFakturData()
        onClose()
        onCloseForm()
      }
    },
  })
  const { mutate: updateFakturPajak } = useUpdateCompanyData({
    onSuccess: (res) => {
      if (!res?.data?.error) {
        resetSelectedFaktur()
        refetchFakturData()
        onClose()
        onCloseForm()
      }
    },
  })
  const handleClickAddFakturPajak = (data) => {
    data.company_npwp = handleNpwpFormatToDigit(data.company_npwp)

    if (data.is_npwp_address === 'yes') {
      data.company_address = ''
    }

    if (data.tipe_wajib_pajak !== 'Wajib Pajak Badan Cabang') {
      data.nitku = ''
    }

    if (!isEmpty(dataFaktur)) {
      const dataUpdate = data

      dataUpdate.customer_company_id = parseInt(dataFaktur.customer_company_id)
      dataUpdate.kpp_pratama = dataFaktur.kpp_pratama || 'no'

      if (
        dataFaktur.npwp_image !== '' &&
        dataFaktur.npwp_image === dataUpdate.npwp_image
      ) {
        dataUpdate.npwp_image = ''
      }

      updateFakturPajak(dataUpdate)

      return
    }

    addFakturPajak(data)
  }

  const modalTitle = isEmpty(dataFaktur)
    ? 'Simpan Faktur Pajak Baru'
    : 'Perbarui Faktur Pajak'

  return (
    <div className='tw-flex tw-flex-col tw-gap-3 tw-py-4'>
      <div className='tw-flex'>
        <p className='heading-2 tw-text-grey-100'>{modalTitle}</p>
      </div>
      <div className='tw-flex tw-flex-col tw-gap-4'>
        <p className='tw-text-grey-100 ui-text-2'>
          Pastikan informasi yang diinput sudah valid dan akurat, pengajuan
          faktur pajak tidak dapat diulang
        </p>
        <div className='tw-flex tw-gap-4 tw-justify-between'>
          <TWButton
            additionalClass='tw-rounded-md tw-w-full'
            handleOnClick={onClose}
            type='primary-border'
          >
            <p className='button-big-text'>Batal</p>
          </TWButton>
          <TWButton
            additionalClass='tw-rounded-md tw-w-full'
            handleOnClick={() => {
              setIsSubmitButtonDisabled(true)
              handleSubmit((data) => handleClickAddFakturPajak(data))()
            }}
            disabled={isSubmitButtonDisabled}
          >
            <p className='button-big-text'>Simpan</p>
          </TWButton>
        </div>
      </div>
    </div>
  )
}
