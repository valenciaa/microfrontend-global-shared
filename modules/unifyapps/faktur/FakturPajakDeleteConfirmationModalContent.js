import React from 'react'
import TWButton from '../../../tw-components/buttons/TWButton'
import { useDeleteFakturData } from '../../../services/api/mutations/unifyapps'

export default function FakturPajakDeleteConfirmationModalContent({
  selectedCustomerCompanyId,
  onClose,
  refetchFakturData,
  setErrorMessage,
  setIsOpenInfobox,
  setSelectedFaktur,
}) {
  const { mutate: deleteFaktur } = useDeleteFakturData({
    onSuccess: (res) => {
      if (!res?.data?.error) {
        refetchFakturData()
        setSelectedFaktur({})
      } else {
        setErrorMessage(res?.data?.error)
        setIsOpenInfobox(true)
      }
      onClose()
    },
  })
  const handleClickDeleteFaktur = () => {
    if (selectedCustomerCompanyId) {
      const data = {
        customer_company_id: parseInt(selectedCustomerCompanyId),
      }

      deleteFaktur(data)
    }
  }
  return (
    <div className='tw-flex tw-flex-col tw-gap-3 tw-py-4'>
      <div className='tw-flex'>
        <p className='heading-2 tw-text-grey-100'>
          Hapus Faktur Pajak Terpilih
        </p>
      </div>
      <div className='tw-flex tw-flex-col tw-gap-4'>
        <p className='tw-text-grey-100 ui-text-2'>
          Apakah kamu yakin ingin menghapus faktur pajak terpilih?
        </p>
        <div className='tw-flex tw-gap-4 tw-justify-between'>
          <TWButton
            additionalClass='tw-rounded-md tw-w-full'
            handleOnClick={onClose}
            type='primary-border'
          >
            <p className='button-big-text'>Batal</p>
          </TWButton>
          <TWButton
            additionalClass='tw-rounded-md tw-w-full'
            handleOnClick={handleClickDeleteFaktur}
          >
            <p className='button-big-text'>Ya, Hapus</p>
          </TWButton>
        </div>
      </div>
    </div>
  )
}
