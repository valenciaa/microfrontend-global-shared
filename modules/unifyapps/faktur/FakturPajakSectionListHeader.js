import React from 'react'
import TWButton from '../../../tw-components/buttons/TWButton'
import clsx from 'clsx'
import config from '../../../../../config'

const containerStyle = {
  desktop: 'tw-pb-3',
  mobile: 'tw-pb-2',
}

export default function FakturPajakSectionListHeader({
  buttonText,
  onClick,
  fromProfile,
}) {
  return (
    <div
      className={clsx(
        'tw-bg-grey-10 tw-flex tw-gap-2 tw-items-center tw-justify-between tw-pt-6 tw-px-4',
        containerStyle[config.environment],
        fromProfile && config.environment === 'desktop' ? 'tw-mx-4' : '',
      )}
    >
      <div className='tw-grow'>
        <p className='heading-3 tw-text-grey-100'>Faktur Pajak Tersimpan</p>
      </div>
      <TWButton
        additionalClass='!tw-p-0'
        handleOnClick={onClick}
        type='link-primary-transparent'
      >
        <p className='button-medium-text'>{buttonText}</p>
      </TWButton>
    </div>
  )
}
