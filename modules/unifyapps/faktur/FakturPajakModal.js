import React from 'react'
import clsx from 'clsx'

import FakturPajakModalContent from './FakturPajakModalContent'

import config from '../../../../../config'

import Modal from '../../../layouts/templates/Modal'

export default function FakturPajakModal({
  addFakturHeaderIcon,
  headerIcon,
  invoiceNo,
  orderNo,
  setIsOpenSuccessRequestFakturModal,
  ...props
}) {
  const fakturPajakModalContent = (onClose) => {
    return (
      <FakturPajakModalContent
        addFakturHeaderIcon={addFakturHeaderIcon}
        headerIcon={headerIcon}
        invoiceNo={invoiceNo}
        orderNo={orderNo}
        onClose={onClose}
        setIsOpenSuccessRequestFakturModal={setIsOpenSuccessRequestFakturModal}
      />
    )
  }
  const isDesktop = config.environment === 'desktop'
  return (
    <Modal
      bodyClass={clsx(isDesktop ? 'tw-w-[416px] tw-h-full' : '', '!tw-p-0')}
      bodyElement={fakturPajakModalContent}
      contentClass={clsx(
        isDesktop ? 'tw-rounded-lg tw-h-4/5' : 'tw-h-full !tw-rounded-none',
      )}
      customStyled={clsx(isDesktop ? '!tw-flex tw-items-center' : '')}
      dialogClass={clsx(
        isDesktop ? 'tw-h-full tw-flex tw-items-center' : 'tw-h-full !tw-m-0',
      )}
      {...props}
    />
  )
}
