import React from 'react'
import clsx from 'clsx'

import FakturPajakSectionItem from './FakturPajakSectionItem'
import config from '../../../../../config'

const containerStyle = {
  desktop: '',
  mobile: '',
}

export default function FakturPajakSectionList({
  fakturData,
  selectedFaktur,
  ...props
}) {
  return (
    <div
      className={clsx(
        'tw-flex tw-flex-col tw-px-4 tw-overflow-auto',
        containerStyle[config.environment],
      )}
    >
      {fakturData?.map((faktur) => {
        const isFakturSelected =
          parseInt(faktur?.customer_company_id) ===
          parseInt(selectedFaktur?.customer_company_id)
        return (
          <FakturPajakSectionItem
            key={faktur?.customer_company_id}
            faktur={faktur}
            isFakturSelected={isFakturSelected}
            {...props}
          />
        )
      })}
    </div>
  )
}
