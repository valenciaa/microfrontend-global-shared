import React from 'react'

import Modal from '../../../layouts/templates/Modal'
import FakturPajakDeleteConfirmationModalContent from './FakturPajakDeleteConfirmationModalContent'

export default function FakturPajakDeleteConfirmationModal({
  selectedCustomerCompanyId,
  refetchFakturData,
  setErrorMessage,
  setIsOpenInfobox,
  setSelectedFaktur,
  ...props
}) {
  const fakturPajakDeleteConfirmationModalBody = (onClose) => (
    <FakturPajakDeleteConfirmationModalContent
      selectedCustomerCompanyId={selectedCustomerCompanyId}
      onClose={onClose}
      refetchFakturData={refetchFakturData}
      setErrorMessage={setErrorMessage}
      setIsOpenInfobox={setIsOpenInfobox}
      setSelectedFaktur={setSelectedFaktur}
    />
  )

  return (
    <Modal
      bodyElement={fakturPajakDeleteConfirmationModalBody}
      customStyled='!tw-flex tw-items-center'
      {...props}
    />
  )
}
