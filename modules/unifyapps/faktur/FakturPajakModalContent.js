import React, { useMemo, useState } from 'react'
import dynamic from 'next/dynamic'
import isEmpty from 'lodash/isEmpty'

import {
  useGetFakturData,
  useGetFakturStatus,
} from '../../../services/api/queries/unifyapps'
import { useAddFakturInvoice } from '../../../services/api/mutations/unifyapps'

import TWButton from '../../../tw-components/buttons/TWButton'

import { useDetailTransactionContext } from '../../../../../src/unifyapps/context/DetailTransactionContext'
import FakturPajakModalHeader from './FakturPajakModalHeader'
import FakturPajakAddSection from './FakturPajakAddSection'
import FakturPajakSectionListHeader from './FakturPajakSectionListHeader'
import FakturPajakSectionList from './FakturPajakSectionList'
import clsx from 'clsx'
import config from '../../../../../config'

const AddFakturPajakModal = dynamic(() => import('./AddFakturPajakModal'))
const FakturPajakDeleteConfirmationModal = dynamic(
  () => import('./FakturPajakDeleteConfirmationModal'),
)
const FakturPajakNotificationModal = dynamic(
  () => import('./FakturPajakNotificationModal'),
)
const FloatingInfobox = dynamic(
  () => import('../../../layouts/atoms/FloatingInfobox'),
)

const modalFooterStyle = {
  desktop: 'tw-pb-8 tw-pt-3',
  mobile:
    'tw-bg-white tw-sticky tw-w-full tw-py-3 tw-shadow-wishlist-bottom-menu',
}

export default function FakturPajakModalContent({
  addFakturHeaderIcon,
  headerIcon,
  orderNo,
  invoiceNo,
  onClose,
  setIsOpenSuccessRequestFakturModal,
  fromProfile,
}) {
  const { data: fakturData, refetch: refetchFakturData } = useGetFakturData()
  const [errorMessage, setErrorMessage] = useState('')
  const { state: detailTransactionContext } = useDetailTransactionContext()
  const [isEdit, setIsEdit] = useState(false)
  const [isOpenDeleteConfirmationModal, setIsOpenDeleteConfirmationModal] =
    useState(false)
  const [isOpenAddFakturModal, setIsOpenAddFakturModal] = useState(false)
  const [isOpenFakturNotificationModal, setIsOpenFakturNotificationModal] =
    useState(false)
  const [isOpenInfobox, setIsOpenInfobox] = useState(false)
  const [selectedFaktur, setSelectedFaktur] = useState({})
  const params = useMemo(
    () => ({
      invoiceNo: invoiceNo,
      orderNo: orderNo,
    }),
    [orderNo, invoiceNo],
  )
  const { refetch: refetchFakturStatus } = useGetFakturStatus(params, {
    enabled: false,
  })
  const { mutate: addFakturInvoice } = useAddFakturInvoice({
    onSuccess: (res) => {
      if (res?.status === 200) {
        setIsOpenSuccessRequestFakturModal(true)
        refetchFakturStatus()
        onClose()
      }
    },
  })
  const isButtonDisabled =
    isEmpty(selectedFaktur) && !selectedFaktur?.customer_company_id
  const showButton = !fromProfile || (fromProfile && isEdit)
  const handleClickSelectedFakturPajak = () => {
    if (isEmpty(selectedFaktur)) {
      return
    }

    if (isEdit) {
      setIsOpenDeleteConfirmationModal(true)
    }

    if (!isEdit) {
      if (!selectedFaktur?.tipe_wajib_pajak) {
        setIsOpenFakturNotificationModal(true)

        return
      }

      const { invoice_no: invoiceNo, sales_order: salesOrder } =
        detailTransactionContext?.results

      const data = {
        customer_company_id: parseInt(selectedFaktur?.customer_company_id),
        invoice_no: [invoiceNo],
        order_no: salesOrder?.order_no,
        status: 'new',
      }
      addFakturInvoice(data)
    }
  }
  return (
    <>
      <div className='tw-flex tw-flex-col tw-h-full tw-overflow-hidden'>
        <FakturPajakModalHeader
          headerIcon={headerIcon}
          onClose={onClose}
          fromProfile={fromProfile}
        />
        <FakturPajakAddSection
          onClick={() => {
            setSelectedFaktur({})
            setIsEdit(false)
            setIsOpenAddFakturModal(true)
          }}
        />
        <FakturPajakSectionListHeader
          buttonText={isEdit ? 'Batal' : 'Edit'}
          onClick={() => setIsEdit(!isEdit)}
          fromProfile={fromProfile}
        />
        <FakturPajakSectionList
          fakturData={fakturData}
          isEdit={isEdit}
          selectedFaktur={selectedFaktur}
          setSelectedFaktur={setSelectedFaktur}
          fromProfile={fromProfile}
        />
        {showButton && (
          <div
            className={clsx(
              'tw-bottom-0 tw-flex tw-mt-auto tw-px-4',
              modalFooterStyle[config.environment],
            )}
          >
            <TWButton
              additionalClass='tw-rounded-md tw-w-full'
              disabled={isButtonDisabled}
              type={isButtonDisabled ? 'disabled' : 'primary'}
              handleOnClick={handleClickSelectedFakturPajak}
            >
              <p className='button-big-text'>
                {(isEdit ? 'Hapus' : 'Pilih') + ' Faktur Pajak'}
              </p>
            </TWButton>
          </div>
        )}
      </div>
      {isOpenDeleteConfirmationModal && (
        <FakturPajakDeleteConfirmationModal
          modalDepth={2}
          selectedCustomerCompanyId={selectedFaktur?.customer_company_id}
          show={isOpenDeleteConfirmationModal}
          onClose={() => setIsOpenDeleteConfirmationModal(false)}
          refetchFakturData={refetchFakturData}
          setErrorMessage={setErrorMessage}
          setIsOpenInfobox={setIsOpenInfobox}
          setSelectedFaktur={setSelectedFaktur}
        />
      )}
      {isOpenAddFakturModal && (
        <AddFakturPajakModal
          addFakturHeaderIcon={addFakturHeaderIcon}
          modalDepth={2}
          selectedFaktur={selectedFaktur}
          show={isOpenAddFakturModal}
          onClose={() => setIsOpenAddFakturModal(false)}
          refetchFakturData={refetchFakturData}
          resetSelectedFaktur={() => setSelectedFaktur({})}
        />
      )}
      {isOpenInfobox && (
        <FloatingInfobox
          duration={3000}
          toggleFlag={isOpenInfobox}
          type='error-minicart'
          setToggle={setIsOpenInfobox}
        >
          {errorMessage}
        </FloatingInfobox>
      )}
      {isOpenFakturNotificationModal && (
        <FakturPajakNotificationModal
          show={isOpenFakturNotificationModal}
          type='update'
          onClose={() => setIsOpenFakturNotificationModal(false)}
          setIsOpenAddFakturModal={setIsOpenAddFakturModal}
        />
      )}
    </>
  )
}
