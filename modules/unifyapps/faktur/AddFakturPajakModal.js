import React from 'react'
import clsx from 'clsx'

import AddFakturPajakModalContent from './AddFakturPajakModalContent'

import config from '../../../../../config'

import Modal from '../../../layouts/templates/Modal'

export default function AddFakturPajakModal({
  addFakturHeaderIcon,
  selectedFaktur,
  refetchFakturData,
  resetSelectedFaktur,
  ...props
}) {
  const addFakturPajakModalBody = (onClose) => (
    <AddFakturPajakModalContent
      addFakturHeaderIcon={addFakturHeaderIcon}
      dataFaktur={selectedFaktur}
      onClose={onClose}
      refetchFakturData={refetchFakturData}
      resetSelectedFaktur={resetSelectedFaktur}
    />
  )
  const isDesktop = config.environment === 'desktop'
  return (
    <Modal
      bodyClass={clsx(isDesktop ? 'tw-w-[416px] tw-h-full' : '', '!tw-p-0')}
      bodyElement={addFakturPajakModalBody}
      contentClass={clsx(
        isDesktop ? 'tw-h-4/5 tw-rounded-lg' : 'tw-h-full !tw-rounded-none',
      )}
      customStyled={clsx(isDesktop ? '!tw-flex tw-items-center' : '')}
      dialogClass={clsx(
        isDesktop
          ? 'tw-h-full tw-my-auto tw-flex tw-items-center'
          : 'tw-h-full !tw-m-0',
      )}
      {...props}
    />
  )
}
