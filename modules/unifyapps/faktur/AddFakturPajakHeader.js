import React from 'react'

import config from '../../../../../config'
import CloseIcon from '../../../../../public/static/icon/close.svg'
import clsx from 'clsx'

const headerContainerStyle = {
  desktop: 'tw-border-b tw-border-grey-20 tw-border-solid tw-pb-3 tw-pt-8',
  mobile: 'tw-py-3 tw-shadow-header-menu',
}

export default function AddFakturPajakHeader({
  addFakturHeaderIcon,
  headerText,
  onClose,
}) {
  return (
    <div
      className={clsx(
        'tw-flex tw-gap-3 tw-px-4',
        headerContainerStyle[config.environment],
      )}
    >
      {config.environment === 'mobile' && (
        <div className='tw-flex' onClick={onClose}>
          {addFakturHeaderIcon}
        </div>
      )}
      <div className='tw-flex tw-gap-2 tw-justify-between tw-text-grey-100 tw-w-full'>
        <p className='heading-2 tw-grow'>{headerText}</p>
        {config.environment === 'desktop' && (
          <CloseIcon
            className='tw-cursor-pointer'
            height={24}
            width={24}
            onClick={onClose}
          />
        )}
      </div>
    </div>
  )
}
