import React from 'react'
import Skeleton from 'react-loading-skeleton'
import clsx from 'clsx'
import { useDropzone } from 'react-dropzone'

import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import config from '../../../../../config'

export default function FakturPajakUploadDocument({
  dataFaktur = {},
  error,
  clearErrors,
  labelClass,
  setFormError = () => {},
  setValue = () => {},
}) {
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    maxFiles: 1,
    maxSize: 3000000,
    accept: ['image/png', 'image/jpg', 'image/jpeg'],
    onDrop: (acceptedFile, rejectedFile) => {
      const error = rejectedFile?.[0]?.errors?.[0]

      if (error?.code && error?.message) {
        switch (error.code) {
          case 'file-too-large':
            setFormError('npwp_image', {
              type: 'size',
              message: 'Ukuran file melebihi batas',
            })

            break

          case 'file-invalid-type':
            setFormError('npwp_image', {
              type: 'type',
              message: 'Format file tidak sesuai',
            })

            break

          default:
            setFormError('npwp_image', {
              type: 'global',
              message: error.message,
            })
        }

        return
      }

      const reader = new FileReader()

      reader.readAsDataURL(acceptedFile?.[0])

      reader.onload = (e) => {
        if (e?.target?.result?.toString()) {
          setValue('npwp_image', e.target.result.toString())

          clearErrors('npwp_image')

          return
        }

        setFormError('npwp_image', {
          type: 'global',
          message: 'Gagal membuka file, mohon gunakan file yang lainnya',
        })
      }
    },
  })
  return (
    <div className='tw-flex tw-flex-col tw-gap-1'>
      <p className={clsx('tw-text-grey-100', labelClass)}>Unggah Dokumen</p>
      <div
        {...getRootProps({
          className:
            'dropzone tw-bg-grey-10 tw-rounded-lg tw-p-5 tw-border-dashed tw-border-2 tw-border-grey-30',
        })}
        // id=''
        id='BtnUploadAddFaktur'
      >
        <input {...getInputProps()} />

        <div className='tw-flex tw-flex-col tw-gap-2.5 tw-items-center tw-justify-center tw-truncate'>
          <CustomLazyLoadImage
            alt='faktur-upload-img-icon'
            height={24}
            placeholder={<Skeleton height={24} width={24} />}
            src={`${config.assetsURL}icon/faktur-document-upload-icon.svg`}
            width={24}
          />

          <p className='tw-text-grey-50 tw-truncate ui-text-4'>
            {acceptedFiles?.[0]?.name ||
              dataFaktur?.npwp_image ||
              'Upload foto NPWP'}
          </p>
        </div>
      </div>
      {error && <p className='tw-text-red-50 ui-text-4'>{error?.message}</p>}
      <p className='tw-text-grey-40 ui-text-4'>
        Upload maks. 3MB, format .png, .jpg, .jpeg
      </p>
    </div>
  )
}
