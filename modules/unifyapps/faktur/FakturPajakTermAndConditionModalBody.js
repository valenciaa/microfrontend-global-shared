import React from 'react'

import config from '../../../../../config'

import CloseIcon from '../../../../../public/static/icon/close.svg'

export default function FakturPajakTermAndConditionModalBody({ onClose }) {
  return (
    <div className='tw-flex tw-flex-col tw-gap-3 tw-py-4'>
      <div className='tw-flex tw-items-center tw-justify-between'>
        <p className='heading-2 tw-text-grey-100'>Faktur Pajak</p>
        {config.environment === 'desktop' && (
          <div className='tw-cursor-pointer tw-flex' onClick={onClose}>
            <CloseIcon className='tw-text-grey-100' height={24} width={24} />
          </div>
        )}
      </div>
      <div className='tw-flex tw-flex-col'>
        <ol className='tw-list-decimal tw-px-4 tw-text-grey-100 ui-text-2'>
          <li>
            Kamu dapat memperoleh e-faktur kamu dengan mengisi formulir
            penambahan faktur pajak yang disertakan scan dokumen asli NPWP
            (Nomor Pokok Wajib Pajak).
          </li>

          <li>Permintaan e-Faktur pajak dapat diajukan saat order dibuat.</li>

          <li>
            Faktur pajak akan dibuat berdasarkan perusahaan penyedia produk (PT.
            Aspirasi Hidup Indonesia/PT. Home Center Indonesia/PT. Toys Kingdom
            Games Indonesia/ PT. Omni Digitama Internusa/ PT. Graha Satwa
            Paramita).
          </li>

          <li>
            Faktur pajak hanya tersedia untuk produk yang dikirim oleh ruparupa.
          </li>

          <li>
            Kami tidak dapat mengeluarkan faktur pajak untuk pembelanjaan produk
            yang dikirim maupun diambil dari toko di Kota Batam (produk bebas
            pajak).
          </li>
        </ol>
      </div>
    </div>
  )
}
