import React from 'react'
import clsx from 'clsx'

import config from '../../../../../config'

const labelStyle = {
  desktop: 'ui-text-4',
  mobile: 'ui-text-3',
}

export default function FakturPajakInput({
  error,
  label,
  name,
  register,
  ...props
}) {
  return (
    <div className='tw-flex tw-flex-col tw-gap-1'>
      <p className={clsx('tw-text-grey-100', labelStyle[config.environment])}>
        {label}
      </p>
      <input
        className={clsx(
          'tw-border tw-border-solid tw-flex tw-h-10 tw-items-center tw-px-3 tw-py-2.5 tw-rounded-lg tw-outline-none tw-text-grey-100',
          error
            ? 'tw-border-red-50'
            : 'tw-border-grey-20 focus:tw-border-blue-50',
        )}
        type='text'
        {...props}
        {...register(name)}
      />
      {error && <p className='tw-text-red-50 ui-text-4'>{error?.message}</p>}
    </div>
  )
}
