import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import isEmpty from 'lodash/isEmpty'

import FakturPajakUploadDocument from './FakturPajakUploadDocument'

import Infobox from '../../../layouts/atoms/Infobox'
import TWRadio from '../../../tw-components/inputs/TWRadio'
import TWButton from '../../../tw-components/buttons/TWButton'

import config from '../../../../../config'

import ChevronDown from '../../../../../public/static/icon/chevron-down-16.svg'

import InfoboxIcon from '../../../../../public/static/icon/infobox-info-16.svg'
import FakturPajakWPTypeList from '../../../../../src/unifyapps/containers/faktur/FakturPajakWPTypeList'
import FakturPajakInput from './FakturPajakInput'
import { AddFakturPajakCompanyDataValidation } from '../../../utils/validation/faktur'
import { handleNpwpFormatToDigit } from '../../../utils/UtilNpwp'
import AddFakturPajakConfirmationModal from './AddFakturPajakConfirmationModal'
import AddFakturPajakHeader from './AddFakturPajakHeader'

const textAreaStyle = {
  desktop: 'tw-text-sm',
  mobile: 'tw-text-xs',
}

const formContainerStyle = {
  desktop: 'tw-py-3',
  mobile: 'tw-py-4',
}

const labelStyle = {
  desktop: 'ui-text-4',
  mobile: 'ui-text-3',
}

const footerStyle = {
  desktop: 'tw-pb-8 tw-pt-3',
  mobile: 'tw-py-3',
}

export default function AddFakturPajakModalContent({
  addFakturHeaderIcon,
  dataFaktur,
  refetchFakturData,
  resetSelectedFaktur,
  onClose,
}) {
  const {
    register,
    formState: { errors },
    clearErrors,
    handleSubmit,
    getValues,
    watch,
    setError: setFormError,
    setValue,
    trigger,
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(AddFakturPajakCompanyDataValidation),
    defaultValues: {
      company_address: dataFaktur?.company_address || '',
      company_name: dataFaktur?.company_name || '',
      company_npwp: dataFaktur?.company_npwp || '',
      is_npwp_address: dataFaktur?.is_npwp_address || 'no',
      npwp_image: dataFaktur?.npwp_image || '',
      recipient_email: dataFaktur?.recipient_email || '',
      tipe_wajib_pajak: dataFaktur?.tipe_wajib_pajak || '',
      nitku: dataFaktur?.nitku || '',
    },
  })
  const [isOpenTipePajakDropdown, setIsOpenTipePajakDropdown] = useState(false)
  const [isOpenConfirmationModal, setIsOpenConfirmationModal] = useState(false)
  const [isSubmitButtonDisabled, setIsSubmitButtonDisabled] = useState(true)
  const isTextAreaDisabled = getValues('is_npwp_address') === 'yes'

  useEffect(() => {
    const {
      company_address: companyAddress,
      company_name: companyName,
      company_npwp: companyNpwp,
      is_npwp_address: isNpwpAddress,
      npwp_image: npwpImage,
      recipient_email: recipientEmail,
      tipe_wajib_pajak: tipeWajibPajak,
      nitku,
    } = getValues()
    const tipeWajibPajakChecker =
      tipeWajibPajak &&
      (tipeWajibPajak !== 'Wajib Pajak Badan Cabang' ||
        (tipeWajibPajak === 'Wajib Pajak Badan Cabang' && nitku?.length > 0))
    const companyAddressChecker =
      isNpwpAddress === 'yes' || (isNpwpAddress === 'no' && companyAddress)
    if (
      isEmpty(errors) &&
      tipeWajibPajakChecker &&
      companyAddressChecker &&
      companyName &&
      companyNpwp &&
      recipientEmail &&
      npwpImage
    ) {
      setIsSubmitButtonDisabled(false)
    } else {
      setIsSubmitButtonDisabled(true)
    }
  }, [errors, getValues, watch()])

  const handleClickSubmitCompanyData = async () => {
    setValue('company_npwp', handleNpwpFormatToDigit(getValues('company_npwp')))

    const valid = await trigger()

    if (valid) {
      setIsOpenConfirmationModal(true)
    }
  }

  const inputLabelSuffix = (text = '') =>
    getValues('tipe_wajib_pajak') === 'Wajib Pajak Perorangan'
      ? text
      : ' Perusahaan'

  const headerAndButtonSuffix = (text = '') =>
    !isEmpty(dataFaktur) ? text : ' Baru'

  return (
    <>
      <div className='tw-flex tw-flex-col tw-h-full'>
        <AddFakturPajakHeader
          addFakturHeaderIcon={addFakturHeaderIcon}
          headerText={'Faktur Pajak' + headerAndButtonSuffix(' Tersimpan')}
          onClose={onClose}
        />
        <div className='tw-flex tw-flex-col tw-h-auto tw-overflow-hidden tw-rounded-b-lg'>
          <div
            className={clsx(
              'tw-flex tw-flex-col tw-gap-4 tw-overflow-y-auto tw-px-4',
              formContainerStyle[config.environment],
            )}
          >
            <Infobox
              text='Pastikan informasi yang diinput sudah valid dan akurat, pengajuan faktur pajak tidak dapat diulang'
              theme='info'
              className='tw-gap-2'
              textClassName='ui-text-3'
            >
              <div className='tw-flex'>
                <InfoboxIcon className='tw-text-blue-60' />
              </div>
            </Infobox>
            <form className='tw-flex tw-flex-col tw-gap-4'>
              <div className='tw-flex tw-flex-col tw-gap-1'>
                <p
                  className={clsx(
                    'tw-text-grey-100',
                    labelStyle[config.environment],
                  )}
                >
                  Tipe Wajib Pajak
                </p>
                <div className='tw-relative'>
                  <div
                    className={clsx(
                      'tw-border tw-border-solid tw-cursor-pointer tw-flex tw-gap-2 tw-h-10 tw-items-center tw-justify-between tw-px-3 tw-py-2.5 tw-rounded-lg tw-outline-none',
                      errors?.tipe_wajib_pajak
                        ? 'tw-border-red-50'
                        : 'tw-border-grey-20 hover:tw-border-blue-50',
                    )}
                    onClick={() =>
                      setIsOpenTipePajakDropdown(!isOpenTipePajakDropdown)
                    }
                  >
                    <p
                      className={clsx(
                        'ui-text-3',
                        getValues('tipe_wajib_pajak')
                          ? 'tw-text-grey-100'
                          : 'tw-text-grey-30',
                      )}
                    >
                      {getValues('tipe_wajib_pajak') || 'Pilih Tipe'}
                    </p>
                    <div className='tw-flex'>
                      <ChevronDown className='tw-text-grey-100' />
                    </div>
                  </div>
                  {isOpenTipePajakDropdown && (
                    <FakturPajakWPTypeList
                      show={isOpenTipePajakDropdown}
                      value={getValues('tipe_wajib_pajak')}
                      onClose={() => setIsOpenTipePajakDropdown(false)}
                      resetNitku={() => setValue('nitku', '')}
                      setValue={(value) => setValue('tipe_wajib_pajak', value)}
                    />
                  )}
                </div>
                {errors?.tipe_wajib_pajak && (
                  <p className='tw-text-red-50 ui-text-4'>
                    {errors?.tipe_wajib_pajak?.message}
                  </p>
                )}
              </div>
              <FakturPajakInput
                error={errors?.company_name}
                label={'Nama' + inputLabelSuffix()}
                name='company_name'
                register={register}
              />
              {getValues('tipe_wajib_pajak') === 'Wajib Pajak Badan Cabang' && (
                <FakturPajakInput
                  error={errors?.nitku}
                  label='NITKU'
                  maxLength={22}
                  name='nitku'
                  placeholder='0000000000000000000000'
                  register={register}
                />
              )}
              <FakturPajakInput
                error={errors?.company_npwp}
                label={'Nomor NPWP' + inputLabelSuffix(' Perorangan')}
                maxLength={16}
                name='company_npwp'
                register={register}
              />
              <FakturPajakInput
                error={errors?.recipient_email}
                label={'Email' + inputLabelSuffix()}
                name='recipient_email'
                register={register}
              />
              <div className='tw-flex tw-flex-col tw-gap-2'>
                <p
                  className={clsx(
                    'tw-text-grey-100',
                    labelStyle[config.environment],
                  )}
                >
                  Alamat Sesuai NPWP/PPSKP
                </p>
                <div className='tw-flex tw-gap-4'>
                  <div className='tw-flex tw-gap-2 tw-items-center'>
                    <TWRadio
                      checked={getValues('is_npwp_address') === 'yes'}
                      name='is_npwp_address'
                      register={register}
                      value='yes'
                      setValue={setValue}
                      handleOnChange={() => {
                        setValue('company_address', '')
                        clearErrors('company_address')
                      }}
                    />
                    <p className='ui-text-4-medium tw-text-grey-100'>Ya</p>
                  </div>
                  <div className='tw-flex tw-gap-2 tw-items-center'>
                    <TWRadio
                      checked={getValues('is_npwp_address') === 'no'}
                      name='is_npwp_address'
                      register={register}
                      value='no'
                      setValue={setValue}
                    />
                    <p className='ui-text-4-medium tw-text-grey-100'>Tidak</p>
                  </div>
                </div>
                <textarea
                  className={clsx(
                    ' tw-border tw-border-solid tw-border-grey-20 tw-px-3 tw-py-2.5 tw-outline-none tw-rounded-lg tw-w-full tw-resize-none',
                    textAreaStyle[config.environment],
                    'tw-h-[130px] tw-max-h-[130px]',
                    isTextAreaDisabled
                      ? 'tw-text-grey-40'
                      : getValues('company_address')?.length > 0
                        ? 'tw-text-grey-100'
                        : 'tw-text-grey-30',
                    isTextAreaDisabled
                      ? 'tw-bg-grey-10 tw-cursor-not-allowed'
                      : 'tw-bg-white',
                    errors?.company_address
                      ? 'tw-border-red-50'
                      : 'focus:tw-border-blue-50',
                  )}
                  disabled={isTextAreaDisabled}
                  placeholder={
                    isTextAreaDisabled
                      ? 'Alamat faktur pajak akan dicetak sesuai dengan alamat NPWP/SPPKP terdaftar'
                      : 'Alamat NPWP/PPSKP'
                  }
                  value={getValues('company_address')}
                  {...register('company_address')}
                />
                {errors?.company_address && (
                  <p className='tw-text-red-50 ui-text-4'>
                    {errors?.company_address?.message}
                  </p>
                )}
              </div>
              <FakturPajakUploadDocument
                clearErrors={clearErrors}
                dataFaktur={dataFaktur}
                error={errors?.npwp_image}
                labelClass={labelStyle[config.environment]}
                setFormError={setFormError}
                setValue={setValue}
              />
            </form>
          </div>
          <div
            className={clsx(
              'tw-bg-white tw-bottom-0 tw-px-4 tw-sticky',
              footerStyle[config.environment],
            )}
          >
            <TWButton
              additionalClass='tw-flex tw-h-10 tw-items-center tw-justify-center tw-rounded-md tw-w-full'
              disabled={isSubmitButtonDisabled}
              type={isSubmitButtonDisabled ? 'disabled' : 'primary'}
              handleOnClick={handleClickSubmitCompanyData}
            >
              <p className='button-big-text'>
                Simpan {headerAndButtonSuffix()}
              </p>
            </TWButton>
          </div>
        </div>
      </div>
      {isOpenConfirmationModal && (
        <AddFakturPajakConfirmationModal
          dataFaktur={dataFaktur}
          modalDepth={3}
          show={isOpenConfirmationModal}
          handleSubmit={handleSubmit}
          onClose={() => {
            setIsOpenConfirmationModal(false)
          }}
          onCloseForm={onClose}
          refetchFakturData={refetchFakturData}
          resetSelectedFaktur={resetSelectedFaktur}
        />
      )}
    </>
  )
}
