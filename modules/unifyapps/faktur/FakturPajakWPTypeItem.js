import React from 'react'
import clsx from 'clsx'
import config from '../../../../../config'
import TWRadio from '../../../tw-components/inputs/TWRadio'

const textStyle = {
  desktop: 'tw-p-2',
  mobile: '',
}

const itemStyle = {
  desktop:
    'hover:tw-rounded-lg tw-h-9 tw-w-[360px] hover:tw-bg-grey-10 hover:tw-border hover:tw-border-solid hover:tw-border-grey-30',
  mobile: 'tw-py-3 tw-shadow-bank-account-bank-card',
}

const itemSelectedStyle = {
  desktop:
    'tw-bg-grey-10 tw-border tw-border-solid tw-border-grey-30 tw-rounded-lg',
  mobile: '',
}

export default function FakturPajakWPTypeItem({
  isChecked,
  resetNitku,
  type,
  onClose,
  setSelectedValue,
  setValue,
}) {
  const handleClickWPType = () => {
    if (config.environment === 'desktop') {
      if (type?.name !== 'Wajib Pajak Badan Cabang') {
        resetNitku()
      }
      setValue(type?.name)
      onClose()
    } else {
      setSelectedValue(type?.name)
    }
  }
  return (
    <div
      className={clsx(
        'tw-flex tw-items-center tw-justify-between tw-w-full',
        itemStyle[config.environment],
        isChecked && itemSelectedStyle[config.environment],
      )}
      onClick={handleClickWPType}
    >
      <div
        className={clsx(
          'tw-cursor-pointer tw-flex',
          textStyle[config.environment],
        )}
      >
        <p className='heading-4 tw-text-grey-100'>{type?.name}</p>
      </div>
      {config.environment === 'mobile' && (
        <TWRadio isChecked={isChecked} onChange={() => {}} />
      )}
    </div>
  )
}
