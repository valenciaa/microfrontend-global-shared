import React, { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import { useDetailTransactionContext } from '../../../../../src/unifyapps/context/DetailTransactionContext'
import { useGetFakturStatus } from '../../../services/api/queries/unifyapps'
import Button from '../../../layouts/atoms/Button'
import isEmpty from 'lodash/isEmpty'
import dayjs from 'dayjs'
import clsx from '../../../../../shared/global/utils/Clsx'

const FloatingInfobox = dynamic(
  () => import('../../../../../shared/global/layouts/atoms/FloatingInfobox'),
)

export default function RequestPajak({ orderData, refetchDownload }) {
  const { setState: setDetailTransactionContext } =
    useDetailTransactionContext()
  const [toggleFloatingInfobox, setToggleFloatingInfobox] = useState(false)
  const [errorMessageRequest, setErrorMessageRequest] = useState('')
  const [showDownloadFakturBtn, setShowDownloadFakturBtn] = useState(false)
  const [statusFakturCompleted, setStatusFakturCompleted] = useState(0)

  const isNotAllowToAddFaktur = orderData?.is_not_allow_to_add_faktur ?? false
  const isNotAllowToAddFakturMessage =
    orderData?.is_not_allow_to_add_faktur_message

  const { data: fakturStatus, refetch } = useGetFakturStatus(
    {
      orderNo: orderData?.sales_order?.order_no,
      invoiceNo: orderData?.invoice_no,
    },
    { enabled: false },
  )

  useEffect(() => {
    refetch()
  }, [])

  useEffect(() => {
    const dataFaktur = []
    if (!isEmpty(fakturStatus)) {
      fakturStatus?.forEach((data) => dataFaktur.push(data))
      setDetailTransactionContext((prevState) => ({
        ...prevState,
        fakturStatus: dataFaktur,
      }))

      // To determine download faktur button is show or not
      let statusFakturCompleted = 0
      let statusFakturCanceled = 0
      for (let i = 0; i < fakturStatus?.length; i++) {
        if (fakturStatus[i].Status === 'completed') {
          statusFakturCompleted++
        } else if (fakturStatus[i].Status === 'canceled') {
          statusFakturCanceled++
        }
      }
      const downloadBtnShowed =
        statusFakturCompleted > 0 &&
        statusFakturCanceled !== fakturStatus?.length
      setStatusFakturCompleted(statusFakturCompleted)
      setShowDownloadFakturBtn(downloadBtnShowed)
      setDetailTransactionContext((prevState) => ({
        ...prevState,
        downloadFakturBtnShowed: downloadBtnShowed,
      })) // save to context
    }
  }, [fakturStatus])

  const now = dayjs()
  const expiredDate = dayjs(orderData?.payment?.created_at)
    .add(1, 'month')
    .date(1)
  const expiredRequest = expiredDate.diff(now) < 0

  const status = [
    'partial_refund',
    'ready_to_pickup',
    'picked',
    'shipped',
    'received',
  ]
  const allowedShipmentStatus = status?.includes(
    orderData?.shipment?.shipment_status,
  )
  const statusReadyToPickup =
    orderData?.shipment?.shipment_status === 'ready_to_pickup'
  const statusPicked = orderData?.shipment?.shipment_status === 'picked'
  const shippingByRuparupa = orderData?.shipment?.shipping_by === 'Ruparupa'

  const handleModalRequest = () => {
    if (allowedShipmentStatus && shippingByRuparupa && !expiredRequest) {
      setDetailTransactionContext((prevState) => ({
        ...prevState,
        clickHeader: false,
        contentPajak: 'tncPajak',
      }))
    } else {
      errorMessagesRequest()
    }
  }

  const errorMessagesRequest = () => {
    if (!shippingByRuparupa) {
      setErrorMessageRequest(
        'Faktur pajak hanya diberikan pada produk yang dikirim Ruparupa',
      )
    } else if (expiredRequest) {
      setErrorMessageRequest(
        'Invoice telah melewati batas waktu pengajuan faktur pajak',
      )
    } else if (
      statusPicked ||
      statusReadyToPickup ||
      statusFakturCompleted > 0 ||
      orderData?.sales_order?.status === 'complete'
    ) {
      setErrorMessageRequest(
        'Silahkan konfirmasi penerimaan barang untuk download faktur pajak',
      )
    } else if (!statusReadyToPickup) {
      setErrorMessageRequest(
        'Pengajuan dapat dilakukan saat pesanan Anda dalam status Pengiriman atau Siap Diambil',
      )
    } else {
      setErrorMessageRequest('Terjadi kesalahan sistem')
    }
    setToggleFloatingInfobox(true)
  }

  const handleDownloadModalShow = () => {
    refetchDownload()

    if (fakturStatus?.[0]?.IsReceived !== 'yes') {
      setErrorMessageRequest(
        'Silahkan konfirmasi penerimaan barang untuk download faktur pajak',
      )
      setToggleFloatingInfobox(true)
    } else {
      setDetailTransactionContext((prevState) => ({
        ...prevState,
        clickHeader: false,
        contentPajak: 'downloadPajak',
      }))
    }
  }

  const allowRequestPajak = () => {
    if (isNotAllowToAddFakturMessage) {
      setErrorMessageRequest(isNotAllowToAddFakturMessage)
      setToggleFloatingInfobox(true)

      return
    }
    if (isNotAllowToAddFaktur) {
      return null
    }

    if (!fakturStatus) {
      return handleModalRequest()
    }

    return setDetailTransactionContext((prevState) => ({
      ...prevState,
      clickHeader: false,
      contentPajak: 'statusPajak',
    }))
  }

  return (
    <>
      <div className='tw-flex tw-flex-col tw-gap-4'>
        {showDownloadFakturBtn && (
          <Button
            type='primary-border'
            size='medium'
            handleOnClick={handleDownloadModalShow}
          >
            <div className='ui-text-3'>Unduh Faktur Pajak</div>
          </Button>
        )}

        {/* BU Cohesive Faktur Pajak Case, Request from Payment Page and Check the Status from Detail Transaction */}
        {isNotAllowToAddFaktur && fakturStatus && !orderData?.store?.name?.includes('DC') && (
          <Button
            type='primary-border'
            size='medium'
            handleOnClick={() =>
              setDetailTransactionContext((prevState) => ({
                ...prevState,
                clickHeader: false,
                contentPajak: 'statusPajak',
              }))
            }
          >
            <div className={clsx('ui-text-3')}>Lihat Status Faktur Pajak</div>
          </Button>
        )}

        {/* BU Non Cohesive Faktur Pajak Case */}
        {(!isNotAllowToAddFaktur || isNotAllowToAddFakturMessage) && !orderData?.store?.name?.includes('DC') ? (
          <Button
            type='primary-border'
            size='medium'
            handleOnClick={allowRequestPajak}
            disabled={isNotAllowToAddFaktur && !isNotAllowToAddFakturMessage}
          >
            <div className={clsx('ui-text-3')}>
              {!fakturStatus
                ? 'Pengajuan Faktur Pajak'
                : 'Lihat Status Faktur Pajak'}
            </div>
          </Button>
        ) : (
          <></>
        )}
      </div>

      {toggleFloatingInfobox && (
        <FloatingInfobox
          duration={isNotAllowToAddFakturMessage ? 5000 : 2500}
          toggleFlag={toggleFloatingInfobox}
          type='warning-top-pajak'
          setToggle={setToggleFloatingInfobox}
          wrapperClassName='infobox-wrapper__floating-over-modal'
        >
          {errorMessageRequest}
        </FloatingInfobox>
      )}
    </>
  )
}
