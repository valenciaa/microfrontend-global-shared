import React, { useEffect, useState } from 'react'
import isEmpty from 'lodash/isEmpty'
import { useDetailTransactionContext } from '../../../../../src/unifyapps/context/DetailTransactionContext'
import { useUnifyAppsContext } from '../../../context/UnifyAppsContext'
import CheckBoxRadioList from '../../../../../src/unifyapps/components/CheckBoxRadioList'
import UploadFaktur from './UploadFaktur'
import config from '../../../../../config'
import Infobox from '../../../layouts/atoms/Infobox'
import Button from '../../../layouts/atoms/Button'
import clsx from '../../../utils/Clsx'

export const FakturPajakAddCompanyForm = ({
  onSubmit,
  register,
  handleSubmit,
  setValue,
  watch,
  errorResponse,
  isFetching,
}) => {
  const unifyBaseStyles = useUnifyAppsContext().unifyBaseStyles
  const { handleChangeState } = useDetailTransactionContext()
  const [placeholderValue, setPlaceholderValue] = useState('Alamat')
  const [errorMessage, setErrorMessage] = useState(null)
  const [addressNpwp, setAddressNpwp] = useState([
    { id: 0, name: 'Ya', isActive: false },
    { id: 1, name: 'Tidak', isActive: false },
  ])
  const handleImageUpload = (file) => setValue('npwp_image', file)
  const handleError = (error) => setErrorMessage(error)

  useEffect(() => {
    setErrorMessage(errorResponse)
  }, [errorResponse])

  const buttonAndInformation = () => {
    const {
      company_name,
      company_npwp,
      recipient_email,
      is_npwp_address,
      company_address,
      npwp_image,
    } = watch()
    const isSubmitDisabled =
      isEmpty(company_name) ||
      isEmpty(company_npwp) ||
      isEmpty(recipient_email) ||
      (is_npwp_address === 'no' && isEmpty(company_address)) ||
      isEmpty(npwp_image)

    return (
      <div className='row justify-between'>
        <Infobox
          text='Pastikan informasi yang diinput di atas sudah valid dan akurat. Pengajuan faktur pajak tidak dapat diulang.'
          theme='info'
          className='margin-bottom-m'
          textClassName='margin-xs ui-text-3 text-semi-bold color-grey-50'
        />
        <Button
          type='link-primary-transparent'
          size='medium'
          handleOnClick={() =>
            handleChangeState('contentPajak', 'companyInformationPajak')
          }
          additionalClass='col margin-right-m padding__vertical--s'
        >
          <div className='ui-text-3 color-grey-100'>Close</div>
        </Button>
        <Button
          type='primary'
          size='medium'
          isSubmit
          fetching={isFetching || isSubmitDisabled}
          additionalClass={clsx(
            { disabled: isSubmitDisabled },
            'col padding__vertical--s',
          )}
        >
          {isFetching ? (
            <img
              src={
                config.assetsURL +
                `${
                  config.environment === 'mobile' ? 'icon' : 'images'
                }/loading-ruparupa.gif`
              }
              width='24px'
              height='24px'
              alt='loading'
            />
          ) : (
            <div className='ui-text-3'>Simpan</div>
          )}
        </Button>
      </div>
    )
  }

  const handleChangeRadio = (selectedAddress) => {
    if (selectedAddress.id === 0) {
      setValue('company_address', '')
      setValue('is_npwp_address', 'yes')
      setPlaceholderValue(
        'Faktur pajak akan diterbitkan sesuai dengan alamat NPWP/SPPKP terdaftar',
      )
    } else {
      setPlaceholderValue('Alamat')
      setValue('is_npwp_address', 'no')
    }

    setAddressNpwp((prev) => {
      const newAddressNpwp = JSON.parse(JSON.stringify(prev))
      newAddressNpwp.map((item) => {
        if (item.name === selectedAddress.name) {
          item.isActive = true
        } else {
          item.isActive = false
        }
      })
      return newAddressNpwp
    })
  }

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)} autoComplete='off'>
        {!isEmpty(errorMessage) && (
          <div
            className={`${unifyBaseStyles['error-text']} margin-bottom-xs bg-red-10 padding-s`}
          >
            {errorMessage}
          </div>
        )}
        <div className='search-input w-100 margin-bottom-s'>
          <input
            {...register('company_name')}
            type='text'
            placeholder='Nama Perusahaan'
            className={`search-bar ${unifyBaseStyles['input-bar']}`}
          />
          <img
            src={config.assetsURL + 'icon/icon-earth.svg'}
            className={`${unifyBaseStyles['input-bar__icon']} ${unifyBaseStyles['input-bar__icon--margin']}`}
          />
        </div>
        <div className='search-input w-100 margin-bottom-s'>
          <input
            {...register('company_npwp', {
              validate: (value) => !isNaN(value),
              pattern: {
                value: /^\d{15}$/,
              },
            })}
            type='text'
            onInput={(e) => {
              const numericValue = e.target.value.replace(/[^0-9]/g, '')
              if (numericValue !== 15) {
                e.target.setCustomValidity(
                  `Please lengthen this text to 15 characters (you are currently using ${e.target.value.length} characters)`,
                )
              } else {
                e.target.setCustomValidity('')
              }
              setValue('company_npwp', numericValue)
            }}
            onChange={(e) => {
              if (e.target.value.length === 15) {
                e.target.setCustomValidity('')
              }
            }}
            maxLength='15'
            minLength='15'
            placeholder='Nomor NPWP Perusahaan'
            className={`search-bar ${unifyBaseStyles['input-bar']}`}
          />
          <img
            src={config.assetsURL + 'icon/icon-card.svg'}
            className={`${unifyBaseStyles['input-bar__icon']} ${unifyBaseStyles['input-bar__icon--margin']}`}
          />
        </div>
        <div className='search-input w-100 margin-bottom-s'>
          <input
            {...register('recipient_email')}
            type='email'
            placeholder='Alamat E-mail'
            className={`search-bar ${unifyBaseStyles['input-bar']}`}
          />
          <img
            src={config.assetsURL + 'icon/Mail.svg'}
            className={`${unifyBaseStyles['input-bar__icon']} ${unifyBaseStyles['input-bar__icon--margin']}`}
          />
        </div>
        <p className='ui-text-3 color-grey-50 text-semi-bold'>
          Alamat NPWP/SPPKP?
        </p>
        <CheckBoxRadioList
          data={addressNpwp}
          groupName='address-npwp'
          type='radio'
          onChange={handleChangeRadio}
          containerClassName='flex w-100'
          className='flex-1'
          labelContainerClassName='row reverse'
          labelTextClassName='margin-left-m'
        />
        <div className='search-input w-100 margin-bottom-s'>
          <textarea
            {...register('company_address')}
            placeholder={placeholderValue}
            rows='4'
            disabled={!addressNpwp[1].isActive}
            className={`${unifyBaseStyles['input-bar']} padding-left-xxl padding-top-xs padding__horizontal--xs`}
          />
          <img
            src={config.assetsURL + 'icon/icon-location.svg'}
            className={`${unifyBaseStyles['input-bar__icon']} padding-top-xs`}
          />
        </div>
        <UploadFaktur
          handleImageUpload={handleImageUpload}
          handleError={handleError}
        />
        {buttonAndInformation()}
      </form>
    </div>
  )
}
