import React, { useState } from 'react'
import ReactDropzone from 'react-dropzone'
import config from '../../../../../config'
import UploadFakturStyle from '../../../scss/5-modules/unifyapps/_upload-faktur.module.scss'

export default function UploadImage({ handleImageUpload, handleError }) {
  const [imageData, setImageData] = useState(null)

  const handleDrop = (acceptedFiles) => {
    const file = acceptedFiles[0]
    const reader = new FileReader()
    reader.readAsDataURL(file)
    if (file.size >= 3072000) {
      handleError('Foto NPWP tidak boleh lebih dari 3MB')
    } else {
      setImageData(file)
      reader.onload = (e) => {
        handleImageUpload(e.target.result)
      }
      handleError(null)
    }
  }

  return (
    <div className={UploadFakturStyle['dropzone-faktur-area']}>
      <ReactDropzone onDrop={handleDrop}>
        {({ getRootProps, getInputProps }) => (
          <div {...getRootProps()}>
            <input {...getInputProps()} />
            <div className={UploadFakturStyle['dropzone-faktur-area__upload']}>
              <img
                src={config.assetsURL + 'icon/icon-upload.svg'}
                className={UploadFakturStyle['dropzone-asset-image']}
              />
              <div className='ui-text-2 text-center'>
                <span>
                  Unggah Foto NPWP/SPPKP
                  <br />
                  (Max. 3MB, JPG/PNG)
                </span>
              </div>
            </div>
          </div>
        )}
      </ReactDropzone>

      {imageData && (
        <div className='padding-top-m'>
          <div className='font-size-m bold padding-bottom-m'>Preview</div>
          <img alt='Preview' src={URL.createObjectURL(imageData)} />
        </div>
      )}
    </div>
  )
}
