import React from 'react'
import Modal from '../../layouts/templates/Modal'

import config from '../../../../config'

export default function MyBankAccountTncModal({ children, ...props }) {
  const MyBankAccountTncModalBody = () => {
    return (
      <div className='tw-flex tw-flex-col tw-gap-3'>
        <div className='tw-flex tw-justify-between'>
          <p className='heading-2 tw-text-grey-100'>Syarat dan Ketentuan</p>
          {children}
        </div>
        <div className='tw-max-h-[280px] tw-overflow-auto'>
          <ol className='tw-pl-5'>
            <li>
              <p className='tw-text-grey-100'>
                Setiap Pengguna hanya dapat memiliki 3 (tiga) rekening bank yang
                terdaftar pada akun {config.ruparupaName} Pengguna.
              </p>
            </li>
            <br />
            <li>
              <p className='tw-text-grey-100'>
                Pendaftaran rekening bank digunakan untuk pengembalian dana
                Customer atas order yang bermasalah.
              </p>
            </li>
            <br />
            <li>
              <p className='tw-text-grey-100'>
                Pengguna memahami dan menyetujui bahwa Pengguna bertanggung
                jawab penuh atas penggunaan rekening bank yang didaftarkan
                tersebut.
              </p>
            </li>
            <br />
            <li>
              <p className='tw-text-grey-100'>
                {config.ruparupaName} berhak untuk melakukan pembatasan
                pendaftaran rekening bank, pembekuan dan/atau penghapusan akun
                bank Pengguna sesuai dengan kebijakan yang ditentukan{' '}
                {config.ruparupaName}.
              </p>
            </li>
          </ol>
        </div>
      </div>
    )
  }

  return (
    <Modal
      backdropOnClose={config.environment === 'desktop'}
      bodyClass={config.environment === 'desktop' ? '!tw-p-8' : ''}
      bodyElement={MyBankAccountTncModalBody}
      dialogClass={
        config.environment === 'desktop' ? '!tw-w-[632px] tw-max-w-[632px]' : ''
      }
      isV2={config.environment === 'desktop'}
      modalType={config.environment === 'desktop' ? 'deals-center' : 'custom'}
      customTop='30vh'
      {...props}
    />
  )
}
