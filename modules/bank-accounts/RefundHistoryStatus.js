import React from 'react'

import RefundHistoryStatusCard from './RefundHistoryStatusCard'

export default function RefundHistoryStatus({ refund }) {
  return (
    <div className='tw-flex tw-flex-col tw-border-t tw-border-grey-20 tw-border-dashed tw-gap-3 tw-pt-3'>
      <p className='button-big-text tw-text-grey-100'>Status Penarikan</p>
      <div>
        {refund?.details?.map((detail, index) => {
          const { title, description } = detail
          return (
            <RefundHistoryStatusCard
              key={index}
              description={description}
              detail={detail}
              index={index}
              refund={refund}
              title={title}
            />
          )
        })}
      </div>
    </div>
  )
}
