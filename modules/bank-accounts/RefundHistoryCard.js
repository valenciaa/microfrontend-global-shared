import React, { useState } from 'react'
import dayjs from 'dayjs'
import clsx from 'clsx'

import RefundStatus from './RefundStatus'

import NumberWithCommas from '../../utils/NumberWithCommas'

import ChevronDown from '../../../../public/static/icon/chevron-down-16.svg'
import config from '../../../../config'

import RefundHistoryDetail from './RefundHistoryDetail'
import RefundHistoryInformation from './RefundHistoryInformation'

const cardStyle = {
  desktop: 'tw-border tw-border-grey-20 tw-border-solid tw-p-4',
  mobile: 'tw-p-3 tw-shadow-bank-account-card',
}

const totalRefundAmountStyle = {
  desktop: 'heading-3',
  mobile: 'button-big-text',
}

export default function RefundHistoryCard({ refund }) {
  const [isOpenRefundDetail, setIsOpenRefundDetail] = useState(false)

  const totalAmount = NumberWithCommas(
    refund?.refund_amount - refund?.customer_penalty,
  )

  const handleClickDropdown = () => {
    setIsOpenRefundDetail(!isOpenRefundDetail)
  }

  return (
    <div className={clsx('tw-rounded-lg', cardStyle[config.environment])}>
      <div
        className={clsx(
          'tw-flex tw-gap-4 tw-items-center tw-justify-between',
          isOpenRefundDetail ? 'tw-pb-3' : '',
        )}
      >
        <div className={clsx('tw-flex tw-flex-col tw-gap-1')}>
          <div className='tw-flex tw-gap-2 tw-items-center'>
            <p className='tw-text-grey-50 ui-text-4'>PENGEMBALIAN DANA</p>
            {refund?.customer_penalty > 0 && <RefundHistoryInformation />}
          </div>
          <p className='heading-3 tw-text-grey-100'>{refund?.refund_no}</p>
          <p className='tw-text-grey-40 ui-text-4'>
            {dayjs(refund?.created_at).format('DD MMMM YYYY, HH:mm')}
          </p>
        </div>
        <div className='tw-flex tw-flex-col tw-items-end tw-gap-2'>
          <RefundStatus
            color={refund?.status_color}
            status={refund?.to_render_status}
          />
          <div className='tw-flex tw-gap-2 tw-text-grey-100'>
            <p className={clsx(totalRefundAmountStyle[config.environment])}>
              Rp{totalAmount}
            </p>
            <div
              className='tw-flex tw-items-center'
              onClick={handleClickDropdown}
            >
              <ChevronDown height={16} width={16} alt='Dropdown Icon' />
            </div>
          </div>
        </div>
      </div>
      {isOpenRefundDetail && (
        <RefundHistoryDetail refund={refund} totalAmount={totalAmount} />
      )}
    </div>
  )
}
