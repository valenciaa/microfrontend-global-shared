import React from 'react'

import Modal from '../../layouts/templates/Modal'
import MyBankAccountConfirmationModalContent from './MyBankAccountConfirmatonModalContent'
import config from '../../../../config'

export default function DeleteBankAccountConfirmationModal({
  accountNumber,
  accountName,
  bankName,
  customerRekeningId,
  refetchBankAccounts,
  setBankAccountActionMessage,
  setIsOpenInfobox,
  ...props
}) {
  const DeleteBankAccountConfirmationModalBody = (onClose) => {
    return (
      <MyBankAccountConfirmationModalContent
        customerRekeningId={customerRekeningId}
        submitButtonText='Ya, Hapus'
        title='Hapus Rekening Bank'
        type='delete'
        onClose={onClose}
        refetchBankAccounts={refetchBankAccounts}
        setBankAccountActionMessage={setBankAccountActionMessage}
        setIsOpenInfobox={setIsOpenInfobox}
      >
        <div className='tw-flex tw-flex-col tw-gap-8 tw-text-grey-50 ui-text-2'>
          <div className='tw-flex tw-flex-col tw-min-w-0'>
            <p className='tw-text-ellipsis tw-overflow-hidden tw-whitespace-nowrap'>
              {accountNumber}
            </p>
            <p className='tw-text-ellipsis tw-overflow-hidden tw-whitespace-nowrap'>
              {accountName}
            </p>
            <p>{bankName}</p>
          </div>
          <p className=''>
            Apa kamu yakin ingin menghapus rekening tersebut dari daftar
            rekening bank?
          </p>
        </div>
      </MyBankAccountConfirmationModalContent>
    )
  }

  return (
    <Modal
      backdropOnClose
      bodyElement={DeleteBankAccountConfirmationModalBody}
      dialogClass={config.environment === 'desktop' ? '!tw-w-[416px]' : ''}
      isV2={config.environment === 'desktop'}
      modalType={config.environment === 'desktop' ? 'deals-center' : 'custom'}
      customTop='30vh'
      {...props}
    />
  )
}
