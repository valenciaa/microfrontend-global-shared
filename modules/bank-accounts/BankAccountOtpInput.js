import React from 'react'
import clsx from 'clsx'
import config from '../../../../config'

const otpInputStyle = {
  desktop: 'heading-2 tw-border-b-2 tw-h-10 tw-w-10',
  mobile: 'tw-border tw-h-12 tw-p-3 tw-rounded tw-w-12',
}

export default function BankAccountOtpInput({ ...props }) {
  return (
    <input
      autoComplete='off'
      className={clsx(
        'tw-border-grey-30 tw-border-solid tw-text-center tw-outline-none tw-text-grey-100',
        '[tw-appearance:textfield] [&::-webkit-outer-spin-button]:tw-appearance-none [&::-webkit-inner-spin-button]:tw-appearance-none', // class to remove default stepper
        otpInputStyle[config.environment],
      )}
      inputMode='numeric'
      maxLength={1}
      type='text'
      {...props}
    />
  )
}
