import React from 'react'
import clsx from 'clsx'

import config from '../../../../config'

import InfoboxErrorIcon from '../../../../public/static/icon/infobox-error-16.svg'
import SearchIcon from '../../../../public/static/icon/search-16.svg'

const inputStyle = {
  desktop: 'tw-pl-9 tw-pr-3 tw-py-2.5',
  mobile: 'tw-pl-8 tw-pr-2 tw-py-2',
}

export default function MyBankAccountBankSearch({ value, setSearchValue }) {
  return (
    <div className='tw-flex tw-items-center tw-relative'>
      <input
        className={clsx(
          'tw-border tw-border-solid tw-border-grey-20 focus:tw-border-blue-50  tw-rounded-lg tw-outline-none tw-w-full ui-text-3',
          inputStyle[config.environment],
        )}
        placeholder='Cari nama bank'
        onChange={(e) => setSearchValue(e.target.value)}
        value={value}
      />
      <div className='tw-absolute ui-text-3 tw-pl-3 tw-py-2.5'>
        <SearchIcon
          className={'tw-text-' + config.companyNameCSS}
          height={16}
          width={16}
        />
      </div>
      {value?.length > 0 && (
        <div
          className='tw-absolute tw-cursor-pointer tw-right-0 tw-py-2.5 tw-pr-3'
          onClick={() => setSearchValue('')}
        >
          <InfoboxErrorIcon className='tw-text-grey-30' />
        </div>
      )}
    </div>
  )
}
