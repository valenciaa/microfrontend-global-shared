import React from 'react'

import RefundHistoryStatus from './RefundHistoryStatus'
import RefundHistoryRefundDetails from './RefundHistoryRefundDetails'

export default function RefundHistoryDetail({ totalAmount, ...props }) {
  return (
    <>
      <RefundHistoryRefundDetails totalAmount={totalAmount} {...props} />
      <RefundHistoryStatus {...props} />
    </>
  )
}
