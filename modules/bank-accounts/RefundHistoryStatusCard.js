import React from 'react'
import clsx from 'clsx'
import dayjs from 'dayjs'

import Dots from '../../layouts/atoms/Dots'

import config from '../../../../config'

const refundStatusInformationStyle = {
  desktop: 'ui-text-5',
  mobile: 'ui-text-4',
}

const refundStatusDescriptionStyle = {
  desktop: 'ui-text-4',
  mobile: 'ui-text-3',
}

export default function RefundHistoryStatusCard({
  description,
  detail,
  index,
  refund,
  title,
}) {
  return (
    <div className='tw-flex' key={index}>
      <div className='tw-flex tw-flex-col tw-items-center tw-relative tw-top-0.5'>
        <Dots className='tw-h-2 tw-w-2' />
        {index !== refund?.details?.length - 1 && (
          <div className='tw-absolute tw-bg-orange-50 tw-h-[calc(100%-8px)] tw-w-0.5 tw-top-2' />
        )}
      </div>
      <div className='tw-flex tw-flex-col tw-ml-2 tw-pb-4 tw-gap-1'>
        <p
          className={clsx(
            'tw-text-grey-40',
            refundStatusInformationStyle[config.environment],
          )}
        >
          ruparupa - {dayjs(detail?.created_at).format('DD MMMM YYYY, HH:mm')}
        </p>
        <div className=''>
          <p
            className={clsx(
              'tw-text-grey-100',
              refundStatusDescriptionStyle[config.environment],
            )}
          >
            {title}
            <br />
            {description}
          </p>
        </div>
      </div>
    </div>
  )
}
