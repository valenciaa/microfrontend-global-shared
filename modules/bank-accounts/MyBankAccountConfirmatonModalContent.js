import React, { useState } from 'react'
import clsx from 'clsx'

import config from '../../../../config'

import TWButton from '../../tw-components/buttons/TWButton'

import { useUpdateBankAccount } from '../../services/api/mutations/global'

const modalContainerStyle = {
  desktop: 'tw-py-4',
  mobile: 'tw-py-2',
}

export default function MyBankAccountConfirmationModalContent({
  children,
  customerRekeningId,
  submitButtonText,
  title,
  type,
  onClose,
  refetchBankAccounts,
  setBankAccountActionMessage,
  setIsOpenInfobox,
}) {
  const [isError, setIsError] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const { mutate: updateBankAccount } = useUpdateBankAccount({
    onSuccess: (res) => {
      if (res.status === 200) {
        setErrorMessage('')
        setIsOpenInfobox(true)
        const notificationText =
          type === 'update'
            ? 'Berhasil mengubah rekening utama'
            : 'Rekening berhasil dihapus'
        setBankAccountActionMessage(notificationText)
        refetchBankAccounts()
        onClose()
      } else {
        setErrorMessage(res?.data?.message)
        setIsError(true)
      }
    },
  })

  const handleClickSubmit = () => {
    const flagValue = type === 'update'

    const data = {
      customer_rekening_id: customerRekeningId,
      is_active: flagValue,
      is_default: flagValue,
    }

    updateBankAccount(data)
  }

  return (
    <div
      className={clsx(
        'tw-flex tw-flex-col tw-gap-3',
        modalContainerStyle[config.environment],
      )}
    >
      <p className='heading-2 tw-text-grey-100'>
        {isError ? 'Tidak Dapat Hapus Rekening' : title}
      </p>
      <div className='tw-flex tw-flex-col tw-gap-4'>
        {!isError && children}
        {isError && <p className='tw-text-grey-50 ui-text-2'>{errorMessage}</p>}
        <div className='tw-flex tw-gap-4'>
          {!isError && (
            <>
              <TWButton
                additionalClass='tw-rounded-md tw-w-full'
                type='primary-border'
                handleOnClick={onClose}
              >
                <p className='button-big-text'>Batal</p>
              </TWButton>
              <TWButton
                additionalClass='tw-rounded-md tw-w-full'
                handleOnClick={handleClickSubmit}
              >
                <p className='button-big-text'>{submitButtonText}</p>
              </TWButton>
            </>
          )}
          {isError && (
            <TWButton
              additionalClass='tw-rounded-md tw-w-full'
              handleOnClick={onClose}
            >
              <p className='button-big-text'>Tutup</p>
            </TWButton>
          )}
        </div>
      </div>
    </div>
  )
}
