import React from 'react'
import clsx from 'clsx'

import NumberWithCommas from '../../utils/NumberWithCommas'

import config from '../../../../config'

const detailRefundAmountTextStyle = {
  desktop: 'ui-text-4',
  mobile: 'ui-text-3',
}

const detailRefundAmountStyle = {
  desktop: 'button-small-text tw-text-grey-40',
  mobile: 'heading-4 tw-text-grey-50',
}

const detailTotalRefundAmountStyle = {
  desktop: 'button-small-text',
  mobile: 'button-medium-text',
}

const detailRefundStyle = {
  desktop: 'tw-gap-1',
  mobile: 'tw-gap-2',
}

export default function RefundHistoryRefundDetails({ refund, totalAmount }) {
  return (
    <div className='tw-flex tw-flex-col tw-gap-2 tw-border-t tw-border-grey-20 tw-border-dashed tw-py-3'>
      <p className='button-big-text tw-text-grey-100'>Rincian Penarikan</p>
      <div
        className={clsx(
          'tw-flex tw-flex-col',
          detailRefundStyle[config.environment],
        )}
      >
        <div className='tw-flex tw-justify-between'>
          <p
            className={clsx(
              'tw-text-grey-40',
              detailRefundAmountTextStyle[config.environment],
            )}
          >
            Jumlah Penarikan
          </p>
          <p className={clsx(detailRefundAmountStyle[config.environment])}>
            Rp{NumberWithCommas(refund?.refund_amount)}
          </p>
        </div>
        <div className='tw-flex tw-justify-between'>
          <p
            className={clsx(
              'tw-text-grey-40',
              detailRefundAmountTextStyle[config.environment],
            )}
          >
            Biaya Penarikan
          </p>
          <p className={clsx(detailRefundAmountStyle[config.environment])}>
            Rp0
          </p>
        </div>
        {refund?.customer_penalty > 0 && (
          <div className='tw-flex tw-justify-between'>
            <p
              className={clsx(detailRefundAmountTextStyle[config.environment])}
            >
              Pengurangan Koin
            </p>
            <p className={clsx(detailRefundAmountStyle[config.environment])}>
              Rp{NumberWithCommas(refund?.customer_penalty)}
            </p>
          </div>
        )}
        {totalAmount > 0 && (
          <div className='tw-flex tw-justify-between'>
            <p
              className={clsx(
                'tw-font-bold tw-text-grey-100',
                detailRefundAmountTextStyle[config.environment],
              )}
            >
              Total yang ditransfer
            </p>
            <p
              className={clsx(
                'tw-text-grey-100',
                detailTotalRefundAmountStyle[config.environment],
              )}
            >
              Rp{totalAmount}
            </p>
          </div>
        )}
        {refund?.coin_refund > 0 && (
          <div className='tw-flex tw-justify-between'>
            <p
              className={clsx(
                'tw-font-bold tw-text-grey-100',
                detailRefundAmountTextStyle[config.environment],
              )}
            >
              Total koin
            </p>
            <p
              className={clsx(
                'tw-text-grey-100',
                detailTotalRefundAmountStyle[config.environment],
              )}
            >
              Rp{NumberWithCommas(refund?.coin_refund)}
            </p>
          </div>
        )}
      </div>
    </div>
  )
}
