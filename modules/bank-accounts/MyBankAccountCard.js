import React from 'react'
import Skeleton from 'react-loading-skeleton'
import clsx from 'clsx'

import config from '../../../../config'

import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'

import MyBankAccountCardAction from './MyBankAccountCardAction'
import BankAccountStatus from '../../layouts/atoms/BankAccountStatus'

const cardStyle = {
  desktop: clsx(
    'tw-border-t tw-border-solid tw-border-grey-30 tw-flex tw-items-center tw-justify-between tw-gap-4 tw-p-4',
  ),
  mobile: clsx(
    'tw-flex tw-flex-col tw-gap-3 tw-rounded-lg tw-p-3 tw-shadow-bank-account-card',
  ),
}

const accountNumberStyle = {
  desktop: 'tw-text-grey-100 ui-text-4',
  mobile: 'heading-5 tw-text-grey-100',
}

const accountNameStyle = {
  desktop: 'tw-text-grey-100 ui-text-4',
  mobile: 'tw-text-grey-50 ui-text-3',
}

const bankNameStyle = {
  desktop: 'tw-text-grey-100 ui-text-4',
  mobile: 'tw-text-grey-40 ui-text-3',
}

export default function MyBankAccountCard({ account, index, ...props }) {
  const {
    account_name: accountName,
    account_number: accountNumber,
    bank_name: bankName,
    customer_rekening_id: customerRekeningId,
    is_default: isDefault,
    short_name: shortName,
  } = account

  const isMainBankAccount = isDefault === 10
  const isShortNameExists = shortName?.length > 0
  const bankImageSrc =
    config.assetsURL +
    'images/bank/logo-' +
    (isShortNameExists ? shortName : 'default') +
    '.png'

  const defaultImageLength = config.environment === 'desktop' ? 62 : 56
  const imageHeight = isShortNameExists
    ? config.environment === 'desktop'
      ? 32
      : 22
    : defaultImageLength
  const imageWidth = isShortNameExists
    ? config.environment === 'desktop'
      ? 82
      : 56
    : defaultImageLength

  return (
    <>
      <div
        className={clsx(
          cardStyle[config.environment],
          config.environment === 'desktop' && index % 2 === 0
            ? 'tw-bg-grey-5'
            : '',
        )}
      >
        <div className='tw-flex tw-items-center tw-gap-2'>
          <div
            className={clsx(
              'tw-flex',
              config.environment === 'desktop'
                ? isShortNameExists
                  ? ''
                  : 'tw-min-w-[82px] tw-justify-center'
                : '',
            )}
          >
            <CustomLazyLoadImage
              src={bankImageSrc}
              height={imageHeight}
              width={imageWidth}
              alt={'Logo ' + bankName}
              placeholder={<Skeleton />}
            />
          </div>
          <div className='tw-flex tw-flex-col tw-gap-1 tw-min-w-0 tw-max-w-[372px]'>
            {config.environment === 'desktop' && isMainBankAccount && (
              <BankAccountStatus />
            )}
            <div className='tw-flex tw-gap-1 tw-items-center tw-min-w-0'>
              <p
                className={clsx(
                  accountNumberStyle[config.environment],
                  'tw-text-ellipsis tw-overflow-hidden',
                )}
              >
                {accountNumber}
              </p>
              {config.environment === 'mobile' && isMainBankAccount && (
                <BankAccountStatus />
              )}
            </div>
            <p
              className={clsx(
                accountNameStyle[config.environment],
                'tw-text-ellipsis tw-overflow-hidden tw-whitespace-nowrap',
              )}
            >
              a.n {accountName}
            </p>
            <p className={bankNameStyle[config.environment]}>{bankName}</p>
          </div>
        </div>
        {!isMainBankAccount && (
          <MyBankAccountCardAction
            accountNumber={accountNumber}
            accountName={accountName}
            bankName={bankName}
            customerRekeningId={customerRekeningId}
            {...props}
          />
        )}
      </div>
    </>
  )
}
