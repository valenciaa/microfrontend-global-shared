import React from 'react'
import clsx from 'clsx'

import BankAccountOtpInput from './BankAccountOtpInput'

import {
  inputFocusBesidesNumberKey,
  onChangeBankAccountOtp,
} from '../../utils/BankAccountOtpUtils'

import config from '../../../../config'

const otpStyle = {
  desktop: 'tw-gap-3',
  mobile: 'tw-gap-2',
}

export default function BankAccountOtp({ otp, setOtp }) {
  return (
    <div
      className={clsx(
        'tw-flex tw-justify-center',
        otpStyle[config.environment],
      )}
    >
      {otp?.map((number, index) => {
        return (
          <BankAccountOtpInput
            key={index}
            id={`otp-field-${index}`}
            tabIndex={index}
            value={number}
            onChange={(e) => onChangeBankAccountOtp(e, index, otp, setOtp)}
            onKeyDown={(e) => inputFocusBesidesNumberKey(e, index, otp, setOtp)}
          />
        )
      })}
    </div>
  )
}
