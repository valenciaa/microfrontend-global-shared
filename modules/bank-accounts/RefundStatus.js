import React from 'react'
import clsx from 'clsx'

import config from '../../../../config'

const statusTypeStyle = (color) => clsx(`tw-bg-${color}-20 tw-text-${color}-60`)

const statusStyle = {
  desktop: 'tw-p-2 tw-rounded-[32px]',
  mobile: 'tw-p-1 tw-rounded-[3px]',
}

export default function RefundStatus({ color, status }) {
  return (
    <div
      className={clsx(
        'tw-max-w-max',
        statusStyle[config.environment],
        statusTypeStyle(color),
      )}
    >
      <p className='tw-text-center ui-text-4'>{status}</p>
    </div>
  )
}
