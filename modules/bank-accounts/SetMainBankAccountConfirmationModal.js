import React from 'react'

import Modal from '../../layouts/templates/Modal'
import MyBankAccountConfirmationModalContent from './MyBankAccountConfirmatonModalContent'
import config from '../../../../config'

export default function SetMainBankAccountConfirmationModal({
  customerRekeningId,
  refetchBankAccounts,
  setBankAccountActionMessage,
  setIsOpenInfobox,
  ...props
}) {
  const SetMainBankAccountConfirmationModalBody = (onClose) => {
    return (
      <MyBankAccountConfirmationModalContent
        customerRekeningId={customerRekeningId}
        submitButtonText='Ya, Ubah'
        title='Jadikan Rekening Utama'
        type='update'
        onClose={onClose}
        refetchBankAccounts={refetchBankAccounts}
        setBankAccountActionMessage={setBankAccountActionMessage}
        setIsOpenInfobox={setIsOpenInfobox}
      >
        <p className='tw-text-grey-50 ui-text-2'>
          Beberapa pencairan dana akan otomatis dikirim ke rekening utama.
          Apakah kamu yakin ingin mengubah rekening utama?
        </p>
      </MyBankAccountConfirmationModalContent>
    )
  }
  return (
    <Modal
      backdropOnClose
      bodyElement={SetMainBankAccountConfirmationModalBody}
      dialogClass={config.environment === 'desktop' ? '!tw-w-[416px]' : ''}
      isV2={config.environment === 'desktop'}
      modalType={config.environment === 'desktop' ? 'deals-center' : 'custom'}
      customTop='30vh'
      {...props}
    />
  )
}
