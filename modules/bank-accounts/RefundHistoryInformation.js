import React, { useState } from 'react'
import HelpCircleIcon from '../../../../public/static/icon/help-circle-icon.svg'

export default function RefundHistoryInformation() {
  const [isOpenPopUp, setIsOpenPopUp] = useState(false)
  return (
    <div className='tw-flex tw-relative'>
      <HelpCircleIcon
        className='tw-text-grey-100'
        onMouseEnter={() => setIsOpenPopUp(true)}
        onMouseLeave={() => setIsOpenPopUp(false)}
      />
      {isOpenPopUp && (
        <div className='tw-absolute tw-bg-black tw-ml-5 tw-p-2 tw-rounded-lg tw-w-52 tw-text-white ui-text-3'>
          Nominal pengembalian dana sudah termasuk pengurangan koin yang telah
          dibelanjakan
        </div>
      )}
    </div>
  )
}
