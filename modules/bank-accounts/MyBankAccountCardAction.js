import React, { useState } from 'react'
import clsx from 'clsx'

import TWButton from '../../tw-components/buttons/TWButton'

import DeleteBankAccountConfirmationModal from './DeleteBankAccountConfirmationModal'
import SetMainBankAccountConfirmationModal from './SetMainBankAccountConfirmationModal'

import config from '../../../../config'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'

const buttonContainerStyle = {
  desktop: 'tw-w-[245px]',
  mobile: '',
}

const buttonStyle = {
  desktop: '!tw-p-o',
  mobile: 'tw-rounded-md tw-w-full',
}

const buttonTextStyle = {
  desktop: 'tw-text-blue-50 tw-underline',
  mobile: '',
}

export default function MyBankAccountCardAction({
  accountNumber,
  accountName,
  bankName,
  customerRekeningId,
  ...props
}) {
  const [
    isOpenSetAccountConfirmationModal,
    setIsOpenSetAccountConfirmationModal,
  ] = useState(false)
  const [isOpenDeleteConfirmationModal, setIsOpenDeleteConfirmationModal] =
    useState(false)

  return (
    <>
      <div
        className={clsx(
          'tw-flex tw-gap-4',
          buttonContainerStyle[config.environment],
        )}
      >
        <TWButton
          additionalClass={buttonStyle[config.environment]}
          type={
            config.environment === 'desktop'
              ? 'link-primary-transparent'
              : 'secondary-border_ghost'
          }
          handleOnClick={() => {
            setIsOpenSetAccountConfirmationModal(true)
            mixpanelTrack('Click Jadikan Rekening Utama', {
              'Bank': bankName,
              'Rekening': accountNumber
            })
          }}
        >
          <p
            className={clsx(
              'button-small-text',
              buttonTextStyle[config.environment],
            )}
          >
            Jadikan Rekening Utama
          </p>
        </TWButton>
        <TWButton
          additionalClass={buttonStyle[config.environment]}
          type={
            config.environment === 'desktop'
              ? 'link-primary-transparent'
              : 'primary-border'
          }
          handleOnClick={() => {
            setIsOpenDeleteConfirmationModal(true)
            mixpanelTrack('Click Hapus Rekening', {
              'Bank': bankName,
              'Rekening': accountNumber
            })
          }}
        >
          <p
            className={clsx(
              'button-small-text',
              buttonTextStyle[config.environment],
            )}
          >
            Hapus
          </p>
        </TWButton>
      </div>
      {isOpenSetAccountConfirmationModal && (
        <SetMainBankAccountConfirmationModal
          customerRekeningId={customerRekeningId}
          isShow={isOpenSetAccountConfirmationModal}
          onClose={() => setIsOpenSetAccountConfirmationModal(false)}
          {...props}
        />
      )}
      {isOpenDeleteConfirmationModal && (
        <DeleteBankAccountConfirmationModal
          accountNumber={accountNumber}
          accountName={accountName}
          bankName={bankName}
          customerRekeningId={customerRekeningId}
          isShow={isOpenDeleteConfirmationModal}
          onClose={() => setIsOpenDeleteConfirmationModal(false)}
          {...props}
        />
      )}
    </>
  )
}
