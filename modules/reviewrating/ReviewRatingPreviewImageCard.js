import React from 'react'
import CloseButton from '../../../../public/static/icon/infobox-error-16.svg'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'

export default function ReviewRatingPreviewImageCard({
  image,
  index,
  previewImages,
  setPreviewImages,
}) {
  const deletePreviewImage = () => {
    const updatedPreviewImages = previewImages.slice(0)
    updatedPreviewImages.splice(index, 1)
    setPreviewImages(updatedPreviewImages)
  }

  return (
    <div className='tw-relative' key={image.uploadId}>
      <div
        className='tw-absolute tw-cursor-pointer -tw-right-2.5 -tw-top-2.5'
        onClick={deletePreviewImage}
      >
        <CloseButton className='tw-text-grey-100' height={20} width={20} />
      </div>
      <CustomLazyLoadImage
        alt={'Review Image ' + (index + 0)}
        className='tw-rounded'
        height={44}
        src={image.image_url}
        width={44}
      />
    </div>
  )
}
