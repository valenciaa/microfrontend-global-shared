import clsx from 'clsx'
import React from 'react'
import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import Dots from '../../layouts/atoms/Dots'
import ReviewRatingStar from './ReviewRatingStar'

const customerNameStyle = {
  desktop: 'heading-4',
  mobile: 'heading-3',
}

const dateStyle = {
  desktop: 'ui-text-4',
  mobile: 'ui-text-3',
}

export default function ServiceReviewInformation({
  averageRating,
  customerName = 'Anonymous',
  reviewDate,
}) {
  const length = config.environment === 'desktop' ? 40 : 16

  return (
    <div className='tw-flex tw-gap-2 tw-items-center'>
      <div className='tw-bg-grey-10 tw-h-8 tw-w-8 tw-relative tw-rounded-full'>
        <div className='tw-absolute tw-p-2'>
          <CustomLazyLoadImage
            height={length}
            src={`${config.assetsURL}icon/account.svg`}
            width={length}
          />
        </div>
      </div>

      <div className='tw-flex tw-flex-col tw-gap-1'>
        <div className='tw-flex tw-gap-1 tw-items-center'>
          <p
            className={clsx(
              'tw-text-grey-100',
              customerNameStyle[config.environment],
            )}
          >
            {customerName}
          </p>

          <Dots backgroundColor='tw-bg-grey-40' />

          <p className={clsx('tw-text-grey-40', dateStyle[config.environment])}>
            {reviewDate}
          </p>
        </div>
        <ReviewRatingStar gap={4} rating={averageRating} starDimension='16px' />
      </div>
    </div>
  )
}
