import clsx from 'clsx'
import dayjs from 'dayjs'
import { useRouter } from 'next/router'
import React from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import { useCartAuthContext } from '../../context/CartAuthContext'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import TWButton from '../../tw-components/buttons/TWButton'

const paddingStyle = {
  desktop: 'tw-p-4',
  mobile: 'tw-p-3',
}

const storeNameStyle = {
  desktop: 'heading-3',
  mobile: 'heading-5',
}

export default function TransactionLocation({ data, reviewDate }) {
  const router = useRouter()

  const cartAuthContext = useCartAuthContext()
  const { auth } = cartAuthContext.state

  const {
    invoice_no,
    receipt_id,
    logo,
    shopping_type,
    store_name,
    transaction_date,
  } = data

  const brandLogoSize = 36

  const isOnline = shopping_type === 'online'

  const id = isOnline ? invoice_no : receipt_id
  const orderType = isOnline ? 'Belanja Online' : 'Belanja di Toko Fisik'

  const headerRight = reviewDate
    ? 'Diulas: ' + dayjs(reviewDate).format('D MMM YYYY')
    : 'Tanggal Transaksi: ' + dayjs(transaction_date).format('D MMM YYYY')

  const handleDirectTransactionDetail = () => {
    let redirectRoute = ''

    if (config.environment === 'desktop') {
      redirectRoute = `/my-account?tab=transaction&child_tab=detail&transaction_no=${invoice_no}&type=online&email=${auth?.user?.email}`
    } else {
      redirectRoute = `/my-account/transaction/detail?transaction_no=${data.invoice_no}&type=online&email=${auth?.user?.email}`
    }

    router.push(redirectRoute, undefined, { shallow: true })
  }

  return (
    <div
      className={clsx(
        'tw-border tw-border-grey-20 tw-border-solid tw-flex tw-flex-col tw-gap-3 tw-rounded-lg',
        paddingStyle[config.environment],
      )}
    >
      <div className='tw-flex tw-justify-between tw-text-grey-40'>
        <p className='ui-text-4'>{id}</p>
        <p className='ui-text-4'>{headerRight}</p>
      </div>

      <div className='tw-flex tw-flex-col tw-gap-3'>
        <div className='tw-flex tw-items-center tw-gap-3'>
          <div className='tw-border tw-border-grey-20 tw-border-solid tw-flex tw-items-center tw-justify-center tw-h-12 tw-w-12 tw-rounded-full'>
            <CustomLazyLoadImage
              alt='store-logo'
              height={brandLogoSize}
              placeholder={<Skeleton />}
              src={logo}
              width={brandLogoSize}
            />
          </div>

          <div className='tw-flex tw-flex-col tw-gap-1'>
            <p className='tw-text-grey-50 ui-text-4'>{orderType}</p>
            <p
              className={clsx(
                'tw-text-grey-100',
                storeNameStyle[config.environment],
              )}
            >
              {store_name}
            </p>
          </div>
        </div>

        {isOnline && (
          <TWButton
            additionalClass={clsx(
              'tw-rounded-md',
              config.environment === 'mobile' ? 'w-full' : '',
            )}
            handleOnClick={handleDirectTransactionDetail}
            type='primary-border'
          >
            <p className='button-medium-text'>Lihat Detail Transaksi</p>
          </TWButton>
        )}
      </div>
    </div>
  )
}
