import React from 'react'

import clsx from 'clsx'
import config from '../../../../config'
import StarIcon from '../../../../public/static/icon/star-icon.svg'

const ratingStyle = {
  desktop: 'tw-text-lg tw-font-semibold',
  mobile: 'tw-text-lg',
}

export default function ReviewAverageRating({ averageRating }) {
  return (
    <div className='tw-flex tw-items-center tw-gap-0.5'>
      <div className='tw-flex'>
        <StarIcon className='tw-text-yellow-50' height={24} width={24} />
      </div>
      <p
        className={clsx(
          `tw-text-${config.companyNameCSS}`,
          ratingStyle[config.environment],
        )}
      >
        {averageRating?.toFixed(1)}
      </p>
    </div>
  )
}
