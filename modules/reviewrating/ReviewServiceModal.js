import clsx from 'clsx'
import debounce from 'lodash/debounce'
import dynamic from 'next/dynamic'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import config from '../../../../config'
import { useReviewRatingDetailContext } from '../../../../src/unifyapps/context/ReviewRatingContext'
import {
  usePostReviewRatingAverage,
  usePostServiceReview,
} from '../../services/api/mutations/unifyapps'
import { useGetReviewDetailService } from '../../services/api/queries/unifyapps'
import TWButton from '../../tw-components/buttons/TWButton'
import TWBaseModal from '../../tw-components/modals/TWBaseModal'
import {
  REVIEW_RATING_OFFLINE_SERVICE_DEFAULT_STATE,
  REVIEW_RATING_OFFLINE_SERVICE_INDICATORS,
  REVIEW_RATING_ONLINE_SERVICE_DEFAULT_STATE,
  REVIEW_RATING_ONLINE_SERVICE_INDICATORS,
} from '../../utils/constants'
import getReviewRatingDetailParam from '../../utils/GetReviewRatingDetailParam'
import ReviewAverageRating from './ReviewAverageRating'
import ReviewRatingModalHeader from './ReviewRatingModalHeader'
import ServiceReviewIndicatorCard from './ServiceReviewIndicatorCard'
import ServiceReviewLocation from './ServiceReviewLocation'

const TWTextArea = dynamic(() => import('../../tw-components/TWTextArea'))

export default function ReviewServiceModal({
  isOnline,
  isReviewed,
  show,
  transactionNumber,
  transactionOfflineNumber,
  onClose,
}) {
  const { handleChangeStateObject: changeReviewRatingContext } =
    useReviewRatingDetailContext()

  const id = isOnline ? transactionNumber : transactionOfflineNumber

  const params = useMemo(() => {
    const params = getReviewRatingDetailParam(id, isReviewed)

    return params
  }, [id])

  const { data: reviewDetailServiceData, refetch } = useGetReviewDetailService(
    params,
    {
      enabled: false,
    },
  )

  const { reviews } = reviewDetailServiceData || {}

  const { store_name: storeName } = reviews?.[0] || {}

  const serviceIndicators = isOnline
    ? REVIEW_RATING_ONLINE_SERVICE_INDICATORS
    : REVIEW_RATING_OFFLINE_SERVICE_INDICATORS

  const [serviceReview, setServiceReview] = useState(
    isOnline
      ? REVIEW_RATING_ONLINE_SERVICE_DEFAULT_STATE
      : REVIEW_RATING_OFFLINE_SERVICE_DEFAULT_STATE,
  )

  const [description, setDescription] = useState('')
  const [averageData, setAverageData] = useState({})

  const errorDescription = description?.length > 0 && description?.length < 5

  // Any Rating Category in Service Review that got <= 3, Not Average.
  const isValidDescriptionShow = !!Object.values(serviceReview).find(
    (value) => value <= 3,
  )

  const { mutate: submitReviewAverage } = usePostReviewRatingAverage({
    onSuccess: (res) => {
      setAverageData(res?.data?.data)
    },
  })

  const { mutate: submitServiceReview, isLoading: isLoadingServiceSubmit } =
    usePostServiceReview({
      onSuccess: (res) => {
        changeReviewRatingContext({
          isScreenLoading: false,
        })

        if (!res?.data?.error) {
          changeReviewRatingContext({
            isRefetchList: true,
            isShowInfoBox: true,
            infoBoxMessage: 'Ulasan Berhasil dikirim',
            infoBoxType: 'success-text-white',
          })

          onClose?.()

          return
        }

        changeReviewRatingContext({
          isRefetchList: true,
          isShowInfoBox: true,
          infoBoxMessage: 'Ulasan gagal, mohon mencoba sesaat lagi',
          infoBoxType: 'error-minicart',
        })

        onClose?.()
      },
    })

  const debouncerAverage = useCallback(
    debounce((review) => {
      submitReviewAverage(review)
    }, 1000),
    [],
  )

  const handleSubmitReview = () => {
    let data = {
      ...serviceReview,
    }

    if (isOnline) {
      data = {
        ...data,
        invoice_no: transactionNumber,
      }
    } else {
      data = {
        ...data,
        receipt_id: transactionOfflineNumber,
      }
    }

    if (description) {
      data = {
        ...data,
        description,
      }
    }

    submitServiceReview(data)
  }

  useEffect(() => {
    debouncerAverage(serviceReview)
  }, [serviceReview])

  useEffect(() => {
    if (isLoadingServiceSubmit) {
      changeReviewRatingContext({
        isScreenLoading: true,
      })

      return
    }
  }, [isLoadingServiceSubmit])

  useEffect(() => {
    if (show && id) {
      refetch()
    }
  }, [show])

  if (!id) {
    return
  }

  return (
    <TWBaseModal
      animationType='fade'
      isShow={show}
      modalType='middle-small'
      setIsShow={onClose}
    >
      <div className='tw-flex tw-flex-col'>
        <ReviewRatingModalHeader
          title='Beri Ulasan Pelayanan'
          onClose={onClose}
        />

        <ServiceReviewLocation storeName={storeName} />

        <div className='tw-flex tw-flex-col tw-gap-4 tw-px-6 tw-py-4'>
          <p className='heading-3 tw-text-grey-100'>Kualitas Pelayanan</p>

          <div className='tw-gap-4 tw-grid tw-grid-cols-3'>
            {Object.keys(serviceIndicators).map((indicator) => {
              return (
                <ServiceReviewIndicatorCard
                  key={indicator}
                  name={serviceIndicators[indicator].name}
                  rating={serviceReview[indicator]}
                  setRating={(rating) =>
                    setServiceReview({ ...serviceReview, [indicator]: rating })
                  }
                />
              )
            })}
          </div>

          {isValidDescriptionShow && (
            <div className='tw-flex tw-flex-col tw-gap-2'>
              <p className='ui-text-4-medium tw-text-grey-100'>
                Ulasan untuk kualitas pelayanan
              </p>
              <TWTextArea
                errorMessage={
                  errorDescription ? 'Mohon beri ulasan minimal 5 karakter' : ''
                }
                placeholder='Masukkan ulasan'
                setValue={setDescription}
                value={description}
              />
            </div>
          )}
        </div>

        <div
          className={clsx(
            'tw-flex tw-flex-col tw-w-full',
            'tw-bg-transparent tw-border-t tw-border-grey-20 tw-border-solid tw-gap-3 tw-pt-4 tw-px-6 tw-pb-6',
          )}
        >
          <div className='tw-flex tw-items-center tw-justify-between'>
            <div className='tw-flex tw-flex-col tw-gap-0.5'>
              <p className={clsx('tw-text-grey-40', 'heading-4')}>
                Kualitas pelayanan
              </p>

              <p
                className={'ui-text-2-medium tw-text-' + config.companyNameCSS}
              >
                {averageData?.description}
              </p>
            </div>

            <ReviewAverageRating averageRating={averageData?.average_rating} />
          </div>
          <div className='tw-flex tw-items-center tw-justify-end'>
            <TWButton
              additionalClass='tw-rounded-md'
              disabled={errorDescription}
              handleOnClick={handleSubmitReview}
            >
              <p className='button-medium-text'>Kirim Penilaian</p>
            </TWButton>
          </div>
        </div>
      </div>
    </TWBaseModal>
  )
}
