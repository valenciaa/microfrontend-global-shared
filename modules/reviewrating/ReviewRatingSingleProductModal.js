import React, { useEffect, useMemo } from 'react'
import { useReviewRatingDetailContext } from '../../../../src/unifyapps/context/ReviewRatingContext'
import { useGetReviewDetailProduct } from '../../services/api/queries/unifyapps'
import TWBaseModal from '../../tw-components/modals/TWBaseModal'
import ReviewRatingComplainList from '../../tw-container/reviewrating/component/ReviewRatingComplainList'
import getReviewRatingDetailParam from '../../utils/GetReviewRatingDetailParam'
import ReviewRatingSingleProductModalContent from './ReviewRatingSingleProductModalContent'

export default function ReviewRatingSingleProductModal({
  isDesktop,
  isOnline,
  isReviewed,
  show,
  transactionNumber,
  transactionOfflineNumber,
  onClose,
}) {
  const id = isOnline ? transactionNumber : transactionOfflineNumber

  const {
    state: reviewRatingContext,
    handleChangeStateObject: changeReviewRatingContext,
  } = useReviewRatingDetailContext()

  const params = useMemo(() => {
    const params = getReviewRatingDetailParam(id, isReviewed)

    return params
  }, [id])

  const { data: reviewDetailProductData, refetch } = useGetReviewDetailProduct(
    params,
    {
      enabled: false,
    },
  )

  useEffect(() => {
    if (show && id) {
      refetch()
    }
  }, [show])

  const { reviews } = reviewDetailProductData || {}

  return (
    <>
      <TWBaseModal
        animationType={isDesktop ? 'fade' : 'flyin-full-screen'}
        isFullWidth={isDesktop ? false : true}
        isShow={show && !reviewRatingContext?.isShowRefundComplainList}
        customPadding={isDesktop ? 'tw-w-[496px]' : 'tw-p-4 !tw-rounded-none'}
        modalType={isDesktop ? 'middle-small' : 'bottom'}
        needFloatingCloseButtonElement={isDesktop ? false : true}
        setIsShow={onClose}
      >
        <ReviewRatingSingleProductModalContent
          data={reviews?.[0]}
          invoice_no={transactionNumber}
          isOnline={isOnline}
          isReviewed={isReviewed}
          receipt_id={transactionOfflineNumber}
          onClose={onClose}
        />
      </TWBaseModal>

      {reviewRatingContext?.isShowRefundComplainList && (
        <TWBaseModal
          animationType={isDesktop ? 'fade' : 'flyin-full-screen'}
          isFullWidth={isDesktop ? false : true}
          isShow={reviewRatingContext?.isShowRefundComplainList}
          customPadding={isDesktop ? 'tw-w-[496px]' : 'tw-p-4 !tw-rounded-none'}
          modalType={isDesktop ? 'middle-small' : 'bottom'}
          needFloatingCloseButtonElement={isDesktop ? false : true}
          setIsShow={() =>
            changeReviewRatingContext({
              isShowRefundComplainList: false,
            })
          }
        >
          <ReviewRatingComplainList
            data={reviews?.[0]}
            onCloseModal={() =>
              changeReviewRatingContext({
                isShowRefundComplainList: false,
              })
            }
          />
        </TWBaseModal>
      )}
    </>
  )
}
