import React from 'react'
import StarRatings from 'react-star-ratings'
import { starSvgIconPathUnify } from '../../utils/StarIconSvgPath'

export default function ReviewRatingStar({
  gap = 8,
  rating,
  starDimension = '23px',
  setRating,
  canSelect = true,
}) {
  const starAdditionalProps = canSelect
    ? {
        starHoverColor: '#FFB152',
        changeRating: setRating,
      }
    : {}

  return (
    <div className={'tw-flex star-ratings__container-gap-' + gap}>
      <StarRatings
        key={`stars ${rating}`}
        numberOfStars={5}
        rating={rating}
        starDimension={starDimension}
        starEmptyColor='#E2E4E7'
        starRatedColor='#FFB152'
        starSpacing='0'
        svgIconPath={starSvgIconPathUnify}
        svgIconViewBox='0 0 22 22'
        {...starAdditionalProps}
      />
    </div>
  )
}
