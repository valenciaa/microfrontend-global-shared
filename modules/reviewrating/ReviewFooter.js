import clsx from 'clsx'
import React from 'react'
import config from '../../../../config'
import TWButton from '../../tw-components/buttons/TWButton'
import TWCheckbox from '../../tw-components/inputs/TWCheckbox'

const componentStyle = {
  desktop:
    'tw-bg-transparent tw-border-t tw-border-grey-20 tw-border-solid tw-gap-3 tw-pt-4 tw-px-6 tw-pb-6',
  mobile:
    'tw-bg-white tw-fixed tw-gap-2 tw-bottom-0 tw-left-0 tw-px-4 tw-py-3 tw-shadow-modal-bottom-action',
}

const hideNameStyle = {
  desktop: 'button-medium-text',
  mobile: 'heading-4',
}

const buttonTextStyle = {
  desktop: 'button-medium-text',
  mobile: 'button-big-text',
}

export default function ReviewFooter({
  containerClassName = '',
  anonymus,
  setAnonymus,
  submitReview,
}) {
  return (
    <div
      className={clsx(
        'tw-flex tw-flex-col tw-w-full',
        containerClassName || componentStyle[config.environment],
      )}
    >
      <div className='tw-flex tw-items-center tw-justify-between'>
        <div className='tw-flex tw-gap-2 tw-items-center'>
          <TWCheckbox handleOnChange={setAnonymus} isChecked={anonymus} />
          <p
            className={clsx(
              'tw-text-grey-100',
              hideNameStyle[config.environment],
            )}
          >
            Sembunyikan namamu
          </p>
        </div>
        <TWButton additionalClass='tw-rounded-md' handleOnClick={submitReview}>
          <p className={clsx(buttonTextStyle[config.environment])}>
            Kirim Penilaian
          </p>
        </TWButton>
      </div>
    </div>
  )
}
