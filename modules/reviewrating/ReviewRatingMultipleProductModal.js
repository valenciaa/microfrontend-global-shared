import clsx from 'clsx'
import React from 'react'

import config from '../../../../config'
import TWBaseModal from '../../tw-components/modals/TWBaseModal'
import ReviewRatingMultipleProductModalContent from './ReviewRatingMultipleProductModalContent'

const contentClass = {
  desktop: 'tw-rounded-lg',
  mobile: 'tw-mt-12 tw-rounded-none',
}

const dialogClass = {
  desktop: 'tw-w-[496px]',
  mobile: 'tw-m-0',
}

export default function ReviewRatingMultipleProductModal({
  imageSrc,
  isDesktop,
  productName,
  setValue,
  show,
  onClose,
  value,
}) {
  return (
    <TWBaseModal
      animationType={isDesktop ? 'fade' : 'flyin-full-screen'}
      isFullWidth={isDesktop ? false : true}
      isShow={show}
      modalType={isDesktop ? 'middle-small' : 'bottom'}
      needFloatingCloseButtonElement={isDesktop ? false : true}
      setIsShow={onClose}
    >
      <div className={clsx(dialogClass[config.environment])}>
        <div className={clsx(contentClass[config.environment])}>
          <ReviewRatingMultipleProductModalContent
            imageSrc={imageSrc}
            productName={productName}
            reviewProduct={value}
            setReviewProduct={setValue}
            onClose={onClose}
          />
        </div>
      </div>
    </TWBaseModal>
  )
}
