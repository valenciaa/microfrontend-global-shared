import isEmpty from 'lodash/isEmpty'
import React, { useEffect, useMemo } from 'react'
import { useGetReviewDetailProduct } from '../../services/api/queries/unifyapps'
import TWBaseModal from '../../tw-components/modals/TWBaseModal'
import getReviewRatingDetailParam from '../../utils/GetReviewRatingDetailParam'
import { mixpanelSeeReview } from '../../utils/MixpanelUnify'
import ReviewedProductCard from './ReviewedProductCard'
import ReviewRatingModalHeader from './ReviewRatingModalHeader'
import TransactionLocation from './TransactionLocation'

export default function DetailReviewProductModal({
  isReviewed,
  isOnline,
  show,
  transactionNumber,
  transactionOfflineNumber,
  onClose,
}) {
  const id = isOnline ? transactionNumber : transactionOfflineNumber

  const params = useMemo(() => {
    const params = getReviewRatingDetailParam(id, isReviewed)

    return params
  }, [id])

  const { data: reviewDetailProductData, refetch } = useGetReviewDetailProduct(
    params,
    {
      enabled: false,
    },
  )

  const { reviews } = reviewDetailProductData || {}

  useEffect(() => {
    if (show && id) {
      refetch()
    }
  }, [show])

  useEffect(() => {
    if (!isEmpty(reviews)) {
      mixpanelSeeReview(reviews)
    }
  }, [reviews])

  if (isEmpty(reviews)) {
    // Error Boundary
    return
  }

  return (
    <TWBaseModal
      backdropOnClose
      animationType='fade'
      customPadding='tw-w-[740px] tw-h-[545px]'
      isShow={show}
      modalType='middle-small'
      setIsShow={onClose}
    >
      <div className='tw-flex tw-flex-col tw-h-[530px] tw-overflow-y-auto'>
        <ReviewRatingModalHeader
          title='Detail Ulasan Produk'
          onClose={onClose}
        />
        <div className='tw-px-6 tw-pb-4'>
          <TransactionLocation
            data={reviews?.[0]}
            reviewDate={reviews?.[0]?.posted_at}
          />
        </div>

        <div className='tw-border-4 tw-border-solid tw-border-grey-20' />

        <div className='tw-flex tw-flex-col tw-px-6 tw-pt-4 tw-pb-6 tw-gap-3'>
          {reviews?.map((item) => (
            <ReviewedProductCard data={item} key={item?.sku} />
          ))}
        </div>
      </div>
    </TWBaseModal>
  )
}
