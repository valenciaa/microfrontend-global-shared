import { useState } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import TWBaseModal from '../../tw-components/modals/TWBaseModal'

export default function ReviewRatingInformation() {
  const isMobile = config.environment === 'mobile'

  const [isOpenSnk, setIsOpenSnk] = useState(false)

  return (
    <>
      <div className='tw-flex tw-flex-col tw-gap-1 tw-p-3 tw-relative tw-text-white tw-rounded-md tw-bg-gradient-to-r tw-from-[#FD8210] tw-to-[#FFAF33]'>
        <p className='tw-text-sm tw-font-bold'>
          Beri Ulasan dan Dapatkan Voucher Hingga Rp50.000
        </p>
        <p className='tw-text-xs'>
          Kamu berpotensi mendapatkan potongan langsung untuk transaksi
          berikutnya.{' '}
          <span
            className='tw-underline tw-font-bold tw-cursor-pointer'
            onClick={() => setIsOpenSnk(!isOpenSnk)}
          >
            Baca syarat & ketentuan
          </span>
        </p>

        <LazyLoadImage
          alt='price-tag-icon'
          className='tw-absolute -tw-right-2 -tw-bottom-9 tw-opacity-25'
          height={113}
          placeholder={<Skeleton height={113} width={113} />}
          src={`${config.assetsURL}icon/price-tag-icon.svg`}
          width={113}
        />
      </div>

      {isOpenSnk && !isMobile && (
        <TWBaseModal
          animationType='fade'
          isShow={isOpenSnk}
          modalType='middle-small'
        >
          <div className='tw-max-w-2xl tw-flex tw-flex-col tw-gap-3 tw-p-2'>
            <div className='tw-flex tw-justify-between'>
              <div className='tw-text-xl tw-text-grey-100 tw-font-bold'>
                Syarat dan Ketentuan
              </div>
              <div>
                <LazyLoadImage
                  className='cursor-pointer'
                  height={24}
                  src={`${config.assetsURL}icon/close-dark.svg`}
                  width={24}
                  onClick={() => setIsOpenSnk(false)}
                />
              </div>
            </div>
            <div className='tw-list-decimal tw-list-inside tw-space-y-2'>
              <li>
                Ulasan diberikan maksimal 30 hari setelah tanggal transaksi
                selesai
              </li>
              <li>
                Voucher hanya berlaku jika customer melampirkan 3 buah foto
                dalam ulasannya
              </li>
              <li>Voucher hanya diberikan maksimum 5x dalam 1 tahun</li>
              <li>
                Nilai 1 voucher Rp10.000 dapat digunakan dengan minimum belanja
                10x nilai voucher (berlaku kelipatan)
              </li>
            </div>
          </div>
        </TWBaseModal>
      )}

      {isOpenSnk && isMobile && (
        <TWBaseModal
          isFullWidth
          needFloatingCloseButtonElement
          animationType='fade'
          isShow={isOpenSnk}
          modalType='bottom'
          setIsShow={() => setIsOpenSnk(!isOpenSnk)}
        >
          <div className='tw-px-2'>
            <div className='tw-text-grey-100 tw-font-bold tw-py-4'>
              <b>Syarat dan Ketentuan</b>
            </div>

            <div className='tw-h-[50vh] tw-overflow-y-auto tw-flex tw-flex-col tw-w-full tw-ml-1'>
              <div className='tw-list-decimal tw-list-inside tw-space-y-2'>
                <li>
                  Ulasan diberikan maksimal 30 hari setelah tanggal transaksi
                  selesai
                </li>
                <li>
                  Voucher hanya berlaku jika customer melampirkan 3 buah foto
                  dalam ulasannya
                </li>
                <li>Voucher hanya diberikan maksimum 5x dalam 1 tahun</li>
                <li>
                  Nilai 1 voucher Rp10.000 dapat digunakan dengan minimum
                  belanja 10x nilai voucher (berlaku kelipatan)
                </li>
              </div>
            </div>
          </div>
        </TWBaseModal>
      )}
    </>
  )
}
