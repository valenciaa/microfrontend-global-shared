import clsx from 'clsx'
import React from 'react'
import config from '../../../../config'
import ReviewRatingStar from './ReviewRatingStar'

const indicatorTextStyle = {
  desktop: 'ui-text-4-medium',
  mobile: 'ui-text-3',
}

export default function ServiceReviewIndicatorCard({
  name,
  rating,
  setRating,
}) {
  if (rating === 0) {
    return null
  }

  const starDimension = config.environment === 'desktop' ? '24px' : '12px'
  return (
    <div className='tw-border tw-border-grey-20 tw-border-solid tw-flex tw-flex-col tw-gap-1 tw-items-center tw-p-2 tw-rounded-lg'>
      <p
        className={clsx(
          'text-center tw-text-grey-100',
          indicatorTextStyle[config.environment],
        )}
      >
        {name}
      </p>

      <ReviewRatingStar
        gap={config.environment === 'desktop' ? 12 : 4}
        rating={rating}
        setRating={setRating}
        starDimension={starDimension}
      />
    </div>
  )
}
