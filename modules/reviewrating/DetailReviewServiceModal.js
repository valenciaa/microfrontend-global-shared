import dayjs from 'dayjs'
import isEmpty from 'lodash/isEmpty'
import React, { useEffect, useMemo } from 'react'
import { useGetReviewDetailService } from '../../services/api/queries/unifyapps'
import TWBaseModal from '../../tw-components/modals/TWBaseModal'
import {
  REVIEW_RATING_OFFLINE_SERVICE_INDICATORS,
  REVIEW_RATING_ONLINE_SERVICE_INDICATORS,
} from '../../utils/constants'
import getReviewRatingDetailParam from '../../utils/GetReviewRatingDetailParam'
import ReviewRatingModalHeader from './ReviewRatingModalHeader'
import ServiceReviewIndicatorCard from './ServiceReviewIndicatorCard'
import ServiceReviewInformation from './ServiceReviewInformation'
import ServiceReviewSummary from './ServiceReviewSummary'
import TransactionLocation from './TransactionLocation'

export default function DetailReviewServiceModal({
  isReviewed,
  isOnline,
  show,
  transactionNumber,
  transactionOfflineNumber,
  onClose,
}) {
  const id = isOnline ? transactionNumber : transactionOfflineNumber

  const params = useMemo(() => {
    const params = getReviewRatingDetailParam(id, isReviewed)

    return params
  }, [id])

  const { data: reviewDetailServiceData, refetch } = useGetReviewDetailService(
    params,
    {
      enabled: false,
    },
  )

  const { reviews } = reviewDetailServiceData || {}

  const { review_service, review_date, customer } = reviews?.[0] || {}

  const { average_rating, average_description, created_at, description } =
    review_service || {}

  const reviewDate = dayjs(created_at).format('DD MMM YYYY')

  const customerName = `${customer?.first_name}${
    customer?.last_name ? ` ${customer.last_name}` : ''
  }`

  const serviceIndicators = id?.includes('INV')
    ? REVIEW_RATING_ONLINE_SERVICE_INDICATORS
    : REVIEW_RATING_OFFLINE_SERVICE_INDICATORS

  useEffect(() => {
    if (show && id) {
      refetch()
    }
  }, [show])

  if (isEmpty(reviews)) {
    return
  }

  return (
    <TWBaseModal
      backdropOnClose
      animationType='fade'
      customPadding='tw-w-[740px] tw-h-[545px]'
      isShow={show}
      modalType='middle-small'
      setIsShow={onClose}
    >
      <div className='tw-flex tw-flex-col tw-h-[530px] tw-overflow-y-auto'>
        <ReviewRatingModalHeader
          title='Detail Ulasan Pelayanan'
          onClose={onClose}
        />

        <div className='tw-px-6 tw-pb-4'>
          <TransactionLocation data={reviews?.[0]} reviewDate={review_date} />
        </div>

        <div className='tw-border-4 tw-border-solid tw-border-grey-20' />

        <div className='tw-border-t-4 tw-border-grey-20 tw-border-solid tw-pt-4 tw-pb-6 tw-px-6'>
          <div className='tw-border tw-border-grey-20 tw-border-solid tw-flex tw-flex-col tw-gap-4 tw-px-4 tw-py-6 tw-rounded-lg'>
            <ServiceReviewInformation
              averageRating={average_rating}
              customerName={customerName}
              reviewDate={reviewDate}
            />

            <ServiceReviewSummary
              averageDescription={average_description}
              averageRating={average_rating}
            />
            <div className='tw-flex tw-flex-col tw-gap-4'>
              <p className='heading-3 tw-text-grey-100'>Kualitas Pelayanan</p>

              <div className='tw-gap-4 tw-grid tw-grid-cols-3'>
                {Object.keys(serviceIndicators).map((indicator) => {
                  return (
                    <ServiceReviewIndicatorCard
                      key={indicator}
                      name={serviceIndicators[indicator].name}
                      rating={review_service?.[indicator]}
                    />
                  )
                })}
              </div>

              {description && (
                <div className='tw-flex tw-flex-col tw-gap-1'>
                  <p className='tw-text-grey-40 ui-text-3'>Ulasan</p>
                  <p className='ui-text-2 tw-text-grey-100 tw-break-words'>
                    {description}
                  </p>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </TWBaseModal>
  )
}
