import clsx from 'clsx'
import dayjs from 'dayjs'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import TWButton from '../../tw-components/buttons/TWButton'
import { mixpanelTrack } from '../../utils/MixpanelWrapper'

const ReviewRatingSingleProductModal = dynamic(
  () => import('./ReviewRatingSingleProductModal'),
)

const ReviewServiceModal = dynamic(() => import('./ReviewServiceModal'))

const DetailReviewServiceModal = dynamic(
  () => import('./DetailReviewServiceModal'),
)

const DetailReviewProductModal = dynamic(
  () => import('./DetailReviewProductModal'),
)

const paddingStyle = {
  desktop: 'tw-p-4',
  mobile: 'tw-p-3',
}

const storeNameStyle = {
  desktop: 'heading-3',
  mobile: 'heading-5',
}

export default function ReviewRatingItem({ data, isReviewed }) {
  const router = useRouter()
  const brandLogoSize = 36
  const isDesktop = config.environment === 'desktop'

  const productImageSize = isDesktop ? 60 : 48

  const {
    invoice_no,
    receipt_id,
    logo,
    items,
    shopping_type,
    store_code,
    store_name,
    transaction_date,
    is_product_reviewed,
    is_service_reviewed,
    items_total,
    reviewed_date,
  } = data

  const isOnline = shopping_type === 'online'

  const { image_url, product_name, sku } = items[0]
  const countProducts = items_total - 1

  const id = isOnline ? invoice_no : receipt_id
  const orderType = isOnline ? 'Belanja Online' : 'Belanja di Toko Fisik'

  const headerRight = isReviewed
    ? 'Diulas: ' + dayjs(reviewed_date).format('D MMM YYYY')
    : 'Tanggal Transaksi: ' + dayjs(transaction_date).format('D MMM YYYY')

  const isNeedReviewProduct = !![0, 5].includes(is_product_reviewed)
  const isNeedReviewService = !!(is_service_reviewed === 0)

  const needReviewProductButton =
    is_product_reviewed === 5 ? 'Perbarui Ulasan Produk' : 'Beri Ulasan Produk'
  const reviewProductTextButton = !isNeedReviewProduct
    ? 'Lihat Ulasan Produk'
    : needReviewProductButton
  const reviewProductButtonClass = !![5, 10].includes(is_product_reviewed)

  const [isOpenSingleProductReviewModal, setIsOpenSingleProductReviewModal] =
    useState(false)
  const [isOpenReviewServiceModal, setIsOpenReviewServiceModal] =
    useState(false)
  const [isOpenDetailReviewServiceModal, setIsOpenDetailReviewServiceModal] =
    useState(false)
  const [isOpenReviewedProductModal, setIsOpenReviewedProductModal] =
    useState(false)

  const handleReviewService = (isServiceReviewed) => {
    if (!isServiceReviewed) {
      mixpanelTrack('Click Button Beri Ulasan Pelayanan', {
        'Location': 'Riwayat Ulasan',
        'Store Code': store_code,
        'Store': shopping_type === 'online' ? 'Online' : 'Offline',
      })
    }

    if (isDesktop) {
      if (isServiceReviewed) {
        setIsOpenDetailReviewServiceModal(true)

        return
      }

      setIsOpenReviewServiceModal(true)

      return
    }

    const urlPath = isServiceReviewed ? 'post-review' : 'review'

    router.push({
      pathname: `/my-account/review-rating/${urlPath}/service/${id}`,
      query: { product_reviewed: is_product_reviewed },
    })
  }

  const handleReviewProducts = (isProductReviewed) => {
    if (isProductReviewed) {
      if (isDesktop) {
        setIsOpenReviewedProductModal(true)

        return
      }

      router.push({
        pathname: `/my-account/review-rating/post-review/products/${id}`,
        query: { product_reviewed: is_product_reviewed },
      })

      return
    }

    if (is_product_reviewed === 5) {
      mixpanelTrack('Click Button Perbarui Ulasan', {
        Location: 'Riwayat Ulasan',
      })
    } else {
      mixpanelTrack('Click Button Beri Ulasan Produk', {
        Location: 'Riwayat Ulasan',
      })
    }

    if (items_total === 1) {
      setIsOpenSingleProductReviewModal(true)

      return
    }

    if (isDesktop) {
      router.push({
        pathname: 'my-account',
        query: {
          tab: 'review-rating',
          child_tab: 'review',
          product_reviewed: is_product_reviewed,
          id,
        },
      })

      return
    }

    router.push({
      pathname: '/my-account/review-rating/review/products/' + id,
      query: { product_reviewed: is_product_reviewed },
    })
  }

  return (
    <>
      <div
        className={clsx(
          'tw-border tw-border-grey-20 tw-border-solid tw-flex tw-flex-col tw-gap-3 tw-rounded-lg',
          paddingStyle[config.environment],
        )}
      >
        <div className='tw-flex tw-justify-between tw-text-grey-40'>
          <p className='ui-text-4'>{id}</p>
          <p className='ui-text-4'>{headerRight}</p>
        </div>

        <div className='tw-flex tw-flex-col'>
          <div className='tw-flex tw-items-center tw-gap-3 tw-pb-3'>
            <div className='tw-border tw-border-grey-20 tw-border-solid tw-flex tw-items-center tw-justify-center tw-h-12 tw-w-12 tw-rounded-full'>
              <CustomLazyLoadImage
                alt='store-logo'
                height={brandLogoSize}
                placeholder={<Skeleton />}
                src={logo}
                width={brandLogoSize}
              />
            </div>

            <div className='tw-flex tw-flex-col tw-gap-1'>
              <p className='tw-text-grey-50 ui-text-4'>{orderType}</p>
              <p
                className={clsx(
                  'tw-text-grey-100',
                  storeNameStyle[config.environment],
                )}
              >
                {store_name}
              </p>
            </div>
          </div>
          <div className='tw-border-t tw-border-grey-10 tw-border-solid tw-flex tw-gap-3 tw-py-3'>
            <div className='tw-flex'>
              <CustomLazyLoadImage
                alt={`${sku}-product-image`}
                height={productImageSize}
                placeholder={<Skeleton />}
                src={image_url}
                width={productImageSize}
              />
            </div>

            <div className='tw-flex tw-flex-col tw-justify-center tw-gap-1'>
              <p className='tw-text-grey-100 ui-text-3'>{product_name}</p>

              {countProducts > 0 && (
                <p className='tw-text-grey-40 ui-text-4'>
                  +{countProducts} Lainnya
                </p>
              )}
            </div>
          </div>
          <div
            className={clsx(
              isDesktop
                ? 'tw-border-t tw-border-grey-20 tw-border-solid tw-pt-3 tw-justify-end'
                : 'tw-justify-between',
              'tw-flex tw-gap-3 ',
            )}
          >
            <TWButton
              additionalClass={clsx('tw-rounded-md', isDesktop ? '' : 'w-full')}
              customPadding='tw-px-2 tw-py-2'
              handleOnClick={() => handleReviewService(!isNeedReviewService)}
              type={isNeedReviewService ? 'primary' : 'primary-border'}
            >
              <p className='button-medium-text'>
                {isNeedReviewService
                  ? 'Beri Ulasan Pelayanan'
                  : 'Lihat Ulasan Pelayanan'}
              </p>
            </TWButton>

            <TWButton
              additionalClass={clsx('tw-rounded-md', isDesktop ? '' : 'w-full')}
              customPadding='tw-px-2 tw-py-2'
              handleOnClick={() => handleReviewProducts(!isNeedReviewProduct)}
              type={!reviewProductButtonClass ? 'primary' : 'primary-border'}
            >
              <p className='button-medium-text'>{reviewProductTextButton}</p>
            </TWButton>
          </div>
        </div>
      </div>

      {isOpenReviewServiceModal && (
        <ReviewServiceModal
          isOnline={isOnline}
          isReviewed={is_service_reviewed}
          show={isOpenReviewServiceModal}
          transactionNumber={invoice_no}
          transactionOfflineNumber={receipt_id}
          onClose={() => setIsOpenReviewServiceModal(false)}
        />
      )}

      {isOpenSingleProductReviewModal && (
        <ReviewRatingSingleProductModal
          isDesktop={isDesktop}
          isOnline={isOnline}
          isReviewed={is_product_reviewed}
          show={isOpenSingleProductReviewModal}
          transactionNumber={invoice_no}
          transactionOfflineNumber={receipt_id}
          onClose={() => setIsOpenSingleProductReviewModal(false)}
        />
      )}

      {isOpenDetailReviewServiceModal && (
        <DetailReviewServiceModal
          isOnline={isOnline}
          isReviewed={is_service_reviewed}
          show={isOpenDetailReviewServiceModal}
          transactionNumber={invoice_no}
          transactionOfflineNumber={receipt_id}
          onClose={() => setIsOpenDetailReviewServiceModal(false)}
        />
      )}

      {isOpenReviewedProductModal && (
        <DetailReviewProductModal
          isOnline={isOnline}
          isReviewed={is_product_reviewed}
          show={isOpenReviewedProductModal}
          transactionNumber={invoice_no}
          transactionOfflineNumber={receipt_id}
          onClose={() => setIsOpenReviewedProductModal(false)}
        />
      )}
    </>
  )
}
