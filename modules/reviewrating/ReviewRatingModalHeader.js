import React from 'react'
import CloseIcon from '../../../../public/static/icon/close.svg'

export default function ReviewRatingModalHeader({ title, onClose }) {
  return (
    <div className='tw-px-6 tw-pt-6 tw-pb-4'>
      <div className='tw-flex tw-items-center tw-justify-between tw-py-3'>
        <p className='heading-2 tw-text-grey-100'>{title}</p>
        <CloseIcon
          alt='Close Button'
          className='cursor-pointer tw-text-grey-100'
          height={24}
          width={24}
          onClick={onClose}
        />
      </div>
    </div>
  )
}
