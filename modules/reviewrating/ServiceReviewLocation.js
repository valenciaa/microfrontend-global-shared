import React from 'react'
import clsx from 'clsx'
import config from '../../../../config'

const componentStyle = {
  desktop: 'tw-border-t tw-px-6 tw-py-4',
  mobile: 'tw-border-b-4 tw-px-4 tw-pt-4 tw-pb-3',
}

const sectionTitleStyle = {
  desktop: 'tw-text-grey-40 ui-text-3',
  mobile: 'tw-text-grey-50 ui-text-4',
}

const storeNameStyle = {
  desktop: 'tw-text-grey-100 ui-text-2',
  mobile: 'tw-text-black ui-text-1',
}

export default function ServiceReviewLocation({ storeName = '' }) {
  return (
    <div
      className={clsx(
        ' tw-border-grey-20 tw-border-solid tw-flex tw-flex-col tw-gap-1',
        componentStyle[config.environment],
      )}
    >
      <p className={clsx(sectionTitleStyle[config.environment])}>Lokasi</p>
      <p className={clsx(storeNameStyle[config.environment])}>{storeName}</p>
    </div>
  )
}
