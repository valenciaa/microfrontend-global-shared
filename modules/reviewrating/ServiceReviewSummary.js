import clsx from 'clsx'
import React from 'react'

import config from '../../../../config'
import ReviewAverageRating from './ReviewAverageRating'

const componentStyle = {
  desktop: 'tw-p-3',
  mobile: 'tw-py-2 tw-px-3',
}

export default function ServiceReviewSummary({
  averageRating,
  averageDescription,
}) {
  return (
    <div
      className={clsx(
        'tw-bg-orange-10 tw-flex tw-items-center tw-justify-between tw-rounded-lg',
        `tw-text-${config.companyNameCSS}`,
        componentStyle[config.environment],
      )}
    >
      <div className='tw-flex tw-flex-col tw-gap-2'>
        <p className='ui-text-2-medium'>{averageDescription}</p>
      </div>
      <ReviewAverageRating averageRating={averageRating} />
    </div>
  )
}
