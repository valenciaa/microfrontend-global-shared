import clsx from 'clsx'
import isEmpty from 'lodash/isEmpty'
import React, { useEffect, useState } from 'react'
import config from '../../../../config'
import CloseButton from '../../../../public/static/icon/close.svg'
import { useReviewRatingDetailContext } from '../../../../src/unifyapps/context/ReviewRatingContext'
import { useCartAuthContext } from '../../context/CartAuthContext'
import { usePostSingleProductReview } from '../../services/api/mutations/unifyapps'
import TWButton from '../../tw-components/buttons/TWButton'
import TWCheckbox from '../../tw-components/inputs/TWCheckbox'
import RefundSuggestionInfo from '../../tw-container/reviewrating/component/RefundSuggestionInfo'
import { mixpanelAddUpdateReview } from '../../utils/MixpanelUnify'
import ReviewRatingCard from './ReviewRatingCard'
import ReviewRatingInformation from './ReviewRatingInformation'

const modalContentStyle = {
  desktop: 'tw-border-t tw-border-solid tw-border-grey-20 tw-px-6 tw-py-4',
  mobile: 'tw-px-1',
}

const modalFooterStyle = {
  desktop:
    'tw-border-t tw-border-solid tw-border-grey-20 tw-pt-4 tw-pb-6 tw-px-6',
  mobile: 'tw-pt-3 tw-px-1 tw-pb-2',
}

export default function ReviewRatingSingleProductModalContent({
  data,
  invoice_no,
  isOnline,
  isReviewed,
  receipt_id,
  onClose,
}) {
  const { handleChangeStateObject: changeReviewRatingContext } =
    useReviewRatingDetailContext()

  const {
    image_url,
    sku,
    product_name,
    rating,
    review_images,
    description,
    shopping_type,
    is_refund_eligible,
  } = data || {}

  const { state: cartAuthContext } = useCartAuthContext()
  const user = cartAuthContext?.auth?.user

  const customerName =
    user?.name ||
    `${user?.first_name}${user?.last_name ? ` ${user.last_name}` : ''}`

  const {
    mutate: submitSingleProductReview,
    isLoading: isLoadingSingleProductSubmit,
  } = usePostSingleProductReview({
    onSuccess: (res) => {
      changeReviewRatingContext({
        isScreenLoading: false,
      })

      if (!res?.data?.error) {
        changeReviewRatingContext({
          isRefetchList: true,
          isShowInfoBox: true,
          infoBoxMessage: `Ulasan Berhasil ${
            isReviewed === 5 ? 'diperbarui' : 'dikirim'
          }`,
          infoBoxType: 'success-text-white',
        })

        onClose?.()

        return
      }

      changeReviewRatingContext({
        isRefetchList: true,
        isShowInfoBox: true,
        infoBoxMessage: 'Ulasan gagal, mohon mencoba sesaat lagi',
        infoBoxType: 'error-minicart',
      })

      onClose?.()
    },
  })

  const [singleReviewProduct, setSingleReviewProduct] = useState({})
  const [anonymousReview, setAnonymousReview] = useState(false)

  const [isShowRefundSuggestion, setIsShowRefundSuggestion] = useState(false)

  const handleSetSingleReviewProduct = (sku, data, value) => {
    setSingleReviewProduct({ ...singleReviewProduct, [data]: value })
  }

  const handleSubmitSingleProduct = () => {
    const eventName = isReviewed === 5 ? 'Perbarui Ulasan' : 'Beri Ulasan'
    const mixpanelPayload = [{ sku, product_name, shopping_type }]

    mixpanelAddUpdateReview(eventName, mixpanelPayload)

    const dataForSubmit = {
      ...singleReviewProduct,
      ...(isOnline ? { invoice_no } : { receipt_id }),
      is_anonymous: anonymousReview ? 1 : 0,
    }

    submitSingleProductReview(dataForSubmit)
  }

  useEffect(() => {
    if (isEmpty(data)) {
      return
    }

    const essentialsData = {
      sku: sku,
      review_as: 'customer',
      customer_name: customerName,
    }

    if (isReviewed === 5) {
      const revisedData = {
        rating: rating,
        description: description,
        images: review_images,
      }

      setSingleReviewProduct({ ...essentialsData, ...revisedData })

      setAnonymousReview(data.is_anonymous ? true : false)

      return
    }

    setSingleReviewProduct({
      ...essentialsData,
      rating: 5,
      description: '',
      images: [],
    })
  }, [data])

  useEffect(() => {
    if (isLoadingSingleProductSubmit) {
      changeReviewRatingContext({
        isScreenLoading: true,
      })

      return
    }
  }, [isLoadingSingleProductSubmit])

  useEffect(() => {
    const isContainsLowRating =
      singleReviewProduct?.rating < 4 && is_refund_eligible

    if (isContainsLowRating && !isShowRefundSuggestion) {
      setIsShowRefundSuggestion(true)
    }
  }, [singleReviewProduct])

  return (
    <>
      <div className='tw-flex tw-flex-col'>
        {config.environment === 'desktop' && (
          <div className='tw-px-6 tw-pt-6 tw-pb-4'>
            <div className='tw-flex tw-justify-between tw-py-3'>
              <div className='tw-text-grey-100'>
                <p className='heading-2'>Beri Ulasan Produk</p>
              </div>
              <div className='tw-flex'>
                <CloseButton
                  alt='Close Button'
                  className='tw-text-grey-100 tw-cursor-pointer'
                  height='1.5em'
                  width='1.5em'
                  onClick={onClose}
                />
              </div>
            </div>
          </div>
        )}

        <div
          className={clsx(
            'tw-flex tw-flex-col tw-gap-2',
            modalContentStyle[config.environment],
          )}
        >
          {config.environment === 'mobile' && (
            <p className='heading-2 tw-text-grey-100'>Beri Ulasan Produk</p>
          )}

          <ReviewRatingInformation />

          <div className='tw-flex tw-flex-col tw-gap-4'>
            <ReviewRatingCard
              isSingleProduct
              imageSrc={image_url}
              isProductRevised={isReviewed === 5}
              productName={product_name}
              productSku={sku}
              reviewedImages={review_images}
              setValue={handleSetSingleReviewProduct}
              value={singleReviewProduct}
            />
          </div>
        </div>
        <div
          className={clsx(
            'tw-flex tw-items-center tw-justify-between',
            modalFooterStyle[config.environment],
          )}
        >
          <div className='tw-flex tw-items-center tw-gap-2'>
            <TWCheckbox
              handleOnChange={() => setAnonymousReview(!anonymousReview)}
              isChecked={anonymousReview}
            />
            <p className='heading-4 tw-text-grey-100'>Sembunyikan namamu</p>
          </div>

          <TWButton
            additionalClass='tw-rounded-md'
            handleOnClick={() => handleSubmitSingleProduct()}
          >
            <p className='button-big-text'>Kirim Penilaian</p>
          </TWButton>
        </div>
      </div>

      {isShowRefundSuggestion && (
        <RefundSuggestionInfo
          duration={3000}
          action={() =>
            changeReviewRatingContext({ isShowRefundComplainList: true })
          }
          setToggle={() => setIsShowRefundSuggestion(false)}
        />
      )}
    </>
  )
}
