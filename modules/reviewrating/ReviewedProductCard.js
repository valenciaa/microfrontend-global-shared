import clsx from 'clsx'
import isEmpty from 'lodash/isEmpty'
import React from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import ReviewRatingStar from './ReviewRatingStar'

const paddingStyle = {
  desktop: 'tw-p-4',
  mobile: 'tw-p-3',
}

export default function ReviewedProductCard({ data }) {
  const productImageSize = config.environment === 'desktop' ? 60 : 48

  const { image_url, sku, product_name, rating, review_images, description } =
    data

  return (
    <div
      className={clsx(
        'tw-border tw-border-grey-20 tw-border-solid tw-flex tw-flex-col tw-gap-3 tw-rounded-lg',
        paddingStyle[config.environment],
      )}
    >
      <div className='tw-flex tw-flex-row tw-gap-3'>
        <div className='tw-flex'>
          <CustomLazyLoadImage
            alt={`${sku}-product-image`}
            height={productImageSize}
            placeholder={<Skeleton />}
            src={image_url}
            width={productImageSize}
          />
        </div>

        <div className='tw-flex tw-flex-col tw-justify-center tw-gap-1'>
          <p className='tw-text-grey-100 tw-font-bold tw-text-sm'>
            {product_name}
          </p>

          <ReviewRatingStar
            canSelect={false}
            gap={8}
            rating={rating}
            starDimension='20px'
          />
        </div>
      </div>

      {!isEmpty(review_images) && (
        <div className='tw-flex tw-flex-row tw-gap-3'>
          {review_images?.map((imageData, index) => (
            <CustomLazyLoadImage
              alt={'Review Image ' + (index + 0)}
              className='tw-rounded tw-flex'
              height={44}
              key={index}
              src={imageData.image_url}
              width={44}
            />
          ))}
        </div>
      )}

      {description && (
        <div className='tw-flex tw-flex-col tw-gap-1'>
          <p className='tw-text-grey-40 tw-text-xxs'>Ulasan</p>
          <p className='tw-text-grey-100 tw-text-xs tw-break-words'>
            {description}
          </p>
        </div>
      )}
    </div>
  )
}
