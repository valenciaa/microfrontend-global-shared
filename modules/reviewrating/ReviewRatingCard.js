import clsx from 'clsx'
import isEmpty from 'lodash/isEmpty'
import React, { useCallback, useEffect, useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import CameraIcon from '../../../../public/static/icon/camera-icon.svg'
import DocumentIcon from '../../../../public/static/icon/document-icon.svg'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import { useuploadMultipleReviewImages } from '../../services/api/mutations/global'
import TWButton from '../../tw-components/buttons/TWButton'
import TWTextArea from '../../tw-components/TWTextArea'

import { useDropzone } from 'react-dropzone'
import FileResizer from 'react-image-file-resizer'

import { useReviewRatingDetailContext } from '../../../../src/unifyapps/context/ReviewRatingContext'
import { REVIEW_RATING_TEXT_AREA_PLACEHOLDER } from '../../utils/constants'
import ReviewRatingMultipleProductModal from './ReviewRatingMultipleProductModal'
import ReviewRatingPreviewImageCard from './ReviewRatingPreviewImageCard'
import ReviewRatingStar from './ReviewRatingStar'

const productNameStyle = {
  desktop: 'price-normal',
  mobile: 'heading-3',
}

const cardStyle = {
  desktop: 'tw-gap-4',
  mobile: 'tw-gap-3',
}

export default function ReviewRatingCard({
  imageSrc,
  isProductRevised,
  isSingleProduct = false,
  productName,
  productSku,
  reviewedImages,
  value,
  setValue,
}) {
  const { handleChangeStateObject: changeReviewRatingContext } =
    useReviewRatingDetailContext()

  const isDesktop = config.environment === 'desktop'

  const [isFirstTimeLoad, setIsFirstTimeLoad] = useState(true)
  const [isOpenModal, setIsOpenModal] = useState(false)
  const [imagesData, setImagesData] = useState([])
  const [previewImages, setPreviewImages] = useState([])

  const isUploadPhotoDisabled = previewImages?.length >= 3

  const {
    mutate: uploadMultipleReviewImages,
    isLoading: isLoadingUploadMultipleReviewImages,
  } = useuploadMultipleReviewImages({
    onSuccess: (response) => {
      changeReviewRatingContext({
        isScreenLoading: false,
      })

      if (!isEmpty(response?.data?.data)) {
        setPreviewImages((prev) => [...prev, ...response.data.data])

        changeReviewRatingContext({
          isShowInfoBox: true,
          infoBoxMessage: 'Foto berhasil ditambahkan',
          infoBoxType: 'success-text-white',
        })

        return
      }

      changeReviewRatingContext({
        isShowInfoBox: true,
        infoBoxMessage: 'Foto gagal ditambahkan, mohon mencoba sesaat lagi',
        infoBoxType: 'error-minicart',
      })
    },
  })

  const onDrop = useCallback(
    async (acceptedFiles) => {
      // Check if accepted Files lenght is more than previewImages because review rating only allows 3 images maximum
      const maximumImage = 3 - previewImages?.length

      if (acceptedFiles?.length > maximumImage) {
        const remainingImage =
          acceptedFiles?.length - (3 - previewImages?.length)
        const isCanAddMoreImages = remainingImage > 0

        changeReviewRatingContext({
          isShowInfoBox: true,
          infoBoxMessage: `Foto gagal ditambahkan, anda hanya dapat menambahkan ${
            isCanAddMoreImages
              ? `${remainingImage} foto lagi`
              : 'maksimal 3 foto'
          }`,
          infoBoxType: 'error-minicart',
        })

        return
      }

      acceptedFiles.forEach(async (file, index) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)

        if (index < 3) {
          FileResizer.imageFileResizer(
            file,
            1920,
            1920,
            'JPEG',
            80,
            0,
            (uri) => {
              const image = {
                base64: uri,
                index,
              }
              setImagesData((prev) => [...prev, image])
            },
            'base64',
          )
        }
      })
    },
    [previewImages.length],
  )

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: 'image/jpeg',
    disabled: isUploadPhotoDisabled,
  })

  useEffect(() => {
    let payload = []

    for (let image of imagesData) {
      const data = {
        image: image?.base64,
        folder: 'review-rating',
        uploadId: image?.index,
      }

      var stringLength = image?.base64?.length - 'data:image/png;base64,'.length

      var sizeInBytes = 4 * Math.ceil(stringLength / 3)

      if (Math.ceil(sizeInBytes) >= 3 * 1024000) {
        changeReviewRatingContext({
          isShowInfoBox: true,
          infoBoxMessage:
            'Foto gagal ditambahkan, foto melebihi batas ukuran 3 MB',
          infoBoxType: 'error-minicart',
        })

        return
      }

      payload.push(data)
    }

    if (!isEmpty(payload)) {
      uploadMultipleReviewImages(payload)

      setImagesData([])
    }
  }, [imagesData])

  useEffect(() => {
    const imageUrl = []

    if (!isEmpty(previewImages)) {
      previewImages.map((image) => {
        const { image_url } = image

        imageUrl.push(image_url)
      })
    }

    setValue(productSku, 'images', imageUrl)
  }, [previewImages])

  useEffect(() => {
    if (!isEmpty(reviewedImages) && isProductRevised && isFirstTimeLoad) {
      setPreviewImages(reviewedImages)

      setIsFirstTimeLoad(false)
    }
  }, [reviewedImages])

  useEffect(() => {
    if (isLoadingUploadMultipleReviewImages) {
      changeReviewRatingContext({
        isScreenLoading: true,
      })

      return
    }
  }, [isLoadingUploadMultipleReviewImages])

  return (
    <div
      className={clsx(
        'tw-border tw-border-solid tw-border-grey-20 tw-flex tw-flex-col tw-rounded-lg tw-p-3',
        cardStyle[config.environment],
      )}
      key={`review-card-${productSku}`}
    >
      <div
        className={clsx(
          'tw-flex tw-justify-between',
          isDesktop ? 'tw-flex-col tw-gap-3' : '',
        )}
      >
        <div className='tw-flex tw-items-center tw-justify-between tw-gap-3'>
          <div className='tw-flex tw-gap-3 tw-items-center'>
            <div className='tw-flex'>
              <CustomLazyLoadImage
                alt={`${productSku}-image`}
                height={52}
                placeholder={<Skeleton height={52} width={52} />}
                src={imageSrc}
                width={52}
              />
            </div>
            <div className='tw-flex tw-flex-col tw-gap-1'>
              <p
                className={clsx(
                  'tw-text-grey-100',
                  productNameStyle[config.environment],
                )}
              >
                {productName}
              </p>
              <ReviewRatingStar
                rating={value?.rating}
                setRating={(star) => setValue(productSku, 'rating', star)}
                starDimension='20px'
              />
            </div>
          </div>
          {isDesktop && !isSingleProduct && (
            <div className='tw-flex tw-gap-2 tw-min-w-[308px]'>
              <TWButton
                additionalClass={clsx('tw-rounded-lg tw-w-full')}
                disabled={isUploadPhotoDisabled}
                type='secondary-border_ghost'
              >
                <div
                  className={`${
                    isUploadPhotoDisabled ? 'tw-cursor-not-allowed' : ''
                  }`}
                  {...getRootProps()}
                >
                  <input {...getInputProps()} />
                  <div className='tw-flex tw-gap-1.5 tw-items-center tw-justify-center'>
                    <CameraIcon
                      className={'tw-text-' + config.companyNameCSS}
                    />
                    <p
                      className={clsx(
                        isDesktop ? 'button-small-text' : 'ui-text-3',
                      )}
                    >
                      Tambah Foto
                    </p>
                  </div>
                </div>
              </TWButton>
              <TWButton
                additionalClass={clsx('tw-rounded-lg tw-w-full')}
                handleOnClick={() => setIsOpenModal(true)}
                type='secondary-border_ghost'
              >
                <div className='tw-flex tw-gap-1.5 tw-items-center tw-justify-center'>
                  <DocumentIcon
                    className={'tw-text-' + config.companyNameCSS}
                  />
                  <p className='button-small-text'>Tambah Ulasan</p>
                </div>
              </TWButton>
            </div>
          )}
        </div>
      </div>

      {(!isDesktop || (isDesktop && isSingleProduct)) && (
        <div className='tw-flex tw-gap-3'>
          <TWButton
            additionalClass={clsx('tw-rounded-lg tw-w-full')}
            disabled={isUploadPhotoDisabled}
            type='secondary-border_ghost'
          >
            <div
              className={`${
                isUploadPhotoDisabled ? 'tw-cursor-not-allowed' : ''
              }`}
              {...getRootProps()}
            >
              <input {...getInputProps()} />
              <div className='tw-flex tw-gap-2 tw-items-center tw-justify-center'>
                <CameraIcon className={'tw-text-' + config.companyNameCSS} />
                <p className='ui-text-3'>Tambah Foto</p>
              </div>
            </div>
          </TWButton>

          {!isSingleProduct && (
            <TWButton
              additionalClass={clsx('tw-rounded-lg tw-w-full')}
              handleOnClick={() => setIsOpenModal(true)}
              type='secondary-border_ghost'
            >
              <div className='tw-flex tw-gap-2 tw-items-center tw-justify-center'>
                <DocumentIcon className={'tw-text-' + config.companyNameCSS} />
                <p className='ui-text-3'>Tambah Ulasan</p>
              </div>
            </TWButton>
          )}
        </div>
      )}

      {previewImages?.length > 0 && (
        <div className='tw-flex tw-gap-3'>
          {previewImages?.map((image, index) => (
            <ReviewRatingPreviewImageCard
              image={image}
              index={index}
              key={`previewImages-${index}`}
              previewImages={previewImages}
              setPreviewImages={setPreviewImages}
            />
          ))}
        </div>
      )}

      {!isSingleProduct && value?.description && (
        <div className='tw-flex tw-flex-col tw-gap-1'>
          <p className='tw-text-grey-40 ui-text-4'>Ulasan</p>
          <p className='tw-text-grey-100 ui-text-3 tw-break-words'>
            {value?.description}
          </p>
        </div>
      )}

      {isSingleProduct && (
        <TWTextArea
          placeholder={REVIEW_RATING_TEXT_AREA_PLACEHOLDER}
          setValue={(description) =>
            setValue(productSku, 'description', description)
          }
          value={value?.description}
        />
      )}

      {isOpenModal && (
        <ReviewRatingMultipleProductModal
          imageSrc={imageSrc}
          isDesktop={isDesktop}
          productName={productName}
          setValue={(description) =>
            setValue(productSku, 'description', description)
          }
          show={isOpenModal}
          value={value?.description}
          onClose={() => setIsOpenModal(false)}
        />
      )}
    </div>
  )
}
