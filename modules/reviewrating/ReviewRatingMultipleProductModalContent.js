import React, { useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import config from '../../../../config'
import CloseButton from '../../../../public/static/icon/close.svg'
import CustomLazyLoadImage from '../../layouts/atoms/CustomLazyLoadImage'
import TWButton from '../../tw-components/buttons/TWButton'
import TWTextArea from '../../tw-components/TWTextArea'
import { REVIEW_RATING_TEXT_AREA_PLACEHOLDER } from '../../utils/constants'

export default function ReviewRatingMultipleProductModalContent({
  imageSrc,
  productName,
  reviewProduct,
  onClose,
  setReviewProduct,
}) {
  const [value, setValue] = useState(reviewProduct)

  const handleClickSaveReview = () => {
    setReviewProduct(value)

    onClose()
  }

  return (
    <div className='tw-flex tw-flex-col'>
      <div className='tw-flex tw-flex-col tw-gap-3 tw-px-4 tw-py-3'>
        <div className='tw-flex tw-justify-between tw-pt-3 tw-pb-2'>
          <p className='heading-2 tw-text-grey-100'>Ulasan untuk produk ini</p>

          {config.environment === 'desktop' && (
            <div className='tw-flex'>
              <CloseButton
                alt='Close Button'
                className='tw-text-grey-100 tw-cursor-pointer'
                height='1.5em'
                width='1.5em'
                onClick={onClose}
              />
            </div>
          )}
        </div>

        <div className='tw-flex tw-items-center tw-gap-3'>
          <div className='tw-flex'>
            <CustomLazyLoadImage
              alt={`${productName}-image`}
              height={52}
              placeholder={<Skeleton height={52} width={52} />}
              src={imageSrc}
              width={52}
            />
          </div>

          <p className='heading-3 tw-text-grey-100'>{productName}</p>
        </div>

        <TWTextArea
          placeholder={REVIEW_RATING_TEXT_AREA_PLACEHOLDER}
          setValue={setValue}
          value={value}
        />
      </div>

      <div className='tw-flex tw-py-3 tw-px-4'>
        <TWButton
          additionalClass='tw-rounded-md w-full'
          handleOnClick={handleClickSaveReview}
        >
          <p className='button-big-text'>Simpan Ulasan</p>
        </TWButton>
      </div>
    </div>
  )
}
