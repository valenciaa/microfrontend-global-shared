import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const OurServices = dynamic(
  () =>
    import('../../../../../src/static/interior-design/containers/OurServices'),
)

export default function OurServicesWrapper({
  configuration,
  componentName,
  identifier,
}) {
  const [refOurServices, inViewOurServices] = useInView({ ...configuration })
  const [OurServicesViewPort, setOurServicesViewport] = useState(false)
  useEffect(() => {
    if (inViewOurServices) {
      setOurServicesViewport(true)
    }
  }, [inViewOurServices])
  return (
    <ErrorBoundary>
      {OurServicesViewPort ? (
        <OurServices componentName={componentName} identifier={identifier} />
      ) : (
        <ComponentWrapper
          ref={refOurServices}
          height={config.environment === 'desktop' ? 285 : 388}
        />
      )}
    </ErrorBoundary>
  )
}
