import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const BannerInteriorDesignContainers = dynamic(
  () =>
    import(
      '../../../../../src/static/interior-design/containers/BannerInteriorDesignContainers'
    ),
)

export default function BannerInteriorDesignWrapper({
  configuration,
  componentName,
  identifier,
}) {
  const [refBannerInteriorDesignWrapper, inViewBannerInteriorDesign] =
    useInView({ ...configuration })
  const [
    BannerInteriorDesignWrapperViewPort,
    setBannerInteriorDesignWrapperViewport,
  ] = useState(false)
  useEffect(() => {
    if (inViewBannerInteriorDesign) {
      setBannerInteriorDesignWrapperViewport(true)
    }
  }, [inViewBannerInteriorDesign])
  return (
    <ErrorBoundary>
      {BannerInteriorDesignWrapperViewPort ? (
        <BannerInteriorDesignContainers
          componentName={componentName}
          identifier={identifier}
        />
      ) : (
        <ComponentWrapper
          ref={refBannerInteriorDesignWrapper}
          height={config.environment === 'desktop' ? 400 : 415}
        />
      )}
    </ErrorBoundary>
  )
}
