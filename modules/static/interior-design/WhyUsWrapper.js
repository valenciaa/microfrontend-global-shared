import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const WhyUsContainers = dynamic(
  () =>
    import(
      '../../../../../src/static/interior-design/containers/WhyUsContainers'
    ),
)

export default function WhyUsWrapper({ configuration, componentName }) {
  const [refWhyUs, inViewWhyUs] = useInView({ ...configuration })
  const [WhyUsViewPort, setWhyUsViewport] = useState(false)
  useEffect(() => {
    if (inViewWhyUs) {
      setWhyUsViewport(true)
    }
  }, [inViewWhyUs])
  return (
    <ErrorBoundary>
      {WhyUsViewPort ? (
        <WhyUsContainers componentName={componentName} />
      ) : (
        <ComponentWrapper
          ref={refWhyUs}
          height={config.environment === 'desktop' ? 140 : 325}
        />
      )}
    </ErrorBoundary>
  )
}
