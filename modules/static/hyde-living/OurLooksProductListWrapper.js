import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const OurLooksProductList = dynamic(
  () =>
    import(
      '../../../../../src/static/hyde-living/components/OurLooksProductList'
    ),
)

export default function OurLooksProductListWrapper({
  configuration,
  currentItems,
  homepageStyles,
}) {
  const [refOurLooksProductList, inViewOurLooksProductList] = useInView({
    ...configuration,
  })
  const [OurLooksProductListViewPort, setOurLooksProductListViewport] =
    useState(false)
  useEffect(() => {
    if (inViewOurLooksProductList) {
      setOurLooksProductListViewport(true)
    }
  }, [inViewOurLooksProductList])
  return (
    <ErrorBoundary>
      {OurLooksProductListViewPort ? (
        <OurLooksProductList
          currentItems={currentItems}
          homepageStyles={homepageStyles}
        />
      ) : (
        <ComponentWrapper
          ref={refOurLooksProductList}
          height={config.environment === 'desktop' ? 800 : 910}
        />
      )}
    </ErrorBoundary>
  )
}
