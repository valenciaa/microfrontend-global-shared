import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const AboutUsContainers = dynamic(
  () =>
    import(
      '../../../../../src/static/hyde-living/containers/AboutUsContainers'
    ),
)

export default function AboutUsWrapper({ configuration }) {
  const [refAboutUs, inViewAboutUs] = useInView({ ...configuration })
  const [AboutUsViewPort, setAboutUsViewport] = useState(false)
  useEffect(() => {
    if (inViewAboutUs) {
      setAboutUsViewport(true)
    }
  }, [inViewAboutUs])
  return (
    <ErrorBoundary>
      {AboutUsViewPort ? (
        <AboutUsContainers />
      ) : (
        <ComponentWrapper
          ref={refAboutUs}
          height={config.environment === 'desktop' ? 540 : 285}
        />
      )}
    </ErrorBoundary>
  )
}
