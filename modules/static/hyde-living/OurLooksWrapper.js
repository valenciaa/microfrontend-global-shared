import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const OurLooksContainers = dynamic(
  () =>
    import(
      '../../../../../src/static/hyde-living/containers/OurLooksContainers'
    ),
)

export default function OurLooksWrapper({ configuration }) {
  const [refOurLooks, inViewOurLooks] = useInView({ ...configuration })
  const [OurLooksViewPort, setOurLooksViewport] = useState(false)
  useEffect(() => {
    if (inViewOurLooks) {
      setOurLooksViewport(true)
    }
  }, [inViewOurLooks])
  return (
    <ErrorBoundary>
      {OurLooksViewPort ? (
        <OurLooksContainers itemsPerPage={6} />
      ) : (
        <ComponentWrapper
          ref={refOurLooks}
          height={config.environment === 'desktop' ? 800 : 910}
        />
      )}
    </ErrorBoundary>
  )
}
