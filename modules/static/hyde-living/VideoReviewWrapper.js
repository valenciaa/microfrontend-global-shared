import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const VideoReviewContainer = dynamic(
  () =>
    import(
      '../../../../../src/static/hyde-living/containers/VideoReviewContainer'
    ),
)

export default function VideoReviewWrapper({ configuration, componentName }) {
  const [refVideoReview, inViewVideoReview] = useInView({ ...configuration })
  const [VideoReviewViewPort, setVideoReviewViewport] = useState(false)
  useEffect(() => {
    if (inViewVideoReview) {
      setVideoReviewViewport(true)
    }
  }, [inViewVideoReview])
  return (
    <ErrorBoundary>
      {VideoReviewViewPort ? (
        <VideoReviewContainer componentName={componentName} />
      ) : (
        <ComponentWrapper
          ref={refVideoReview}
          height={config.environment === 'desktop' ? 500 : 325}
        />
      )}
    </ErrorBoundary>
  )
}
