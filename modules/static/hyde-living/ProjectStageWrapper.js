import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const ProjectStageContainers = dynamic(
  () =>
    import(
      '../../../../../src/static/hyde-living/containers/ProjectStageContainers'
    ),
)

export default function ProjectStageWrapper({ configuration }) {
  const [refProjectStage, inViewProjectStage] = useInView({ ...configuration })
  const [ProjectStageViewPort, setProjectStageViewport] = useState(false)
  useEffect(() => {
    if (inViewProjectStage) {
      setProjectStageViewport(true)
    }
  }, [inViewProjectStage])
  return (
    <ErrorBoundary>
      {ProjectStageViewPort ? (
        <ProjectStageContainers />
      ) : (
        <ComponentWrapper
          ref={refProjectStage}
          height={config.environment === 'desktop' ? 732 : 600}
        />
      )}
    </ErrorBoundary>
  )
}
