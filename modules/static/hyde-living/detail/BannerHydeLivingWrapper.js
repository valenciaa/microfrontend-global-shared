import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../../config'
const BannerHydeLivingContainer = dynamic(
  () =>
    import(
      '../../../../../../src/static/hyde-living/containers/detail/BannerHydeLivingContainer'
    ),
)

export default function BannerHydeLivingWrapper({
  configuration,
  componentName,
  dataProduct,
}) {
  const [refBannerHydeLivingWrapper, inViewBannerHydeLiving] = useInView({
    ...configuration,
  })
  const [BannerHydeLivingWrapperViewPort, setBannerHydeLivingWrapperViewport] =
    useState(false)
  useEffect(() => {
    if (inViewBannerHydeLiving) {
      setBannerHydeLivingWrapperViewport(true)
    }
  }, [inViewBannerHydeLiving])
  return (
    <ErrorBoundary>
      {BannerHydeLivingWrapperViewPort ? (
        <BannerHydeLivingContainer
          componentName={componentName}
          dataProduct={dataProduct}
        />
      ) : (
        <ComponentWrapper
          ref={refBannerHydeLivingWrapper}
          height={config.environment === 'desktop' ? 550 : 300}
        />
      )}
    </ErrorBoundary>
  )
}
