import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../../config'
const HydeLivingDetailContainer = dynamic(
  () =>
    import(
      '../../../../../../src/static/hyde-living/containers/detail/HydeLivingDetailContainer'
    ),
)

export default function ProductDetailWrapper({
  configuration,
  componentName,
  dataProduct,
  projectKey,
}) {
  const [refProductDetailWrapper, inViewProductDetail] = useInView({
    ...configuration,
  })
  const [ProductDetailWrapperViewPort, setProductDetailWrapperViewport] =
    useState(false)
  useEffect(() => {
    if (inViewProductDetail) {
      setProductDetailWrapperViewport(true)
    }
  }, [inViewProductDetail])
  return (
    <ErrorBoundary>
      {ProductDetailWrapperViewPort ? (
        <HydeLivingDetailContainer
          componentName={componentName}
          dataProduct={dataProduct}
          projectKey={projectKey}
        />
      ) : (
        <ComponentWrapper
          ref={refProductDetailWrapper}
          height={config.environment === 'desktop' ? 1300 : 500}
        />
      )}
    </ErrorBoundary>
  )
}
