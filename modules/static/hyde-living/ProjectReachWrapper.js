import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const ProjectReachContainer = dynamic(
  () =>
    import(
      '../../../../../src/static/hyde-living/containers/ProjectReachContainer'
    ),
)

export default function ProjectReachWrapper({ configuration, componentName }) {
  const [refProjectReach, inViewProjectReach] = useInView({ ...configuration })
  const [ProjectReachViewPort, setProjectReachViewport] = useState(false)
  useEffect(() => {
    if (inViewProjectReach) {
      setProjectReachViewport(true)
    }
  }, [inViewProjectReach])
  return (
    <ErrorBoundary>
      {ProjectReachViewPort ? (
        <ProjectReachContainer componentName={componentName} />
      ) : (
        <ComponentWrapper
          ref={refProjectReach}
          height={config.environment === 'desktop' ? 140 : 285}
        />
      )}
    </ErrorBoundary>
  )
}
