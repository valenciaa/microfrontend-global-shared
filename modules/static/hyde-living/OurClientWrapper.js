import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'
import ErrorBoundary from '../../../layouts/moleculs/ErrorBoundary'
import config from '../../../../../config'
const OurClientContainer = dynamic(
  () =>
    import(
      '../../../../../src/static/hyde-living/containers/OurClientContainer'
    ),
)

export default function OurClientWrapper({ configuration, componentName }) {
  const [refOurClient, inViewOurClient] = useInView({ ...configuration })
  const [OurClientViewPort, setOurClientViewport] = useState(false)
  useEffect(() => {
    if (inViewOurClient) {
      setOurClientViewport(true)
    }
  }, [inViewOurClient])
  return (
    <ErrorBoundary>
      {OurClientViewPort ? (
        <OurClientContainer componentName={componentName} />
      ) : (
        <ComponentWrapper
          ref={refOurClient}
          height={config.environment === 'desktop' ? 140 : 285}
        />
      )}
    </ErrorBoundary>
  )
}
