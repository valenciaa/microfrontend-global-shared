import { useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'
import config from '../../../../../config'
import ShopedMiniBlock from '../../../../../src/static/shoped/ShopedMiniBlock'
import ComponentWrapper from '../../../layouts/templates/ComponentWrapper'

export default function ShopedMiniBlockWrapper({ configuration }) {
  const [refShopedMiniBlock, inViewShopedMiniBlock] = useInView({
    ...configuration,
  })
  const [shopedMiniBlockViewport, setShopedMiniBlockViewport] = useState(false)
  useEffect(() => {
    if (inViewShopedMiniBlock) {
      setShopedMiniBlockViewport(true)
    }
  }, [inViewShopedMiniBlock])
  return (
    <>
      {shopedMiniBlockViewport ? (
        <ShopedMiniBlock />
      ) : (
        <ComponentWrapper
          ref={refShopedMiniBlock}
          height={config.environment === 'desktop' ? 246 : 164}
        />
      )}
    </>
  )
}
