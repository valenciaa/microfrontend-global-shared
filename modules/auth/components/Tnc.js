import React from 'react'
import config from '../../../../../config'
import TahuWordingContent from '../../../layouts/atoms/TahuWordingContent'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { useAuthContext } from '../../../context/AuthContext'

export default function Tnc({
  title,
  isBottom,
  handleScroll,
  handleScrollToBottom,
  handleChecked,
}) {
  const { authStyles } = useAuthContext()

  return (
    <>
      <div
        className={authStyles['container-text']}
        onScroll={handleScroll}
        style={{ textAlign: 'justify' }}
        id='tnc-container'
      >
        <TahuWordingContent title={title} companyCodeAll />
      </div>
      <div
        className={`${authStyles['scroll-tobottom']} ${
          config.environment === 'mobile'
            ? `${authStyles?.mobile}`
            : authStyles?.desktop
        }`}
        onClick={() => handleScrollToBottom()}
      >
        <CustomLazyLoadImage
          src={config?.assetsURL + 'icon/arrow-down-white.svg'}
          width={25}
        />
      </div>
      <div className={authStyles['req-check']}>
        <input
          type='checkbox'
          className={authStyles['square-check']}
          disabled={!isBottom ? 'disabled' : ''}
          onChange={handleChecked}
        />
        <div className={`${authStyles['text-check']} ui-text-3`}>
          Saya sudah membaca dan mengerti, serta setuju dengan syarat ketentuan
          dan kebijakan privasi di atas
        </div>
      </div>
    </>
  )
}
