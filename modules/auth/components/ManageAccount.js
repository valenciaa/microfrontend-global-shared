import { useForm } from 'react-hook-form'
import localforage from 'localforage'
import clsx from '../../../utils/Clsx'

import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import Button from '../../../layouts/atoms/Button'
import { ModalV2, ModalBody } from '../../../layouts/templates/ModalV2'

import config from '../../../../../config'
import { useAuthContext } from '../../../context/AuthContext'

export default function ManageAccount({
  isOpen,
  setIsOpen,
  action,
  isDesktop = false,
}) {
  const { authStyles } = useAuthContext()
  const { register, watch } = useForm({
    mode: 'all',
  })

  const handleConfirm = () => {
    if (action) {
      action({
        accountAction: watch('mergeAction', false),
      })
      setIsOpen(false)
      localforage.removeItem('passkey_sms_choice')
      localforage.removeItem('passkey_wa_choice')
      localforage.removeItem('passkey_email_choice')
    }
  }

  return (
    <ModalV2
      isShow={isOpen}
      className={
        authStyles[`account-modal${!isDesktop ? '--bottom-position' : ''}`]
      }
    >
      {/* //! Close Button */}
      {!isDesktop && (
        <div
          className={authStyles['account-modal__close-button__container']}
          onClick={() => setIsOpen(false)}
        >
          <CustomLazyLoadImage
            className={clsx('icon', authStyles['account-modal__close-button'])}
            src={config?.assetsURL + 'icon/close-dark.svg'}
          />
        </div>
      )}

      <ModalBody
        className={
          authStyles[`account-modal__body${!isDesktop ? '--full' : ''}`]
        }
      >
        <div className={authStyles['account-modal__header']}>
          <div>Kelola Akun</div>
          {isDesktop && (
            <div onClick={() => setIsOpen(false)}>
              <CustomLazyLoadImage
                className='icon'
                src={config?.assetsURL + 'icon/close-dark.svg'}
              />
            </div>
          )}
        </div>

        <div
          className='flex-col gap-xs'
          style={{ maxWidth: isDesktop ? '400px' : undefined }}
        >
          <div className={authStyles['manage-account__container']}>
            <div className={authStyles['manage-account__sub-container']}>
              <p className={authStyles['manage-account__header']}>
                Gabungkan Sekarang
              </p>
              <p className={authStyles['manage-account__sub-header']}>
                Yuk gabungkan akun rewards-mu sekarang dan dapatkan lebih banyak
                keuntungan!
              </p>
            </div>
            <input
              value='merge'
              type='radio'
              className={authStyles['account-radio--orange']}
              {...register('mergeAction')}
            />
          </div>

          <hr />

          <div className={authStyles['manage-account__container']}>
            <div className={authStyles['manage-account__sub-container']}>
              <p className={authStyles['manage-account__header']}>
                Bukan Akun Saya
              </p>
              <p className={authStyles['manage-account__sub-header']}>
                Saya tidak merasa memiliki email atau nomor handphone yang
                terdaftar di rewards lainnya
              </p>
            </div>
            <input
              value='decline'
              type='radio'
              className={authStyles['account-radio--orange']}
              {...register('mergeAction')}
            />
          </div>

          <hr />
        </div>

        <Button
          type={watch('mergeAction', false) ? 'primary' : 'disabled'}
          handleOnClick={handleConfirm}
          fetching={!watch('mergeAction', false)}
        >
          Pilih
        </Button>
      </ModalBody>
    </ModalV2>
  )
}
