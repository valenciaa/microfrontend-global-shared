import { ModalV2, ModalBody } from '../../../layouts/templates/ModalV2'
import Button from '../../../layouts/atoms/Button'
import { useAuthContext } from '../../../context/AuthContext'

export default function InfoModal({
  isOpen,
  setIsOpen,
  header,
  content,
  confirmButton,
  cancelButton,
  actionOnCancel,
  actionOnConfirm,
  err = '',
}) {
  const { authStyles } = useAuthContext()
  const handleConfirm = () => {
    setIsOpen(false)
    // ? actionOnComfirm akan di panggil di sini atau push ke component lain
    if (actionOnConfirm) {
      actionOnConfirm()
    }
  }

  const handleCancel = () => {
    setIsOpen(false)
    if (actionOnCancel) {
      actionOnCancel()
    }
  }

  return (
    <ModalV2 isShow={isOpen} className={authStyles['account-modal']}>
      <ModalBody className={authStyles['account-modal__body']}>
        <div className={authStyles['account-modal__header']}>{header}</div>
        <div className={authStyles['account-modal__content']}>{content}</div>

        <div className={authStyles['account-modal__button-container']}>
          {cancelButton && (
            <Button type='primary-border' handleOnClick={handleCancel}>
              {cancelButton}
            </Button>
          )}
          <Button type='primary' handleOnClick={handleConfirm}>
            {confirmButton}
          </Button>
        </div>

        {err !== '' && <div className='error-message ui-text-3'>{err}</div>}
      </ModalBody>
    </ModalV2>
  )
}
