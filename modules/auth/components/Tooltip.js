import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { useAuthContext } from '../../../context/AuthContext'

export default function InputWithHooks({
  title = '',
  content = '',
  iconTooltip = null,
  iconClose = null,
  additionalLabelClass = '',
  contentClass = '',
}) {
  const { authStyles } = useAuthContext()

  return (
    <>
      <label
        className={`${authStyles?.['tooltip']} flex items-center ${additionalLabelClass}`}
      >
        {iconTooltip && <CustomLazyLoadImage src={iconTooltip} />}
        <input type='checkbox' />
        <span>
          <div className={contentClass}>
            <b>{title}</b>
            {iconClose && <img src={iconClose} />}
          </div>
          {content}
        </span>
      </label>
    </>
  )
}
