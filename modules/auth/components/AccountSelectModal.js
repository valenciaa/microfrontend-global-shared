import { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import localforage from 'localforage'

import clsx from '../../../utils/Clsx'

import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import Button from '../../../layouts/atoms/Button'
import { ModalV2, ModalBody } from '../../../layouts/templates/ModalV2'
import BUBox from './BUBox'

import config from '../../../../../config'
import { useAuthContext } from '../../../context/AuthContext'

export default function AccountSelectModal({
  isOpen,
  setIsOpen,
  legacyMembers = [],
  action,
  chosenMemberToken,
  isDesktop = false,
}) {
  const { authStyles } = useAuthContext()
  const { register, watch, setValue } = useForm({
    mode: 'all',
  })

  useEffect(() => {
    if (chosenMemberToken) {
      setValue('memberToken', chosenMemberToken)
    }
  }, [])

  const handleConfirm = () => {
    const memberToken = watch('memberToken', false)
    if (memberToken) {
      localforage.setItem('chosenMemberToken', memberToken)
    }
    action(memberToken)
  }

  return (
    <ModalV2
      isShow={isOpen}
      className={
        authStyles[`account-modal${!isDesktop ? '--bottom-position' : ''}`]
      }
    >
      {/* //! Close Button */}
      {!isDesktop && (
        <div
          className={authStyles['account-modal__close-button__container']}
          onClick={() => setIsOpen(false)}
        >
          <CustomLazyLoadImage
            className={clsx('icon', authStyles['account-modal__close-button'])}
            src={config?.assetsURL + 'icon/close-dark.svg'}
          />
        </div>
      )}

      <ModalBody
        className={
          authStyles[`account-modal__body${!isDesktop ? '--full' : ''}`]
        }
      >
        <div className={authStyles['account-modal__header']}>
          <div>Akun Terdaftar</div>
          {isDesktop && (
            <div onClick={() => setIsOpen(false)}>
              <CustomLazyLoadImage
                className='icon'
                src={config?.assetsURL + 'icon/close-dark.svg'}
              />
            </div>
          )}
        </div>
        <div className={authStyles['account-modal__content']}>
          Pilih akun yang ingin digunakan untuk masuk ke ruparupa
        </div>

        <div style={{ display: 'flex', gap: '8px', flexDirection: 'column' }}>
          {legacyMembers.map((el, index) => (
            <BUBox
              key={index}
              userInfo={el}
              isInput
              register={register}
              isChecked={watch('memberToken', false) === el.memberToken}
              setValue={setValue}
            />
          ))}
        </div>

        <Button
          type={watch('memberToken', false) ? 'primary' : 'disabled'}
          handleOnClick={handleConfirm}
          fetching={!watch('memberToken', false)}
        >
          Pilih
        </Button>
      </ModalBody>
    </ModalV2>
  )
}
