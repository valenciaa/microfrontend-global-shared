import { useState } from 'react'
import config from '../../../../../config'
import Button from '../../../layouts/atoms/Button'

import Chevron from '../../../layouts/atoms/Chevron'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import ToggleImage from '../../../layouts/moleculs/ToggleImage'
import { ModalV2, ModalBody } from '../../../layouts/templates/ModalV2'
import {
  useGetAllCities,
  useGetAllDistricts,
  useGetAllProvinces,
  useGetSearchedAddress,
} from '../../../services/api/queries/global'
import clsx from '../../../utils/Clsx'
import { useAuthContext } from '../../../context/AuthContext'

const nameMap = {
  districts: 'kecamatan',
  provinces: 'provinsi',
  cities: 'kota',
}

const labels = [
  {
    type: 'provinces',
    label: 'Pilih Provinsi',
    chosenLabel: '',
  },
  {
    type: 'cities',
    label: 'Pilih Kota',
    chosenLabel: '',
  },
  {
    type: 'districts',
    label: 'Pilih Kecamatan',
    chosenLabel: '',
  },
]

const translate = {
  provinces: 'Provinsi',
  cities: 'Kota',
  districts: 'Kecamatan',
}

// Todo Component sangat brutal, perlu refactor
export default function LocationInput({
  isOpen = true,
  setIsOpen,
  action,
  disctrict = 0,
  city = 0,
  province = 0,
  defaultLabels = labels,
}) {
  const { authStyles } = useAuthContext()
  const [chosenProvince, setChosenProvince] = useState(province)
  const [chosenCity, setChosenCity] = useState(city)
  const [chosenDistrict, setChosenDistrict] = useState(disctrict)
  const [defaultOptions, setDefaultOptions] = useState(defaultLabels)
  // ? State untuk menentukan opsi mana yang muncul
  const [shownOptions, setShownOptions] = useState('all')
  // ? State untuk menampilkan provinces, cities dan districts
  const [locationLists, setLocationLists] = useState([])
  const [searchAllResult, setSearchAllResult] = useState([])
  const [search, setSearch] = useState('')
  const [searchAll, setSearchAll] = useState('')

  const { data: allProvinces, isLoading: isLoadingProvinces } =
    useGetAllProvinces(
      {},
      {
        enabled: true,
      },
    )
  const { data: allCities, isLoading: isLoadingCities } = useGetAllCities(
    { provinceId: chosenProvince },
    {
      enabled: chosenProvince > 0,
    },
  )
  const { data: allDistricts, isLoading: isLoadingDistrict } =
    useGetAllDistricts(
      { cityId: chosenCity },
      {
        enabled: chosenCity > 0,
      },
    )

  const { isLoading: isLoadingSearched } = useGetSearchedAddress(
    {
      addressSearchType: 'all-by-district',
      search: searchAll,
    },
    {
      onSuccess: (response) => {
        if (response && response.length) {
          setSearchAllResult(response)
        }
      },
    },
  )

  const handleClose = () => {
    // Todo nanti akan ada beberapa opsi close
    if (shownOptions === 'all') {
      setIsOpen(false)
    } else {
      setShownOptions('all')
    }
  }

  const handleChooseOptions = (type) => {
    if (type === 'cities' && !chosenProvince) {
      return
    } else if (type === 'districts' && !chosenCity) {
      return
    }

    // ? Jadi kalau milih provinces dan cities, lokasi di atasnya akan di reset
    if (type === 'provinces' && allProvinces) {
      setChosenCity(0)
      setChosenDistrict(0)

      setLocationLists(
        allProvinces.map((el) => {
          return {
            type: 'provinces',
            label: el.province_name,
            id: el.province_id,
          }
        }),
      )

      const newDefaultOptions = defaultOptions.map((el) => {
        return {
          ...el,
          chosenLabel:
            el.type === 'cities' || el.type === 'districts'
              ? ''
              : el.chosenLabel,
        }
      })
      setDefaultOptions(newDefaultOptions)
    } else if (type === 'cities' && allCities) {
      setChosenDistrict(0)

      setLocationLists(
        allCities.map((el) => {
          return {
            type: 'cities',
            label: el.city_name,
            id: el.city_id,
          }
        }),
      )

      const newDefaultOptions = defaultOptions.map((el) => {
        return {
          ...el,
          chosenLabel: el.type === 'districts' ? '' : el.chosenLabel,
        }
      })
      setDefaultOptions(newDefaultOptions)
    } else if (type === 'districts' && allDistricts) {
      setLocationLists(
        allDistricts.map((el) => {
          return {
            type: 'districts',
            label: el.kecamatan_name,
            id: el.kecamatan_id,
          }
        }),
      )
    }
    setSearch('')
    setShownOptions(type)
  }

  const handleChooseLocation = (type, id, label) => {
    if (type === 'provinces') {
      setChosenProvince(id)
    } else if (type === 'cities') {
      setChosenCity(id)
    } else if (type === 'districts') {
      setChosenDistrict(id)
    }

    const newDefaultOptions = defaultOptions.map((el) => {
      return {
        ...el,
        chosenLabel: el.type === type ? label : el.chosenLabel,
      }
    })
    setDefaultOptions(newDefaultOptions)
    setShownOptions('all')
  }

  const handleSubmit = () => {
    setIsOpen(false)
    action({
      districtId: chosenDistrict,
      cityId: chosenCity,
      provinceId: chosenProvince,
      label: defaultOptions.map((el) => el.chosenLabel).join(', '),
      defaultOptions,
    })
  }

  const handleSearch = (value) => {
    if (shownOptions === 'all') {
      setSearch('')
      setSearchAll(value)
    } else {
      setSearch(value)
    }
  }

  const handleSearchConfirm = (data) => {
    action({
      districtId: data.kecamatan_id,
      cityId: data.city_id,
      provinceId: data.province_id,
      label: data.full_address,
      defaultOptions: [
        {
          type: 'provinces',
          label: 'Pilih Provinsi',
          chosenLabel: data.province_name,
        },
        {
          type: 'cities',
          label: 'Pilih Kota',
          chosenLabel: data.city_name,
        },
        {
          type: 'districts',
          label: 'Pilih Kecamatan',
          chosenLabel: data.kecamatan_name,
        },
      ],
    })
    setIsOpen(false)
  }

  const handleTitle = () => {
    if (shownOptions === 'provinces') {
      return 'Pilih Provinsi'
    } else if (shownOptions === 'cities') {
      return 'Pilih Kota'
    } else if (shownOptions === 'districts') {
      return 'Pilih Kecamatan'
    } else {
      return 'Provinsi, Kota, Kecamatan'
    }
  }

  return (
    <>
      <ModalV2
        isShow={isOpen}
        className={authStyles['account-modal--bottom-position']}
      >
        <ModalBody className={authStyles['account-modal__body--full-height']}>
          {/* Header */}
          <div
            className={clsx(
              authStyles['top-header__container'],
              authStyles['top-header__text'],
            )}
            onClick={() => handleClose()}
          >
            <CustomLazyLoadImage
              className='icon'
              src={config?.assetsURL + 'icon/close-dark.svg'}
            />
            <span>{handleTitle()}</span>
          </div>

          <div
            className={clsx(
              authStyles['account-default-container'],
              authStyles['button-bottom-position'],
            )}
          >
            <div>
              {/* Input Untuk search */}
              <div
                className='search-input help__container'
                style={{ padding: '0px', margin: '0px 0px 8px 0px' }}
              >
                <input
                  className='search-bar'
                  id='search-bar-address-locations'
                  placeholder={`Cari ${
                    shownOptions === 'all'
                      ? 'Kecamatan'
                      : `nama ${nameMap[shownOptions]} di sini`
                  }`}
                  onChange={(e) => handleSearch(e.target.value)}
                  onBlur={(e) => handleSearch(e.target.value)}
                />
                <span
                  className='help__searchbar help__searchbar--magnifier'
                  style={{ left: 2 }}
                >
                  <ToggleImage
                    type='header-top'
                    primaryImage={`${config.assetsURL}icon/search-primary-ruparupa.svg`}
                    alt='Search'
                    condition
                    height={16}
                    width={16}
                    className='search-icon__mobile'
                  />
                </span>
              </div>

              {/* List Option  */}
              <div>
                {/* For Default Options */}
                {shownOptions === 'all' &&
                  !searchAll &&
                  defaultOptions.map((el, index) => (
                    <div
                      className='padding-top-s padding-bottom-s list-border-bottom'
                      key={index}
                      onClick={() => handleChooseOptions(el.type)}
                    >
                      <div className='flex items-center justify-between ui-text-3'>
                        <div>{el.chosenLabel ? el.chosenLabel : el.label}</div>
                        <Chevron width={9} />
                      </div>
                    </div>
                  ))}
                {/* List all locations */}
                {(!isLoadingProvinces ||
                  !isLoadingCities ||
                  !isLoadingDistrict) &&
                  shownOptions !== 'all' &&
                  locationLists &&
                  locationLists
                    .filter(
                      (el) =>
                        el.label
                          .toLowerCase()
                          .search(search.toLocaleLowerCase()) > -1,
                    )
                    .map((el) => (
                      <div
                        className='padding-top-s padding-bottom-s list-border-bottom'
                        key={el.id}
                        onClick={() =>
                          handleChooseLocation(el.type, el.id, el.label)
                        }
                      >
                        <div className='flex items-center justify-between ui-text-3'>
                          <div>{el.label}</div>
                          <Chevron width={9} />
                        </div>
                      </div>
                    ))}

                {(!isLoadingProvinces ||
                  !isLoadingCities ||
                  !isLoadingDistrict) &&
                shownOptions !== 'all' &&
                locationLists &&
                locationLists.filter(
                  (el) =>
                    el.label.toLowerCase().search(search.toLocaleLowerCase()) >
                    -1,
                ).length === 0 ? (
                  <p
                    className={clsx(
                      authStyles['account-label--error'],
                      'text-center',
                    )}
                  >{`${translate[shownOptions]} "${search}" tidak dapat ditemukan`}</p>
                ) : (
                  ''
                )}

                {/* Search result all discrict */}
                {!isLoadingSearched &&
                  shownOptions === 'all' &&
                  searchAll &&
                  searchAllResult &&
                  searchAllResult.map((el) => (
                    <div
                      className='padding-top-s padding-bottom-s list-border-bottom'
                      key={el.id}
                      onClick={() => handleSearchConfirm(el)}
                    >
                      <div className='flex items-center justify-between ui-text-3'>
                        <div>{el.full_address}</div>
                        <Chevron width={9} />
                      </div>
                    </div>
                  ))}
              </div>
            </div>

            {shownOptions === 'all' && (
              <Button
                type={chosenDistrict ? 'primary' : 'disabled'}
                fetching={!chosenDistrict}
                handleOnClick={() => handleSubmit()}
              >
                Selesai
              </Button>
            )}
          </div>
        </ModalBody>
      </ModalV2>

      {/* {(isLoadingProvinces || isLoadingCities || isLoadingDistrict) && <LoadingModal isOpen={isLoadingProvinces || isLoadingCities || isLoadingDistrict} />} */}
    </>
  )
}
