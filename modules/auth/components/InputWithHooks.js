import { useState } from 'react'
import config from '../../../../../config'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import Chevron from '../../../layouts/atoms/Chevron'
import ChevronDirections from '../../../utils/ChevronDirections'
import clsx from '../../../utils/Clsx'
import { useAuthContext } from '../../../context/AuthContext'
import { parsePhonePrefix } from '../../../utils/PhoneNumber'

export default function InputWithHooks({
  register,
  error,
  placeholder,
  type = 'text',
  length,
  inputmode,
  disabled = false,
  id,
  value,
  phoneCode = false,
  setToggleModalCountryCodes,
  hidedArrow = false,
}) {
  const { authStyles, phoneCountryCode, phoneCountryCodeListMap } =
    useAuthContext()
  const { onChange, onBlur, name, ref } = register
  const [isVisible, setIsVisible] = useState(false)

  const showHidePassword = () => {
    setIsVisible(!isVisible)
  }

  const countryCodeStyling = (
    phoneCode = false,
    phoneCountryCode = '',
    disabled = false,
  ) => {
    let finalStyle = ''
    let prefix = parsePhonePrefix(phoneCountryCodeListMap, phoneCountryCode)
    if (phoneCode) {
      switch (prefix?.length) {
        case 3:
          if (disabled) {
            finalStyle = authStyles?.['country-code-left-padding--code-3']
          }

          break
        case 4:
          if (!disabled) {
            finalStyle = authStyles?.['country-code-left-padding--code-4']
          }
          break
      }
    }
    return finalStyle
  }

  return (
    <div className={clsx(authStyles['account-input-password__container'])}>
      <div className='input-bar-container'>
        <input
          className={clsx(
            authStyles[
              phoneCode ? 'account-input-field--phone' : 'account-input-field'
            ],
            countryCodeStyling(phoneCode, phoneCountryCode, disabled),
            'input-bar',
            authStyles[`account-input${error ? '--error' : ''}`],
          )}
          disabled={disabled}
          type={type === 'password' ? (isVisible ? 'text' : 'password') : type}
          name={name}
          onBlur={onBlur}
          placeholder={placeholder}
          maxLength={length > 0 ? length : undefined}
          onChange={onChange}
          value={value}
          id={id}
          ref={ref}
          inputMode={inputmode}
          autoComplete='off'
        />
      </div>
      {type === 'password' && (
        <div onClick={() => showHidePassword()}>
          <CustomLazyLoadImage
            className={`icon ${authStyles['account-input-password__icon']}`}
            src={
              isVisible
                ? 'https://cdn.ruparupa.io/media/promotion/ruparupa/aset-ruparupa-rewards/form/Eye-On.svg'
                : 'https://cdn.ruparupa.io/media/promotion/ruparupa/aset-ruparupa-rewards/form/eye-off.svg'
            }
            height={20}
          />
        </div>
      )}
      {phoneCode && (
        <div
          className={clsx(
            authStyles['phone-input'],
            authStyles['account-input__phone-symbol'],
            !disabled
              ? `${authStyles['phone-input__country']} flex items-center justify-center`
              : '',
            config.environment === 'desktop' &&
              !disabled &&
              setToggleModalCountryCodes
              ? 'cursor-pointer'
              : '',
          )}
          onClick={
            !disabled && setToggleModalCountryCodes
              ? () => setToggleModalCountryCodes(true)
              : null
          }
        >
          <b>{parsePhonePrefix(phoneCountryCodeListMap, phoneCountryCode)}</b>
          {!disabled && (
            <Chevron
              {...(hidedArrow ? { className: 'tw-opacity-50' } : {})}
              {...ChevronDirections.chevronDown}
              fill='#F26A24'
              width={22}
            />
          )}
        </div>
      )}
    </div>
  )
}
