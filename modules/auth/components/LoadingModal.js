import config from '../../../../../config'

import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { useAuthContext } from '../../../context/AuthContext'
// ? Modal baru di ambil dari module pcp. Modal lebih cocok untuk content yang simple seperti untuk notif atau loading
import { ModalV2, ModalBody } from '../../../layouts/templates/ModalV2'

export default function LoadingModal({ isOpen = false }) {
  const { authStyles } = useAuthContext()
  return (
    <ModalV2 isShow={isOpen} className={authStyles['account-loading']}>
      <ModalBody className={authStyles['account-loading__body']}>
        <CustomLazyLoadImage
          src={config?.assetsURL + 'images/auth/bouncing-dot.gif'}
        />
      </ModalBody>
    </ModalV2>
  )
}
