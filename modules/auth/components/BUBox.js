import clsx from 'clsx'
import CustomLazyLoadImage from '../../../layouts/atoms/CustomLazyLoadImage'
import { useAuthContext } from '../../../context/AuthContext'

export default function BUBox({
  userInfo,
  isInput = false,
  register,
  isChecked = false,
  isChange = false,
  changeAccount,
  handleAccount,
  setValue,
}) {
  const { authStyles } = useAuthContext()

  const shortenText = (value) => {
    if (userInfo && userInfo.status === 'declined' && value.length >= 12) {
      return value.slice(4)
    } else {
      return value
    }
  }

  const handleChooseBu = () => {
    if (setValue) {
      setValue('memberToken', userInfo.memberToken)
    }
  }

  return (
    <div
      className={
        authStyles[`bu-box__container${isChecked ? '--darken-background' : ''}`]
      }
      style={{
        flexDirection:
          userInfo && userInfo.status === 'unknown' ? 'column' : 'row',
      }}
      onClick={handleChooseBu}
    >
      <div
        style={{
          width: '100%',
          display: 'flex',
          flexDirection:
            userInfo && userInfo.status === 'unknown' ? 'column' : 'row',
          alignItems: 'center',
        }}
      >
        <div className={authStyles['bu-box__spread-around']}>
          {userInfo.companyCode && (
            <CustomLazyLoadImage
              className='icon'
              src={userInfo.imageUrl}
              width={50}
              placeholderSrc={userInfo.imageUrl}
            />
          )}

          <div>
            <div
              className={clsx(authStyles['bu-box__info-text'], 'tw-font-sans')}
            >
              {userInfo.member_id
                ? `Member ID: ${shortenText(userInfo.member_id)}`
                : 'Member ID tidak terdaftar'}
            </div>
            <div
              className={clsx(
                authStyles[
                  `bu-box__info-text${
                    userInfo && userInfo.status === 'declined'
                      ? '--declined'
                      : ''
                  }`
                ] + ' margin-top-xxs',
                'tw-font-sans',
              )}
            >
              {userInfo.email ? userInfo.email : 'Email tidak terdaftar'}
            </div>
            <div
              className={clsx(
                authStyles['bu-box__info-text'] + ' margin-top-xs',
                'tw-font-sans',
              )}
            >
              {userInfo.phone || userInfo.mobile_number
                ? userInfo.phone || userInfo.mobile_number
                : 'Nomor HP tidak terdaftar'}
            </div>
          </div>
        </div>
        {isInput && register && (
          <input
            value={userInfo.memberToken}
            type='radio'
            className={authStyles['account-radio--orange']}
            {...register('memberToken')}
          />
        )}

        {/* //? Pakai button sendiri karena styling khusus */}
        {userInfo && userInfo.status === 'merged' && (
          <p className={authStyles['bu-box__success-text']}>Tergabung</p>
        )}
        {userInfo && userInfo.status === 'declined' && (
          <p className={authStyles['bu-box__failed-text']}>Bukan akun saya</p>
        )}
        {isChange && (
          <p
            onClick={() => changeAccount && changeAccount()}
            className={authStyles['bu-box__prompt-text']}
          >
            Ganti
          </p>
        )}
      </div>
      {userInfo && userInfo.status === 'unknown' && (
        <button
          onClick={() => handleAccount && handleAccount(userInfo.memberToken)}
          className={authStyles['bu-box__button'] + ' margin-top-xxs'}
        >
          Kelola Akun Ini
        </button>
      )}
    </div>
  )
}
