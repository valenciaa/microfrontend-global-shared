// todo:  Page that using this is: Homepage, Product, Category

import Head from 'next/head'
import React from 'react'
import isEmpty from 'lodash/isEmpty'
import { useConstantsContext } from '../context/ConstantsContext'
import { useSeoContext } from '../context/SeoContext'
import {
  GetItemListElement,
  GetItemListElementCategoryList,
  GetItemListReviewCountAlgolia,
  GetItemListReviewCountCatalog,
  GetItemListReviewCountStore,
  GetItemListReviewCountProductDetail,
  GetItemListElementDetailBreadcrumbList,
  GetItemListElementArticle,
  GetItemListReviewCountArticle,
  GetItemListReviewCountBrands,
  GetItemListElementBrandBreadcrumbList,
} from '../utils/GetItemListElement'

export default function HeadSchema() {
  // This is for all HEad schema function
  // Right now its only contain schema for page Catalog and Jual

  const constantContext = useConstantsContext()
  const {
    productsSeo,
    categoriesSeo,
    algoliaProductsSeo,
    metaDataSeo,
    sosmedDataSeo,
  } = useSeoContext()

  const schema = [
    `{
       "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "https://www.ruparupa.com/",
        "potentialAction":
            {
                "@type": "SearchAction",
                "target": "https://www.ruparupa.com/catalog?keyword={search_term_string}",
                "query-input": "required name=search_term_string"
            },
        "name": "Ruparupa",
        "alternateName": "Ruparupa.com"
          }`,
    `{
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "Ruparupa",
        "address":
            {
                "@type": "PostalAddress",
                "addressCountry": "Indonesia",
                "addressLocality": "Jakarta Barat",
                "addressRegion": "DKI Jakarta",
                "postalCode": "11610",
                "streetAddress": "Gedung Kawan Lama lt. 7, Jl. Puri Kencana no. 1, RT 6 / RW 2, Kembangan Sel., Kembangan."
            },
        "legalName": "PT. Omni Digitama Internusa",
        "url": "https://www.ruparupa.com/",
        "contactPoint":
            {
                "@type": "ContactPoint",
                "telephone": "+62-21-582-9191",
                "contactType": "customer service",
                "email": "mailto:help@ruparupa.com"
            },
        "logo": "https://res.cloudinary.com/ruparupa-com/image/upload/v1543829312/SEO/Logo/logo_ruparupa_square.png",
        "sameAs":
              [
                "https://www.facebook.com/ruparupacom",
                "https://www.instagram.com/ruparupacom",
                "https://www.twitter.com/ruparupacom",
                "https://id.pinterest.com/ruparupacom",
                "https://www.youtube.com/channel/UCWx9vHjiFoOCPFtjEPkr1Aw",
                "https://id.wikipedia.org/wiki/Ruparupa.com"
              ]
      }`,
  ]
  const schemaProduct = []
  const schemaProductDetail = []
  let schemaReviewCount = ''
  let schemaMicrosite = ''
  const schemaProductMicrosite = []

  // ItemList BreadCrumb
  if (
    !isEmpty(categoriesSeo) &&
    !isEmpty(GetItemListElementCategoryList(categoriesSeo))
  ) {
    schemaProduct.push(`{
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": ${GetItemListElementCategoryList(
        categoriesSeo,
      ).toString()}
    }`)
  }
  // check Product SEO, from Catalog   Item list Product
  if (!isEmpty(productsSeo) && !isEmpty(GetItemListElement(productsSeo))) {
    schemaProduct.push(`{
      "@context": "https://schema.org",
      "@type": "ItemList",
      "itemListElement": [${GetItemListElement(productsSeo).toString()}]
    }`)
  }

  // check Product SEO, from algolia Item list Product
  if (
    !isEmpty(algoliaProductsSeo) &&
    !isEmpty(GetItemListElement(algoliaProductsSeo?.[0]))
  ) {
    schemaProduct.push(`{
      "@context": "https://schema.org",
      "@type": "ItemList",
      "itemListElement": [${GetItemListElement(
        algoliaProductsSeo?.[0],
      ).toString()}]
    }`)
  }

  // check Sosmed SEO, from sosmed data Item list Product
  if (
    !isEmpty(sosmedDataSeo) &&
    !isEmpty(GetItemListElement(sosmedDataSeo?.products))
  ) {
    schemaProduct.push(`{
      "@context": "https://schema.org",
      "@type": "ItemList",
      "itemListElement": [${GetItemListElement(
        sosmedDataSeo?.products,
      ).toString()}]
    }`)
  }

  // Catalog Product review
  if (
    !isEmpty(categoriesSeo) &&
    !isEmpty(productsSeo) &&
    !isEmpty(GetItemListReviewCountCatalog(categoriesSeo, productsSeo))
  ) {
    schemaReviewCount = GetItemListReviewCountCatalog(
      categoriesSeo,
      productsSeo,
    ).toString()
  }

  // Aloglia Product Review
  if (!isEmpty(algoliaProductsSeo) && metaDataSeo?.name) {
    schemaReviewCount = GetItemListReviewCountAlgolia(
      metaDataSeo?.name,
      algoliaProductsSeo,
    ).toString()
  }

  // Store Product Review
  if (
    !isEmpty(algoliaProductsSeo) &&
    metaDataSeo?.name &&
    constantContext?.pageFrom === 'store'
  ) {
    schemaReviewCount = GetItemListReviewCountStore(
      metaDataSeo,
      algoliaProductsSeo,
    ).toString()
  }

  // Product Detail Schema
  if (!isEmpty(metaDataSeo) && constantContext?.pageFrom === 'product-detail') {
    const productSchema = GetItemListReviewCountProductDetail(metaDataSeo)
    const breadcrumbListSchema = GetItemListElementDetailBreadcrumbList(
      metaDataSeo?.breadcrumb,
    )

    if (!isEmpty(productSchema)) {
      schemaProductDetail.push(productSchema)
    }

    if (!isEmpty(breadcrumbListSchema)) {
      schemaProductDetail.push(breadcrumbListSchema)
    }
  }

  // Microsite Artikel Schema
  if (!isEmpty(sosmedDataSeo) && sosmedDataSeo?.ogUrl?.includes('artikel')) {
    schemaMicrosite = GetItemListElementArticle(sosmedDataSeo).toString()
    schemaReviewCount = GetItemListReviewCountArticle(sosmedDataSeo).toString()
  }

  // Microsite Brand Schema
  if (!isEmpty(sosmedDataSeo) && sosmedDataSeo?.shop_in_shop) {
    schemaProductMicrosite.push(`{
      "@context": "https://schema.org",
      "@type": "ItemList",
      "itemListElement": [${GetItemListElement(
        sosmedDataSeo?.products,
      ).toString()}]
    }`)
    schemaMicrosite =
      GetItemListElementBrandBreadcrumbList(sosmedDataSeo).toString()
    schemaReviewCount = GetItemListReviewCountBrands(sosmedDataSeo).toString()
  }

  return (
    <Head>
      {/* schema for homepage only */}
      {constantContext?.pageFrom === 'home'
        ? schema.map((data, index) => (
            <script
              type='application/ld+json'
              dangerouslySetInnerHTML={{ __html: data }}
              key={'schema_' + index}
            />
          ))
        : null}
      {/* schema for product detail */}
      {!isEmpty(schemaProductDetail)
        ? schemaProductDetail.map((data, index) => (
            <script
              type='application/ld+json'
              dangerouslySetInnerHTML={{ __html: data }}
              key={'schema_' + index}
            />
          ))
        : null}
      {/* schema  for ms artikel */}
      {!isEmpty(schemaMicrosite) ? (
        <script
          type='application/ld+json'
          dangerouslySetInnerHTML={{
            __html: '[' + schemaMicrosite.toString() + ']',
          }}
        />
      ) : null}
      {/* schema product for catalog, store */}
      {!isEmpty(schemaProduct) &&
      (isEmpty(sosmedDataSeo) ||
        (sosmedDataSeo && !sosmedDataSeo.shop_in_shop)) ? (
        <script
          className={
            constantContext?.pageFrom === 'catalog' ||
            constantContext?.pageFrom === 'store'
              ? 'category-schema'
              : ''
          }
          type='application/ld+json'
          dangerouslySetInnerHTML={{
            __html: '[' + schemaProduct.toString() + ']',
          }}
        />
      ) : null}
      {/* schema product for catalog, store */}
      {!isEmpty(schemaProductMicrosite) &&
      (!isEmpty(sosmedDataSeo) ||
        (sosmedDataSeo && sosmedDataSeo.shop_in_shop)) ? (
        <script
          className={sosmedDataSeo?.shop_in_shop ? 'category-schema' : ''}
          type='application/ld+json'
          dangerouslySetInnerHTML={{
            __html: '[' + schemaProductMicrosite.toString() + ']',
          }}
        />
      ) : null}
      {/* schema review for catalog, article, store */}
      {!isEmpty(schemaReviewCount) ? (
        <script
          className={
            constantContext?.pageFrom === 'catalog' ||
            constantContext?.pageFrom === 'store' ||
            sosmedDataSeo?.ogUrl?.includes('artikel') ||
            sosmedDataSeo?.shop_in_shop
              ? 'review-count-schema'
              : ''
          }
          type='application/ld+json'
          dangerouslySetInnerHTML={{ __html: schemaReviewCount.toString() }}
        />
      ) : null}
    </Head>
  )
}
