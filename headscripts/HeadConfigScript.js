import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
// import * as Sentry from '@sentry/browser'

import config from '../../../config'
import { useCartAuthContext } from '../../../shared/global/context/CartAuthContext'
import { getRrSID } from '../utils/GetRupaUID'
import { ACE_B2B_PREFIX } from '../utils/constants/AceConstants'

export default function HeadConfigScript({ chatBypassScroll, isBot = false }) {
  const [onShowLennaScript, setOnShowLennaScript] = useState(false)
  const [payloadAuth, setPayloadAuth] = useState(null)
  const { state: cartAuthContext } = useCartAuthContext()
  const auth = cartAuthContext?.auth
  const minicartId = cartAuthContext?.minicart_id
  const isProduction =
    config.node_env === 'production' && config.isLiveSite && !isBot
  let clarityScript = `(function(c,l,a,r,i,t,y){
    c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
    t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
    y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
    })(window, document, "clarity", "script", "jwv5cr1uzo");`

  useEffect(() => {
    window.addEventListener('scroll', handleOnShowLennaScript, {
      passive: true,
    })

    return () => {
      window.removeEventListener('scroll', handleOnShowLennaScript)
    }
  }, [])

  useEffect(() => {
    // ? Temp disable
    // async function registerNotifSW() {
    //   try {
    //     if ('serviceWorker' in navigator) {
    //       await navigator.serviceWorker.register('/notif-sw.js', {
    //         scope: '/firebase',
    //       })
    //     }
    //   } catch (err) {
    //     console.log(err)
    //     Sentry.captureException(err)
    //   }
    // }
    async function handleRrSID() {
      let rrSID = await getRrSID()
      if (isProduction) {
        if (rrSID) {
          window.clarity('set', 'rr-sid', rrSID)
        }
      }
    }
    // registerNotifSW()
    handleRrSID()
  }, [])

  const handleOnShowLennaScript = () => {
    setOnShowLennaScript(true)
  }
  let lenna = null
  let moengage = null
  const timeStamp = Math.floor(Date.now() / 1000)

  useEffect(() => {
    if (chatBypassScroll) {
      if (config.activeLenna === true) {
        setOnShowLennaScript(true)
      }
    }
  }, [chatBypassScroll])

  useEffect(() => {
    if (auth !== null) {
      const userName = auth?.user?.name || auth?.user?.first_name
      const userEmail = auth?.user?.email
      const userPhone = auth?.user?.phone
      const userCustomerId = auth?.user?.customer_id
      if (userCustomerId && minicartId && isProduction) {
        window.clarity('identify', String(userCustomerId), String(userEmail))
        window.clarity('set', 'name', userName)
        window.clarity('set', 'phone', userPhone)
        window.clarity('set', 'minicart_id', minicartId)
        if (config.companyCode === 'AHI') {
          window.clarity('set', 'header', navigator)
        }
      }
      setPayloadAuth(
        `{LennaWebchatInit("PdyMgd","1aKRMe");UserAuth("${userName}", "${userEmail}", "${userPhone}");};`,
      )
    } else {
      setPayloadAuth(
        `{LennaWebchatInit("PdyMgd","1aKRMe");UserAuth("Rupers", "${timeStamp}@ruparupa.com", "0");};`,
      )
    }
  }, [auth, minicartId])

  if (config.moengageScript && config.moengageAppId && isProduction) {
    moengage = `
      ${config.moengageScript}

      Moengage = moe({
        app_id: '${config.moengageAppId}',
        debug_logs: ${config.moengageDebugLevel},
        swPath: '/moengage-sw.js'
      });
    `
  }

  if (
    config.lennaScript &&
    config.lennaScript !== '' &&
    config.activeLenna &&
    onShowLennaScript &&
    payloadAuth
  ) {
    lenna = `
    ${config.lennaScript + payloadAuth}
    `
  }

  const router = useRouter()

  return (
    <Head>
      {moengage && (
        <script
          rel='dns-prefetch'
          dangerouslySetInnerHTML={{ __html: moengage }}
          crossOrigin='true'
        />
      )}
      {lenna &&
        !router?.pathname?.includes(`${ACE_B2B_PREFIX}`) &&
        router?.query?.floatingBar !== 'true' && (
          <script
            rel='dns-prefetch'
            defer
            dangerouslySetInnerHTML={{ __html: lenna }}
            crossOrigin='true'
          />
        )}
      {/* {config.hotjarScript && (
        <script
          rel='dns-prefetch'
          defer
          dangerouslySetInnerHTML={{ __html: config.hotjarScript }}
          crossOrigin='true'
        />
      )} */}
      {/* <script rel='dns-prefetch' defer src='https://wchat.freshchat.com/js/widget.js' crossOrigin /> */}
      <link
        href='https://www.ruparupa.com/opensearch.xml'
        title='Ruparupa'
        type='application/opensearchdescription+xml'
        rel='search'
      />
      {isProduction && (
        <script
          type='text/javascript'
          dangerouslySetInnerHTML={{ __html: clarityScript }}
          crossOrigin='true'
          defer
        />
      )}
    </Head>
  )
}
