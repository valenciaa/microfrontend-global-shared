// todo: Page that using this is: Homepage, Product, Anniversary, Payday, Custom Furniture, Tahu (some of it, need to ask first), Store Pickup, Static
// ? Tahu =  payday, furnifest, anniversary, wowsale, missace-gratis-ongkir, poin hangus, semua-promosi (based on company code),
import Head from 'next/head'
import React from 'react'
import isEmpty from 'lodash/isEmpty'
import { useRouter } from 'next/router'
import config from '../../../config'
import { useSeoContext } from '../context/SeoContext'
import { useConstantsContext } from '../context/ConstantsContext'
import { metaConstructor } from '../utils/MetaConstructor'
import { NumberWithCommas } from '../utils'
import { ACE_HARDWARE } from '../utils/constants/AceConstants'

export default function HeadSosmed() {
  const {
    sosmedDataSeo,
    categoriesSeo,
    productsSeo,
    metaDataSeo,
    algoliaProductsSeo,
  } = useSeoContext()
  const router = useRouter()
  const constantsContext = useConstantsContext()

  const pageFrom = constantsContext?.pageFrom
  let currentUrl = config.canonicalURL
  const appliedMeta = metaConstructor(
    constantsContext?.pageFrom,
    metaDataSeo,
  ) || {
    title: config.defTitle,
    keywords: '',
    description: config.defMetaDescription,
  }
  const data = {
    itemPropName:
      config.sosmedTitle ||
      config.ruparupaName + ': Situs Belanja Online Berbagai Kebutuhan',
    itemPropDescription:
      config.sosmedDescription ||
      `Online Store: ${ACE_HARDWARE}, Informa, Toys Kingdom, etc. Jaminan 100% Produk ASLI, Garansi, Belanja Online Aman & Nyaman. Semua ada disini. Let's Get MORE!`,
    itemPropImage:
      config.sosmedImage ||
      config?.assetsURL + `images/icons/ruparupa/icon-144x144.png`,
    sosmedCard: 'product',
    sosmedTitle:
      config.sosmedTitle ||
      config.ruparupaName + ': Situs Belanja Online Berbagai Kebutuhan',
    sosmedDescription:
      config.sosmedDescription ||
      `Online Store: ${ACE_HARDWARE}, Informa, Toys Kingdom, etc. Jaminan 100% Produk ASLI, Garansi, Belanja Online Aman & Nyaman. Semua ada disini. Let's Get MORE!`,
    sosmedImage:
      config.sosmedImage ||
      config?.assetsURL + `images/icons/ruparupa/icon-144x144.png`,
    ogType: 'website',
    ogUrl: '',
    ogTitle: '',
    ogImage: '',
    ogDescription: '',
    ogSiteName: '',
  }

  switch (config.companyCode) {
    case 'AHI':
      data.ogUrl = 'https://www.ruparupa.com/' + ACE_HARDWARE //ACE_HARDWARE
      data.ogTitle = `${ACE_HARDWARE} - Official Store`
      data.ogImage =
        config?.assetsURL + `images/icons/azko/icon-144x144.png` // ACE_IMAGE
      data.ogDescription = `Toko Online Resmi ${ACE_HARDWARE} ✅ Belanja ${ACE_HARDWARE} Online · 100% Produk ASLI · Garansi Resmi ${ACE_HARDWARE} ✅`
      data.ogSiteName = config.ruparupaName
      break

    case 'HCI':
      data.ogUrl =
        'https://www.ruparupa.com/' + (config.entryPoint || 'informa')
      data.ogTitle = 'Informa Official Store in ' + config.ruparupaName
      data.ogImage =
        'https://res.cloudinary.com/homecenterindonesia/image/upload/w_200,h_200,f_auto,q_auto/v1543394782/seo/opengraph/logo_informa_square_blue.jpg'
      data.ogDescription =
        'Toko Furniture & Mebel Online Terlengkap & Terbesar di Indonesia › Produk Original ✅ Harga Terjangkau 👍 Cicilan 0% ⚡ FREE Ongkir*'
      data.ogSiteName = 'Informa Official Store'
      break

    case 'TGI':
      data.ogUrl = 'https://www.ruparupa.com/toyskingdomonline/'
      data.ogTitle = 'Toys Kingdom - Official Online Store'
      data.ogImage =
        'https://cdn.ruparupa.io/media/promotion/toyskingdom/new-logo-tgi/logo-header-tgi-outline-rev.png'
      data.ogDescription =
        'Toko Online Resmi Toys Kingdom Indonesia ✅ Belanja Toys Kingdom Online · 100% Produk ASLI · Garansi Resmi Toys Kingdom ✅'
      data.ogSiteName = 'Toyskingdom Official Store'
      break

    default: // ODI
      data.ogUrl = 'https://www.ruparupa.com/'
      data.ogTitle =
        config.ruparupaName + ': Situs Belanja Online Berbagai Kebutuhan'
      data.ogImage =
        config?.assetsURL + `images/icons/ruparupa/icon-144x144.png`
      data.ogDescription =
        `Online Store: ${ACE_HARDWARE}, Informa, Toys Kingdom, etc. Jaminan 100% Produk ASLI, Garansi, Belanja Online Aman & Nyaman. Semua ada disini. Let` +
        "'" +
        's Get MORE!'
      data.ogSiteName = config.ruparupaName
      break
  }

  if (router && router.asPath) {
    const cleanUrl = router?.asPath?.split('?') || ''
    if (cleanUrl.length > 0) {
      currentUrl += cleanUrl?.[0]?.slice(1)
    }
  }

  const defaultContentByItemProp = {
    'name': data?.itemPropName,
    'description': data?.itemPropDescription,
    'image': data?.itemPropImage,
    'twitter:card': data?.sosmedCard,
    'twitter:title': data?.sosmedTitle,
    'twitter:image:src': data?.sosmedImage,
    'twitter:description': data?.sosmedDescription,
    'og:type': data?.ogType,
    'og:url': data?.ogUrl,
    'og:title': data?.ogTitle,
    'og:image': data?.ogImage,
    'og:description': data?.ogDescription,
    'og:site_name': data?.ogSiteName,
  }

  const getContentByItemProp = (itemProp, fallbackContent = '') => {
    // set by default content item props
    let content = fallbackContent || defaultContentByItemProp[itemProp]

    categoriesSeo &&
      categoriesSeo.additional_header &&
      categoriesSeo.additional_header.map((categoryDetail) => {
        if (
          categoryDetail &&
          categoryDetail?.attributes &&
          categoryDetail?.attributes?.length >= 2
        ) {
          const checkFirstAttribute =
            categoryDetail?.attributes?.[0]?.itemprop === itemProp ||
            categoryDetail?.attributes?.[0]?.name === itemProp ||
            categoryDetail?.attributes?.[0]?.property === itemProp
          if (checkFirstAttribute) {
            content = categoryDetail?.attributes?.[1]?.content
          }
        }
      })

    return content
  }

  const getContentImageCc = () => {
    if (!isEmpty(productsSeo)) {
      if (productsSeo?.[0]?.variants?.[0]?.images?.[0]?.full_image_url) {
        data.ogImage =
          productsSeo?.[0]?.variants?.[0]?.images?.[0]?.full_image_url
      } else {
        data.ogImage =
          'https://res.cloudinary.com/ruparupa-com' +
          productsSeo?.[0]?.variants?.[0]?.images?.[0]?.image_url
      }
    }

    if (!isEmpty(algoliaProductsSeo)) {
      if (algoliaProductsSeo?.[0]?.[0]?.images?.[0]?.full_image_url) {
        data.ogImage = algoliaProductsSeo?.[0]?.[0]?.images?.[0]?.full_image_url
      } else {
        data.ogImage =
          'https://res.cloudinary.com/ruparupa-com' +
          algoliaProductsSeo?.[0]?.[0]?.images?.[0]?.image_url
      }
    }

    return data.ogImage
  }

  switch (pageFrom) {
    case 'catalog':
      data.itemPropName = getContentByItemProp('name')
      data.itemPropDescription = getContentByItemProp('description')
      data.itemPropImage = getContentByItemProp('image')
      data.sosmedCard = getContentByItemProp('twitter:card', 'category')
      data.sosmedTitle = getContentByItemProp(
        'twitter:title',
        categoriesSeo?.meta_title,
      )
      data.sosmedDescription = getContentByItemProp(
        'twitter:description',
        categoriesSeo?.meta_description,
      )
      data.sosmedImage = getContentByItemProp(
        'twitter:image:src',
        getContentImageCc(),
      )
      data.ogType = getContentByItemProp('og:type', 'product.group')
      data.ogTitle = getContentByItemProp('og:title', categoriesSeo?.meta_title)
      data.ogDescription = getContentByItemProp(
        'og:description',
        categoriesSeo?.meta_description,
      )
      data.ogImage = getContentByItemProp('og:image', getContentImageCc())
      data.ogUrl = getContentByItemProp('og:url', currentUrl)
      data.ogSiteName = getContentByItemProp('og:site_name', 'Ruparupa.com')
      break

    case 'jual':
      data.itemPropName = getContentByItemProp('name')
      data.itemPropDescription = getContentByItemProp('description')
      data.itemPropImage = getContentByItemProp('image')
      data.sosmedCard = 'product'
      data.sosmedTitle = getContentByItemProp(
        'twitter:title',
        appliedMeta?.title,
      )
      data.sosmedDescription = getContentByItemProp(
        'twitter:description',
        appliedMeta?.description,
      )
      data.sosmedImage = getContentByItemProp('og:image', getContentImageCc())
      data.ogType = 'product.group'
      data.ogTitle = getContentByItemProp('og:title', appliedMeta?.title)
      data.ogDescription = getContentByItemProp(
        'og:description',
        appliedMeta?.description,
      )
      data.ogImage = getContentByItemProp('og:image', getContentImageCc())
      data.ogUrl = currentUrl
      data.ogSiteName = 'ruparupa.com'
      break

    case 'brand':
      data.itemPropName = getContentByItemProp('name')
      data.itemPropDescription = getContentByItemProp('description')
      data.itemPropImage = getContentByItemProp('image')
      data.sosmedCard = getContentByItemProp('twitter:card', 'category')
      data.sosmedTitle = getContentByItemProp(
        'twitter:title',
        appliedMeta?.title,
      )
      data.sosmedDescription = getContentByItemProp(
        'twitter:description',
        appliedMeta?.description,
      )
      data.sosmedImage = getContentByItemProp(
        'twitter:image:src',
        getContentImageCc(),
      )
      data.ogType = getContentByItemProp('og:type', 'product.group')
      data.ogTitle = getContentByItemProp('og:title', appliedMeta?.title)
      data.ogDescription = getContentByItemProp(
        'og:description',
        appliedMeta?.description,
      )
      data.ogImage = getContentByItemProp('og:image', getContentImageCc())
      data.ogUrl = getContentByItemProp('og:url', currentUrl)
      data.ogSiteName = getContentByItemProp('og:site_name', 'Ruparupa.com')
      break

    case 'product-detail':
      data.itemPropName = metaDataSeo?.meta_title?.replace('Jual', '')
      data.itemPropDescription = `Harga Rp ${NumberWithCommas(
        metaDataSeo?.meta_prices,
      )} - Lihat Produk ini sekarang`
      data.itemPropImage = metaDataSeo?.meta_image
      data.sosmedCard = 'product'
      data.sosmedTitle = metaDataSeo?.meta_title?.replace('Jual', '')
      data.sosmedDescription = `Harga Rp ${NumberWithCommas(
        metaDataSeo?.meta_prices,
      )} - Lihat Produk ini sekarang`
      data.sosmedImage = metaDataSeo?.meta_image
      data.ogType = 'product'
      data.ogTitle = metaDataSeo?.meta_title?.replace('Jual', '')
      data.ogDescription = `Harga Rp ${NumberWithCommas(
        metaDataSeo?.meta_prices,
      )} - Lihat Produk ini sekarang`
      data.ogImage = metaDataSeo?.meta_image
      data.ogUrl = metaDataSeo?.meta_url
      data.ogSiteName = 'Ruparupa.com'
      break

    case 'tahu':
      if (!isEmpty(sosmedDataSeo)) {
        Object.entries(sosmedDataSeo).map(([property, value]) => {
          if (!isEmpty([property]) && !isEmpty(value)) {
            data[property] = value
          }
        })
      }
      break

    default:
      break
  }

  return (
    <>
      {/* item prop */}
      <Head>
        <meta
          itemProp='name'
          content={data.itemPropName}
        />
        <meta
          itemProp='description'
          content={data.itemPropDescription}
        />
        <meta itemProp='image' content={data.itemPropImage} />
      </Head>

      {/* twitter */}
      <Head>
        <meta name='twitter:card' content={data.sosmedCard} />
        <meta name='twitter:site' content='@ruparupacom' />
        <meta
          name='twitter:title'
          content={data.sosmedTitle}
        />
        <meta name='twitter:image:src' content={data.sosmedImage} />
        <meta
          name='twitter:description'
          content={data.sosmedDescription}
        />
        <meta name='twitter:creator' content='@ruparupacom' />
      </Head>
      {pageFrom && pageFrom === 'product-detail' && (
        <Head>
          <meta
            name='twitter:data1'
            content={`Rp ${NumberWithCommas(metaDataSeo?.meta_prices)}`}
          />
          <meta name='twitter:label1' content='Harga' />
          <meta name='twitter:data2' content={metaDataSeo?.meta_brand} />
          <meta name='twitter:label2' content='Brand' />
        </Head>
      )}

      {/* facebook */}
      <Head>
        <meta property='og:type' content={data.ogType} />
        <meta property='og:url' content={data.ogUrl} />
        <meta
          property='og:title'
          content={data.ogTitle}
        />
        <meta property='og:image' content={data.ogImage} />
        <meta
          property='og:description'
          content={data.ogDescription}
        />
        <meta property='og:site_name' content={data.ogSiteName} />
        <meta property='fb:app_id' content='794695870671633' />
        <meta property='fb:pages' content='1523809694578830' />
      </Head>
      {pageFrom && pageFrom === 'product-detail' && (
        <Head>
          <meta
            property='product:price:amount'
            content={parseInt(metaDataSeo?.meta_prices) || ''}
          />
          <meta property='product:price:currency' content='IDR' />
          <meta
            property='product:retailer_item_id'
            content={metaDataSeo?.sku}
          />
          <meta
            property='product:availability'
            content={metaDataSeo?.is_in_stock}
          />
          <meta property='product:brand' content={metaDataSeo?.meta_brand} />
        </Head>
      )}
    </>
  )
}
