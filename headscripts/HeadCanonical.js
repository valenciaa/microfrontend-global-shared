// todo: Page that using this is: Homepage, Product, Static, Category, Anniversary, payday, jual, superten
import isEmpty from 'lodash/isEmpty'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React from 'react'
import config from '../../../config'
import { useConstantsContext } from '../context/ConstantsContext'
import { useSeoContext } from '../context/SeoContext'
import { prefixPathChecker, prefixPathModifier } from '../utils/PrefixPathUtils'

export default function HeadCanonical() {
  const { metaDataSeo, categoriesSeo, sosmedDataSeo, autoLinkSeo } =
    useSeoContext()
  const router = useRouter()

  const constantsContext = useConstantsContext()
  const seoContext = useSeoContext()
  const pageFrom = constantsContext?.pageFrom
  // config.canonical need to be contain the BU company name
  let url = config.canonicalURL
  if (autoLinkSeo && autoLinkSeo.seo_footer) {
    seoContext.setSeoFooter(autoLinkSeo?.seo_footer || '')
  }

  if (router && router.asPath) {
    let routePath = router?.asPath

    if (routePath && prefixPathChecker(routePath)) {
      routePath = prefixPathModifier(routePath)
    }

    // For this section, we need to clean up the "companyName" because if you access BU web, the route you receive initially didn't have a prefix
    // for BU's website, but somehow after it rendered, the prefix appeared. As a result, "config.canonicalURL" needs to have the prefix,
    // and we just clean up everything here if they have other paths.
    const cleanCompanyName =
      config.companyName !== 'ruparupa'
        ? routePath?.replace(`/${config.companyName}`, '')
        : routePath

    const cleanUrl = cleanCompanyName?.split('?') || ''

    if (cleanUrl.length > 0) {
      url += cleanUrl?.[0]?.slice(1)
    }
  }

  if ((pageFrom === 'catalog' || pageFrom === 'brand') && categoriesSeo) {
    url = categoriesSeo.canonical_url ? categoriesSeo.canonical_url : url
  }

  if (pageFrom === 'jual') {
    if (autoLinkSeo && !isEmpty(autoLinkSeo.seo_header)) {
      const filter =
        autoLinkSeo &&
        autoLinkSeo?.seo_header.filter(
          (e) => e?.attributes?.[0]?.name === 'canonical',
        )
      if (!isEmpty(filter)) {
        url = filter?.[0]?.attributes?.[1]?.content || url
      }
    } else {
      url = url + `?query=${encodeURIComponent(router?.query?.query)}`
    }
  }

  if (pageFrom === 'tahu') {
    url = sosmedDataSeo.ogUrl
  }

  if (router && router.asPath === '/brands/informa.html') {
    url = 'https://www.ruparupa.com/informastore'
  }

  // check if the products in pdp only has 1 or more value
  let count = 0

  for (const key in metaDataSeo?.company_code) {
    if (metaDataSeo?.company_code[key] === 10) {
      count++
    }
  }

  if (pageFrom === 'product-detail' && count > 1) {
    url = url
      .replace(config.canonicalURL, 'https://www.ruparupa.com/')
      .replace('/informastore', '')
      .replace('/acestore', '')
      .replace('/toyskingdomonline', '')
  }

  return (
    <Head>
      <link rel='canonical' href={url} />
    </Head>
  )
}
