// todo: Page that using this is: Homepage, Product, static, category, superten, anniversary, payday, custom-furniture, jual
import Head from 'next/head'
import { useRouter } from 'next/router'
import React from 'react'
import config from '../../../config'

export default function HeadAlternate() {
  const router = useRouter()
  const asPath =
    config.baseURL + router?.asPath?.split('?')?.[0]?.substring(1) || ''
  const alternateUrl =
    asPath || config.alternateUrl || 'https://m.ruparupa.com/'
  return (
    <Head>
      <link
        rel='alternate'
        media='only screen and (max-width: 500px)'
        href={alternateUrl}
      />
    </Head>
  )
}
