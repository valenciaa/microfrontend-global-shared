import React, { useEffect, useState } from 'react'
import HeadAlternate from './HeadAlternate'
import HeadCanonical from './HeadCanonical'
import HeadConfigScript from './HeadConfigScript'
import HeadGtm from './HeadGtm'
import HeadMeta from './HeadMeta'
import HeadLinkCrossOrigin from './HeadLinkCrossOrigin'

import HeadSchema from './HeadSchema'
import HeadSosmed from './HeadSosmed'
import HeadUniversalLink from './HeadUniversalLink'

export default function HeadScripts({
  isBot,
  pageFrom,
  chatBypassScroll = false,
  isDelayHeadGTM = false,
}) {
  const [isRenderHeadGtm, setIsRenderHeadGtm] = useState(false)

  useEffect(() => {
    const renderHeadGtm = () => {
      // ! DONT ADD ANYMORE CODE, RACE CONDITION POTENSIAL
      if (!isRenderHeadGtm && isDelayHeadGTM) {
        setIsRenderHeadGtm(true)
      }
    }
    const caller = () => setTimeout(renderHeadGtm)
    window.addEventListener('load', caller, { once: true })
    window.addEventListener('DOMContentLoaded', caller, { once: true })

    return () => {
      window.removeEventListener('load', caller)
      window.removeEventListener('DOMContentLoaded', caller)
    }
  }, [])

  return (
    <>
      <HeadLinkCrossOrigin />
      <HeadMeta />
      <HeadSosmed />
      <HeadCanonical />
      <HeadConfigScript chatBypassScroll={chatBypassScroll} isBot={isBot} />
      <HeadSchema />
      <HeadAlternate />
      {!isDelayHeadGTM || isRenderHeadGtm ? (
        <HeadGtm pageFrom={pageFrom} />
      ) : null}
      <HeadUniversalLink />
    </>
  )
}
