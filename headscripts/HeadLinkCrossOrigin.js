import Head from 'next/head'

export default function HeadLinkCrossOrigin() {
  // put every thrid party link here that we used in the site
  const linkMeta = [
    'https://res.cloudinary.com',
    'https://cdn.ruparupa.io',
    'https://google-analytics.com',
    'https://recommender.scarabresearch.com',
    'https://wchat.freshchat.com',
  ]

  return (
    <>
      {linkMeta.map((url, index) => {
        return (
          <Head key={index}>
            <link
              rel='preconnect'
              key={'crossorigin-preconnect-' + index}
              href={url}
              crossOrigin='anonymous'
            />
            <link
              rel='dns-prefetch'
              key={'crossorigin-dnsprefetch-' + index}
              href={url}
            />
          </Head>
        )
      })}
    </>
  )
}
