// todo:  Page that using this is: Homepage, Product, Category
import Head from 'next/head'
import React, { useEffect, useState } from 'react'
import config from '../../../config'

export default function HeadGtm(pageFrom) {
  const [gtmDataLayerDeployed, setGtmDataLayerDeployed] = useState(
    `
        var productList = [];
        if (typeof dataLayer === 'undefined') {
          var dataLayer = [];
          
          ${config.gtmScript}
        }`,
  )

  useEffect(() => {
    let gtmDataLayer = ''
    // Tolong dicek kalau misalkan CSR, ini jalan atau tidak. Dalam proses dev waktu ini, hanya ada homepage sehingga selalu SSR jadi masuk ke else.
    // Before: if (!isServer)
    if (!process.browser) {
      websitePush()
    } else {
      if (pageFrom === 'home') {
        // every company code have difference GTM
        switch (config.companyCode) {
          case 'AHI':
            gtmDataLayer += `dataLayer.push({
                'event': 'halaman',
                'halaman': 'homepageAce'
              });
              `
            //! MOVE TO GLOBAL
            gtmDataLayer += `dataLayer.push({
                'event': 'website',
                'website': 'aceHardwareOnline'
              });`
            break
          case 'HCI':
            gtmDataLayer += `dataLayer.push({
                'event': 'halaman',
                'halaman': 'homepageInforma'
              });
              `
            //! MOVE TO GLOBAL
            gtmDataLayer += `dataLayer.push({
            'event': 'website',
            'website': 'informaOnline'
          });`
            break
          case 'TGI':
            gtmDataLayer += `dataLayer.push({
                    'event': 'halaman',
                    'halaman': 'homepageToys'
                  });
                  `

            //! MOVE TO GLOBAL
            gtmDataLayer += `dataLayer.push({
            'event': 'website',
            'website': 'toysKingdomOnline'
          });`
            break
          default:
            gtmDataLayer += `dataLayer.push({
                'event': 'halaman',
                'halaman': 'homepageRuparupa'
              });
              `
            gtmDataLayer += `dataLayer.push({
            'event': 'website',
            'website': 'ruparupa'
          });`
            break
        }
        setGtmDataLayerDeployed(gtmDataLayer)
      }
    }
  }, [])

  function websitePush() {
    if (window && window.dataLayer) {
      if (config.companyCode === 'HCI') {
        window.dataLayer.push({
          event: 'website',
          website: 'informaOnline',
        })
      } else if (config.companyCode === 'AHI') {
        window.dataLayer.push({
          event: 'website',
          website: 'aceHardwareOnline',
        })
      } else if (config.companyCode === 'TGI') {
        window.dataLayer.push({
          event: 'website',
          website: 'toysKingdomOnline',
        })
      } else {
        window.dataLayer.push({
          event: 'website',
          website: 'ruparupa',
        })
      }
    }
  }

  return (
    <Head>
      {gtmDataLayerDeployed && (
        <script dangerouslySetInnerHTML={{ __html: gtmDataLayerDeployed }} />
      )}
      {config.appsflyerWebKey && (
        <script dangerouslySetInnerHTML={{ __html: config.appsflyerWebKey }} />
      )}
    </Head>
  )
}
