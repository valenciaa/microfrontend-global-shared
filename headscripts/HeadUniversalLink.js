import React from 'react'
import config from '../../../config'
import Head from 'next/head'

export default function HeadUniversalLink() {
  return (
    <Head>
      <link
        rel='assetlinks.json file'
        href={`${config.baseURL}.well-known/assetlinks.json`}
      />
      <link
        rel='apple-app-site-association file'
        href={`${config.baseURL}.well-known/apple-app-site-association`}
      />
    </Head>
  )
}
