// if user not login, get from ugi, if user login get from user data
// get user data from localforage
//! Need to connect first to main homepage
import { useEffect, useState } from 'react'
import { GetUniqueId } from '../utils'

export default function HeadUserId() {
  const [ugid, setUgid] = useState(null)
  const auth = {}
  useEffect(() => {
    async function fetchID() {
      const ugid = await GetUniqueId()
      setUgid(ugid)
      // Tolong dicek kalau misalkan CSR, ini jalan atau tidak. Dalam proses dev waktu ini, hanya ada homepage sehingga selalu SSR jadi tidak masuk if.
      // Before: if (!isServer)
      if (!process.browser) {
        userIdPush(auth)
      }
    }
    fetchID()
  }, [])

  function userIdPush(auth) {
    if (auth && !auth?.fetching && auth?.user?.email) {
      if (window && window.dataLayer) {
        window.dataLayer.push({
          userId: auth?.user?.customer_id,
        })
      }
    } else {
      if (window && window.dataLayer) {
        window.dataLayer.push({
          userId: ugid,
        })
      }
    }
    this.setState({ serverPush: true })
  }
  return null
}
