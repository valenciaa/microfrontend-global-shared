// todo: Page that using this is: Homepage, Product, Static, Category, Anniversary, 404, payday-suprise, apply-kartu-kredit, promo-bca, jual, custom-furniture, tahu
// ? Tahu = payday, furnifest, promo-9-9, promo-10-10, wowsale, miss-ace-gratis-ongkir, poin-hangus, semua-promosi  (based on company code)
import isEmpty from 'lodash/isEmpty'
import Head from 'next/head'
import React, { useEffect } from 'react'
import config from '../../../config'
import { useConstantsContext } from '../context/ConstantsContext'
import { useSeoContext } from '../context/SeoContext'
import CheckCleanUrl from '../utils/CheckCleanUrl'
import { metaConstructor } from '../utils/MetaConstructor'
import { prefixPathChecker } from '../utils/PrefixPathUtils'
import { pushDiscordNotification } from '../utils/PushDiscordNotification'

export default function HeadMeta() {
  const { metaDataSeo, categoriesSeo, is404, autoLinkSeo, urlSeo } =
    useSeoContext()
  const isIndexFollow = !is404
  let robots = 'INDEX,FOLLOW'
  let robotsKey = 'index-follow'
  let keywords = ''
  const isProduction = config.node_env === 'production' && config.isLiveSite

  const constantContext = useConstantsContext()

  const pageType = constantContext?.pageFrom

  const appliedMeta = !isEmpty(metaDataSeo)
    ? metaConstructor(
        constantContext?.pageFrom,
        metaDataSeo,
        autoLinkSeo?.seo_header,
        urlSeo,
      )
    : {
        title: config.defTitle,
        keywords: '',
        description: config.defMetaDescription,
      }

  if (isProduction) {
    if (!isIndexFollow) {
      robots = 'NOINDEX,NOFOLLOW'
      robotsKey = 'noindex-nofollow'
    }

    if (urlSeo && !CheckCleanUrl(urlSeo)) {
      robots = 'NOINDEX,FOLLOW'
      robotsKey = 'noindex-follow'
    }

    if (pageType === 'catalog' || pageType === 'brand') {
      if (urlSeo && CheckCleanUrl(urlSeo)) {
        robots =
          categoriesSeo && categoriesSeo.index_setup
            ? categoriesSeo.index_setup.toUpperCase()
            : 'INDEX,FOLLOW'
        robotsKey =
          categoriesSeo && categoriesSeo.index_setup
            ? categoriesSeo.index_setup
            : 'index-follow'
      } else {
        robots = 'NOINDEX,FOLLOW'
        robotsKey = 'noindex-follow'
      }
    }

    if (pageType === 'tahu') {
      if (urlSeo && CheckCleanUrl(urlSeo)) {
        robotsKey = metaDataSeo?.meta_robot
        if (robotsKey === 'index-follow') {
          robots = 'INDEX,FOLLOW'
        } else if (robotsKey === 'noindex-follow') {
          robots = 'NOINDEX,FOLLOW'
        } else if (robotsKey === 'index-nofollow') {
          robots = 'INDEX,NOFOLLOW'
        } else if (robotsKey === 'noindex-nofollow') {
          robots = 'NOINDEX,NOFOLLOW'
        }
      } else {
        robots = 'NOINDEX,FOLLOW'
        robotsKey = 'noindex-follow'
      }
    }

    if (urlSeo && CheckCleanUrl(urlSeo) && pageType === 'store-location') {
      robots = 'INDEX,FOLLOW'
      robotsKey = 'index-follow'
    }

    if (
      pageType === 'product-detail' &&
      metaDataSeo &&
      metaDataSeo?.company_code
    ) {
      if (
        (config?.companyCode === 'ODI' &&
          metaDataSeo?.company_code?.ODI !== 10) ||
        (config?.companyCode === 'TGI' &&
          metaDataSeo?.company_code?.TGI !== 10) ||
        (config?.companyCode === 'HCI' &&
          metaDataSeo?.company_code?.HCI !== 10) ||
        (config?.companyCode === 'AHI' && metaDataSeo?.company_code?.AHI !== 10)
      ) {
        robots = 'NOINDEX,NOFOLLOW'
        robotsKey = 'noindex-nofollow'
      }
    }

    if (pageType === 'jual') {
      if (urlSeo && CheckCleanUrl(urlSeo)) {
        if (autoLinkSeo && !isEmpty(autoLinkSeo.seo_header)) {
          const filter = autoLinkSeo?.seo_header?.find(
            (e) => e?.attributes?.[0]?.name === 'robots',
          )
          robots = filter?.attributes?.[1]?.content || 'INDEX,FOLLOW'
          robotsKey = filter?.attributes?.[1]?.content || 'index,follow'
        }
      } else {
        robots = 'NOINDEX,FOLLOW'
        robotsKey = 'noindex-follow'
      }
    }

    if (
      urlSeo === '/brands/informa.html' ||
      urlSeo === '/p/rr-other-charges-too.html'
    ) {
      robots = 'NOINDEX,FOLLOW'
      robotsKey = 'noindex-follow'
    }

    if (urlSeo && prefixPathChecker(urlSeo)) {
      robots = 'NOINDEX,NOFOLLOW'
      robotsKey = 'noindex-nofollow'
    }
  } else {
    robots = 'NOINDEX,NOFOLLOW'
    robotsKey = 'noindex-nofollow'
  }

  useEffect(() => {
    const pageTypePCP = ['catalog', 'brand', 'jual', 'store'].includes(pageType)
    const isCleanUrl = CheckCleanUrl(urlSeo)
    const robotsData = robots?.replace(/\s+/g, '')?.toLowerCase()

    const shouldSendNotification =
      pageTypePCP &&
      ((isCleanUrl && robotsData !== 'index,follow') ||
        (!isCleanUrl && robotsData !== 'noindex,follow'))

    if (isProduction && urlSeo && shouldSendNotification) {
      pushDiscordNotification(urlSeo, pageType)
    }
  }, [])

  let manifestLink = config.assetsURL + 'sw/manifest.json'
  if (config.companyCode === 'AHI') {
    manifestLink = config.assetsURL + 'sw/azko-manifest.json'
  } else if (config.companyCode === 'HCI') {
    manifestLink = config.assetsURL + 'sw/informa-manifest.json'
  }

  if (autoLinkSeo && !isEmpty(autoLinkSeo.seo_header)) {
    const filter =
      autoLinkSeo &&
      autoLinkSeo?.seo_header.filter(
        (e) => e?.attributes?.[0]?.name === 'keywords',
      )
    if (!isEmpty(filter)) {
      keywords = filter?.[0]?.attributes?.[1]?.content || keywords
    }
  }

  return (
    <>
      <Head>
        {/* this is depends on the company Code, check your config */}
        <title>{appliedMeta.title}</title>
        <link
          rel='icon'
          type='image/png'
          sizes='16x16'
          href='https://cdn.ruparupa.io/fit-in/16x16/promotion/ruparupa/asset/logorr-144x144.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='32x32'
          href='https://cdn.ruparupa.io/fit-in/32x32/promotion/ruparupa/asset/logorr-144x144.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='60x60'
          href='https://cdn.ruparupa.io/fit-in/60x60/promotion/ruparupa/asset/logorr-144x144.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='96x96'
          href='https://cdn.ruparupa.io/fit-in/96x96/promotion/ruparupa/asset/logorr-144x144.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='144x144'
          href='https://cdn.ruparupa.io/fit-in/144x144/promotion/ruparupa/asset/logorr-144x144.png'
        />

        <meta httpEquiv='Content-Type' content='text/html; charset=utf-8' />
        <meta
          name='viewport'
          content='width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1'
        />
        <meta
          name='apple-mobile-web-app-status-bar-style'
          content='black'
          key='apple-mobile-web-app-status-bar-style'
        />
        <meta
          name='msvalidate.01'
          content='9230D2514B0FDEC9BC3CAB2A1007608F'
          key='msvalidate.01'
        />
        <meta
          name='ahrefs-site-verification'
          content='23e7e841d524312f2ed5146a23234ef563f4ee64eae2bdcded257c6fdc363827'
          key='ahrefs-site-verification'
        />
        <meta
          name='p:domain_verify'
          content='0b789a70b789c02f49028ee612a07ba6'
          key='p:domain_verify'
        />
        <meta
          name='facebook-domain-verification'
          content='r913w3l0rehk7xdp5x2thtxosd012p'
          key='facebook-domain-verification'
        />
        <meta
          name='referrer'
          content='no-referrer-when-downgrade'
          key='referrer'
        />
      </Head>

      <Head>
        <meta name='robots' content={robots} key={robotsKey} />
      </Head>

      {config && config.enablePWA && config.environment === 'mobile' && (
        <Head>
          <meta name='mobile-web-app-capable' content='yes' key='pwa1' />
          <meta name='apple-mobile-web-app-capable' content='yes' key='pwa2' />
          <meta name='theme-color' content='#ffffff' key='pwa3' />
        </Head>
      )}
      <Head>
        <meta name='title' content={appliedMeta.title} key='title' />
        <meta
          name='description'
          content={appliedMeta.description}
          key='description'
        />
        {keywords && <meta name='keywords' content={keywords} />}
        <meta name='theme-color' content='#ffffff' key='theme-color' />
        {manifestLink !== '' && (
          <link rel='manifest' href={manifestLink} key='manifest' />
        )}
        <link
          rel='apple-touch-icon'
          sizes='32x32'
          href='https://cdn.ruparupa.io/fit-in/32x32/promotion/asset/icon-rr-16_16_px.jpeg'
          key='apple-touch-32'
        />
        {/* only use 32x32 */}
        {/* <link
          rel='apple-touch-icon'
          sizes='180x180'
          href={`${config.assetsIconURL}2.1/PWA-assets/touch-icon-iphone-retina.webp`}
          key='apple-touch-180'
        />
        <link
          rel='apple-touch-icon'
          sizes='167x167'
          href={`${config.assetsIconURL}2.1/PWA-assets/touch-icon-ipad-retina.webp`}
          key='167'
        /> */}
        {/* for apple related product about scaling when tried to click the input bar */}
      </Head>
    </>
  )
}
