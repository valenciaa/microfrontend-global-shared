const CheckCleanUrl = (url) => {
  const pathWithQuery = url?.startsWith('/') ? url?.substring(1) : url
  const queryString = pathWithQuery?.includes('?')
    ? pathWithQuery?.split('?')[1]
    : ''
  const queryParams = queryString
    ? queryString?.split('&')?.filter(Boolean)
    : []

  if (
    queryParams.some(
      (param) => param.includes('utm_source') || param.includes('itm_device'),
    )
  ) {
    return false
  }

  if (queryParams?.length === 0) {
    return true
  }

  if (queryParams?.length === 1 && queryParams[0]?.startsWith('query=')) {
    return true
  }

  if (queryParams?.length === 0) {
    return true
  }

  if (queryParams?.length === 1 && queryParams[0]?.startsWith('query=')) {
    return true
  }

  if (queryParams?.length === 1 && queryParams[0]?.startsWith('store_name=')) {
    return true
  }

  return false
}

export default CheckCleanUrl
