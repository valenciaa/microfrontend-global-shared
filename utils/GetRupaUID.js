const Cookies = require('js-cookie')
const dayjs = require('dayjs')

export function makeid(length) {
  var result = ''
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  var charactersLength = characters.length
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

const getRupaUID = async () => {
  const rupaUID = await Cookies.get('sessionrupaUID')
  const checkedrupaUID = rupaUID || ''

  if (checkedrupaUID) {
    return rupaUID
  } else {
    const unique =
      String(makeid(15)) + String(dayjs().unix()) + String(makeid(15))
    await Cookies.set('sessionrupaUID', unique.substring(0, 40), {
      expires: 365,
    })
    return unique
  }
}

const getRrSID = async () => {
  const rrSID = await Cookies.get('rr-sid')
  const checkedrrSID = rrSID || ''

  if (checkedrrSID) {
    return rrSID
  } else {
    const unique =
      String(makeid(5)) + String(dayjs().unix()) + String(makeid(10))
    await Cookies.set('rr-sid', unique.substring(0, 40), {
      expires: 365,
    })
    return unique
  }
}

export { getRupaUID, getRrSID }
