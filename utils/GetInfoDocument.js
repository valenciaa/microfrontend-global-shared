const GetInfoDocument = async (file) => {
  const document = await new Promise((resolve, reject) => {
    const reader = new FileReader() // eslint-disable-line
    reader.readAsDataURL(file)
    reader.onload = (e) => {
      const reversalDocument = e.target.result
      resolve(reversalDocument)
    }
    // Make sure to handle error states
    reader.onerror = function (e) {
      reject(e)
    }
  })
  return document
}
export { GetInfoDocument }
