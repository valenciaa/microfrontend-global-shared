import isEmpty from 'lodash/isEmpty'

const getFirstAndLastName = (fullName) => {
  let firstName, lastName
  if (!isEmpty(fullName)) {
    const nameInArray = fullName?.split(' ')
    if (nameInArray && nameInArray?.length > 1) {
      firstName = nameInArray?.[0]
      lastName = nameInArray?.[nameInArray?.length - 1]
    } else {
      firstName = nameInArray?.[0] ?? ' '
      lastName = ' '
    }
  }
  return { firstName, lastName }
}

export default getFirstAndLastName
