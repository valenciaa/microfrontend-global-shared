import GetParam from './GetParam'
const GetAffiliateProductLink = () => {
  const url = GetParam('deep_link_value')
    ? GetParam('deep_link_value').split('_')[1]
    : ''

  return url
}

export default GetAffiliateProductLink
