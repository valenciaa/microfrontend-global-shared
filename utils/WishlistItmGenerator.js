import config from '../../../config'

const getWishlistProductItm = (url, sku) => {
  const value = 'wishlist'
  const sourceKey = '?itm_source=' + value
  const campaignKey = '&itm_campaign=' + value
  const termKey = '&itm_term=' + sku
  const deviceKey = '&itm_device=' + config.environment
  const itmUrl = sourceKey + campaignKey + termKey + deviceKey
  return url + itmUrl
}

export default getWishlistProductItm
