const ItmConverter = (title) => {
  if (typeof title !== 'undefined') {
    title = title
      .replace(/\?/g, '')
      .replace(/ |#|\+/g, '-')
      .toLowerCase()
  } else {
    title = ''
  }
  return title.replace('%', '')
}
export default ItmConverter
