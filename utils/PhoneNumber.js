import isArray from 'lodash/isArray'

export const parsePhonePrefix = (countryList = [], countryCode = 'ID') => {
  if (isArray(countryList) && countryList.length) {
    const filteredCountry = countryList.filter(
      (el) => el.country_code === countryCode,
    )
    if (filteredCountry.length) {
      return filteredCountry[0].dial_code
    } else {
      return '+62'
    }
  } else {
    return '+62'
  }
}

export const parsePhoneLimit = (countryList = [], countryCode = 'ID') => {
  const defaultValues = { min: 8, max: 12 }

  if (!isArray(countryList) || !countryList.length) {
    return defaultValues
  }
  const filteredCountry = countryList.find(
    (el) => el.country_code === countryCode,
  )

  if (filteredCountry) {
    return {
      min: filteredCountry.phone_number_minimum_value,
      max: filteredCountry.phone_number_maximum_value,
    }
  }
  return defaultValues
}
