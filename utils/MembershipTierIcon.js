export default function membershipTierIcon(level) {
  let tierIcon =
    'https://cdn.ruparupa.io/media/promotion/ruparupa/icon-svg-cohesive/Silver.svg'

  if (level === 'Gold') {
    tierIcon =
      'https://cdn.ruparupa.io/media/promotion/ruparupa/icon-svg-cohesive/Gold.svg'
  } else if (level === 'Platinum') {
    tierIcon =
      'https://cdn.ruparupa.io/media/promotion/ruparupa/icon-svg-cohesive/Platinum.svg'
  }

  return tierIcon
}
