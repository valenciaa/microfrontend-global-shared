const PrivacyEyes = (status = 'eye-off') => {
  return `https://cdn.ruparupa.io/media/promotion/ruparupa/aset-ruparupa-rewards/form/${status}.svg`
}
export default PrivacyEyes
