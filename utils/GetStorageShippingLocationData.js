import Cookies from 'js-cookie'
import localforage from 'localforage'
import { SHIPPING_LOCATION } from './constants'
import getModifiedGeolocation from './GetModifiedGeolocation'

const getShippingLocationFromLocalforage = async () => {
  const shippingLocation = await localforage.getItem(SHIPPING_LOCATION)
  if (shippingLocation) {
    const pcpGeolocation =
      shippingLocation?.latitude + ',' + shippingLocation?.longitude
    const usedGeolocation = shippingLocation?.geolocation
      ? shippingLocation?.geolocation
      : shippingLocation?.latitude
        ? pcpGeolocation
        : undefined
    const geolocation = getModifiedGeolocation(usedGeolocation)
    Cookies.set(SHIPPING_LOCATION, JSON.stringify(geolocation))
  }
  return shippingLocation
}

const getStorageShippingLocationData = async () => {
  let shippingLocationData
  const shippingLocationCookies = Cookies.get(SHIPPING_LOCATION)

  if (shippingLocationCookies) {
    const parsedShippingLocation = JSON.parse(shippingLocationCookies)
    if (parsedShippingLocation && parsedShippingLocation?.address_name) {
      // PDP(geolocation) vs PCP
      const pcpGeolocation =
        parsedShippingLocation?.latitude +
        ',' +
        parsedShippingLocation?.longitude
      const usedGeolocation = parsedShippingLocation?.geolocation
        ? parsedShippingLocation?.geolocation
        : parsedShippingLocation?.latitude
          ? pcpGeolocation
          : undefined
      localforage.setItem(
        SHIPPING_LOCATION,
        JSON.stringify(parsedShippingLocation),
      )
      const geolocation = getModifiedGeolocation(usedGeolocation)
      Cookies.set(SHIPPING_LOCATION, JSON.stringify(geolocation))
      shippingLocationData = shippingLocationCookies
    } else {
      shippingLocationData = getShippingLocationFromLocalforage()
    }
  } else {
    shippingLocationData = getShippingLocationFromLocalforage()
  }
  return shippingLocationData
}

export default getStorageShippingLocationData
