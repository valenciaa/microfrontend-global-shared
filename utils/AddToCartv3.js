import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'
import localforage from 'localforage'

import config from '../../../config'
import GetAffiliateProductLink from './GetAffiliateProductLink'
import {
  handleSendAddToCartCapi,
  handleSendTiktokAddToCartCapi,
} from '../container/cart'

export const addToCartv3 = async (
  name,
  sku,
  packageId,
  normalPrice,
  specialPrice,
  quantity,
  storeCode,
  cartType,
  deliveryMethod,
  type,
  brand,
  l1Category,
  auth,
) => {
  const utm = Cookies.get('utm')
  const itmOrigin = Cookies.get('itm_origin')
  const ip = await localforage.getItem('ip')
  const minicartId = await localforage.getItem('minicart_id')

  const locationHref = `${
    utm !== undefined && !['utm_'].includes(window.location.href)
      ? window.location.href.indexOf('?') !== -1
        ? window.location.href + '&' + utm
        : window.location.href + '?' + utm
      : window.location.href
  }`

  const getItmOrigin = `${
    itmOrigin !== undefined
      ? locationHref + '&itm_origin=' + itmOrigin
      : locationHref
  }`

  // TODO perlu di adjust nama object
  const affiliate = {}

  const affiliateProductLink = GetAffiliateProductLink()
  const affiliateId = Cookies.get('affiliate_id')

  if (affiliateProductLink) {
    affiliate.affiliate_product_link = affiliateProductLink
  }

  if (config.subCompanyCode && !isEmpty(config.subCompanyCode)) {
    if (config.subCompanyCode === 'KWI') {
      if (affiliateId && !isEmpty(affiliateId)) {
        affiliate.affiliate_id = affiliateId
      }
    }
  }

  const metaPrice =
    specialPrice > 0 ? parseInt(specialPrice) : parseInt(normalPrice)

  // Handle send Facebook Conversion API
  handleSendAddToCartCapi({ price: metaPrice, sku: sku }, name, quantity, type)

  // Handle send Tiktok Conversion API
  handleSendTiktokAddToCartCapi(
    { price: metaPrice, sku: sku },
    name,
    quantity,
    type,
  )

  let visitorEmail = ''
  let customerID = ''
  if (auth?.user?.email && auth?.user?.customer_id) {
    visitorEmail = auth?.user?.email
    customerID = auth?.user?.customer_id
  }

  if (window && window.dataLayer) {
    window.dataLayer.push({
      event: 'addToCart',
      productID: sku,
      CriteoProductID: sku,
      email: visitorEmail,
      customerID: customerID,
      ecommerce: {
        currencyCode: 'IDR',
        add: {
          products: [
            {
              name: name,
              id: sku,
              price: metaPrice,
              brand: brand ? brand.replace("'", '') : '',
              category: l1Category ? l1Category.replace("'", '') : '',
              quantity: quantity,
            },
          ],
        },
      },
    })
  }

  return {
    minicart_id: minicartId,
    cart_type: cartType || '',
    remote_ip: ip || '',
    device: config.environment,
    company_code: config.companyCode,
    delivery_method: deliveryMethod || '',
    item: {
      sku: sku,
      package_id: !sku && packageId ? packageId : undefined, // Either package_id or sku will be provided
      qty: quantity || 1,
      store_code: storeCode,
      utm_parameter: getItmOrigin,
    },
    affiliate: !isEmpty(affiliate) ? affiliate : undefined,
  }
}

export const addToCartv3Variant = async (
  activeVariant,
  type,
  quantity,
  storeCode,
  deliveryMethod,
  product,
  auth,
  // addToCartOriginLocation,
) => {
  const sku = product?.sku
  const name = product?.name
  const normalPrice = activeVariant[0]?.price || 0
  const specialPrice = activeVariant[0]?.special_price || 0
  const brand = product?.brand?.name || ''
  const l1Category = product?.categories?.[0]?.name || ''

  return addToCartv3(
    name,
    sku,
    undefined,
    normalPrice,
    specialPrice,
    quantity,
    storeCode,
    '',
    deliveryMethod,
    type,
    brand,
    l1Category,
    auth,
  )
}
