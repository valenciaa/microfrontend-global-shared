export const buttonStatusChecker = (data) => {
  if (data?.is_warranty === true) {
    if (data?.ace_warranty_id === null) {
      // ACE_HARDWARE
      return 'activation'
    }
    if (data?.warranty_status === 'expired') {
      return 'expired'
    }
    return 'active'
  }
  return 'not_available'
}

export const buttonTypes = {
  active: 'primary',
  activation: 'primary-border',
  not_available: 'disabled',
  expired: 'disabled',
}

export const buttonTextTypes = {
  active: 'Cek Detail Garansi',
  activation: 'Aktifkan Garansi',
  not_available: 'Tidak Ada Garansi',
  expired: 'Garansi Habis',
}

// desktop only
export const buttonColorTypes = {
  active: '--green',
  activation: '--yellow',
  not_available: '--red',
  expired: '--red',
}

export const boxTypes = {
  active: '--active',
  activation: '--active',
  not_available: '--disabled',
  expired: '--disabled',
}

export const badgeTextTypes = {
  active: 'Garansi Aktif',
  activation: 'Aktifkan Garansi',
  not_available: 'Tidak Ada Garansi',
  expired: 'Garansi Habis',
}
