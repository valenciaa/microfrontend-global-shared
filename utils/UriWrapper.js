const UriWrapper = (defaultValue, method, value) => {
  let result = defaultValue
  try {
    if (method === 'encode') {
      result = encodeURIComponent(value)
    } else {
      result = decodeURIComponent(value)
    }
  } catch (e) {
    result = defaultValue
  }

  return result
}

export default UriWrapper
