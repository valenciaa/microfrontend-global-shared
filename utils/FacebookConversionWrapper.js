import { globalAPI } from '../services/IndexApi'
import config from '../../../config'
import Cookies from 'js-cookie'
import dayjs from 'dayjs'
import localforage from 'localforage'
import { v4 as uuidv4 } from 'uuid'

const SHA256 = require('crypto-js/sha256')

export const handleSendFacebookConversionRequest = async (
  eventName,
  customData = {},
) => {
  if (config.isLiveSite) {
    let userData = {}
    let ip = null
    let userAgent = null

    // AUTH (USER)
    await localforage.getItem('persist:ruparupa').then((auth) => {
      if (auth) {
        let user = JSON.parse(auth)
        user = user.auth ? JSON.parse(user.auth).user : null

        // LOGGED IN
        if (user) {
          const customerId =
            user && user.customer_id
              ? SHA256(user.customer_id).toString()
              : null
          const email =
            user && user.email ? SHA256(user.email).toString() : null
          const phone =
            user && user.phone ? SHA256(user.phone).toString() : null
          const firstName =
            user && (user?.name || user?.first_name)
              ? SHA256(
                  user?.name?.toLowerCase() || user?.first_name?.toLowerCase(),
                ).toString()
              : null
          const lastName =
            user && user.last_name
              ? SHA256(user.last_name.toLowerCase()).toString()
              : null
          const gender =
            user && user.gender ? SHA256(user.gender).toString() : null
          const dob =
            user && user.birth_date
              ? SHA256(user.birth_date.replace('-', '')).toString()
              : null

          if (customerId) {
            userData = { ...userData, external_id: customerId }
          }
          if (email) {
            userData = { ...userData, em: email }
          }
          if (phone) {
            userData = { ...userData, ph: phone }
          }
          if (firstName) {
            userData = { ...userData, fn: firstName }
          }
          if (lastName) {
            userData = { ...userData, ln: lastName }
          }
          if (gender) {
            userData = { ...userData, ge: gender }
          }
          if (dob) {
            userData = { ...userData, db: dob }
          }
        }
      }
    })

    // IP
    await localforage.getItem('ip').then((value) => {
      if (value) {
        // Take only first IP address
        ip = value.split(',')[0]
        userData = { ...userData, client_ip_address: ip }
      }
    })

    // USER AGENT
    if (window && window.navigator && window.navigator.userAgent) {
      userAgent = window.navigator.userAgent
      userData = { ...userData, client_user_agent: userAgent }
    }

    // LOCATION (CITY)
    await localforage.getItem('location_data').then((location) => {
      if (location) {
        let city = JSON.parse(location)
        city = SHA256(city.city).toString()

        userData = { ...userData, ct: city }
      }
    })

    // COUNTRY
    const country = SHA256('id').toString()
    userData = { ...userData, country }

    const eventTime = dayjs(new Date()).unix()
    const eventId = `${uuidv4()}-${uuidv4()}`

    // FBP
    const fbp = Cookies.get('_fbp')

    if (fbp) {
      userData = { ...userData, fbp }
    }

    // FBC
    const fbc = Cookies.get('_fbc')

    if (fbc) {
      userData = { ...userData, fbc }
    }

    const payload = {
      data: [
        {
          event_id: eventId,
          event_name: eventName,
          event_time: eventTime,
          event_source_url: window.location.href,
          action_source: 'website',
          user_data: userData,
          custom_data: customData,
        },
      ],
    }

    await globalAPI.facebookConversion(payload)
  }
}

export const handleFacebookConversionAddToCartData = (
  pageFrom = '',
  activeVariant,
  product,
  quantity,
) => {
  let customData = {
    currency: 'IDR',
    content_type: 'product',
    num_items: quantity,
  }

  if (pageFrom === '') {
    customData = {
      ...customData,
      value: activeVariant
        ? activeVariant?.prices?.[0]?.special_price !== 0
          ? activeVariant?.prices?.[0]?.special_price
          : activeVariant?.prices?.[0]?.price
        : 0,
      content_name: product && product?.name ? product?.name : '',
      content_ids:
        activeVariant && activeVariant?.sku ? activeVariant?.sku : '',
    }
  }

  return customData
}

export const handleSendFacebookConversionAddToCartRequest = (
  pageFrom,
  activeVariant,
  product,
  quantity,
) => {
  const customData = handleFacebookConversionAddToCartData(
    pageFrom,
    activeVariant,
    product,
    quantity,
  )

  handleSendFacebookConversionRequest('AddToCart', customData)
}

export const handleSetFbclid = (identifier) => {
  const expires = dayjs().add(7, 'day').toDate()
  const value = {
    identifier,
    expires,
  }

  Cookies.set('fbclid-session', JSON.stringify(value), {
    expires,
    domain: config.isLiveSite ? '.ruparupa.com' : '.ruparupastg.my.id',
  })
}
