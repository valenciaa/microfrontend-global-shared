export function CharacterCensor(character, replaceLength) {
  let result = character?.substring(replaceLength)
  let censor = '*'

  for (let i = 0; i < replaceLength; i++) {
    censor += '*'
  }

  result = censor + result
  return result
}
