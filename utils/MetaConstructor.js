import dayjs from 'dayjs'
import 'dayjs/locale/id'
import config from '../../../config'
import { capitalize } from './String'
import { ACE_HARDWARE } from './constants/AceConstants'

const metaConstructor = (pageFrom, data, seoDataLink, urlSeo) => {
  const url = urlSeo
  const params = new URLSearchParams(url?.split('?')?.[1])
  const queryValue = params?.get('query')

  let companyName = 'Ruparupa'
  dayjs.locale('id')

  switch (config.companyCode) {
    case 'AHI':
      companyName = ACE_HARDWARE
      break
    case 'HCI':
      companyName = 'INFORMA'
      break
    case 'TGI':
      companyName = 'Toys Kingdom'
      break
    default:
      companyName = 'Ruparupa'
      break
  }
  const meta = {
    title: config.defTitle,
    keywords: '',
    description: config.defMetaDescription,
  }

  const generateTitleDefault = (name) => {
    switch (companyName) {
      case 'INFORMA':
        return (
          'Jual ' +
          capitalize(name) +
          ' ' +
          companyName +
          ' Harga Baru ' +
          dayjs().format('MMMM') +
          ' ' +
          dayjs().format('YYYY')
        )
      case ACE_HARDWARE:
        return (
          'Jual ' +
          capitalize(name) +
          ' ' +
          companyName +
          ' Harga Baru ' +
          dayjs().format('MMMM') +
          ' ' +
          dayjs().format('YYYY')
        )
      case 'Toys Kingdom':
        return (
          'Jual ' +
          capitalize(name) +
          ' ' +
          companyName.toUpperCase() +
          ' Harga Baru ' +
          dayjs().format('MMMM') +
          ' ' +
          dayjs().format('YYYY')
        )
      default:
        return (
          'Jual ' +
          capitalize(name) +
          ' ' +
          ' Harga Baru ' +
          dayjs().format('MMMM') +
          ' ' +
          dayjs().format('YYYY')
        )
    }
  }

  const generateDescriptionDefault = (name) => {
    switch (companyName) {
      case 'INFORMA':
        return (
          'Beli ' +
          capitalize(name) +
          ' berkualitas ragam model dengan Gratis Ongkir*✅ Langsung Kirim*✅ Cicilan 0%⚡Belanja produk ' +
          companyName +
          ' hanya di ruparupa'
        )
      case ACE_HARDWARE:
        return (
          'Beli ' +
          capitalize(name) +
          ' berkualitas ragam model dengan Gratis Ongkir*✅ Langsung Kirim*✅ Cicilan 0%⚡Belanja produk ' +
          companyName +
          ' hanya di ruparupa'
        )
      case 'Toys Kingdom':
        return (
          'Beli ' +
          capitalize(name) +
          ' berkualitas ragam model dengan Gratis Ongkir*✅ Langsung Kirim*✅ Cicilan 0%⚡Belanja produk ' +
          companyName +
          ' hanya di ruparupa'
        )
      default:
        return (
          'Beli ' +
          capitalize(name) +
          ` berkualitas ragam model dengan Gratis Ongkir*✅ Langsung Kirim*✅ Cicilan 0%⚡Belanja produk ${ACE_HARDWARE}, INFORMA, Toys Kingdom hanya di ruparupa`
        )
    }
  }

  const getTitleValue = () => {
    const filter =
      seoDataLink &&
      seoDataLink.find((e) => e?.attributes?.[0]?.name === 'title')

    if (filter && filter?.attributes?.[1]?.content) {
      return filter.attributes[1].content
    }

    return generateTitleDefault(data?.name)
  }

  const getDescriptionValue = () => {
    const filter =
      seoDataLink &&
      seoDataLink.find((e) => e?.attributes?.[0]?.name === 'description')

    if (filter && filter?.attributes?.[1]?.content) {
      return filter.attributes[1].content
    }

    return generateDescriptionDefault(data?.name)
  }

  switch (pageFrom) {
    case 'catalog':
      if (data) {
        meta.title = data?.meta_title || generateTitleDefault(data?.name)
        meta.description =
          data?.meta_description || generateDescriptionDefault(data?.name)

        const isEmptyMetaInspirations =
          (!data?.meta_title || !data?.meta_description) &&
          url?.indexOf('inspiration') > -1

        if (isEmptyMetaInspirations && url?.indexOf('/cc/') > -1) {
          meta.title = `Inspirasi Produk ${data?.name} Terbaru`
          meta.description = `Dapatkan pilihan produk inspirasi ${data?.name} terbaik dan terbaru dari ${companyName}`
          return meta
        }

        if (isEmptyMetaInspirations && url?.indexOf('/c/') > -1) {
          meta.title = `Inspirasi ${data?.name} Terbaru`
          meta.description = `Dapatkan koleksi pilihan produk inspirasi ${data?.name} terbaik dan terbaru dari ${companyName}`
          return meta
        }

        // checked for inspiration and ideas in HCI
        if (
          data?.url_key?.indexOf('inspirations-and-ideas') > -1 &&
          config.companyCode === 'HCI'
        ) {
          meta.title = `Koleksi Produk Inspirasi ${data?.name} Terbaik: Informa`
          meta.description = `Dapatkan koleksi pilihan produk inspirasi ${data?.name} terbaik untuk keluarga Anda setiap harinya hanya di Informa`
        }

        // check for brands
        if (data?.url_key?.indexOf('brands') > -1) {
          meta.title = `Official Store Online ${data?.name} ${
            config.companyCode === 'AHI'
              ? `| ${companyName}`
              : `> Original Bergaransi | ${companyName}`
          } `
          meta.description = `Official Store ${data?.name} Online Harga Terbaru. Dapatkan promo produk original berkualitas serta bergaransi resmi banyak pilihan & 100% Asli di ${companyName}`
        }
      }
      break
    case 'brand':
      if (data) {
        meta.title = data?.meta_title
        meta.description = data?.meta_description
      } else {
        meta.title = `Official Store Online ${data?.name} ${
          config.companyCode === 'AHI'
            ? `| ${companyName}`
            : `> Original Bergaransi | ${companyName}`
        } `
        meta.description = `Official Store ${data?.name} Online Harga Terbaru. Dapatkan promo produk original berkualitas serta bergaransi resmi banyak pilihan & 100% Asli di ${companyName}`
      }
      break
    case 'static-interior-design':
      if (data) {
        meta.title = data?.meta_title
        meta.description = data?.meta_description
      } else {
        meta.title = 'Jasa Desain Interior Rumah dan Furnitur Terpercaya'
        meta.description =
          'Jasa Desain Interior Rumah dan Custom Furnitur, Kitchen Set, Ruang Tamu, Kamar Tidur dengan Harga Transparan | ☑️Desain Menarik ☑️Transaksi Aman ☑️Garansi 3 Tahun'
      }
      break
    case 'custom-furniture':
      if (data) {
        meta.title = data?.meta_title
        meta.description = data?.meta_description
      } else {
        meta.title = 'Informa Custom Furniture › Jaminan Kepuasan'
        meta.description =
          'Informa Custom Furniture Berkualitas › Jaminan 100% Kepuasan⭐ Gratis Konsultasi Desain Interior ✅ Gratis Pengiriman & Instalasi ⚡'
      }
      break
    case 'jual':
      if (data?.name) {
        meta.title = getTitleValue()
        meta.description = getDescriptionValue()
      } else {
        meta.title = generateTitleDefault(queryValue)
        meta.description = generateDescriptionDefault(queryValue)
      }
      break

    case 'store': {
      const storeNameUrl =
        data?.name
          ?.replace(/---/g, '_')
          .replace(/-/g, ' ')
          .replace(/_/g, ' - ') || ''
      const storeNameUrlSplit = storeNameUrl?.split(' - ') || []
      if (storeNameUrlSplit?.length > 1) {
        const store = {
          store_location: capitalize(storeNameUrlSplit[0]),
          store_name: capitalize(storeNameUrlSplit[1]),
        }
        meta.title = `Toko ${store?.store_name} ${store?.store_location}`
        meta.description = `Temukan katalog promo dan diskon terbaru produk rumah tangga dan gaya hidup terlengkap dari ${store.store_name} di Kota ${store?.store_location}`
      }
      break
    }
    case 'tahu':
    case 'product-detail':
      meta.title = data?.meta_title
      meta.description = data?.meta_description
      break

    case 'store-location':
      meta.title = data?.meta_title
      meta.description = data?.meta_description
      break

    default:
      break
  }

  return meta
}

export { metaConstructor }
