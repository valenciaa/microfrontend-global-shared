export default interface TEditProfile {
  // User Profile
  'hobi': string[]
  'member_level': String
  // Child Profile
  'child_name_1': String
  'child_gender_1': String
  'child_birthday_1': Date
  'child_name_2': String
  'child_gender_2': String
  'child_birthday_2': Date
  'child_name_3': String
  'child_gender_3': String
  'child_birthday_3': Date
  'child_name_4': String
  'child_gender_4': String
  'child_birthday_4': Date
  // Pet Profile
  'pet_name_1 ': String
  'pet_birthday_1 ': Date
  'pet_type_1 ': String
  'pet_name_2': String
  'pet_birthday_2': Date
  'pet_type_2': String
  'pet_name_3': String
  'pet_birthday_3': Date
  'pet_type_3': String
  'pet_name_4': String
  'pet_birthday_4': Date
  'pet_type_4': String
  // Card Profile - get BU from wapi
  'card_id_selma': String[]
  'card_id_toys': String[]
  'card_id_ace': String[]
  'card_id_informa': String[]
}
