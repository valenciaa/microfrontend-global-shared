const ResizeImage = (url, height, width) => {
  return (
    url?.includes('cdn.ruparupa.io') &&
    url?.replace('.io', '.io/fit-in/' + width + 'x' + height)
  )
}

export default ResizeImage
