import GetParam from './GetParam'
import TryParseJSONObject from './TryParseJSONObject'

export const getShopbackData = () => {
  // klo localStorage isinya JSON dia kembalikan parsed JSON
  // klo localStorage isinya string biasa dia kembalikan string biasa
  // klo localStorage tidak ada dia kembalikan null

  let shopbackData = localStorage.getItem('shopbackTransactionId') // eslint-disable-line
  const shopbackDataParsed = TryParseJSONObject(shopbackData)

  return shopbackDataParsed || shopbackData
}

export const processShopbackTransactionId = () => {
  let shopbackData = getShopbackData()

  if (shopbackData !== null && typeof shopbackData !== 'object') {
    localStorage.setItem(
      'shopbackTransactionId',
      JSON.stringify({
        // eslint-disable-line
        id: shopbackData,
        timestamp: +new Date(),
      }),
    )
    shopbackData = getShopbackData()
  }

  const source = GetParam('utm_source')
  const transactionId = GetParam('transaction_id')
  if (source && transactionId && source.includes('shopback.co.id')) {
    if (
      shopbackData === null ||
      (shopbackData.id && shopbackData.id !== transactionId)
    ) {
      localStorage.setItem(
        'shopbackTransactionId',
        JSON.stringify({
          // eslint-disable-line
          id: transactionId,
          timestamp: +new Date(),
        }),
      )
      shopbackData = getShopbackData()
    }
  }

  if (shopbackData !== null && shopbackData.timestamp) {
    if (+new Date() > shopbackData.timestamp + 30 * 24 * 60 * 60 * 1000)
      localStorage.removeItem('shopbackTransactionId') // eslint-disable-line
  }
}
