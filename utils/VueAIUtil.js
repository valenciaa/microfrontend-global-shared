import { GetUniqueId } from '.'
import config from '../../../config'
import { ACE_HARDWARE } from './constants/AceConstants'

const setUtmMetadata = () => {
  const url = new URL(window?.location?.href)
  const queryString = url?.search
  const queryParams = new URLSearchParams(queryString)

  const dir = queryParams.get('dir')

  // Check if 'dir' contains another query string (indicating incorrect formatting)
  if (dir && dir?.includes('?')) {
    const [newDir, remainingParams] = dir?.split('?')
    queryParams.set('dir', newDir)

    const remainingParamsObj = new URLSearchParams(remainingParams)
    remainingParamsObj?.forEach((value, key) => {
      queryParams?.set(key, value)
    })
  }

  const utm_source = queryParams.get('utm_source')
  const utm_campaign = queryParams.get('utm_campaign')

  return {
    utm_source,
    utm_campaign,
  }
}

export function setMetaData(
  category = null,
  posOfReco = null,
  orderBy = null,
  filters = null,
  pageType = null,
  productList = null,
  moduleId = null,
  moduleName = null,
  moduleBehaviour = null,
  templateType = null,
  recosServed = null,
  strategyId = null,
  referrerProduct = null,
) {
  let metaData = {
    currency: 'IDR',
  }
  if (category) {
    metaData = {
      ...metaData,
      category,
    }
  }
  if (posOfReco) {
    metaData = {
      ...metaData,
      pos_of_reco: posOfReco,
    }
  }
  if (orderBy) {
    metaData = {
      ...metaData,
      order_by: orderBy,
    }
  }
  if (strategyId) {
    metaData = {
      ...metaData,
      strategy_id: strategyId,
    }
  }
  if (filters) {
    metaData = {
      ...metaData,
      filters,
    }
  }

  if (pageType) {
    metaData = {
      ...metaData,
      page_type: pageType,
    }
  }

  const allowProductListPages = [
    'home',
    'pdp',
    'popupcart',
    'cat',
    'search',
    'empty-search',
    'rewards',
    'page-not-found',
    'pcp-brand',
    'cart',
    'homepage',
    'wishlist',
    'tahu',
    'microsite',
  ]
  const allowProductList = [...allowProductListPages].includes(
    pageType?.toLowerCase(),
  )

  if (productList && (allowProductList || category)) {
    // product_list for pdp page and pcp only
    metaData = {
      ...metaData,
      product_list: productList,
    }

    if (referrerProduct) {
      delete referrerProduct?.[0]?.quantity

      metaData = {
        ...metaData,
        referrer_product: referrerProduct?.[0],
      }
    }
  }

  if (moduleId) {
    metaData = {
      ...metaData,
      module_id: moduleId,
    }
  }
  if (moduleName) {
    metaData = {
      ...metaData,
      module_name: moduleName,
    }
  }
  if (moduleBehaviour) {
    metaData = {
      ...metaData,
      module_behaviour: moduleBehaviour,
    }
  }
  if (templateType) {
    metaData = {
      ...metaData,
      template_type: templateType,
    }
  }
  if (recosServed) {
    metaData = {
      ...metaData,
      recos_served: recosServed,
    }
  }
  return metaData
}

export function setDefaultData(
  url = null,
  referrer = null,
  name = null,
  action = null,
  nonInteraction = null,
  uuid = null,
) {
  let defaultData = {
    platform: config.environment,
  }
  if (url) {
    defaultData = {
      ...defaultData,
      url,
    }
  }
  if (referrer) {
    defaultData = {
      ...defaultData,
      referrer,
    }
  }
  if (uuid) {
    defaultData = {
      ...defaultData,
      mad_uuid: uuid,
    }
  }
  if (name) {
    defaultData = {
      ...defaultData,
      name,
    }
  }
  if (nonInteraction) {
    defaultData = {
      ...defaultData,
      non_interaction: nonInteraction,
    }
  }
  if (action) {
    defaultData = {
      ...defaultData,
      action,
    }
  }

  return defaultData
}

export async function fetchTracking(
  eventName,
  dataVueReco,
  sendTracking,
  interaction = 'true',
  action = 'pageView',
  pageType = 'home',
  productDetailVue,
  templateType = 'simple-carousel',
) {
  const moduleName = dataVueReco?.module_name
  const moduleId = dataVueReco?.module_id
  let recoServe = dataVueReco?.data?.length
  const productData = productDetailVue
  const strategyId = productData?.strategy_id ?? productData?.[0]?.strategy_id

  if (['rightSwipe', 'leftSwipe'].includes(eventName)) {
    recoServe = null
  }

  const ugid = await GetUniqueId()
  const url = window?.location?.href
  const defaultData = setDefaultData(
    url,
    url,
    eventName,
    action,
    interaction,
    ugid,
  )
  const metaData = setMetaData(
    null,
    null,
    null,
    null,
    pageType,
    productData,
    moduleId,
    moduleName,
    'inline',
    templateType,
    recoServe,
    strategyId,
  )
  if (dataVueReco?.correlationID) {
    defaultData.correlation_id = dataVueReco?.correlationID
    if (
      [
        'recommendedProductView',
        'addToCart',
        'removeFromWishlist',
        'addToWishlist',
      ].includes(eventName)
    ) {
      if (dataVueReco?.index !== '') {
        metaData.pos_of_reco = dataVueReco?.index || 0
      }
    }
  }
  sendTracking(bodyTrack(defaultData, {}, metaData))
}

export function bodyTrack(defaultData, journeyMeta = {}, metaData = {}) {
  //* * regarding external vue team req at 4 AUG SIT, product_list should't be send when the data name pageView and all swipe type */
  if (
    defaultData?.name === 'recommendationView' ||
    defaultData?.name?.toLowerCase()?.includes('swipe')
  ) {
    delete metaData.product_list
  }

  if (defaultData.name === 'pageView') {
    metaData = {
      ...metaData,
      ...setUtmMetadata(),
    }
  }

  const data = {
    data: {
      ...defaultData,
      metadata: {
        ...metaData,
      },
      _journey_metadata: {
        ...journeyMeta,
      },
    },
  }

  return data
}

export async function bodyFeeds(
  pageFrom = 'home',
  moduleName = '',
  productId = '',
  categoryData,
  keyword,
  brand = '',
) {
  const ugid = await GetUniqueId()

  const body = {
    data: {
      page_name: pageFrom,
      platform: config.environment,
      mad_uuid: ugid,
      product_id: productId,
      fields: [
        'brand',
        'price',
        'Link',
        'image',
        'title',
        'msrp',
        'category',
        'ontology',
        'discount_percentage',
      ],
    },
  }

  if (['pcp-category', 'pcp-search', 'empty-search'].includes(pageFrom)) {
    if (categoryData?.l_category) {
      body.data.l_category = categoryData?.l_category
    }
    if (categoryData?.l_level) {
      body.data.l_level = categoryData?.l_level - 1 + ''
    }
  }

  // spesific module_name
  if (moduleName !== '') {
    delete body.data.page_name
    body.data.module_name = moduleName
  }

  if (keyword !== '') {
    body.data.keyword = keyword
  }
  if (brand !== '') {
    body.data.brand = brand
  }

  return body
}

export function urlToObjectConverter(urlString) {
  if (urlString && !urlString?.includes('undefined')) {
    const url = new URL(urlString)
    const urlObject = {}

    url.searchParams.forEach((value, key) => {
      urlObject[key] = value
    })

    if (urlObject) {
      return urlObject
    }
  }
}

export function vueUrlVerification(url) {
  /** Example User Insert Raw Link Below that contain filter fields
   * {baseUrl}/cc/hot-offers/online-exclusive.html?board+penawaran+saat+ini+mobile=online+exclusive&itm_source=homepage-penawaran+saat+ini&itm_campaign=spesial-online&itm_device=mobile&total=3479&categoryId=10572&isRuleBased=true&size=24&sort=matching&from=24
   */

  if (!url) {
    return false
  }

  const urlParams = new URLSearchParams(url.split('?')[1])

  /** urlParams Value
   * { 'board penawaran saat ini mobile' => 'online exclusive', 'itm_source' => 'homepage-penawaran saat ini', 'itm_campaign' => 'spesial-online', 'itm_device' => 'mobile', 'total' => '3479', 'categoryId' => '10572', 'isRuleBased' => 'true', 'size' => '24', 'sort' => 'matching', 'from' => '24' }
   */

  const dataAtt = {
    sort: urlParams.get('sort') || '',
    locations: urlParams.get('locations') || '',
    province_id: urlParams.get('province_id') || '',
    city_id: urlParams.get('city_id') || '',
    store: urlParams.get('store') || '',
    express_courier: urlParams.get('express_courier') || '',
    minprice: urlParams.get('minprice') || '',
    maxprice: urlParams.get('maxprice') || '',
    brands: urlParams.get('brands') || '',
    labels: urlParams.get('labels') || '',
    color: urlParams.get('color') || '',
    children: urlParams.get('children') || '',
  }

  /** If only one field is filled, it will return a false value, indicating that this URL contains a filter. */
  return Object.values(dataAtt).some((value) => value !== '')
}

export function getCompanyCode(companyCode) {
  let company
  switch (companyCode) {
    case 'AHI':
      company =  ACE_HARDWARE //ACE_HARDWARE
      break
    case 'HCI':
      company = 'informa'
      break
    case 'TGI':
      company = 'toyskingdomonline'
      break
    default:
      company = 'rr'
      break
  }
  return company
}

// blox_uuid & correlation ID generator - created by VUE vendor
export function getCorrelationId() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = (Math.random() * 16) | 0
    const v = c === 'x' ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })
}

// Below is instant VUE UTILS, just call and it can be used for all pages
const allowProductListPage = ['wishlist']

const vueAttributeSetup = async (data = {}) => {
  const { name, action, correlationID } = data

  const url = window.location.href
  let urlReferrer =
    window?.location?.referrer ||
    window?.navigation?.activation?.from?.url ||
    ''

  const uuid = await GetUniqueId()

  const defaultData = {
    non_interaction: 'false',
    platform: config?.environment,
    ...(url && { url }),
    ...(urlReferrer && { referrer: urlReferrer }),
    ...(uuid && { mad_uuid: uuid }),
    ...(name && { name }),
    ...(action && { action }),
    ...(correlationID && { correlation_id: correlationID }),
  }

  return defaultData
}

const vueMetaDataSetup = ({
  pageType = null,
  recoData = null,
  module = null,
  strategyId = null,
  referrerProduct = null,
  category = null,
  pos_of_reco = null,
}) => {
  const {
    module_name = '',
    module_id = '',
    data: module_data = '',
  } = module || {}

  const allowProductList = allowProductListPage?.includes(
    (pageType || '')?.toLowerCase(),
  )

  const metaData = {
    currency: 'IDR',
    module_behaviour: 'inline',
    template_type: 'simple-carousel',
    ...(category && { category }),
    ...(pos_of_reco && { pos_of_reco }),
    ...(strategyId && { strategy_id: strategyId }),
    ...(pageType && { page_type: pageType }),
    ...(module_id && { module_id }),
    ...(module_name && { module_name }),
    ...(module_data && { recos_served: module_data?.length }),
  }

  if (recoData && allowProductList) {
    metaData.product_list = recoData
    if (referrerProduct) {
      delete referrerProduct?.[0]?.quantity
      metaData.referrer_product = referrerProduct?.[0]
    }
  }

  return metaData
}

/** [SWIPE L & R EVENT]
 * Function to handle category swiping in a Vue component.
 * @typedef {Object} SwipeTypes
 * @property {Object} module
 * @property {string} direction
 * @property {string} pageFrom
 * @property {string} [correlationID]
 * @property {Function} sendTracking
 *
 * @param {SwipeTypes} props
 */

export function vueSwiper(props) {
  const {
    module,
    correlationID,
    sendTracking,
    pageFrom,
    previousIndex,
    realIndex,
  } = props

  let arrowName = 'leftSwipe'

  if (previousIndex < realIndex) {
    arrowName = 'rightSwipe'
  }
  if (correlationID) {
    module.correlationID = correlationID
  }
  fetchTracking(arrowName, module, sendTracking, 'false', 'click', pageFrom)
}

/** [CLICK CARD RECOMENDATION]
 * Function to handle category swiping in a Vue component.
 *
 * @typedef {Object} ClickTypes
 * @property {Object} module
 * @property {string} [correlationID]
 * @property {string} pageFrom
 * @property {object} recoData
 * @property {Function} sendTracking
 * @property {number} clickPosition
 *
 * @param {ClickTypes} props
 */

export async function vueClick(props) {
  const {
    module,
    correlationID,
    sendTracking,
    pageFrom,
    recoData,
    clickPosition,
  } = props

  const isCategory = recoData?.strategy_name
    ?.toLowerCase()
    ?.includes('categories')
  const clickedProduct = {
    product_id: '',
  }

  if (isCategory) {
    clickedProduct.product_id = recoData?.l3_category
  }

  const eventName = 'recommendedProductView'

  const metaData = vueMetaDataSetup({
    pageType: pageFrom,
    recoData: [clickedProduct],
    module,
    strategyId: recoData?.strategy_id,
    category: isCategory && recoData?.l3_category,
    pos_of_reco: clickPosition,
  })

  const attribute = await vueAttributeSetup({
    name: eventName,
    action: 'click',
    correlationID,
    pageFrom,
  })

  sendTracking({
    data: {
      ...attribute,
      metadata: {
        ...metaData,
      },
    },
  })
}

/** [REMOVE FROM WISHLIST]
 * handle click heart wishlist button
 *
 * @typedef {Object} WishlistTypes
 * @property {Object} recoData
 * @property {string} [correlationID]
 * @property {string} pageFrom
 * @property {string} type // category || product
 * @property {Function} sendTracking
 * @property {number} clickPosition
 *
 * @param {WishlistTypes} props
 */

export async function vueWishlist(props) {
  const { correlationID, sendTracking, pageFrom, recoData, type } = props

  let eventName = 'addToWishlist'
  if (type === 'remove') {
    eventName = 'removeFromWishlist'
  }

  const clickedProduct = {
    product_id: recoData?.sku?.[0],
    price: recoData?.price + '', // price send in string
  }

  const metaData = vueMetaDataSetup({
    pageType: pageFrom,
    recoData: [clickedProduct],
  })

  const attribute = await vueAttributeSetup({
    name: eventName,
    action: 'click',
    correlationID,
  })

  sendTracking({
    data: {
      ...attribute,
      metadata: {
        ...metaData,
      },
    },
  })
}

/** [ADD TO CART]
 * handle add to cart button
 *
 * @typedef {Object} WishlistTypes
 * @property {Object} recoData
 * @property {string} [correlationID]
 * @property {string} pageFrom
 * @property {Function} sendTracking
 * @property {Object} [module]
 * @property {number} [recoIndex]
 *
 * @param {WishlistTypes} props
 */

export async function vueAddToCart(props) {
  const {
    correlationID,
    sendTracking,
    recoData,
    pageFrom,
    module = null, // if module then its widget, else non-widget
    recoIndex = null, //widget
  } = props

  const eventName = 'addToCart'

  const clickedProduct = {
    product_id: recoData?.sku,
    [module ? 'product_price' : 'price']: recoData?.price + '', // price send in string
    quantity: 1, // will always set to 1
    ...(module && { strategy_id: module?.strategy_id }),
  }

  const metaData = vueMetaDataSetup({
    pageType: pageFrom,
    recoData: [clickedProduct],
    ...(module && {
      module,
      strategyId: module?.strategy_id,
      pos_of_reco: recoIndex,
    }),
  })

  const attribute = await vueAttributeSetup({
    name: eventName,
    action: 'click',
    correlationID,
  })

  sendTracking({
    data: {
      ...attribute,
      metadata: { ...metaData },
    },
  })
}

/** [ VUE PAGE VIEW]
 * Function that triggered when user open page.
 *
 * @typedef {Object} PageViewTypes
 * @property {string} [correlationID]
 * @property {string} pageFrom
 *
 * @param {PageViewTypes} props
 */

export async function vuePageView(props) {
  const { correlationID, sendTracking, pageFrom } = props
  const eventName = 'pageView'

  const metaData = vueMetaDataSetup({
    pageType: pageFrom,
  })

  const attribute = await vueAttributeSetup({
    name: eventName,
    action: eventName,
    correlationID,
    pageFrom,
  })

  sendTracking({
    data: {
      ...attribute,
      metadata: {
        ...metaData,
        ...setUtmMetadata(),
      },
    },
  })
}

/** [ VUE PAGE SWIPE]
 * Function that triggered when user open page.
 *
 * @typedef {Object} SwipeRecoTypes
 * @property {string} [correlationID]
 * @property {string} pageFrom
 *
 * @param {SwipeRecoTypes} props
 */

export async function vueRecoSwiper(props) {
  const { correlationID, sendTracking, pageFrom, type, module } = props
  const eventName = type

  const metaData = vueMetaDataSetup({
    pageType: pageFrom,
    ...(module && {
      module,
    }),
  })

  const attribute = await vueAttributeSetup({
    name: eventName,
    action: 'click',
    correlationID,
    pageFrom,
  })

  sendTracking({
    data: {
      ...attribute,
      metadata: {
        ...metaData,
        ...setUtmMetadata(),
      },
    },
  })
}

/** [ VUE Recommendation VIEW]
 * Function that triggered when user see recomendation.
 *
 * @typedef {Object} RecoViewTypes
 * @property {string} [correlationID]
 * @property {string} pageFrom
 * @property {Function} sendTracking
 * @property {Object} [module]
 *
 * @param {RecoViewTypes} props
 */

export async function vueRecommendationView(props) {
  const {
    correlationID,
    sendTracking,
    pageFrom,
    module,
    slot_id: slotId = '',
  } = props
  const eventName = 'recommendationView'

  const metaData = vueMetaDataSetup({
    pageType: pageFrom,
    ...(module && {
      module,
      strategyId: module?.strategy_id,
    }),
  })

  const attribute = await vueAttributeSetup({
    name: eventName,
    action: 'pageView',
    correlationID,
  })

  sendTracking({
    data: {
      ...attribute,
      metadata: {
        ...metaData,
      },
      ...(slotId && { slot_id: slotId }),
    },
  })
}

/** [ VUE Recommendation VIEW]
 * Function that triggered when user see recomendation.
 *
 * @typedef {Object} RecoViewTypes
 * @property {string} [correlationID]
 * @property {string} pageFrom
 * @property {Function} sendTracking
 * @property {Object} [module]
 *
 * @param {RecoViewTypes} props
 */

export async function recoBodyFeed(props) {
  const {
    from,
    productId = '',
    categoryDetail,
    correlationID = '',
    keyword = '',
    brand = '',
    additionalValue = null,
    ruleBase = '', // optional, only used on /c /cc
  } = props

  const ugid = await GetUniqueId()
  const body = {
    data: {
      page_name: from,
      platform: config?.environment,
      mad_uuid: ugid,
      product_id: productId || '',
      correlation_id: correlationID,
      keyword,
      ...(from === 'microsite' &&
        additionalValue && {
          vue_attributes: additionalValue?.vue_attributes,
        }),
      ...(ruleBase && { is_rule_based: ruleBase }),
    },
  }

  const { url_path: urlPath, level } = categoryDetail || {}

  if (
    ['pcp-category', 'empty-search', 'search', 'brand'].includes(from) &&
    categoryDetail
  ) {
    body.data = {
      ...body.data,
      ...(urlPath && { l_category: urlPath }),
      ...(level && { l_level: String(level - 1) }),
    }
  }
  if (from === 'pcp-brand' && brand) {
    body.data = {
      ...body.data,
      ...(brand && { brand }),
    }
  }

  return body
}

// Section Vue Title
export const sectionTitle = (module_name) => {
  if (!module_name) {
    return
  }
  const lowerCaseModuleName = module_name?.toLowerCase()
  let title = 'Lengkapi Belanjaanmu'

  const titles = {
    viewed: 'Terakhir Dilihat',
    best: 'Produk Terlaris Saat Ini',
    recommend: 'Produk yang Mungkin Kamu Suka',
    search: 'Produk Pilihan Untukmu',
  }

  for (const key in titles) {
    if (lowerCaseModuleName?.includes(key)) {
      return titles?.[key]
    }
  }

  return title
}

// handle vue label display
export const vueRecoLabel = (labelProduct) => {
  if (labelProduct) {
    const labelProductBU = (label) => {
      /** Change Vue Original Url
       * "Best Offer|||<li>Promo berlaku hingga 30 September 2023 </li>"
       *into
       *"Best Offer",
       * if the string "|||" is less than three nor didn't content any of "|||" will return empty string
       */

      // Split the string using "|||" as the separator and get the first part
      const parts = label?.split('|||')
      // Trim to remove any leading/trailing whitespace
      const extractedText = parts?.[0]?.trim()

      return extractedText
    }

    let labelManipulate = labelProduct?.includes('|||')
      ? labelProductBU(labelProduct)
      : labelProduct

    if (labelProduct === 'NULL') {
      labelManipulate = ''
    }

    return labelManipulate
  }
  return ''
}

export const vueTitleSection = (moduleName) => {
  const convert = (string) => moduleName?.toLowerCase()?.includes(string)
  if (convert('best')) {
    return 'Produk Terlaris'
  }
  if (convert('search')) {
    return 'Pencarian Yang Sesuai Untuk Anda'
  }
  if (convert('trend')) {
    return 'Produk Populer'
  }
  if (convert('recent')) {
    return 'Terakhir Dilihat'
  }
  if (convert('compliment')) {
    return 'Lengkapi Belanjaanmu'
  }
  if (convert('recommend')) {
    return 'Produk Yang Mungkin Kamu Suka'
  }
  if (convert('categ')) {
    return 'Produk Pilihan Untuk Anda'
  }
  return 'Produk yang Mungkin Kamu Suka'
}

const vueFilterMetaDataSetup = ({ module = {}, pageType = null, ugid }) => {
  const metaData = {
    filter: [],
    ...module,
    page_type: pageType,
    platform: config?.environment,
    mad_uuid: ugid,
    currency: 'IDR',
  }
  return metaData
}

/** [ VUE PAGE FILTER] */
export async function vueRecoFilter(props) {
  const { correlationID, sendTracking, pageFrom, categoryDetail, module } =
    props

  if (pageFrom === 'jual') {
    // Disable Tracker VUE.Ai for /jual
    return
  }
  const ugid = await GetUniqueId()

  const metaData = vueFilterMetaDataSetup({
    pageType: pageFrom,
    module,
    ugid,
  })

  const attribute = await vueAttributeSetup({
    name: 'filter',
    correlationID,
    pageFrom,
  })

  delete attribute.non_interaction

  sendTracking({
    data: {
      ...attribute,
      metadata: {
        ...metaData,
        ...setUtmMetadata(),
        ...categoryDetail,
      },
    },
  })
}
