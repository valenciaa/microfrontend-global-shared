import config from '../../../config'
export const userAgentChecker = () => {
  const userAgent = window?.navigator?.userAgent
  const listDevices = [
    'RuparupaApps',
    'aa2-ace-android', //ACE_HARDWARE
    'ace-android', //ACE_HARDWARE
    'aa2-ace-ios', //ACE_HARDWARE
    'ace-ios', //ACE_HARDWARE
    'aa2-informa-android',
    'informa-android',
    'aa2-informa-ios',
    'informa-ios',
    'aa2-informa-huawei',
    'informa-huawei',
    'aa2-selma-android',
    'selma-android',
    'aa2-selma-ios',
    'selma-ios',
    'aa2-android',
    'android',
    'aa2-ios',
    'ios',
  ]
  return listDevices.includes(userAgent) ? userAgent : config.environment
}
