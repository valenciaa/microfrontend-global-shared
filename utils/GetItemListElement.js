import config from '../../../config'
import { LOGO_RUPARUPA_DESKTOP } from './constants'

const OFFICIAL_STORE = [
  {
    name: 'ace ', //ACE_HARDWARE
    logo: 'icon/store-logo/AZKO.webp', // ACE_IMAGE
    company_code: 'AHI',
  },
  {
    name: 'acepresso',
    logo: 'icon/store-logo/Acepresso.webp',
    company_code: 'ODI',
  },
  { name: 'ataru', logo: 'icon/store-logo/Ataru.webp', company_code: 'ODI' },
  {
    name: 'bikeco fit',
    logo: 'icon/store-logo/BikeCoFit.webp',
    company_code: 'ODI',
  },
  {
    name: 'chatime',
    logo: 'icon/store-logo/Chatime.webp',
    company_code: 'ODI',
  },
  { name: 'dr kong', logo: 'icon/store-logo/DrKong.webp', company_code: 'ODI' },
  {
    name: 'eyesoul',
    logo: 'icon/store-logo/Eyesoul.webp',
    company_code: 'ODI',
  },
  {
    name: 'informa',
    logo: 'icon/store-logo/Informa.webp',
    company_code: 'HCI',
  },
  {
    name: 'pendopo',
    logo: 'icon/store-logo/Pendopo.webp',
    company_code: 'ODI',
  },
  {
    name: 'pet kingdom',
    logo: 'icon/store-logo/PetKingdom.webp',
    company_code: 'ODI',
  },
  { name: 'selma', logo: 'icon/store-logo/Selma.webp', company_code: 'ODI' },
  { name: 'susen', logo: 'icon/store-logo/Susen.webp', company_code: 'ODI' },
  {
    name: 'toys kingdom',
    logo: 'icon/store-logo/ToysKingdom.webp',
    company_code: 'TGI',
  },
  {
    name: 'travelink',
    logo: 'icon/store-logo/Travelink.webp',
    company_code: 'ODI',
  },
]

export const GetItemListElement = (product) => {
  return product?.map((item, index) => {
    const url = `https://ruparupa.com/${
      config.companyCode === 'ODI' ? '' : config.companyName + '/'
    }${item?.url_key}`

    let imageUrl = item?.variants?.[0]?.images?.[0]?.image_url
      ? config?.imageURL +
        item?.variants?.[0]?.images?.[0]?.image_url?.replace('/', '')
      : item?.images?.[0]?.image_url
        ? config?.imageURL + item?.images?.[0]?.image_url?.replace('/', '')
        : ''

    if (item?.variants?.[0]?.images?.[0]?.full_image_url) {
      imageUrl = item?.variants?.[0]?.images?.[0]?.full_image_url
    }
    return JSON.stringify({
      '@type': 'ListItem',
      'name': item?.name,
      'url': url,
      'position': index + 1,
      'image': imageUrl,
    })
  })
}

export const GetItemListElementCategoryList = (category) => {
  const explode = category?.url_path?.split('/') || []
  const itemListElement = []
  let elementTemp = ''

  explode.forEach((element, index) => {
    itemListElement.push({
      '@type': 'ListItem',
      'position': index + 1,
      'name': element.replace(/-/g, ' '),
      'item': config.baseURL + elementTemp + element + '.html',
    })
    elementTemp = elementTemp + element + '/'
  })

  return JSON.stringify(itemListElement)
}

export const GetItemListReviewCountCatalog = (category, productPayload) => {
  let schema = null
  let name = ''
  let reviewCount = 0
  let price = 0

  if (category.name && Array.isArray(productPayload)) {
    name = category.name

    productPayload.forEach((product) => {
      if (product.review && product.review.ODI && product.review.ODI.total) {
        reviewCount += product.review.ODI.total
      }
    })

    if (productPayload?.[0]?.variants?.[0]?.prices) {
      const productPrice = productPayload?.[0]?.variants?.[0]?.prices?.[0]

      if (productPrice && productPrice.special_price) {
        price = productPrice.special_price
      } else if (productPrice && productPrice.default_price) {
        price = productPrice.default_price
      } else if (productPrice && productPrice.price) {
        price = productPrice.price
      }
    }
  }
  if (name && reviewCount && price) {
    let imageUrl = `${
      Array.isArray(productPayload) &&
      'https://res.cloudinary.com/ruparupa-com/image/upload' +
        productPayload?.[0].variants?.[0].images?.[0].image_url
    }`
    if (productPayload?.[0].variants?.[0].images?.[0]?.full_image_url) {
      imageUrl = productPayload?.[0].variants?.[0].images?.[0]?.full_image_url
    }
    schema = `
    {
      "@context": "https://schema.org",
      "@type": "Product",
      "name": ${JSON.stringify(name)},
      "image": "${imageUrl}",
      "offers": {
        "@type":"Offer",
        "priceCurrency":"IDR",
        "price":${price}
      },
      "aggregateRating":{
        "@type":"AggregateRating",
        "ratingValue":5,
        "worstRating":1,
        "bestRating":5,
        "reviewCount":${reviewCount}
      }
    }`
  }

  return schema || ''
}

export const GetItemListReviewCountAlgolia = (keyword, products) => {
  let reviewCount = 0
  let price = 0
  let schema = null
  let productsPayload = products?.[0]

  if (productsPayload) {
    productsPayload?.forEach((product) => {
      if (product?.review?.ODI?.total) {
        reviewCount += product.review.ODI.total
      }
    })

    if (productsPayload?.[0]?.special_price !== 0) {
      price = productsPayload?.[0]?.special_price
    } else {
      price = productsPayload?.[0]?.default_price
    }

    if (keyword && reviewCount && price) {
      let imageUrl = `${
        Array.isArray(productsPayload) &&
        productsPayload.length > 0 &&
        'https://res.cloudinary.com/ruparupa-com/image/upload' +
          productsPayload?.[0].images?.[0].image_url
      }`
      if (productsPayload?.[0].images?.[0]?.full_image_url) {
        imageUrl = productsPayload?.[0].images?.[0]?.full_image_url
      }
      schema = `
    {
      "@context": "https://schema.org",
      "@type": "Product",
      "image": "${imageUrl}",
      "name": ${JSON.stringify(keyword)},
      "offers": {
        "@type":"Offer",
        "priceCurrency":"IDR",
        "price":${price}
      },
      "aggregateRating":{
        "@type":"AggregateRating",
        "ratingValue":5,
        "worstRating":1,
        "bestRating":5,
        "reviewCount":${reviewCount}
      }
    }`
    }
  }
  return schema || ''
}

export const renderStoreLogo = (storeName) => {
  const store = OFFICIAL_STORE.find((item) =>
    storeName?.toLowerCase().includes(item.name),
  )

  return store ? store.logo : ''
}

export const GetItemListReviewCountStore = (keyword, products) => {
  let reviewCount = 0
  let price = 0
  let schema = null
  let productsPayload = products?.[0]

  if (productsPayload) {
    productsPayload?.forEach((product) => {
      if (product?.review?.ODI?.total) {
        reviewCount += product.review.ODI.total
      }
    })

    if (productsPayload?.[0]?.special_price !== 0) {
      price = productsPayload?.[0]?.special_price
    } else {
      price = productsPayload?.[0]?.default_price
    }

    if (keyword && reviewCount && price) {
      schema = `
    {
      "@context": "https://schema.org",
      "@type": "Store",
      "name": ${JSON.stringify(keyword?.store_name)},
      "image":"${config.assetsURL + renderStoreLogo(keyword?.store_name)}",
      "address":"${keyword?.store_location}",
      "offers": {
        "@type":"Offer",
        "priceCurrency":"IDR",
        "price":${price}
      },
      "aggregateRating":{
        "@type":"AggregateRating",
        "ratingValue":5,
        "worstRating":1,
        "bestRating":5,
        "reviewCount":${reviewCount}
      }
    }`
    }
  }
  return schema || ''
}

export const GetItemListReviewCountProductDetail = (productDetail) => {
  let schema = null
  let reviews = []

  if (productDetail?.reviews?.length > 0) {
    productDetail.reviews.forEach((review) => {
      reviews.push({
        '@type': 'Review',
        'author': review.customer_name,
        'datePublished': review.posted_at,
        'reviewbody': review.description,
        'reviewRating': {
          '@type': 'Rating',
          'bestRating': 5,
          'ratingValue': review.rating,
          'worstRating': 1,
        },
      })
    })
  } else {
    reviews.push({
      '@type': 'Review',
      'author': '-',
      'datePublished': '',
      'reviewbody': '',
      'reviewRating': {
        '@type': 'Rating',
        'bestRating': 5,
        'ratingValue': 5,
        'worstRating': 1,
      },
    })
  }

  if (productDetail) {
    const formattedImageArray = productDetail?.image?.map((url) => {
      const parts = url.split('/')
      const imageName = parts.pop()
      return `"https://res.cloudinary.com/ruparupa-com/image/upload/w_360,h_360,f_auto,q_auto/${imageName}"`
    })
    const paragraphs = productDetail?.description?.split('<p>')
    const description = paragraphs?.[1]?.split('</p>')?.[0]

    schema = `{
      "@context": "https://schema.org",
      "@type": "Product",
      "name": "${productDetail?.name}",
      "description": "${description}",
      "image": [${formattedImageArray}],
      "sku": "${productDetail?.sku}",
      "gtin": "${productDetail?.gtin}",
      "brand": "${productDetail?.meta_brand}",
      "color": "${productDetail?.color}",
      "itemCondition": "new"`

    if (productDetail?.total_review > 0) {
      schema += `,
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": ${productDetail?.total_rating},
        "reviewCount": ${productDetail?.total_review}
      }`
    }

    schema += `,
      "review": ${JSON.stringify(reviews)},
      "offers": {
        "@type": "Offer",
        "availability": "http://schema.org/InStock",
        "price": ${productDetail?.meta_prices},
        "priceCurrency": "IDR",
        "priceValidUntil": "",
        "url": "${productDetail?.meta_url}"
        }
      }`
  }
  return schema || ''
}

export const GetItemListElementDetailBreadcrumbList = (breadcrumb) => {
  if (!breadcrumb) {
    return ''
  }

  const itemListElement = breadcrumb.map((item, index) => ({
    '@type': 'ListItem',
    'position': index + 1,
    'name': item.name,
    'item': `${config.baseURL}${item.url_key}`,
  }))

  const breadcrumbList = {
    '@context': 'https://schema.org/',
    '@type': 'BreadcrumbList',
    'itemListElement': itemListElement,
  }

  return JSON.stringify(breadcrumbList)
}

export const GetItemListElementArticle = (data) => {
  let schema = null

  if (data) {
    schema = `
    {
      "@context": "https://schema.org",
      "@type": "Article",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "${data?.ogUrl}"
      },
      "headline": "${data?.title_article}",
      "image": "${data?.image_article}",  
      "author": {
        "@type": "Organization",
        "name": "Ruparupa",
        "url": "https://www.ruparupa.com"
      },  
      "publisher": {
        "@type": "Organization",
        "name": "Ruparupa",
        "logo": {
          "@type": "ImageObject",
          "url": ${LOGO_RUPARUPA_DESKTOP}
        }
      },
      "datePublished": "${data?.start_date}",
      "dateModified": "${data?.update_date}"
    }`
  }
  return schema || ''
}

export const GetItemListReviewCountArticle = (data) => {
  let reviewCount = 0
  let schema = null
  let productsPayload = data?.products

  if (productsPayload) {
    productsPayload?.forEach((product) => {
      if (product?.review?.ODI?.total) {
        reviewCount += product.review.ODI.total
      }
    })

    if (data && reviewCount) {
      schema = `
    {
      "@context": "https://schema.org",
      "@type": "Product",
      "name": "Ruparupa",
      "image":"${data?.image_article}",
      "aggregateRating":{
        "@type":"AggregateRating",
        "ratingValue":5,
        "worstRating":1,
        "bestRating":5,
        "reviewCount":${reviewCount}
      }
    }`
    }
  }
  return schema || ''
}

export const GetItemListReviewCountBrands = (data) => {
  let reviewCount = 0
  let schema = null
  let price = 0
  let productsPayload = data?.products

  if (productsPayload) {
    productsPayload?.forEach((product) => {
      if (product?.review?.ODI?.total) {
        reviewCount += product.review.ODI.total
      }
    })

    if (productsPayload?.[0]?.variants?.[0]?.prices?.[0]?.special_price !== 0) {
      price = productsPayload?.[0]?.variants?.[0]?.prices?.[0]?.special_price
    } else {
      price = productsPayload?.[0]?.variants?.[0]?.prices?.[0]?.price
    }

    if (data && reviewCount && price) {
      schema = `
    {
      "@context": "https://schema.org",
      "@type": "Organization",
      "name": "${data?.shop_in_shop}",
      "image":"${config.assetsURL + renderStoreLogo(data?.url)}",
       "offers": {
        "@type":"Offer",
        "priceCurrency":"IDR",
        "price":${price}
      },
      "aggregateRating":{
        "@type":"AggregateRating",
        "ratingValue":5,
        "worstRating":1,
        "bestRating":5,
        "reviewCount":${reviewCount}
      }
    }`
    }
  }
  return schema || ''
}

export const GetItemListElementBrandBreadcrumbList = (breadcrumb) => {
  if (!breadcrumb) {
    return ''
  }

  const breadcrumbList = {
    '@context': 'https://schema.org/',
    '@type': 'BreadcrumbList',
    'itemListElement': [
      {
        '@type': 'ListItem',
        'position': 1,
        'name': 'Ruparupa',
        'item': 'https://www.ruparupa.com',
      },
      {
        '@type': 'ListItem',
        'position': 2,
        'name': `${breadcrumb?.shop_in_shop}`,
        'item': `https://www.ruparupa.com/ms/${breadcrumb?.url}`,
      },
    ],
  }

  return JSON.stringify(breadcrumbList)
}
