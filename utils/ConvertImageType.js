const ConvertImageType = (url) => {
  // /\.(gif|jpe?g|tiff?|png|webp|bmp)$/i
  const regex = /\.(jpe?g|png)$/i
  let urlValidate = url
  if (url) {
    if (url.includes('cloudinary')) {
      urlValidate = url?.replace(regex, '.webp')
    } else if (url.includes('cdn.ruparupa.io')) {
      urlValidate = url?.replace('.io', '.io/filters:format(webp)')
    }
  }
  return urlValidate
}

export default ConvertImageType
