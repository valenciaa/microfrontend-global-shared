import localforage from 'localforage'

const getStoreData = () => {
  // return '58eb0b7914bb9f64fd346b04'
  // return false
  return localforage
    .getItem('store_data')
    .then((value) => {
      return value
    })
    .catch(() => {
      return false
    })
}
export default { getStoreData }
