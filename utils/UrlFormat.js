import { removeObjectIsEmpty } from './Array'

/**
 * mengubah object menjadi query string
 * {a:2, b:4} => a=2&b=4
 *
 * @param   {[object]}  obj
 *
 * @return  {[string]}
 */
export const objectToQueryString = (obj) => {
  return new URLSearchParams(obj).toString()
}

/**
 * generate multiple params with same key for array query params (e.g. {foo: [a,b] -> foo=a&foo=b})
 * @param {[object]} obj
 *
 * @return {[string]}
 */
export const objectWithArrayParamsToQueryString = (obj) => {
  let queryString = ''
  for (const key in obj) {
    const arrayParam = Array.isArray(obj[key]) ? obj[key] : [obj[key]]
    for (const paramValue of arrayParam) {
      queryString += `${queryString.length > 0 ? '&' : ''}${key}=${paramValue}`
    }
  }
  return encodeURI(queryString)
}

/**
 * generate url link
 *
 * @param   {[string]}  pathname
 * @param   {[object]}  obj
 *
 * @return  {[string]}
 */
export const queryStringUrl = (
  pathname,
  obj = {},
  transformArrayQueryParams = false,
) => {
  const newObj = removeObjectIsEmpty(obj)
  const qs = transformArrayQueryParams
    ? objectWithArrayParamsToQueryString(newObj)
    : objectToQueryString(newObj)
  return getPathFromUrl(pathname) + (qs ? `?${qs}` : '')
}

/**
 * menghapus querystring di url
 *
 * @param   {[string]}  url
 *
 * @return  {[string]}
 */
export const getPathFromUrl = (url) => {
  return url?.split('?')?.[0]
}

/**
 * bersihkan parameter yg bernilai kosong
 *
 * @param   {[object]}  params   ex {level1:rumah-tangga, level2:dekorasi-rumah, level3: undefined}
 *
 * @return  {[object]}         ex {level1:rumah-tangga, level2:dekorasi-rumah}
 */
export const clearRouteParams = (params) => {
  const tmpUrl = { ...params }
  Object.keys(tmpUrl).forEach((i) => {
    if (!params[i]) {
      delete tmpUrl[i]
    }
  })
  return tmpUrl
}
/**
 * ubah parameter menjadi url_key
 *
 * @param   {[object]}  params  ex {level1:rumah-tangga, level2:dekorasi-rumah}
 * @param   {[boolean]}  isRemoveHtml digunakan untuk penanda hapus html url.html
 *
 * @return  {[string]}          ex: rumah-tangga/dekorasi-rumah
 */
export const paramsToUrlKey = (params, isRemoveHtml = false) => {
  const tmpUrl = []
  Object.keys(params).forEach((i) => {
    if (params[i]) {
      tmpUrl.push(params[i])
    }
  })

  const urlKeys = tmpUrl.join('/')
  return isRemoveHtml ? removeHtml(urlKeys) : urlKeys
}

/**
 * generate query string products
 * @param {object} query
 * @param {{identifier:string, categoryId:string }} param1  {identifier digunakan untuk merubah data identifier}
 */
export const generateQsProduct = (
  query,
  { identifier, categoryId, identifierUrl = '' } = {},
) => {
  const qs = { ...query }
  qs.from = parseInt(qs.from) || 0
  qs.size = parseInt(qs.size) || 48
  // set url key dari data query
  if (qs[identifier]) {
    const keys = Object.keys(qs[identifier])
    const keyUrl = keys.map((i) => qs[identifier][i]).join('/')
    if (identifierUrl !== '') {
      qs.identifier = identifierUrl
    } else {
      qs.identifier = keyUrl
    }
    delete qs[identifier]
  }
  // categoryId
  qs.categoryId = categoryId
  return qs
}

/**
 * menambah html pada url
 * ex: rumah-tangga => rumah-tangga.html
 * @param {string} url
 * @returns {string}
 */
export const addHtml = (url) => {
  return removeHtml(url) + '.html'
}

/**
 * menghapus html
 * @param {string} url
 * @returns {string}
 */
export const removeHtml = (url) => {
  return url.split('.html')[0]
}
