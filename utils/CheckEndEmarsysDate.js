import dayjs from 'dayjs'

export function isNotEndDateEmarsys() {
  return dayjs() < dayjs('2023-01-01') || false
}
