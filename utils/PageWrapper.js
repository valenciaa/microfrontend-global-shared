import React from 'react'
import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'

import globalAPI from '../services/IndexApi'
import config from '../../../config'
// import CategoryActions from '../redux/reducer/CategoryRedux'
export const PageWrapper = (PagesComponent) => {
  class PageContainer extends React.Component {
    static async getInitialProps(ctx) {
      const { query, req } = ctx
      // const { query, req, store } = ctx
      const isServer = !!req
      let results = {}

      let storeData = {}
      if (req) {
        // SSR
        storeData =
          Object.prototype.hasOwnProperty.call(req, 'cookies') &&
          req.cookies.store_data
            ? JSON.parse(req.cookies.store_data)
            : {}
      } else {
        // CSR
        storeData = Cookies.get('store_data')
          ? JSON.parse(Cookies.get('store_data'))
          : {}
      }

      const storeCode = !isEmpty(storeData) ? storeData.store_code : ''
      const storeName = !isEmpty(storeData) ? storeData.name : ''
      const timestamp = req && req.timestamp ? req.timestamp : Date.now()
      const searchState = ''
      // const pathName = !isEmpty(req.originalUrl) ? req.originalUrl.replace(/\?.*$/, '') : ''

      // reserved index 0, for get initial props from pages
      const tempApiCall = [{}]

      // starting from here, just push any api you want to get from server side
      // tempApiCall.push(api.getInspirations(storeCode))
      if (config.environment === 'desktop') {
        tempApiCall.push(globalAPI.getCategory())
      }

      let allResponse
      if (PagesComponent.getInitialProps) {
        // only run if pages have getinitialprops function
        tempApiCall[0] = PagesComponent.getInitialProps(ctx)
        allResponse = await Promise.all(tempApiCall)
        results = {
          ...allResponse[0],
        }
      } else {
        allResponse = await Promise.all(tempApiCall)
      }

      // Uncomment this section for SSR data fetching and storing data in Redux
      // if (isServer) {
      //   store.dispatch(..............)
      // }

      results =
        config.environment === 'desktop'
          ? {
              ...results,
              // inspiration: allResponse[1]?.data?.data,
              inspirationPromoBrand: allResponse[1]?.data?.data,
              categories: allResponse[2]?.data?.data,
            }
          : {
              ...results,
              inspirationPromoBrand: allResponse[1]?.data?.data,
            }

      return {
        results,
        isServer,
        query,
        storeCode,
        storeName,
        timestamp,
        searchState,
      }
    }

    render() {
      return <PagesComponent {...this.props} />
    }
  }

  return PageContainer
}
