import config from '../../../config'

const GetB2bType = (isReturnNon) => {
  if (!config.isB2b) {
    return isReturnNon ? 'non-b2b' : ''
  }

  if (config.companyCode === 'HCI') {
    return 'informa_b2b'
  }

  if (config.companyCode === 'AHI') {
    return 'ace_b2b' // ACE_HARDWARE
  }
}

export default GetB2bType
