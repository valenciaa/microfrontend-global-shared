/**
 * merubah data number dengan . menjadi format angka
 * ex : 1.000 => 1000
 * @param {string} x
 * @returns {string}
 */
export const NumberFormat = (x) => String(x).replace(/\./g, '')
export default NumberFormat
