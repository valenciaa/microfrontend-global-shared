const handleNpwpIsNotNumber = (e) => {
  if (
    isNaN(parseInt(e.target.value)) &&
    e.target.value !== '-' &&
    e.target.value !== '.'
  ) {
    const el = document.getElementById('company_npwp')

    el.value = ''

    return
  }
}

const handleNpwpFormatToDigit = (value) => {
  return value?.replace(/[-]/g, '')?.replace(/\./g, '')
}

export { handleNpwpIsNotNumber, handleNpwpFormatToDigit }
