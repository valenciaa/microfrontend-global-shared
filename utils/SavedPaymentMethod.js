import GetParam from '../../global/utils/GetParam'

export const formatCcNumber = (value) => {
  const v = value?.replace(/\s+/g, '')?.replace(/[^0-9]/gi, '')
  const matches = v?.match(/\d{4,16}/g)
  const match = (matches && matches[0]) || ''
  const parts = []

  for (let i = 0, len = match.length; i < len; i += 4) {
    parts?.push(match?.substring(i, i + 4))
  }

  if (parts?.length) {
    return parts?.join('-')
  }

  return value
}

export const formatCcExpiry = (value) => {
  const v = value?.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
  const matches = v?.match(/\d{2,4}/g)
  const match = (matches && matches[0]) || ''
  const parts = []

  for (let i = 0, len = match?.length; i < len; i += 2) {
    parts?.push(match?.substring(i, i + 2))
  }

  if (parts?.length === 1) {
    return parts?.toString() + ' / '
  }

  if (parts?.length > 1) {
    return parts?.join(' / ')
  }

  return value
}

export const setCcCardNumber = (value, setValue) => {
  const ccNumber = formatCcNumber(value)
  const cardNumber = ccNumber ? ccNumber.replace(/-/g, '') : ''

  setValue?.('card_number', cardNumber)
}

export const setCcCardExpiry = (value, setValue) => {
  const expFormat = formatCcExpiry(value)
  const exp = expFormat ? expFormat.replace(/\s/g, '').replace(/\//g, '') : ''

  setValue?.('expiry', exp)
}

export const formatCcNumberText = (card) => {
  return `${card?.substr(0, 2)}**-****-****-*${card
    ?.replace('-', '')
    ?.substr(card?.length - 4, 3)}`
}

export const formatCcExpiryText = (expire, month) => {
  return `${month < 10 ? `0${month}` : month}/
                    ${expire?.getFullYear()?.toString()?.slice(-2)}`
}

export const getCcCardAtribute = (list) => {
  const brand = list?.cc_brand
  const logo = brand?.includes?.('american')
    ? 'amex'
    : brand === 'unknown'
      ? 'jcb'
      : brand?.toLowerCase()
  const card = list?.cc_number

  const expire = new Date(list?.saved_token_id_expired_at)
  const month = expire?.getMonth() + 1

  return {
    brand,
    logo,
    card,
    expire,
    month,
  }
}

export const getCcMaskedCardNumber = (cardNum) => {
  return `${cardNum.substr(0, 8)}-${cardNum.substr(cardNum.length - 4, 4)}`
}

export const getMidtransErrorMessage = (response) => {
  const validations = response?.validation_messages
  const isValidationsArray = !!(
    Array.isArray(validations) && validations.length > 0
  )
  const message = response?.status_message

  if (!isValidationsArray && message) {
    switch (message) {
      case 'Success, transaction is found':
        return 'Transaksi gagal. Silakan coba lagi atau gunakan metode pembayaran lainnya'
      case 'Card is not authenticated.':
        return 'Mohon hubungi bank penerbit kartu anda'
      default:
        return message
    }
  }

  if (!isValidationsArray) {
    return 'Terjadi kesalahan, mohon ulangi beberapa saat lagi'
  }

  if (
    validations?.includes?.('card_exp_year and card_exp_month must be greater')
  ) {
    return 'Tahun dan bulan expired anda harus lebih besar dari tahun dan bulan sekarang'
  }

  if (
    validations?.includes?.('card_number does not match with luhn algorithm')
  ) {
    return 'Nomor kartu kredit atau cvv yang Anda masukkan salah'
  }

  return validations?.[0]
}

export const handleSaveCc = (
  watch,
  getRedirectUrl,
  setError,
  setIsProcessing,
) => {
  const MidtransNew3ds = window?.MidtransNew3ds

  if (!MidtransNew3ds) {
    setIsProcessing?.(false)

    setError?.('Terjadi kesalahan, mohon ulangi beberapa saat lagi')

    return
  }

  const options = {
    onSuccess: (response) => {
      setIsProcessing?.(false)

      getRedirectUrl?.({
        cc_name: watch?.('cc_name'),
        masked_card: getCcMaskedCardNumber(watch?.('card_number')),
        token: response?.token_id,
      })
    },
    onFailure: (response) => {
      setIsProcessing?.(false)

      setError(getMidtransErrorMessage(response))
    },
  }

  return MidtransNew3ds?.getCardToken(
    {
      card_cvv: watch?.('card_cvv'),
      card_number: watch?.('card_number'),
      card_exp_month: watch?.('expiry')?.slice(0, 2),
      card_exp_year: watch?.('expiry')?.slice(-2),
    },
    options,
  )
}

export const getCcFormErrorMessage = (errors) => {
  const errCcName = errors?.cc_name?.message
  const errCardNumber = errors?.card_number?.message
  const errExpiry = errors?.expiry?.message
  const errCardCvv = errors?.card_cvv?.message

  return {
    errCcName,
    errCardNumber,
    errExpiry,
    errCardCvv,
  }
}

export const isCcFormValid = (watch, errors) => {
  const errCcName = errors?.cc_name?.message
  const errCardNumber = errors?.card_number?.message
  const errExpiry = errors?.expiry?.message
  const errCardCvv = errors?.card_cvv?.message

  const isFormNotValid =
    !watch?.('cc_name') ||
    !watch?.('card_number') ||
    !watch?.('expiry') ||
    !watch?.('card_cvv') ||
    !!errCcName ||
    !!errCardNumber ||
    !!errExpiry ||
    !!errCardCvv

  return isFormNotValid
}

export const getGopayPhoneFormat = (phone) => {
  const length = phone?.length

  return {
    front: `${phone?.replace('62', '0')?.substring(0, 4)}`,
    middle: `${'*'.repeat(length - 8)}`,
    back: `${phone?.slice(-3)}`,
  }
}

export const getGopayErrorMessage = () => {
  const code = GetParam('responseCode')

  if (!code) {
    return ''
  }

  let message = 'Gagal menghubungkan GoPay'

  switch (code) {
    case '4001002':
      return `${message}, nomor telepon tidak dapat ditemukan`
    case '4011000':
      return `${message}, autentikasi gagal`
    case '4041012':
      return `${message}, nomor telepon belum terdaftar pada aplikasi GoPay`
    case '5001001':
      return `${message}, terjadi kesalahan pada sistem`
    case '5041000':
      return `${message}, sesi kamu telah berakhir`
    default:
      return message
  }
}

export const getGopayErrorLogPayload = () => {
  return {
    code: GetParam('responseCode'),
    message: GetParam('responseMessage'),
    reference: GetParam('referenceNo'),
  }
}

export const getGopayBalance = (options) => {
  const getValue = (name) =>
    options?.find((option) => option?.name === name)?.balance?.value || 0

  return {
    balance: getValue('GOPAY_WALLET'),
    payLaterLimit: getValue('PAY_LATER'),
  }
}
