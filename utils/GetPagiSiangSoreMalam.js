const getPagiSiangSoreMalam = () => {
  const currentDate = new Date()
  if (currentDate.getHours() >= 0 && currentDate.getHours() < 10) {
    return 'Pagi'
  } else if (currentDate.getHours() >= 10 && currentDate.getHours() < 15) {
    return 'Siang'
  } else if (currentDate.getHours() >= 15 && currentDate.getHours() < 19) {
    return 'Sore'
  } else if (currentDate.getHours() >= 19 && currentDate.getHours() <= 23) {
    return 'Malam'
  }
}

export default getPagiSiangSoreMalam
