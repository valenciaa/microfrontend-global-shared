import config from '../../../config'
import { ACE_HARDWARE } from './constants/AceConstants'

const MetaStoreLocation = (currentCityName) => {
  let metaData = {}

  if (config.companyCode === 'AHI' && currentCityName) {
    metaData = {
      meta_title:
        `Toko ${ACE_HARDWARE} ` +
        currentCityName +
        ' - Lokasi, Promo dan Katalog',
      meta_description:
        `Temukan katalog promo dan diskon terbaru produk rumah tangga dan gaya hidup terlengkap dari ${ACE_HARDWARE} Indonesia Official Store di ` +
        currentCityName,
    }
  } else if (config.companyCode === 'HCI' && currentCityName) {
    metaData = {
      meta_title:
        'Toko Informa ' + currentCityName + ' - Lokasi, Promo dan Katalog',
      meta_description:
        'Dapatkan aneka produk home living dan furniture terbaik dari Informa Online. Temukan juga promo dan katalog terbaru dan terlengkap dari Informa ' +
        currentCityName,
    }
  } else if (config.companyCode === 'TGI' && currentCityName) {
    metaData = {
      meta_title:
        'Toko Toys Kingdom ' + currentCityName + ' - Lokasi, Promo dan Katalog',
      meta_description:
        'Temukan katalog promo dan diskon mainan anak ekslusif dan resmi terlengkap dan berkualitas dari Toys Kingdom Official Store di ' +
        currentCityName,
    }
  } else if (config.companyCode === 'AHI' && !currentCityName) {
    metaData = {
      meta_title: `Toko ${ACE_HARDWARE} terdekat - Lokasi, Promo dan Katalog`,
      meta_description: `Temukan katalog promo dan diskon terbaru produk rumah tangga dan gaya hidup terlengkap dari ${ACE_HARDWARE} Indonesia Official Store terdekat`,
    }
  } else if (config.companyCode === 'HCI' && !currentCityName) {
    metaData = {
      meta_title: 'Toko Informa terdekat - Lokasi, Promo dan Katalog',
      meta_description:
        'Dapatkan aneka produk home living dan furniture terbaik dari Informa Online. Temukan juga promo dan katalog terbaru dan terlengkap dari Informa terdekat',
    }
  } else if (config.companyCode === 'TGI' && !currentCityName) {
    metaData = {
      meta_title: 'Toko Toys Kingdom terdekat - Lokasi, Promo dan Katalog',
      meta_description:
        'Temukan katalog promo dan diskon mainan anak eksklusif dan resmi terlengkap dan berkualitas dari Toys Kingdom Official Store terdekat',
    }
  }

  return metaData
}

export { MetaStoreLocation }
