const CountView = (totalProducts) => {
  if (totalProducts > 50) {
    return Math.floor(totalProducts / 10) * 10
  } else if (totalProducts <= 50) {
    return totalProducts
  }
}
export default CountView
