export const boldResultPart = (searchResult, searchTerm, sensitive = false) => {
  // searchResult = props search result that we map
  // searchTerm = props our search input
  const sensitiveRegex = sensitive ? 'g' : 'ig' // ig means that the regex is case insensitive while g means case sensitive
  const newRgx = new RegExp(searchTerm, sensitiveRegex)
  // const results = searchResult.replace(/- /g, '').replace(/\./g, '').split(re) // asli
  const results = searchResult.split(newRgx)
  // please be aware of this function's return
  // you should call this function like this => const [resultNotHighlighted, resultHighlighted] = boldResultPart(searchResult, searchTerm, false)
  return [results, [...searchResult.matchAll(newRgx)].map((x) => x[0])]
}
// if you want to use this function, please aware that the params here are results from the boldResultPart function above
export const reconstructHighlightedAndNonHighlighted = (
  notHighlightedWord,
  highlightedWord,
) => {
  const convertCharToBold = (char) => {
    return <b>{char}</b>
  }
  const reconstructSearchWord = notHighlightedWord.map(
    (wordSplitPart, index) =>
      index ? (
        <span key={index}>
          {convertCharToBold(highlightedWord[index - 1])}
          {wordSplitPart}
        </span>
      ) : (
        wordSplitPart
      ),
  )
  return reconstructSearchWord
}
