const SliceName = (string, number) => {
  return string?.slice(0, number) + (string.length > number ? '...' : '')
}

export default SliceName
