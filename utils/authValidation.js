import { object, string, number, ref } from 'yup'
import dayjs from 'dayjs'

export const phoneRegexWithoutZero = /(^8)(\d{3,4}-?){2}\d{2,3}$/
// export const phoneRegexWithZero = /(^\+628|628|^08)(\d{3,4}-?){2}\d{2,3}$/
export const emailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/
export const memberIdRegex = /^[a-zA-Z0-9]*$/
export const nikRegex = /^NIK[a-zA-Z0-9]{5,6}$/
export const passwordRegex = /^(?=.*?[A-Z])(?=.*?[a-z]).{8,}$/
export const passwordCompleteRegex =
  /^(?=.*[A-Z])(?=.*[\W_])(?=.*\d)(?!.*%).{8,}$/
export const alphabethRegex = /^[a-zA-Z\s]+$/

// ? Schema untuk identity form, input hanya phone number

export const schemaIdentityPhoneOnly = (minLength = 8, maxLength = 11) => {
  const phoneRegexv2 = `^[0-9]{${minLength},${maxLength}}$`
  return object({
    identifier: string()
      .trim()
      .required('Nomor handphone di perlukan')
      .matches(
        phoneRegexv2,
        'Pastikan nomor handphone yang kamu masukkan benar',
      ),
  }).required()
}

// ? Schema untuk identity form, input bisa phone number, email, member id dan nik
export const schemaIdentityAll = (minLength = 8, maxLength = 11) => {
  const phoneRegexv2 = `^[0-9]{${minLength},${maxLength}}$`

  return object({
    identifier: string()
      .required('Identitas di perlukan')
      .trim()
      .test({
        name: 'identifier test',
        test: function (value) {
          if (value.match(/^nik/i) && !value.match(nikRegex)) {
            return this.createError({
              message: 'Pastikan NIK yang kamu masukkan benar',
              path: 'identifier', // Fieldname
            })
          }

          if (value.includes('@') && !value.match(emailRegex)) {
            return this.createError({
              message: 'Pastikan alamat email yang kamu masukkan benar',
              path: 'identifier', // Fieldname
            })
          }

          if (
            (Number(value) || Number(value) === 0) &&
            !value.match(phoneRegexv2)
          ) {
            return this.createError({
              message: 'Pastikan nomor handphone yang kamu masukkan benar',
              path: 'identifier', // Fieldname
            })
          }

          if (
            !value.includes('@') &&
            !value.includes('+') &&
            !value.match(memberIdRegex)
          ) {
            return this.createError({
              message: 'Pastikan Member ID yang kamu masukkan benar',
              path: 'identifier', // Fieldname
            })
          }

          return true
        },
      }),
  }).required()
}

export const schemaProfileForm1 = (
  isCountryCode = false,
  minLength = 8,
  maxLength = 11,
) => {
  const phoneRegexv2 = `^[0-9]{${minLength},${maxLength}}$`

  return object({
    name: string()
      .required('Nama lengkap di perlukan')
      .matches(alphabethRegex, 'Nama harus berformatkan alfabet'),
    birthday: string()
      .required('Tanggal lahir di perlukan')
      .test(
        'birthday',
        'Usia yang didaftarkan tidak boleh lebih dari tanggal hari ini',
        function (value) {
          const now = dayjs().format('YYYY-MM-DD')
          const dateDifferent = dayjs(now).diff(value, 'day')
          return dateDifferent >= 0
        },
      )
      .test('birthday', 'Usia harus min.12 tahun', function (value) {
        const now = dayjs().format('YYYY-MM-DD')
        const yearDifferent = dayjs(now).diff(value, 'year')
        return yearDifferent >= 12
      }),
    email: string()
      .trim()
      .email('Email tidak valid')
      .required('Email di perlukan'),
    phone: string()
      .trim()
      .required('Nomor Handphone di perlukan')
      .matches(
        isCountryCode ? phoneRegexv2 : phoneRegexWithoutZero,
        'Nomor Handphone tidak valid',
      ),
    password: string()
      .required('Password di perlukan')
      .min(8, 'Kata sandi belum mencapai 8 karakter')
      .matches(
        passwordRegex,
        'Kata sandi harus terdiri dari minimal 8 karakter yang mengandung huruf kapital dan huruf kecil',
      ),
    employeeCode: string(),
  }).required()
}

export const schemaProfileForm2 = object({
  gender: string()
    .typeError('Jenis Kelamin di perlukan')
    .required('Jenis Kelamin di perlukan'),
  address: string(),
  districtId: number()
    .typeError('Provinsi, Kota, Kecematan di perlukan')
    .required('Provinsi, Kota, Kecematan di perlukan'),
}).required()

export const schemaResetPassword = object({
  password: string()
    .required('Kata sandi tidak boleh kosong')
    .min(8, 'Kata sandi belum mencapai 8 karakter')
    .matches(
      passwordRegex,
      'Kata sandi harus terdiri dari minimal 8 karakter yang mengandung huruf kapital dan huruf kecil',
    ),
  confirmPassword: string()
    .required('Kata sandi tidak bole kosong')
    .oneOf(
      [ref('password'), null],
      'Konfirmasi kata sandi tidak sesuai. Silakan ulangi kembali',
    ),
}).required()

export const schemaQuickRegisterB2b = object({
  firstName: string().trim().required('Nama depan wajib diisi'),
  lastName: string().trim().required('Nama belakang wajib diisi'),
  email: string()
    .trim()
    .email('Alamat email tidak valid')
    .required('Alamat Email diperlukan'),
  taxNumberType: string().required('Tipe Wajib Pajak diperlukan'),
  nitkuNo: string()
    .trim()
    .test({
      name: 'test',
      test: function (value, context) {
        if (context.parent.taxNumberType !== 'Z2') {
          return true
        }

        if (value === '') {
          return this.createError({
            message: 'NITKU diperlukan',
            path: 'nitkuNo',
          })
        }

        if (!+value?.match(/^\d+$/)) {
          return this.createError({
            message: 'Format NITKU tidak valid',
            path: 'nitkuNo',
          })
        }

        if (value.length < 22) {
          return this.createError({
            message: 'NITKU minimal 22 karakter',
            path: 'nitkuNo',
          })
        }

        return true
      },
    }),
  password: string()
    .required('Password diperlukan')
    .min(
      8,
      'Minimal 8 karakter yang mengandung huruf kapital, huruf kecil, angka, dan simbol',
    )
    .matches(
      passwordCompleteRegex,
      'Kata sandi harus terdiri dari 8 karakter yang mengandung huruf besar, huruf kecil, angka, dan simbol (kecuali %).',
    ),
  npwpNumber: string()
    .required('Nomor NPWP diperlukan')
    .test('number only', 'Format Nomor NPWP tidak valid', (val) =>
      val?.match(/^\d+$/),
    )
    .min(15, 'Nomor NPWP harus terdiri dari 15 karakter'),
}).required()
