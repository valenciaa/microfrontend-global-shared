import isEmpty from 'lodash/isEmpty'
import GetParam from './GetParam'
import dayjs from 'dayjs'
import isArray from 'lodash/isArray'

import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'

dayjs.extend(isSameOrAfter)
dayjs.extend(isSameOrBefore)

// 2.0 - mixpanelTrackViewPDP
export const getMixpanelProductDetailData = (product, review, source) => {
  let isFlashSale = false
  if (product?.checkFlashSale) {
    const flashSaleEvent =
      product?.events?.find((event) => event?.url_key.includes('flash-sale')) ||
      {}
    isFlashSale =
      !isEmpty(flashSaleEvent) &&
      dayjs().isSameOrAfter(flashSaleEvent?.start_date, 'dd-mm-yy h:mm:ss') &&
      dayjs().isSameOrBefore(flashSaleEvent?.end_date, 'dd-mm-yy h:mm:ss')
  }

  const itemPrice = product?.variants?.[0]?.prices?.[0]?.special_price
    ? parseInt(product?.variants[0]?.prices?.[0]?.special_price)
    : 0
  const sellingPrice = product?.variants?.[0]?.prices?.[0]?.price
    ? parseInt(product?.variants?.[0]?.prices?.[0]?.price)
    : 0
  const isItemCanInstalled = !!product?.attributes?.find(
    (attribute) => attribute?.attribute_id === 396,
  )
  const isPromoTukarKoin = () => {
    if (isArray(product?.variants?.[0].price_promo)) {
      const isPromo = product?.variants?.[0].price_promo.filter((el) => {
        return el.description.includes('Tukarkan Koinmu')
      })
      return !!isPromo.length
    } else {
      return false
    }
  }

  let data = {
    'Item Name': product?.name ? product?.name?.toLowerCase() : 'None',
    'Item Category LV 1': product?.trackBreadcrumb?.[0]?.name
      ? product?.trackBreadcrumb?.[0]?.name
      : 'None',
    'Item Category LV 2': product?.trackBreadcrumb?.[1]?.name
      ? product?.trackBreadcrumb?.[1]?.name
      : 'None',
    'Item Category LV 3': product?.trackBreadcrumb?.[2]?.name
      ? product?.trackBreadcrumb?.[2]?.name
      : 'None',
    'Item Category LV 4': product?.trackBreadcrumb?.[3]?.name
      ? product?.trackBreadcrumb?.[3]?.name
      : 'None',
    'Item Price': itemPrice !== 0 ? itemPrice : sellingPrice,
    'Selling Price': sellingPrice,
    'Item Colour': product?.variants?.[0]?.attributes?.[0]?.attribute_option
      ?.option_value
      ? product?.variants[0]?.attributes?.[0]?.attribute_option?.option_value
      : 'Mix',
    'Discount?': itemPrice !== 0,
    'Item Price Discount%':
      itemPrice !== 0 ? ((sellingPrice - itemPrice) / sellingPrice) * 100 : 0,
    'Item ID': product?.variants?.[0]?.sku || 'None',
    'Item Brand': product?.brand?.name || 'None',
    'Stock Available?': product?.is_in_stock === 1,
    'Ownfleet?': 'None',
    'Instant Delivery?': 'None',
    'Regular Delivery?': 'None',
    'Pickup?': 'None',
    'Promo?': itemPrice !== 0 || isPromoTukarKoin(),
    'Promo Tukar koin': isPromoTukarKoin(),
    'Product Label': product?.variants?.[0]?.label || 'None',
    'Product Label?': product?.variants?.[0]?.label !== '',
    'Supplier Alias': product?.supplier?.supplier_alias || 'None',
    'Discount Amount':
      itemPrice !== 0 && sellingPrice !== 0 ? sellingPrice - itemPrice : 0,
    'Is Rating & Review': review?.total > 0 || false,
    'Room Simulation?':
      product?.variants?.[0]?.is_augmented_reality_enabled || false,
    'Brand Store Id': handleStoreAvailable(product),
    'Is Flash Sale': isFlashSale,
    'Bisa Dirakit?': isItemCanInstalled,
    'ITM Source': GetParam('itm_source') || 'None',
    'ITM Term': GetParam('itm_term') || 'None',
    'ITM Campaign': GetParam('itm_campaign') || 'None',
    'ITM Device': GetParam('itm_device') || 'None',
    'Brand Store Name': product?.supplier?.name || 'None',
  }
  if (
    [
      'product-recommendation-related-pdp',
      'product-recommendation-also-bought-pdp',
    ].includes(source)
  ) {
    data = {
      ...data,
      'Recommendation Source': 'AWS',
    }
  }

  if (source === 'gift-registry-recommendation') {
    data = {
      ...data,
      'Location': 'gift registry',
      'ITM Source': GetParam('itm_source'),
      'ITM Campaign': GetParam('itm_campaign'),
      'ITM Term': GetParam('itm_term'),
    }
  }

  if (['detail-transaction'].includes(source)) {
    data = {
      ...data,
      Location: 'Detail Transaction',
    }
  }
  if (source?.toLowerCase()?.includes('wishlist')) {
    data = {
      ...data,
      Location: 'Wishlist',
    }
  }
  return data
}

export const getMixpanelTrackViewPDPRecommendationAWS = (
  product,
  location,
  section,
) => {
  const itemPrice = product?.variants?.[0]?.prices?.[0]?.special_price
    ? parseInt(product?.variants[0]?.prices?.[0]?.special_price)
    : 0
  const sellingPrice = product?.variants?.[0]?.prices?.[0]?.price
    ? parseInt(product?.variants?.[0]?.prices?.[0]?.price)
    : 0
  const data = {
    'Source': 'AWS',
    'Section': section,
    'Location': location,
    'Item Name': product?.name ? product?.name?.toLowerCase() : 'None',
    'Selling Price': sellingPrice,
    'Item Colour': product?.variants?.[0]?.attributes?.[0]?.attribute_option
      ?.option_value
      ? product?.variants[0]?.attributes?.[0]?.attribute_option?.option_value
      : 'Mix',
    'Discount?': itemPrice !== 0,
    'Item Price Discount%':
      itemPrice !== 0 ? ((sellingPrice - itemPrice) / sellingPrice) * 100 : 0,
    'Item ID': product?.variants?.[0]?.sku || 'None',
    'Item Brand': product?.brand?.name ? product?.brand?.name : 'None',
    'Stock Available?': product?.is_in_stock === 1,
    'Instant Delivery?': 'None',
    'Promo?': itemPrice !== 0,
    'Discount Amount':
      itemPrice !== 0 && sellingPrice !== 0 ? sellingPrice - itemPrice : 0,
    'Is Rating & Review': product?.review?.ODI?.total > 0 || false,
  }

  return data
}

export const getMixpanelProductDetailEventName = (source) => {
  return source === 'product-last-seen-pdp' ? 'Last Seen Product' : 'View PDP'
}

const parseIsFlashSale = (product) => {
  const flashSaleEvent =
    product?.events?.find((event) => event?.url_key.includes('flash-sale')) ||
    {}
  return (
    !isEmpty(flashSaleEvent) &&
    dayjs().isSameOrAfter(flashSaleEvent?.start_date, 'dd-mm-yy h:mm:ss') &&
    dayjs().isSameOrBefore(flashSaleEvent?.end_date, 'dd-mm-yy h:mm:ss')
  )
}

// 2.0 - mixpanelTrackAddToCart
export const getMixpanelAddToCartData = (product, additionalProperty) => {
  const itemPrice = product?.variants?.[0]?.prices?.[0]?.special_price
    ? parseInt(product?.variants[0]?.prices?.[0]?.special_price)
    : 0
  const sellingPrice = product?.variants?.[0]?.prices?.[0]?.price
    ? parseInt(product?.variants?.[0]?.prices?.[0]?.price)
    : 0

  let data = {
    'Stock Available?': product?.is_in_stock === 1,
    'Regular Delivery?': 'None',
    'Promo?': itemPrice !== 0,
    'Pickup?': 'None',
    'Ownfleet?': 'None',
    'ITM Source': GetParam('itm_source') || 'None',
    'ITM Campaign': GetParam('itm_campaign') ?? 'hanging-cart',
    'Item Price': itemPrice !== 0 ? itemPrice : sellingPrice,
    'Selling Price': sellingPrice,
    'Item Name': product?.name ? product?.name?.toLowerCase() : 'None',
    'Item ID': product?.variants?.[0]?.sku || 'None',
    'Item Colour': product?.variants?.[0]?.attributes?.[0]?.attribute_option
      ?.option_value
      ? product?.variants[0]?.attributes?.[0]?.attribute_option?.option_value
      : 'Mix',
    'Discount?': itemPrice !== 0,
    'Instant Delivery?': 'None',
    'Item Brand': product?.brand?.name || 'None',
    'Item Category LV 1': product?.trackBreadcrumb?.[0]?.name ?? 'None',
    'Item Category LV 2': product?.trackBreadcrumb?.[1]?.name ?? 'None',
    'Item Category LV 3': product?.trackBreadcrumb?.[2]?.name ?? 'None',
    'Item Category LV 4': product?.trackBreadcrumb?.[3]?.name ?? 'None',
    'Item Price Discount%':
      itemPrice !== 0 ? ((sellingPrice - itemPrice) / sellingPrice) * 100 : 0,
    'Product Label': product?.variants?.[0]?.label || 'None',
    'Product Label?': product?.variants?.[0]?.label !== '',
    'Supplier Alias': product?.supplier?.supplier_alias || 'None',
    'Discount Amount':
      itemPrice !== 0 && sellingPrice !== 0 ? sellingPrice - itemPrice : 0,
    'Brand Store Id': handleStoreAvailable(product),
    'SKU Flashsale': parseIsFlashSale(product),
  }

  if (additionalProperty) {
    data = {
      ...data,
      ...additionalProperty,
    }
  }

  return data
}

// export const mixpanelTrackCart = (items, additionalProperty) => {
//   const itemName = []
//   const itemCategory1 = []
//   const itemCategory2 = []
//   const itemCategory3 = []
//   const itemCategory4 = []
//   const itemPrice = []
//   const itemQty = []
//   const itemId = []
//   const itemBrand = []
//   const itemColour = []
//   const stockAvailable = []
//   const discount = []
//   const productLabel = []
//   const existProductLabel = []

//   if (items && items.length > 0) {
//     items = items?.forEach(item => {
//       let isDiscount = false

//       if (items?.prices?.normal_price && items?.prices?.selling_price) {
//         isDiscount = (items?.prices?.normal_price !== items?.prices?.selling_price)
//       }

//       itemName.push(item?.name || 'None')
//       itemCategory1.push(item?.breadcrumbs?.[0] ?? 'None')
//       itemCategory2.push(item?.breadcrumbs?.[1] ?? 'None')
//       itemCategory3.push(item?.breadcrumbs?.[2] ?? 'None')
//       itemCategory4.push(item?.breadcrumbs?.[3] ?? 'None')
//       itemPrice.push(item?.prices?.selling_price ?? 0)
//       itemQty.push(item?.qty_ordered || 0)
//       itemId.push(item?.sku || 'None')
//       itemBrand.push(item.brand || 'None')
//       itemColour.push(item?.attributes?.[0]?.attribute_value ?? 'Mix')
//       stockAvailable.push(item.max_qty !== 0)
//       discount.push(isDiscount)
//       productLabel.push(handleGetProductLabel(item.label))
//       existProductLabel.push(handleGetProductLabel(item.label, 'bool'))
//     })

//     let data = {
//       'Item Name': itemName,
//       'Item Category LV 1': itemCategory1,
//       'Item Category LV 2': itemCategory2,
//       'Item Category LV 3': itemCategory3,
//       'Item Category LV 4': itemCategory4,
//       'Item Price': itemPrice,
//       'Item Quantity': itemQty,
//       'Item ID': itemId,
//       'Item Brand': itemBrand,
//       'Item Colour': itemColour,
//       'Stock Available?': stockAvailable,
//       'Discount?': discount,
//       'Product Label': productLabel,
//       'Product Label?': existProductLabel
//     }

//     if (additionalProperty) {
//       data = {
//         ...data,
//         ...additionalProperty
//       }
//     }

//     return data
//   }
// }

// export const mixpanelTrackCartItem = (items, additionalProperty) => {
//   if (items && items.length > 0) {
//     items = items?.forEach(item => {
//       let isDiscount = false

//       if (items?.prices?.normal_price && items?.prices?.selling_price) {
//         isDiscount = (items?.prices?.normal_price !== items?.prices?.selling_price)
//       }

//       let data = {
//         'Item Name': item.name || 'None',
//         'Item Category LV 1': item?.breadcrumbs?.[0] ?? 'None',
//         'Item Category LV 2': item?.breadcrumbs?.[1] ?? 'None',
//         'Item Category LV 3': item?.breadcrumbs?.[2] ?? 'None',
//         'Item Category LV 4': item?.breadcrumbs?.[3] ?? 'None',
//         'Item Price': item?.prices?.selling_price ?? 0,
//         'Item Quantity': item.qty_ordered || 0,
//         'Item ID': item.sku || 'None',
//         'Item Brand': item.brand || 'None',
//         'Item Colour': item?.attributes?.[0]?.attribute_value ?? 'Mix',
//         'Stock Available?': item.max_qty !== 0,
//         'Discount?': isDiscount,
//         'Product Label': handleGetProductLabel(item.label),
//         'Product Label?': handleGetProductLabel(item.label, 'bool'),
//         Subtotal: item.qty_ordered * item.prices.selling_price
//       }

//       if (additionalProperty) {
//         data = {
//           ...data,
//           ...additionalProperty
//         }
//       }

//       return data
//     })
//   }
// }

// const handleGetProductLabel = (label, type = '') => {
//   if (type === '') {
//     return (label && label.ODI !== '')
//       ? (label.ODI.split('|')[0] || 'Buy 1 Get 1')
//       : 'None'
//   } else {
//     return label && label.ODI !== ''
//   }
// }

const handleStoreAvailable = (product) => {
  let brandStoreIds =
    product?.store_available?.map(({ brand_store_id }) => brand_store_id) ?? []
  return brandStoreIds?.length > 0 ? brandStoreIds : ['None']
}
