import config from '../../../config'

const getAllPromotionLink = (companyCode) => {
  // /ace & /informa changed into /acestore & /informastore based on SEO team request
  const ace = `https://www.ruparupa.com/azko/semua-promosi?board+favorite+category+azko+${config.environment}=semua+promosi` // ACE_HARDWARE
  const informa = `https://www.ruparupa.com/informastore/semua-promosi?board+favorite+category+informa+${config.environment}=semua+promosi`
  const toyskingdomonline = `https://www.ruparupa.com/toyskingdomonline/page/semua-promo?board+favorite+category+toyskingdomonline+${config.environment}=semua+promosi`
  const informaB2b = `https://www.ruparupa.com/informab2b/semua-promosi?board+favorite+category+informa+${config.environment}=semua+promosi`

  const url = `${config?.baseURL}page/semua-promo?board+favorite+category+`
  const ruparupa = `${url}${config.environment}=promo+hari+ini`
  // UNCOMMENT THIS AFTER SEMUA-PROMO PAGE FOR BU IS READY
  // // /ace & /informa changed into /acestore & /informastore based on SEO team request
  // const ace = `${url}ace+${config.environment}=semua+promosi`
  // const informa = `${url}informa+${config.environment}=semua+promosi`
  // const toyskingdomonline = `${url}toyskingdomonline+${config.environment}=semua+promosi`
  // const informaB2b = `${url}informa+${config.environment}=semua+promosi`
  switch (companyCode) {
    case 'AHI':
      return ace // ACE_HARDWARE
    case 'HCI':
      if (config.isB2b) {
        return informaB2b
      }
      return informa
    case 'TGI':
      return toyskingdomonline
    default:
      return ruparupa
  }
}

export default getAllPromotionLink
