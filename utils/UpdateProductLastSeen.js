import dayjs from 'dayjs'
import Cookies from 'js-cookie'
import localforage from 'localforage'
import { globalAPI } from '../services/IndexApi'
import FilterProductLastSeen from './FilterProductLastSeen'

const checkIsExist = (arr, val) => {
  return arr?.some((product) => {
    return product?.variants?.[0]?.sku === val
  })
}

const removeExistProduct = (arr = [], val) => {
  return arr.filter((product) => {
    return product?.variants?.[0]?.sku !== val
  })
}

const addProductLastSeen = (
  isExist,
  productDetail,
  lastSeen = [],
  sku = '',
) => {
  let params = lastSeen
  if (params && !Array.isArray(params)) {
    params = [params]
  }

  if (isExist) {
    params = removeExistProduct(params, sku)
  }
  params = [productDetail, ...params].slice(0, 12)

  if (params) {
    localforage.setItem('product_last_seenv3', JSON.stringify(params))
  }
}

// Logic Based on RRTIC-80 => Product Last Seen Sagas frontend-shared-src

const updateProductLastSeen = async (productDetail) => {
  const productLastSeen = await localforage.getItem('product_last_seenv3')
  let lastSeen = JSON.parse(productLastSeen) || []
  const productDetailLastSeen = {
    variants: [
      {
        sku: productDetail.variants?.[0].sku,
        label: productDetail?.variants?.[0]?.label ?? '',
        images: productDetail.variants?.[0].images,
        prices: productDetail.variants?.[0].prices,
        price_zone: productDetail.variants?.[0].price_zone,
        created_at: productDetail.variants?.[0].created_at,
      },
    ],
    breadcrumb_last_seen_ODI: productDetail?.breadcrumb_last_seen?.ODI,
    breadcrumb_last_seen_AHI: productDetail?.breadcrumb_last_seen?.AHI,
    breadcrumb_last_seen_HCI: productDetail?.breadcrumb_last_seen?.HCI,
    breadcrumb_last_seen_TGI: productDetail?.breadcrumb_last_seen?.TGI,
    brand: productDetail?.brand,
    name: productDetail?.name,
    url_key: productDetail?.url_key,
    company_code: productDetail?.company_code,
    created_at: productDetail?.created_at,
    events: productDetail?.events,
    checkFlashSale: productDetail?.checkFlashSale,
    checkDoubleDate: productDetail?.checkDoubleDate,
    ribbonUrl: productDetail?.ribbonUrl,
    tahuRibbonUrl: productDetail?.tahuRibbonUrl,
    supplier: {
      supplier_alias: productDetail?.supplier?.supplier_alias,
    },
    minimumOrder: productDetail?.minimum_order,
  }
  let isExist = true
  if (productDetail && productDetail?.is_in_stock === 1) {
    isExist =
      lastSeen?.length > 0
        ? checkIsExist(lastSeen, productDetail?.variants?.[0]?.sku)
        : false
  }

  lastSeen = FilterProductLastSeen(lastSeen)

  addProductLastSeen(
    isExist,
    productDetailLastSeen,
    lastSeen,
    productDetail?.variants?.[0]?.sku,
  )
}

export const checkIsLastSeenValid = async (products = []) => {
  try {
    products = products.slice(0, 12)
    const now = dayjs()
    const lastSeenIsValid = Cookies.get('last-seen-is-valid-v2')
    let newLastSeen = products

    // ? Price will be updated once a day, and after 10 am
    if (!lastSeenIsValid && now.hour() >= 10) {
      newLastSeen = await Promise.all(
        products
          .map(async (product) => {
            if (product) {
              const keyword = product.variants[0]?.sku
              const response = await globalAPI.getProductByKeyword(keyword)
              if (response.status === 200 && response?.data?.data?.products) {
                return {
                  variants: [
                    {
                      sku: response?.data?.data?.products[0]?.variants?.[0]
                        ?.sku,
                      label:
                        response?.data?.data?.products[0]?.variants?.[0]
                          ?.label ?? '',
                      images:
                        response?.data?.data?.products[0]?.variants?.[0]
                          ?.images,
                      prices:
                        response?.data?.data?.products[0]?.variants?.[0]
                          ?.prices,
                      price_zone:
                        response?.data?.data?.products[0]?.variants?.[0]
                          ?.price_zone,
                      created_at:
                        response?.data?.data?.products[0]?.variants?.[0]
                          ?.created_at,
                    },
                  ],
                  name: response?.data?.data?.products?.[0]?.name,
                  url_key: response?.data?.data?.products?.[0]?.url_key,
                  company_code:
                    response?.data?.data?.products?.[0]?.company_code,
                  breadcrumb_last_seen_ODI:
                    response?.data?.data?.products?.[0]
                      ?.breadcrumb_last_seen_ODI,
                  breadcrumb_last_seen_AHI:
                    response?.data?.data?.products?.[0]
                      ?.breadcrumb_last_seen_AHI,
                  breadcrumb_last_seen_HCI:
                    response?.data?.data?.products?.[0]
                      ?.breadcrumb_last_seen_HCI,
                  breadcrumb_last_seen_TGI:
                    response?.data?.data?.products?.[0]
                      ?.breadcrumb_last_seen_TGI,
                  supplier: {
                    supplier_alias:
                      response?.data?.data?.products?.[0]?.supplier
                        ?.supplier_alias,
                  },
                  minimumOrder:
                    response?.data?.data?.products?.[0].minimum_order,
                }
              }
            } else {
              return null
            }
          })
          .filter((product) => product),
      )
      const cookiesExpire = new Date() // set cookies expire time to midnight same day
      cookiesExpire.setHours(23, 59, 59, 0)
      Cookies.set('last-seen-is-valid-v2', true, { expires: cookiesExpire })
    }
    return newLastSeen
  } catch (err) {
    console.log(err)
  }
}

export default updateProductLastSeen
