export const warrantyDataModifier = (
  data,
  customerId,
  warrantyCardActive,
  transactionType,
  itemData,
) => {
  if (warrantyCardActive) {
    if (transactionType === 'online') {
      return {
        customer_id: customerId, // customer ID login (auth iD)
        sales_invoice_item_id: itemData?.sales_invoice_item_id, // ada dari luar, nanti cek aja
        warranty_number: data?.warranty_card,
      }
    }
    return {
      customer_id: customerId, // customer ID login (auth iD)
      receipt_id: itemData?.receipt_id_and_sku?.split('-')[0] || '',
      warranty_number: data?.warranty_card,
      sku: itemData?.sku_number,
    }
  } else {
    if (transactionType === 'online') {
      return {
        customer_id: customerId, // customer ID login (auth iD)
        sales_invoice_item_id: itemData?.sales_invoice_item_id, // ada dari luar, nanti cek aja
        serial_number: data?.serial_number,
      }
    }
    return {
      customer_id: customerId, // customer ID login (auth iD)
      receipt_id: itemData?.receipt_id_and_sku?.split('-')[0] || '',
      serial_number: data?.serial_number,
      sku: itemData?.sku_number,
    }
  }
}
