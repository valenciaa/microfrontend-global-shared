const getModifiedGeolocation = (geolocation) => {
  const preprocessedGeolocation = geolocation?.split(',')

  const geolocationData = {
    latitude: geolocation ? preprocessedGeolocation?.[0] : '',
    longitude: geolocation ? preprocessedGeolocation?.[1] : '',
  }

  return geolocationData
}

export default getModifiedGeolocation
