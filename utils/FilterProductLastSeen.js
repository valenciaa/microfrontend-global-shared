import config from '../../../config'

export default function FilterProductLastSeen(lastSeen) {
  const isNeedFilterLastSeenProduct = lastSeen?.find((product) =>
    config.productLastSeenSkuBlackList?.includes(product?.variants?.[0]?.sku),
  )
  if (isNeedFilterLastSeenProduct) {
    lastSeen = lastSeen?.filter(
      (product) =>
        !config.productLastSeenSkuBlackList?.includes(
          product?.variants?.[0]?.sku,
        ),
    )
  }

  return lastSeen
}
