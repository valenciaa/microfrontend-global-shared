// https://stackoverflow.com/a/20392392

const TryParseJSONObject = (jsonString) => {
  try {
    const o = JSON.parse(jsonString)
    if (o && typeof o === 'object') {
      return o
    }
  } catch (e) {
    console.log('error try parse JSON: ', e)
  }
  return false
}

export default TryParseJSONObject
