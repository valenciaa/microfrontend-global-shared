const getReviewRatingDetailParam = (id, isReviewed) => {
  let params = {
    reviewed: isReviewed === 10 ? 1 : 0,
  }

  if (id?.includes('INV')) {
    params = {
      ...params,
      invoice_no: id,
    }
  } else {
    params = {
      ...params,
      receipt_id: id,
    }
  }

  return params
}

export default getReviewRatingDetailParam
