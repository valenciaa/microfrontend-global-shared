import dayjs from 'dayjs'

export const MONTH_LIST = [
  {
    label: 'Januari',
    value: 1,
  },
  {
    label: 'Februari',
    value: 2,
  },
  {
    label: 'Maret',
    value: 3,
  },
  {
    label: 'April',
    value: 4,
  },
  {
    label: 'Mei',
    value: 5,
  },
  {
    label: 'Juni',
    value: 6,
  },
  {
    label: 'Juli',
    value: 7,
  },
  {
    label: 'Augustus',
    value: 8,
  },
  {
    label: 'September',
    value: 9,
  },
  {
    label: 'Oktober',
    value: 10,
  },
  {
    label: 'November',
    value: 11,
  },
  {
    label: 'December',
    value: 12,
  },
]

export const setYearList = (maxYear, minYear) => {
  let years = []
  let maxYearNumber = maxYear.year()
  for (let i = maxYearNumber; i >= minYear; i--) {
    years.unshift({
      value: i,
      label: i,
    })
  }
  return years
}

export const setLabel = (chosenValue, options = [], defaultLabel) => {
  if (!chosenValue) {
    return defaultLabel
  }

  const dateData = options.find((el) => el.value === chosenValue)
  if (!dateData) {
    return defaultLabel
  }

  return dateData.label
}

export const setDayList = (chosenMonth, chosenYear, maxDate, minDate) => {
  let dayInAMonth = setMaxDaysInMonth(chosenMonth, chosenYear)
  let minDay = 1
  if (maxDate) {
    const maxYear = maxDate.year()
    const maxMonth = maxDate.month()

    if (maxYear === chosenYear && maxMonth + 1 === chosenMonth) {
      dayInAMonth = maxDate.date()
    }
  }

  if (minDate) {
    const minYear = minDate.year()
    const minMonth = minDate.month()

    if (minYear === chosenYear && minMonth + 1 === chosenMonth) {
      minDay = minDate.date()
    }
  }

  let days = []
  for (let i = minDay; i <= dayInAMonth; i++) {
    days.push({
      value: i,
      label: `${i < 10 ? '0' : ''}${i}`,
    })
  }
  return days
}

export const setMaxDaysInMonth = (month, year) => {
  let maxDay = 31
  let chosenYear = year ? Number(year) : dayjs().year()
  let monthWith30Days = [4, 6, 9, 11]

  // ? FOR FEBRUARY
  if (month && month === 2) {
    maxDay =
      (chosenYear % 4 == 0 && chosenYear % 100 != 0) || chosenYear % 400 == 0
        ? 29
        : 28
  } else if (month && monthWith30Days.includes(month)) {
    maxDay = 30
  }

  return maxDay
}

export const setMonthList = (maxDate, minDate, chosenYear) => {
  if (maxDate) {
    const maxYear = maxDate.year()

    if (maxYear === chosenYear) {
      const maxMonth = maxDate.month()
      return MONTH_LIST.slice(0, maxMonth + 1)
    }
  }

  if (minDate) {
    const minYear = minDate.year()

    if (minYear === chosenYear) {
      const minMonth = minDate.month()
      return MONTH_LIST.slice(minMonth)
    }
  }

  return MONTH_LIST
}
