const UtilSplitWord = (word) => {
  const fullWord = word.trim().split(' ')
  const firstWord = fullWord?.[0] || ''
  let lastWord = ''

  if (fullWord?.length > 1) {
    for (let i = 1; i < fullWord.length; i++) {
      lastWord += `${fullWord[i]} `
    }

    lastWord = lastWord.trim()
  }

  return { firstWord, lastWord }
}

export default UtilSplitWord
