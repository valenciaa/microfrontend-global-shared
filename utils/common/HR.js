export default function HR({ background = 'bg-grey-20', height = 'h-2xs' }) {
  return <div className={`${background} w-full ${height}`}></div>
}
