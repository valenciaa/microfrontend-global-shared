import isEmpty from 'lodash/isEmpty'

export const CheckCartItem = (cartData, cards) => {
  if (!isEmpty(cartData) && !isEmpty(cards?.variants)) {
    if (cartData.find((e) => e.sku === cards?.variants?.[0]?.sku)) {
      return 1
    } else {
      return cards.minimum_order
    }
  } else {
    return cards.minimum_order
  }
}

export const calculateCartItem = (
  cartData,
  activeVariant,
  quantity,
  setToggleErrorClipboardInfobox,
  setErrMessage,
  bundling,
) => {
  const findSpecificSku = cartData.find((e) => e.sku === activeVariant?.sku)

  if (!isEmpty(bundling?.sku_detail)) {
    for (const product of bundling.sku_detail) {
      const findSpecificSkuInBundling = cartData?.filter(
        (data) => data?.sku === product?.sku,
      )
      if (!isEmpty(findSpecificSkuInBundling)) {
        let stock = findSpecificSkuInBundling?.[0]?.max_qty
        for (const exist of findSpecificSkuInBundling) {
          stock = stock - exist?.qty_ordered
        }
        if (stock < 0) {
          setErrMessage(
            () => 'Seluruh stok sudah di keranjangmu',
            setToggleErrorClipboardInfobox(true),
          )
          return 0
        }
      }
    }
  }

  if (findSpecificSku) {
    if (findSpecificSku?.qty_ordered + quantity > findSpecificSku?.max_qty) {
      setErrMessage(
        () => 'Seluruh stok sudah di keranjangmu',
        setToggleErrorClipboardInfobox(true),
      )
      return 0
    }
  }
  return quantity
}
