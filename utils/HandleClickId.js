import dayjs from 'dayjs'
import Cookies from 'js-cookie'

export const handleGclidData = () => {
  let gclidSession = Cookies.get('gclid-session')

  if (gclidSession) {
    gclidSession = JSON.parse(gclidSession)

    const gclid = {
      identifier: gclidSession.identifier,
      valid_at: 'invoice_created',
      platform: 'Google',
      expired_at: dayjs(gclidSession.expires).format('YYYY-MM-DD HH:mm:ss'),
    }

    return gclid
  }

  return undefined
}

export const handleFbclidData = () => {
  let fbclidSession = Cookies.get('fbclid-session')

  if (fbclidSession) {
    fbclidSession = JSON.parse(fbclidSession)

    const fbclid = {
      identifier: fbclidSession.identifier,
      valid_at: 'invoice_created',
      platform: 'Facebook',
      expired_at: dayjs(fbclidSession.expires).format('YYYY-MM-DD HH:mm:ss'),
    }

    return fbclid
  }

  return undefined
}

export const handleTtclidData = () => {
  let ttclidSession = Cookies.get('ttclid-session')

  if (ttclidSession) {
    ttclidSession = JSON.parse(ttclidSession)

    const ttclid = {
      identifier: ttclidSession.identifier,
      valid_at: 'invoice_created',
      platform: 'Tiktok',
      expired_at: dayjs(ttclidSession.expires).format('YYYY-MM-DD HH:mm:ss'),
    }

    return ttclid
  }

  return undefined
}
