const PREV_PATH = 'prev_path'

export const saveCurrentPath = () => {
  window.sessionStorage.setItem(PREV_PATH, window.location.toString())
}

export const removePrevPath = () => {
  window.sessionStorage.removeItem(PREV_PATH)
}

export const getPrevPath = () => {
  return window.sessionStorage.getItem(PREV_PATH)
}
