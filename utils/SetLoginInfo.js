import Cookies from 'js-cookie'
import localforage from 'localforage'
import isEmpty from 'lodash/isEmpty'
import config from '../../../config'

export const setLoginInfo = async (access_token, userData, b2bcData) => {
  try {
    await localforage.removeItem('payment_cart_id')
    await localforage.setItem('access_token', access_token)
    Cookies.set('access_token', access_token, { expires: 365 })

    // ? CONTOH STRINGIFY APP 2.0
    // ? localforage.setItem('persist:ruparupa', JSON.stringify({ auth: JSON.stringify({ user: data }) }))
    if (userData) {
      await localforage.setItem(
        'persist:ruparupa',
        JSON.stringify({
          auth: JSON.stringify({
            user: {
              ...userData,
            },
            // ? isKLK harus di luar user
            isKlk: true,
          }),
        }),
      )
    }
    if (userData && userData.minicart_id) {
      await localforage.setItem('minicart_id', userData.minicart_id)
    }

    // ? check b2bc data
    if (!isEmpty(b2bcData)) {
      await localforage.setItem('b2bc', JSON.stringify(b2bcData))
      Cookies.set('b2bc', JSON.stringify(b2bcData))
      Cookies.set('redirect', config.baseURL + b2bcData.path)
    }

    if (isEmpty(userData.email) || !userData.is_email_verified) {
      const addParam = isEmpty(userData.email)
        ? { action: 'change-email', newEmail: '' }
        : { action: 'verify-email' }
      const param = {
        customerId: userData.customer_id,
        email: userData.email,
        phone: userData.phone,
        ...addParam,
      }
      await localforage.setItem('htua_pto2', JSON.stringify(param))
      // const params = isEmpty(userData.email) ? 'otp-choices' : 'verify-confirm&channel=email'
      //! matikan dulu pindah ke halaman verifynya
      // window.location.href = config.baseURL + 'verification?page=' + params
    }
  } catch (err) {
    console.log(err, 'SET LOGIN INFO ERROR')
  }
}
