const bankAccountOtpMethodText = {
  wa: 'WhatsApp',
  email: 'Email',
}

const bankAccountOtpChangeMethodText = {
  wa: 'Email',
  email: 'WhatsApp',
}

export { bankAccountOtpMethodText, bankAccountOtpChangeMethodText }
