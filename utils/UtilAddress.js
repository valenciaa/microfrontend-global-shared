import Cookies from 'js-cookie'
import localforage from 'localforage'
import isEmpty from 'lodash/isEmpty'
import { SHIPPING_LOCATION } from './constants'
import getStorageShippingLocationData from './GetStorageShippingLocationData'
import UpperCase from './UpperCase'
import UtilSplitWord from './UtilSplitWord'

const handleAddressAddDefaultValue = () => {
  return {
    address_name: '',
    city: {
      city_id: '',
      city_name: '',
    },
    first_name: '',
    full_address: '',
    geolocation: '',
    kecamatan: {
      kecamatan_id: '',
      kecamatan_name: '',
    },
    kelurahan: {
      kelurahan_id: '',
      kelurahan_name: '',
      post_code: '',
    },
    phone: '',
    province: {
      province_id: '',
      province_name: '',
    },
  }
}

const handleAddressEditDefaultValue = (selected) => {
  return {
    address_name: selected?.address_name || '',
    city: {
      city_id: selected?.city?.city_id || '',
      city_name: selected?.city?.city_name || '',
    },
    first_name: `${selected?.first_name}${
      selected?.last_name ? ` ${selected?.last_name}` : ''
    }`,
    full_address: selected?.full_address || '',
    geolocation: selected?.geolocation || '',
    kecamatan: {
      kecamatan_id: selected?.kecamatan?.kecamatan_id || '',
      kecamatan_name: selected?.kecamatan?.kecamatan_name || '',
    },
    kelurahan: {
      kelurahan_id: selected?.kelurahan?.kelurahan_id || '',
      kelurahan_name: selected?.kelurahan?.kelurahan_name || '',
      post_code: selected?.kelurahan?.post_code || '',
    },
    phone: selected?.phone?.replace(/^08/, '8'),
    province: {
      province_id: selected?.province?.province_id || '',
      province_name: selected?.province?.province_name || '',
    },
  }
}

const handleAddressAddPayload = (data, withGeo, isFirstAddress) => {
  const fullName = UtilSplitWord(data.first_name)

  const payload = {
    address_name: data?.address_name?.trim(),
    first_name: fullName.firstWord,
    last_name: fullName.lastWord,
    phone: `0${data?.phone}`,
    full_address: data?.full_address,
    province: {
      province_id: data?.province?.province_id,
    },
    city: {
      city_id: data?.city?.city_id,
    },
    kecamatan: {
      kecamatan_id: data?.kecamatan?.kecamatan_id,
    },
    kelurahan: {
      kelurahan_id: data?.kelurahan?.kelurahan_id,
    },
    post_code: data?.kelurahan?.post_code,
  }

  if (withGeo || data?.geolocation === '') {
    payload.geolocation = data.geolocation
  }

  if (isFirstAddress) {
    payload.is_default = 1
  }

  return payload
}

const handleAddressEditPayload = (data, withGeo, selectedId) => {
  let payload = handleAddressAddPayload(data, withGeo, false)

  payload = {
    ...payload,
    address_id: selectedId,
    customer_address_id: selectedId,
  }

  return payload
}

const handleAddressProvinceName = (province) => {
  return province?.province_id === '490' ||
    parseInt(province?.province_id) === 490
    ? 'DKI Jakarta'
    : UpperCase(province?.province_name)
}

const handleAddressTemplate = (address) => {
  return `${address?.full_address},${
    address?.kelurahan?.kelurahan_name
      ? ` ${UpperCase(address?.kelurahan?.kelurahan_name)},`
      : ''
  }${
    address?.kecamatan?.kecamatan_name
      ? ` ${UpperCase(address?.kecamatan?.kecamatan_name)},`
      : ''
  } ${UpperCase(address?.city?.city_name)}, ${handleAddressProvinceName(
    address?.province,
  )}${address?.post_code ? `, ${UpperCase(address?.post_code)}` : ''}`
}

const handleAddressMiscAllSelected = (watch, withKelurahan = true) => {
  if (
    watch('province.province_name') &&
    watch('city.city_name') &&
    watch('kecamatan.kecamatan_name') &&
    withKelurahan &&
    watch('kelurahan.kelurahan_name')
  ) {
    return true
  }

  if (
    watch('province.province_name') &&
    watch('city.city_name') &&
    watch('kecamatan.kecamatan_name') &&
    !withKelurahan
  ) {
    return true
  }

  return false
}

const handleAddressMiscAnySelected = (watch) => {
  return watch('province.province_name') ||
    watch('city.city_name') ||
    watch('kecamatan.kecamatan_name') ||
    watch('kelurahan.kelurahan_name')
    ? true
    : false
}

const handleAddressMiscTemplate = (watch) => {
  return `${handleAddressProvinceName(watch('province'))}, ${UpperCase(
    watch('city.city_name'),
  )}${
    watch('kecamatan.kecamatan_name')
      ? `, ${UpperCase(watch('kecamatan.kecamatan_name'))}`
      : ''
  }${
    watch('kelurahan.kelurahan_name') && watch('kelurahan.post_code')
      ? `, ${UpperCase(watch('kelurahan.kelurahan_name'))} - ${watch(
          'kelurahan.post_code',
        )}`
      : ''
  }`
}

const handleAddressResetMiscField = (
  resetField,
  province = false,
  city = false,
  kecamatan = false,
  kelurahan = false,
) => {
  if (province) {
    resetField('province.province_id')
    resetField('province.province_name')
  }

  if (city) {
    resetField('city.city_id')
    resetField('city.city_name')
  }

  if (kecamatan) {
    resetField('kecamatan.kecamatan_id')
    resetField('kecamatan.kecamatan_name')
  }

  if (kelurahan) {
    resetField('kelurahan.kelurahan_id')
    resetField('kelurahan.kelurahan_name')
    resetField('kelurahan.post_code')
    resetField('kelurahan.geolocation')
  }
}

const handleAddressMiscIsChange = (watch, selected) => {
  return (
    watch('province.province_name') !== selected?.province?.province_name ||
    watch('city.city_name') !== selected?.city?.city_name ||
    watch('kecamatan.kecamatan_name') !== selected?.kecamatan?.kecamatan_name ||
    (watch('kelurahan.kelurahan_name') &&
      watch('kelurahan.kelurahan_name') !== selected?.kelurahan?.kelurahan_name)
  )
}

const handleAddressGeoIsChange = (watch, selected) => {
  return watch('geolocation') !== selected?.geolocation
}

const handleCurrentAdministrativeLevel = (data, administrativeLevel) => {
  if (
    data?.toLowerCase()?.split(' ')?.length >= 2 &&
    data?.toLowerCase()?.split(' ')?.[0] === administrativeLevel
  ) {
    return data
      .toLowerCase()
      .split(' ')
      .filter((element) => element !== administrativeLevel)
      .join(' ')
  }

  if (
    data?.toLowerCase()?.split(' ')?.length === 1 ||
    data?.toLowerCase()?.split(' ')?.length >= 2
  ) {
    return data.toLowerCase()
  }

  return data
}

const handleAddressInputLimit = (input, maxLength) => {
  const el = document.getElementById(input)

  if (el?.value?.length > maxLength) {
    el.value = el.value.slice(0, maxLength)
  }
}

const handleCheckShippingLocation = async (selectedAddressId, addressList) => {
  const shippingLocation = await getStorageShippingLocationData()

  if (!shippingLocation) {
    return
  }

  const addressLocation = JSON.parse(shippingLocation)

  if (addressLocation.address_id !== selectedAddressId) {
    return
  }

  const filteredAddress = addressList?.filter(
    (el) => el.address_id !== selectedAddressId,
  )

  if (!isEmpty(filteredAddress)) {
    const primaryAddress = filteredAddress?.find((el) => el.is_default === '1')

    if (primaryAddress) {
      const primaryAddressGeo =
        primaryAddress?.geolocation || primaryAddress?.kelurahan?.geolocation
      const splitGeo = primaryAddressGeo?.split(',')
      const cookiesGeo = {
        latitude: splitGeo[0],
        longitude: splitGeo[1],
      }

      const storageDataLocation = {
        full_address: primaryAddress?.full_address,
        address_name: primaryAddress?.address_name,
        geolocation: primaryAddressGeo,
      }

      localStorage.setItem(
        'SHIPPING_LOCATION',
        JSON.stringify(storageDataLocation),
      )
      localforage.setItem(SHIPPING_LOCATION, JSON.stringify(primaryAddress))
      Cookies.set(SHIPPING_LOCATION, JSON.stringify(cookiesGeo))

      return
    }

    const firstIndexAddressListData = filteredAddress[0]

    const firstIndexAddressListGeo =
      firstIndexAddressListData?.geolocation ||
      firstIndexAddressListData?.kelurahan?.geolocation
    const splitGeo = firstIndexAddressListGeo?.split(',')
    const cookiesGeo = {
      latitude: splitGeo[0],
      longitude: splitGeo[1],
    }

    const storageDataLocation = {
      full_address: firstIndexAddressListData?.full_address,
      address_name: firstIndexAddressListData?.address_name,
      geolocation: firstIndexAddressListGeo,
    }

    localStorage.setItem(
      'SHIPPING_LOCATION',
      JSON.stringify(storageDataLocation),
    )

    localforage.setItem(
      SHIPPING_LOCATION,
      JSON.stringify(firstIndexAddressListData),
    )

    Cookies.set(SHIPPING_LOCATION, JSON.stringify(cookiesGeo))
  }
}

const handleAddressReceiverNameTemplate = (firstName, lastName) => {
  if (lastName) {
    return firstName + ' ' + lastName
  }

  return firstName
}

export {
  handleAddressAddDefaultValue,
  handleAddressAddPayload,
  handleAddressEditDefaultValue,
  handleAddressEditPayload,
  handleAddressGeoIsChange,
  handleAddressInputLimit,
  handleAddressMiscAllSelected,
  handleAddressMiscAnySelected,
  handleAddressMiscIsChange,
  handleAddressMiscTemplate,
  handleAddressProvinceName,
  handleAddressReceiverNameTemplate,
  handleAddressResetMiscField,
  handleAddressTemplate,
  handleCheckShippingLocation,
  handleCurrentAdministrativeLevel,
}
