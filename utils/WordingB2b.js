import dayjs from 'dayjs'

const currentDate = dayjs()
const lastDateInYear = dayjs('2024-12-31 23:59:59')

const isNewLogoApplied = currentDate.isAfter(lastDateInYear)

const AHI_B2B = isNewLogoApplied ? 'AZKO' : 'ACE'

const AHI_B2B_LOGO =
  'https://cdn.ruparupa.io/ahoi/website/Desktop/RRD_HeaderAZKOOnline.webp'

export { AHI_B2B, AHI_B2B_LOGO }
