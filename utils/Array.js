/**
 * digunakan untuk mencari data dalam array
 * ex:
 * fruits = ['apple', 'banana', 'grapes', 'mango', 'orange']
 * filterItems(fruits, 'ang') // ['mango', 'orange']
 *
 * @param {array} data
 * @param {string} query
 * @returns {array}
 */
export const filterItems = (data, query, key = null) => {
  const q = query?.toLowerCase()
  return data?.filter((item) => {
    const it = key ? item[key] : item
    return it?.toLowerCase().indexOf(q) >= 0
  })
}

/**
 * menghapus nilai object yg kosong
 * ex:
 * {a: 3, b:'', c:null} = {a:3}
 * @param {Object} obj
 * @returns {Object}
 */
export const removeObjectIsEmpty = (obj) => {
  const tmpObj = {}
  Object.keys(obj).forEach((key) => {
    if (obj[key]) {
      tmpObj[key] = obj[key]
    }
  })
  return tmpObj
}

export default {
  filterItems,
  removeObjectIsEmpty,
}
