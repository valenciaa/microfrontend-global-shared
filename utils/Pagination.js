export const NOTATION_SPACE = '...'

// Implementation in ES6
/**
 * thanks Farnaz Kakhsaz
 * @url https://gist.github.com/kottenator/9d936eb3e4e3c3e02598?permalink_comment_id=4188920#gistcomment-4188920
 *
 * @param   {[integer]}  currentPageNumber    {page sekarang}
 * @param   {[integer]}  totalPageNumber  {total page}
 * @param   {[integer]}  offset      {jarak antara page sekarang dengan '...'}
 *
 * @return  {[array]}     [return description]
 */
export const generatePaginate = (
  currentPageNumber,
  totalPageNumber,
  offset = 2,
) => {
  // By doing this, when we are close to the beginning or end of the pagination, two numbers are generated after/before the current page,
  // but when we are far from these points (in the middle of the pagination), we generate only one number after/before the current page.
  const offsetNumber =
    currentPageNumber <= offset || currentPageNumber > totalPageNumber - offset
      ? offset
      : offset - 1
  const numbersList = []
  const numbersListWithDots = []

  // If itemsPerPage is less than what the user selected with the Select component or if there is no page or only one page:
  if (totalPageNumber <= 1 || totalPageNumber === undefined) {
    return [1]
  }

  // Create list of numbers:
  numbersList.push(1)
  for (
    let i = currentPageNumber - offsetNumber;
    i <= currentPageNumber + offsetNumber;
    i++
  ) {
    if (i < totalPageNumber && i > 1) {
      numbersList.push(i)
    }
  }
  numbersList.push(totalPageNumber)

  // Add three dots to the list of numbers:
  numbersList.reduce((accumulator, currentValue) => {
    if (accumulator === 1) {
      numbersListWithDots.push(accumulator)
    }
    if (currentValue - accumulator !== 1) {
      numbersListWithDots.push(NOTATION_SPACE)
    }
    numbersListWithDots.push(currentValue)

    return currentValue
  })

  return numbersListWithDots
}
