const GetShopbackUrl = () => {
  const shopback = localStorage.getItem('shopbackTransactionId')
  return shopback ? '&shopbackTransactionId=' + window.btoa(shopback) : ''
}

export default GetShopbackUrl
