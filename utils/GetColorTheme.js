import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import { useConstantsContext } from '../context/ConstantsContext'

const GetColorTheme = (forceTheme) => {
  // forceTheme is a prop to force the color theme despite the config.companyNameCSS value
  // forceTheme is needed since there is a case that cannot use pageFrom below as validation (go see BannerCampaignApps.js for usage example)
  const { pageFrom } = useConstantsContext()

  let colorTheme = ''

  switch (pageFrom) {
    case 'static-hyde-living':
      colorTheme = 'hyde-living'
      break
    case 'static-interior-design':
      colorTheme = 'ruparupa'
      break
    default:
      colorTheme = config.companyNameCSS
  }

  if (!isEmpty(forceTheme)) {
    colorTheme = forceTheme
  }

  return colorTheme
}

export default GetColorTheme
