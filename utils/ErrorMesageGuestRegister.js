export const errorMesageGuestRegister = (successType) => {
  // BE send guest_equality which the value is represent which one that true,
  // so if the value is 'email' then email value of the prompt and the phone is wrong, so the error message will said that the phone is wrong
  // and it will send the hint of the right value to your whatsapp (mailbox for email)
  let errMsg = ''
  if (successType === 'email') {
    errMsg =
      'Nomor Telepon yang kamu masukkan tidak sesuai dengan nomor yang pernah kamu gunakan untuk berbelanja di ruparupa.<br/><br>Silahkan cek Email kamu untuk informasi nomor telepon yang terdaftar'
  } else if (successType === 'phone') {
    errMsg =
      'Email yang kamu masukkan tidak sesuai dengan Email yang pernah kamu gunakan untuk berbelanja di ruparupa.<br/><br>Silahkan cek pesan yang telah kami kirimkan melalui WhatsApp untuk informasi email yang terdaftar.'
  }

  if (errMsg !== '') {
    return { err: true, message: errMsg }
  } else {
    return { err: false, message: errMsg }
  }
}
