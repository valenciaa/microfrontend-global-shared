import { generateTrackingPayload, mixpanelTrack } from './MixpanelWrapper'
import isEmpty from 'lodash/isEmpty'

const getItmParam = (params, href) => {
  const regex = new RegExp('[\\?&]' + params + '=([^&#]*)')
  const results = regex.exec(href)
  return results === null
    ? null
    : decodeURIComponent(results[1].replace(/\+/g, '-'))
}

export const micrositeMixpanelGenerator = (
  mixpanelEvent,
  type,
  productName,
  itemId,
  micrositeName,
  href,
  additionalParams,
  product = null,
) => {
  const trackingBase = {
    'Item Name': productName || 'None',
    'Item ID': itemId || 'None',
  }

  /* inject itm attribute */
  let mixpanelItm = {
    ...trackingBase,
    'ITM Source':
      getItmParam('itm_source', href) &&
      getItmParam('itm_source', href) !== 'undefined'
        ? getItmParam('itm_source', href)
        : 'None',
    'ITM Term':
      getItmParam('itm_term', href) &&
      getItmParam('itm_term', href) !== 'undefined'
        ? getItmParam('itm_term', href)
        : 'None',
    'ITM Campaign':
      getItmParam('itm_campaign', href) &&
      getItmParam('itm_campaign', href) !== 'undefined'
        ? getItmParam('itm_campaign', href)
        : 'None',
  }

  /* additional attribute */
  let locationMicrosite = ''
  if (window.location.href.includes('/ms/inspirations-and-ideas')) {
    locationMicrosite = 'inspirations and ideas'
  } else if (window.location.href.includes('/ms')) {
    locationMicrosite = 'Microsite'
  } else {
    locationMicrosite = 'Homepage'
  }

  if (type === 'mini-block-promo') {
    const { bannerName } = additionalParams
    if (href === null) {
      mixpanelItm = {
        'Microsite Name': micrositeName,
        'Location': locationMicrosite,
        'Banner Name': bannerName || 'None',
      }
    } else {
      mixpanelItm = {
        ...mixpanelItm,
        'Microsite Name': micrositeName,
        'Location': locationMicrosite,
        'Banner Name': bannerName || 'None',
      }
    }
  }

  if (type === 'image') {
    if (additionalParams) {
      const { code, pageTitle } = additionalParams
      mixpanelItm = {
        'Voucher Name': code,
        'Voucher Code': code,
        'Code': code,
        'Page Title': pageTitle,
      }
    } else if (href === null) {
      mixpanelItm = {
        'Microsite Name': micrositeName,
        'Location': locationMicrosite,
      }
    } else {
      mixpanelItm = {
        ...mixpanelItm,
        'Microsite Name': micrositeName,
        'Location': locationMicrosite,
      }
    }
  }

  if (type === 'copy-voucher') {
    mixpanelItm = null /* reset itm data */
    const { voucherCode } = additionalParams

    mixpanelItm = {
      'Voucher Name': voucherCode,
      'Voucher Code': voucherCode,
      'Location': locationMicrosite,
      'Page Name': micrositeName,
    }

    if (!isEmpty(product)) {
      const handleStoreAvailable = (product) => {
        let brandStoreIds =
          product?.store_availableForGA?.map(
            ({ brand_store_id }) => brand_store_id,
          ) ?? []
        return brandStoreIds?.length > 0 ? brandStoreIds : ['None']
      }

      mixpanelItm = {
        ...mixpanelItm,
        'Brand Store Id': handleStoreAvailable(product),
      }
    }
  }
  if (type === 'inspiration') {
    const { activeMenu, bannerName, type, title } = additionalParams

    let additionalObj

    if (type === 'Category') {
      additionalObj = {
        'Category Title': title || 'None',
      }

      mixpanelItm = {
        ...mixpanelItm,
        ...additionalObj,
      }
    }

    mixpanelItm = {
      ...mixpanelItm,
      'Kategori Inspirasi': activeMenu || 'None',
      'Click Location': type,
      'Title': bannerName || 'None', // eslint-disable-line
      'Location': locationMicrosite,
    }
  }

  if (type === 'promo-brands') {
    const { inspiration, sectionName, itmType } = additionalParams

    let additionalObj

    if (itmType === 'promo-brand-banner') {
      additionalObj = {
        'Click Location': 'Banner',
        'Banner Title': inspiration || 'None',
        'Item ID': 'None',
        'Kategori Inspirasi': 'None',
        'Category Title': 'None',
        'Microsite Name': micrositeName,
        'Location': locationMicrosite,
      }
    } else {
      additionalObj = {
        'Location': locationMicrosite,
        'Section Name': sectionName,
        'Button Name': 'Lihat Semua',
        'Microsite Name': micrositeName || 'None',
      }
    }

    mixpanelItm = {
      ...mixpanelItm,
      ...additionalObj,
    }
  }

  if (type === 'category-recommendation') {
    const { title } = additionalParams

    mixpanelItm = {
      ...mixpanelItm,
      'Location': locationMicrosite,
      'Page Title': title || 'None',
    }
  }

  if (
    [
      'product-recommendation',
      'product-last-seen-microsite',
      'product-wishlist-microsite',
    ].includes(type)
  ) {
    const { tab, dataType } = additionalParams

    if (type === 'product-last-seen-microsite') {
      const { title } = additionalParams
      mixpanelItm = { ...mixpanelItm, 'Page Title': title || 'None' }
    }

    mixpanelItm = {
      ...mixpanelItm,
      'Click Location': 'Product',
      'Section Name': dataType || 'None',
      'Location': locationMicrosite,
      'Tab': tab || 'None', // eslint-disable-line
    }
  }

  if (
    [
      'promo-product',
      'promo-voucher',
      'copy-voucher',
      'inspiration',
      'promo-brand',
      'custom-inspiration',
      'product-recommendation',
      'product-last-seen-microsite',
      'product-wishlist-microsite',
      'product-wishlist-microsite',
    ].includes(type)
  ) {
    let dataType = ''
    if (type === 'promo-product') {
      dataType = 'Product'
    } else if (type === 'promo-voucher') {
      dataType = 'Product Voucher'
    }
    mixpanelItm = {
      ...mixpanelItm,
      'Type': dataType || 'None', // eslint-disable-line
      'Microsite Name': micrositeName,
    }
  }

  if (type === 'promo-brand') {
    mixpanelEvent = 'Belanja Mudah di Toko Official'
    const { storeName, bannerName } = additionalParams
    mixpanelItm = {
      ...mixpanelItm,
      'Toko': storeName || 'None', //eslint-disable-line
      'Click Location': 'Product',
      'Banner Title': bannerName || 'None',
      'Location': 'Microsite', // eslint-disable-line
    }
  }

  if (
    [
      'flash-sale',
      'daily-deals',
      'flash-sale-button',
      'daily-deals-button',
    ].includes(type)
  ) {
    const { promoName, section, buttonName } = additionalParams
    if (type === 'flash-sale-button' || type === 'daily-deals-button') {
      mixpanelItm = {
        ...mixpanelItm,
        'Section Name': section,
        'Button Name': buttonName,
        'Location': 'Microsite', //eslint-disable-line
      }
    } else {
      mixpanelItm = {
        ...mixpanelItm,
        'Promo Name': promoName || 'None',
        'Location': locationMicrosite,
        'TnC Clicked?': false,
        'See More Clicked?': false,
        'Voucher Copied?': false,
        'Voucher Name': 'None',
        'Voucher Code': 'None',
        'Click Location': 'Product',
      }
    }
  }

  if (['shopthelook', 'shopthelookTab'].includes(type)) {
    const { title, clickLocation } = additionalParams

    if (type === 'shopthelookTab') {
      mixpanelItm = null
    }

    mixpanelItm = {
      ...mixpanelItm,
      'Section Name': title,
      'Click Location': clickLocation,
      'Location': locationMicrosite,
    }
  }

  if (['cms-block'].includes(type)) {
    const { title, name, location } = additionalParams

    if (type === 'shopthelookTab') {
      mixpanelItm = null
    }
    mixpanelItm = {
      ...mixpanelItm,
      'Microsite Title': title || 'None',
      'Name': name || 'None', // eslint-disable-line
      'Location': location || 'None', // eslint-disable-line
    }
  }

  return mixpanelTrack(
    mixpanelEvent,
    generateTrackingPayload({ ...mixpanelItm }),
  )
}
