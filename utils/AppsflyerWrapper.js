import config from '../../../config'

// contoh data
// AF('pba', 'setCustomerUserId' , '663274')
// window.AF('pba', 'event', {eventType: 'EVENT', eventValue: {'category': 'holiday_promotion'}, eventName: 'checkout'})

export const appsflyerEventNames = {
  COMPLETE_REGISTRATION: 'af_complete_registration', // When a user completes registration as new Rupper
  LOGIN: 'af_login', // When a user successfully logs in
  SEARCH: 'af_search', // When the user clicks on the search button or when the user lands on the search results page
  CONTENT_VIEW: 'af_content_view', // When the user views a specific product details page (pdp)
  LIST_VIEW: 'af_list_view', // When the user views a PCP/specific list
  ADD_WISHLIST: 'af_add_to_wishlist', // When the user adds items to their wishlist
  ADD_CART: 'af_add_to_cart', // When the user adds a product to the cart
  INITIATED_CHECKOUT: 'af_initiated_checkout', // When the user initiates the checkout but didn't yet complete the checkout
  PURCHASE: 'af_purchase', // When the user lands on the thank you page after a successful purchase
  FIRST_PURCHASE: 'first_purchase', // When the user completes their first purchase
  REMOVE_CART: 'remove_from_cart', // When the user removes an item from the cart
}

export const appsflyerGenerateCategoriesString = (
  breadcrumbs,
  isChildObject = false,
) => {
  // expected value: ['Category1', 'Category2', 'Category3', 'Category4']
  if (isChildObject) {
    return (
      (breadcrumbs && breadcrumbs[0] ? breadcrumbs[0].name : '-') +
      '>' +
      (breadcrumbs && breadcrumbs[1] ? breadcrumbs[1].name : '-') +
      '>' +
      (breadcrumbs && breadcrumbs[2] ? breadcrumbs[2].name : '-') +
      '>' +
      (breadcrumbs && breadcrumbs[3] ? breadcrumbs[3].name : '-')
    )
  }
  return (
    (breadcrumbs && breadcrumbs[0] ? breadcrumbs[0] : '-') +
    '>' +
    (breadcrumbs && breadcrumbs[1] ? breadcrumbs[1] : '-') +
    '>' +
    (breadcrumbs && breadcrumbs[2] ? breadcrumbs[2] : '-') +
    '>' +
    (breadcrumbs && breadcrumbs[3] ? breadcrumbs[3] : '-')
  )
}

export const appsflyerSetCustomerID = (customerId) => {
  if (config.appsflyerWebKey) {
    window.AF('pba', 'setCustomerUserId', customerId)
  }
}

export const appsflyerTrackEvent = (name, values) => {
  if (config.appsflyerWebKey) {
    window.AF('pba', 'event', {
      eventType: 'EVENT',
      eventName: name,
      eventValue: values,
    })
  }
}
