import isEmpty from 'lodash/isEmpty'

/**
 * update params without reload page, e.g. for case filter/search
 * @param {object} router object router from userRouter()
 * @param {string} pathName get from router.pathname (path name without baseUrl)
 * @param {object} currentParams get from router.query (object form)
 * @param {object} overrideParams new params that will override the previous params
 */
export const changeParamUrl = (router, overrideParams) => {
  const pathName = router.pathname
  const newParams = router.query
  for (const key in overrideParams) {
    newParams[key] = overrideParams[key]
  }
  for (const key in newParams) {
    if (
      newParams[key] === '' ||
      (Array.isArray(newParams[key]) && isEmpty(newParams[key]))
    ) {
      delete newParams[key] // remove empty query param
    }
  }
  router.replace(
    {
      pathname: pathName,
      query: newParams,
    },
    null,
    { shallow: true },
  )
}
