import { formB2bMapper } from './FormB2b'

const initValueFormB2b = (accountData) => {
  const convertToObjectLevel1 = (data) => {
    return data.reduce(
      (obj, item) =>
        Object.assign(obj, {
          [item.name]:
            item?.value && typeof item.value === 'object'
              ? convertToObjectLevel2(item.value, 'name', 'checked')
              : item.value !== undefined
                ? item.value
                : '',
        }),
      {},
    )
  }

  const convertToObjectLevel2 = (data, nameKey = 'name', valueKey = '') => {
    return data.reduce(
      (obj, item) =>
        Object.assign(obj, {
          [item[nameKey]]: item[valueKey] ?? '',
        }),
      {},
    )
  }

  const data = {
    customerProfile: {
      ...convertToObjectLevel1(formB2bMapper.customerProfile),
      email: accountData?.email,
      companyName: accountData?.company_name,
      companyGroup: accountData?.company_group,
      address: accountData?.npwp_address,
      mobilePhone: accountData?.phone,
      ktpNo: accountData?.ktp_no,
      taxNumberType: accountData?.tax_number_type,
      nitkuNo: accountData?.nitku,
      npwpNumber: accountData?.npwp_no,
      npwpNumber2: accountData?.npwp_no_2,
      npwpAddress: accountData?.npwp_address,
      npwpFullname: accountData?.npwp_fullname,
      jobTitle: accountData?.job_title,
      firstName: accountData?.first_name,
      lastName: accountData?.last_name,
      isNpwpUsed: false,
    },
    billingInformation: convertToObjectLevel1(formB2bMapper.billingInformation),
    companyInformation: {
      ...convertToObjectLevel1(formB2bMapper.companyInformation),
      department1: 'Finance & Accounting',
      department2: 'Purchasing',
      department3: 'Owner',
      department4: 'Warehouse',
    },
  }

  return data
}

export default initValueFormB2b
