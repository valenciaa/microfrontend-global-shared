import dayjs from 'dayjs'

export const useDateVoucher = (expired, formatDate = 'DD MMMM YYYY') => {
  const expireDate = new Date(expired)
  const dateDiffInSeconds = (expireDate.getTime() - new Date().getTime()) / 1000
  const dateDiffInDays = Math.ceil(dateDiffInSeconds / 3600 / 24)
  const dateDiffInHours = Math.ceil(dateDiffInSeconds / 3600)

  let expire = ''
  let note = ''
  let highlight = true
  if (dateDiffInSeconds / 3600 < 1) {
    const dateDiffInMinutes = dateDiffInSeconds / 60
    expire = '1 jam'
    const thresholds = [30, 20, 10, 5, 4, 3, 2, 1]
    for (let i = 0; i < thresholds.length; i++) {
      const threshold = thresholds[i]
      if (dateDiffInMinutes <= threshold) {
        expire = `${threshold} menit`
      } else {
        break
      }
    }
  } else if (dateDiffInDays <= 1) {
    expire = `${dateDiffInHours} jam`
  } else if (dateDiffInDays <= 3) {
    expire = `${dateDiffInDays} hari`
  } else {
    expire = dayjs(expireDate).format(formatDate)
    highlight = false
  }

  note = dateDiffInDays > 3 ? 'Berlaku hingga' : 'Berakhir dalam'
  return { highlight, note, expire }
  // return `${note} ${expire}`
}
