import localforage from 'localforage'
import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'
import config from '../../../../config'
import { useGetMembership } from '../../services/api/queries/auth'
import { useGetCustomerMembershipAlt } from '../../services/api/queries/global'
import { useEffect } from 'react'

// ? callback is a function called after check klk is not logged in
export default function useCheckKLkUser(
  isEnabled = false,
  identifier,
  callbackNeedActivate = () => {},
  callbackSuccess = () => {},
  callbackError = () => {},
) {
  const { refetch: getUserProfileKLK } = useGetCustomerMembershipAlt({
    enabled: false,
    onSuccess: (data) => {
      if (!data.error) {
        const user = data.user
        const { sha256 } = require('js-sha256')
        const hashUser = user.customer_id
          ? sha256(user.customer_id.toString()).toString()
          : ''
        if (Cookies.get('algolia_ssr') !== hashUser && user) {
          if (hashUser) {
            Cookies.set('algolia_ssr', hashUser)
          }
        }

        localforage.setItem(
          'persist:ruparupa',
          JSON.stringify({
            auth: JSON.stringify({
              user: { ...data.user },
              isKlk: true,
            }),
          }),
        )

        callbackSuccess()
      }
    },
    onError: (error) => {
      if (error?.message?.toLowerCase() === 'token not valid') {
        localforage.clear()
        Cookies.remove('access_token')
        Cookies.remove('algolia_ssr')
        Cookies.remove('ugid')
        Cookies.remove('sessionrupaUID')
        Cookies.remove('rr-sid')
        Cookies.remove('geolocation')
        localStorage.removeItem('SHIPPING_LOCATION')
        window.location.href = config.baseURL
        return
      }
      if (callbackError) {
        callbackError()
      }
    },
  })

  const { refetch: refetchCheckAuth } = useGetMembership(identifier, {
    enabled: false,
    onSuccess: async (response) => {
      const {
        next_action: nextAction,
        legacy_member: legacyMember,
        identifier,
        data,
        input_type: inputType,
      } = response
      if (nextAction !== 'login') {
        localforage.setItem('identifier', identifier)
        if (legacyMember) {
          await localforage.setItem('legacy_member', legacyMember)
        }

        if (!isEmpty(data)) {
          const {
            email,
            member_id: memberId,
            phone,
            companyCode,
            imageUrl,
            merchant,
            memberToken,
            validationMethod,
          } = data
          localforage.setItem('userInfo', {
            email,
            phone,
            member_id: memberId,
            companyCode,
            imageUrl,
            merchant,
            memberToken,
            validationMethod,
          })
        }

        await localforage.removeItem('KLK_isDisabled')
        const klkIsDisabled = {
          isEmailDisabled: false,
          isPhoneDisabled: false,
        }
        if (inputType) {
          switch (inputType) {
            case 'email':
              klkIsDisabled.isEmailDisabled = true
              break
            case 'phone':
              klkIsDisabled.isPhoneDisabled = true
              break
            case 'both':
              klkIsDisabled.isEmailDisabled = true
              klkIsDisabled.isPhoneDisabled = true
              break
          }
        }
        await localforage.setItem('KLK_isDisabled', klkIsDisabled)
        Cookies.remove('track_activate') // make sure we don't have this cookie when activate from login situation
        callbackNeedActivate(nextAction)
      } else if (nextAction === 'login') {
        getUserProfileKLK()
      }
    },
    onError: async () => {
      if (callbackError) {
        callbackError()
      }
    },
  })

  useEffect(() => {
    if (isEnabled && identifier) {
      refetchCheckAuth()
    }
  }, [isEnabled, identifier])
}
