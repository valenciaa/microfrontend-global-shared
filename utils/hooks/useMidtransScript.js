import { useEffect } from 'react'
import config from '../../../../config'

export default function useMidtransScript() {
  useEffect(() => {
    const script = document.createElement('script')

    script.id = 'midtrans-script'
    script.type = 'text/javascript'
    script.src = 'https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js'
    script.setAttribute(
      'data-environment',
      config.isLiveSite ? 'production' : 'sandbox',
    )
    script.setAttribute('data-client-key', config.midtransClientKey)
    script.async = true

    document.body.appendChild(script)

    return () => {
      document.body.removeChild(script)
    }
  }, [])
}
