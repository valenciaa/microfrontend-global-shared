import { useEffect } from 'react'
import { GetUniqueId } from '..'
import { useSendTrackEvent } from '../../services/api/mutations/global'
import { bodyTrack, setDefaultData, setMetaData } from '../VueAIUtil'

export default function usePageViewVueTrack(
  pageName,
  products = null,
  correlationID = null,
) {
  // This only for page view at pages level
  const { mutate: sendTracking } = useSendTrackEvent()
  useEffect(() => {
    // Handle Tracking Product Recommend
    async function tracker() {
      const ugid = await GetUniqueId()
      const url = window.location.href
      const defaultData = setDefaultData(
        url,
        document.referrer,
        'pageView',
        'pageView',
        'true',
        ugid,
      )
      const metaData = setMetaData(null, null, null, null, pageName, products)
      if (correlationID) {
        defaultData.correlation_id = correlationID
      }
      sendTracking(bodyTrack(defaultData, {}, metaData))
    }
    tracker()

    // End Tracking
  }, [])
}

export function TrackVuePageViewV2({
  pageName,
  products = null,
  correlationID = undefined,
  callbackEvent,
}) {
  const { mutate: sendTracking } = useSendTrackEvent()

  // Move the tracker function outside of the useEffect
  const tracker = async () => {
    const ugid = await GetUniqueId()
    const url = window.location.href
    const defaultData = setDefaultData(
      url,
      document.referrer,
      'pageView',
      'pageView',
      'true',
      ugid,
    )
    const metaData = setMetaData(null, null, null, null, pageName, products)
    if (correlationID) {
      defaultData.correlation_id = correlationID
    }
    if (callbackEvent) {
      callbackEvent()
    }
    sendTracking(bodyTrack(defaultData, {}, metaData))
  }

  useEffect(() => {
    if (correlationID !== undefined) {
      tracker()
    }
  }, [correlationID])
}
