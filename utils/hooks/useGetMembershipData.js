import { useEffect } from 'react'
import localforage from 'localforage'
import Cookies from 'js-cookie'

import { useGetCustomerMembershipAlt } from '../../services/api/queries/global'
import config from '../../../../config'

// ? callback is a function called after check klk is not logged in
export default function useGetMembershipData() {
  const { refetch: getUserProfileKLK } = useGetCustomerMembershipAlt({
    enabled: false,
    onSuccess: (data) => {
      if (!data.error) {
        Cookies.set('interval_fetch_membership', true, {
          expires: new Date(new Date().getTime() + 60 * 1000),
        })

        const user = data.user
        const { sha256 } = require('js-sha256')
        const hashUser = user.customer_id
          ? sha256(user.customer_id.toString()).toString()
          : ''
        if (Cookies.get('algolia_ssr') !== hashUser && user) {
          if (hashUser) {
            Cookies.set('algolia_ssr', hashUser)
          }
        }

        localforage.setItem(
          'persist:ruparupa',
          JSON.stringify({
            auth: JSON.stringify({
              user: { ...data.user },
              isKlk: true,
              force_logout: data.force_logout,
            }),
          }),
        )

        if (data.force_logout) {
          localforage.clear()
          window.location.href = config.baseURL + 'auth/login'
        }
      }
    },
    onError: (error) => {
      if (error?.message?.toLowerCase() === 'token not valid') {
        localforage.clear()
        Cookies.remove('access_token')
        Cookies.remove('algolia_ssr')
        Cookies.remove('ugid')
        Cookies.remove('sessionrupaUID')
        Cookies.remove('rr-sid')
        Cookies.remove('geolocation')
        localStorage.removeItem('SHIPPING_LOCATION')
        window.location.href = config.baseURL
        return
      }
    },
  })

  useEffect(() => {
    const setup = async () => {
      const isFetch = await Cookies.get('interval_fetch_membership')
      const accessToken = await localforage.getItem('access_token')

      // ? check if user already activated
      // let isKlkActive = false
      // const authData = await localforage.getItem('persist:ruparupa')
      // let parsedAuthData = JSON.parse(authData)
      // parsedAuthData = parsedAuthData?.auth ? JSON.parse(parsedAuthData.auth) : null
      // if (parsedAuthData?.isKlk) isKlkActive = true
      //! Because there may be an issue with the value of isKLK not being set after activation,
      //! we remove the validation for active accounts checking force_logout
      if (!isFetch && accessToken) {
        getUserProfileKLK()
      }
    }
    setup()
  }, [])
}
