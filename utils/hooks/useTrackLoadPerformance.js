import mixpanel from 'mixpanel-browser'
import { useEffect } from 'react'
import config from '../../../../config'

const STORAGE_FIRST_TIME_KEY = 'first-time-visit-3.0'

export default function useTrackLoadPerformance(pageType) {
  useEffect(() => {
    const trackPerformance = () => {
      const ttlMs =
        window?.performance?.getEntriesByType('navigation')?.[0]?.duration
      if (!ttlMs || !window.location) {
        return
      }
      const firstTimeVisit =
        window.sessionStorage.getItem(STORAGE_FIRST_TIME_KEY) === '1' ? 1 : 0
      const ttl = Math.floor(ttlMs / 10)

      let connectionType = 'Connection information is not available'

      if ('connection' in navigator) {
        connectionType = navigator?.connection?.effectiveType
      }

      mixpanel.track('PageLoadTime', {
        'Connection Type': connectionType,
        'Location': pageType,
        'url': window.location.origin + window.location.pathname,
        'Time To Load': ttl,
        'First Time Visit Global': firstTimeVisit,
        'Device': config.environment === 'mobile' ? 'Mobile Web' : 'Desktop',
      })

      const fcpMs = window?.performance
        ?.getEntriesByType('paint')
        ?.find(({ name }) => name === 'first-contentful-paint')?.startTime

      if (fcpMs) {
        const fcp = Math.floor(fcpMs / 10)
        mixpanel.track('FirstContentfulPaint', {
          'Connection Type': connectionType,
          'Location': pageType,
          'url': window.location.origin + window.location.pathname,
          'First Contentful Paint': fcp,
          'First Time Visit Global': firstTimeVisit,
          'Device': config.environment === 'mobile' ? 'Mobile Web' : 'Desktop',
        })
      }

      window.sessionStorage.setItem(STORAGE_FIRST_TIME_KEY, 1)
    }
    const caller = () => setTimeout(trackPerformance)
    window.addEventListener('load', caller, { once: true })

    return () => {
      window.removeEventListener('load', caller)
    }
  }, [])
}
