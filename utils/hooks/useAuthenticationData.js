import { useMemo } from 'react'
import isEmpty from 'lodash/isEmpty'
import { useCartAuthContext } from '../../context/CartAuthContext'

export default function useAuthenticationData() {
  // const [isLogin, setIsLogin] = useState(false)
  const { state: cartAuthContext } = useCartAuthContext()
  const token = cartAuthContext?.access_token
  const user = cartAuthContext?.auth?.user
  // useEffect(() => {
  //   if (token?.length > 0 && typeof token === 'string' && !isEmpty(user)) {
  //     setIsLogin(true)
  //   } else {
  //     setIsLogin(false)
  //   }
  // }, [token, user])
  return useMemo(
    () => ({
      isLogin: token?.length > 0 && typeof token === 'string' && !isEmpty(user),
      user,
    }),
    [token, user],
  )
}
