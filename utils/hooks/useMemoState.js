import { useCallback, useMemo } from 'react'

export default function useMemoState(state, setState) {
  const handleChangeState = useCallback((key, value) => {
    setState((prev) => {
      if (prev[key] === value) {
        return prev
      }
      return { ...prev, [key]: value }
    })
  }, [])

  const handleChangeStateObject = useCallback((value) => {
    setState((prev) => {
      return { ...prev, ...value }
    })
  }, [])

  return useMemo(
    () => ({
      state,
      handleChangeState,
      handleChangeStateObject,
      setState,
    }),
    [handleChangeState, handleChangeStateObject, state],
  )
}
