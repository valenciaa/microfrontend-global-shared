import { useEffect } from 'react'
import { useCreateLog } from '../../services/api/mutations/global'

export default function usePostLogPdp(data) {
  // Data needed for post log PDP
  // {
  //     name (product name),
  //     imageUrl (first image product url),
  //     sid (get it from url query params),
  //     sku (product sku),
  //     urlKey:(product url_key)
  // }
  const { mutate: createLog } = useCreateLog({}, { enabled: false })
  useEffect(() => {
    if (data && data.sid) {
      createLog(data)
    }
  }, [])
}
