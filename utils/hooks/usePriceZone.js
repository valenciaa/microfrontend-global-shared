import { useEffect, useMemo, useState } from 'react'
import isEmpty from 'lodash/isEmpty'
import dayjs from 'dayjs'

import { useGetProductPriceZone } from '../../services/api/queries/pdp'

export default function usePriceZone(
  price,
  productDetail,
  activeVariant,
  zoneId,
  useElasticData,
  isVariantChanged,
  setDefaultSkuPrice,
) {
  const [selectedPriceZone, setSelectedPriceZone] = useState({})
  const [isSpecialPriceActive, setIsSpecialPriceActive] = useState(false)

  const params = useMemo(() => {
    if (
      !isEmpty(activeVariant) &&
      zoneId &&
      zoneId !== null &&
      !useElasticData
    ) {
      return {
        sku: activeVariant?.sku,
        zone_id: zoneId,
      }
    }
  }, [activeVariant, zoneId])

  const { data: basePriceZone } = useGetProductPriceZone(params, {
    enabled: !useElasticData,
  })

  useEffect(() => {
    if (!useElasticData) {
      if (
        basePriceZone &&
        !isEmpty(basePriceZone) &&
        zoneId &&
        selectedPriceZone &&
        selectedPriceZone?.zone_id !== parseInt(zoneId)
      ) {
        const filteredPriceZone = basePriceZone?.filter(
          (e) => e?.zone_id === parseInt(zoneId),
        )
        const dataPrice = {
          final_price: filteredPriceZone[0]?.sale_price,
          sku: filteredPriceZone[0]?.sku,
          zone_id: filteredPriceZone[0]?.zone_id,
        }
        setSelectedPriceZone(dataPrice)
      }
    } else {
      if (
        !isEmpty(activeVariant) &&
        activeVariant.price_zone &&
        !isEmpty(activeVariant?.price_zone)
      ) {
        const filteredPriceZone = activeVariant?.price_zone?.filter(
          (e) => e.zone_id === parseInt(zoneId),
        )
        if (filteredPriceZone && !isEmpty(filteredPriceZone)) {
          let finalPrice = filteredPriceZone[0]?.price
          // check for special_price and its in period or not
          let basePrice = 0
          if (
            filteredPriceZone[0].special_price &&
            filteredPriceZone[0].special_price > 0 &&
            filteredPriceZone[0].start_date &&
            filteredPriceZone[0].end_date &&
            dayjs().isSameOrAfter(filteredPriceZone[0].start_date) &&
            dayjs().isSameOrBefore(filteredPriceZone[0].end_date)
          ) {
            setIsSpecialPriceActive(true)
            basePrice = filteredPriceZone?.[0]?.price
            finalPrice = filteredPriceZone?.[0]?.special_price
          }

          const dataPrice = {
            base_price: basePrice,
            final_price: finalPrice,
            sku: activeVariant.sku,
            zone_id: filteredPriceZone[0].zone_id,
          }
          setSelectedPriceZone(dataPrice)
        }
      }
    }
  }, [activeVariant, productDetail, useElasticData, zoneId])

  return useMemo(() => {
    if (
      !isEmpty(selectedPriceZone) ||
      (!useElasticData && price?.price !== selectedPriceZone?.sale_price)
    ) {
      const data = {
        adjustedPrice: isSpecialPriceActive
          ? selectedPriceZone?.base_price
          : selectedPriceZone?.final_price,
        adjustedSpecialPrice: isSpecialPriceActive
          ? selectedPriceZone?.final_price
          : 0,
      }
      setIsSpecialPriceActive(false)
      if (!isVariantChanged) {
        setDefaultSkuPrice(data)
      }
      return data
    } else {
      return 0
    }
  }, [price, selectedPriceZone])
}
