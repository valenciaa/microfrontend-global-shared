import { useMemo } from 'react'
import config from '../../../../config'

export const indexODIPrd = 'productsvariant'
export const indexAHIPrd = 'productsvariant_ahi'
export const indexTGIPrd = 'productsvariant_tgi'
export const indexHCIPrd = 'productsvariant_hci'
export const indexInformaOnlinePrd = 'productsvariant_informa_online'

/**
 * hooks untuk inisialisasi data algolia
 *
 *
 * @param   {[string]}  companyCode  [companyCode ]
 * @param   {[string]}  companyName  [companyName ]
 * @param   {[boolean]}  isLiveSite   [isLiveSite ]
 *
 * @return  {[Object]}
 */
export const useAlgoliaInit = ({
  companyCode,
  companyName,
  url,
  isLiveSite = false,
}) => {
  const data = useMemo(() => {
    let indexName = ''
    let filter = 'status:10'

    if (companyCode !== 'ODI') {
      filter += ' AND is_mp:0'
    }
    let filterByCompany = ' AND '
    if (companyCode === 'ODI') {
      indexName = indexODIPrd
      filterByCompany += 'company_code.ODI:10'
    } else if (companyCode === 'TGI') {
      indexName = indexTGIPrd
      filterByCompany += 'company_code.TGI:10'
    } else if (companyCode === 'AHI') {
      indexName = indexAHIPrd
      filterByCompany += 'company_code.AHI:10'
    } else if (companyCode === 'HCI') {
      indexName = indexInformaOnlinePrd
      filterByCompany += 'company_code.HCI:10'
      if (companyName === 'informastore') {
        indexName = indexHCIPrd
      }
    }

    let platformTag = config.environment === 'mobile' ? 'mobile-web' : 'desktop'
    const analyticsTag = []
    let utmParam = url?.includes('utm_')
    const fromStore = url?.includes('store')
    if (!utmParam && typeof window !== 'undefined') {
      utmParam = window.location.href.includes('utm_')
    }
    // if (window && window.location && window.location.href) {
    //   utmParam = window?.location?.href?.includes('utm_') || ''
    // }

    if (config.companyCode === 'TGI') {
      platformTag = 'tgi-online-' + platformTag
    }

    if (isLiveSite) {
      platformTag += '-production'
    } else {
      platformTag += '-stg'
    }

    analyticsTag.push(platformTag)

    if (utmParam) {
      analyticsTag.push('contain-utm-source')
    }
    if (fromStore) {
      analyticsTag.push(`${config.environment}-pcp-store`)
    }
    return {
      indexName,
      filter: filter + filterByCompany,
      analyticsTag,
    }
  }, [])

  return data
}

export default useAlgoliaInit
