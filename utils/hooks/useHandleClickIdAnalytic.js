import { useEffect } from 'react'
import GetParam from '../GetParam'
import { handleSetGclid } from '../GclidWrapper'
import {
  handleSendFacebookConversionRequest,
  handleSetFbclid,
} from '../FacebookConversionWrapper'
import {
  handleSendTiktokConversionRequest,
  handleSetTtclid,
} from '../TiktokConversionWrapper'

export default function useHandleClickIdAnalytic() {
  // Google Click ID, Fb Click ID, Tiktok Click ID & header footer
  useEffect(() => {
    const gclid = GetParam('gclid')
    const fbclid = GetParam('fbclid')
    const ttclid = GetParam('ttclid')

    if (gclid) {
      handleSetGclid(gclid)
    }

    if (fbclid) {
      handleSetFbclid(fbclid)

      handleSendFacebookConversionRequest('PageView')
    } else {
      handleSendFacebookConversionRequest('PageView')
    }

    if (ttclid) {
      handleSetTtclid(ttclid)

      handleSendTiktokConversionRequest('ViewContent')
    } else {
      handleSendTiktokConversionRequest('ViewContent')
    }
  }, [])
}
