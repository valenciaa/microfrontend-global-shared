import localforage from 'localforage'
import config from '../../../config'
const Cookies = require('js-cookie')
const dayjs = require('dayjs')

export function makeid(length) {
  var result = ''
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  var charactersLength = characters.length
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

const GetUniqueId = async () => {
  let domainServer
  if (config.node_env !== 'development') {
    domainServer = 'ruparupa.com'
  } else {
    domainServer = 'ruparupastg.my.id' // change it to localhost if you want to try it in your local
  }
  let ugid = await Cookies.get('ugid', { domain: domainServer })
  if (!ugid || ugid === '') {
    ugid = await localforage.getItem('ugid')
  } else {
    await localforage.setItem('ugid', ugid)
  }
  let checkedUgid = ugid || ''
  // case UGID value berbentuk object atau memiliki value 9844f81e1408f6ecb932137d33bed7cfdcf518a3 harus di clean up
  if (
    checkedUgid !== '' &&
    checkedUgid === '9844f81e1408f6ecb932137d33bed7cfdcf518a3'
  ) {
    checkedUgid = ''
  }
  if (checkedUgid) {
    return ugid
  } else {
    const unique = String(dayjs().unix()) + String(makeid(35))
    if (unique && unique !== '') {
      await Cookies.set('ugid', unique.substring(0, 40), {
        domain: domainServer,
        expires: 365,
      })
      await localforage.setItem('ugid', unique.substring(0, 40))
    }
    return unique
  }
}
const generateRandomNumbers = () =>
  // generate 6 numbers
  Array.from({ length: 6 }, () => (Math.random() * 9).toFixed()).join('')

export const generateHypersonalizedUUID = () =>
  generateRandomNumbers() + Date.now()

export const createHypersonalizedIdentifier = (source) => ({
  hyper_detail: source?.data?.hyper_detail ?? '',
  ab_testing_id: source?.data?.ab_testing_id ?? '',
  list_content: source?.data?.list_content ?? [],
})

export default GetUniqueId
