import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import ItmCompanyName from './ItmCompanyName'
import GetParam from './GetParam'

export const micrositeItmGenerator = (
  type,
  micrositeName,
  url,
  sku,
  bannerName,
  isProduct = false,
  isTagging = false,
) => {
  let finalItm = url
  if (
    url.includes('ruparupa') ||
    [
      'category-recommendation-microsite',
      'promo-brand-banner',
      'promo-brand-button',
    ].includes(type) ||
    sku
  ) {
    let itmSource = 'itm_source='
    let itmCampaign = 'itm_campaign='
    const oldItmCampaign = GetParam('itm_campaign')

    let companyName = ItmCompanyName()
    if (companyName === 'toys') {
      companyName = 'toyskingdom'
    }

    let board = url?.trim()?.split('=')
    if (board.length > 1) {
      board = board[1]
        ?.replace(/\?/g, '')
        ?.replace(/ |#|\+/g, '-')
        ?.toLowerCase()
    } else {
      board = ''
    }

    if (!type?.includes('microsite')) {
      micrositeName = micrositeName
        ?.replace(/\?/g, '')
        ?.replace(/ |#|\+/g, '-')
        ?.toLowerCase()
    }

    // itm_campaign
    if (isEmpty(oldItmCampaign)) {
      if (!isEmpty(micrositeName)) {
        if (type?.includes('microsite') && micrositeName?.category) {
          let categoryName = micrositeName?.category?.replace(' ', '%20')
          itmCampaign += categoryName
        } else {
          itmCampaign += micrositeName
        }
      }
    } else {
      itmCampaign += oldItmCampaign
    }

    // itm_source
    if (!isEmpty(companyName) && !isProduct) {
      itmSource += companyName + '-'
    }
    if (!isEmpty(type)) {
      itmSource += type + '-'
    }
    if (!isEmpty(micrositeName)) {
      if (type?.includes('microsite') && micrositeName?.tahuName) {
        micrositeName = micrositeName?.tahuName || ''
      }
      itmSource += micrositeName + '-'
    }
    if (!isEmpty(bannerName)) {
      itmSource += bannerName + '-'
    }
    if (!isEmpty(board) && !isTagging) {
      itmSource += board
    }

    if (itmSource.slice(-1) === '-') {
      itmSource = itmSource.slice(0, -1)
    }

    // itm_device
    const itmDevice = 'itm_device=' + config?.environment

    let dividerItm = '?'

    if (
      !sku &&
      ![
        'inspiration-microsite',
        'category-recommendation-microsite',
        'promo-brand-banner',
        'promo-brand-button',
        'inspiration-button-microsite',
        'category-recommendation-microsite',
        'button-text-container',
      ].includes(type)
    ) {
      dividerItm = '&' /* Pdp itm has sku parameter */
    }

    finalItm = `${url?.trim()}${dividerItm}${itmSource}&${itmCampaign}&${itmDevice}`

    // itm_term

    if (!isEmpty(sku)) {
      finalItm += '&itm_term=' + sku
    }
  }

  return finalItm
}
