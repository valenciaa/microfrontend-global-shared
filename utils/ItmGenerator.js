import config from '../../../config'
import GetParam from './GetParam'
import isEmpty from 'lodash/isEmpty'
import { ItmConverter } from '.'
import ItmCompanyName from './ItmCompanyName'

// source = itm-source
// sourceCampaign, used if itm-source doesn't consist itm-campaign
// title = itm-campaign
// addCompanyName = unused company anme at the first sentence
const ItmGenerator = (
  source,
  url,
  title = '',
  sourceCampaign = null,
  term = null,
  addCompanyName = true,
  referral = '',
) => {
  let newUrl
  const companyName = ItmCompanyName()

  if (url === '') {
    newUrl = '#'
  } else if (title !== '') {
    const campaign = title
      .replace(/\?/g, '')
      .replace(/ |#|\+/g, '-')
      .toLowerCase()
    const sourceNameCampaign =
      sourceCampaign !== null
        ? sourceCampaign === ''
          ? ''
          : '-' +
            sourceCampaign
              .replace(/\?/g, '')
              .replace(/ |#|\+/g, '-')
              .toLowerCase()
        : '-' + campaign
    let itmCampaign = GetParam('itm_campaign')
    if (isEmpty(itmCampaign)) {
      itmCampaign = campaign
    }
    newUrl = url?.trim().includes('?')
      ? url?.trim() +
        `&itm_source=${
          companyName && addCompanyName ? companyName + '-' : ''
        }${source}${sourceNameCampaign}&itm_campaign=${campaign}${
          term ? '&itm_term=' + ItmConverter(term) : ''
        }&itm_device=${config.environment}`
      : url?.trim() +
        `?itm_source=${
          companyName && addCompanyName ? companyName + '-' : ''
        }${source}${sourceNameCampaign}&itm_campaign=${itmCampaign}${
          term ? '&itm_term=' + ItmConverter(term) : ''
        }&itm_device=${config.environment}`
  } else {
    if (url?.trim().includes('?')) {
      // if we detect in url there is itm campaign on there. we use them as itm campaign.
      const campaign = url.trim().split('=')
      let itmCampaign = GetParam('itm_campaign')
      if (isEmpty(itmCampaign)) {
        itmCampaign = campaign[1]
      }
      if (campaign.length > 1) {
        newUrl =
          url?.trim() +
          `&itm_source=${
            companyName && addCompanyName ? companyName + '-' : ''
          }${source}-${campaign[1]
            .replace(/\?/g, '')
            .replace(/ |#|\+/g, '-')
            .toLowerCase()}&itm_campaign=${itmCampaign
            .replace(/\?/g, '')
            .replace(/ |#|\+/g, '-')
            .toLowerCase()}&itm_device=${config.environment}`
      } else {
        newUrl =
          url?.trim() +
          `&itm_source=${
            companyName && addCompanyName ? companyName + '-' : ''
          }${source}&itm_device=${config.environment}`
      }
    } else {
      newUrl =
        url?.trim() +
        `?itm_source=${
          companyName && addCompanyName ? companyName + '-' : ''
        }${source}&itm_device=${config.environment}`
    }
  }
  if (referral !== '') {
    newUrl += referral
  }
  return encodeURI(newUrl)
}
export default ItmGenerator
