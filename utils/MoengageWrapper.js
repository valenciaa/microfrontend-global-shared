import isEmpty from 'lodash/isEmpty'
import config from '../../../config'

/**
 * Set current logged-in user data to MoEngage session
 * @param {number} customerId Current logged in user's customer ID
 * @param {string} firstName Current logged in user's first name
 * @param {string} lastName Current logged in user's last name
 * @param {string} email Current logged in user's email address
 * @param {string} phone Current logged in user's phone number
 * @param {'male' | 'female' | null} gender Current logged in user's gender
 * @param {Date?} bithDate Current logged in user's birth date
 * @return {Promise<void>}
 **/

const isProduction =
  config?.node_env === 'production' &&
  config?.isLiveSite &&
  config?.moengageScript

export const moengageLogin = async (
  customerId,
  firstName,
  lastName,
  email,
  phone,
  gender,
  birthDate,
  isActive,
  memberId,
  registrationStatus,
  isBot = false,
) => {
  if (isProduction && !isBot) {
    await Promise.all([
      /* eslint-disable-next-line no-undef */
      Moengage.track_event('Login'),

      /* eslint-disable-next-line no-undef */
      Moengage.track_event('Save User Attribute', {
        customer_id: customerId,
        first_name: firstName,
        last_name: lastName,
        email: email,
        phone: phone,
        gender: gender,
        birthday: birthDate,
        is_activated: isActive,
        rrr_id: memberId,
        registration_status: registrationStatus,
      }),

      /* eslint-disable-next-line no-undef */
      Moengage.add_unique_user_id(customerId),

      /* eslint-disable-next-line no-undef */
      Moengage.add_first_name(firstName),

      /* eslint-disable-next-line no-undef */
      Moengage.add_last_name(lastName),

      /* eslint-disable-next-line no-undef */
      Moengage.add_email(email),

      /* eslint-disable-next-line no-undef */
      Moengage.add_mobile(phone),

      /* eslint-disable-next-line no-undef */
      Moengage.add_gender(gender),

      /* eslint-disable-next-line no-undef */
      Moengage.add_birthday(birthDate),

      // Custom props

      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('member_id_odi', customerId.toString()),

      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('is_activated', isActive),

      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('rrr_id', memberId),

      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('registration_status', registrationStatus),
    ])
  }
}

export const moengageCompletedActivation = async (memberId, isBot = false) => {
  if (isProduction && !isBot && memberId) {
    await Promise.all([
      /* eslint-disable-next-line no-undef */
      Moengage.track_event('Completed Activation'),
      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('is_activated', true),
      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('rrr_id', memberId),
    ])
  }
}

/**
 * Set registered user data to MoEngage session
 * @param {number} customerId Registered user's customer ID
 * @param {string} firstName Registered user's first name
 * @param {string} lastName Registered user's last name
 * @param {string} email Registered user's email address
 * @param {string} phone Registered user's email phone number
 * @return {Promise<void>}
 **/
export const moengageRegister = async (
  customerId,
  firstName,
  lastName,
  email,
  phone,
  gender,
  isActive,
  memberId,
  registrationStatus,
) => {
  if (isProduction) {
    await Promise.all([
      /* eslint-disable-next-line no-undef */
      Moengage.track_event('Complete Registration', {
        timestamp: Date.now(),
        customer_id: customerId,
        email: email,
        phone_number: phone,
      }),

      /* eslint-disable-next-line no-undef */
      Moengage.track_event('Save User Attribute', {
        customer_id: customerId,
        first_name: firstName,
        last_name: lastName,
        email: email,
        phone: phone,
        gender: gender,
        birthday: ' ',
        is_activated: isActive,
        rrr_id: memberId,
        registration_status: registrationStatus,
      }),

      /* eslint-disable-next-line no-undef */
      Moengage.add_unique_user_id(customerId),

      /* eslint-disable-next-line no-undef */
      Moengage.add_first_name(firstName),

      /* eslint-disable-next-line no-undef */
      Moengage.add_last_name(lastName),

      /* eslint-disable-next-line no-undef */
      Moengage.add_email(email),

      /* eslint-disable-next-line no-undef */
      Moengage.add_mobile(phone),

      /* eslint-disable-next-line no-undef */
      Moengage.add_gender(gender),
      // Custom props

      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('member_id_odi', customerId.toString()),

      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('is_activated', isActive),

      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('rrr_id', memberId),

      /* eslint-disable-next-line no-undef */
      Moengage.add_user_attribute('registration_status', registrationStatus),
    ])
  }
}

/**
 * Track search event
 * @param {string} keyword Search keyword
 * @returns {Promise<void>}
 */
export const moengageSearch = async (keyword, isBot) => {
  if (isProduction && !isBot) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('Search', {
      keyword,
    })
  }
}

/**
 * Track input birthday date event and set current user's birthdate in MoEngage
 * @param {Date} birthDate Birth date
 * @returns {Promise<void>}
 */
export const moengageInputBirthdayDate = async (birthDate) => {
  if (isProduction) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('Input Birthday Date', {
      date: birthDate,
    })

    // eslint-disable-next-line no-undef
    await Moengage.add_birthday(birthDate)
  }
}

/**
 * Track view PCP event
 * @param {string} name PCP category name
 * @returns {Promise<void>}
 */
export const moengageViewPCP = async (name, isBot = false) => {
  if (isProduction && !isBot) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('View PCP', {
      name,
    })
  }
}

/**
 * Track view microsite event
 * @param {string} title Title or path of the microsite
 * @returns {Promise<void>}
 */
export const moengageViewMicrosite = async (title, isBot) => {
  if (isProduction && !isBot) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('View Microsite', {
      title,
    })
  }
}

/**
 * Extract event data from product
 * @param {Object} activeVariant Variant of the product
 * @param {string?} activeVariant.sku SKU of the active variant
 * @param {Object} product Product data
 * @param {string?} product.name Name of the product
 * @param {string[]?} product.trackBreadcrumb Category of the product as shown in the breadcrumb
 * @param {number} customerID Current logged in user's customer ID
 */
const extractEventData = (activeVariant, product, customerID = '') => {
  const price = activeVariant?.prices?.[0]
  const finalPrice =
    price?.special_price > 0 ? price?.special_price : price?.price
  const discount =
    price?.special_price > 0
      ? Math.floor(((price?.price - price?.special_price) / price?.price) * 100)
      : 0
  const brandStoreId = []
  if (product?.store_available && product?.store_available.length > 0) {
    product.store_available.forEach((store) => {
      brandStoreId.push(store.brand_store_id)
    })
  }
  const moengageEventData = {
    customer_id: customerID,
    item_id:
      activeVariant && !Number.isNaN(activeVariant.sku)
        ? Number.parseInt(activeVariant.sku)
        : 0,
    item_name: product && product.name ? product.name.toLowerCase() : 'None',
    item_brand: product?.brand?.name || 'None',
    item_price: finalPrice || 0,
    item_discount: discount,
    supplier_alias: product?.supplier?.supplier_alias || 'None',
    product_label: activeVariant?.label || 'None',
    brand_store_id: brandStoreId?.length > 0 ? brandStoreId.toString() : '',
    is_flash_sale: !!(product?.events && product?.events?.length > 0),
  }

  if (activeVariant?.attributes && activeVariant?.attributes?.length > 0) {
    const colorAttribute =
      activeVariant?.attributes?.find(
        (attribute) => attribute.attribute_name === 'color',
      ) || []
    if (!isEmpty(colorAttribute)) {
      moengageEventData.item_color =
        colorAttribute?.attribute_option?.option_value || ''
    }
  }

  if (
    product &&
    product.trackBreadcrumb &&
    Array.isArray(product.trackBreadcrumb)
  ) {
    product.trackBreadcrumb.forEach((category, index) => {
      moengageEventData[`item_category_lv_${index + 1}_id`] =
        category.category_id || ''
      moengageEventData[`item_category_lv_${index + 1}`] =
        category.name || 'None'
    })
  }

  return moengageEventData
}

/**
 * Track View PDP event
 * @param {Object} activeVariant Variant of the product
 * @param {Object} product Product data
 * @param {number} customerID Current logged in user's customer ID
 */
export const moengageViewPDP = async (
  activeVariant,
  product,
  customerID = '',
  isBot = false,
) => {
  const moengageEventData = extractEventData(activeVariant, product, customerID)
  if (isProduction && !isBot) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('View PDP', moengageEventData)
  }
}

/**
 * Track Add to Cart event
 * @param {Object} activeVariant Variant of the product
 * @param {Object} product Product data
 * @param {number} customerID Current logged in user's customer ID
 */
export const moengageAddToCart = async (
  activeVariant,
  product,
  customerID,
  isBot = false,
) => {
  const moengageEventData = extractEventData(activeVariant, product, customerID)
  if (isProduction && !isBot) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('Add to Cart', moengageEventData)
  }
}

/**
 * Track Add to Wishlist event
 * @param {Object} activeVariant Variant of the product
 * @param {Object} product Product data
 * @param {number} customerID Current logged in user's customer ID
 */
export const moengageAddToWishlist = async (
  activeVariant,
  product,
  customerID = '',
  isBot = false,
) => {
  const moengageEventData = extractEventData(activeVariant, product, customerID)
  if (isProduction && !isBot) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('Add to Wishlist', moengageEventData)
  }
}

/**
 * Track submit rating event
 * @param {string} customerId Customer ID
 * @param {number} rating Rating of the product
 * @param {string?} comment Comment of the product, if any
 */
export const moengageSubmitRating = async (customerId, rating, comment) => {
  if (isProduction) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('Rating Submitted', {
      customer_id: customerId,
      rating,
      submitted_comment: comment,
      timestamp: Date.now(),
    })
  }
}

/**
 * Track subscribe newsletter event
 * @param {string} email Email that wants to be subscribed
 */
export const moengageSubscribeNewsletter = async (email, isBot = false) => {
  if (isProduction && !isBot) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('Daftar Newsletter', {
      email_address: email,
    })
  }
}

/**
 * Set customer ID to identify MoEngage user
 * @param {object} user user data
 */
export const moengageIdentify = async (user, isBot = false) => {
  if (isProduction && !isBot) {
    // eslint-disable-next-line no-undef
    await Moengage.add_unique_user_id(user.customer_id)
  }
}

export const moengageViewHomepage = async (isBot = false) => {
  if (isProduction && !isBot) {
    let eventName = 'View Homepage'

    switch (config.companyCode) {
      case 'AHI':
        eventName = `View Page ACE Online'` //ACE_HARDWARE
        break
      case 'HCI':
        eventName = 'View Page Informa Online'
        break
      case 'TGI':
        eventName = 'View Page TGI Online'
        break
      default:
        break
    }
    // eslint-disable-next-line no-undef
    await Moengage.track_event(eventName)
  }
}

export const moengageViewMyProfile = async () => {
  if (isProduction) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('View Page Profile Saya')
  }
}

const MoengageAttributeController = (attribute) => {
  for (const key in attribute) {
    if (attribute[key].length > 0) {
      // eslint-disable-next-line no-undef
      Moengage.add_user_attribute(key, attribute[key])
    }
  }
}

/**
 * @param {import('./types/MoengageTypes').TEditProfile} profileProps
 */

export const moengageTrackEditProfile = async (profileProps) => {
  const {
    children,
    pets,
    bu_card_group: cardGroup,
    member_level: memberLevel,
    marital_status: maritalStatus,
    hobbies,
  } = profileProps
  const additionalData = {}

  let maritalStatusLabel
  if (maritalStatus === 2) {
    maritalStatusLabel = 'Menikah'
  } else {
    maritalStatusLabel = 'Tidak Menikah'
  }

  Array.from({ length: 4 }, (_, index) => {
    const childArr = children && children[index] ? children[index] : {} // If child datas is not available, use an empty object
    const petsArr = pets && pets[index] ? pets[index] : {} // If pet datas is not available, use an empty object

    let petTypes = petsArr?.type?.name
    if (petsArr?.breed?.name) {
      petTypes += `: ${petsArr?.breed?.name}`
    }

    if (childArr?.name) {
      additionalData[`child_name_${index + 1}`] = childArr.name
    }

    if (childArr?.gender) {
      additionalData[`child_gender_${index + 1}`] = childArr.gender
    }

    if (childArr?.birthday) {
      additionalData[`child_birthday_${index + 1}`] = childArr.birthday
    }

    if (petsArr?.name) {
      additionalData[`pet_name_${index + 1}`] = petsArr.name
    }

    if (petTypes) {
      additionalData[`pet_type_${index + 1}`] = petTypes
    }

    if (petsArr?.birthday) {
      additionalData[`pet_birthday_${index + 1}`] = petsArr?.birthday
    }
  })

  const data = {
    // User Profile
    ...(hobbies !== null && { hobby: hobbies }),
    member_level: memberLevel,
    marital_status: maritalStatusLabel,
    // Child and Pet Profile
    ...additionalData,
    // Card Profile
    ...cardGroup,
  }

  if (isProduction) {
    // eslint-disable-next-line no-undef
    await Moengage.track_event('Save User Attribute', data)
    MoengageAttributeController(data)
  }
}

// export const moengageEditProfile = async (customerId, firstName, lastName, email, phone, gender, birthDate, isActive, memberId) => {
//   if (isProduction) {
//     await Promise.all([
//       /* eslint-disable-next-line no-undef */
//       Moengage.track_event('Update Profile'),

//       Moengage.track_event('Save User Attribute', {
//         customer_id: customerId,
//         first_name: firstName,
//         last_name: lastName,
//         email: email,
//         phone: phone,
//         gender: gender,
//         birthday: birthDate,
//         is_activated: isActive,
//         rrr_id: memberId
//       }),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_unique_user_id(customerId),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_first_name(firstName),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_last_name(lastName),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_email(email),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_mobile(phone),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_gender(gender),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_birthday(birthDate),

//       // Custom props

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_user_attribute('member_id_odi', customerId.toString()),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_user_attribute('is_activated', isActive),

//       /* eslint-disable-next-line no-undef */
//       Moengage.add_user_attribute('rrr_id', memberId)
//     ])
//   }
// }
