/**
 * string replace
 * ex: rumah-tangga.html = rumah tangga
 *
 * @param   {[string]}  str
 *
 * @return  {[string]}
 */
export const clearQueryParam = (str) =>
  str?.replace('.html', '').replace(/-/g, ' ')

/**
 * string capitalize
 * ex: foo = Foo
 *
 * @param {[string]} str
 *
 * @return {[string]}
 */
export const capitalizeFirstLetter = (str) =>
  str?.charAt(0)?.toUpperCase() + str?.slice(1)

/**
 * string capitalize
 * ex: foo bar = Foo Bar
 *
 * @param {[string]} str
 *
 * @return {[string]}
 */
export const capitalize = (str) =>
  str
    ?.toLowerCase()
    ?.split(' ')
    .map((item) => capitalizeFirstLetter(item))
    .join(' ')

/**
 * mengetahui string dapat dipotong atau tidak
 */
export const isTrimString = (str, lenght = 236) => {
  return str?.length >= lenght
}

/**
 * menghapus html tag
 *
 * @param   {[string]}  str
 *
 * @return  {[string]}
 */
export const clearHtmlTag = (str) => {
  return str?.replace(/<\/?[^>]+(>|$)/g, '')
}

export const emailBlur = (str = '') => {
  if (str) {
    const email = str
    let findBefore = email.split('@')[0]
    var setArr = [...findBefore]
    for (let i = 0; i <= setArr.length; i++) {
      if (i <= setArr.length - 3 && i !== 0) {
        setArr[i] = '*'
      }
    }
    findBefore = setArr.join('')
    return findBefore + '@' + email.split('@')[1]
  }
  return ''
}
