import localforage from 'localforage'
import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import FilterProductLastSeen from './FilterProductLastSeen'

export const getLastSeenProduct = async () => {
  localforage.removeItem('product_last_seenv2')
  let lastSeen = await localforage.getItem('product_last_seenv3')
  if (lastSeen) {
    lastSeen = JSON.parse(lastSeen)
    if (!Array.isArray(lastSeen)) {
      lastSeen = [lastSeen]
      localforage.setItem('product_last_seenv3', JSON.stringify(lastSeen))
    }
    lastSeen = lastSeen.filter(
      (el) =>
        !isEmpty(el.variants) &&
        el?.company_code?.[`${config.companyCode}`] === parseInt(10),
    )
    lastSeen = FilterProductLastSeen(lastSeen)
    return lastSeen
  }
}
