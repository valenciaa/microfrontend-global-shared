import GlobalApi from '../services/GlobalApi'

export async function postSetAttributeMoengage(payload) {
  const api = GlobalApi.create()
  await api.postSetAttributeMoengage(payload)
}
