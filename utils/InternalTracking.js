import dayjs from 'dayjs'
import Cookies from 'js-cookie'

import config from '../../../config'
import { makeid } from './GetUniqueId'
import { handleGetSource } from './MixpanelWrapper'
import GetParam from './GetParam'
import GlobalApi from '../services/GlobalApi'

async function postInternalTracker(payload) {
  const api = GlobalApi.create()
  await api.postInternalTracker(payload)
}

export function browserValue() {
  // https://stackoverflow.com/questions/5916900/how-can-you-detect-the-version-of-a-browser
  return (function () {
    var userAgent = navigator.userAgent
    var tem
    var M =
      userAgent.match(
        /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i,
      ) || []
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(userAgent) || []
      return 'IE ' + (tem[1] || '')
    }
    if (M[1] === 'Chrome') {
      tem = userAgent.match(/\b(OPR|Edge)\/(\d+)/)
      if (tem != null) {
        return tem.slice(1).join(' ').replace('OPR', 'Opera')
      }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?']
    if ((tem = userAgent.match(/version\/(\d+)/i)) != null) {
      M.splice(1, 1, tem[1])
    }

    return { browser_name: M?.[0] || '', version: M?.[1] || '' }
  })()
}

const defaultValue = (eventName, userData = {}) => {
  const utmSource =
    GetParam('utm_source') || Cookies.get('mp-last-touch-utm-source') || 'None'
  const utmMedium =
    GetParam('utm_medium') || Cookies.get('mp-last-touch-utm-medium') || 'None'
  const utmCampaign =
    GetParam('utm_campaign') ||
    Cookies.get('mp-last-touch-utm-campaign') ||
    'None'
  const utmContent =
    GetParam('utm_content') ||
    Cookies.get('mp-last-touch-utm-content') ||
    'None'

  return {
    'event_name': eventName,
    'time': String(dayjs().unix()),
    'Customer Email': userData?.email || '',
    'Customer ID': userData?.customer_id || '',
    'Device': config.environment === 'desktop' ? 'Desktop' : 'Mobile Web',
    'distinct_id': userData?.customer_id || userData?.uuid || '',
    '$browser': browserValue().browser_name || '',
    '$browser_version': browserValue().version || '',
    '$current_url': window?.location?.href,
    '$insert_id': String(dayjs().unix()) + String(makeid(35)), // unix time + random A-Za-z0-9 total 35 character
    '$os': window?.navigator?.userAgentData?.platform,
    '$referrer': document?.referrer || '',
    '$reffering_domain':
      document?.referrer?.length > 0 ? document?.referrer?.split('/')[2] : '',
    '$screen_height': window?.screen?.height,
    '$screen_width': window?.screen?.width,
    '$user_id': userData?.customer_id || '',
    'Company Code': config.companyCode,
    'Last Touch UTM Campaign': utmSource,
    'Last Touch UTM Content': utmContent,
    'Last Touch UTM Medium': utmMedium,
    'Last Touch UTM Source': utmCampaign,
    'Web Source': handleGetSource(),
  }
}

/**
 * internal tracker data
 * @param {string} eventName event name that will be send
 * @param {object} userData Current userData
 * @param {object} payload Main payload of the event
 * @return {object}
 **/
export function internalTrackerData(eventName, userData, payload) {
  // there's chance that the endpoint from config (staging / development mode) will be terminate, if that happen please contact team data
  postInternalTracker({ ...defaultValue(eventName, userData), ...payload })
}
