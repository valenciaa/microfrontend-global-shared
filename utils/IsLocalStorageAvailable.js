const IsLocalStorageAvailable = () => {
  if (window && window.localStorage) {
    const test = '__localStorageTest__'

    try {
      window.localStorage.setItem(test, test)
      window.localStorage.removeItem(test)
    } catch (e) {
      return false
    }

    return true
  }

  return false
}

export default IsLocalStorageAvailable
