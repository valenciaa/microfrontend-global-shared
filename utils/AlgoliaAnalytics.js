import SHA256 from 'crypto-js/sha256'
import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import NumberFormat from './NumberFormat'
import aa from 'search-insights'
import Cookies from 'js-cookie'
import ItmCompanyName from './ItmCompanyName'

let getItmSource = false
if (typeof window !== 'undefined') {
  const companyName = ItmCompanyName()
  getItmSource = window.location.href.includes(
    `itm_source=${companyName ? companyName + '-' : ''}search`,
  )
}
const enableAA = getItmSource

export function initAlgoliaAnalytics(source = '') {
  if (!enableAA && source !== 'searchbar') {
    return
  }
  aa('init', {
    appId: `${config.apiIndexAlgolia}`,
    apiKey: `${config.apiKeyAlgolia}`,
    useCookie: true, // cookies expired in 6 month by default algolia
  })
}

export function submitClickAAEvent(searchState) {
  if (!enableAA) {
    return
  }
  const { filterObject, queryID } = getFilterStringForAA(searchState)
  const customerId = getHashedCustomerId()
  const indexName = getAAIndexName()
  if (filterObject.price) {
    aa('clickedFilters', {
      index: indexName,
      eventName: 'filter_price_clicked',
      filters: [filterObject.price],
      userToken: customerId,
      queryID: queryID,
    })
  }
}

export function submitViewAAEvent(searchState, hitData) {
  if (!enableAA) {
    return
  }
  const { filterObject } = getFilterStringForAA(searchState)
  const customerId = getHashedCustomerId()
  const indexName = getAAIndexName()
  function sendViewedAAEvent(eventName) {
    aa('viewedObjectIDs', {
      index: indexName,
      eventName,
      userToken: customerId,
      queryID: hitData?.__queryID || '',
      objectIDs: [hitData.objectID],
    })
  }
  if (filterObject.price) {
    sendViewedAAEvent('filter_price_viewed')
  }
  if (filterObject.brand) {
    sendViewedAAEvent('filter_brand_viewed')
  }
  if (filterObject.promo) {
    sendViewedAAEvent('filter_promo_viewed')
  }
  if (filterObject.colour) {
    sendViewedAAEvent('filter_color_viewed')
  }
  if (filterObject.delivery) {
    sendViewedAAEvent('filter_delivery_viewed')
  }
  if (filterObject.category) {
    sendViewedAAEvent('filter_category_viewed')
  }
  if (filterObject.store) {
    sendViewedAAEvent('filter_official_store_viewed')
  }
  if (filterObject.city) {
    sendViewedAAEvent('filter_location_viewed')
  }
}

export function submitClickProductAAEvent(searchState, hitData) {
  if (!enableAA) {
    return
  }
  const customerId = getHashedCustomerId()
  try {
    aa('clickedObjectIDsAfterSearch', {
      index: searchState.sortBy || 'productsvariant',
      eventName: 'product_catalog',
      objectIDs: [hitData.objectID],
      queryID: hitData.__queryID || '',
      positions: [hitData.__position],
      userToken: customerId,
    })
  } catch (e) {
    console.log('Error click product AA Event: ', e)
  }
}

export function submitAddToCartAAEvent(
  activeVariant,
  queryId,
  sort,
  eventName,
) {
  const customerId = getHashedCustomerId()

  let indexName = 'productsvariant'
  if (!isEmpty(sort) && sort !== 'productsvariant') {
    indexName += '_' + sort
  } else {
    switch (config.companyCode) {
      case 'AHI':
        indexName += '_ahi'
        break
      case 'HCI':
        if (config.companyName === 'informastore') {
          indexName += '_hci'
        } else {
          indexName += '_informa_online'
        }
        break
      case 'TGI':
        indexName += '_tgi'
        break
      default:
        break
    }
  }
  if (enableAA) {
    if (!isEmpty(queryId)) {
      aa('convertedObjectIDsAfterSearch', {
        index: indexName,
        eventName,
        objectIDs: [activeVariant.sku],
        queryID: queryId,
        userToken: customerId,
      })
      // if (type === 'wishlist') {
      //   aa('convertedObjectIDsAfterSearch', {
      //     index: indexName,
      //     eventName: 'add_product_wishlist',
      //     objectIDs: [activeVariant.sku],
      //     queryID: queryId
      //   })
      // }
    }
  }
}

export function getHashedCustomerId() {
  const customerId =
    Cookies.get('algolia_ssr') ||
    Cookies.get('_ALGOLIA')?.toString() ||
    SHA256('').toString()
  // e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 hasil hashing SHA256 ke string
  return customerId || undefined
}

function getAAIndexName() {
  let indexName = 'productsvariant'
  if (config.companyCode !== 'ODI') {
    indexName = indexName + '_' + config.companyCode.toLowerCase()
  }
  return indexName
}

function getDataFromSearchState(searchState = {}) {
  const { range = {}, refinementList = {} } = searchState
  const { selling_price: sellingPrice = '' } = range
  const min = sellingPrice?.split(':')[0]
  const max = sellingPrice?.split(':')[1]
  let minimal = NumberFormat(min)
  let maximal = NumberFormat(max)
  if (minimal === '' && maximal !== '') {
    minimal = 0
  } else if (minimal !== '' && maximal === '') {
    maximal = 999999999
  }

  return {
    min: minimal,
    max: maximal,
    refinementList,
  }
}

function getFilterStringForAA(searchState) {
  const { max, min, refinementList } = getDataFromSearchState(searchState)
  const price =
    min &&
    max &&
    (min !== '' || max !== '') &&
    (parseInt(min) !== parseInt(0) || parseInt(max) !== parseInt(999999999))
      ? `(selling_price: ${min} TO ${max})`
      : ''

  const brandData = refinementList['brand.name']
  const brand =
    brandData && brandData.length
      ? '(brand.name:' + brandData.join(' OR brand.name:') + ')'
      : ''

  const promoData = refinementList[`label.${config.companyCode}`]
  const promo =
    promoData && promoData.length
      ? `(label.${config.companyCode}:` +
        promoData.join(` OR label.${config.companyCode}:`) +
        ')'
      : ''

  const colourData = refinementList.colour
  const colour =
    colourData && colourData.length
      ? '(colour:' + colourData.join(' OR colour:') + ')'
      : ''

  const deliveryData = refinementList.delivery_method
  const delivery =
    deliveryData && deliveryData.length
      ? '(delivery_method:' + deliveryData.join(' OR delivery_method:') + ')'
      : ''

  const categoryData =
    refinementList[`bread_crumb3_${config.companyCode.toLowerCase()}.name`]
  const category =
    categoryData && categoryData.length
      ? `(bread_crumb3_${config.companyCode.toLowerCase()}.name:` +
        refinementList[
          `bread_crumb3_${config.companyCode.toLowerCase()}.name`
        ].join(` OR bread_crumb3_${config.companyCode.toLowerCase()}.name:`) +
        ')'
      : ''

  const cityData = refinementList?.city_name
  const city =
    cityData && cityData?.length
      ? '(city:' + cityData.join(' OR city:') + ')'
      : ''

  const storeData = refinementList?.store_name
  const store =
    storeData && storeData.length
      ? '(store:' + storeData.join('OR store:') + ')'
      : ''

  const filterArray = [brand, promo, colour, delivery, category, price].filter(
    (el) => !!el,
  )
  const queryID = searchState?.queryID || ''

  return {
    filterArray,
    filterObject: {
      brand,
      promo,
      colour,
      delivery,
      category,
      price,
      city,
      store,
    },
    queryID,
  }
}

export function submitClickProductSearchBarAAEvent(
  indexName,
  hitData,
  queryID,
  position,
) {
  if (!enableAA) {
    return
  }
  const customerId = getHashedCustomerId()
  try {
    aa('clickedObjectIDsAfterSearch', {
      index: indexName || 'productsvariant',
      eventName: 'product_search',
      objectIDs: [hitData.objectID],
      queryID: queryID || '',
      positions: [position],
      userToken: customerId,
    })
  } catch (e) {
    console.log('error submit click product Searchbar AA Event: ', e)
  }
}
