import isEmpty from 'lodash/isEmpty'
import { CURRENT_PAGES } from './constants/server'

const regex = /\/m-([^/]+)\/?/

const getPrefix = (url) => {
  const match = url?.match(regex)

  if (isEmpty(match)) {
    return null
  }

  return match[1]
}

const prefixPathChecker = (url) => {
  const prefix = getPrefix(url)

  if (!prefix) {
    return false
  }

  const isMatch = CURRENT_PAGES.some((page) => page.includes(`/${prefix}`))

  if (!isMatch) {
    return false
  }

  return true
}

const prefixPathModifier = (url) => {
  const prefix = getPrefix(url)

  if (!prefix) {
    return url
  }

  const transformedUrl = url?.replace(regex, `/${prefix}`)

  return transformedUrl
}

export { getPrefix, prefixPathChecker, prefixPathModifier }
