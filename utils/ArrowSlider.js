export default function ArrowSlider({
  children,
  id,
  dataLength,
  micrositeName,
}) {
  if (micrositeName) {
    return slider(children, id, dataLength)
  }
  if (children) {
    return children
  }
  return null
}

function slider(children, id, dataLength) {
  const setOverflowScroll = (position) => {
    const button = document?.getElementById(id)

    if (button) {
      if (position === 'left') {
        button.scrollLeft -= 250
      } else {
        button.scrollLeft += 250
      }
    }
  }

  return (
    <>
      <div
        className={`overflow-scroll__arrow swiper-button-prev  overflow-scroll__arrow-left overflow-scroll__arrow__${
          dataLength > 5 ? 'active' : 'disabled'
        }`}
        onClick={() => setOverflowScroll('left')}
      />

      {children}

      <div
        className={`overflow-scroll__arrow swiper-button-next overflow-scroll__arrow-right overflow-scroll__arrow__${
          dataLength > 5 ? 'active' : 'disabled'
        }`}
        onClick={() => setOverflowScroll('right')}
      />
    </>
  )
}
