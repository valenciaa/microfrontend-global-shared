import isEmpty from 'lodash/isEmpty'
import config from '../../../config'

const isIncludedInWhiteList = (email) => {
  if (isEmpty(config.whiteListFeature)) {
    return true
  }

  if (config.whiteListFeature.includes(email)) {
    return true
  } else {
    return false
  }
}

export default isIncludedInWhiteList
