export const getIPAddress = () => {
  if (typeof window !== 'undefined') {
    return window?.myIpAddress || ''
  }
  return ''
}
