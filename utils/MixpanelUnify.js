import isEmpty from 'lodash/isEmpty'
import toSafeInteger from 'lodash/toSafeInteger'
import { mixpanelTrack } from './MixpanelWrapper'

export const mixpanelClickWaitingPaymentDetail = (data) => {
  const dataShipment = data.shipment
  const mixpanelDataItem = []
  const mixpanelData = []
  if (!isEmpty(dataShipment)) {
    dataShipment?.forEach((data) => {
      data.details?.forEach((item) => {
        const sellingPrice =
          item?.selling_price.indexOf('Rp') === -1
            ? item?.selling_price
            : item.selling_price.substr(3)
        const normalPrice =
          item?.normal_price.indexOf('Rp') === -1
            ? item?.normal_price
            : item.normal_price.substr(3)
        mixpanelDataItem.push({
          'Item Quantity': item?.qty_ordered || 1,
          'Item Price': sellingPrice || 0, // final price after discount
          'Selling Price': normalPrice || 0,
          'Discount Amount':
            normalPrice > sellingPrice ? normalPrice - sellingPrice : 0,
          'Store Name': item?.shipping_from_store || '',
          'Store Code': item?.store_code || '',
        })
      })
      mixpanelData.push({
        'Shipping Amount': data?.biaya_kirim || 0,
        'Service Amount': data?.biaya_layanan || 0,
        'Subtotal': data?.total || '',
      })
    })
    mixpanelTrack('Click Menunggu Pembayaran Detail', {
      'Item Name': data.productNameList,
      'Item ID': data.skuList,
      'Item Quantity': mixpanelDataItem.map((item) => item['Item Quantity']),
      'Item Price': mixpanelDataItem.map((item) => item['Item Price']),
      'Selling Price': mixpanelDataItem.map((item) => item['Selling Price']),
      'Discount Amount': mixpanelDataItem.map(
        (item) => item['Discount Amount'],
      ),
      'Gift Card Amount':
        toSafeInteger(data.discount_amount) +
        toSafeInteger(data.gift_cards_amount) +
        toSafeInteger(data.shipping_discount_amount),
      'Shipping Amount': mixpanelData.map((item) => item['Shipping Amount']),
      'Service Amount': mixpanelData.map((item) => item['Service Amount']),
      'Subtotal': mixpanelData.map((item) => item['Subtotal']), // eslint-disable-line
      'Store Name': mixpanelDataItem
        .map((item) => item['Store Name'])
        .filter((value, index, array) => array.indexOf(value) === index),
      'Store Code': mixpanelDataItem
        .map((item) => item['Store Code'])
        .filter((value, index, array) => array.indexOf(value) === index),
      'Grand Total': data.grand_total,
      'Order No': data.order_no,
      'Payment Method': data.payment?.label,
    })
  }
}

export const mixpanelChangePaymentMethod = (paymentData) => {
  mixpanelTrack('Ubah Metode Pembayaran', {
    'Bank Name':
      paymentData?.type === 'bank_transfer'
        ? paymentData?.va_bank
        : paymentData?.method,
    'Payment Method': paymentData?.type,
    'Location': 'Menunggu Pembayaran',
  })
}

export const mixpanelChangePaymentMethodPopUp = (paymentData) => {
  mixpanelTrack('Ubah Metode Pembayaran Pop Up', {
    'Bank Name':
      paymentData?.type === 'bank_transfer'
        ? paymentData?.va_bank
        : paymentData?.method,
    'Payment Method': paymentData?.type,
    'Location': 'Menunggu Pembayaran',
  })
}

export const mixpanelCancelOrder = (selectedReason) =>
  mixpanelTrack('Batalkan Pesanan', {
    Reason: selectedReason,
    Location: 'Menunggu Pembayaran',
  })

export const mixpanelSeeHowToPay = (paymentData) =>
  mixpanelTrack('Lihat Cara Bayar', {
    'Payment Method': paymentData?.type,
    'Location': 'Menunggu Pembayaran',
  })

export const mixpanelSearchTransaction = (keyword) =>
  mixpanelTrack('Search Transaksi', { Keyword: keyword })

export const mixpanelSeeReview = (mixpanelData) => {
  const itemDesc = mixpanelData.find((item) => item.description)
  const itemImage = mixpanelData.find((item) => item.review_images)

  mixpanelTrack('Lihat Ulasan', {
    'Item Name': mixpanelData.map((item) => item.product_name),
    'Item ID': mixpanelData.map((item) => item.sku),
    'Rating': mixpanelData.map((item) => item.rating),
    'Review': !isEmpty(itemDesc) ? 'true' : 'false',
    'Image': !isEmpty(itemImage) ? 'true' : 'false',
  })
}

export const mixpanelAddUpdateReview = (eventName, mixpanelData) => {
  const mixpanel = {
    'Location': 'Ulasan',
    'Item ID': mixpanelData?.map((item) => item.sku) || 'none',
    'Item Name': mixpanelData?.map((item) => item.product_name) || 'none',
    'Transaction': mixpanelData?.map((item) => item.shopping_type),
  }

  mixpanelTrack(eventName, mixpanel)
}

export const mixpanelFilterReviewRating = (filterParamName, selectedFilter) => {
  let mixpanelName = ''
  let mixpanelPayload = null
  switch (filterParamName) {
    case 'shopping_type':
      mixpanelName = 'Click Filter Metode Belanja'
      mixpanelPayload = { Method: selectedFilter }
      break
    case 'date':
      mixpanelName = 'Filter Periode Transaksi'
      mixpanelPayload = { 'Periode Transaksi': selectedFilter }
      break
  }
  if (mixpanelName && mixpanelPayload && !isEmpty(selectedFilter)) {
    mixpanelTrack(mixpanelName, mixpanelPayload)
  }
}

export const mixpanelFilterOnlineTransaction = (
  filterParamName,
  selectedFilter,
) => {
  let mixpanelName = ''
  let mixpanelPayload = null
  switch (filterParamName) {
    case 'status':
      mixpanelName = 'Click Filter Status Transaksi'
      mixpanelPayload = { Status: selectedFilter }
      break
    case 'date':
      mixpanelName = 'Click Filter Tanggal Transaksi'
      mixpanelPayload = { Durasi: selectedFilter }
      break
    case 'brand_id':
      mixpanelName = 'Search and Click Filter Official Store'
      mixpanelPayload = { Name: selectedFilter }
      break
  }
  if (mixpanelName && mixpanelPayload && !isEmpty(selectedFilter)) {
    mixpanelTrack(mixpanelName, mixpanelPayload)
  }
}
