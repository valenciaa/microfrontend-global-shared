const getModifiedUrlLinkFlashSale = (link, wrapperCompanyCode) => {
  const azko = `azko/${link}` // ACE_HARDWARE
  const informa = `informa/${link}`
  const toyskingdomonline = `toyskingdomonline/${link}`

  return wrapperCompanyCode === 'AHI'
    ? azko // ACE_HARDWARE
    : wrapperCompanyCode === 'HCI'
      ? informa
      : wrapperCompanyCode === 'TGI'
        ? toyskingdomonline
        : link
}

const GetBestDealsLink = (
  companyCode,
  device,
  identifier,
  link,
  wrapperCompanyCode,
) => {
  // Default Flash Sale ODI
  let ruparupa = `https://www.ruparupa.com/best-deals.html?board+favorite+category${
    device === 'mobile' ? '+mobile' : ''
  }=best+deal`
  if (companyCode === 'ODI') {
    if (identifier && identifier.split('-').length >= 3) {
      ruparupa = `https://www.ruparupa.com/${getModifiedUrlLinkFlashSale(
        link,
        wrapperCompanyCode,
      )}?board+favorite+category+${identifier.split('-')[2]}${
        device === 'mobile' ? '+mobile' : ''
      }=best+deal`
    }
  }
  const ace = `https://www.ruparupa.com/azko/best-deal-1.html?board+favorite+category+azko${
    device === 'mobile' ? '+mobile' : ''
  }=best+deal` // ACE_HARDWARE
  const informa = `https://www.ruparupa.com/informa/best-deal.html?board+favorite+category+informa${
    device === 'mobile' ? '+mobile' : ''
  }=best+deal`
  const toyskingdomonline = `https://www.ruparupa.com/toyskingdomonline/best-deals.html?board+favorite+category+toys${
    device === 'mobile' ? '+mobile' : ''
  }=best+deal`

  return companyCode === 'AHI'
    ? ace // ACE_HARDWARE
    : companyCode === 'HCI'
      ? informa
      : companyCode === 'TGI'
        ? toyskingdomonline
        : ruparupa
}

export default GetBestDealsLink
