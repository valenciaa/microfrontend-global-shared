import GetParam from './GetParam'
import { mixpanelTrack } from './MixpanelWrapper'

const AnalyticsTrackCmsBlock = (
  payload,
  mixpanelTrackName,
  itmSource,
  mixpanelPropertiesLocation,
) => {
  if (mixpanelTrackName) {
    let mixpanelPayload = {
      Title: payload?.title, //eslint-disable-line
    }

    switch (mixpanelTrackName) {
      case 'Official Partner Ruparupa':
        mixpanelPayload = {
          Title: payload?.title || 'None',
          Location: mixpanelPropertiesLocation,
        }
        break
      case 'Koleksi Brand Ruparupa':
        mixpanelPayload = {
          'Name': payload?.title, //eslint-disable-line
          'Page Referrer': 'PDP',
          'Location': mixpanelPropertiesLocation,
        }
        break
      case 'Promo Partner':
        mixpanelPayload = {
          'Title': payload?.url_key, //eslint-disable-line
          'Page Referrer': 'PDP',
          'Location': mixpanelPropertiesLocation,
        }
        break
      case 'Belanja Berdasarkan Kategori':
        mixpanelPayload = {
          'Page Title': payload?.title || 'None',
          'ITM Campaign': GetParam('itm_campaign') || 'None',
          'ITM Source': itmSource || 'None',
          'Location': mixpanelPropertiesLocation,
        }
        break
      case 'Pilihan Para Ruppers':
        mixpanelPayload = {
          'Page Title': payload?.title || 'None',
        }
        break
    }

    mixpanelTrack(mixpanelTrackName, mixpanelPayload)
  }
}
export default AnalyticsTrackCmsBlock
