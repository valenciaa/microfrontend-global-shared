/**
 * Thanks for Luke Edwards
 * @url https://github.com/lukeed/clsx
 *
 * Example
 * 1. clsx('foo', true && 'bar', 'baz');
 * 2. clsx({ foo:true, bar:false, baz:isTrue() });
 * 3. clsx({ foo:true }, { bar:false }, null, { '--foobar':'hello' });
 * 4. clsx(['foo', 0, false, 'bar']);
 * 5. clsx(['foo'], ['', 0, false, 'bar'], [['baz', [['hello'], 'there']]]);
 * 6. clsx('foo', [1 && 'bar', { baz:false, bat:null }, ['hello', ['world']]], 'cya');
 */

function toVal(mix) {
  var k,
    y,
    str = ''

  if (typeof mix === 'string' || typeof mix === 'number') {
    str += mix
  } else if (typeof mix === 'object') {
    if (Array.isArray(mix)) {
      for (k = 0; k < mix.length; k++) {
        if (mix[k]) {
          if ((y = toVal(mix[k]))) {
            str && (str += ' ')
            str += y
          }
        }
      }
    } else {
      for (k in mix) {
        if (mix[k]) {
          str && (str += ' ')
          str += k
        }
      }
    }
  }

  return str
}

export default function clsx() {
  var i = 0,
    tmp,
    x,
    str = ''
  while (i < arguments.length) {
    if ((tmp = arguments[i++])) {
      if ((x = toVal(tmp))) {
        str && (str += ' ')
        str += x
      }
    }
  }
  return str
}
