import config from '../../../config'
import { GetParam, ItmConverter } from '.'
import ItmCompanyName from './ItmCompanyName'
import { useVueContext } from '../context/VueContext'

// source = itm-source
// sourceCampaign, used if itm-source doesn't consist itm-campaign
// title = itm-campaign
// addCompanyName = unused company anme at the first sentence

const VueItmGenerator = (
  link,
  term = null,
  title,
  location,
  pcpCategory = null,
  useKeyword,
  replacementItmSource, // Only used in vue reco in minicart page
  isPdpRedirect,
) => {
  const pageLocation = location
  const companyName = ItmCompanyName()
  const vueContext = useVueContext() || {}

  let itmType = 'also-bought'
  let itmCampaign = 'pdp-product-recommendation'

  if (title?.toLowerCase()?.includes('suka')) {
    itmType = 'related'
  }
  if (location === 'popupcart') {
    itmCampaign = 'popupcart-product-recommendation'
  }

  if (
    location?.toLowerCase() === 'cat' ||
    location?.toLowerCase() === 'empty-search'
  ) {
    itmType = ''
    location = 'pcp'
    if (isPdpRedirect) {
      let tempItm = '-redirect'

      if (title?.toLowerCase()?.includes('terakhir')) {
        tempItm = '-recent'
      }
      location += tempItm
    }
    const searchTerms = ['terlaris', 'populer']
    const isMatch = searchTerms.some((term) =>
      title?.toLowerCase()?.includes(term),
    )

    if (isMatch) {
      itmType = 'popular'
    }
    itmCampaign =
      pcpCategory && typeof pcpCategory === 'string'
        ? pcpCategory.replace(/\s/g, '-').toLowerCase()
        : ''

    if (useKeyword) {
      itmCampaign = 'direct-search'
      location = 'search-result'
    }
  } else if (location?.toLowerCase() === 'search') {
    itmType = ''
    itmCampaign = 'direct-search'
    location += '-result'
  } else if (location?.toLowerCase() === 'pcp brand') {
    itmType = ''
    itmCampaign = 'direct-search'
    location = 'search-result'
  } else if (location === 'microsite') {
    itmType = ''
    location = vueContext?.micrositeName || ''
  }

  let itmSource = `product-recommendation${
    itmType ? `-${itmType}` : ''
  }-${location?.toLowerCase()}-${companyName || 'rr'}`

  let itmUrl
  let symbol = '?'
  let campaignValue = itmCampaign
  if (isPdpRedirect) {
    campaignValue = GetParam('itm_campaign')
  }
  if (pageLocation === 'microsite') {
    campaignValue = vueContext?.micrositeName || ''
  }

  if (link?.trim()?.includes('?')) {
    symbol = '&'
  }

  itmUrl = link?.trim()
  if (location === 'rewards') {
    itmSource = 'profile-page-recommendation-rrr'
    campaignValue = 'ruparuparewards'
    itmUrl += `${symbol}location=Homepage RRRewards`
    itmUrl += '&section=Rekomendasi Spesial Untukmu'
    itmUrl += '&source=Vue'
    symbol = '&'
  } else if (location === 'cart' && replacementItmSource) {
    itmSource = replacementItmSource
  }

  if (location === 'wishlist') {
    itmSource = replacementItmSource
    campaignValue = 'wishlist-product-recommendation'
  }

  itmUrl += `${symbol}itm_source=${itmSource}`
  itmUrl += `&itm_campaign=${campaignValue}`
  if (term) {
    itmUrl += `&itm_term=${ItmConverter(term)}`
  }
  itmUrl += `&itm_device=${config.environment}`

  if (pageLocation?.toLowerCase() === 'cat' && useKeyword) {
    itmUrl += '&location=PCP'
  }

  return encodeURI(itmUrl)
}
export default VueItmGenerator
