import isEmpty from 'lodash/isEmpty'

const ServicePriceCalculator = ({ additionalCharge, serviceParts }) => {
  const partsPrices = []
  if (!isEmpty(serviceParts)) {
    serviceParts.forEach((element) => {
      element?.service_price && partsPrices.push(element.service_price)
    })
    if (!isEmpty(partsPrices)) {
      return (
        parseFloat(additionalCharge) +
        partsPrices.reduce((sum, a) => sum + a, 0)
      )
    } else if (!isEmpty(additionalCharge)) {
      return parseFloat(additionalCharge)
    }
  }
}
export default ServicePriceCalculator
