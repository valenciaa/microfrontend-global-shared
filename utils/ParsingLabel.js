export const parsingLabel = (label) => {
  const labels = label.split('|||')

  const parsedLabel = labels.length ? labels[0] : ''
  return {
    parsedLabel,
  }
}
