export const getMixpanelLocationProperty = (from) => {
  if (from === 'home') {
    return 'Homepage'
  }
  if (from === 'product-detail') {
    return 'PDP'
  }
  if (['brand', 'catalog', 'jual', 'store'].includes(from)) {
    return 'PCP'
  }
}
