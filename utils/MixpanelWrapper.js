import mixpanel from 'mixpanel-browser'
import config from '../../../config'
import Cookies from 'js-cookie'
import isEmpty from 'lodash/isEmpty'
import GetParam from './GetParam'
import IsLocalStorageAvailable from './IsLocalStorageAvailable'
import { urlToObjectConverter, vueTitleSection } from './VueAIUtil'
import { getIPAddress } from './GetIpAddress'
import { getUserAgent } from './GetUserAgent'
import { DEFAULT_PROMO } from '../container/pcp/filterPromo'

const dayjs = require('dayjs')
export const generateTrackingPayload = (payload, itemPayload) => {
  let result = payload

  if (itemPayload) {
    result = {
      ...result,
      ...itemPayload,
    }
  }

  return result
}

export const convertUrlToString = (string) => {
  let tempString = string
  let result = ''
  tempString = tempString.replace(/-+/g, ' ').split(' ')

  tempString.forEach((element, index) => {
    tempString[index] = element.charAt(0).toUpperCase() + element.slice(1)
    result += tempString[index] + ' '
  })

  return result
}

export const mixpanelInit = () => {
  if (config.mixpanelToken) {
    mixpanel.init(config.mixpanelToken, {
      persistence: 'localStorage',
      persistence_name: 'DATA',
    })
  }
}

export const mixpanelLogout = () => {
  mixpanel.reset()

  mixpanelSuperPropsInit()
}

export const mixpanelIdentify = (customerId) => {
  if (config.mixpanelToken && customerId) {
    mixpanel.identify(customerId)

    mixpanel.people.set({
      $moengage_user_id: customerId,
    })
  }
}

export const mixpanelSuperPropsInit = (user) => {
  if (typeof window !== 'undefined') {
    if (window.myIpAddress) {
      mixpanel.register({
        'Device': config.environment === 'desktop' ? 'Desktop' : 'Mobile Web',
        'Company Code': config.companyCode,
        'Web Source': handleGetSource(),
        // 'ITM Source': GetParam('itm_source') && GetParam('itm_source') !== 'undefined' ? GetParam('itm_source') : 'None',
        // 'ITM Term': GetParam('itm_term') && GetParam('itm_term') !== 'undefined' ? GetParam('itm_term') : 'None',
        // 'ITM Campaign': GetParam('itm_campaign') && GetParam('itm_campaign') !== 'undefined' ? GetParam('itm_campaign') : 'None',
        // 'ITM Referral': GetParam('itm_referral') && GetParam('itm_referral') !== 'undefined' ? GetParam('itm_referral') : 'None',
        // 'Last Touch UTM Source': GetParam('utm_source') || 'None',
        // 'Last Touch UTM Medium': GetParam('utm_medium') || 'None',
        // 'Last Touch UTM Campaign': GetParam('utm_campaign') || 'None',
        // 'Last Touch UTM Content': GetParam('utm_content') || 'None',
        'Customer ID': user?.customer_id || mixpanel.get_distinct_id(),
        'Customer Email': user?.email || 'None',
        'Moengage User ID': user?.customer_id || mixpanel.get_distinct_id(),
        'Member Level': user && user.member_id ? user.level_text : '',
        'IP': getIPAddress(),
        'User Agent': getUserAgent(),
      })
    }
  }
}

export const mixpanelTrack = (eventName, payload, additionalParams) => {
  if (config.mixpanelToken && mixpanel && mixpanel.track) {
    const utmSource =
      GetParam('utm_source') ||
      Cookies.get('mp-last-touch-utm-source') ||
      'None'
    const utmMedium =
      GetParam('utm_medium') ||
      Cookies.get('mp-last-touch-utm-medium') ||
      'None'
    const utmCampaign =
      GetParam('utm_campaign') ||
      Cookies.get('mp-last-touch-utm-campaign') ||
      'None'
    const utmContent =
      GetParam('utm_content') ||
      Cookies.get('mp-last-touch-utm-content') ||
      'None'

    const cookiesConfig = {
      expires: dayjs().add(30, 'minute').toDate(),
      domain: config.isLiveSite ? '.ruparupa.com' : '.ruparupastg.my.id',
    }

    if (utmSource !== 'None') {
      Cookies.set('mp-last-touch-utm-source', utmSource, cookiesConfig)
    }

    if (utmMedium !== 'None') {
      Cookies.set('mp-last-touch-utm-medium', utmMedium, cookiesConfig)
    }

    if (utmCampaign !== 'None') {
      Cookies.set('mp-last-touch-utm-campaign', utmCampaign, cookiesConfig)
    }

    if (utmContent !== 'None') {
      Cookies.set('mp-last-touch-utm-content', utmContent, cookiesConfig)
    }

    let payloadTrack = {
      ...(payload || {}),
      'ITM Term': !isEmpty(payload?.['ITM Term'])
        ? payload['ITM Term']
        : GetParam('itm_term') && GetParam('itm_term') !== 'undefined'
          ? GetParam('itm_term')
          : 'none',
      'ITM Source': !isEmpty(payload?.['ITM Source'])
        ? payload['ITM Source']
        : GetParam('itm_source') && GetParam('itm_source') !== 'undefined'
          ? GetParam('itm_source')
          : 'none',
      'ITM Device': !isEmpty(payload?.['ITM Device'])
        ? payload['ITM Device']
        : GetParam('itm_device') && GetParam('itm_device') !== 'undefined'
          ? GetParam('itm_device')
          : 'none',
      'ITM Campaign': !isEmpty(payload?.['ITM Campaign'])
        ? payload['ITM Campaign']
        : GetParam('itm_campaign') && GetParam('itm_campaign') !== 'undefined'
          ? GetParam('itm_campaign')
          : 'none',
    }

    if (['VUE', 'PBrand'].includes(additionalParams)) {
      payloadTrack = {
        ...(payload || {}),
      }
      additionalParams = {}
    }

    mixpanel.track(eventName, {
      ...(payloadTrack || {}),
      'ITM Referral': !isEmpty(payload?.['ITM Referral'])
        ? payload['ITM Referral']
        : GetParam('itm_referral') && GetParam('itm_referral') !== 'undefined'
          ? GetParam('itm_referral')
          : 'None',
      'Last Touch UTM Source': utmSource,
      'Last Touch UTM Medium': utmMedium,
      'Last Touch UTM Campaign': utmCampaign,
      'Last Touch UTM Content': utmContent,
      ...(additionalParams || {}),
      'Device': config.environment === 'desktop' ? 'Desktop' : 'Mobile Web', //eslint-disable-line
    })
  }
}

export const handleGetSource = () => {
  if (config.companyName) {
    switch (config.companyName) {
      case 'informastore':
        return 'Informa Store'
      case 'acestore':
        return 'Ace Store' //ACE_HARDWARE
      case 'informa':
        return 'Informa'
      case 'ace': //ACE_HARDWARE
        return 'Ace' //ACE_HARDWARE
      case 'toyskingdomonline':
        return 'Toys Kingdom'
      default:
        return 'RR Web'
    }
  }

  return 'RR Web'
}

export const handleMixpanelSetCurrentSession = () => {
  const expires = dayjs().add(30, 'minute').toDate()

  Cookies.set('mp-session-' + config.companyCode, '1', {
    expires,
    domain: config.isLiveSite ? '.ruparupa.com' : '.ruparupastg.my.id',
  })

  Cookies.set('mp-session-global', '1', {
    expires,
    domain: config.isLiveSite ? '.ruparupa.com' : '.ruparupastg.my.id',
  })
}

export const mixpanelGetCurrentSession = () => {
  const session = Cookies.get('mp-session-' + config.companyCode)
  const globalSession = Cookies.get('mp-session-global')
  const urlGclidParam = GetParam('gclid') || 'None'

  if (!globalSession) {
    mixpanelTrack('Session Start Global', { Gclid: urlGclidParam })
  }

  if (!session) {
    switch (config.companyCode) {
      case 'HCI':
        mixpanelTrack('Session Start Informa', { Gclid: urlGclidParam })
        break
      case 'AHI':
        mixpanelTrack('Session Start ACE', { Gclid: urlGclidParam }) //ACE_HARDWARE
        break
      case 'TGI':
        mixpanelTrack('Session Start Toys Kingdom', { Gclid: urlGclidParam })
        break
      default:
        mixpanelTrack('Session Start', { Gclid: urlGclidParam })
        break
    }
  }

  handleMixpanelSetCurrentSession()
}

export const mixpanelConstructLink = (pageFrom) => {
  let redirectLink = 'https://ruparupamobileapp.page.link?link='
  let page = ''

  switch (pageFrom) {
    case 'product-detail':
      page = 'pdp/'
      break
    case 'catalog':
      page = 'pcp/'
      break
    case 'tahu':
      page = 'promopage/'
      break
    case 'home':
      page = 'home'
      break
    default:
      page = ''
      break
  }

  if (page) {
    const currentUrl = window.location.href.split(config.baseURL)
    const paramExist = window.location.href.indexOf('?') !== -1
    const utmExist = window.location.href.indexOf('utm') !== -1
    const jualExist = window.location.href.indexOf('/jual/') !== -1
    const concatLink = pageFrom === 'pcp/' ? currentUrl && currentUrl[1] ? config.baseURL + currentUrl[1] : '':
      currentUrl && currentUrl[1] ? config.baseURL + page + currentUrl[1] : ''

    try {
      if (utmExist) {
        if (jualExist || page === 'home') {
          redirectLink += encodeURIComponent(window.location.href)
        } else {
          redirectLink += encodeURIComponent(concatLink)
        }
      } else {
        if (
          config.environment === 'mobile' &&
          ['home', 'product-detail'].includes(pageFrom)
        ) {
          // this block of logics is needed to track utm if they don't exist in url but exist in Cookies for when user want to register or activate ruparupa account
          const lastTouchUTMSource = Cookies.get('mp-last-touch-utm-source')
          const lastTouchUTMMedium = Cookies.get('mp-last-touch-utm-medium')
          const lastTouchUTMCampaign = Cookies.get('mp-last-touch-utm-campaign')

          const utmGroup = {
            ...(lastTouchUTMSource && { utm_source: lastTouchUTMSource }),
            ...(lastTouchUTMMedium && { utm_medium: lastTouchUTMMedium }),
            ...(lastTouchUTMCampaign && { utm_campaign: lastTouchUTMCampaign }),
          }

          if (!isEmpty(utmGroup)) {
            redirectLink = 'https://ruparupa.page.link?link='
            const utmParams = new URLSearchParams(utmGroup)
            // we must put the utmParams inside the bracket so they will be delivered to moapp inside query param 'link' since moapp read all the link sent to them inside 'link'
            if (jualExist || page === 'home') {
              redirectLink += encodeURIComponent(
                window.location.href + `&${utmParams.toString()}`,
              )
            } else {
              redirectLink += encodeURIComponent(
                concatLink + `${paramExist ? '&' : '?'}${utmParams.toString()}`,
              )
            }
          } else {
            redirectLink = ''
          }
        } else {
          redirectLink = ''
        }
      }
    } catch (e) {
      redirectLink = ''
    }
  } else {
    redirectLink = ''
  }

  return (
    redirectLink || 'https://ruparupa.onelink.me/xDlg/appdownloadfrommobile'
  )
}

export const mixpanelTrackCart = (items, additionalProps) => {
  const itemName = []
  const itemCategory1 = []
  const itemCategory2 = []
  const itemCategory3 = []
  const itemCategory4 = []
  const itemPrice = []
  const itemQty = []
  const itemId = []
  const itemBrand = []
  const itemColour = []
  const stockAvailable = []
  const discount = []
  const productLabel = []
  const existProductLabel = []
  const packageName = []
  const packagePrice = []
  const packageId = []
  const isVoucher = []
  const promoMarketing = []

  if (items) {
    items.map((item) => {
      let isDiscount = false

      if (
        item.prices &&
        item.prices.selling_price &&
        item.prices.normal_price
      ) {
        isDiscount = item.prices.selling_price !== item.prices.normal_price
      }

      if (item?.package?.package_id !== 0) {
        // item?.package?.sku_detail.map(detail => {
        //   isDiscount = detail.promo_price !== detail.normal_price
        //   itemId.push(detail.sku)
        //   itemName.push(detail.name || 'None')
        //   itemCategory1.push(detail.breadcrumbs && detail.breadcrumbs[0] ? detail.breadcrumbs[0] : 'None')
        //   itemCategory2.push(detail.breadcrumbs && detail.breadcrumbs[1] ? detail.breadcrumbs[1] : 'None')
        //   itemCategory3.push(detail.breadcrumbs && detail.breadcrumbs[2] ? detail.breadcrumbs[2] : 'None')
        //   itemCategory4.push(detail.breadcrumbs && detail.breadcrumbs[3] ? detail.breadcrumbs[3] : 'None')
        //   itemPrice.push(detail.promo_price ? detail.promo_price : 0)
        //   itemQty.push(detail.qty_ordered || 0)
        //   itemBrand.push(detail.brand || 'None')
        //   itemColour.push(detail.attributes && detail.attributes[0] && detail.attributes[0].attribute_value ? detail.attributes[0].attribute_value : 'Mix')
        //   stockAvailable.push(detail.max_qty !== 0)
        //   discount.push(isDiscount)
        //   productLabel.push(handleGetProductLabel(detail.label))
        //   existProductLabel.push(handleGetProductLabel(detail.label, 'bool'))
        // })
        isDiscount = item.promo_price !== item.normal_price
        itemId.push(item.sku)
        itemName.push(item.name || 'None')
        itemCategory1.push(
          item.breadcrumbs && item.breadcrumbs[0]
            ? item.breadcrumbs[0]
            : 'None',
        )
        itemCategory2.push(
          item.breadcrumbs && item.breadcrumbs[1]
            ? item.breadcrumbs[1]
            : 'None',
        )
        itemCategory3.push(
          item.breadcrumbs && item.breadcrumbs[2]
            ? item.breadcrumbs[2]
            : 'None',
        )
        itemCategory4.push(
          item.breadcrumbs && item.breadcrumbs[3]
            ? item.breadcrumbs[3]
            : 'None',
        )
        itemPrice.push(item.promo_price ? item.promo_price : 0)
        itemQty.push(item.qty_ordered || 0)
        itemBrand.push(item.brand || 'None')
        itemColour.push(
          item.attributes &&
            item.attributes[0] &&
            item.attributes[0].attribute_value
            ? item.attributes[0].attribute_value
            : 'Mix',
        )
        stockAvailable.push(item.max_qty !== 0)
        discount.push(isDiscount)
        productLabel.push(handleGetProductLabel(item.label))
        existProductLabel.push(handleGetProductLabel(item.label, 'bool'))
        if (!packageName.includes(item?.package?.package_name)) {
          packageName.push(item?.package?.package_name)
          packagePrice.push(item?.package?.grand_total)
          packageId.push(item?.package?.package_id)
          isVoucher.push(!isEmpty(item?.package?.voucher_code))
          promoMarketing.push(!isEmpty(item?.package?.tnc))
        }
      } else {
        itemId.push(item.sku || 'None')
        itemName.push(item.name || 'None')
        itemCategory1.push(
          item.breadcrumbs && item.breadcrumbs[0]
            ? item.breadcrumbs[0]
            : 'None',
        )
        itemCategory2.push(
          item.breadcrumbs && item.breadcrumbs[1]
            ? item.breadcrumbs[1]
            : 'None',
        )
        itemCategory3.push(
          item.breadcrumbs && item.breadcrumbs[2]
            ? item.breadcrumbs[2]
            : 'None',
        )
        itemCategory4.push(
          item.breadcrumbs && item.breadcrumbs[3]
            ? item.breadcrumbs[3]
            : 'None',
        )
        itemPrice.push(
          item.prices && item.prices.selling_price
            ? item.prices.selling_price
            : 0,
        )
        itemQty.push(item.qty_ordered || 0)
        itemBrand.push(item.brand || 'None')
        itemColour.push(
          item.attributes &&
            item.attributes[0] &&
            item.attributes[0].attribute_value
            ? item.attributes[0].attribute_value
            : 'Mix',
        )
        stockAvailable.push(item.max_qty !== 0)
        discount.push(isDiscount)
        productLabel.push(handleGetProductLabel(item.label))
        existProductLabel.push(handleGetProductLabel(item.label, 'bool'))
      }
    })

    let track = {
      'Item Name': itemName,
      'Item Category LV 1': itemCategory1,
      'Item Category LV 2': itemCategory2,
      'Item Category LV 3': itemCategory3,
      'Item Category LV 4': itemCategory4,
      'Item Price': itemPrice,
      'Item Quantity': itemQty,
      'Item ID': itemId,
      'Item Brand': itemBrand,
      'Item Colour': itemColour,
      'Stock Available?': stockAvailable,
      'Discount?': discount,
      'Product Label': productLabel,
      'Product Label?': existProductLabel,
    }

    if (!isEmpty(packageName)) {
      track = {
        ...track,
        'Package name': packageName,
        'Package Price': packagePrice,
        'Package ID': packageId,
        'Is Voucher?': isVoucher,
        'Promo Marketing?': promoMarketing,
      }
    }

    if (additionalProps) {
      track = {
        ...track,
        ...additionalProps,
      }
    }

    mixpanelTrack('Lanjut Ke Keranjang', track)
  }
}

export const mixpanelTrackCartItem = (items, additionalProps) => {
  items &&
    items.map((item) => {
      let isDiscount = false

      if (
        item.prices &&
        item.prices.selling_price &&
        item.prices.normal_price
      ) {
        isDiscount = item.prices.selling_price !== item.prices.normal_price
      }

      if (item?.package?.package_id !== 0) {
        item?.package?.sku_detail?.map((detail) => {
          isDiscount = detail.promo_price !== detail.normal_price
          let track = {
            'Item Name': detail.name || 'None',
            'Item Category LV 1':
              detail.breadcrumbs && detail.breadcrumbs[0]
                ? detail.breadcrumbs[0]
                : 'None',
            'Item Category LV 2':
              detail.breadcrumbs && detail.breadcrumbs[1]
                ? detail.breadcrumbs[1]
                : 'None',
            'Item Category LV 3':
              detail.breadcrumbs && detail.breadcrumbs[2]
                ? detail.breadcrumbs[2]
                : 'None',
            'Item Category LV 4':
              detail.breadcrumbs && detail.breadcrumbs[3]
                ? detail.breadcrumbs[3]
                : 'None',
            'Item Price': detail.promo_price ? detail.promo_price : 0,
            'Item Quantity': detail.qty_ordered || 0,
            'Item ID': detail.sku || 'None',
            'Item Brand': detail.brand || 'None',
            'Item Colour':
              detail.attributes &&
              detail.attributes[0] &&
              detail.attributes[0].attribute_value
                ? detail.attributes[0].attribute_value
                : 'Mix',
            'Stock Available?': detail.max_qty !== 0,
            'Discount?': isDiscount,
            'Product Label': handleGetProductLabel(detail.label),
            'Product Label?': handleGetProductLabel(detail.label, 'bool'),
            'Subtotal': detail.qty_ordered * detail.promo_price, // eslint-disable-line
            'Package name': item?.package?.package_name,
            'Package Price': item?.package?.grand_total,
            'Package ID': item?.package?.package_id,
            'Is Voucher?': !isEmpty(item?.package?.voucher_code),
            'Promo Marketing?': !isEmpty(item?.package?.tnc),
          }

          if (additionalProps) {
            track = {
              ...track,
              ...additionalProps,
            }
          }

          mixpanelTrack('Lanjut Ke Keranjang Item', track)
        })
      } else {
        let track = {
          'Item Name': item.name || 'None',
          'Item Category LV 1':
            item.breadcrumbs && item.breadcrumbs[0]
              ? item.breadcrumbs[0]
              : 'None',
          'Item Category LV 2':
            item.breadcrumbs && item.breadcrumbs[1]
              ? item.breadcrumbs[1]
              : 'None',
          'Item Category LV 3':
            item.breadcrumbs && item.breadcrumbs[2]
              ? item.breadcrumbs[2]
              : 'None',
          'Item Category LV 4':
            item.breadcrumbs && item.breadcrumbs[3]
              ? item.breadcrumbs[3]
              : 'None',
          'Item Price':
            item.prices && item.prices.selling_price
              ? item.prices.selling_price
              : 0,
          'Item Quantity': item.qty_ordered || 0,
          'Item ID': item.sku || 'None',
          'Item Brand': item.brand || 'None',
          'Item Colour':
            item.attributes &&
            item.attributes[0] &&
            item.attributes[0].attribute_value
              ? item.attributes[0].attribute_value
              : 'Mix',
          'Stock Available?': item.max_qty !== 0,
          'Discount?': isDiscount,
          'Product Label': handleGetProductLabel(item.label),
          'Product Label?': handleGetProductLabel(item.label, 'bool'),
          'Subtotal': item.qty_ordered * item.prices.selling_price, // eslint-disable-line
        }

        if (additionalProps) {
          track = {
            ...track,
            ...additionalProps,
          }
        }

        mixpanelTrack('Lanjut Ke Keranjang Item', track)
      }
    })
}

export const mixpanelTrackShareButton = (
  location,
  mediaName,
  item = 'None',
  additionalValue = false,
) => {
  let additionalAtt = {}
  if (
    !isEmpty(additionalValue) &&
    additionalValue?.pageFroms === 'shop-the-look'
  ) {
    additionalAtt = {
      'Section Title': additionalValue?.product?.sectionTitle,
    }
  }
  mixpanelTrack('Social Share Button', {
    'Location': location,
    'Item Name': item,
    'Social Media Name': mediaName,
    ...additionalAtt,
  })
}

const handleGetProductLabel = (label, type = '') => {
  if (type === '') {
    return label && label.ODI !== ''
      ? label.ODI.split('|')[0] || 'Buy 1 Get 1'
      : 'None'
  } else {
    return label && label.ODI !== ''
  }
}

export const mixpanelPCPTrack = (
  eventName,
  propertyValue,
  additionalProps = {},
) => {
  if (
    window &&
    window.location &&
    window.location.href &&
    window.location.href.replace(config.baseURL, '')
  ) {
    const url = window.location.href
    let pcpType = 'Category'

    if (url.indexOf('/brands/') > -1) {
      pcpType = 'Brands'
    } else if (url.indexOf('/store/') > -1) {
      pcpType = 'Store'
      propertyValue = 'Store'
    } else if (url.indexOf('/jual/') > -1) {
      pcpType = 'Search'
      propertyValue = 'Search'
    }

    const referrerUrl = document.referrer
    if (referrerUrl.includes('/jual/')) {
      pcpType = 'Search'
      propertyValue = 'Search'
    }

    if (referrerUrl.includes('/brands/')) {
      pcpType = 'PCP Brand'
    }

    let itmSource = GetParam('itm_source') || 'None'

    if (itmSource?.includes('product-category-recommendation-homepage')) {
      itmSource = itmSource.replace('product-', '')
    }

    if (GetParam('st')) {
      pcpType = 'Redirect-Category'
    }

    if (itmSource?.includes('megamenu')) {
      pcpType = 'Category Megamenu'
    }

    let payload = {
      'Name': propertyValue || 'None',
      'Title': url.replace(config.baseURL, ''),
      'PCP Type': pcpType || 'None',
      'ITM Device': config.environment,
      'ITM Source': itmSource,
    }

    if (additionalProps) {
      payload = {
        ...payload,
        ...additionalProps,
      }
    }

    mixpanelTrack(eventName, payload)
  }
}

export const mixpanelPcpEmarsysRecommendationTrack = (title, item) => {
  const sellingPrice = item.msrp || 0
  const itemPrice = item.price || sellingPrice

  const data = {
    'Source': 'Emarsys',
    'Section': title,
    'Location': 'PCP',
    'Item Name': (item.title && item.title.toLowerCase()) || 'None',
    'Item Price': itemPrice,
    'Selling Price': sellingPrice,
    'Discount?': sellingPrice !== itemPrice,
    'Item Price Discount%': item.discount || 0,
    'Item ID': item.id || 'None',
    'Item Brand': item.brand || 'None',
    'Stock Available?': item.available || false,
    'Instant Delivery?': item.c_instant_delivery === 'TRUE' || false,
    'Promo?': sellingPrice !== itemPrice,
    'Discount Amount': sellingPrice - itemPrice,
    'Is Rating & Review':
      item.c_review_count && parseInt(item.c_review_count) > 0,
  }

  mixpanelTrack('View PDP Recommendation', data)
}

export const mixpanelLogin = (user, location) => {
  if (user?.customer_id) {
    mixpanelIdentify(user.customer_id)
    mixpanelSuperPropsInit(user)

    const profile = {
      '$name':
        (user?.name || user?.first_name) +
        (user?.last_name ? ` ${user.last_name}` : ''),
      '$email': user?.email,
      'Birth Date':
        (user?.birth_date && dayjs(user.birth_date).format('YYYY-MM-DD')) ||
        'Unknown',
      'Gender': user?.gender || 'male', //eslint-disable-line
      'Phone Number': user?.phone,
      '$moengage_user_id': user.customer_id, //eslint-disable-line
      'Member Level': user && user.member_id ? user.level_text : '',
    }

    mixpanel.people.set(profile)
  }

  const date = new Date()

  const data = {
    'Name':
      (user?.name || user?.first_name) +
      (user?.last_name ? ` ${user.last_name}` : ''),
    'Email': user?.email,
    'Phone Number': user?.phone,
    'Gender': user?.gender || 'male',
    'Login Method': user?.registered_by,
    'Login Date': date.toLocaleString('id-ID'),
    'Birth Date':
      (user?.birth_date && dayjs(user.birth_date).format('YYYY-MM-DD')) ||
      'Unknown',
    'Location': location,
  }

  mixpanelTrack('Login', data)
}

export const mixpanelVueRecommendationTrack = (
  pageFrom,
  title,
  type = 'PDP',
  item,
  moduleName,
  newUrl,
) => {
  if (pageFrom === 'microsite') {
    moduleName = vueTitleSection(moduleName || '')
  }
  let itmCollection = urlToObjectConverter(newUrl) || ''
  //** itmCollection type object contain = itm_campaign,itm_device,itm_source,itm_term */

  const sellingPrice = item?.msrp || 0
  const itemPrice = item?.price || sellingPrice

  if (type === 'popupcart') {
    pageFrom = 'Popup Cart'
    type = 'Popup Cart'
  }

  if (title === 'Terakhir Dilihat') {
    title = 'Produk terakhir dilihat'
  }

  if (pageFrom === 'microsite') {
    itmCollection.location = pageFrom
  }

  let data = {
    'Source': 'Vue',
    'Section': title,
    'Location': itmCollection?.location || pageFrom,
    'ITM Campaign':
      itmCollection?.itm_campaign || GetParam('itm_device') || 'None',
    'ITM Source': itmCollection?.itm_source || GetParam('itm_source') || 'None',
    'ITM Device':
      itmCollection?.itm_device ||
      config.environment ||
      GetParam('itm_device') ||
      'None',
    'ITM Term': item?.id || 'None',
    'Item Name': (item?.title && item?.title.toLowerCase()) || 'None',
    'Item Price': itemPrice,
    'Selling Price': sellingPrice,
    'Discount?': sellingPrice !== itemPrice,
    'Item Price Discount%': item?.discount || 0,
    'Item ID': item?.id || 'None',
    'Item Brand': item?.brand || 'None',
    // 'Stock Available?': item?.available || false,
    // 'Instant Delivery?': item?.c_instant_delivery === 'TRUE' || false,
    'Module Name': moduleName,
    'Strategy Name': item?.strategy_name,
    'Promo?': sellingPrice !== itemPrice,
    'Discount Amount': sellingPrice - itemPrice,
    // 'Is Rating & Review':
    //   item?.c_review_count && parseInt(item?.c_review_count) > 0
  }

  if (pageFrom === 'PDP') {
    data = {
      ...data,
      'Instant Delivery': item?.c_instant_delivery,
    }
  }

  /** All action from PDP, PCP, PopupCart will send event "View PDP Recommendation"
   * refers: RW-463
   */

  // mixpanelTrack(`View ${type} Recommendation`, data)

  let mixpanelEvent = 'PDP'
  if (title?.toLowerCase()?.includes('kategori')) {
    mixpanelEvent = 'PCP'
  }

  let vueKlkPayload = {}
  let additionalParams = 'VUE'
  if (pageFrom === 'rewards') {
    vueKlkPayload = {
      'ITM Term': item?.id,
      'ITM Source': 'profile-page-recommendation-rrr',
      'ITM Campaign': 'ruparuparewards',
      'Location': 'Homepage RRRewards',
      'Section': 'Rekomendasi Spesial Untukmu',
      'Source': 'Vue',
    }
    additionalParams = vueKlkPayload
  }
  let mixpanelEventName = `View ${mixpanelEvent} Recommendation`

  if (pageFrom === 'Error Page 404') {
    data = {
      ...data,
      'Error Message': 'Halaman Tidak Ditemukan',
      'Current URL': itmCollection?.dir || 'None',
      'ITM Term': itmCollection?.itm_term || 'None',
      'ITM Campaign': itmCollection?.itm_campaign || 'None',
      'ITM Source': itmCollection?.itm_source || 'None',
    }
    delete data.Location
    mixpanelEventName = pageFrom
  }

  mixpanelTrack(mixpanelEventName, data, additionalParams)
}

export const mixpanelRemoveCookie = () => {
  const mpCookie = Cookies.get(`mp_${config.mixpanelToken}_mixpanel`)

  if (mpCookie && IsLocalStorageAvailable()) {
    const domain = config.isLiveSite ? '.ruparupa.com' : '.ruparupastg.my.id'

    Cookies.remove(`mp_${config.mixpanelToken}_mixpanel`, { path: '/', domain })
  }
}

export const mixpanelPersonalised = (payload) => {
  mixpanelTrack('Click Salin Kode Voucher', payload)
}

export const mixpanelLoginKlk = (
  user,
  location = 'Login Page',
  socialMedia,
) => {
  if (user) {
    mixpanelIdentify(user?.customer_id)
    mixpanelSuperPropsInit(user)

    mixpanel.people.set({
      '$name': user?.name,
      '$email': user?.email,
      'Phone Number': user?.phone,
      'Birth Date': user?.birthday || 'Unknown',
      'Gender': user?.gender || 'male',
      '$moengage_user_id': user?.customer_id,
      'Member Level': user && user.member_id ? user.level_text : '',
    })

    const data = {
      'Name': user?.name,
      'Email': user?.email,
      'Phone Number': user?.phone,
      'Gender': user?.gender || 'male',
      'Login Method': socialMedia ? socialMedia : user?.registered_by,
      'Login Date': new Date().toISOString(),
      'Birth Date': user?.birthday || 'Unknown',
      'Location': location,
    }

    mixpanelTrack('Login', data)
  }
}

export const mixpanelRegisterKlk = (
  user,
  location = 'Register Page',
  socialMedia,
) => {
  if (user) {
    mixpanelIdentify(user.customer_id)
    mixpanelSuperPropsInit(user)

    mixpanel.people.set({
      '$name': user?.name,
      '$email': user?.email,
      'Phone Number': user?.phone,
      'Birth Date': user?.birthday || 'Unknown',
      'Gender': user?.gender || 'male',
      '$moengage_user_id': user?.customer_id,
      'Member Level': user && user.member_id ? user.level_text : '',
    })

    const data = {
      'Name': user?.name,
      'Email': user?.email,
      'Phone Number': user?.phone,
      'Gender': user?.gender || 'male',
      'Registration Date': new Date().toISOString(),
      'Registration Method': socialMedia
        ? socialMedia
        : user?.registered_by || '',
      'Birth Date': user?.birthday || 'Unknown',
      'Location': location,
    }

    mixpanelTrack('Register', data)
  }
}

export const mixpanelKlkTrack = (
  eventName,
  pageFrom,
  additionalValue,
  callback = () => {},
) => {
  let data = {
    Source: 'KLK',
    Location: pageFrom,
  }

  if (eventName.includes('Level Membership')) {
    data = {
      ...data,
      'Level Member': additionalValue,
    }
  }

  if (
    eventName.includes('Koin Page') ||
    eventName.includes('View Page Gabungkan') ||
    eventName.includes('Click Instant')
  ) {
    if (data.Location !== 'Click Button Voucher Merchant') {
      data = {
        ...data,
        Section: additionalValue,
      }
    }
  }

  if (eventName.includes('Click Survei')) {
    data = {
      ...data,
      'Survei ID': additionalValue?.id,
      'Jumlah Koin': additionalValue?.award,
    }
  }

  if (
    eventName.includes('Detail Voucher') ||
    eventName.includes('Belanja Sekarang') ||
    eventName.includes('Click Pakai Voucher')
  ) {
    let moduleName = 'Voucher'

    if (eventName.includes('Detail')) {
      moduleName = 'Use'
    }
    moduleName += ' Type'

    data = {
      ...data,
      'Voucher Name': additionalValue?.name,
      'Voucher Code': additionalValue?.voucher_code,
      [moduleName]: additionalValue?.type,
    }
  }

  if (eventName.includes('Lihat Semua')) {
    data = {
      ...data,
      'Section Name': additionalValue,
    }
  }

  if (
    [
      'View Page Gabungkan Rewards',
      'Gabungkan Rewards Pilih Akun',
      'View Page Verifikasi Gabungkan Rewards',
      'Lewati Page Gabungkan Rewards',
      'Success Gabungkan Rewards',
    ].includes(eventName)
  ) {
    if (
      !['Success Gabungkan Rewards', 'Lewati Page Gabungkan Rewards'].includes(
        eventName,
      )
    ) {
      data = { ...data, Type: additionalValue }
    } else {
      if (eventName === 'Success Gabungkan Rewards') {
        let companyCode
        switch (additionalValue?.bu) {
          case 0:
          case 9:
            companyCode = 'ODI'
            break
          case 5:
          case 10:
            companyCode = 'AHI'
            break
          case 6:
          case 22:
            companyCode = 'HCI'
            break
          case 7:
            companyCode = 'TGI'
            break
          case 23:
            companyCode = 'KLG'
            break
        }

        data = {
          ...data,
          'Type': additionalValue?.type,
          'Company Code': companyCode,
        }
      }
    }
  }

  if (eventName === 'Click Tab Pakai Koin (Web)') {
    delete data.Location
    data = { ...data, 'Click Location': additionalValue }
  }

  if (eventName === 'PopUp Modal') {
    delete data.Location
    data = {
      ...data,
      'Click CTA': additionalValue?.click,
      'PopUp Name': additionalValue?.popUpName,
    }
  }

  if (eventName === 'Lengkapi Profile Step 3') {
    delete data.Location
  }

  if (['View Register Page', 'View Login Page'].includes(eventName)) {
    delete data.Source
    delete data.Location
    data = {
      ...data,
      'Store Code': additionalValue?.storeCode,
      'Register Using NIP': additionalValue?.isRegisByNIP,
    }
  }

  if (eventName.includes('Voucher Tab')) {
    data = {
      ...data,
      'Click Location': additionalValue.tab,
    }
  }

  if (
    [
      'Click Button Voucher Merchant',
      'View Page Voucher Merchant',
      'Click Pakai Voucher Merchant',
      'View Detail Voucher Merchant',
      'Sukses Pakai Voucher Merchant',
      'View Page Promo Merchant',
      'Click Tab Promo Merchant',
      'Click Baca Selengkapnya',
      'View Detail Promo Merchant',
    ].includes(eventName)
  ) {
    delete data.Source
    delete data.Location
    switch (eventName) {
      case 'Click Button Voucher Merchant':
      case 'View Page Voucher Merchant':
      case 'View Page Promo Merchant':
      case 'Click Tab Promo Merchant':
        data = {
          'Membership Status': additionalValue.membershipStatus,
          'Member ID': additionalValue.memberId,
        }
        break
      case 'Click Pakai Voucher Merchant':
        data = {
          'Voucher Name': additionalValue.voucherName,
          'Voucher Notes': additionalValue.voucherNotes,
        }
        break
      case 'Click Baca Selengkapnya':
        data = { 'Promo Name': additionalValue, 'Location': pageFrom }
        break
      case 'View Detail Voucher Merchant':
        data = { 'Voucher Name': additionalValue }
        break
      case 'View Detail Promo Merchant':
        data = { 'Promo Name': additionalValue }
        break
      case 'Sukses Pakai Voucher Merchant':
        data = {
          'Voucher Name': additionalValue.voucherName,
          'Voucher Code': additionalValue.voucherCode,
        }
        break
    }
  }

  if (
    [
      'View Modal Set New PIN',
      'Setup New PIN',
      'Click Button Lupa PIN',
      'Submit New PIN',
      'PIN Terkunci',
      'Click Buka Akses PIN',
      'Click Forgot Passkey',
      'Click Metode Passkey Lainnya',
    ].includes(eventName)
  ) {
    delete data.Source
    switch (eventName) {
      case 'Click Buka Akses PIN':
        data = { ...data, 'Is Success?': additionalValue }
        break
      case 'Click Metode Passkey Lainnya':
        data = { ...data, Method: additionalValue }
        break
    }
  }

  let section = GetParam('section') || null
  let location = GetParam('location') || null
  let sectionName = GetParam('section_name') || null
  if (!additionalValue) {
    //** set to be only apply for page after */
    if (location) {
      data = {
        ...data,
        Location: GetParam('location'),
      }
    }

    if (section) {
      data = {
        ...data,
        Section: GetParam('section'),
      }
    }
    if (sectionName) {
      data = {
        ...data,
        'Section Name': GetParam('section_name'),
      }
    }
  }
  mixpanelTrack(eventName, data)
  callback()
}

export const mixpanelKlkTrackSuccessRedeemCoin = (
  redemptionType,
  values,
  callback = () => {},
) => {
  mixpanelTrack('Success Tukar Koin', {
    'Token': 'private-dev',
    'Source': 'KLK',
    'Jenis Penukaran': redemptionType,
    'Store Name': values?.storeName || '(not set)',
    'Store Code': values?.storeCode || '(not set)',
    'Voucher Name': values?.voucherName || '(not set)',
    'Voucher ID': values?.voucherId || '(not set)',
    'Koin Amount': values?.coinUsed,
  })
  callback()
}

export const mixpanelTrackInstallationPopup = (installationItemPopup) => {
  const installationItemSku = []
  const installationItemName = []
  const installationItemStoreCode = []
  const installationItemStoreName = []

  if (!isEmpty(installationItemPopup)) {
    installationItemPopup?.installation_items?.forEach((item) => {
      installationItemSku.push(item?.sku)
      installationItemName.push(item?.name)
    })

    if (!isEmpty(installationItemPopup?.store)) {
      installationItemStoreCode.push(installationItemPopup?.store?.store_code)
      installationItemStoreName.push(installationItemPopup?.store?.name)
    }
  }

  mixpanelTrack('Jasa Perakitan Item', {
    'Location': 'Detail Pesanan Saya',
    'Item Id': installationItemSku,
    'Item Name': installationItemName,
    'Store Code': installationItemStoreCode,
    'Store Name': installationItemStoreName,
  })
}

export const mixpanelTrackSubmitInstallation = (
  selectedInstallation,
  installationItemSubmit,
) => {
  const installationItemSku = []
  const installationItemName = []
  const installationItemStoreCode = []
  const installationItemStoreName = []

  if (!isEmpty(selectedInstallation) && !isEmpty(installationItemSubmit)) {
    installationItemSubmit?.installation_items?.forEach((item) => {
      const isSelected = !!selectedInstallation?.find(
        (sku) => sku === item?.sku,
      )

      if (!isSelected) {
        return
      }

      installationItemSku.push(item?.sku)
      installationItemName.push(item?.name)
    })

    if (!isEmpty(installationItemSubmit?.store)) {
      installationItemStoreCode.push(installationItemSubmit?.store?.store_code)
      installationItemStoreName.push(installationItemSubmit?.store?.name)
    }
  }

  mixpanelTrack('Submit Jasa Perakitan', {
    'Location': 'Checkout',
    'Item Id': installationItemSku,
    'Item Name': installationItemName,
    'Store Code': installationItemStoreCode,
    'Store Name': installationItemStoreName,
  })
}

export const sendFilterPcpAlgoliaMixpanel = (uistate, selectedStore) => {
  if (uistate?.city_name) {
    mixpanelTrack('Click Filter Lokasi Pengiriman', {
      Kota: uistate?.city_name || 'None',
    })
  }
  if (selectedStore && selectedStore !== '') {
    mixpanelTrack('Click Filter Official Store Partner', {
      'Store Name': selectedStore?.store_name || 'None',
      'Store Code': selectedStore?.store_code || 'None',
    })
  }
  if (uistate?.delivery_method) {
    mixpanelTrack('Click Filter Dukungan Pengiriman', {
      'Is Same Day Delivery':
        uistate?.delivery_method.includes('Sameday Delivery'),
      'Is Instant Delivery':
        uistate?.delivery_method.includes('Instant Delivery'),
      'Is Regular': uistate?.delivery_method.includes('Regular Delivery'),
      'Is Pickup': uistate?.delivery_method.includes('Pickup'),
      'Is Ownfleet Delivery':
        uistate?.delivery_method.includes('Ownfleet Delivery'),
    })
  }
  if (uistate?.selling_price) {
    mixpanelTrack('Click Filter Harga', {
      Range: `${uistate?.selling_price?.replace(':', ' - ')}`,
      Minimum: uistate?.selling_price?.split(':')?.[0] || 'None',
      Maximum: uistate?.selling_price?.split(':')?.[1] || 'None',
    })
  }
  if (uistate?.['brand.name']) {
    let lastClicked =
      uistate?.['brand.name']?.[uistate?.['brand.name']?.length - 1]
    mixpanelTrack('Click Filter Brand', {
      'Brand Name': lastClicked,
      'Filter Applied': uistate?.['brand.name'],
    })
  }
  if (uistate?.[`label.${config.companyCode}`]) {
    const value = uistate?.[`label.${config.companyCode}`]
    let isBestOffer = false
    let isClearanceSale = false
    isClearanceSale = DEFAULT_PROMO.ALGOLIA_CLEARANCE.includes(value)
    isBestOffer = !DEFAULT_PROMO.ALGOLIA.includes(value)
    mixpanelTrack('Click Filter Promo', {
      'Is Buy 1 Get 1': value.includes('Buy 1 Get 1'),
      'Is Online Exclusive': value.includes('Online Exclusive'),
      'Is New Arrivals': value.includes('New Arrivals'),
      'Is Clearance Sale': isClearanceSale,
      'Is Best offer': isBestOffer,
    })
  }
  if (uistate?.[`bread_crumb3_${config.companyCode.toLowerCase()}.name`]) {
    let value =
      uistate?.[`bread_crumb3_${config.companyCode.toLowerCase()}.name`]
    mixpanelTrack('Click Filter Kategori', {
      'Filter Applied': value,
      'Level 3': value,
    })
  }

  if (uistate?.['colour']) {
    let value = uistate?.['colour']
    value = value.map((color) => {
      return color.split('|')[1]
    })
    mixpanelTrack('Click Filter Warna', {
      Name: value,
    })
  }
}

// export const mixpanelTrackPageView = () => {
//   mixpanel?.track_pageview()
// }

export const sendMixpanelHypersonalizedHomepage = (
  eventName,
  hypersonalizedUUID,
  hypersonalizedIdentifier,
) => {
  mixpanelTrack(eventName, {
    'uuid': hypersonalizedUUID,
    'Hyper Detail': hypersonalizedIdentifier?.hyper_detail,
    'AB Testing ID': hypersonalizedIdentifier?.ab_testing_id,
    'List Content': hypersonalizedIdentifier?.list_content,
  })
}