import dayjs from 'dayjs'
import isEmpty from 'lodash/isEmpty'
import { useCartAuthContext } from '../context/CartAuthContext'

const formatDate = (date) =>
  date
    ? dayjs(date, ['YYYY-MM-DD HH:mm:ss ZZ', 'YYYY-MM-DD HH:mm:ss']).format(
        'D MMMM YYYY',
      )
    : ''
const formatInstallationExpiredDate = (date) =>
  date
    ? dayjs(date, ['YYYY-MM-DD HH:mm:ss ZZ', 'YYYY-MM-DD HH:mm:ss']).format(
        'dddd, D MMMM YYYY HH:mm WIB',
      )
    : ''

const refundWording = (wording) => {
  if (wording === 'Refund') {
    return 'Full Refund'
  } else {
    return wording
  }
}

const generateStatusInstallation = (data) => {
  let text = ''
  let statusType = 'warning'

  if (data === 'installer_confirmation') {
    text = 'Dalam Pengecekan'
  } else if (data === 'waiting_shipment') {
    text = 'Menunggu Pengiriman'
  } else if (data === 'waiting_payment') {
    text = 'Menunggu Pembayaran'
  } else if (data === 'pending_installation') {
    text = 'Pembayaran Diterima'
  } else if (data === 'waiting_installation') {
    text = 'Menunggu Perakitan'
  } else if (['on_installation', 'ready_to_go', 'on_the_way'].includes(data)) {
    text = 'Teknisi Berangkat'
  } else if (data === 'canceled') {
    text = 'Perakitan Dibatalkan'
    statusType = 'danger'
  } else if (data === 'completed') {
    text = 'Selesai'
    statusType = 'success'
  } else {
    text = 'Tertunda'
  }

  if (text) {
    return { text, statusType }
  }
  return null
}

const generateStatusLabel = (data, type) => {
  let text = ''
  let statusType = ''
  let installationStatus = null

  if (type === 'installation') {
    installationStatus = generateStatusInstallation(data)
  }

  switch (type) {
    case 'transaction_online':
      if (data.to_render_status || data.invoice_status === 'on_process_tugu') {
        text =
          refundWording(data.to_render_status) ||
          (data.invoice_status === 'on_process_tugu'
            ? 'Dalam proses retur'
            : '')
        statusType = data.to_render_class_name || 'warning'
      }
      break

    case 'transaction_offline':
      if (data.stamps) {
        switch (data.status) {
          case 1:
            text = 'Pesanan telah diterima'
            statusType = 'success'
            break
          case 2:
            text = 'Refund'
            statusType = 'info'
            break
        }
      } else {
        text = 'Pesanan telah diterima'
        statusType = 'success'
      }
      break
    case 'service':
      if (data?.to_render_status && data?.to_render_class_name) {
        text = data.to_render_status || ''
        statusType = data.to_render_class_name || ''
      }
      break

    case 'installation':
      text = installationStatus?.text
      statusType = installationStatus?.statusType
      break
  }
  if (text && statusType) {
    return { text, statusType }
  }
  return null
}

/**
 * transform transaction online/offline/service/installation data to one form that can consumed by TransactionItem Component
 * @param {*} data
 * @param {*} type
 * @returns {{formatted transaction object}}
 */
const TransformTransData = (data, type) => {
  let item = {}

  if (type === 'installation') {
    item = data?.installation_item_detail
  } else {
    item = data?.items?.[0]
  }

  let typeLabel = ''
  let storeName = ''
  let transactionOfflineNumber = ''
  let transactionNumber = ''
  let installationOrderNumber = ''
  let installationStatus = ''
  let installationType = ''
  let dateLabel = ''
  let date = ''
  let installationExpiredDate = ''
  let status = {}
  let productItem = {}
  let totalSku = 0
  let grandTotal = 0
  let reviewProductFlag = 0
  let reviewServiceFlag = 0
  let installationPaymentObject = null
  let mobileRoute = ''
  let desktopRoute = ''
  let serviceRate = 0
  let isContainWarranty = false
  let totalTransaction = 0
  const cartAuthContext = useCartAuthContext()
  const { auth } = cartAuthContext.state

  if (!isEmpty(data) && !isEmpty(type)) {
    switch (type) {
      case 'transaction_online':
        typeLabel = 'PESANAN TOKO ONLINE'
        transactionNumber = data.invoice_no
        date = formatDate(data.transaction_date)
        status = generateStatusLabel(data, type)
        productItem = {
          imageUrl: item?.image_url,
          skuName: item?.sku_name,
          qty: item?.qty,
          sku: item?.sku,
          fullImageUrl: item?.full_image_url || '',
        }
        totalSku = data.total_sku
        grandTotal = data.grand_total
        reviewProductFlag = data.is_reviewed
        reviewServiceFlag = data.is_service_reviewed
        mobileRoute = `/my-account/transaction/detail?transaction_no=${data.invoice_no}&type=online&email=${auth?.user?.email}`
        desktopRoute = `/my-account?tab=transaction&child_tab=detail&transaction_no=${data.invoice_no}&type=online&email=${auth?.user?.email}`
        isContainWarranty = data.contain_warranty || false
        totalTransaction = data.subtotal
        break
      case 'transaction_offline':
        typeLabel = 'PESANAN TOKO FISIK'
        storeName = data.company_name
        transactionOfflineNumber = data.receipt_id
        date = formatDate(data.transaction_date)
        status = generateStatusLabel(data, type)
        productItem = {}
        grandTotal = parseInt(data.amount)
        mobileRoute = `/my-account/transaction/detail?transaction_no=${
          data.stamps ? data.transaction_id : data.receipt_id
        }&type=offline&company_code=${data.companyCode}${
          data.stamps ? `&stamps=true&receipt_id=${data.receipt_id}` : ''
        }`
        desktopRoute = `/my-account?tab=transaction&child_tab=detail&transaction_no=${
          data.stamps ? data.transaction_id : data.receipt_id
        }&type=offline&company_code=${data.companyCode}${
          data.stamps ? `&stamps=true&receipt_id=${data.receipt_id}` : ''
        }`
        serviceRate = data.service_rate
        isContainWarranty = data.contain_warranty || false
        totalTransaction = parseInt(data.amount)
        reviewProductFlag = data.is_reviewed
        reviewServiceFlag = data.is_service_reviewed
        break
      case 'service':
        typeLabel = 'SERVIS'
        transactionNumber = data?.ace_service_number // ACE_HARDWARE
        dateLabel = 'Tanggal Pengajuan '
        date = formatDate(data?.created_at)
        status = generateStatusLabel(data, type)
        productItem = {
          imageUrl: data?.sku_image,
          skuName: data?.sku_name,
          fullImageUrl: data?.full_image_url || '',
        }
        mobileRoute = `/my-account/service/detail?service_no=${data?.ace_service_number}` // ACE_HARDWARE
        desktopRoute = `/my-account?tab=service&child_tab=detail&service_no=${data?.ace_service_number}` // ACE_HARDWARE
        grandTotal = 'Not a Number' // to disable the Price Text
        break
      case 'installation':
        typeLabel = 'JASA PERAKITAN'
        // assign values for online and offline installation
        if (data?.type === 'offline') {
          transactionNumber = data?.receipt_id ? data?.receipt_id : '-'
          installationOrderNumber = data?.receipt_id ? data?.receipt_id : null
          mobileRoute = `/my-account/installation/detail?type=offline&receipt_id=${data?.receipt_id}&bu=${data?.company_code}`
          desktopRoute = `/my-account?tab=installation&child_tab=detail&type=offline&receipt_id=${data?.receipt_id}&bu=${data?.company_code}`
        }
        if (data?.type === 'online') {
          transactionNumber = data?.installation_invoice_no
            ? data?.installation_invoice_no
            : '-'
          installationOrderNumber = data?.installation_order_no
            ? data?.installation_order_no
            : null
          mobileRoute = `/my-account/installation/detail?transaction_no=${data?.reference_invoice_no}&installation_id=${data?.sales_installation_id}`
          desktopRoute = `/my-account?tab=installation&child_tab=detail&reference_invoice_no=${data?.reference_invoice_no}&installation_id=${data?.sales_installation_id}`
        }
        date = formatDate(data?.installation_created_at)
        if (data?.installation_status === 'waiting_payment') {
          installationExpiredDate = formatInstallationExpiredDate(
            data?.payment_expired_at,
          )
        }
        status = generateStatusLabel(data?.installation_status, type)
        installationStatus = data?.installation_status
        installationType = data?.type
        productItem = {
          imageUrl: item?.image_url,
          skuName: item?.name,
          qty: item?.installation_item_total,
          fullImageUrl: item?.full_image_url || '',
        }
        installationPaymentObject = {
          cart_id: data?.installation_cart_id,
          expired: data?.payment_expired_at,
          reference_invoice_no: data?.reference_invoice_no,
          sales_installation_id: data?.sales_installation_id,
          installation_order_no: data?.installation_order_no
            ? data?.installation_order_no
            : null,
        }
        totalSku = data?.installation_item_detail?.length
        grandTotal = data?.installation_price
        totalTransaction = data?.installation_price
        break
      default:
        break
    }
  }

  return {
    type,
    typeLabel,
    storeName,
    transactionOfflineNumber,
    transactionNumber,
    installationOrderNumber,
    installationStatus,
    installationType,
    dateLabel,
    date,
    installationExpiredDate,
    status,
    productItem,
    totalSku,
    grandTotal,
    reviewProductFlag,
    reviewServiceFlag,
    installationPaymentObject,
    mobileRoute,
    desktopRoute,
    serviceRate,
    isContainWarranty,
    totalTransaction,
  }
}

export default TransformTransData
