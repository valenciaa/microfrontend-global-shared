const UpperCase = (string) => {
  return string.toLowerCase().replace(/\b\w/g, (l) => l.toUpperCase())
}
export default UpperCase
