const numberExtFormater = (number, decimalPrecision) => {
  const numberExt = ['', 'rb', 'jt', 'M']
  // need Math.floor in case result is in decimal after Math.log10 to get the precise numberExt type in array above
  const numExtOrderDecision = Math.floor(Math.log10(Math.abs(number)) / 3)
  const extChosenOrder = Math.max(
    0,
    Math.min(numExtOrderDecision, numberExt.length - 1),
  )
  const finalExt = numberExt[extChosenOrder]
  const finalFormatedResult =
    (number / Math.pow(10, extChosenOrder * 3)).toFixed(decimalPrecision) * 1
  return finalFormatedResult.toString() + finalExt
}

export { numberExtFormater }
