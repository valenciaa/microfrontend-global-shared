import CoachmarkContent from '../layouts/atoms/CoachmarkContent'
import config from '../../../config'
import clsx from 'clsx'

const giftRegistryProductDetailCoachmarkStep = [
  {
    content: () => (
      <CoachmarkContent title='Tambah Produk ke Gift Registry'>
        <p className='ui-text-2'>
          Tap tombol <b className='heading-3'>+ Tambah</b> untuk menambahkan
          produk ini ke gift registry-mu
        </p>
      </CoachmarkContent>
    ),
    selector: '#gift-registry__container',
  },
]

const giftRegistryMicrositeCoachmarkStep = [
  {
    content: () => (
      <CoachmarkContent title='Belum menemukan yang kamu cari?'>
        <p className='ui-text-2'>Temukan barang dengan memasukkan kata kunci</p>
      </CoachmarkContent>
    ),
    selector: '.search-input',
  },
]

const giftRegistryCategoryCoachmarkStep = [
  {
    content: () => (
      <CoachmarkContent title='Belum menemukan yang kamu cari?'>
        <p
          className={clsx(
            'text-left',
            config.environment === 'desktop' ? 'ui-text-3' : 'ui-text-2',
          )}
        >
          Temukan barang dengan memasukkan kata kunci
        </p>
      </CoachmarkContent>
    ),
    selector:
      config.environment === 'desktop' ? '#search-input' : '#search-icon',
  },
]

export {
  giftRegistryCategoryCoachmarkStep,
  giftRegistryMicrositeCoachmarkStep,
  giftRegistryProductDetailCoachmarkStep,
}
