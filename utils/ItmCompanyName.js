import config from '../../../config'
import { ACE_HARDWARE } from './constants/AceConstants'

const ItmCompanyName = () => {
  let companyName = ''
  if (config.companyName === 'acestore' || config.companyName === 'ace') {
    //ACE_HARDWARE
    companyName = ACE_HARDWARE
  } else if (
    config.companyName === 'informastore' ||
    config.companyName === 'informa'
  ) {
    companyName = 'informa'
  } else if (config.companyName === 'toyskingdomonline') {
    companyName = 'toys'
  }

  return companyName
}
export default ItmCompanyName
