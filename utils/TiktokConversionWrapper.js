import { globalAPI } from '../services/IndexApi'
import config from '../../../config'
import Cookies from 'js-cookie'
import dayjs from 'dayjs'
import localforage from 'localforage'
import { v4 as uuidv4 } from 'uuid'
import GetParam from './GetParam'

const SHA256 = require('crypto-js/sha256')

export const handleSendTiktokConversionRequest = async (
  eventName,
  customData = {},
) => {
  if (config.isLiveSite) {
    let userData = {}
    let ip = null
    let userAgent = null

    // AUTH (USER)
    await localforage.getItem('persist:ruparupa').then((auth) => {
      if (auth) {
        let user = JSON.parse(auth)
        user = user.auth ? JSON.parse(user.auth).user : null

        // LOGGED IN
        if (user) {
          const customerId =
            user && user.customer_id
              ? SHA256(user.customer_id).toString()
              : null
          const email =
            user && user.email ? SHA256(user.email).toString() : null
          const phone =
            user && user.phone ? SHA256(user.phone).toString() : null

          if (customerId) {
            userData = { ...userData, external_id: customerId }
          }
          if (email) {
            userData = { ...userData, email: email }
          }
          if (phone) {
            userData = { ...userData, phone_number: phone }
          }
        }
      }
    })

    // TTP
    const ttp = Cookies.get('_ttp')
    if (ttp) {
      userData = { ...userData, ttp }
    }

    // IP
    await localforage.getItem('ip').then((value) => {
      if (value) {
        // Take only first IP address
        ip = value.split(',')[0]
      }
    })

    // USER AGENT
    if (window && window.navigator && window.navigator.userAgent) {
      userAgent = window.navigator.userAgent
    }

    // CALLBACK
    let adData = {}
    let ttclid = ''
    // Cek if in url contain ttclid
    if (window.location.href.indexOf('ttclid') !== -1) {
      ttclid = GetParam('ttclid')
      if (ttclid) {
        adData = { ...adData, callback: ttclid }
      }
      // Cek if in cookie contain ttclid
    } else if (Cookies.get('ttclid-session')) {
      if (ttclid) {
        adData = { ...adData, callback: ttclid }
      }
    }

    const eventTime = dayjs(new Date()).format('YYYY-MM-DDTHH:mm:ss[Z]')
    const eventId = `${uuidv4()}-${uuidv4()}`

    const payload = {
      pixel_code: config.tiktokPixelCode,
      event: eventName,
      event_id: eventId,
      timestamp: eventTime,
      context: {
        ad: adData,
        page: {
          url: window.location.href,
          referrer: 'https://ruparupa.com',
        },
        user: userData,
        user_agent: userAgent,
        ip: ip,
      },
      properties: customData,
    }
    await globalAPI.tiktokConversion(payload)
  }
}

export const handleTiktokConversionAddToCartData = (
  pageFrom = '',
  activeVariant,
  product,
  quantity,
) => {
  let customData = {
    currency: 'IDR',
    value: activeVariant
      ? (activeVariant?.prices?.[0]?.special_price !== 0
          ? activeVariant?.prices?.[0]?.special_price
          : activeVariant?.prices?.[0]?.price) * quantity
      : 0,
  }

  if (pageFrom === '') {
    customData = {
      contents: [
        {
          price: activeVariant
            ? activeVariant?.prices?.[0]?.special_price !== 0
              ? activeVariant?.prices?.[0]?.special_price
              : activeVariant?.prices?.[0]?.price
            : 0,
          quantity: quantity,
          content_type: 'product',
          content_id:
            activeVariant && activeVariant?.sku ? activeVariant?.sku : '',
        },
      ],
      ...customData,
    }
  }

  return customData
}

export const handleSendTiktokConversionAddToCartRequest = (
  pageFrom,
  activeVariant,
  product,
  quantity,
) => {
  const customData = handleTiktokConversionAddToCartData(
    pageFrom,
    activeVariant,
    product,
    quantity,
  )

  handleSendTiktokConversionRequest('AddToCart', customData)
}

export const handleSetTtclid = (identifier) => {
  const expires = dayjs().add(7, 'day').toDate()
  const value = {
    identifier,
    expires,
  }

  Cookies.set('ttclid-session', JSON.stringify(value), {
    expires,
    domain: config.isLiveSite ? '.ruparupa.com' : '.ruparupastg.my.id',
  })
}
