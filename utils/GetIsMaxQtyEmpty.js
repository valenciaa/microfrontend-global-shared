const GetIsMaxQtyEmpty = (productMaxStock) => {
  if (!productMaxStock) {
    return false
  }

  return (
    Object.prototype.hasOwnProperty.call(productMaxStock, 'max_qty') &&
    !productMaxStock?.max_qty
  )
}

export default GetIsMaxQtyEmpty
