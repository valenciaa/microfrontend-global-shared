const formB2bMapper = {
  customerProfile: [
    {
      label: 'Nama Perusahaan',
      type: 'text',
      name: 'companyName',
      required: true,
      minLength: 3,
      maxLength: 70,
      pattern: null,
    },
    {
      label: 'Grup Perusahaan',
      type: 'text',
      name: 'companyGroup',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Alamat',
      type: 'textarea',
      name: 'address',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Kota',
      type: 'select',
      name: 'city',
      option: [],
      required: true,
      pattern: null,
    },
    {
      label: 'Kode Pos',
      type: 'text',
      name: 'postCode',
      maxLength: 5,
      inputMode: 'number',
      minLength: 5,
      required: true,
    },
    {
      label: 'Nomor Handphone',
      type: 'text',
      name: 'mobilePhone',
      inputMode: 'number',
      required: true,
      minLength: 11,
      maxLength: 14,
      pattern: /(^(08))\d{8,12}$/,
    },
    {
      label: 'Telepon',
      type: 'text',
      name: 'homePhone',
      required: true,
      inputMode: 'number',
      minLength: 6,
      maxLength: 14,
    },
    {
      label: 'Fax',
      type: 'text',
      name: 'fax',
      required: false,
      minLength: 0,
    },
    {
      label: 'Alamat Email',
      type: 'email',
      name: 'email',
      required: true,
      minLength: 3,
    },
    {
      label: 'Website',
      type: 'text',
      name: 'website',
      required: false,
      minLength: 0,
    },
    {
      label: 'Tipe Wajib Pajak',
      type: 'select',
      name: 'taxNumberType',
      option: [
        {
          label: 'Z1 - WP Badan Pusat',
          value: 'Z1',
        },
        {
          label: 'Z2 - WP Badan Cabang',
          value: 'Z2',
        },
      ],
      required: true,
    },
    {
      label: 'Nomor NPWP (15 Digit)',
      type: 'text',
      name: 'npwpNumber',
      inputMode: 'number',
      required: true,
      maxLength: 15,
    },
    {
      label: 'Nomor NPWP (16 Digit)',
      type: 'text',
      name: 'npwpNumber2',
      inputMode: 'number',
      required: true,
      maxLength: 16,
    },
    {
      label: 'NITKU',
      type: 'text',
      name: 'nitkuNo',
      inputMode: 'number',
      required: true,
      maxLength: 22,
    },
    {
      label: 'Nomor Identitas',
      type: 'text',
      name: 'ktpNo',
      inputMode: 'number',
      required: true,
      minLength: 16,
      maxLength: 16,
    },
    {
      label: 'Nama Lengkap NPWP',
      type: 'text',
      name: 'npwpFullname',
      required: true,
      minLength: 0,
      maxLength: 135,
      pattern: null,
    },
    {
      label: 'Alamat NPWP',
      type: 'textarea',
      name: 'npwpAddress',
      required: true,
      minLength: 5,
      maxLength: 100,
      pattern: null,
    },
    {
      label: 'Nomor PKP',
      type: 'text',
      name: 'pkpNumber',
      required: false,
      minLength: 0,
      maxLength: 50,
      pattern: null,
    },
    {
      label: 'Tanggal PKP',
      type: 'date',
      name: 'pkpDate',
      required: false,
      minLength: 0,
      pattern: null,
      minDate: new Date(),
      maxDate: null,
    },
    {
      label: 'Tipe PKP',
      type: 'select',
      name: 'typeOfPkp',
      option: [],
      required: true,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Unggah NPWP',
      type: 'file',
      name: 'npwp',
      required: true,
      minLength: 0,
      pattern: null,
      isUploading: false,
    },
    {
      label: 'Unggah SPPKP',
      type: 'file',
      name: 'sppkp',
      required: false,
      minLength: 0,
      pattern: null,
      isUploading: false,
    },
  ],
  billingInformation: [
    {
      label: 'Samakan dengan Alamat Perusahaan?',
      type: 'select',
      name: 'sameAsCompanyAddressBilling',
      option: [
        {
          label: 'No',
          value: 'No',
        },
        {
          label: 'Yes',
          value: 'Yes',
        },
      ],
      required: false,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Alamat Penagihan',
      type: 'textarea',
      name: 'address',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Kota',
      type: 'select',
      name: 'city',
      option: [],
      value: null,
      required: true,
      pattern: null,
    },
    {
      label: 'Kode Pos',
      type: 'text',
      name: 'postCode',
      inputMode: 'number',
      required: true,
      minLength: 5,
      maxLength: 5,
    },
    {
      label: 'Telepon',
      type: 'text',
      inputMode: 'number',
      name: 'mobilePhone',
      required: true,
      minLength: 11,
      maxLength: 14,
    },
    {
      label: 'Fax',
      type: 'text',
      name: 'fax',
      required: false,
      minLength: 0,
    },
    {
      label: 'Alamat Email',
      type: 'email',
      name: 'email',
      required: true,
      minLength: 5,
      pattern: /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i,
    },
    {
      label: 'Dokumen untuk Faktur',
      type: 'checkbox',
      name: 'documentForInvoicing',
      value: [
        { name: 'originalInvoice', label: 'Original Invoice', checked: false },
        { name: 'receipt', label: 'Kwintansi', checked: false },
        {
          name: 'fakturPajak',
          label: 'Faktur Pajak',
          checked: false,
          value: [
            { label: 'Original', value: 1 },
            { label: 'Copy', value: 0 },
          ],
        },
        { name: 'deliveryNote', label: 'Catatan Pengiriman', checked: false },
        { name: 'bast', label: 'BAST', checked: false },
        {
          name: 'wetStamp',
          label: 'Cap Basah Saat Pengiriman',
          checked: false,
        },
        { name: 'grn', label: 'GRN (Good Receive Note)', checked: false },
        { name: 'other', label: 'Lainnya', checked: false },
        { name: 'otherText' },
      ],
      option: [
        {
          label: 'Original',
          value: 'original',
        },
        {
          label: 'Copy',
          value: 'copy',
        },
      ],
      required: true,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Jadwal Tukar Faktur',
      type: 'text',
      name: 'tukarFakturSchedule',
      maxLength: 10,
      pattern: null,
    },
    {
      label: 'Tanggal Penerimaan Faktur Terakhir',
      type: 'text',
      name: 'lastInvoiceReceiveDate',
      maxLength: 10,
      pattern: null,
    },
    {
      label: 'Jadwal Pembayaran',
      type: 'text',
      name: 'paymentSchedule',
      maxLength: 10,
      pattern: null,
    },
    {
      label: 'Nama Bank',
      type: 'text',
      name: 'bankName',
      required: false,
      pattern: null,
    },
    {
      label: 'Cabang Bank',
      type: 'text',
      name: 'bankBranch',
      required: false,
      pattern: null,
    },
    {
      label: 'Nomor Rekening',
      type: 'text',
      inputMode: 'number',
      name: 'accountNumber',
      required: false,
    },
    {
      label: 'Nama Akun',
      type: 'text',
      name: 'accountName',
      required: false,
      pattern: null,
    },
    {
      label: 'Term Of Payment',
      type: 'select',
      name: 'termOfPayment',
      option: [
        {
          label: 'Paid Before Delivery',
          value: 'paid_before_delivery',
        },
        {
          label: '7 Days',
          disabled: true,
          value: '7_days',
        },
        {
          label: '14 Days',
          disabled: true,
          value: '14_days',
        },
        {
          label: '21 Days',
          disabled: true,
          value: '21_days',
        },
        {
          label: '30 Days',
          disabled: true,
          value: '30_days',
        },
      ],
      required: true,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Dokumen untuk Pengiriman',
      type: 'checkbox',
      name: 'documentForDelivery',
      value: [
        {
          name: 'originalDo',
          label: 'Pesanan Pengiriman Asli',
          checked: false,
        },
        { name: 'other', label: 'Lainnya', checked: false },
        { name: 'otherText' },
      ],
      required: true,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Samakan dengan Alamat Perusahaan?',
      type: 'select',
      name: 'sameAsCompanyAddressShipping',
      value: 'No',
      option: [
        {
          label: 'No',
          value: 'No',
        },
        {
          label: 'Yes',
          value: 'Yes',
        },
      ],
      required: false,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Alamat Pengiriman (1)',
      type: 'textarea',
      name: 'address1',
      required: true,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Kota',
      type: 'select',
      name: 'city1',
      option: [],
      required: true,
      pattern: null,
    },
    {
      label: 'Kode Pos',
      type: 'text',
      name: 'postCode1',
      required: true,
      inputMode: 'number',
      minLength: 5,
      maxLength: 5,
    },
    {
      label: 'Alamat Pengiriman (2)',
      type: 'textarea',
      name: 'shippingAddress2',
      required: false,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Kota',
      type: 'select',
      name: 'city2',
      option: [],
      required: false,
      pattern: null,
      value: 1,
    },
    {
      label: 'Kode Pos',
      type: 'text',
      name: 'postCode2',
      value: '',
      required: false,
      inputMode: 'number',
      minLength: 0,
      maxLength: 5,
    },
  ],
  companyInformation: [
    {
      label: 'Tipe Pengguna',
      type: 'checkbox',
      name: 'customerType',
      value: [
        { name: 'endUser', label: 'Pengguna Akhir', checked: false },
        { name: 'supplier', label: 'Pemasok', checked: false },
        { name: 'agent', label: 'Agen', checked: false },
        { name: 'distributor', label: 'Distributor', checked: false },
        { name: 'manufacturer', label: 'Produsen', checked: false },
        { name: 'retail', label: 'Pengecer', checked: false },
        { name: 'goverment', label: 'Pemerintah', checked: false },
        { name: 'other', label: 'Lainnya', checked: false },
        { name: 'otherText' },
      ],
      required: true,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Bidang Usaha',
      type: 'select',
      name: 'lineOfBusiness',
      option: [],
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Produk Utama',
      type: 'select',
      name: 'mainProduct',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Tahun Didirikan',
      type: 'text',
      name: 'yearOfEstablished',
      required: true,
      minLength: 0,
      inputMode: 'number',
      maxLength: 4,
      pattern: null,
    },
    {
      label: 'Jumlah Karyawan',
      type: 'text',
      name: 'numberOfEmployee',
      required: true,
      minLength: 0,
      inputMode: 'number',
      maxLength: 10,
    },
    {
      label: 'Kepemilikan Bangunan',
      type: 'select',
      name: 'buildingOwnership',
      option: [],
      required: true,
      pattern: null,
    },
    {
      label: 'Pendapatan per tahun (IDR)',
      type: 'select',
      name: 'revenuePerYear',
      option: [],
      required: true,
      pattern: null,
    },
    {
      label: 'Estimasi Pembelian Tahunan (IDR)',
      type: 'select',
      name: 'annualPurchasedEstimation',
      option: [],
      required: true,
      pattern: null,
    },
    {
      label: 'Apakah Anda pernah membeli produk di Kawan Lama?',
      type: 'select',
      name: 'everWithKawanLama',
      option: [
        {
          label: 'Yes',
          value: 1,
        },
        {
          label: 'No',
          value: 0,
        },
      ],
      required: true,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Departemen',
      type: 'text',
      name: 'department1',
      disabled: true,
      value: 'Finance & Accounting',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Nama Depan',
      type: 'text',
      name: 'firstName1',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Nama Belakang',
      type: 'text',
      name: 'lastName1',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Telepon & Ext. Langsung',
      type: 'text',
      name: 'directPhoneShipping1',
      required: true,
      minLength: 6,
      inputMode: 'number',
      maxLength: 14,
      pattern: /(^(0))\d{8,12}$/,
    },
    {
      label: 'Nomor Handphone',
      type: 'text',
      name: 'mobilePhone1',
      required: true,
      inputMode: 'number',
      minLength: 6,
      maxLength: 14,
      pattern: /(^(08))\d{8,12}$/,
    },
    {
      label: 'Alamat Email',
      type: 'email',
      name: 'email1',
      required: true,
      minLength: 3,
      pattern: /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i,
    },
    {
      label: 'Departemen',
      type: 'text',
      name: 'department2',
      disabled: true,
      value: 'Purchasing',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Nama Depan',
      type: 'text',
      name: 'firstName2',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Nama Belakang',
      type: 'text',
      name: 'lastName2',
      required: true,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Telepon & Ext. Langsung',
      type: 'text',
      name: 'directPhoneShipping2',
      required: true,
      minLength: 6,
      inputMode: 'number',
      maxLength: 14,
      pattern: /(^(0))\d{8,12}$/,
    },
    {
      label: 'Nomor Handphone',
      type: 'text',
      name: 'mobilePhone2',
      required: true,
      inputMode: 'number',
      minLength: 6,
      maxLength: 14,
      pattern: /(^(08))\d{8,12}$/,
    },
    {
      label: 'Alamat Email',
      type: 'email',
      name: 'email2',
      required: true,
      minLength: 3,
      pattern: /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i,
    },
    {
      label: 'Departemen',
      type: 'text',
      name: 'department3',
      disabled: true,
      value: 'Owner',
      required: false,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Nama Depan',
      type: 'text',
      name: 'firstName3',
      required: false,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Nama Belakang',
      type: 'text',
      name: 'lastName3',
      required: false,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Telepon & Ext. Langsung',
      type: 'text',
      name: 'directPhoneShipping3',
      required: false,
      inputMode: 'number',
      minLength: 0,
      maxLength: 14,
      pattern: /(^(0))\d{8,12}$/,
    },
    {
      label: 'Nomor Handphone',
      type: 'text',
      name: 'mobilePhone3',
      required: false,
      inputMode: 'number',
      minLength: 0,
      maxLength: 14,
      pattern: /(^(08))\d{8,12}$/,
    },
    {
      label: 'Alamat Email',
      type: 'email',
      name: 'email3',
      required: false,
      minLength: 0,
      pattern: /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i,
    },
    {
      label: 'Departemen',
      type: 'text',
      name: 'department4',
      disabled: true,
      value: 'Warehouse',
      required: false,
      minLength: 0,
      pattern: null,
    },
    {
      label: 'Nama Depan',
      type: 'text',
      name: 'firstName4',
      required: false,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Nama Belakang',
      type: 'text',
      name: 'lastName4',
      required: false,
      minLength: 3,
      pattern: null,
    },
    {
      label: 'Telepon & Ext. Langsung',
      type: 'text',
      name: 'directPhoneShipping4',
      required: false,
      inputMode: 'number',
      minLength: 0,
      maxLength: 14,
      pattern: /(^(0))\d{8,12}$/,
    },
    {
      label: 'Nomor Handphone',
      type: 'text',
      name: 'mobilePhone4',
      required: false,
      minLength: 0,
      inputMode: 'number',
      maxLength: 14,
      pattern: /(^(08))\d{8,12}$/,
    },
    {
      label: 'Alamat Email',
      type: 'email',
      name: 'email4',
      required: false,
      minLength: 0,
      pattern: /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i,
    },
  ],
}

const initRegisterState = [
  {
    label: 'Nama Depan',
    type: 'text',
    name: 'firstName',
    required: true,
    value: '',
    minLength: 0,
    pattern: null,
    maxLength: 135,
  },
  {
    label: 'Nama Belakang',
    type: 'text',
    name: 'lastName',
    required: true,
    value: '',
    minLength: 0,
    pattern: null,
    maxLength: 135,
  },
  {
    label: 'Email Address',
    type: 'email',
    name: 'email',
    required: true,
    value: '',
    minLength: 0,
    pattern:
      /^(([^<>()[\].,;:\s@']+(\.[^<>()[\].,;:\s@']+)*)|('.+'))@(([^<>()[\].,;:\s@']+\.)+[^<>()[\].,;:\s@']{2,})$/,
    maxLength: 135,
  },
  {
    label: 'Phone Number',
    type: 'text',
    name: 'phone',
    required: true,
    value: '',
    minLength: 10,
    pattern: /(^(08))\d{8,12}$/,
    maxLength: 13,
  },
  {
    label: 'Password',
    type: 'password',
    name: 'password',
    required: true,
    value: '',
    minLength: 5,
    pattern: /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/,
    maxLength: 135,
  },
  {
    label: 'Company',
    type: 'text',
    name: 'companyName',
    required: true,
    value: '',
    minLength: 0,
    pattern: null,
    maxLength: 70,
  },
  {
    label: 'Job Title',
    type: 'text',
    name: 'jobTitle',
    required: true,
    value: '',
    minLength: 5,
    pattern: null,
    maxLength: 135,
  },
  {
    label: 'Identity Number',
    type: 'text',
    name: 'ktpNo',
    required: false,
    value: '',
    minLength: 16,
    maxLength: 16,
  },
  {
    label: 'NPWP Full Name',
    type: 'text',
    name: 'npwpFullname',
    required: true,
    value: '',
    minLength: 0,
    pattern: null,
    maxLength: 135,
  },
  {
    label: 'NPWP No.',
    type: 'text',
    name: 'npwpNumber',
    required: true,
    value: '',
    minLength: 15,
    maxLength: 18,
  },
  {
    label: 'NPWP Address',
    type: 'textarea',
    name: 'npwpAddress',
    required: true,
    value: '',
    minLength: 10,
    pattern: null,
    maxLength: 1024,
  },
  {
    label: 'I agree to the terms and conditions',
    type: 'checkbox',
    name: 'agreement',
    required: true,
    value: false,
    pattern: null,
    maxLength: null,
  },
]
const quickRegisterMap = [
  {
    label: 'Nama Depan',
    type: 'text',
    name: 'firstName',
    required: true,
  },
  {
    label: 'Nama Belakang',
    type: 'text',
    name: 'lastName',
    required: true,
  },
  {
    label: 'Tipe Wajib Pajak',
    type: 'select',
    name: 'taxNumberType',
    option: [
      {
        label: 'Z1 - WP Badan Pusat',
        value: 'Z1',
      },
      {
        label: 'Z2 - WP Badan Cabang',
        value: 'Z2',
      },
    ],
    required: true,
  },
  {
    label: 'Nomor NPWP (15 Digit)',
    type: 'text',
    name: 'npwpNumber',
    required: true,
    maxLength: 15,
  },
  {
    label: 'NITKU',
    type: 'text',
    name: 'nitkuNo',
    required: true,
    maxLength: 22,
  },
  {
    label: 'Alamat Email',
    type: 'email',
    name: 'email',
    required: true,
  },
  {
    label: 'Kata Sandi',
    type: 'password',
    name: 'password',
    required: true,
  },
]

export { initRegisterState, formB2bMapper, quickRegisterMap }
