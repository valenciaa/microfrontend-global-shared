import React from 'react'
import config from '../../../config'

if (
  typeof window !== 'undefined' &&
  process.env.NODE_ENV === 'development' &&
  config.isWydrActive
) {
  const whyDidYouRender = require('@welldone-software/why-did-you-render')
  console.log(`Why did you render is active, enjoy!`)
  whyDidYouRender(React, {
    trackAllPureComponents: true,
    trackHooks: true,
    logOwnerReasons: true,
    collapseGroups: false,
  })
}
