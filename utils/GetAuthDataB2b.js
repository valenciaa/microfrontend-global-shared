import localforage from 'localforage'
import config from '../../../config'

export const GetAuthDataB2b = async (type) => {
  try {
    return await localforage
      .getItem(`persist:ruparupa_b2b_${config.companyNameCSS}`)
      .then((auth) => {
        if (!auth) {
          return null
        }

        const userData = JSON.parse(auth)

        switch (type) {
          case 'access_token':
            return userData?.access_token
          case 'minicart_id':
            return userData?.minicart_id
          case 'all':
            return userData

          default:
            return JSON.parse(userData?.auth)?.user || null
        }
      })
  } catch {
    return null
  }
}
