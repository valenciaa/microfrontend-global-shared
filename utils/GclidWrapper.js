import config from '../../../config'
import Cookies from 'js-cookie'
const dayjs = require('dayjs')

export const handleSetGclid = (identifier) => {
  const expires = dayjs().add(7, 'day').toDate()
  const value = {
    identifier,
    expires,
  }

  Cookies.set('gclid-session', JSON.stringify(value), {
    expires,
    domain: config.isLiveSite ? '.ruparupa.com' : '.ruparupastg.my.id',
  })
}

export const handleGclidMobileAppsLink = (pageFrom, gclid) => {
  let redirectLink = 'https://ruparupa.page.link?link='
  let page = ''

  switch (pageFrom) {
    case 'product-detail':
      page = 'pdp/'
      break
    case 'catalog':
      page = 'pcp/'
      break
    case 'tahu':
      page = 'promopage/'
      break
    case 'home':
      page = 'home'
      break
    default:
      page = ''
      break
  }

  if (page) {
    const currentUrl = window.location.href.split(config.baseURL)
    const paramExist = window.location.href.indexOf('?') !== -1
    const gclidExist = window.location.href.indexOf('gclid') !== -1
    const jualExist = window.location.href.indexOf('/jual/') !== -1
    const concatLink =
      currentUrl && currentUrl[1] ? config.baseURL + page + currentUrl[1] : ''
    const additional = '&apn=com.mobileappruparupa&ibi=com.ruparupa.ios'

    try {
      let param = ''

      if (gclidExist) {
        gclid = ''
      } else {
        param = paramExist ? '&gclid=' : '?gclid='
      }

      if (jualExist || page === 'home') {
        redirectLink +=
          encodeURIComponent(window.location.href + param + gclid) + additional
      } else {
        redirectLink +=
          encodeURIComponent(concatLink + param + gclid) + additional
      }
    } catch (e) {
      redirectLink = ''
    }
  } else {
    redirectLink = ''
  }

  return (
    redirectLink || 'https://ruparupa.onelink.me/xDlg/appdownloadfrommobile'
  )
}
