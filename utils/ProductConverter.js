import config from '../../../config'

export const productsConverter = (products) => {
  return products
    .filter((p) => p.variants.length)
    .map((item) => {
      const { variants } = item
      const firstVariant = variants[0]
      // parsing image
      const imagesUrl = firstVariant.images.map((i) =>
        imageUrlProduct(i.image_url),
      )
      const prices = firstVariant.prices.map((i) => ({
        price: i.special_price || i.price,
        msrp: i.price,
        discount: discountFormula(i),
      }))
      return {
        name: item.name,
        image: imagesUrl,
        prices: prices,
        sku: firstVariant.sku,
        is_in_stock: item.is_in_stock,
        rating: item.review?.[config.companyCode],
        checkReviewRating:
          item.checkReviewRating && !!item.review?.[config.companyCode],
        checkFlashSale: item.checkFlashSale,
        variantLabel: firstVariant.label,
      }
    })
}

export const imageUrlProduct = (imageUrl) => {
  return imageUrl !== '___'
    ? `${config.imageURL}w_500,h_500,f_auto,q_auto${imageUrl}`
    : `${config.assetsURL}images/loading-ruparupa.gif`
}

/**
 * discount
 * formula =  {Math.floor((variants.prices[0].price - variants.prices[0].special_price) / variants.prices[0].price * 100)}%
 *
 * @param   {[object]}  price  {price, special_price}
 *
 * @return  {[number]}
 */
export const discountFormula = (price) => {
  return price.special_price > 0
    ? Math.floor(((price.price - price.special_price) / price.price) * 100)
    : 0
}

const getDateString = () => {
  const now = new Date()
  const year = now.getFullYear()
  const month = String(now.getMonth() + 1).padStart(2, '0')
  const day = String(now.getDate()).padStart(2, '0')
  const hours = String(now.getHours()).padStart(2, '0')
  const minutes = String(now.getMinutes()).padStart(2, '0')
  const seconds = String(now.getSeconds()).padStart(2, '0')

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
}

export const algoliaProductRibbon = (ribbonArray) => {
  if (!ribbonArray) {
    return ''
  }

  const now = getDateString()
  const companyCode = config.companyCode

  const activeRibbon = ribbonArray.filter(
    ({ company_code, start_date, end_date }) =>
      company_code === companyCode && now >= start_date && now <= end_date,
  )

  return activeRibbon?.[0]?.url || ''
}
