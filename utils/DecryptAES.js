import CryptoJS from 'crypto-js'

const DecryptAES = (encryptedData, secretKey) => {
  const bytes = CryptoJS.AES.decrypt(encryptedData, secretKey)
  const decryptedData = bytes?.toString(CryptoJS.enc.Utf8)
  return decryptedData
}

export default DecryptAES
