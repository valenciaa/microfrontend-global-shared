const GetIsEligibleGiftWrapping = (length, width, height, uom) => {
  if (uom?.toLowerCase() === 'm') {
    length = length * 100
    width = width * 100
    height = height * 100
  }

  if (uom?.toLowerCase() === 'mm') {
    length = length * 0.1
    width = width * 0.1
    height = height * 0.1
  }

  const isEligibleGiftWrapping = length <= 60 && width <= 20 && height <= 20
  return isEligibleGiftWrapping
}

export default GetIsEligibleGiftWrapping
