const getDefaultAddress = () => {
  return {
    province: {
      province_id: '490',
      province_name: 'DKI JAKARTA',
    },
    city: {
      city_id: '573',
      city_name: 'KOTA ADMINISTRASI JAKARTA BARAT',
    },
    kecamatan: {
      kecamatan_id: '7004',
      kecamatan_code: '7004',
      kecamatan_name: 'KEMBANGAN',
      kecamatan_geolocation: '-6.191140471555,106.74130492346',
    },
  }
}

export default getDefaultAddress
