export const getUserAgent = () => {
  if (typeof navigator !== 'undefined') {
    return navigator?.userAgent || ''
  }
  return ''
}
