import Cookies from 'js-cookie'
import config from '../../../../config'
import GetB2bType from '../GetB2bType'
import { ACE_HARDWARE } from './AceConstants'

export const SHIPPING_LOCATION = 'shipping_location'

export const JABODETABEK_CITY_ID = [
  '514',
  '515',
  '526',
  '569',
  '570',
  '571',
  '572',
  '573',
  '619',
  '908',
  '909',
  '910',
  '527',
]

export const DEFAULT_HEADER = {
  'X-Company-Name': config.companyName,
  'X-Frontend-Type': config.environment,
  'user-platform': config.environment,
  'b2b-type': GetB2bType(true),
  'rr-sid': Cookies.get('rr-sid') || 'None',
}

export const CURRENT_OFFER = '-penawaran-saat-ini'

export const COMPANY_NAME =
  config.companyCode === 'AHI'
    ? ACE_HARDWARE
    : config.companyCode === 'HCI'
      ? 'INFORMA'
      : config.companyCode === 'TGI'
        ? 'TOYS KINGDOM'
        : config.ruparupaName

// WISHLIST

export const WISHLIST_EMPTY_STATE_TITLE = 'Belum Ada Wishlist'

export const WISHLIST_EMPTY_STATE_DESCRIPTION =
  'Jangan sampai ketinggalan. Masukkan wishlist-mu dan beli kapan saja'

export const WISHLIST_ITEM_LIMIT = 16

export const WISHLIST_PRICE_INFORMATION =
  '*Perbedaan harga mungkin terjadi, harga terbaru tertera pada halaman detail produk'

export const WISHLIST_RECOMMENDATION_CLOUDINARY_IMAGE_RESIZE_LENGTH = (
  length,
) => {
  return `w_${length + 8},h_${length + 8}`
}

export const WISHLIST_STOCK_AVAILABILITY_FILTER = [
  { name: 'Tersedia', value: 1 },
  { name: 'Stok Habis', value: 0 },
]

export const WISHLIST_EXCLUDE_CATEGORY_FILTER = ['Tantangan Seru', 'Best Deals']

export const WISHLIST_MOBILE_URL = () => {
  return 'wishlist'
}

export const REVIEW_RATING_TEXT_AREA_PLACEHOLDER =
  'Yuk ulas produk ini untuk membantu Ruppers lainnya'

export const REVIEW_RATING_OFFLINE_SERVICE_INDICATORS = {
  product_completeness_rating: {
    name: 'Kelengkapan Produk',
  },
  service_speed_rating: {
    name: 'Kecepatan Pelayanan',
  },
  store_environment_rating: {
    name: 'Kenyamanan Toko',
  },
  hospitality_rating: {
    name: 'Keramahan',
  },
  staff_knowledge_rating: {
    name: 'Pengetahuan Staff Toko',
  },
}

export const REVIEW_RATING_OFFLINE_SERVICE_DEFAULT_STATE = {
  product_completeness_rating: 5,
  service_speed_rating: 5,
  store_environment_rating: 5,
  hospitality_rating: 5,
  staff_knowledge_rating: 5,
}

export const REVIEW_RATING_ONLINE_SERVICE_INDICATORS = {
  service_speed_rating: {
    name: 'Kecepatan Pelayanan',
  },
  packing_rating: {
    name: 'Kualitas Pengemasan',
  },
  shipment_rating: {
    name: 'Pengiriman',
  },
}

export const REVIEW_RATING_ONLINE_SERVICE_DEFAULT_STATE = {
  service_speed_rating: 5,
  packing_rating: 5,
  shipment_rating: 5,
}

export const LOGO_RUPARUPA_DESKTOP =
  'https://cdn.ruparupa.io/media/promotion/ruparupa/upcdn/deskweb-on-all-pages.svg'

// My Bank Account

export const BANK_ACCOUNT_EMPTY_STATE_TITLE =
  'Belum Ada Rekening Bank Tersimpan'

export const BANK_ACCOUNT_EMPTY_STATE_DESCRIPTION =
  'Tambahkan rekening bank untuk mempermudah proses pengembalian danamu'

export const BANK_ACCOUNT_REFUND_HISTORY_EMPTY_STATE_TITLE =
  'Belum Ada Riwayat Pengembalian Dana'

export const BANK_ACCOUNT_EXCLUDE_CHECK_LIST = ['CENAIDJA']

export const BANK_ACCOUNT_MAXIMUM_OTP_LIMIT = 3

// Faktur Pajak Pesanan Saya

export const FAKTUR_PAJAK_WAJIB_PAJAK_TYPE_LIST = [
  { name: 'Wajib Pajak Badan Pusat' },
  { name: 'Wajib Pajak Badan Cabang' },
  { name: 'Wajib Pajak Perorangan' },
]
// Catalogues
export const CATALOGUES_COMPANIES = [
  {
    companyUrl: 'informastore',
    companyName: 'informa',
    bannerUrl:
      config.environment === 'desktop'
        ? 'https://cdn.ruparupa.io/media/promotion/ruparupa/catalog/HCI/informa_desktop.jpg'
        : 'https://cdn.ruparupa.io/media/promotion/ruparupa/catalog/HCI/informa_mobile.png',
  },
  {
    companyUrl: 'azko', //ACE_HARDWARESTORE
    companyName: 'ace', //ACE_HARDWARE
    bannerUrl:
      config.environment === 'desktop'
        ? 'https://cdn.ruparupa.io/ahoi/oms/OMS_E-Catalogue%20Banner%20Desktop.webp' // ACE_IMAGE
        : 'https://cdn.ruparupa.io/ahoi/oms/OMS_E-Catalogue%20Banner%20Mobile.webp', // ACE_IMAGE
  },
  {
    companyUrl: 'https://selma.co.id/catalogues',
    companyName: 'selma',
    bannerUrl:
      config.environment === 'desktop'
        ? 'https://cdn.ruparupa.io/media/promotion/selma/e-catalogue/selma_ecatalogue_banner.jpg'
        : 'https://cdn.ruparupa.io/media/promotion/selma/e-catalogue/selma_ecatalogue_banner.jpg',
  },
  {
    companyUrl: 'toyskingdomonline',
    companyName: 'toys_kingdom',
    bannerUrl:
      config.environment === 'desktop'
        ? 'https://cdn.ruparupa.io/media/promotion/ruparupa/catalog/TGI/toys_kingdom_desktop.jpg'
        : 'https://cdn.ruparupa.io/media/promotion/ruparupa/catalog/TGI/toys_kingdom_mobile.png',
  },
  {
    companyUrl: '', // Empty karena gabung sama ODI, cuma ditambah query di url
    companyName: 'krisbow',
    bannerUrl:
      config.environment === 'desktop'
        ? 'https://cdn.ruparupa.io/media/promotion/ruparupa/catalog/KLS/desktop-alt2.jpg'
        : 'https://cdn.ruparupa.io/media/promotion/ruparupa/catalog/KLS/mobile-alt2.jpg',
  },
]

export const CATALOGUES_ROW_FILTERS = {
  // taken from the index of the first row that contains promo images in "semua-promo" campaign
  AHI: config.environment === 'desktop' ? 4 : JSON.stringify([4, 5, 6, 7, 8]),
  HCI: config.environment === 'desktop' ? 3 : JSON.stringify([3, 4, 5, 6, 7]),
}

export const CATALOGUES_SIZE = config.environment === 'desktop' ? 20 : 12

export const DUMMY_CATALOGUES_DATA = Array.from(
  { length: config.environment === 'desktop' ? 5 : 4 },
  () => ({
    id: Math.random(),
    img_src: '',
    title: 'Katalog',
    start_date: new Date(),
    end_date: new Date(),
  }),
)

// Installation

export const INSTALLATION_SERVICE_SUBMISSION_DEADLINE = 14

export const INSTALLATION_INFOBOX_TEXT = `Jasa perakitan masih bisa ditambahkan maksimal ${INSTALLATION_SERVICE_SUBMISSION_DEADLINE} hari dari tanggal pesanan selesai`

export const INSTALLATION_CANCEL_MODAL_TEXT = `Kamu masih bisa melakukan penambahan jasa perakitan hingga ${INSTALLATION_SERVICE_SUBMISSION_DEADLINE} hari dari
barang diterima`

export const INSTALLATION_HIDE_ERECEIPT_STATUS = [
  'canceled',
  'installer_confirmation',
  'waiting_payment',
  'waiting_shipment',
]
