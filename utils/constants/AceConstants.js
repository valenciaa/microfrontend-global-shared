export const ACE_HARDWARE = 'AZKO'
export const ACE_B2B_PREFIX = 'azkob2b'
export const ACE_B2B_BASEURL = 'azko/page/azkob2b'
export const ACE_PREFIX = 'azko'
export const ACE_REWARDS = 'AZKO'
export const LINK_ACESTORE = 'acestore/'

// Note:
// Mentions used in logic / sent over network is only marked with comment, not replaced
// Images & Image URLs Are Marked with ACE_IMAGE / Replaced
// ACE_OG_DATA_COMPANY need to recheck
