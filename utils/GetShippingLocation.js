import Cookies from 'js-cookie'
import localforage from 'localforage'
import { SHIPPING_LOCATION } from './constants'

const getShippingLocation = async () => {
  const localforageResponse = await localforage.getItem('location_data')
  let location = JSON.parse(localforageResponse)

  if (!location || !location?.longitude || !location?.accurateLocation) {
    let shippingLocation = Cookies.get(SHIPPING_LOCATION)
    let longlat = '-6.191140471555,106.74130492346'
    if (typeof shippingLocation !== 'undefined') {
      shippingLocation = JSON.parse(shippingLocation)
      if (shippingLocation?.kecamatan?.kecamatan_geolocation) {
        longlat = shippingLocation?.kecamatan?.kecamatan_geolocation
      } else {
        longlat = ''
      }
    }
    longlat = longlat.split(',')
    location = {
      latitude: longlat[0],
      longitude: longlat[1],
    }
  }

  return location
}

export default getShippingLocation
