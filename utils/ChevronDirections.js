export default {
  chevronDown: {
    // first used in EditLocationsGroup.js
    d: 'M4.516 7.548c.436-.446 1.043-.481 1.576 0L10 11.295l3.908-3.747c.533-.481 1.141-.446 1.574 0 .436.445.408 1.197 0 1.615-.406.418-4.695 4.502-4.695 4.502a1.095 1.095 0 0 1-1.576 0S4.924 9.581 4.516 9.163c-.409-.418-.436-1.17 0-1.615z',
    viewBox: '0 0 19 20',
    width: '23',
    fill: 'black',
  },
  chevronUp: {
    // first used in EditLocationsGroup.js
    d: 'M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6l-6 6l1.41 1.41Z',
    viewBox: '0 0 22 22',
    width: '25px',
    fill: 'black',
  },
  // please add more object if any of these chevron above do not meet your conditions
}
