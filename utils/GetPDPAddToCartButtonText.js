import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import GetIsMaxQtyEmpty from './GetIsMaxQtyEmpty'

const getPDPAddToCartButtonText = (
  isLoadingAddToCart,
  productCanDelivery,
  productMaxStock,
  cartText = 'Tambah ke Keranjang',
) => {
  const isMaxQtyEmpty = GetIsMaxQtyEmpty(productMaxStock)
  if (isLoadingAddToCart) {
    return 'Memuat'
  } else {
    if (
      (!isEmpty(productCanDelivery) && !productCanDelivery?.can_add_to_cart) ||
      (config.isB2b && isMaxQtyEmpty)
    ) {
      return 'Stok Habis'
    } else if (productMaxStock?.max_stock > 0) {
      return cartText
    } else {
      return 'Stok Habis'
    }
  }
}

export default getPDPAddToCartButtonText
