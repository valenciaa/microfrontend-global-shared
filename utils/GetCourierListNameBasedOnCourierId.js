import { ACE_HARDWARE } from './constants/AceConstants'

const getCourierListNameBasedOnCourierId = (courierId) => {
  if (courierId === 2) {
    return 'JNE'
  }
  if (courierId === 3) {
    return 'Ownfleet'
  }
  if ([7, 8].includes(courierId)) {
    return 'Gosend'
  }
  if (courierId === 9) {
    return 'NCS'
  }
  if (courierId === 11) {
    return 'Informa Delivery'
  }
  if (courierId === 13) {
    return `${ACE_HARDWARE} Delivery`
  }
  if (courierId === 23) {
    return 'Bluebird'
  }
  if (courierId === 33) {
    return 'JNE JTR'
  }
}

export default getCourierListNameBasedOnCourierId
