const onChangeBankAccountOtp = (e, index, otp, setOtp) => {
  const next = e.target.tabIndex
  const tempOtp = otp.slice()
  tempOtp.splice(index, 1, e.target.value)
  setOtp(tempOtp)
  focusNextOTPField(e, next, otp)
}

const inputFocusBesidesNumberKey = (e, index, otp, setOtp) => {
  const allowButtonList = ['Backspace', 'Delete', 'ArrowLeft', 'ArrowRight']
  if (isNaN(e.key) && !allowButtonList?.includes(e.key)) {
    e.preventDefault()
  }
  const pressedKey = e.key
  const next = e.target.tabIndex
  switch (pressedKey) {
    case 'Backspace':
    case 'Delete': {
      e.preventDefault()
      if (otp[index]) {
        const tempOtp = [...otp]
        tempOtp.splice(index, 1, '')
        setOtp(tempOtp)
      } else {
        focusPrevOTPField(e, next)
      }
      break
    }
    case 'ArrowLeft': {
      e.preventDefault()
      focusPrevOTPField(e, next)
      break
    }
    case 'ArrowRight': {
      e.preventDefault()
      focusNextOTPField(e, next, otp)
      break
    }
  }
}

const focusNextOTPField = (e, next, otp) => {
  if (next < otp.length - 1) {
    // e.target.form.elements[next + 1].focus()
    e.target.nextElementSibling.focus()
  } else if (next === otp.length - 1) {
    // e.target.form.elements[otp.length - 1].focus()
  }
}

const focusPrevOTPField = (e, next) => {
  if (next > 0) {
    e.target.previousElementSibling.focus()
  } else {
    // e.target.form.elements[0].focus()
  }
}

export { inputFocusBesidesNumberKey, onChangeBankAccountOtp }
