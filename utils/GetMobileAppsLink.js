const GetMobileAppsLink = (device, companyCode) => {
  // const missAce = 'https://acemobileapp.page.link/DownloadMissAce' // takeout deeplink MISS ACE since AHI has joined KLK
  let ruparupa = 'https://ruparupa.onelink.me/xDlg/appdownloadfrommobile'
  if (device === 'android') {
    ruparupa =
      'https://play.google.com/store/apps/details?id=com.mobileappruparupa&hl=en&gl=US'
  } else if (device === 'ios') {
    ruparupa = 'https://apps.apple.com/id/app/ruparupa/id1324434624?l=id'
  }
  const informa = 'https://informamobileapp.page.link/home'

  return companyCode === 'HCI' ? informa : ruparupa
}

export default GetMobileAppsLink
