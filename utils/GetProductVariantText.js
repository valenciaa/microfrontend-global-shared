import capitalize from 'lodash/capitalize'

const getProductVariantText = (value) => {
  const isFirstLetterNumeric = Number.isInteger(parseInt(value?.charAt(0)))

  if (isFirstLetterNumeric) {
    return value
  } else {
    return capitalize(value)
  }
}

export default getProductVariantText
