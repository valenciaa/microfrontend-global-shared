export default function StatusBarConverter(balance, requirement) {
  const percentage = (balance / (balance + requirement)) * 100

  return `${percentage}%`
}
