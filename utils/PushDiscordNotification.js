import { create } from 'apisauce'
const WEBHOOK_URL =
  '1274985333045329930/1RqkKRRP6YrG6Bg42pZ1ScTyHZWclZ9DTNRQgW2W1XjrQisE6mBCZ5f4DpcCk6e5CenY'

const api = create({
  baseURL: 'https://discord.com/api/webhooks/',
})

export const pushDiscordNotification = async (urlSeo, pageType) => {
  const payload = {
    username: 'Alert-SEO-Webhook',
    content: `
        > **Issue Detected: Wrong Meta Robot Configuration**
        > 
        > **URL:** ${urlSeo}
        > 
        > **Page Type:** ${pageType}
        > 
        > **Description:** The Meta Robot setting for the above URL is incorrect. Please review and correct the Meta Robot configuration to ensure proper SEO indexing and visibility.`,
  }

  try {
    api.setHeaders({
      'Content-Type': 'application/json',
    })

    const response = await api.post(WEBHOOK_URL, payload)

    if (response.ok) {
      console.log('Push notification sent successfully')
    } else {
      console.error('Push notification failed with status:', response.status)
    }
  } catch (error) {
    console.error('Error sending push notification:', error)
  }
}
