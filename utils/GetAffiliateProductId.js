import GetParam from './GetParam'
const GetAffiliateProductId = () => {
  const url = GetParam('deep_link_value')
    ? GetParam('deep_link_value').split('_')[3]
    : ''

  return url
}

export default GetAffiliateProductId
