// NOTES!
// CB: China brand security problem, mostly because the acces to the trigger is prohibited
export const IGNORED_ERROR_MESSAGE_SENTRY = [
  't is not a function',
  'Database deleted by request of the user',
  'Failed to fetch',
  'undefined is not an object',
  'The quota has been exceeded',
  'Illegal invocation',
  'A mutation operation was attempted on a database that did not allow mutations',
  'Transaction timed out due to inactivity',
  'UnknownError',
  'Non-Error promise rejection captured with keys: data, status, success',
  'Access is denied for this document',
  'ResizeObserver loop limit exceeded',
  'No available storage method found',
  'QuotaExceededError',
  'criteoImpressions is undefined', // google ads related
  "Can't find variable: IntersectionObserver",
  't.postMessage is not a function',
  'AbortError: Version change transaction was aborted in upgradeneeded event handler', // moengage related
  "Unexpected token '<'",
  'The database is not running a version change transaction', // web view problem accessing indexdb
  'One of the specified object stores was not found', // moengage related
  'Clipboard write is not allowed',
  'NotAllowedError: Write permission denied',
  'fireReadyEvent', // CB problem
  'fireChangeEvent', // CB problem
  'audioVolumeChange', // CB problem
  'setCurrentPosition', // CB problem
  'fireEvent', // CB problem
  'setScreenSize', // CB problem
  'offsetTop', // CB problem
  'postMessage', // CB problem
  'setMaxSize', // CB problem
  'setDefaultPosition', // CB problem
  'setIsViewable', // CB problem
  'runCustomize', // CB problem
  'Non-Error promise rejection captured with value:',
  'r.WEB_SETTINGS',
  'window.AF_SDK.PLUGINS[e[n][1].toUpperCase()] is undefined',
  'window.AF.plugins is undefined',
  'Object captured as promise rejection with keys: data, status, success',
  'ToutiaoJSBridge._handleMessageFromToutiao is not a function',
  "Failed to read the 'cssRules' property from 'CSSStyleSheet': Cannot access rules",
  'InvalidStateError: Object store cannot be found in the backing store',
  'AbortError: The operation was aborted.',
  'AbortError: The transaction was aborted, so the request cannot be fulfilled.',
  'domInteractive',
  'The operation is insecure.',
  'window.jtjb.click is not a function', // google ads
  'interceptXmlHttpRequest',
  'createObjectStore',
  'QuotaExceededError',
  'scrollReadRandom', // several scroll in some phone gave this kind of error, can't be replicate, it happens when the the modal for open the in apps
  'Blocked a frame with origin',
  'doGameClick',
  'b985903d-f45c-4568-a8ec-471e9121c85c_ruparupa-desktop', // storage full problem, 'Failed to execute 'setItem' on 'Storage': Setting the value of 'b985903d-f45c-4568-a8ec-471e9121c85c_ruparupa-desktop' exceeded the quota.'
  'Unreachable hosts - your application id may be incorrect', // all of this problem come from ip Kawan Lama
  'getReadModeConfig',
  'Java object is gone',
  '_AutofillCallbackHandler',
  'JSON Parse error: Unexpected identifier',
  'track_event', // this is from moengage sdk
  'captured as promise rejection', // for err Event `Event` (type=error) captured as promise rejection (Freshchat)
  'Cannot redefine property: googletag',
  'window.ScarabQueue is undefined', // emarsys
  'a._dbInfo is null', // moengage relater
  'more than 100 times per 30 seconds',
  'b.container is undefined', //google tag related
  '198230182308109283091823098102938908128390',
  'matched_personal_last_seen_valid_date',
  'An invalid or illegal string was specified', //gtm related
  '92329746', //gtag related
  '127160896', //gtag related
  'processRandomSelector',
  'object SQLError',
  'IDBDatabase', //moengage related
  'originalPrompt',
  'object Object',
  'gpt-',
  'AbortError: The connection was closed',
  'onPagePause is not defined',
  'sbNormalizedTrackingConfigurations',
  'ifr2top',
  'e.templateType',
  'ResizeObserver loop completed with undelivered notifications',
  'Permission denied to access property',
  'parentElement',
  'invalid setting for restrictSearchableAttributes', // algolia related
  'Index matching does not exist', // aloglia related
  'Document is not focused',
  'The user agent does not support public key credentials',
  'secret is not defined',
  'a._dbInfo.db', //moengage related
  'this.displayManager.queueLiveCampaign', //moengage related
  'last_meta_call_time', //moengage related
  'apis.google.com', // google api related
  'tgetT is not defined', // this sentry related
  'getReadModeConfig', // extension heyTap related
  'getReadModeExtract', // extension heyTap related
  'getReadModeRender', // extension heyTap related
  'Unexpected end of script',
  'Unexpected end of input',
  '_pcmBridgeCallbackHandler',
  'Unexpected token o in JSON at position 1',
  'getRestrictions',
  'Retries exceeded',
  'to be interpolated properly',
  'is not valid JSON',
  'Action Site URL and Success Page URL Not Match',
  'out of memory',
  'on proxy: trap returned falsish for property',
  'failed to decode param',
  'MOE_DATA',
  'TiktokAnalyticsObject',
  'WeixinJSBridge',
  'WebViewJavascriptBridge',
  'Invariant: attempted to hard navigate to the same URL',
  'Java bridge method invocation error',
  'Could not get payload for 65490994c6ed2aeab48460fd_F_T_ON_AB_0_P_0_UEI_1701087733000_UED_21_FL_O_FP_P3_L_0',
  'Object captured as exception with keys: message, name, statusCode',
  'Load failed',
  'NetworkError when attempting to fetch resource.', // problems with the network or server when trying to access or retrieve resources
  'itemList', // err in GA4
  'querySelector', // err web-widget-5178
  'emptyQ',
  'insertBefore', //  err in ./node_modules/@sentry/browser/esm/helpers.js in sentryWrapped
  'Web SDK Settings',
  'Error in getting Web SDK Settings. This might indicate that the App is blocked.',
  'jQuery is not defined', // gtm related
  '_lt is not defined',
  'globalThis',
  'adKill',
  '654908f8defa5cb7d3107b26_F_T_ON_AB_0_P_0_UEI_1720881463000_UED_250_FL_O_FP_P2_L_0',
  'campaignRenderMap', // moengage
  'matched_personal_last_seen',
  'Web SDK',
  'displayManager', // moengage
  'This might indicate that the App is blocked', // moengage
  'Error in getting Web SDK Settings', // moengage
  'aroundLatLng', // algolia
  'parentNode',
  'between 0 and 9223372036854775807',
  'window.AF is not a function',
  'No connection to push daemon',
  'serviceName is not defined',
  'sharedStorage',
  'webVitals',
  'hj is not defined', //gtm
  'appendChild',
  'removeChild',
  'vid_mate_check',
  'sessionId',
  'auryc',
  'Should not already be working',
  'Replace_with_Variable_Revenue_Function',
  // prettier-ignore
  'Can\'t find variable: ecommerce',
  'ecommerce',
  'IFrameMessageHandler',
  'clarity',
  'clearClarity',
  'Hydration Error',
  'Text content does not match server-rendered HTML',
  'Java exception was raised during method invocation',
  'al_onAdViewRendered',
  'Cannot read properties of undefined (reading \'addEventListener\')',
  'Cannot read properties of null (reading \'addEventListener\')',
  'setRemoteDescription',
  'RTCPeerConnection',
  'Cannot read property \'serialize\' of undefined',
  'ReferenceError: xbrowser is not defined',
  'TypeError: Cannot read properties of undefined (reading \'L\')',
  'Route Cancelled',
  'unknown',
  'No error message',
  'gmo' // google issue for ios only
]
