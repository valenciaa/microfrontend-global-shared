import { IGNORED_ERROR_MESSAGE_SENTRY } from './sentryConstant'

export function isInjectedCode(event) {
  const frames = event?.exception?.values?.[0]?.stacktrace?.frames

  if (!frames || frames.length === 0) {
    return false
  }

  // ? To handle injected code from outside ruparupa (ex: gtm, extension)
  const firstFrame = frames[0]
  if (firstFrame?.filename === '<anonymous>') {
    return true
  }

  return false
}

const sentryErrorMessages = IGNORED_ERROR_MESSAGE_SENTRY || []
export function filterEvent(event, hint) {
  const errorHint = hint.originalException
  const errorEvents = event.exception.values.map((el) => el.value)

  if ((errorEvents && errorEvents.length) || (errorHint && errorHint.message)) {
    for (let i = 0; i < sentryErrorMessages.length; i++) {
      const messageRegex = new RegExp(sentryErrorMessages[i], 'i')
      if (errorEvents && errorEvents.length) {
        const result = errorEvents.some((msg) => msg.match(messageRegex))
        if (result) {
          return true
        }
      }

      if (
        errorHint &&
        errorHint.message &&
        errorHint.message.match(messageRegex)
      ) {
        return true
      }
    }
  }

  return false
}
