import { ACE_HARDWARE } from './constants/AceConstants'

const GetFooterText = (pageFrom, layoutContext, metaDataSeo) => {
  let footerText = layoutContext
  const stringWithoutPrefix = metaDataSeo?.name?.split('---')[1]
  const storeName = stringWithoutPrefix
    ? stringWithoutPrefix
        .replace(/-/g, ' ')
        .replace(/\b\w/g, (l) => l.toUpperCase())
    : ''

  if (pageFrom === 'store') {
    if (metaDataSeo?.name?.includes('ace')) {
      //ACE_HARDWARE
      footerText = `Dapatkan aneka produk ${ACE_HARDWARE} Indonesia, pusat perlengkapan, kebutuhan rumah tangga dan gaya hidup terlengkap. Temukan aneka produk rumah tangga dan gaya hidup terlengkap dari <strong>${storeName}</strong>. Nikmati juga promo Gratis Ongkir serta nikmati Cicilan 0% dari berbagai bank pilihan, serta dapatkan Garansi Resmi serta garansi 14 hari pengembalian produk & bisa ambil di toko terdekat dengan berbelanja hanya didi ruparupa sebagai e-Commerce resmi ${ACE_HARDWARE} Indonesia. Jadi Tunggu apa lagi ? segera beli dan miliki produk dari brand pilihan terbaik di ${ACE_HARDWARE} dengan kualitas terjamin di sekarang juga!`
    }

    if (metaDataSeo?.name?.includes('informa')) {
      footerText = `Dapatkan aneka produk home living dan furniture terbaik dari Informa Indonesia. Temukan aneka produk rumah tangga dan gaya hidup terlengkap dari <strong>${storeName}</strong>. Nikmati juga promo Gratis Ongkir serta nikmati Cicilan 0% dari berbagai bank pilihan, serta dapatkan Garansi Resmi dengan berbelanja hanya di ruparupa sebagai e-Commerce resmi ${ACE_HARDWARE}, Informa, Toys Kingdom. Gratis ongkir, cicilan 0%, garansi 14 hari & bisa ambil di toko terdekat. Jadi Tunggu apa lagi ? segera beli dan miliki produk dari brand pilihan terbaik di Informa dengan kualitas terjamin di sekarang juga!`
    }

    if (metaDataSeo?.name?.includes('toys')) {
      footerText = `Temukan berbagai  koleksi mainan anak edukatif, inspiratif, berkualitas dan mainan hobi untuk segala usia di <strong>${storeName}</strong>. Dapatkan Garansi Resmi dengan berbelanja hanya di ruparupa sebagai e-Commerce resmi Toys Kingdom Online. Nikmati juga promo Gratis Ongkir serta nikmati Cicilan 0% dari berbagai bank pilihan, garansi 14 hari & bisa ambil di toko terdekat. Jadi Tunggu apa lagi ? segera beli dan miliki produk dari brand pilihan terbaik di Toys Kingdom Online dengan kualitas terjamin di sekarang juga!`
    }

    if (metaDataSeo?.name?.includes('selma')) {
      footerText = `Dapatkan aneka produk home living dan furniture terbaik dari SELMA di <strong>${storeName}</strong>. Nikmati juga promo Gratis Ongkir serta nikmati Cicilan 0% dari berbagai bank pilihan, serta dapatkan Garansi Resmi dengan berbelanja hanya di ruparupa sebagai e-Commerce resmi SELAM. Gratis ongkir, cicilan 0%, garansi 14 hari & bisa ambil di toko terdekat. Jadi Tunggu apa lagi ? segera beli dan miliki produk dari brand pilihan terbaik di SELMA Online dengan kualitas terjamin di sekarang juga!`
    }

    if (metaDataSeo?.name?.includes('informa-elektronik')) {
      footerText = `Dapatkan aneka produk elektronik rumah tangga terbaik dan berkualitas dari <strong>${storeName}</strong>. Dapatkan Garansi Resmi dengan berbelanja hanya di ruparupa sebagai e-Commerce resmi Informa Electronics. Nikmati juga promo Gratis Ongkir serta nikmati Cicilan 0% dari berbagai bank pilihan, garansi 14 hari & bisa ambil di toko terdekat. Jadi Tunggu apa lagi ? segera beli dan miliki produk dari brand pilihan terbaik di Informa Electronics dengan kualitas terjamin di sekarang juga!`
    }

    if (metaDataSeo?.name?.includes('ataru')) {
      footerText = `Dapatkan berbagai produk kebutuhan sehari-hari. Belanja kebutuhan harian dengan produk lifestyle unik dan inovatif.terbaik dan berkualitas dari <strong>${storeName}</strong>. Dapatkan Garansi Resmi dengan berbelanja hanya di ruparupa sebagai e-Commerce resmi Ataru Indonesia. Nikmati juga promo Gratis Ongkir serta nikmati Cicilan 0% dari berbagai bank pilihan, garansi 14 hari & bisa ambil di toko terdekat. Jadi Tunggu apa lagi ? segera beli dan miliki produk dari brand pilihan terbaik di Ataru dengan kualitas terjamin di sekarang juga!`
    }

    if (metaDataSeo?.name?.includes('eyesoul')) {
      footerText = `Dapatkan beragam koleksi  kacamata berkualitas untuk menunjang penglihatan dan mode untuk anak-anak hingga dewasa. Tingkatkan Penglihatan dengan kacamata stylish dan berkualiats di <strong>${storeName}</strong>. Dapatkan Garansi Resmi dengan berbelanja hanya di ruparupa sebagai e-Commerce resmi Ataru Indonesia. Nikmati juga promo Gratis Ongkir serta nikmati Cicilan 0% dari berbagai bank pilihan, garansi 14 hari & bisa ambil di toko terdekat. Jadi Tunggu apa lagi ? segera beli dan miliki produk dari brand pilihan terbaik di Eyesoul dengan kualitas terjamin di sekarang juga!`
    }
  }

  return footerText
}

export default GetFooterText
