import config from '../../../config'
import { useCreateInstantCheckout } from '../services/api/mutations/cart'
import { useGetMembershipDetail } from '../services/api/queries/global'
import GetShopbackUrl from './GetShopbackUrl'

export function useMembershipUpgrade() {
  const {
    data: memberDetail,
    isLoading: isLoadingMemberDetail,
    isFetching,
  } = useGetMembershipDetail({
    needGoldBenefits: true,
  })

  const { mutate: createInstantCheckout } = useCreateInstantCheckout({
    onSuccess: (res) => {
      if (res?.data?.data && res?.data?.data?.cart_id) {
        const { cart_id: cartId } = res.data.data

        window.location =
          config.paymentURL + '?token=' + cartId + GetShopbackUrl()
      }
    },
  })

  const memberInstantUpgrade = async () => {
    if (
      !isLoadingMemberDetail &&
      !isFetching &&
      memberDetail &&
      memberDetail?.length
    ) {
      const product = memberDetail?.[0]?.instant_upgrade_att

      const InstantUpgradedata = {
        items: [
          {
            sku: product?.sku_instant_upgrade,
            qty_ordered: product?.sku_instant_upgrade_qty_ordered,
            shipping: {
              delivery_method: product?.sku_instant_upgrade_delivery_method,
              is_update_store_code:
                product?.sku_instant_upgrade_is_update_store_code,
              store_code: product?.sku_instant_upgrade_store_code,
              default_store_code:
                product?.sku_instant_upgrade_default_store_code,
            },
          },
        ],
        device: config.environment,
        cart_type: 'instant',
        company_code: config.companyCode,
      }

      await createInstantCheckout(InstantUpgradedata)
    }
  }

  return { memberInstantUpgrade }
}
