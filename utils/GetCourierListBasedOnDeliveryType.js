const getCourierBasedOnDeliveryType = (couriers, deliveryType) => {
  const ownfleet = [11, 13]
  const instant = [7, 23]
  const sameday = [8]
  const regular = [7, 8, 11, 13, 23]

  switch (deliveryType) {
    case 'ownfleet':
      return couriers?.filter((courier) =>
        ownfleet?.includes(courier?.courier_id),
      )
    case 'instant':
      return couriers?.filter((courier) =>
        instant?.includes(courier?.courier_id),
      )
    case 'sameday':
      return couriers?.filter((courier) =>
        sameday?.includes(courier?.courier_id),
      )
    default:
      return couriers?.filter(
        (courier) => !regular?.includes(courier?.courier_id),
      )
  }
}

export default getCourierBasedOnDeliveryType
