import config from '../../../config'

export default function SetRRRLogo() {
  let logo
  switch (config.companyCode) {
    case 'HCI':
    case 'TGI':
      logo =
        'https://cdn.ruparupa.io/media/promotion/ruparupa/ruparupa-rewards/logo-rrr-reverse-bu-online.PNG'
      break
    default:
      logo =
        'https://cdn.ruparupa.io/media/promotion/ruparupa/asset-cohesive/Logo-rewards-di-Profil.png'
  }
  return logo
}
