import config from '../../../config'

const GetPhotoWatermarkValue = () => {
  if (config.companyCode === 'AHI') {
    return 'l_ace:c5f05d:8cb580'
  }

  return 'l_2.1:c5f05d:8cb583'
}

export default GetPhotoWatermarkValue
