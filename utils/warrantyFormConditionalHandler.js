export const warrantySerialErrorHandler = (errors) => {
  return (
    errors.serial_number?.type === 'required' && (
      <p role='alert' className='ui-text-4 color-red-50 padding-bottom-xs'>
        Serial Number tidak boleh kosong
      </p>
    )
  )
}

export const warrantyCardErrorHandler = (errors) => {
  return (
    errors.warranty_card?.type === 'required' && (
      <p role='alert' className='ui-text-4 color-red-50 padding-bottom-xs'>
        Kartu Garansi tidak boleh kosong
      </p>
    )
  )
}

export const postErrorHandler = (errors) => {
  return (
    <p role='alert' className='ui-text-4 color-red-50 padding-bottom-xs'>
      {errors}
    </p>
  )
}

export const warrantyRequiredHandler = (warrantyBool) => {
  if (!warrantyBool) {
    return {
      required: true,
    }
  }
  return {
    required: false,
  }
}
