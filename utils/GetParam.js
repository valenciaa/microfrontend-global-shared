const GetParam = (params) => {
  let paramValue = null
  if (typeof window !== 'undefined' && window.location) {
    try {
      paramValue = new URLSearchParams(window.location.search).get(params)
      return paramValue
    } catch (error) {
      console.error('Error parsing URL parameters:', error)
    }
  }
}
export default GetParam
