import isEmpty from 'lodash/isEmpty'
import config from '../../../config'
import { GetAuthData } from '../../../src/utils/misc/GetAuthData'
import { emailBlur } from './String'
import isArray from 'lodash/isArray'

// regis / login / klk activation in ruparupa
export function accessCustomerDatalayer(event = '', loginType = '') {
  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: event,
      method: loginType,
      company_code: config.companyCode,
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// page pdp
export function productDetailDatalayer(productDetail = [], event = '') {
  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })

    const dataItemCurrents = []
    const totalPrice = []
    productDetail.map((data) => {
      totalPrice.push(
        data?.prices?.[0]?.special_price > 0
          ? data?.prices?.[0]?.special_price
          : data?.prices?.[0]?.price,
      )
      let affiliation = []
      data?.store_available?.map((store) => {
        affiliation.push(store.supplier_alias)
      })
      dataItemCurrents.push({
        item_id: data?.sku || '1',
        item_name: data?.name || 'no product name',
        affiliation: [...new Set(affiliation)].toString(),
        discount:
          data?.prices?.[0]?.special_price > 0
            ? (data?.prices?.[0]?.price || 0) -
              (data?.prices?.[0]?.special_price || 0)
            : 0,
        item_brand: data?.brand?.name || '',
        quantity: 1,
        item_category: data?.trackBreadcrumb?.[0]?.name || '',
        item_category2: data?.trackBreadcrumb?.[1]?.name || '',
        item_category3: data?.trackBreadcrumb?.[2]?.name || '',
        item_category4: data?.trackBreadcrumb?.[3]?.name || '',
        item_list_id: data?.trackBreadcrumb?.[3]?.category_id || '',
        item_list_name: data?.trackBreadcrumb?.[3]?.name || '',
        price: data?.prices?.[0]?.price || 0,
      })
    })

    const initialValue = 0
    const sumWithInitial = totalPrice.reduce(
      (accumulator, currentValue) => accumulator + currentValue,
      initialValue,
    )

    const dataCurrets = {
      currency: 'IDR',
      items: dataItemCurrents,
      value: sumWithInitial,
    }

    // if (event === 'view_item') {
    //   dataCurrets.CriteoProductID = productDetail[0].sku
    //   dataCurrets.criteoPageType = 'ProductPage'
    // }
    let windowDatalayer = {
      event: event,
      ecommerce: {
        ...dataCurrets,
      },
      company_code: config.companyCode,
    }

    if (window && window.dataLayer) {
      window.dataLayer.push(windowDatalayer)
    }
  }
}

// page pcp
export async function productCategoryDatalayer(category, product) {
  let totalPrice = []
  let productDataLayer = []
  product?.map((data) => {
    let affiliation = []
    if (data.store_availableForGA) {
      data.store_availableForGA.map((store) => {
        affiliation.push(store.supplier_alias)
      })
    }
    let itemCategory = {}
    if (data[`breadcrumb_last_seen_${config.companyCode}`]) {
      if (data[`breadcrumb_last_seen_${config.companyCode}`].length >= 3) {
        itemCategory = {
          item_category:
            data[`breadcrumb_last_seen_${config.companyCode}`]?.[2]?.name || '',
          item_category2:
            data[`breadcrumb_last_seen_${config.companyCode}`]?.[1]?.name || '',
          item_category3:
            data[`breadcrumb_last_seen_${config.companyCode}`]?.[0]?.name || '',
        }
      } else if (
        data[`breadcrumb_last_seen_${config.companyCode}`].length >= 4
      ) {
        itemCategory = {
          item_category:
            data[`breadcrumb_last_seen_${config.companyCode}`]?.[3].name || '',
          item_category2:
            data[`breadcrumb_last_seen_${config.companyCode}`]?.[2].name || '',
          item_category3:
            data[`breadcrumb_last_seen_${config.companyCode}`]?.[1].name || '',
          item_category4:
            data[`breadcrumb_last_seen_${config.companyCode}`]?.[0].name || '',
        }
      }
    }
    totalPrice.push(
      data.variants[0].prices[0].special_price > 0
        ? data.variants[0].prices[0].special_price
        : data.variants[0].prices[0].price,
    )
    productDataLayer.push({
      item_id: data.variants[0].sku || 0,
      item_name: data.name || 0,
      affiliation: [...new Set(affiliation)].toString(),
      discount:
        data.variants[0].prices[0].special_price > 0
          ? data.variants[0].prices[0].price -
            data.variants[0].prices[0].special_price
          : 0,
      item_brand: data.brand.name || '',
      ...itemCategory,
      item_list_id: category?.category_id || 0,
      item_list_name: category?.name || '',
      price: data.variants[0].prices[0].price,
      quantity: 1,
    })
  })
  const initialValue = 0
  const sumWithInitial = totalPrice.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue,
  )
  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'view_item_list',
      ecommerce: {
        currency: 'IDR',
        value: sumWithInitial,
        items: productDataLayer,
      },
      company_code: config.companyCode,
    }
    window.dataLayer.push(windowDatalayer)
  }
  if (window && window.dataLayer && category) {
    const authData = await GetAuthData()
    const categoryPageViewDataLayer = {
      event: 'CategoryPageView',
      pageType: 'ListingPage',
      CatID: category?.category_id,
      email: authData ? authData?.email : '',
      customerID: authData ? authData?.customer_id : '',
    }
    window.dataLayer.push(categoryPageViewDataLayer)
  }
}

// all page
export function pageDataLayer(userId = '') {
  if (window && window.dataLayer) {
    window.dataLayer.push({
      user_id: userId.toString(),
    })
  }
}

// search product algolia
export function productSearchCategoryDatalayer(searchState = '') {
  if (window && window.dataLayer && config.companyCode) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'search',
      search_term: searchState,
      company_code: config.companyCode,
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// case ketika add to cart(shopthelook, pdp)
export async function productDetailDatalayerOld(product = '') {
  if (window && window.dataLayer) {
    const auth = await GetAuthData()
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'detail',
      productID: `${product.sku}`,
      CriteoProductID: `${product.sku}`,
      email: '',
      user_id: '',
      currencyCode: 'IDR',
      ecommerce: {
        detail: {
          products: [
            {
              name: `${product?.name}`,
              id: `${product?.sku}`,
              price: `${product?.prices?.[0]?.price || 0}`,
              brand: `${product?.brand?.name?.replace("'", '') || ''}`,
              category: `${
                product?.categories?.[0]?.name?.replace("'", '') || ''
              }`,
            },
          ],
        },
      },
    }

    if (!isEmpty(auth) && !isEmpty(auth.email) && !isEmpty(auth.customer_id)) {
      windowDatalayer = {
        email: `${emailBlur(auth?.email || '') || ''}`,
        user_id: `${auth?.customer_id.toString() || ''}`,
        ...windowDatalayer,
      }
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// case ketika add to cart(shopthelook, pdp)
export async function addToCartDataLayer(product = '') {
  const auth = await GetAuthData()
  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'addToCart',
      productID: `${product.sku}`,
      CriteoProductID: `${product.sku}`,
      email: `${emailBlur(auth?.email || '') || ''}`,
      user_id: `${auth?.customer_id.toString() || ''}`,
      ecommerce: {
        currencyCode: 'IDR',
        add: {
          products: [
            {
              name: `${product?.name}`,
              id: `${product?.sku}`,
              price: `${product?.prices?.[0]?.price || 0}`,
              brand: `${product?.brand?.name?.replace("'", '') || ''}`,
              category: `${
                product?.categories?.[0]?.name?.replace("'", '') || ''
              }`,
              quantity: 1,
            },
          ],
        },
      },
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// floating voucher(baso)
export function addfloatingVoucher(activeVoucher = '') {
  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'floatingVoucher',
      name: activeVoucher?.name,
      company_code: config.platform,
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// case delete cart
export function removeFromCart(product = '') {
  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'removeFromCart',
      ecommerce: {
        remove: {
          products: [
            {
              name: product?.name,
              id: product?.sku,
              price: product?.prices?.selling_price?.toString(),
              category: product?.category?.name || '',
              quantity: product?.qty_ordered,
            },
          ],
        },
      },
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// while click product card
export function productClickDatalayer(product = {}, index = 1) {
  let list = 'search'

  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'productClick',
      ecommerce: {
        actionField: { list },
        products: [
          {
            name: product?.variant?.[0]?.name || '',
            id: product?.variant?.[0]?.sku || 0,
            price: product?.variant?.[0]?.selling_price || 0,
            brand: product?.variant?.[0]?.brand.name || '',
            position: index + 1 || 0,
          },
        ],
      },
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// inbox(baso)
export function inboxPromoDatalayer(name = '') {
  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'inboxPromo',
      name: name,
      company_code: config.platform,
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// case out of stock product pdp
export function outOfStockDatalayer(productDetail = {}) {
  if (window && window.dataLayer) {
    window.dataLayer.push({
      ecommerce: null,
    })
    let windowDatalayer = {
      event: 'outOfStock',
      sku: productDetail?.variants?.[0]?.sku || '',
      name: productDetail?.variants?.[0]?.name || '',
    }
    window.dataLayer.push(windowDatalayer)
  }
}

// case while pdp have value property event marketing
export function eventDatalayer(type = '', product = {}) {
  let event = ''
  switch (type) {
    case 'productRecoAws':
      event = 'productRecomendation'
      break
    case 'last-seen':
      event = 'productLastSeen'
      break
  }
  if (window && window.dataLayer) {
    let windowDatalayer = {
      event: event,
      name: product?.variants?.[0].name,
      sku: product.sku,
      price: product?.variants?.[0].prices[0].price,
      specialPrice: product?.variants?.[0].prices[0].special_price,
    }
    window.dataLayer.push({
      ecommerce: null,
    })
    window.dataLayer.push(windowDatalayer)
  }
}

// Google Analytic Layer using custom removedTier

const removeDataTier1 = {
  productID: undefined,
  CriteoProductID: undefined,
  email: undefined,
  customerID: undefined,
}

const removeDataTier2 = {
  // add new attribute name here to remove from tracker
}

const checkItemQty = ({ allItems = [], sku = '' }) => {
  if (!isArray(allItems)) {
    return 1
  }
  const userCart = allItems?.[0]?.group_items || []
  const findSkuInCart = userCart?.find((item) => item?.sku === sku)

  const cartItemQty = findSkuInCart?.qty_ordered
    ? findSkuInCart?.qty_ordered
    : 1

  return cartItemQty
}

export function ga4TrackViewMinicart({ allItems = null, summary }) {
  const filterCartItem = allItems?.[0]?.group_items?.filter(
    (item) => item?.is_checked === true,
  )
  let transformedItems = filterCartItem?.map((item) => ({
    item_id: item?.sku || '',
    item_name: item?.name || '',
    affiliation: '', // Add affiliation value if needed
    discount: item?.prices.normal_price - item?.prices?.selling_price,
    item_brand: item?.brand || '',
    item_category: item?.breadcrumbs?.[0] || '',
    item_category2: item?.breadcrumbs?.[1] || '',
    item_category3: item?.breadcrumbs?.[2] || '',
    item_category4: item?.breadcrumbs?.[3] || '',
    item_list_id: '', // Add category id lvl 4 if applicable
    item_list_name: item?.breadcrumbs?.[3] || '', // Assuming category name lvl 4 is the same as the 4th breadcrumb
    price: item?.prices?.normal_price || 0,
    quantity: item?.qty_ordered || 0,
  }))

  if (window && window.dataLayer) {
    window.dataLayer.push({ ecommerce: null }) // Clear the previous ecommerce object.
    window.dataLayer.push({
      event: 'view_cart',
      ecommerce: {
        currency: 'IDR',
        value: summary?.total,
        items: transformedItems || [],
      },
    })
    window.dataLayer.push(function () {
      this.reset()
    })
  }
}

export function ga4TrackAddToCart({ item = null }) {
  const { brand = '', sku = '', title = '', breadcrumbs = [] } = item
  const { special_price = 0, normal_price = 0 } = item.price

  const transformedItems = [
    {
      item_id: sku || '',
      item_name: title || '',
      affiliation: '',
      discount: normal_price - special_price,
      item_brand: brand || '',
      item_category: breadcrumbs?.[0] || '',
      item_category2: breadcrumbs?.[1] || '',
      item_category3: breadcrumbs?.[2] || '',
      item_category4: breadcrumbs?.[3] || '',
      item_list_id: '',
      item_list_name: breadcrumbs?.[3] || '',
      price: normal_price || 0,
      quantity: 1,
    },
  ]

  if (window && window.dataLayer) {
    window.dataLayer.push({
      event: 'add_to_cart',
      ...removeDataTier1,
      ecommerce: {
        ...removeDataTier2,
        currency: 'IDR',
        value: special_price || 0,
        items: transformedItems || [],
      },
    })
    window.dataLayer.push(function () {
      this.reset()
    })
  }
}

export function ga4TrackRemoveFromCart({
  item = null,
  allItems = [],
  type = 'single',
  summary = 0,
}) {
  let ecommerceValue = {}

  // it devined by 2 type because DataStruct on cart on header delete cart is different
  if (type === 'multiple') {
    const transformedItems = item?.map((item) => {
      const { sku, name, selling_price, prices, brand, breadcrumbs, quantity } =
        item
      return {
        item_id: sku,
        item_name: name,
        affiliation: '',
        discount: prices - selling_price,
        item_brand: brand,
        item_category: breadcrumbs?.[0] || '',
        item_category2: breadcrumbs?.[1] || '',
        item_category3: breadcrumbs?.[2] || '',
        item_category4: breadcrumbs?.[3] || '',
        item_list_id: '',
        item_list_name: breadcrumbs?.[3] || '',
        price: prices,
        quantity: quantity || 0,
      }
    })

    ecommerceValue = {
      ...removeDataTier2,
      currency: 'IDR',
      value: summary?.total || 0,
      items: transformedItems || [],
    }
  } else {
    const {
      brand = '',
      sku = '',
      name = '',
      breadcrumbs = [],
      productQty = 0,
    } = item || {}
    const { selling_price = 0, normal_price = 0 } = item?.prices || {}

    let item_qty = checkItemQty({ allItems, sku })
    if (isEmpty(allItems)) {
      item_qty = productQty
    }

    const item_cart_price = selling_price * item_qty

    const transformedItems = [
      {
        item_id: sku || '',
        item_name: name || '',
        affiliation: '',
        discount: normal_price - selling_price || '',
        item_brand: brand || '',
        item_category: breadcrumbs?.[0] || '',
        item_category2: breadcrumbs?.[1] || '',
        item_category3: breadcrumbs?.[2] || '',
        item_category4: breadcrumbs?.[3] || '',
        item_list_id: '',
        item_list_name: breadcrumbs?.[3] || '',
        price: normal_price || 0,
        quantity: item_qty || 1,
      },
    ]

    ecommerceValue = {
      ...removeDataTier2,
      currency: 'IDR',
      value: item_cart_price || selling_price,
      items: transformedItems || [],
    }
  }

  if (window && window.dataLayer) {
    window.dataLayer.push({
      event: 'remove_from_cart',
      ...removeDataTier1,
      ecommerce: {
        ...ecommerceValue,
      },
    })

    window.dataLayer.push(function () {
      this.reset()
    })
  }
}

export function ga4TrackAddToWishlist({ item = null, summary = {} }) {
  const checkItemType = isArray(item)
  const itemQty = 1 // item qty will be set always 1

  if (checkItemType) {
    const transformedItems = item?.map((item) => {
      const {
        sku = '',
        name = '',
        selling_price = 0,
        prices = 0,
        brand = '',
        breadcrumbs = '',
      } = item
      return {
        item_id: sku || '',
        item_name: name || '',
        affiliation: '', // Add affiliation value if needed
        discount: prices - selling_price,
        item_brand: brand,
        item_category: breadcrumbs?.[0] || '',
        item_category2: breadcrumbs?.[1] || '',
        item_category3: breadcrumbs?.[2] || '',
        item_category4: breadcrumbs?.[3] || '',
        item_list_id: '',
        item_list_name: breadcrumbs?.[3] || '',
        price: prices,
        quantity: itemQty,
      }
    })

    if (window && window.dataLayer) {
      window.dataLayer.push({
        event: 'add_to_wishlist',
        ...removeDataTier1,
        ecommerce: {
          ...removeDataTier2,
          currency: 'IDR',
          value: summary?.total,
          items: transformedItems || [],
        },
      })

      window.dataLayer.push(function () {
        this.reset()
      })
    }

    return
  }

  const { brand = '', sku = '', name = '', breadcrumbs = [] } = item || {}
  const { selling_price = 0, normal_price = 0 } = item?.prices || {}

  const transformedItems = [
    {
      item_id: sku || '',
      item_name: name || '',
      affiliation: '',
      discount: normal_price - selling_price,
      item_brand: brand,
      item_category: breadcrumbs?.[0] || '',
      item_category2: breadcrumbs?.[1] || '',
      item_category3: breadcrumbs?.[2] || '',
      item_category4: breadcrumbs?.[3] || '',
      item_list_id: '',
      item_list_name: breadcrumbs?.[3] || '',
      price: normal_price,
      quantity: itemQty,
    },
  ]

  if (window && window.dataLayer) {
    window.dataLayer.push({
      event: 'add_to_wishlist',
      ...removeDataTier1,
      ecommerce: {
        ...removeDataTier2,
        currency: 'IDR',
        value: selling_price * itemQty,
        items: transformedItems || [],
      },
    })

    window.dataLayer.push(function () {
      this.reset()
    })
  }
}

export function gaTrackMultipleEvent({ from = 'wishlist', itemsId = [] }) {
  if (itemsId.length === 0) {
    return
  }

  const itemCart = itemsId?.map((el) => {
    // Set default to undefined, as undefined values will remove attributes from the object

    return {
      sku: el?.sku || undefined,
      name: el?.name || undefined,
      prices: el?.prices?.normal_price || undefined,
      selling_price: el?.prices?.selling_price || undefined,
      brand: el?.brand || undefined,
      quantity: el?.qty_ordered || undefined,
      breadcrumbs: el?.breadcrumbs ? el?.breadcrumbs : undefined,
    }
  })

  let totalValue = 0

  itemCart?.forEach((item) => {
    const itemQty =
      from === 'wishlist' ? 1 : item?.quantity || item?.qty_ordered || 1
    totalValue += item?.selling_price * itemQty
  })

  switch (from) {
    case 'wishlist':
      ga4TrackAddToWishlist({
        item: itemCart,
        summary: {
          total: totalValue || 0,
        },
      })
      break
    case 'cart':
      ga4TrackRemoveFromCart({
        item: itemCart,
        summary: {
          total: totalValue || 0,
        },
        type: 'multiple',
      })
      break
    default:
      break
  }
}
