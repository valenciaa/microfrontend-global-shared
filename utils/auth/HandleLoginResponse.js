import Cookies from 'js-cookie'
import localforage from 'localforage'
import getFirstAndLastName from '../GetFirstAndLastName'
import { setLoginInfo } from '../SetLoginInfo'
import { moengageLogin } from '../MoengageWrapper'
import { mixpanelLoginKlk } from '../MixpanelWrapper'
import { getRupaUID } from '../GetRupaUID'
import { accessCustomerDatalayer, pageDataLayer } from '../DataLayerGoogle'

export async function HandleLoginResponse(
  accessToken,
  userData,
  b2bcData,
  callback,
  router,
  setAccessToken,
  setIsSuccess,
  socialMedia, // ? Google || undefined
) {
  localforage.removeItem('userInfo')
  localforage.removeItem('identifier')

  if (setAccessToken) {
    setAccessToken(accessToken)
  }
  await setLoginInfo(accessToken, userData, b2bcData)
  if (setIsSuccess) {
    setIsSuccess(true)
  }

  const splitName = getFirstAndLastName(userData?.name)
  const firstName = splitName?.firstName
  const lastName = splitName?.lastName
  const isFullActive =
    userData?.registration_status &&
    userData?.registration_status.toLowerCase() === 'full'

  moengageLogin(
    userData.customer_id,
    firstName,
    lastName,
    userData.email,
    userData.phone,
    userData.gender,
    userData.birthday,
    isFullActive,
    userData?.member_id,
    userData?.registration_status,
  )

  if (Cookies.get('sessionrupaUID') !== '') {
    Cookies.remove('sessionrupaUID')
    getRupaUID()
  }

  mixpanelLoginKlk(userData, undefined, socialMedia)

  // ga4 tracker login
  pageDataLayer(userData?.customer_id)
  accessCustomerDatalayer('login', socialMedia ? 'google' : 'manual')

  // ? force merging adter login
  if (userData?.need_force_merging) {
    await localforage.setItem(
      'isNeedForceMerging',
      userData?.need_force_merging,
    )
  }

  if (!userData?.is_phone_verified) {
    localforage.setItem('otpEmail', userData.email)
    localforage.setItem('otpPhone', userData.phone)

    if (!userData?.is_email_verified) {
      window?.sessionStorage?.setItem('last_verify_status', 'none')
    } else {
      window?.sessionStorage?.setItem('last_verify_status', 'email')
    }

    router.push({
      pathname: '/auth/otp-verification',
      query: { action: 'verify-phone', is_activate: false },
    })
  } else if (!userData?.is_email_verified) {
    localforage.setItem('otpEmail', userData.email)
    window?.sessionStorage?.setItem('last_verify_status', 'phone')
    router.push({
      pathname: '/auth/otp-verification',
      query: { action: 'verify-email', is_activate: false },
    })
  } else {
    if (callback) {
      callback({ isNeedForceMerging: userData?.need_force_merging })
    }
  }
}
