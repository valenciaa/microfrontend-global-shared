import localforage from 'localforage'

export async function SetInputType(
  inputType,
  setIsPhonelDisabled,
  setIsEmailDisabled,
) {
  try {
    const allowedInputType = ['email', 'phone', 'both']

    if (!allowedInputType.includes(inputType)) {
      return
    }

    const klkIsDisabled = {
      isEmailDisabled: false,
      isPhoneDisabled: false,
    }
    if (inputType) {
      switch (inputType) {
        case 'email':
          klkIsDisabled.isEmailDisabled = true
          if (setIsEmailDisabled) {
            setIsEmailDisabled(true)
          }
          break
        case 'phone':
          klkIsDisabled.isPhoneDisabled = true
          if (setIsPhonelDisabled) {
            setIsPhonelDisabled(true)
          }
          break
        case 'both':
          klkIsDisabled.isEmailDisabled = true
          klkIsDisabled.isPhoneDisabled = true
          if (setIsPhonelDisabled && setIsEmailDisabled) {
            setIsPhonelDisabled(true)
            setIsEmailDisabled(true)
          }
          break
      }
    }
    await localforage.setItem('KLK_isDisabled', klkIsDisabled)
  } catch (err) {
    return err
  }
}
