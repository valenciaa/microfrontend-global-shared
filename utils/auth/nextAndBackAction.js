import localforage from 'localforage'
import GetParam from '../GetParam'

export const setTNCNextComponent = async (
  isActivate,
  isPartialRegister,
  isLoginLegacy,
  isActivatePartner,
) => {
  const activateMemberToken = localforage.getItem('activateMemberToken')
  const bpEmail = sessionStorage.getItem('bp_email')

  if (isActivate || isPartialRegister) {
    if (activateMemberToken && bpEmail) {
      // this validations is for bypass password-form case google sign in
      return 'profile-form-1'
    } else {
      return 'password-form'
    }
  }

  if (isLoginLegacy) {
    return 'password-form'
  } else if (isActivatePartner) {
    return 'change-pin-form'
  } else {
    return 'profile-form-1'
  }
}

export const setProfileForm1BackComponent = async (
  isActivate,
  isPartialRegister,
  isLoginLegacy,
  isActivatePartner,
  allowedSocials = [],
) => {
  const activateMemberToken = localforage.getItem('activateMemberToken')
  const bpEmail = sessionStorage.getItem('bp_email')
  const querySocial = GetParam('social')

  if (isActivate || isPartialRegister) {
    if (
      activateMemberToken &&
      bpEmail &&
      allowedSocials.includes(querySocial)
    ) {
      // this validations is for case google sign in
      return 'tnc'
    } else {
      return 'password-form'
    }
  }

  if (isLoginLegacy) {
    return 'password-form'
  } else if (isActivatePartner) {
    return 'change-pin-form'
  } else {
    return 'tnc'
  }
}
