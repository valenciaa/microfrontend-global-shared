const months = [
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember',
]

export default function FormatDate(date) {
  const formattedDate = new Date(date)
  return `${formattedDate.getDate()} ${
    months[formattedDate.getMonth()]
  } ${formattedDate.getFullYear()}`
}

export const FormatDateDiff = (date) => {
  const expireDate = new Date(date)
  const dateDiffInSeconds = (expireDate.getTime() - new Date().getTime()) / 1000
  const dateDiffInDays = Math.ceil(dateDiffInSeconds / 3600 / 24)
  const dateDiffInHours = Math.ceil(dateDiffInSeconds / 3600)

  let expire = ''
  const isLessThan1Hour = dateDiffInSeconds / 3600 < 1
  if (isLessThan1Hour) {
    const dateDiffInMinutes = dateDiffInSeconds / 60
    expire = '1 jam'
    const thresholds = [30, 20, 10, 5, 4, 3, 2, 1]
    for (let i = 0; i < thresholds.length; i++) {
      const threshold = thresholds[i]
      if (dateDiffInMinutes <= threshold) {
        expire = `${threshold} menit`
      } else {
        break
      }
    }
  } else if (dateDiffInDays <= 1) {
    expire = `${dateDiffInHours} jam`
  } else if (dateDiffInDays <= 3) {
    expire = `${dateDiffInDays} hari`
  } else {
    expire = FormatDate(expireDate)
  }

  return {
    day: dateDiffInDays,
    hour: dateDiffInHours,
    second: dateDiffInSeconds,
    formatted: expire,
  }
}
