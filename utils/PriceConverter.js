export default function PriceConverter(
  nominal,
  useCurrency = true,
  useKoin = false,
) {
  let currency = ''
  let koin = ''
  if (useCurrency) {
    currency = 'Rp '
  }

  if (useKoin) {
    koin = ' Koin'
  }

  const numberString = nominal?.toString()
  // const numberString = '12321321'
  const sisa = numberString?.length % 3
  let rupiah = numberString?.substr(0, sisa)
  const ribuan = numberString?.substr(sisa).match(/\d{3}/g)

  if (ribuan) {
    const separator = sisa ? '.' : ''
    rupiah += separator + ribuan?.join('.')
  }

  return `${currency}${rupiah}${koin}`
}
