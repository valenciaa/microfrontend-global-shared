import dayjs from 'dayjs'

const GetFooterTitle = (companyCode, pageFrom = '', data = {}) => {
  if (['', 'home', 'product-detail'].includes(pageFrom)) {
    return ''
  } else {
    switch (pageFrom) {
      case 'jual':
        if (data) {
          return {
            brand: `Daftar Harga ${data?.name} Terbaru ${dayjs().year()}`,
          }
        }
        break
      case 'catalog':
      case 'brand':
        if (data) {
          return { brand: `Daftar Harga ${data?.name}` }
        }
        break
      default:
        return ''
    }
  }
}

export default GetFooterTitle
