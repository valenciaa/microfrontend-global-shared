import isEmpty from 'lodash/isEmpty'
import dayjs from 'dayjs'

const checkValidSubmmissionValidation = (invoiceData) => {
  const isDcInvoice = invoiceData?.store_code?.indexOf('DC') > -1

  if (isDcInvoice) {
    if (isEmpty(invoiceData?.tugu_dc) && isEmpty(invoiceData?.refund_dc)) {
      return true
    }

    if (!isEmpty(invoiceData?.tugu_dc)) {
      return false
    }

    if (!isEmpty(invoiceData?.refund_dc)) {
      // Pengajuan berupa refund tetap boleh mengajukan komplain dengan alasan produk belum diterima seluruhnya
      const isHasRTDC = invoiceData?.refund_dc?.find(
        (refund) => refund?.submission_type !== 'refund',
      )

      if (!isEmpty(isHasRTDC)) {
        return false
      }

      return true
    }
  }

  if (isEmpty(invoiceData?.tugu) && isEmpty(invoiceData?.refund)) {
    return true
  }

  if (!isEmpty(invoiceData?.tugu)) {
    return false
  }

  if (!isEmpty(invoiceData?.refund)) {
    // Pengajuan berupa refund tetap boleh mengajukan komplain dengan alasan produk belum diterima seluruhnya
    const isHasRT = invoiceData?.refund?.find(
      (refund) => refund?.refund_type !== 'cancel_refund',
    )

    if (!isEmpty(isHasRT)) {
      return false
    }

    return true
  }
}

const checkValidNotReceivedComplainDate = (invoiceData) => {
  const shipmentData = invoiceData?.shipment
  const shipmentStatus = shipmentData?.shipment_status

  if (shipmentStatus !== 'received') {
    return false
  }

  const receivedDate = shipmentData?.delivered_date || shipmentData?.updated_at

  const thisDate = dayjs()
  const maxValidDate = dayjs(receivedDate).add(1, 'day')

  const thisDateUnix = dayjs(thisDate).unix()
  const maxValidDateUnix = dayjs(maxValidDate).unix()

  const isValidNotReceivedComplain = maxValidDateUnix >= thisDateUnix

  if (isValidNotReceivedComplain) {
    return checkValidSubmmissionValidation(invoiceData)
  }

  return isValidNotReceivedComplain
}

const handleComplainNameForMixpanel = (complainId) => {
  if (complainId === '1') {
    return 'Produk Rusak/Cacat/Tidak Sesuai'
  }

  if (complainId === '2') {
    return 'Jumlah Produk Kurang/Part Tidak Lengkap'
  }

  if (complainId === '3') {
    return 'Produk Belum Diterima Seluruhnya'
  }

  return 'Alasan Pribadi'
}

const UtilGetStatusDisplayData = (status) => {
  if (status === 'rejected') {
    return {
      textColor: 'tw-text-red-60',
      bgColor: 'tw-bg-red-20',
      wording: 'Pengajuan Dibatalkan',
    }
  }

  if (['generated', 'refunded', 'received'].includes(status)) {
    return {
      textColor: 'tw-text-green-60',
      bgColor: 'tw-bg-green-20',
      wording: 'Pengembalian Berhasil',
    }
  }

  return {
    textColor: 'tw-text-yellow-60',
    bgColor: 'tw-bg-yellow-20',
    wording: 'Sedang Diproses',
  }
}

export {
  checkValidNotReceivedComplainDate,
  handleComplainNameForMixpanel,
  UtilGetStatusDisplayData,
}
