import * as yup from 'yup'

const AddressAddAndEditValidation = yup.object({
  address_name: yup
    .string()
    .required('Nama alamat wajib diisi')
    .max(35, 'Nama alamat maksimal 35 karakter'),
  first_name: yup
    .string()
    .required('Nama wajib diisi')
    .matches(/^[a-zA-Z ]*$/, 'Nama tidak valid')
    .max(60, 'Nama maksimal 60 karakter'),
  phone: yup
    .string()
    .required('Nomor telepon wajib diisi')
    .matches(/^\d+$/, 'Nomor telepon wajib angka')
    .matches(/^8/, 'Nomor telepon tidak valid')
    .min(9, 'Nomor telepon minimal 9 digit')
    .max(12, 'Nomor telepon maksimal 12 digit'),
  full_address: yup
    .string()
    .required('Alamat lengkap wajib diisi')
    .max(350, 'Alamat lengkap maksimal 350 karakter'),
  province: yup.object({
    province_id: yup.string().required(),
    province_name: yup.string().required('Provinsi wajib dipilih'),
  }),
  city: yup.object({
    city_id: yup.string().required(),
    city_name: yup.string().required('Kota wajib dipilih'),
  }),
  kecamatan: yup.object({
    kecamatan_id: yup.string().required(),
    kecamatan_name: yup.string().required('Kecamatan wajib dipilih'),
  }),
  kelurahan: yup.object({
    kelurahan_id: yup.string().required(),
    kelurahan_name: yup.string().required('Kelurahan wajib dipilih'),
    post_code: yup
      .string()
      .required('Kode pos wajib diisi')
      .matches(/^\d+$/, 'Kode pos wajib angka'),
  }),
  geolocation: yup.string(),
})

export { AddressAddAndEditValidation }
