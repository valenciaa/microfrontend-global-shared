import * as yup from 'yup'
const phoneRegex = /(^(08))\d{8,12}$/
const wordingUseNumber = 'Please use number only'
const wordingPostCode = 'Post code needs to be excatly 5 digits'
const numberRegex = /^\d*$/

const showErrorsDefault = (field, valueLen = 0, min = 0) => {
  if (valueLen === 0) {
    return `${field} diperlukan`
  } else if (valueLen > 0 && valueLen < min) {
    return `${field} minimal ${min} karakter`
  } else {
    return ''
  }
}

const registerFormB2bSchema = yup
  .object()
  .shape({
    customerProfile: yup.object().shape({
      companyName: yup
        .string()
        .required(() => showErrorsDefault('Nama Perusahaan')),
      companyGroup: yup
        .string()
        .required(() => showErrorsDefault('Grup Perusahaan')),
      address: yup.string().required(() => showErrorsDefault('Alamat')),
      city: yup
        .number()
        .typeError(() => showErrorsDefault('Kota'))
        .required(() => showErrorsDefault('Kota')),
      postCode: yup
        .string()
        .required(() => showErrorsDefault('Kode Pos'))
        .min(5, 'Minimal 5 digit')
        .max(5, 'Maksimal 5 digit')
        .matches(
          numberRegex,
          'Format Kode Pos tidak valid, mohon hanya gunakan angka',
        ),
      mobilePhone: yup
        .string()
        .required(() => showErrorsDefault('Nomor Handphone'))
        .matches(
          phoneRegex,
          'Format nomor handphone tidak valid, gunakan 08xxxx',
        ),
      homePhone: yup
        .string()
        .required(() => showErrorsDefault('Telepon'))
        .matches(
          numberRegex,
          'Format nomor telepon rumah tidak valid, mohon hanya gunakan angka',
        ),
      taxNumberType: yup
        .string()
        .required(() => showErrorsDefault('Tipe Wajib Pajak')),
      nitkuNo: yup.string().when('taxNumberType', (taxNumberType, schema) => {
        if (taxNumberType === 'Z2') {
          return schema
            .required(() => showErrorsDefault('Nomor NITKU (22 Digit)'))
            .matches(
              numberRegex,
              'Format NITKU tidak valid, mohon hanya gunakan angka',
            )
            .min(22, 'Minimal 22 digit')
            .max(22, 'Maksimal 22 digit')
        }
        return schema
      }),
      npwpNumber: yup
        .string()
        .required(() => showErrorsDefault('Nomor NPWP (15 Digit)'))
        .min(15, 'Minimal 15 digit')
        .max(15, 'Maksimal 15 digit')
        .matches(
          numberRegex,
          'Format nomor NPWP tidak valid, mohon hanya gunakan angka',
        ),
      npwpNumber2: yup
        .string()
        .required(() => showErrorsDefault('Nomor NPWP (16 Digit)'))
        .min(16, 'Minimal 16 digit')
        .max(16, 'Maksimal 16 digit')
        .matches(
          numberRegex,
          'Format nomor NPWP tidak valid, mohon hanya gunakan angka',
        ),
      isNpwpUsed: yup.boolean().oneOf([false], 'Nomor NPWP sudah digunakan'),
      ktpNo: yup
        .string()
        .required(() => showErrorsDefault('Nomor KTP'))
        .matches(
          numberRegex,
          'Format nomor NPWP tidak valid, mohon hanya gunakan angka',
        )
        .min(10, 'minimal 10 digit')
        .max(16, 'Maksimal 16 digit'),
      npwpFullname: yup
        .string()
        .required(() => showErrorsDefault('Nama Lengkap NPWP')),
      typeOfPkp: yup.string().required(() => showErrorsDefault('Tipe PKP')),
      typeOfPkpOtherText: yup
        .string()
        .when('typeOfPkp', (typeOfPkp, schema) => {
          if (typeOfPkp === 'other') {
            return schema.required(() => showErrorsDefault('Tipe PKP lain'))
          }
          return schema
        }),
      npwp: yup.string().required(() => showErrorsDefault('Dokumen NPWP')),
      website: yup
        .string()
        .url('Format website tidak valid. Cth: https://ruparupa.com')
        .nullable(),
    }),
    billingInformation: yup.object().shape({
      address: yup
        .string()
        .required(() => showErrorsDefault('Alamat Penagihan')),
      city: yup
        .number()
        .typeError(() => showErrorsDefault('Kota'))
        .required(() => showErrorsDefault('Kota')),
      postCode: yup
        .number()
        .typeError(wordingUseNumber)
        .test(
          'len',
          wordingPostCode,
          (val) => val && val.toString().length === 5,
        )
        .required(() =>
          showErrorsDefault('Billing address post code/zip code'),
        ),
      mobilePhone: yup
        .string()
        .required(() => showErrorsDefault('Nomor Handphone'))
        .matches(
          phoneRegex,
          'Format nomor handphone tidak valid, gunakan 08xxxx',
        ),
      email: yup
        .string()
        .email('Format email tidak valid')
        .required(() => showErrorsDefault('Email')),
      documentForInvoicing: yup
        .object()
        .shape({
          originalInvoice: yup.boolean(),
          receipt: yup.boolean(),
          fakturPajak: yup.boolean(),
          deliveryNote: yup.boolean(),
          bast: yup.boolean(),
          wetStamp: yup.boolean(),
          grn: yup.boolean(),
          other: yup.boolean(),
          otherText: yup.string().when('other', (other, schema) => {
            if (other) {
              return schema.required(() =>
                showErrorsDefault('Dokumen lain diperlukan'),
              )
            }
          }),
          fakturPajakType: yup
            .string()
            .when('fakturPajak', (fakturPajak, schema) => {
              if (fakturPajak) {
                return schema.required(() =>
                  showErrorsDefault('Faktur pajak lain diperlukan'),
                )
              }
            }),
        })
        .test({
          name: 'one-true',
          message: 'Mohon pilih salah satu tipe dokumen untuk faktur',
          test: (val) => {
            const checked = Object.keys(val).filter((key) => {
              return (
                val[key] === true &&
                !['otherText', 'fakturPajakType'].includes(key)
              )
            })

            return checked.length
          },
        }),
      tukarFakturSchedule: yup
        .string()
        .typeError(() => showErrorsDefault('Jadwal Tukar Faktur'))
        .max(10, 'Maksimal 10 karakter'),
      lastInvoiceReceiveDate: yup
        .string()
        .typeError(() => showErrorsDefault('Jadwal Tukar Faktur'))
        .max(10, 'Maksimal 10 karakter'),
      paymentSchedule: yup
        .string()
        .typeError(() => showErrorsDefault('Jadwal Tukar Faktur'))
        .max(10, 'Maksimal 10 karakter'),
      termOfPayment: yup
        .string()
        .required(() => showErrorsDefault('Term Of Payment')),
      documentForDelivery: yup
        .object()
        .shape({
          originalDo: yup.boolean(),
          other: yup.boolean(),
          otherText: yup.string().when('other', (other, schema) => {
            if (other) {
              return schema.required(() =>
                showErrorsDefault('Dokumen untuk Pengiriman lain diperlukan'),
              )
            }
          }),
        })
        .test({
          name: 'one-true',
          message: 'Mohon pilih salah satu tipe Dokumen untuk Pengiriman',
          test: (val) => {
            const checked = Object.keys(val).filter((key) => {
              return val[key] === true && !['otherText'].includes(key)
            })

            return checked.length
          },
        }),
      address1: yup
        .string()
        .required(() => showErrorsDefault('Alamat Pengiriman (1)')),
      city1: yup
        .number()
        .typeError(() => showErrorsDefault('Kota'))
        .required(() => showErrorsDefault('Kota')),
      postCode1: yup
        .string()
        .required(() => showErrorsDefault('Kode Pos'))
        .min(5, 'Minimal 5 digit')
        .max(5, 'Maksimal 5 digit')
        .matches(
          numberRegex,
          'Format Kode Pos tidak valid, mohon hanya gunakan angka',
        ),
      address2: yup.string(),
      city2: yup.number().typeError(() => showErrorsDefault('Kota')),
      postCode2: yup.string().when({
        is: (val) => val !== '',
        then: yup
          .string()
          .min(5, 'Minimal 5 digit')
          .max(5, 'Maksimal 5 digit')
          .matches(
            numberRegex,
            'Format Kode Pos tidak valid, mohon hanya gunakan angka',
          ),
        otherwise: yup.string(),
      }),
    }),
    companyInformation: yup.object().shape({
      customerType: yup
        .object()
        .shape({
          endUser: yup.boolean(),
          supplier: yup.boolean(),
          agent: yup.boolean(),
          distributor: yup.boolean(),
          retail: yup.boolean(),
          goverment: yup.boolean(),
          other: yup.boolean(),
          otherText: yup.string().when('other', (other, schema) => {
            if (other) {
              return schema.required(() =>
                showErrorsDefault('Tipe customer lain diperlukan'),
              )
            }
          }),
        })
        .test({
          name: 'one-true',
          message: 'Mohon pilih salah satu tipe customer',
          test: (val) => {
            const checked = Object.keys(val).filter((key) => {
              return val[key] === true
            })

            return checked.length
          },
        }),
      lineOfBusiness: yup
        .string()
        .required(() => showErrorsDefault('Bidang Usaha')),
      mainProduct: yup
        .string()
        .required(() => showErrorsDefault('Produk Utama')),
      yearOfEstablished: yup
        .string()
        .required(() => showErrorsDefault('Tahun Didirikan'))
        .matches(numberRegex, 'Tahun Didirikan harus berupa angka')
        .min(4, 'Tahun Didirikan tidak valid')
        .max(4, 'Tahun Didirikan tidak valid'),
      numberOfEmployee: yup
        .string()
        .required(() => showErrorsDefault('Jumlah Karyawan'))
        .matches(numberRegex, 'Jumlah Karyawan harus berupa angka'),
      buildingOwnership: yup
        .string()
        .required(() => showErrorsDefault('Kepemilikan Bangunan')),
      revenuePerYear: yup
        .string()
        .required(() => showErrorsDefault('Pendapatan per tahun (IDR)')),
      annualPurchasedEstimation: yup
        .string()
        .required(() => showErrorsDefault('Estimasi Pembelian Tahunan (IDR)')),
      everWithKawanLama: yup
        .string()
        .required(() => showErrorsDefault('Field ini diperlukan')),
      department1: yup.string().required(() => showErrorsDefault('Departemen')),
      firstName1: yup.string().required(() => showErrorsDefault('Nama Depan')),
      lastName1: yup
        .string()
        .required(() => showErrorsDefault('Nama Belakang')),
      directPhoneShipping1: yup
        .string()
        .required(() => showErrorsDefault('Telepon & Ext. Langsung')),
      mobilePhone1: yup.string().required(() => showErrorsDefault('Email')),
      email1: yup
        .string()
        .email('Format email tidak valid')
        .required(() => showErrorsDefault('Nomor Handphone')),
      department2: yup.string().required(() => showErrorsDefault('Departemen')),
      firstName2: yup.string().required(() => showErrorsDefault('Nama Depan')),
      lastName2: yup
        .string()
        .required(() => showErrorsDefault('Nama Belakang')),
      directPhoneShipping2: yup
        .string()
        .required(() => showErrorsDefault('Telepon & Ext. Langsung')),
      mobilePhone2: yup
        .string()
        .required(() => showErrorsDefault('Nomor Handphone')),
      email2: yup
        .string()
        .email('Format email tidak valid')
        .required(() => showErrorsDefault('Email')),
    }),
  })
  .required()

export { registerFormB2bSchema }
