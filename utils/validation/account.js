// HARUS CEK JANGAN LUPA KARENA DI DESKTOP JUGA ADA FILE INI TAPI BUKAN DI SHARED

import { string, object, ref, boolean } from 'yup'

const phoneNumberPattern = /(^(08))\d{8,12}$/
const passwordPattern = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/

const registrationSchema = object().shape({
  firstName: string().required('Nama lengkap tidak boleh kosong'),
  email: string()
    .email('Email tidak sesuai format')
    .required('Email tidak boleh kosong'),
  password: string()
    .matches(
      passwordPattern,
      'Kata sandi hanya boleh menggunakan kombinasi huruf (a-z) dan angka (0-9)',
    )
    .min(5, 'Panjang kata sandi minimal 5 karakter'),
  confirmPassword: string()
    .oneOf([ref('password'), null], 'Kata sandi tidak sesuai')
    .required('Kata sandi tidak boleh kosong'),
  phone: string()
    .matches(phoneNumberPattern, 'Nomor telepon tidak valid, hanya boleh angka')
    .min(5, 'Panjang nomor telepon minimal 5 karakter'),
  terms: boolean()
    .required('Anda harus menyetujui syarat dan ketentuan')
    .oneOf([true], 'Anda harus menyetujui syarat dan ketentuan'),
})

const loginSchema = object().shape({
  email: string()
    // .email("Enter a valid email")
    .required('Email atau nomor telepon tidak boleh kosong')
    .test(
      'test-email-phone',
      'Email atau nomor telepon tidak sesuai format',
      function (value) {
        /* eslint-disable no-useless-escape */
        const emailRegex =
          /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
        const phoneRegex = /(^(08|62|\+62))\d{8,12}$/ // Change this regex based on requirement
        const isValidEmail = emailRegex.test(value)
        const isValidPhone = phoneRegex.test(value)
        if (!isValidEmail && !isValidPhone) {
          return false
        }
        return true
      },
    ),
  password: string()
    .required('Kata sandi tidak boleh kosong')
    .min(5, 'Panjang kata sandi minimal 5 karakter'),
})

const downloadMobileAppsLinkSchema = object().shape({
  recipient: string()
    .matches(phoneNumberPattern, 'Nomor telepon tidak valid')
    .min(5, 'Panjang nomor telepon minimal 5 karakter'),
})

export { downloadMobileAppsLinkSchema, registrationSchema, loginSchema }
