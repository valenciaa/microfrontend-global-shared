import * as yup from 'yup'

const CcAddValidation = yup.object({
  card_cvv: yup
    .string()
    .required('CVV wajib diisi')
    .matches(/^\d+$/, 'CVV harus berupa angka')
    .min(3, 'CVV minimal 3 digit')
    .max(4, 'CVV maksimal 4 digit'),
  cc_name: yup
    .string()
    .required('Nama Lengkap wajib diisi')
    .min(3, 'Nama Lengkap minimal 3 karakter')
    .max(135, 'Nama Lengkap maksimal 135 karakter'),
  card_number: yup
    .string()
    .required('Nomor Kartu wajib diisi')
    .matches(/^\d+$/, 'Nomor Kartu harus berupa angka')
    .min(15, 'Nomor Kartu minimal 15 digit')
    .max(16, 'Nomor Kartu maksimal 16 digit'),
  expiry: yup
    .string()
    .required('Tanggal Kadaluwarsa wajib diisi')
    .matches(/^\d+$/, 'Tanggal Kadaluwarsa harus berupa angka')
    .test(
      'len',
      'Tanggal Kadaluwarsa harus dilengkapi',
      (val) => val?.length === 4,
    )
    .test(
      'month',
      'Bulan berlaku antara 01-12',
      (val) =>
        !!(
          val &&
          val?.length >= 1 &&
          parseInt(val?.substring(0, 2)) >= 1 &&
          parseInt(val?.substring(0, 2)) <= 12
        ),
    )
    .test(
      'year',
      'Tahun berlaku tidak kurang dari ' + new Date().getFullYear(),
      (val) =>
        !!(
          val &&
          val?.length === 4 &&
          parseInt(val?.substring(2, 4)) >=
            parseInt(new Date().getFullYear().toString().substring(2, 4))
        ),
    ),
})

const GopayInitValidation = yup.object({
  identifier: yup
    .string()
    .required('Nomor telepon wajib diisi')
    .matches(/^\d+$/, 'Nomor telepon harus berupa angka')
    .matches(/^8/, 'Nomor telepon tidak valid')
    .max(15, 'Nomor telepon maksimal 15 digit'),
})

export { CcAddValidation, GopayInitValidation }
