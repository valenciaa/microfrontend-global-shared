import { string, object, number, ref } from 'yup'

const phoneNumberPattern = /(^(08))\d{8,12}$/
const passwordPattern = /^(?=.*?[A-Z])(?=.*?[a-z]).{8,}$/
const pinRegexSameNumber = /\b(\d)\1+\b/
const pinRegexIncrementNumber = /^(012345|123456|234567|345678|456789|567890)$/
const pinRegexDecrementNumber = /^(098765|987654|876543|765432|654321|543210)$/

const emailSchema = string()
  .email('Email tidak sesuai format')
  .required('Email tidak boleh kosong')

const phoneSchema = string()
  .matches(
    phoneNumberPattern,
    'Pastikan nomor handphone yang kamu masukkan benar',
  )
  .min(5, 'Panjang nomor telepon minimal 5 karakter')

const passwordSchema = object().shape(
  {
    currentPassword: string().when('currentPassword', (val) => {
      if (val?.length > 0) {
        return string().min(8, 'Panjang kata sandi minimal 8 karakter')
      } else {
        return string().notRequired()
      }
    }),
    password: string()
      .required('Kata sandi tidak boleh kosong')
      .min(8, 'Panjang kata sandi minimal 8 karakter')
      .matches(
        passwordPattern,
        'Kata sandi harus terdiri dari minimal 8 karakter yang mengandung huruf kapital dan huruf kecil',
      ),
    confirmPassword: string()
      .oneOf(
        [ref('password'), null],
        'Konfirmasi kata sandi tidak sesuai. Silakan ulangi kembali',
      )
      .required('Konfirmasi kata sandi tidak boleh kosong')
      .matches(
        passwordPattern,
        'Konfirmasi Kata sandi harus terdiri dari minimal 8 karakter yang mengandung huruf kapital dan huruf kecil',
      ),
  },
  [
    ['currentPassword', 'currentPassword'], // cyclic dependency
  ],
)

const pinSchema = object().shape(
  {
    currentPin: string().when('currentPin', (val) => {
      if (val?.length > 0) {
        return string().min(6, 'Panjang pin minimal 6 karakter')
      } else {
        return string().notRequired()
      }
    }),
    pin: string()
      .length(6, 'Panjang pin minimal 6 karakter')
      .required('PIN tidak boleh kosong')
      .test(
        'same number',
        'Hindari menggunakan angka yang berurutan dan berulang ',
        (val) => !val.match(pinRegexSameNumber),
      )
      .test(
        'increment number',
        'Hindari menggunakan angka yang berurutan dan berulang',
        (val) => !val.match(pinRegexIncrementNumber),
      )
      .test(
        'decrement number',
        'Hindari menggunakan angka yang berurutan dan berulang',
        (val) => !val.match(pinRegexDecrementNumber),
      ),
    confirmPin: string()
      .oneOf([ref('pin'), null], 'PIN tidak sesuai')
      .required('Konfirmasi PIN tidak boleh kosong')
      .test(
        'same number',
        'Hindari menggunakan angka yang berurutan dan berulang ',
        (val) => !val.match(pinRegexSameNumber),
      )
      .test(
        'increment number',
        'Hindari menggunakan angka yang berurutan dan berulang',
        (val) => !val.match(pinRegexIncrementNumber),
      )
      .test(
        'decrement number',
        'Hindari menggunakan angka yang berurutan dan berulang',
        (val) => !val.match(pinRegexDecrementNumber),
      ),
  },
  [
    ['currentPin', 'currentPin'], // cyclic dependency
  ],
)

const newPinSchema = object().shape({
  pin: string()
    .length(6, 'Panjang pin minimal 6 karakter')
    .required('PIN tidak boleh kosong')
    .test(
      'same number',
      'Hindari menggunakan angka yang berurutan dan berulang ',
      (val) => !val.match(pinRegexSameNumber),
    )
    .test(
      'increment number',
      'Hindari menggunakan angka yang berurutan dan berulang',
      (val) => !val.match(pinRegexIncrementNumber),
    )
    .test(
      'decrement number',
      'Hindari menggunakan angka yang berurutan dan berulang',
      (val) => !val.match(pinRegexDecrementNumber),
    ),
})

const editMainFormSchema = object().shape({
  gender: string().required('Mohon isi terlebih dahulu untuk menyimpan'),
  birthday: string().required('Mohon isi terlebih dahulu untuk menyimpan'),
  districtId: number()
    .required('Mohon isi terlebih dahulu untuk menyimpan')
    .moreThan(0, 'Mohon isi terlebih dahulu untuk menyimpan'),
})

const phoneSchemaV2 = (minLength = 5, maxLength = 12) => {
  const phoneNumberPatternV2 = /(^(?!0))\d*$$/
  return string()
    .matches(
      phoneNumberPatternV2,
      'Pastikan nomor handphone yang kamu masukkan benar',
    )
    .min(minLength, `Panjang nomor telepon minimal ${minLength} karakter`)
    .max(maxLength, `Panjang nomor telepon maksimal ${maxLength} karakter`)
}

export {
  emailSchema,
  phoneSchema,
  passwordSchema,
  pinSchema,
  newPinSchema,
  editMainFormSchema,
  phoneSchemaV2,
}
