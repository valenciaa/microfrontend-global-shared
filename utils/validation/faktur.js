import * as yup from 'yup'

const AddFakturPajakCompanyDataValidation = yup.object({
  company_address: yup.string().when('is_npwp_address', {
    is: 'no',
    then: yup.string().required('Alamat perusahaan wajib diisi'),
  }),
  company_name: yup.string().required('Nama perusahaan wajib diisi'),
  company_npwp: yup
    .string()
    .required('Nomor NPWP wajib diisi')
    .matches(/^\d+$/, 'Nomor NPWP wajib angka')
    .test(
      'len',
      'Nomor NPWP minimal 15 digit',
      (val) => val?.length === 15 || val?.length === 16,
    ),
  is_npwp_address: yup.string().required(),
  npwp_image: yup.string().required('Foto NPWP wajib dipilih'),
  recipient_email: yup
    .string()
    .required('Email wajib diisi')
    .email('Email tidak valid'),
  tipe_wajib_pajak: yup.string().required('Tipe wajib pajak wajib dipilih'),
  nitku: yup.string().when('tipe_wajib_pajak', {
    is: 'Wajib Pajak Badan Cabang',
    then: yup
      .string()
      .required('NITKU wajib diisi')
      .matches(/^\d+$/, 'NITKU wajib angka')
      .test('len', 'NITKU wajib 22 digit', (val) => val?.length === 22),
  }),
})

export { AddFakturPajakCompanyDataValidation }
