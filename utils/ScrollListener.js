export const onScrollListener = (
  timer,
  elementId,
  posOnShow,
  posOnHide,
  isLenna = false,
) => {
  let el
  if (isLenna) {
    el = document
      ?.getElementById('lenna-webchat')
      ?.getElementsByTagName('img')[0]
  } else {
    el = document.getElementById(elementId)
  }
  if (el) {
    el.style.right = posOnShow
    if (timer !== null) {
      clearTimeout(timer)
    }
    return (timer = setTimeout(function () {
      el.style.right = posOnHide
    }, 500))
  }
}
