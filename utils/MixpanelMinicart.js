import isEmpty from 'lodash/isEmpty'
import GetParam from './GetParam'
import { mixpanelTrack } from './MixpanelWrapper'

import dayjs from 'dayjs'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'

dayjs.extend(isSameOrAfter)
dayjs.extend(isSameOrBefore)

const parseBreadcrumb = (breadcrumbs = [], index = 0) => {
  if (breadcrumbs && breadcrumbs.length && breadcrumbs[index]) {
    return breadcrumbs[index]
  }
  return 'None'
}

const parseColor = (attributes = []) => {
  return (
    attributes?.find((attribute) => attribute?.attribute_id === 195)
      ?.attribute_value || 'None'
  )
}

const parsePrice = (prices = {}) => {
  const normalPrice = prices?.normal_price ? parseInt(prices?.normal_price) : 0
  const specialPrice = prices?.special_price
    ? parseInt(prices?.special_price)
    : 0

  return specialPrice || normalPrice
}

export const parseIsFlashSale = (events) => {
  const flashSaleEvent =
    events?.find((event) => event?.url_key.includes('flash-sale')) || {}
  return (
    !isEmpty(flashSaleEvent) &&
    dayjs().isSameOrAfter(flashSaleEvent?.start_date, 'dd-mm-yy h:mm:ss') &&
    dayjs().isSameOrBefore(flashSaleEvent?.end_date, 'dd-mm-yy h:mm:ss')
  )
}

export const parseIsEventSale = (events) => {
  return events && events.length > 0
}

export const mixpanelTrackMinicartItem = (
  eventName,
  sku,
  name,
  prices,
  attributes,
  brand,
  maxQty = 0,
  label,
  events,
  breadcrumbs = [],
  qty = 1,
  message,
  additionalProperty = {},
  location = 'Cart',
) => {
  const itemPrice = prices?.special_price ? parseInt(prices?.special_price) : 0
  const sellingPrice = prices?.normal_price ? parseInt(prices?.normal_price) : 0
  const isItemCanInstalled = !!attributes?.find(
    (attribute) => attribute?.attribute_id === 396,
  )
  const itemColor = attributes?.find(
    (attribute) => attribute?.attribute_id === 195,
  )?.attribute_value

  const isFlashSale = parseIsFlashSale(events)

  const mixpanelObject = {
    'Item Name': name ? name?.toLowerCase() : 'None',
    'Item Category LV 1': parseBreadcrumb(breadcrumbs, 0),
    'Item Category LV 2': parseBreadcrumb(breadcrumbs, 1),
    'Item Category LV 3': parseBreadcrumb(breadcrumbs, 2),
    'Item Category LV 4': parseBreadcrumb(breadcrumbs, 3),
    'Item Price': itemPrice !== 0 ? itemPrice : sellingPrice,
    'Selling Price': sellingPrice,
    'Item Colour': itemColor || 'Mix',
    'Discount?': itemPrice !== 0,
    'Item Price Discount%':
      itemPrice !== 0 ? ((sellingPrice - itemPrice) / sellingPrice) * 100 : 0,
    'Item ID': sku || 'None',
    'Item Brand': brand || 'None',
    'Item Quantity': qty,
    'Stock Available?': maxQty > 0,
    'Ownfleet?': 'None',
    'Instant Delivery?': 'None',
    'Regular Delivery?': 'None',
    'Pickup?': 'None',
    'Promo?': itemPrice !== 0,
    'Product Label': label || 'None',
    'Product Label?': label !== '',
    'Supplier Alias': 'None', // ? Need to verify
    'Discount Amount':
      itemPrice !== 0 && sellingPrice !== 0 ? sellingPrice - itemPrice : 0,
    'SKU FlashSale': isFlashSale,
    'Bisa Dirakit?': isItemCanInstalled,
    'ITM Source': GetParam('itm_source') || 'None',
    'ITM Term': GetParam('itm_term') || 'None',
    'ITM Campaign': GetParam('itm_campaign') || 'None',
    'ITM Device': GetParam('itm_device') || 'None',
    'Location': location,
    ...additionalProperty,
  }

  if (message) {
    mixpanelObject.message = message
  }

  mixpanelTrack(eventName, mixpanelObject)
}

export const mixpanelTrackMultipleMinicartItems = (
  eventName,
  items = [],
  // itemsTotal = 0,
  message = '',
  additionalProperty = {},
) => {
  const itemsBrand = []
  const itemsCategoryLvl1 = []
  const itemsCategoryLvl2 = []
  const itemsCategoryLvl3 = []
  const itemsCategoryLvl4 = []
  const itemsColor = []
  const itemsId = []
  const itemsName = []
  const itemsPrice = []
  const itemsQty = []
  const isFlashSaleArr = []

  items.forEach((item) => {
    itemsBrand.push(item.brand || 'None')
    itemsCategoryLvl1.push(parseBreadcrumb(item.breadcrumbs, 0))
    itemsCategoryLvl2.push(parseBreadcrumb(item.breadcrumbs, 1))
    itemsCategoryLvl3.push(parseBreadcrumb(item.breadcrumbs, 2))
    itemsCategoryLvl4.push(parseBreadcrumb(item.breadcrumbs, 3))
    itemsColor.push(parseColor(item.attributes))
    itemsId.push(item.sku)
    itemsName.push(item.name)
    itemsPrice.push(parsePrice(item.prices))
    itemsQty.push(item.qty_ordered)
    isFlashSaleArr.push(parseIsFlashSale(item.marketing.events))
  })

  const mixpanelObject = {
    'Item Name': itemsName.length ? itemsName : 'None',
    'Item Category LV 1': itemsCategoryLvl1.length ? itemsCategoryLvl1 : 'None',
    'Item Category LV 2': itemsCategoryLvl2.length ? itemsCategoryLvl2 : 'None',
    'Item Category LV 3': itemsCategoryLvl3.length ? itemsCategoryLvl3 : 'None',
    'Item Category LV 4': itemsCategoryLvl4.length ? itemsCategoryLvl4 : 'None',
    'Item Price': itemsPrice.length ? itemsPrice : 'None',
    'Item Colour': itemsColor.length ? itemsColor : 'None',
    'Item ID': itemsId.length ? itemsId : 'None',
    'Item Brand': itemsBrand.length ? itemsBrand : 'None',
    'Item Quantity': itemsQty.length ? itemsQty : 'None',
    // 'Stock Available?': maxQty > 0,
    'Ownfleet?': 'None',
    'Instant Delivery?': 'None',
    'Regular Delivery?': 'None',
    'Pickup?': 'None',
    // 'Promo?': itemPrice !== 0,
    // 'Product Label': label || 'None',
    // 'Product Label?': label !== '',
    'SKU FlashSale': isFlashSaleArr,
    // 'Bisa Dirakit?': isItemCanInstalled,
    'ITM Source': GetParam('itm_source') || 'None',
    'ITM Term': GetParam('itm_term') || 'None',
    'ITM Campaign': GetParam('itm_campaign') || 'None',
    'ITM Device': GetParam('itm_device') || 'None',
    'Location': 'Cart',
    ...additionalProperty,
  }

  if (message) {
    mixpanelObject.message = message
  }

  mixpanelTrack(eventName, mixpanelObject)
}
