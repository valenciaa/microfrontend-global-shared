import React from 'react'
import { useHomepageContext } from '../../../../../src/homepage/context/HomepageContext'

export default function ReviewReasons({
  reasons,
  selectedReasons,
  handleSelectedReasons,
}) {
  const homepageContext = useHomepageContext()
  const homepageStyles = homepageContext?.homepageStyles
  return (
    <ul className={homepageStyles['modal-body__review-rating--pills']}>
      {reasons?.map((reason) => {
        let selected = false
        if (selectedReasons?.includes(reason?.review_tag_id)) {
          selected = true
        }

        return (
          <li
            key={reason?.review_tag_id}
            onClick={() =>
              handleSelectedReasons(parseInt(reason?.review_tag_id))
            }
            className={`cursor-pointer ${
              selected
                ? homepageStyles['modal-body__review-rating--pills-selected']
                : ''
            }`}
          >
            {reason?.tag_description}
          </li>
        )
      })}
    </ul>
  )
}
