import React from 'react'
import StarRatings from 'react-star-ratings'
import { starSvgIconPath } from '../../../utils/StarIconSvgPath'
import { useHomepageContext } from '../../../../../src/homepage/context/HomepageContext'

export default function ReviewCard({ product, rating, setRating }) {
  const homepageContext = useHomepageContext()
  const homepageStyles = homepageContext?.homepageStyles
  return (
    <>
      <div className={homepageStyles['review-rating__title']}>
        {product?.product_name?.toLowerCase()}
      </div>
      <div>Nilai Produk</div>
      <div className={homepageStyles['review-rating__star-container']}>
        <StarRatings
          rating={rating}
          starRatedColor='#FFB152'
          starHoverColor='#FFB152'
          starEmptyColor='white'
          changeRating={setRating}
          numberOfStars={5}
          name='rating'
          starDimension='15px'
          starSpacing='3px'
          svgIconViewBox='0 0 22 22'
          svgIconPath={starSvgIconPath}
        />
      </div>
    </>
  )
}
