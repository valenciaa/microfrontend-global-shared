import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'

import { isEmpty } from 'lodash'
import clsx from 'clsx'
import config from '../../../../../config'
import { useRequestProformaInvoice } from '../../../services/api/mutations/qoutation'
import TWLoadingModal from '../../../tw-components/modals/TWLoadingModal'
import TWBaseModal from '../../../tw-components/modals/TWBaseModal'
import TWButton from '../../../tw-components/buttons/TWButton'
import {
  RUPARUPA_BISNIS_EMAIL_ORDER,
  RUPARUPA_BISNIS_WA_DISPLAY,
} from '../../../tw-container/b2b-qoutation-v2/utils/b2bConstants'

export default function RequestProformaWrapper() {
  const router = useRouter()
  const [showModalFailed, setShowModalFailed] = useState(false)
  const [showModalSuccess, setShowModalSuccess] = useState(false)
  const [isAlreadyUsed, setIsAlreadyUsed] = useState(false)
  const { token } = router.query

  const {
    mutate: requestProformaInvoice,
    isLoading: requestProformaInvoiceLoading,
  } = useRequestProformaInvoice({
    onSuccess: (res) => {
      if (!res?.ok || !res?.data?.success) {
        return setShowModalFailed(true)
      }

      if (res?.data?.data?.is_already_requested) {
        return setIsAlreadyUsed(true)
      }

      return setShowModalSuccess(true)
    },
    onError: () => {
      setShowModalFailed(true)
    },
  })

  useEffect(() => {
    if (!router.isReady) {
      return
    }

    if (isEmpty(token)) {
      return handleRedirectHomepage()
    }

    requestProformaInvoice({ token })
  }, [router.isReady, token, requestProformaInvoice])

  const handleRedirectHomepage = () => {
    router.push(`${config.baseURL}/page/b2b`)
  }

  if (requestProformaInvoiceLoading) {
    return <TWLoadingModal />
  }

  const modalWidth =
    config.environment === 'mobile' ? 'tw-w-[343px]' : 'tw-w-[416px]'

  const isShowErrorModal = showModalFailed || isAlreadyUsed

  return (
    <div className='tw-mx-4'>
      {isShowErrorModal && (
        <TWBaseModal
          animationType='fade'
          customPadding={clsx('tw-py-8', modalWidth)}
          isShow={isShowErrorModal}
          modalType='middle-small'
        >
          <div className='tw-flex tw-flex-col tw-px-4'>
            <div className='tw-font-bold tw-text-xl'>
              Pengajuan Proforma Invoice Gagal
            </div>
            <p className='tw-py-4'>
              {isAlreadyUsed
                ? 'Permintaan proforma invoice sudah diajukan. Anda tidak dapat mengajukan permintaan baru.'
                : 'Pengajuan proforma invoice gagal karena token tidak valid.'}
            </p>
            <TWButton
              handleOnClick={handleRedirectHomepage}
              additionalClass='tw-rounded-md tw-mt-2'
            >
              <p>Kembali Ke Halaman Utama</p>
            </TWButton>
          </div>
        </TWBaseModal>
      )}
      {showModalSuccess && (
        <TWBaseModal
          animationType='fade'
          customPadding={clsx('tw-py-8', modalWidth)}
          isShow={showModalSuccess}
          modalType='middle-small'
          additionalModalFullClass='px-6'
        >
          <div className='tw-flex tw-flex-col tw-px-6'>
            <div className='tw-font-bold tw-text-xl'>
              Pengajuan Proforma Invoice
            </div>
            <div className='tw-my-4 tw-text-gray-600'>
              <p>
                Proforma invoice akan dikirimkan ke email Anda dalam waktu 1-2
                hari kerja.
              </p>
              <p>
                Jika terdapat pertanyaan lanjutan, Anda bisa menghubungi kami
                di:
              </p>
              <p>
                Whatsapp:
                <span className='tw-text-blue-50 tw-ml-2'>
                  {RUPARUPA_BISNIS_WA_DISPLAY}
                </span>
              </p>
              <p>
                Email:
                <span className='tw-text-blue-50 tw-ml-2'>
                  {RUPARUPA_BISNIS_EMAIL_ORDER}
                </span>
              </p>
            </div>
            <TWButton
              handleOnClick={handleRedirectHomepage}
              additionalClass='tw-rounded-md tw-mt-2'
            >
              <p>Kembali Ke Ruparupa Bisnis</p>
            </TWButton>
          </div>
        </TWBaseModal>
      )}
    </div>
  )
}
