import React from 'react'
import config from '../../../../config'
import InfoboxInfo from '../../../../public/static/icon/infobox-info-16.svg'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import Infobox from '../../layouts/atoms/Infobox'
import Modal from '../../layouts/templates/Modal'
import Button from '../../layouts/atoms/Button'

export default function DownloadFakturModal({ downloadFaktur }) {
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const unifyBaseStyles = useUnifyAppsContext().unifyBaseStyles
  const fakturStatus = detailTransactionContext?.fakturStatus?.[0]

  const downloadPajak = () => {
    return (
      <div className='container-card'>
        <div className='flex items-center margin-bottom-m'>
          <img
            src={config.assetsURL + 'icon/icon-pdffile.svg'}
            width={30}
            height={30}
            className='padding-right-s'
          />
          <span>
            {fakturStatus?.CompanyName} - {fakturStatus?.InvoiceNo}.pdf
          </span>
          {downloadFaktur?.url && (
            <Button
              type='primary'
              size='medium'
              handleOnClick={() =>
                window.open(downloadFaktur?.url || '', '_blank')
              }
              additionalClass='col margin-left-s'
            >
              <div className='flex justify-center items-center'>
                <img
                  src={config.assetsURL + 'icon/icon-download.svg'}
                  width={28}
                  height={28}
                  className='padding-right-xs'
                />
                <p>Download</p>
              </div>
            </Button>
          )}
        </div>
        <Infobox
          text='Faktur pajak tidak sesuai? '
          subtext='Klik di sini'
          theme='info'
          onClickSubText={
            fakturStatus?.Revisi !== 'yes'
              ? () => handleChangeState('contentPajak', 'revisionPajak')
              : null
          }
        >
          <div className='flex'>
            <InfoboxInfo className='color-blue-60' />
          </div>
        </Infobox>
        <div className='col-xs-12 text-left margin-left-s margin-top-s'>
          <p
            className={`${unifyBaseStyles['error-text']}`}
            style={{ display: fakturStatus?.Revisi === 'yes' ? '' : 'none' }}
          >
            Pengajuan revisi faktur pajak hanya dapat dilakukan sekali
          </p>
        </div>
      </div>
    )
  }
  return (
    <Modal
      modalType='center'
      dialogClass={unifyBaseStyles['modal-size-desktop']}
      backdropOnClose
      bodyElement={() => downloadPajak()}
      onClose={() => handleChangeState('contentPajak', null)}
      headerElement='Download Faktur Pajak'
      show={detailTransactionContext?.clickHeader}
      bodyClass='padding__horizontal--none margin-top-xs'
    />
  )
}
