import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { FakturPajakAddCompanyForm } from '../../modules/unifyapps/fakturPajak/FakturPajakAddCompanyForm'
import { useAddCompanyData } from '../../services/api/mutations/unifyapps'
import Button from '../../layouts/atoms/Button'
import Infobox from '../../layouts/atoms/Infobox'
import Modal from '../../layouts/templates/Modal'

export default function AddCompanyModal() {
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const unifyBaseStyles = useUnifyAppsContext().unifyBaseStyles
  const addCompany =
    detailTransactionContext?.contentPajak === 'addNewInformation'
  const addCompanySuccess =
    detailTransactionContext?.contentPajak === 'addCompanySuccess'
  const [errorResponse, setErrorResponse] = useState(null)

  const { mutate: addCompanyData, isLoading } = useAddCompanyData({
    enabled: false,
    onSuccess: (res) => {
      if (!res?.data?.error) {
        handleChangeState('contentPajak', 'addCompanySuccess')
      } else {
        setErrorResponse(res?.data?.message)
      }
    },
  })

  const { register, handleSubmit, setValue, watch } = useForm({
    mode: 'onChange',
    defaultValues: {
      company_name: '',
      company_npwp: '',
      recipient_email: '',
      is_npwp_address: '',
      company_address: '',
      npwp_image: '',
    },
  })

  const onSubmit = async (data) => {
    try {
      addCompanyData(data)
    } catch (e) {
      console.log('err')
    }
  }

  const addCompanyForm = () => {
    return (
      <div className='container-card'>
        <div className='ui-text-2 padding__horizontal--m padding-bottom-m color-grey-50 text-justify'>
          {addCompany && (
            <FakturPajakAddCompanyForm
              onSubmit={onSubmit}
              register={register}
              handleSubmit={handleSubmit}
              setValue={setValue}
              watch={watch}
              errorResponse={errorResponse}
              isFetching={isLoading}
            />
          )}
          {addCompanySuccess && (
            <div>
              <Infobox
                text='Data perusahaan sudah berhasil ditambahkan.'
                theme='info'
                className='margin-bottom-m'
                textClassName='margin-xs ui-text-3 color-grey-50'
              />
              <Button
                type='link-primary-transparent'
                size='medium'
                handleOnClick={() =>
                  handleChangeState('contentPajak', 'companyInformationPajak')
                }
                additionalClass='col margin-right-m padding__vertical--s'
              >
                <div className='ui-text-3 color-grey-100'>Close</div>
              </Button>
            </div>
          )}
        </div>
      </div>
    )
  }
  return (
    <Modal
      modalType='center'
      dialogClass={unifyBaseStyles['modal-size-desktop']}
      backdropOnClose
      bodyElement={() => addCompanyForm()}
      onClose={() => handleChangeState('contentPajak', null)}
      show={addCompany || addCompanySuccess}
      headerElement='Penambahan Data Perusahaan'
      bodyClass='padding__horizontal--none margin-top-xs'
    />
  )
}
