import React, { useEffect, useState } from 'react'
import config from '../../../../config'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
// import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import TWBaseModal from '../../tw-components/modals/TWBaseModal'
import Button from '../../layouts/atoms/Button'

export default function StatusFakturModal() {
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const [labelData, setLabelData] = useState([])

  // const unifyBaseStyles = useUnifyAppsContext().unifyBaseStyles
  const fakturStatus = detailTransactionContext?.fakturStatus?.[0]

  const labels = [
    {
      name: 'Anda telah mengajukan faktur pajak dan\nmenunggu untuk diproses',
      position: 1,
    },
    {
      name: 'Request faktur pajak telah diproses oleh\ntoko dan menunggu diproses oleh bagian pajak',
      position: 2,
    },
    {
      name: 'Faktur pajak telah selesai diproses dan\nsudah dapat didownload',
      position: 3,
    },
  ]

  const revisedLabels = [
    {
      name: 'Anda telah mengajukan perbaikan faktur pajak\ndan menunggu untuk diproses',
      position: 1,
    },
    {
      name: 'Request perbaikan faktur pajak telah diproses\noleh toko dan menunggu diproses oleh bagian pajak',
      position: 2,
    },
    {
      name: 'Perbaikan faktur pajak telah selesai diproses\ndan sudah dapat didownload',
      position: 3,
    },
  ]

  const canceledLabel = [
    { name: 'Pengajuan faktur pajak Anda sudah dibatalkan' },
  ]

  // const status =
  //   fakturStatus?.Status === 'new' || fakturStatus?.Status === 'new_revised'
  //     ? 1
  //     : fakturStatus?.Status === 'processing'
  //       ? 2
  //       : fakturStatus?.Status === 'completed'
  //         ? 3
  //         : 0

  // const iconCheckStatus = (label) => {
  //   if (fakturStatus?.Status === 'canceled') {
  //     return (
  //       <img
  //         src={config.assetsURL + 'icon/icon-infobox-error.svg'}
  //         width={30}
  //         height={30}
  //         className='padding-right-s'
  //       />
  //     )
  //   } else {
  //     if (status >= label.position) {
  //       return (
  //         <img
  //           src={config.assetsURL + 'icon/icon-infobox-success.svg'}
  //           width={30}
  //           height={30}
  //           className='padding-right-s'
  //         />
  //       )
  //     } else {
  //       return (
  //         <img
  //           src={config.assetsURL + 'icon/icon-infobox-warning.svg'}
  //           width={30}
  //           height={30}
  //           className='padding-right-s'
  //         />
  //       )
  //     }
  //   }
  // }

  // const statusIndicator = () => {
  //   let getLabel
  //   if (fakturStatus?.Status === 'canceled') {
  //     getLabel = canceledLabel
  //   } else if (fakturStatus?.Revisi === 'yes') {
  //     getLabel = revisedLabels
  //   } else {
  //     getLabel = labels
  //   }
  //   return (
  //     <div className='container-card'>
  //       <table>
  //         <tbody className='flex-col'>
  //           {getLabel.map((label, index) => (
  //             <tr key={index} className='margin-bottom-m'>
  //               <td className='flex items-center'>
  //                 {iconCheckStatus(label)}
  //                 <p className='ui-text-1'>{label.name}</p>
  //               </td>
  //             </tr>
  //           ))}
  //         </tbody>
  //       </table>
  //     </div>
  //   )
  // }

  useEffect(() => {
    if (fakturStatus?.Status === 'canceled') {
      setLabelData(canceledLabel)
    } else if (fakturStatus?.Revisi === 'yes') {
      setLabelData(revisedLabels)
    } else {
      setLabelData(labels)
    }
  }, [fakturStatus])

  // return (
  //   <Modal
  //     modalType='center'
  //     dialogClass={unifyBaseStyles['modal-size-desktop']}
  //     backdropOnClose
  //     bodyElement={() => statusIndicator()}
  //     onClose={() => handleChangeState('contentPajak', null)}
  //     headerElement={`Status Faktur Pajak ${fakturStatus?.InvoiceNo}`}
  //     show={detailTransactionContext?.clickHeader}
  //     bodyClass='padding__horizontal--none margin-top-xs'
  //   />
  // )
  return (
    <TWBaseModal
      backdropOnClose
      animationType='fade'
      customPadding={config.environment === 'mobile' && 'tw-w-80'}
      isShow={!detailTransactionContext?.clickHeader}
      modalType='middle-small'
      setIsShow={() => handleChangeState('contentPajak', null)}
    >
      <div className='tw-flex tw-flex-col tw-gap-2 tw-p-5 md:tw-gap-5 md:tw-p-0'>
        <h1 className='tw-text-lg tw-font-bold tw-mt-4 md:tw-text-xl'>
          Status Faktur Pajak {fakturStatus?.InvoiceNo}
        </h1>
        <div
          className='tw-relative tw-m-2 tw-list-none'
          style={{ borderLeft: '2px solid #62ad47' }}
        >
          {labelData.map((label, index) => (
            <li
              key={index}
              className={`tw-ms-4 ${
                index !== labelData.length - 1 ? 'tw-mb-10' : ''
              }`}
            >
              <div className='tw-absolute tw-w-4 tw-h-4 tw-bg-green-50 tw-rounded-full tw-mt-1 -tw-start-[9px]'></div>
              <p className='tw-text-sm md:tw-text-base'>{label.name}</p>
            </li>
          ))}
        </div>
        <Button
          type='primary-border'
          size='medium'
          handleOnClick={() => handleChangeState('contentPajak', null)}
        >
          <p className='tw-text-sm md:tw-text-base'>Kembali</p>
        </Button>
      </div>
    </TWBaseModal>
  )
}
