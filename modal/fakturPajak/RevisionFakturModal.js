import React, { useState } from 'react'
import config from '../../../../config'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import { useAddFakturRevision } from '../../services/api/mutations/unifyapps'
import Button from '../../layouts/atoms/Button'
import Modal from '../../layouts/templates/Modal'
import Accordion from '../../layouts/moleculs/Accordion'
import isEmpty from 'lodash/isEmpty'
import CheckBoxRadioList from '../../../../src/unifyapps/components/CheckBoxRadioList'
import clsx from 'clsx'

export default function RevisionFakturModal() {
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const unifyBaseStyles = useUnifyAppsContext().unifyBaseStyles
  const fakturStatus = detailTransactionContext?.fakturStatus?.[0]
  const { mutate: fakturRevision } = useAddFakturRevision({
    enabled: false,
    onSuccess: (res) => {
      if (isEmpty(res?.data?.errors)) {
        handleChangeState('contentPajak', 'revisionRequestPajak')
      }
    },
  })
  const [textAreaValue, setTextAreaValue] = useState({
    npwp: '',
    namaPt: '',
    alamat: '',
    nominal: '',
    kepemilikan: '',
    other: '',
  })
  const [revisionReason, setRevisionReason] = useState([
    { id: 0, value: 'npwp', name: 'Kesalahan NPWP', isActive: false },
    { id: 1, value: 'namaPt', name: 'Kesalahan nama PT', isActive: false },
    {
      id: 2,
      value: 'alamat',
      name: 'Kesalahan penulisan alamat',
      isActive: false,
    },
    { id: 3, value: 'nominal', name: 'Kesalahan nominal', isActive: false },
    { id: 4, value: 'kepemilikan', name: 'Bukan punya saya', isActive: false },
    { id: 5, value: 'other', name: 'Lainnya', isActive: false },
  ])

  const companyDataDetail = () => {
    return (
      <div>
        <div className='flex padding-xs bg-grey-10'>
          <img
            src={config.assetsURL + 'icon/icon-earth.svg'}
            className='margin-right-m'
          />
          <p>{fakturStatus?.CompanyName}</p>
        </div>
        <div className='flex padding-xs margin-bottom-s margin-top-xs bg-grey-10'>
          <img
            src={config.assetsURL + 'icon/icon-card.svg'}
            className='margin-right-m'
          />
          <p>{fakturStatus?.CompanyNPWP}</p>
        </div>

        <p className='ui-text-3 color-grey-50 text-semi-bold'>
          Alamat NPWP/SPPKP?
        </p>
        {/* showing radio button */}
        <div className='row'>
          <div className='margin-right-m'>
            <input
              type='radio'
              checked={fakturStatus?.IsNpwpAddress === 'yes'}
              disabled
            />
            <label className='margin-left-m'>Ya</label>
          </div>
          <div>
            <input
              type='radio'
              checked={fakturStatus?.IsNpwpAddress === 'no'}
              disabled
            />
            <label className='margin-left-m'>Tidak</label>
          </div>
        </div>

        <div className='search-input w-100 margin-bottom-s margin-top-m'>
          <textarea
            placeholder={
              isEmpty(fakturStatus?.CompanyAddress)
                ? 'Faktur pajak akan diterbitkan sesuai dengan alamat NPWP/SPPKP terdaftar'
                : fakturStatus?.CompanyAddress
            }
            rows='4'
            disabled
            className={`${unifyBaseStyles['input-bar']} padding-left-xxl padding-top-xs padding__horizontal--xs`}
          />
          <img
            src={config.assetsURL + 'icon/icon-location.svg'}
            className={`${unifyBaseStyles['input-bar__icon']} padding-top-xs`}
          />
        </div>
        <div className='flex padding-xs margin-bottom-s margin-top-xs bg-grey-10'>
          <img
            src={config.assetsURL + 'icon/Mail.svg'}
            className='margin-right-m'
          />
          <p>{fakturStatus?.RecipientEmail}</p>
        </div>
        {/* // image */}
        <div className='flex'>
          <img src={fakturStatus?.NPWPImage} />
        </div>
      </div>
    )
  }
  const companyData = () => {
    return (
      <div>
        <Accordion
          containerClass='accordion__no-border-bottom padding__horizontal--none'
          textClass='padding-top-none'
          titleContent={
            <span className='ui-text-1 color-blue-50 text-semi-bold'>
              Data Perusahaan Anda
            </span>
          }
          title=''
          customFillChevron='color-blue-50'
          contentClass='margin-top-s'
          content={companyDataDetail()}
        />
      </div>
    )
  }

  const handleSelectFilter = (optionData) => {
    setTextAreaValue((prev) => ({ ...prev, [optionData.value]: '' }))
    setRevisionReason((prevRevReason) => {
      const newTempRevReason = JSON.parse(JSON.stringify(prevRevReason))
      newTempRevReason.map((item) => {
        if (item.id === optionData.id) {
          item.isActive = !item.isActive
        }
        return item
      })
      return newTempRevReason
    })
  }

  const submitRevision = async () => {
    const params = {
      data: {
        Npwp: textAreaValue.npwp,
        NamaPt: textAreaValue.namaPt,
        Alamat: textAreaValue.alamat,
        Nominal: textAreaValue.nominal,
        Kepemilikan: textAreaValue.kepemilikan,
        Other: textAreaValue.other,
        InvoiceNo: fakturStatus.InvoiceNo,
        FormPD: '',
      },
    }
    let valid = true
    if (revisionReason[0].isActive && textAreaValue.npwp.length < 10) {
      valid = false
    } else if (revisionReason[1].isActive && textAreaValue.namaPt.length < 10) {
      valid = false
    } else if (revisionReason[2].isActive && textAreaValue.alamat.length < 10) {
      valid = false
    } else if (
      revisionReason[3].isActive &&
      textAreaValue.nominal.length < 10
    ) {
      valid = false
    } else if (
      revisionReason[4].isActive &&
      textAreaValue.kepemilikan.length < 10
    ) {
      valid = false
    } else if (revisionReason[5].isActive && textAreaValue.other.length < 10) {
      valid = false
    }

    if (!valid) {
      window.alert('Setiap alasan harus terdiri dari minimal 10 karakter!')
      return false
    } else {
      fakturRevision(params)
    }
  }

  const revisionContent = () => {
    return (
      <div className='container-card'>
        <p className='ui-text-2 color-grey-50 text-semi-bold'>
          Alasan Ketidaksesuaian (Boleh pilih lebih dari 1)
        </p>
        <CheckBoxRadioList
          type='checkbox'
          data={revisionReason}
          onChange={handleSelectFilter}
          labelContainerClassName='row reverse'
          labelTextClassName='margin-left-m'
        >
          {(item) =>
            item.isActive && (
              <textarea
                placeholder='Masukan detil alasan ketidaksesuaian'
                rows='2'
                onChange={(e) =>
                  setTextAreaValue({
                    ...textAreaValue,
                    [item.value]: e.target.value,
                  })
                }
                className={`${unifyBaseStyles['input-bar']} ui-text-3 padding-left-s padding-top-xs padding__horizontal--xs`}
              />
            )
          }
        </CheckBoxRadioList>
        {companyData()}
        <Button
          type='disabled'
          size='medium'
          handleOnClick={() =>
            handleChangeState('contentPajak', 'downloadPajak')
          }
          additionalClass='col padding__vertical--s margin-bottom-m'
        >
          <div className='ui-text-3 color-grey-100'>Batalkan</div>
        </Button>
        <Button
          type='primary'
          size='medium'
          handleOnClick={() => submitRevision()}
          additionalClass='col margin-right-m padding__vertical--s'
        >
          <div className='ui-text-3 text-semi-bold'>Kirim</div>
        </Button>
      </div>
    )
  }
  return (
    <Modal
      backdropOnClose={config.environment === 'desktop'}
      bodyClass={clsx(
        'padding__horizontal--none margin-top-xs tw-h-full tw-overflow-y-auto',
        config.environment === 'desktop' ? '' : 'tw-max-h-[568px]',
      )}
      bodyElement={() => revisionContent()}
      contentClass={
        config.environment === 'desktop' ? 'tw-h-[73%]' : 'tw-h-[75%]'
      }
      customStyled='!tw-flex tw-items-center'
      dialogClass={clsx(
        'tw-flex tw-h-full tw-items-center',
        config.environment === 'desktop'
          ? '!tw-max-w-[700px] tw-w-[700px]'
          : 'tw-justify-center',
      )}
      headerElement='Perbaikan Faktur Pajak'
      onClose={() => handleChangeState('contentPajak', null)}
      show={detailTransactionContext?.clickHeader}
    />
  )
}
