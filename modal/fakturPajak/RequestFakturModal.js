import React, { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import config from '../../../../config'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import Button from '../../layouts/atoms/Button'
import Infobox from '../../layouts/atoms/Infobox'
import Dropdown from '../../layouts/moleculs/Dropdown'
import isEmpty from 'lodash/isEmpty'
import clsx from '../../utils/Clsx'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import { useGetFakturData } from '../../services/api/queries/unifyapps'
import {
  useAddFakturInvoice,
  useDeleteFakturData,
} from '../../services/api/mutations/unifyapps'
import Modal from '../../layouts/templates/Modal'
import TahuWordingContent from '../../layouts/atoms/TahuWordingContent'

const FloatingInfobox = dynamic(
  () => import('../../layouts/atoms/FloatingInfobox'),
)

export default function RequestFakturModal() {
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const unifyBaseStyles = useUnifyAppsContext().unifyBaseStyles
  const [showDropdown, setShowDropdown] = useState(false)
  const [pajakInformationCompany, setPajakInformationCompany] = useState(null)
  const [selectedFakturDelete, setSelectedFakturDelete] = useState(null)
  const [toggleFloatingInfobox, setToggleFloatingInfobox] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [typeInfoBox, setTypeInfoBox] = useState('info')

  const contentPajakTnc = detailTransactionContext?.contentPajak === 'tncPajak'
  const contentPajakCompanyInfo =
    detailTransactionContext?.contentPajak === 'companyInformationPajak'
  const contentDeleteFaktur =
    detailTransactionContext?.contentPajak === 'deleteFaktur'

  const { data: fakturData, refetch: refetchFakturData } = useGetFakturData(
    null,
    { enabled: false },
  )
  const { mutate: deleteFaktur, isSuccess } = useDeleteFakturData({
    onSuccess: (res) => {
      if (!res?.data?.error) {
        setErrorMessage('Faktur pajak berhasil dihapus')
      } else {
        setErrorMessage(res?.data?.error)
        setTypeInfoBox('warning')
      }
      setToggleFloatingInfobox(true)
      setShowDropdown(false)
      handleContentPajakContext('companyInformationPajak')
    },
  })
  const { mutate: addFakturInvoice, isLoading } = useAddFakturInvoice({
    onSuccess: (res) => {
      if (res?.ok) {
        handleContentPajakContext('submitRequestPajak')
      }
    },
  })

  useEffect(() => {
    refetchFakturData()
  }, [isSuccess])

  const fakturPajakTnc = () => {
    return (
      <>
        <div className='ui-text-2 padding__horizontal--m padding-bottom-m margin-left-s color-grey-50 text-justify'>
          <TahuWordingContent title='tnc-faktur-pajak' companyCodeAll='All' />
        </div>

        <div className='row justify-between padding__horizontal--m'>
          <Infobox
            text='Faktur pajak yang diberikan akan disesuaikan dengan harga setelah dipotong oleh voucher'
            theme='info'
            className='margin-bottom-m'
            textClassName='margin-xs ui-text-3 text-bold color-grey-50'
          />
        </div>
      </>
    )
  }

  const fakturPajakCompanyInformation = () => {
    return (
      <div className='ui-text-2 padding__horizontal--m padding-bottom-m color-grey-50 text-justify text-semi-bold'>
        <div>Pilih informasi faktur Pajak</div>
        <Dropdown
          data={fakturData}
          dataTextKey='company_name'
          filterDropdownData
          isDropdownOpen={showDropdown}
          setIsDropdownOpen={setShowDropdown}
          setSelectedData={(data) => setPajakInformationCompany(data)}
          placeholder={
            !isEmpty(pajakInformationCompany?.company_name)
              ? pajakInformationCompany?.company_name
              : '-- Pilih Informasi Faktur Pajak --'
          }
          iconDelete
          showAction={(selected) => {
            handleContentPajakContext('deleteFaktur')
            setSelectedFakturDelete(selected)
          }}
          className={unifyBaseStyles['dropdown-round-xs']}
          extraButton='+ Tambah Informasi Faktur Pajak'
          handleExtraButton={() =>
            handleContentPajakContext('addNewInformation')
          }
        />
      </div>
    )
  }

  const handleContentPajakContext = (data) => {
    handleChangeState('contentPajak', data)
    if (!data) {
      document.body.style.overflow = null
      document.body.classList.remove('modal-open')
    }
  }

  const handleOnClick = () => {
    const orderData = detailTransactionContext.results
    if (contentPajakTnc) {
      handleContentPajakContext('companyInformationPajak')
    } else {
      addFakturInvoice({
        customer_company_id: parseInt(
          pajakInformationCompany?.customer_company_id,
        ),
        invoice_no: [orderData.invoice_no],
        status: 'new',
        order_no: orderData.sales_order.order_no,
      })
    }
  }

  const renderBodyElement = () => {
    if (contentPajakTnc || contentPajakCompanyInfo) {
      return (
        <div className='container-card'>
          {contentPajakTnc ? fakturPajakTnc() : fakturPajakCompanyInformation()}
          <div className='row justify-between padding__horizontal--m'>
            <Button
              type='link-primary-transparent'
              size='medium'
              handleOnClick={() => handleContentPajakContext(null)}
              additionalClass='col margin-right-m padding__vertical--s'
            >
              <div className='ui-text-3 color-grey-100'>Tutup</div>
            </Button>
            <Button
              type='primary'
              size='medium'
              fetching={!contentPajakTnc && isLoading}
              handleOnClick={() =>
                !contentPajakTnc &&
                isEmpty(pajakInformationCompany?.company_name)
                  ? null
                  : handleOnClick()
              }
              additionalClass={clsx(
                {
                  disabled:
                    !contentPajakTnc &&
                    isEmpty(pajakInformationCompany?.company_name),
                },
                'col padding__vertical--s',
              )}
            >
              {isLoading ? (
                <img
                  src={
                    config.assetsURL +
                    `${
                      config.environment === 'mobile' ? 'icon' : 'images'
                    }/loading-ruparupa.gif`
                  }
                  width='24px'
                  height='24px'
                  alt='loading'
                />
              ) : (
                <div className='ui-text-3'>
                  {contentPajakTnc ? 'Mengerti' : 'Lanjutkan'}
                </div>
              )}
            </Button>
          </div>
        </div>
      )
    } else if (contentDeleteFaktur) {
      return (
        <div className='container-card'>
          <div className='ui-text-1 color-grey-50'>
            Apakah kamu yakin ingin menghapus faktur pajak terpilih?
          </div>
          <div className='row justify-between padding-top-m'>
            <Button
              type='primary-border'
              size='medium'
              handleOnClick={() =>
                handleContentPajakContext('companyInformationPajak')
              }
              additionalClass='col margin-right-m padding-s'
            >
              <div className='ui-text-3 text-semi-bold'>Batal</div>
            </Button>
            <Button
              type='primary'
              size='medium'
              handleOnClick={() =>
                deleteFaktur({
                  customer_company_id: parseInt(
                    selectedFakturDelete.customer_company_id,
                  ),
                })
              }
              additionalClass='col padding-s'
            >
              <div className='ui-text-3 text-semi-bold'>Ya, Hapus</div>
            </Button>
          </div>
        </div>
      )
    }
  }

  return (
    <div>
      <Modal
        modalType='center'
        dialogClass={unifyBaseStyles['modal-size-desktop']}
        bodyElement={() => renderBodyElement()}
        onClose={() => handleContentPajakContext(null)}
        show={contentPajakTnc || contentPajakCompanyInfo || contentDeleteFaktur}
        bodyClass='padding__horizontal--none margin-top-xs'
        backdropOnClose
        headerElement={
          contentDeleteFaktur
            ? 'Hapus Faktur Pajak'
            : 'Informasi Proses Faktur Pajak'
        }
      />
      {toggleFloatingInfobox && (
        <FloatingInfobox
          duration={2500}
          toggleFlag={toggleFloatingInfobox}
          type={`${typeInfoBox}-modal`}
          additionalButton={{ text: 'OK' }}
          additionalButtonClose
          setToggle={setToggleFloatingInfobox}
          wrapperClassName='infobox-wrapper__floating-over-modal'
        >
          {errorMessage}
        </FloatingInfobox>
      )}
    </div>
  )
}
