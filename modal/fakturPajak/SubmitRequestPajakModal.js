import React from 'react'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import Button from '../../layouts/atoms/Button'
import Modal from '../../layouts/templates/Modal'

export default function SubmitRequestPajakModal() {
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const unifyBaseStyles = useUnifyAppsContext().unifyBaseStyles
  const submitRequestPajak =
    detailTransactionContext?.contentPajak === 'submitRequestPajak'
  const revisionRequestPajak =
    detailTransactionContext?.contentPajak === 'revisionRequestPajak'

  const addDays = () => {
    let dateObj = new Date(+new Date() + 12096e5)
    var idx = dateObj.getUTCMonth() + 1 // months from 1-12
    var day = dateObj.getUTCDate()
    var year = dateObj.getUTCFullYear()

    const listMonth = [
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember',
    ]
    const finishDay = `${day} ${listMonth[idx - 1]} ${year}`
    return finishDay
  }

  const successHeader = [
    'Penambahan Faktur Pajak Berhasil',
    'Pengajuan Perbaikan Faktur Pajak Berhasil',
  ]

  const successText = [
    <span key='success-faktur-pajak' className='text-medium color-grey-50'>
      Terima kasih, faktur pajak Anda akan segera diproses dalam 14 hari kerja
      (perkiraan tanggal penerimaan faktur pajak){' '}
      <span className='text-bold'>{addDays()}</span> dan faktur pajak yang akan
      Anda terima akan terpisah berdasarkan invoice dari masing-masing toko yang
      mengirimkan barang yang Anda pesan.
    </span>,
    'Terima kasih, faktur pajak akan kami proses kembali sesuai dengan perbaikan yang anda tulis di kolom sebelumnya.',
  ]

  const successSubmitPajak = () => {
    return (
      <div className='container-card'>
        <div className='ui-text-2 padding__horizontal--m padding-bottom-m color-grey-50 text-justify'>
          <div>
            <div className='margin-bottom-m'>
              {revisionRequestPajak ? successText[1] : successText[0]}
            </div>
            <Button
              type='primary'
              size='medium'
              handleOnClick={() => window.location.reload()}
              additionalClass='col margin-right-m padding__vertical--s'
            >
              <div className='ui-text-3'>Kembali</div>
            </Button>
          </div>
        </div>
      </div>
    )
  }
  return (
    <Modal
      modalType='center'
      dialogClass={unifyBaseStyles['modal-size-desktop']}
      backdropOnClose
      headerElement={revisionRequestPajak ? successHeader[1] : successHeader[0]}
      headerClass='margin-right-xs margin-left-xs'
      bodyElement={() => successSubmitPajak()}
      onClose={() => handleChangeState('contentPajak', null)}
      show={submitRequestPajak}
      bodyClass='padding__horizontal--none margin-top-xs'
    />
  )
}
