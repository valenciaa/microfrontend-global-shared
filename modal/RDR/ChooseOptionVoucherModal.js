import React from 'react'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { usePutVoucherRefund } from '../../services/api/mutations/unifyapps'
import { usePutVoucherRefundDc } from '../../services/api/mutations/global'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import Button from '../../layouts/atoms/Button'
import clsx from '../../utils/Clsx'
import config from '../../../../config'
import Modal from '../../layouts/templates/Modal'

export default function ChooseOptionVoucherModal() {
  const unifyAppsContext = useUnifyAppsContext()
  const unifyBaseStyles = unifyAppsContext?.unifyBaseStyles
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const orderData = detailTransactionContext.results
  const RDRModal = detailTransactionContext?.RDRModal

  const { mutate: voucherRefund, isVoucherLoading } = usePutVoucherRefund({
    enabled: false,
    onSuccess: (res) => {
      if (!res?.data?.error) {
        handleChangeState('RDRModal', null)
        window.location.reload()
      }
    },
  })

  const { mutate: voucherRefundDc } = usePutVoucherRefundDc({
    enabled: false,
    onSuccess: (res) => {
      if (!res?.data?.error) {
        handleChangeState('RDRModal', null)
        window.location.reload()
      }
    },
  })

  const handleButton = (textButton = '', onClick, disabled, isFetching) => {
    return (
      <div className='row justify-between padding-top-m'>
        <Button
          type='secondary'
          size='medium'
          handleOnClick={() => handleChangeState('RDRModal', 'refundOption')}
          additionalClass='col margin-right-m'
        >
          <div className='ui-text-3 text-semi-bold'>Batal</div>
        </Button>
        <Button
          type='primary'
          size='medium'
          handleOnClick={onClick}
          fetching={isFetching}
          additionalClass={clsx({ disabled: disabled }, 'col')}
        >
          {isFetching ? (
            <img
              src={
                config.assetsURL +
                `${
                  config.environment === 'mobile' ? 'icon' : 'images'
                }/loading-ruparupa.gif`
              }
              width='24px'
              height='24px'
              alt='loading'
            />
          ) : (
            <div className='ui-text-3 text-semi-bold'>{textButton}</div>
          )}
        </Button>
      </div>
    )
  }

  const handleChooseVoucherRefund = () => {
    if (orderData?.refund_dc.length > 0) {
      const params = {
        retref_no: orderData?.refund_dc[0]?.refund_no,
        email: orderData?.sales_order?.customer?.customer_email,
        action: 'reveal_voucher',
      }
      voucherRefundDc(params)
    } else {
      voucherRefund({ refund_no: orderData?.refund[0]?.refund_no })
    }
  }

  const bodyElement = () => {
    return (
      <div className='container-card'>
        <div className='ui-text-2 text-center'>
          Apakah Anda yakin untuk memilih opsi Jadikan Voucher?
        </div>
        {handleButton(
          'Lanjut',
          () => handleChooseVoucherRefund(),
          false,
          isVoucherLoading,
        )}
      </div>
    )
  }

  return (
    <Modal
      modalType='center'
      dialogClass={unifyBaseStyles['modal-size-desktop']}
      bodyElement={() => bodyElement()}
      onClose={() => handleChangeState('RDRModal', null)}
      show={RDRModal === 'voucher'}
      bodyClass='padding__horizontal--none margin-top-xs'
      backdropOnClose
      headerElement='Jadikan Voucher'
    />
  )
}
