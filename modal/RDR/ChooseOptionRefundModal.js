import React, { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import isEmpty from 'lodash/isEmpty'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { useGetUserBankAccount } from '../../services/api/queries/global'
import { useUpdateBankAccount } from '../../services/api/mutations/global'
import Button from '../../layouts/atoms/Button'
import config from '../../../../config'
import Infobox from '../../layouts/atoms/Infobox'
import InfoboxInfo from '../../../../public/static/icon/infobox-info-16.svg'
import Modal from '../../layouts/templates/Modal'
import Dropdown from '../../layouts/moleculs/Dropdown'
import clsx from '../../utils/Clsx'
import { useRouter } from 'next/router'
import { saveCurrentPath } from '../../utils/PrevPathSession'

const FloatingInfobox = dynamic(
  () => import('../../../../shared/global/layouts/atoms/FloatingInfobox'),
)

export default function ChooseOptionRefundModal({
  selectedAccount,
  setIsOpenAddBankAccountModal,
}) {
  const router = useRouter()
  const unifyAppsContext = useUnifyAppsContext()
  const unifyBaseStyles = unifyAppsContext?.unifyBaseStyles
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const orderData = detailTransactionContext.results
  const RDRModal = detailTransactionContext?.RDRModal
  const pageFrom = detailTransactionContext?.pageFrom

  const customer = orderData?.sales_order?.customer
  const [checkPrimaryAccount, setCheckPrimaryAccount] = useState(
    customer?.is_already_default_rekening,
  )
  const [showDropdown, setShowDropdown] = useState(false)
  const [accountInformation, setAccountInformation] = useState(null)
  const [selectedAccountDelete, setSelectedAccountDelete] = useState(null)
  const [toggleFloatingInfobox, setToggleFloatingInfobox] = useState(false)
  const [floatingMessage, setFloatingMessage] = useState(null)

  const { data: bankAccountData, refetch: refetchBankAccountData } =
    useGetUserBankAccount(null, { enabled: false })
  const {
    mutate: updateBank,
    isSuccess,
    isUpdateBankLoading,
  } = useUpdateBankAccount({
    onSuccess: (res) => {
      if (!res?.data?.error) {
        setToggleFloatingInfobox(true)

        if (pageFrom === 'installation') {
          setTimeout(() => {
            handleChangeState('RDRModal', null)
            handleChangeState('isSuccess', true)
            handleChangeState('pageFrom', null)
          }, 300)

          return
        }

        if (detailTransactionContext?.RDRModal === 'deleteBankAccount') {
          handleChangeState('RDRModal', 'bankAccount')
        } else {
          handleChangeState('RDRModal', null)
        }
      }

      if (pageFrom === 'installation') {
        handleChangeState('isSuccess', false)
        handleChangeState('pageFrom', null)
      }
    },
  })

  useEffect(() => {
    if (pageFrom === 'installation' && !isEmpty(selectedAccount)) {
      setSelectedAccountDelete(selectedAccount)
    }
  }, [])

  useEffect(() => {
    refetchBankAccountData()
  }, [isSuccess])

  useEffect(() => {
    setAccountInformation(bankAccountData?.[0])
  }, [bankAccountData])

  const handleOnClickCancelButton = () => {
    if (pageFrom === 'installation') {
      handleChangeState('RDRModal', null)
      handleChangeState('pageFrom', null)
      handleChangeState('isSuccess', false)
    } else {
      handleChangeState('RDRModal', 'refundOption')
    }
  }

  const handleButton = (textButton = '', onClick, disabled, isFetching) => {
    return (
      <div className='row justify-between padding-top-m'>
        <Button
          type='secondary'
          size='medium'
          handleOnClick={() => handleOnClickCancelButton()}
          additionalClass='col margin-right-m'
        >
          <div className='ui-text-3 text-semi-bold'>Batal</div>
        </Button>
        <Button
          type='primary'
          size='medium'
          handleOnClick={onClick}
          fetching={isFetching}
          additionalClass={clsx({ disabled: disabled }, 'col')}
        >
          {isFetching ? (
            <img
              src={
                config.assetsURL +
                `${
                  config.environment === 'mobile' ? 'icon' : 'images'
                }/loading-ruparupa.gif`
              }
              width='24px'
              height='24px'
              alt='loading'
            />
          ) : (
            <div className='ui-text-3 text-semi-bold'>{textButton}</div>
          )}
        </Button>
      </div>
    )
  }

  const handleInfoBox = (textInfo = '') => {
    return (
      <Infobox text={textInfo} theme='info' className='margin-top-s'>
        <div className='flex'>
          <InfoboxInfo className='color-blue-60' />
        </div>
      </Infobox>
    )
  }

  const handleTitle = (value) => {
    switch (value) {
      case 'bankAccount':
        return 'Pengembalian Dana'
      case 'deleteBankAccount':
        return 'Hapus Rekening Bank'
      default:
        return 'Pilih Opsi Pengembalian Dana'
    }
  }

  const handleAddAccount = () => {
    if (config.environment === 'mobile') {
      // window.history.replaceState(
      //   {},
      //   window.history.state?.as,
      //   `${window.history.state?.as}&state=modal`,
      // )

      // setTimeout(() => {
      //   window.location.href =
      //     config.baseURL + 'account/create-bank-account?is_default=false'
      // }, 100)
      saveCurrentPath()
      router.push('/my-bank-account/add')
    } else {
      //desktop
      setIsOpenAddBankAccountModal(true)
      handleChangeState('RDRModal', '')
    }
  }

  const showAccountInformation = () => {
    const placeHolder =
      customer?.is_already_default_rekening && !isEmpty(bankAccountData)
        ? `${bankAccountData?.[0]?.bank_name} - ${bankAccountData?.[0]?.account_number} - ${bankAccountData?.[0]?.account_name}`
        : !isEmpty(accountInformation?.bank_name)
          ? `${accountInformation?.bank_name} - ${accountInformation?.account_number} - ${accountInformation?.account_name}`
          : 'Nomor Akun Rekening'
    return (
      <div className='margin-bottom-xs'>
        <Dropdown
          data={bankAccountData}
          dataTextKey='bank_name'
          dataValueKey='account_number'
          dataNameKey='account_name'
          isDropdownOpen={showDropdown}
          setIsDropdownOpen={setShowDropdown}
          setSelectedData={(data) => setAccountInformation(data)}
          placeholder={placeHolder}
          iconDelete
          showAction={(selected) => {
            handleChangeState('RDRModal', 'deleteBankAccount')
            setSelectedAccountDelete(selected)
          }}
          className={`${unifyBaseStyles['dropdown-round-xs']}`}
          textClassName='tw-line-clamp-1'
          disabled={customer?.is_already_default_rekening}
          extraButton={
            (bankAccountData?.length ?? 0) < 3 && '+ Tambah Rekening'
          }
          handleExtraButton={() => handleAddAccount()}
        />
      </div>
    )
  }

  const handleUpdateBank = (option) => {
    const data = {
      customer_id:
        selectedAccountDelete?.customer_id || accountInformation?.customer_id,
      customer_rekening_id:
        selectedAccountDelete?.customer_rekening_id ||
        accountInformation?.customer_rekening_id,
      is_default: option === 'delete' ? false : true,
      is_active: option === 'delete' ? false : true,
    }
    updateBank(data)
    setFloatingMessage(
      option === 'delete'
        ? 'Hapus Rekening Berhasil'
        : 'Sukses menambahkan rekening utama',
    )
  }

  const bodyElement = (value) => {
    return (
      <>
        <div className='container-card'>
          {value === 'bankAccount' ? (
            <>
              <div className='ui-text-3 margin-bottom-m'>
                Silahkan pilih nomor rekening bank yang ingin digunakan untuk
                pengembalian dana
              </div>
              {showAccountInformation()}
              <label className='container-checkbox'>
                <input
                  name='isAgree'
                  id='checkPrimaryAccount'
                  type='checkbox'
                  checked={true}
                  disabled={true}
                  onChange={() => setCheckPrimaryAccount(!checkPrimaryAccount)}
                />
                <span className='checkbox' />
                <span className='ui-text-3 padding-left-xs'>
                  Jadikan Rekening Utama
                </span>
              </label>
              {handleInfoBox(
                'Rekening yang Anda pilih akan dijadikan rekening utama. Untuk melakukan perubahan rekening utama, dapat melalui halaman Akun Saya.',
              )}
              {handleButton(
                'Lanjut',
                () => handleUpdateBank('update'),
                !accountInformation,
                isUpdateBankLoading,
              )}
            </>
          ) : (
            <>
              <div>Apakah Anda yakin untuk menghapus rekening bank?</div>
              {handleButton(
                'Hapus',
                () => handleUpdateBank('delete'),
                false,
                isUpdateBankLoading,
              )}
            </>
          )}
        </div>
        {toggleFloatingInfobox && (
          <FloatingInfobox
            duration={2500}
            additionalAction={() =>
              floatingMessage?.toLowerCase().includes('sukses') &&
              window.location.reload()
            }
            toggleFlag={toggleFloatingInfobox}
            type='success-modal'
            additionalButtonClose
            setToggle={setToggleFloatingInfobox}
            wrapperClassName='infobox-wrapper__floating-over-modal'
          >
            {floatingMessage}
          </FloatingInfobox>
        )}
      </>
    )
  }

  return (
    <Modal
      modalType='center'
      dialogClass={unifyBaseStyles['modal-size-desktop']}
      bodyElement={() => bodyElement(RDRModal)}
      onClose={() => handleChangeState('RDRModal', null)}
      show={RDRModal === 'deleteBankAccount'}
      bodyClass='padding__horizontal--none margin-top-xs'
      backdropOnClose
      headerElement={handleTitle(RDRModal)}
    />
  )
}
