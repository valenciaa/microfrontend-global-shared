import React, { useState } from 'react'
import { useDetailTransactionContext } from '../../../../src/unifyapps/context/DetailTransactionContext'
import { useUnifyAppsContext } from '../../context/UnifyAppsContext'
import Infobox from '../../layouts/atoms/Infobox'
import InfoboxInfo from '../../../../public/static/icon/infobox-info-16.svg'
import Modal from '../../layouts/templates/Modal'
import CheckBoxRadioList from '../../../../src/unifyapps/components/CheckBoxRadioList'

export default function OptionRefundModal() {
  const unifyAppsContext = useUnifyAppsContext()
  const unifyBaseStyles = unifyAppsContext?.unifyBaseStyles
  const { state: detailTransactionContext, handleChangeState } =
    useDetailTransactionContext()
  const RDRModal = detailTransactionContext?.RDRModal

  const [optionRdr, setOptionRdr] = useState([
    { id: 0, name: 'Jadikan Voucher', isActive: false },
    { id: 1, name: 'Pilih Rekening Pengembalian Dana', isActive: false },
  ])
  const handleInfoBox = (textInfo = '') => {
    return (
      <Infobox text={textInfo} theme='info' className='margin-top-s'>
        <div className='flex'>
          <InfoboxInfo className='color-blue-60' />
        </div>
      </Infobox>
    )
  }

  const handleChangeRadio = (selectedOption) => {
    if (selectedOption.id === 0) {
      handleChangeState('RDRModal', 'voucher')
    } else {
      handleChangeState('RDRModal', 'bankAccount')
    }

    setOptionRdr((prev) => {
      const newOptionRdr = JSON.parse(JSON.stringify(prev))
      newOptionRdr.map((item) => {
        if (item.name === selectedOption.name) {
          item.isActive = true
        } else {
          item.isActive = false
        }
      })
      return newOptionRdr
    })
  }
  const bodyElement = () => {
    return (
      <div className='container-card'>
        <CheckBoxRadioList
          type='radio'
          data={optionRdr}
          onChange={handleChangeRadio}
          labelContainerClassName='row reverse'
          labelTextClassName='margin-left-m'
          className={`${unifyBaseStyles['checkboxnoborder']}`}
        />
        {handleInfoBox(
          'Jika Anda memilih Jadikan Voucher, maka keseluruhan dana refund Anda akan menjadi voucher yang dapat anda gunakan kembali untuk belanja di ruparupa.',
        )}
        {handleInfoBox(
          'Jika Anda memilih Pengembalian Dana, maka keseluruhan dana Anda akan dikembalikan ke rekening default yang Anda pilih.',
        )}
      </div>
    )
  }

  return (
    <Modal
      modalType='center'
      dialogClass={unifyBaseStyles['modal-size-desktop']}
      bodyElement={() => bodyElement()}
      onClose={() => handleChangeState('RDRModal', null)}
      show={RDRModal === 'refundOption'}
      bodyClass='padding__horizontal--none margin-top-xs'
      backdropOnClose
      headerElement='Pilih Opsi Pengembalian Dana'
    />
  )
}
